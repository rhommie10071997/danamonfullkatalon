<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>MultiUserInternational(Immediate)</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-02-25T13:44:53</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>2837eb82-26e8-4738-a326-c250aa6d5fd0</testSuiteGuid>
   <testCaseLink>
      <guid>57456cee-1a18-4b52-b71d-704bfacccdd3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-International(Immediate)/MultiUserPayroll-International(Immediate)CreateEntryScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>48bda0e1-9692-41db-8ffc-3524d6713918</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-International(Immediate)/MultiUserPayroll-International(Immediate)BucketListLoop</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>3021cedf-4a9e-4b6c-8bf4-835afbef0615</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>c83378f9-6010-4a61-b4aa-0e30d06ce7c6</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>a62c1e46-6c7a-4221-8804-69ef5bfd6a7c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-International(Immediate)/MultiUserPayroll-International(Immediate)BucketDetails</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>56eb5927-7bd1-43c1-879a-384793c596a5</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>b8b3da4a-1800-4ec4-9b22-ca7a6839cb53</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-International(Immediate)/MultiUserPayroll-International(Immediate)BucketConfirm</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>f0f37509-32f1-4f4c-90d8-03a940fc82f0</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>62da75a1-8cd4-4526-bfd5-8a77d25264e0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-International(Immediate)/MultiUserPayroll-International(Immediate)CreateResultScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0de5051b-070d-4e17-a59f-05fff7813807</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-International(Immediate)/MultiUserPayroll-International(Immediate)ApproverEntryScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b7542603-8036-4180-ab53-00f7e7e6626e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-International(Immediate)/MultiUserPayroll-International(Immediate)ApproverConfrimScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>eb142125-c79b-439b-ac5f-c2f7447f6633</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-International(Immediate)/MultiUserPayroll-International(Immediate)ApproverResultScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d63cf04f-d9d0-4110-a89e-0c2bc07f0218</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-International(Immediate)/MultiUserPayroll-International(Immediate)TransactionStatusEntryScreenReffNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bdfa2e18-1c07-4c6d-b514-404eaeb06d25</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-International(Immediate)/MultiUserPayroll-International(Immediate)TransactionStatusReffNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f5b30993-7deb-460a-b9a8-eab4fde7cc9e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-International(Immediate)/MultiUserPayroll-International(Immediate)TransactionStatusEntryScreenDocNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cbef84b6-c08d-4fca-92f3-28e65eff3139</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-International(Immediate)/MultiUserPayroll-International(Immediate)TransactionStatusDocNo</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
