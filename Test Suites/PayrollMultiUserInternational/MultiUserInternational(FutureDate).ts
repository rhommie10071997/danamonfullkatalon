<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>MultiUserInternational(FutureDate)</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-01-25T17:27:47</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>1b12df19-e966-4652-a0ce-0b8d032d6a5d</testSuiteGuid>
   <testCaseLink>
      <guid>394e7da6-fd52-4105-81be-159f28b5f966</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-International(FutureDate)/MultiUserPayroll-International(FutureDate)CreateEntryScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cc1adfa6-2b96-4462-942c-8926ed3159aa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-International(FutureDate)/MultiUserPayroll-International(FutureDate)BucketListLoop</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>66302658-9738-4d46-9b7c-27c4acf1583e</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>e3687560-3e52-4bd4-940e-cbb2d14078f1</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>92b5e9cc-8018-4b88-a3b7-a12d94bdf6d6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-International(FutureDate)/MultiUserPayroll-International(FutureDate)BucketDetails</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>ed5524df-a6e8-4275-ab41-e06fb335733f</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>b837e8e2-01ee-4317-a09d-ea7846b44265</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-International(FutureDate)/MultiUserPayroll-International(FutureDate)BucketConfirm</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>117b13b6-a0e0-446b-9b95-5417bc439ea1</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>68804fe1-9582-4ff7-b01c-28d680dfe7f8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-International(FutureDate)/MultiUserPayroll-International(FutureDate)CreateResultScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2b654942-0904-475f-bc04-febd09b88c49</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-International(FutureDate)/MultiUserPayroll-International(FutureDate)ApproverEntryScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>23b44bcf-9675-4dcc-98ee-18583d551755</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-International(FutureDate)/MultiUserPayroll-International(FutureDate)ApproverConfrimScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>96f24961-f860-43fe-b6c0-15293493cbf6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-International(FutureDate)/MultiUserPayroll-International(FutureDate)ApproverResultScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9c622d04-8fe6-41cc-ad8c-7b76291e9cc5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-International(FutureDate)/MultiUserPayroll-International(FutureDate)TransactionStatusEntryScreenReffNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>40d3412c-55ed-46f1-8330-a66716dd8fc0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-International(FutureDate)/MultiUserPayroll-International(FutureDate)TransactionStatusReffNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2535b991-a882-48fb-baed-49ddb5ba5ae1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-International(FutureDate)/MultiUserPayroll-International(FutureDate)TransactionStatusEntryScreenDocNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dcb6f0f7-28db-48d5-a41b-fca08663a18f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-International(FutureDate)/MultiUserPayroll-International(FutureDate)TransactionStatusDocNo</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
