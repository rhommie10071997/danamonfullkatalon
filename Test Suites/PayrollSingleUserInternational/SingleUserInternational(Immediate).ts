<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>SingleUserInternational(Immediate)</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-01-25T17:50:17</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>346899f8-b3a0-4eb7-81ad-44c12702ebc7</testSuiteGuid>
   <testCaseLink>
      <guid>5e4510d1-dc04-4931-b069-a292c0b55f2f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-International(Immediate)/SingleUserPayroll-International(Immediate)CreateEntryScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ad4976fa-ab2d-4429-bc2a-c46e5067232f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-International(Immediate)/SingleUserPayroll-International(Immediate)BucketListLoop</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>0aa5e59b-e5f5-43ed-b362-dc7cd9cc97de</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>ae532d5b-88c9-441f-b055-9f9c8f925518</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>4e7cd056-6512-4ce5-946e-7016acffc5fe</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-International(Immediate)/SingleUserPayroll-International(Immediate)BucketDetails</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>385f45dd-191c-4bf4-b680-2160b347fed7</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>fb6b541b-e7a1-4037-b971-1abb0916cb40</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-International(Immediate)/SingleUserPayroll-International(Immediate)BucketConfirm</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>760988a2-b297-4305-a075-5ca0fe0def30</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>2fafc70c-78a9-4d88-b835-f0669a5fa0cc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-International(Immediate)/SingleUserPayroll-International(Immediate)CreateResultScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f9aa6f7c-d13a-47e9-bc77-7496df109a4c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-International(Immediate)/SingleUserPayroll-International(Immediate)TransactionStatusEntryScreenReffNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b3169b2f-99dc-4fbc-be98-313d1f086f7d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-International(Immediate)/SingleUserPayroll-International(Immediate)TransactionStatusReffNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7d6c6955-4edc-47e9-ad29-3c86c5ffa28f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-International(Immediate)/SingleUserPayroll-International(Immediate)TransactionStatusEntryScreenDocNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4f13e229-409b-4b15-b4cc-c8424f66e584</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-International(Immediate)/SingleUserPayroll-International(Immediate)TransactionStatusDocNo</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
