<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>SingleUserInternational(FutureDate)</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-01-25T18:06:05</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>0a23ace9-5c70-427e-9164-3d071d382770</testSuiteGuid>
   <testCaseLink>
      <guid>ae031b8e-82a3-4f73-8bfa-9b4d3d6399ab</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-International(FutureDate)/SingleUserPayroll-International(FutureDate)CreateEntryScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a5d11df6-73d2-4a6a-900f-9ae15958c34c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-International(FutureDate)/SingleUserPayroll-International(FutureDate)BucketListLoop</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>75e13a89-a7cc-44cd-9736-da7fdb284fb6</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>44ae2853-e71d-4890-b273-6e870982fdbb</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>5e52235f-5762-41c6-b834-a9a4be8508ea</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-International(FutureDate)/SingleUserPayroll-International(FutureDate)BucketDetails</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>928a94b2-35df-4424-9f19-2756cdf08270</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>2056a540-d889-499b-9c72-05a2e5635ec7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-International(FutureDate)/SingleUserPayroll-International(FutureDate)BucketConfirm</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>c43a5990-774f-46dd-ae08-9d5e95a88842</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>bae49325-b114-47b1-a2b8-d679fce9cb7c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-International(FutureDate)/SingleUserPayroll-International(FutureDate)CreateResultScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8adad490-90cd-449a-9106-210f524d22ec</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-International(FutureDate)/SingleUserPayroll-International(FutureDate)TransactionStatusEntryScreenReffNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bc8855a1-60d8-436c-aefd-40e477f7429e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-International(FutureDate)/SingleUserPayroll-International(FutureDate)TransactionStatusReffNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3f75638a-32af-4ee1-97f9-d635413694d3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-International(FutureDate)/SingleUserPayroll-International(FutureDate)TransactionStatusEntryScreenDocNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d3047318-0fd4-470b-a118-4e268e783f87</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-International(FutureDate)/SingleUserPayroll-International(FutureDate)TransactionStatusDocNo</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
