<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>rangeBalanceMultiImme</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-01-16T15:27:05</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>9376420d-3026-4e9f-b0c0-18c80768526e</testSuiteGuid>
   <testCaseLink>
      <guid>5a8a1443-00f8-46a4-aee2-e3ad3b325e72</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/url</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8fc516b7-7b5a-49e3-b7cf-7f75ba9622c3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RangeBalance/rangeBalanceMultiImme/rangeBalanceMultiImme(Maker)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>aaeee2bb-11a9-4cd4-af4e-ed46fecba185</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RangeBalance/rangeBalanceMultiImme/rangeBalanceMultiImme(Maker.Result)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c7f1b2da-a3f3-421c-83e0-ae8f74743443</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/pendingTask/pendingTask</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a9d64f01-e870-4cee-a613-be86094f320b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RangeBalance/rangeBalanceMultiImme/rangeBalanceMultiImme(Approver)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dabcb0e8-fd67-4940-9d58-115b19b36a0d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RangeBalance/rangeBalanceMultiImme/rangeBalanceMultiImme(Approver.Result)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fd513f40-1fdb-4996-9bd7-a7e0f91cf551</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/transStatus/transStatus</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>53f2dc46-c5cc-46ed-be10-321e26af74b9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RangeBalance/rangeBalanceMultiImme/rangeBalanceMultiImme(TransStatus)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>70fc23fd-afaa-45fd-8cb9-80a7e2ac3553</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RangeBalance/rangeBalanceMultiImme/rangeBalanceMultiImme(TransStatus_2)</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
