<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>rangeBalanceMultiRepeat</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-01-18T16:54:16</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>4a7596d1-979a-451e-8312-87b27e55a005</testSuiteGuid>
   <testCaseLink>
      <guid>9616b53a-6409-4c58-9a40-ecb9ecec07fc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/url</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cacde8fd-94b7-4d0f-81e0-527642f686ed</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RangeBalance/rangeBalanceMultiRepeat/rangeBalanceMultiRepeat(Maker)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c607ce7b-78c6-4aa1-9ba6-8566599d1800</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RangeBalance/rangeBalanceMultiRepeat/rangeBalanceMultiRepeat(Maker.Result)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e120db2b-2faf-4da0-a482-b1063fbe9847</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/pendingTask/pendingTask</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fea95b36-c99a-461f-bde5-8d7289d3b541</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RangeBalance/rangeBalanceMultiRepeat/rangeBalanceMultiRepeat(Approver)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3ddf185b-4e9a-46e4-97b5-02636fb635a3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RangeBalance/rangeBalanceMultiRepeat/rangeBalanceMultiRepeat(Approver.Result)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d3d9e8e7-c80a-4712-a10e-3c3a26831755</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/transStatus/transStatus</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>93aee832-c221-4e48-bb35-67c6e45e8658</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RangeBalance/rangeBalanceMultiRepeat/rangeBalanceMultiRepeat(TransStatus)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c3fad4cd-34f9-42f5-9d50-b684c5ce6930</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RangeBalance/rangeBalanceMultiRepeat/rangeBalanceMultiRepeat(TransStatus_2)</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
