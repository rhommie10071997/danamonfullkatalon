<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>rangeBalanceMultiFuture</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-01-16T16:47:25</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>54195ce7-e753-450f-97c8-fd1eeddb0808</testSuiteGuid>
   <testCaseLink>
      <guid>91008362-f856-409e-a87b-1247c5564ba9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/url</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>59aa091f-af3b-4921-b826-3af7a8e76c84</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RangeBalance/rangeBalanceMultiFuture/rangeBalanceMultiFuture(Maker)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ee6927c1-b4a5-4d1d-9377-79bd3676c6dc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RangeBalance/rangeBalanceMultiFuture/rangeBalanceMultiFuture(Maker.Result)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4af224da-38ea-4565-981b-8aca0cca509d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/pendingTask/pendingTask</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>05f7b99d-6bed-41ec-b29c-8108e55a9c2e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RangeBalance/rangeBalanceMultiFuture/rangeBalanceMultiFuture(Approver)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>579f9797-548d-47ed-9ee8-af0d5013d4d5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RangeBalance/rangeBalanceMultiFuture/rangeBalanceMultiFuture(Approver.Result)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>98c11c3c-7fe5-493c-aa43-1918219f73b1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/transStatus/transStatus</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4553d969-ece6-4e7c-9c28-996c6e99295e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RangeBalance/rangeBalanceMultiFuture/rangeBalanceMultiFuture(TransStatus)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>352d2c2d-81ed-4e32-a3ad-b8959fff8882</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RangeBalance/rangeBalanceMultiFuture/rangeBalanceMultiFuture(TransStatus_2)</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
