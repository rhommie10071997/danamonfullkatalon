<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>rangeBalanceSingleImme</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-01-21T11:07:25</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>350839b6-dc46-479f-aa0a-89b04c70ab12</testSuiteGuid>
   <testCaseLink>
      <guid>9f757f99-d916-443e-ad2b-5d0c9d8f4def</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/url</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>08711ada-5e86-4943-816a-2b02fa82a573</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RangeBalance/rangeBalanceSingleImme/rangeBalanceSingleImme(Maker) (1)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>662f0857-0e50-47f0-b8dd-0464de197df4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RangeBalance/rangeBalanceSingleImme/rangeBalanceSingleImme(Maker.Result)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fcb8fae6-05b4-4367-9636-938be865fae8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/transStatus/transStatus</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a5b079d9-9a2c-4018-aaac-2636b5c43217</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RangeBalance/rangeBalanceSingleImme/rangeBalanceSingleImme(TransStatus)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b814e274-9ef3-401f-9ba9-409a2f66d4af</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RangeBalance/rangeBalanceSingleImme/rangeBalanceSingleImme(TransStatus_2)</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
