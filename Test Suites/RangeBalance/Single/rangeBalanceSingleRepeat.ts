<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>rangeBalanceSingleRepeat</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-10-10T14:45:35</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>6ad7e4b7-f9dc-47be-85fa-f17c8335f1c9</testSuiteGuid>
   <testCaseLink>
      <guid>150d82f2-1e9a-49c7-a708-89bdf7f1f620</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/url</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f4585baa-c4f3-4fd1-9469-36eb5bb32804</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RangeBalance/rangeBalanceMultiRepeat/rangeBalanceMultiRepeat(Maker)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9c1141d7-fc1d-444b-a0b8-2fe4263580ff</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RangeBalance/rangeBalanceMultiRepeat/rangeBalanceMultiRepeat(Maker.Result)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5dce5a87-8c41-4ecb-9d72-72115d2a7e38</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/transStatus/transStatus</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6b760b37-383b-4f2e-9da7-f549c852d93d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RangeBalance/rangeBalanceMultiRepeat/rangeBalanceMultiRepeat(TransStatus)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8ccb8fa1-e9b3-4bf8-a23e-ded70282a3c9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RangeBalance/rangeBalanceMultiRepeat/rangeBalanceMultiRepeat(TransStatus_2)</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
