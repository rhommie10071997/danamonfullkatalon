<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>rangeBalanceSingleFuture</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-01-21T11:27:55</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>0356bd82-b551-4680-a017-f6d86643eddc</testSuiteGuid>
   <testCaseLink>
      <guid>940b7683-513b-4b71-a04f-e230c4e7422d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/url</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>764d2b02-4400-4bd0-8e15-a2f9a0b6566b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RangeBalance/rangeBalanceSingleFuture/rangeBalanceSingleFuture(Maker)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ad3b78ae-1a90-4326-a0be-161620b25a85</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RangeBalance/rangeBalanceSingleFuture/rangeBalanceSingleFuture(Maker.Result)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>77248f07-8cfa-4ecb-9cd2-42466409a2d8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/transStatus/transStatus</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5b3cb1b9-8ee0-4fb8-abc5-5caffe65765a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RangeBalance/rangeBalanceSingleFuture/rangeBalanceSingleFuture(TransStatus)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>49a06251-49fb-4444-9d33-a8bbdfaa3adf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/RangeBalance/rangeBalanceSingleFuture/rangeBalanceSingleFuture(TransStatus_2)</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
