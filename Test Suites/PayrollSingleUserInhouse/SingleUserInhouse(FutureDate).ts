<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>SingleUserInhouse(FutureDate)</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-12-19T18:15:05</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>6058dea9-471b-4a49-b653-bb5255824992</testSuiteGuid>
   <testCaseLink>
      <guid>a94d5a79-2212-4e35-a71d-4ede290c6c92</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-Inhouse(FutureDate)/SingleUserPayroll-Inhouse(FutureDate)CreateEntryScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5bbb3490-1ee6-4af6-9afb-8185ae8caa5b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-Inhouse(FutureDate)/SingleUserPayroll-Inhouse(FutureDate)BucketListLoop</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>6d66ce8c-ef41-4ffe-8436-61756afa878b</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>a095de1d-0c11-4782-9bda-4eacdd54bf41</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>2d103d03-ffc7-4a96-b3a8-7efb7e3769fa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-Inhouse(FutureDate)/SingleUserPayroll-Inhouse(FutureDate)BucketDetails</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>51d16ee5-9332-4aa3-bd32-94c86a0614f3</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>be33e3f5-e5bc-49a2-842d-55ea6afd6468</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-Inhouse(FutureDate)/SingleUserPayroll-Inhouse(FutureDate)BucketConfirm</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>b9ab0622-3882-4371-a035-ee4d6ef4bf2b</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>ec267913-5336-43e2-a7cf-dc64818ba0f9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-Inhouse(FutureDate)/SingleUserPayroll-Inhouse(FutureDate)CreateResultScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6ac3e8a6-d58f-43e8-8046-06553465aeb1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-Inhouse(FutureDate)/SingleUserPayroll-Inhouse(FutureDate)TransactionStatusEntryScreenReffNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cf12ccf3-b52c-458f-ba71-413b34cee0ee</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-Inhouse(FutureDate)/SingleUserPayroll-Inhouse(FutureDate)TransactionStatusReffNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>08124e17-bf79-4ada-818f-fd5da37b238a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-Inhouse(FutureDate)/SingleUserPayroll-Inhouse(FutureDate)TransactionStatusEntryScreenDocNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1d6ae266-ee9f-4bd5-b423-a610b596ba55</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-Inhouse(FutureDate)/SingleUserPayroll-Inhouse(FutureDate)TransactionStatusDocNo</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
