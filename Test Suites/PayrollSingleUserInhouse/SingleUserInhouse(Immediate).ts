<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>SingleUserInhouse(Immediate)</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-12-19T17:52:53</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>64a15577-d224-4644-814f-938f498038f3</testSuiteGuid>
   <testCaseLink>
      <guid>66066bdc-5ba4-4b71-ba1a-8b262071bd91</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-Inhouse(Immediate)/SingleUserPayroll-Inhouse(Immediate)CreateEntryScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fcdfd206-f061-444b-bbc4-30b94909c609</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-Inhouse(Immediate)/SingleUserPayroll-Inhouse(Immediate)BucketListLoop</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>f45ff132-300d-45b1-88ca-043b077999eb</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>9119b518-1020-4508-b5ec-fca8dc0fd313</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>0a594478-100e-4515-bf16-d9045d63eb06</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-Inhouse(Immediate)/SingleUserPayroll-Inhouse(Immediate)BucketDetails</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>12adb67f-2aac-4423-a9f4-c4627dd2f502</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>64d8e753-ceba-4cee-b148-71ed64cfb025</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-Inhouse(Immediate)/SingleUserPayroll-Inhouse(Immediate)BucketConfirm</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>8f054cab-251e-4a52-81dc-ec74f256e4f0</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>5472f371-a085-43f3-b3e2-a97678c1236c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-Inhouse(Immediate)/SingleUserPayroll-Inhouse(Immediate)CreateResultScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2076219b-d093-4da5-a46e-84c6234b8560</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-Inhouse(Immediate)/SingleUserPayroll-Inhouse(Immediate)TransactionStatusEntryScreenReffNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8be2c919-7cb6-4b57-9b87-9be1ec2b811d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-Inhouse(Immediate)/SingleUserPayroll-Inhouse(Immediate)TransactionStatusReffNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0488c350-4e22-4bf3-8646-64f6b9de6747</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-Inhouse(Immediate)/SingleUserPayroll-Immediate(Immediate)TransactionStatusEntryScreenDocNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cee6e52f-ca70-46f1-9525-1c9a0e2b19b9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-Inhouse(Immediate)/SingleUserPayroll-Inhouse(Immediate)TransactionStatusDocNo</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
