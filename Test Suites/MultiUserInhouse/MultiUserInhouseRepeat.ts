<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>MultiUserInhouseRepeat</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>01083b15-7729-4acf-8260-1a20bfd0b9e4</testSuiteGuid>
   <testCaseLink>
      <guid>1be5ee61-bbec-4048-bab1-d9c000adc18c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Inhouse(Repeat)/MultiUser-Inhouse(Repeat)CreateEntryScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>02911f04-d559-4135-b0ba-81b40c03afe2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Inhouse(Repeat)/MultiUser-Inhouse(Repeat)CreateConfirmScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e1c4caa1-bd81-4b4c-953c-f36358f488f5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Inhouse(Repeat)/MultiUser-Inhouse(Repeat)CreateResultScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9b1d47d0-bdba-4d07-9d85-31bd768b3b1b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Inhouse(Repeat)/MultiUser-Inhouse(Repeat)ApproverEntryScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>57ea03d4-9c24-4d3f-a1d2-a9c9fa8a3e84</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Inhouse(Repeat)/MultiUser-Inhouse(Repeat)ApproverConfirmScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7c0c4f76-05e9-4134-87e9-7ff5f2e31ab3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Inhouse(Repeat)/MultiUser-Inhouse(Repeat)ApproverResultScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>53b0b47e-2401-4a5e-9f29-6c1f8f8c455c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Inhouse(Repeat)/MultiUser-Inhouse(Repeat)TransactionStatusEntryScreenReffNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bf9d6acf-9283-42e0-802f-6a7879662174</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Inhouse(Repeat)/MultiUser-Inhouse(Repeat)TransactionStatusScreenReffNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0aa52be6-4e57-409b-82e2-7953b5609037</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Inhouse(Repeat)/MultiUser-Inhouse(Repeat)TransactionStatusEntryScreenDocNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>94d80a39-318d-43d2-9edd-8bd5c3e8679e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Inhouse(Repeat)/MultiUser-Inhouse(Repeat)TransactionStatusScreenDocNo</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
