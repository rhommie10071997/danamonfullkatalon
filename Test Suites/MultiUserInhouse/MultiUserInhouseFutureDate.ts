<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>MultiUserInhouseFutureDate</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>18d47b2e-bd4e-4856-a63c-79c001a4dce8</testSuiteGuid>
   <testCaseLink>
      <guid>7b8e66ff-69ad-4037-ae93-0c86680470cb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Inhouse(FutureDate)/MultiUser-Inhouse(FutureDate)CreateEntryScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>44afd9b2-ae3f-47df-83bd-03cc3ecf3475</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Inhouse(FutureDate)/MultiUser-Inhouse(FutureDate)CreateConfirmScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6836db9f-0ccb-4001-bfa5-9537d52e3efa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Inhouse(FutureDate)/MultiUser-Inhouse(FutureDate)CreateResultScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7d986a4b-18d7-41eb-abd0-744b67ad3014</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Inhouse(FutureDate)/MultiUser-Inhouse(FutureDate)ApproverEntryScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>334997a6-f93e-47bf-9de1-381d9c22eab0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Inhouse(FutureDate)/MultiUser-Inhouse(FutureDate)ApproverConfirmScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ce464f69-00fd-4509-8671-a3ba7c4aa264</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Inhouse(FutureDate)/MultiUser-Inhouse(FutureDate)ApproverResultScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fdb958d7-01e6-4c08-b349-4825307238ff</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Inhouse(FutureDate)/MultiUser-Inhouse(FutureDate)TransactionStatusReffNoEntryScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d3fe8249-1551-40d1-baf8-b805d4b922bf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Inhouse(FutureDate)/MultiUser-Inhouse(FutureDate)TransactionStatusReffNoScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>151b0521-cb36-4e89-9ca5-2fdce9975cb2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Inhouse(FutureDate)/MultiUser-Inhouse(FutureDate)TransactionStatusDocNoEntryScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0ff197d3-6911-49ae-a6ff-829dee126985</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Inhouse(FutureDate)/MultiUser-Inhouse(FutureDate)TransactionStatusDocNoScreen</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
