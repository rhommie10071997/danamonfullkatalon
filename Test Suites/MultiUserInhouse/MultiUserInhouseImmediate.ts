<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>MultiUserInhouseImmediate</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>f72ffec0-8d80-4e03-8904-f5b3bcb50a12</testSuiteGuid>
   <testCaseLink>
      <guid>d12b0d14-7404-43e1-9536-eca715633f2c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Inhouse(Immediate)/MultiUser-Inhouse(Immediate)EntryScreen</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>08b004d5-0af6-40c3-9d54-3e2e431da4a1</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>5372977a-c1e8-4f2e-9043-c2353c150cf0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Inhouse(Immediate)/MultiUser-Inhouse(Immediate)ConfirmScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6504a302-4f85-41fe-93d7-c0688f04556b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Inhouse(Immediate)/MultiUser-Inhouse(Immediate)ResultScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a37f7bc3-0d7c-4ed6-bed1-4b7e808e8b4d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Inhouse(Immediate)/MultiUser-Inhouse(Immediate)ApproverConfirmScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1e4c8986-a08c-4beb-bab7-a27f3199c73b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Inhouse(Immediate)/MultiUser-Inhouse(Immediate)ApproverResultScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ca0f906d-f4bf-40e0-bd32-8bf558fa40a2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Inhouse(Immediate)/MultiUser-Inhouse(Immediate)TransactionStastusReffNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bb094e6a-ed58-413d-a861-83c408906f6f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Inhouse(Immediate)/MultiUser-Inhouse(Immediate)TransactionStatusDocNo</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
