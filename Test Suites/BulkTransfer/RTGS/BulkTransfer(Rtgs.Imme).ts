<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>BulkTransfer(Rtgs.Imme)</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-02-21T22:00:27</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>8b15bbd1-b0da-4ed3-aaaf-a250b95f184c</testSuiteGuid>
   <testCaseLink>
      <guid>11ee4afc-871f-4135-980b-cc9b4bd04be7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/url</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dd3c16b9-a231-428c-9416-37ca250bd1cf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BulkTransfers/RtgsSingleImme/bulkTransferRtgsSingleImme(Maker)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>161a6e82-1dd0-4283-90c4-52fe2609f73c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BulkTransfers/RtgsSingleImme/bulkTransferRtgsSingleImme(Maker.Result)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>aa4dce23-2381-4d5a-84ed-28b764a1fa6e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/transStatus/transStatus</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c13be239-3359-4def-b4cc-7145e57f520c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BulkTransfers/RtgsSingleImme/bulkTransferRtgsSingleImme(TransStatus)</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
