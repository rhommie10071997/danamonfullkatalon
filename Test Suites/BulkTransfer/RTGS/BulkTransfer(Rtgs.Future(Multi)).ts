<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>BulkTransfer(Rtgs.Future(Multi))</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-02-18T22:21:11</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>381bff4e-4689-48b8-b49c-032e9dadfffa</testSuiteGuid>
   <testCaseLink>
      <guid>0b064069-347a-48b5-8d41-345758dfd32e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/url</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d30226fd-c232-44f3-bc04-fadfeabc615a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BulkTransfers/RtgsMultiFuture/bulkTransferRtgsMultiFuture(Maker)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1b78fe43-7266-4944-b5a8-a867ec302ab0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BulkTransfers/RtgsMultiFuture/bulkTransferRtgsMultiFuture(Maker.Result)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>db58df93-b245-4b0f-bc91-1e90c62ff724</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/pendingTask/pendingTask</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8a118a79-ebfe-4630-892f-66e86e7b76b4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BulkTransfers/RtgsMultiFuture/bulkTransferRtgsMultiFuture(Approver)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7a3a1cbf-8142-44a0-a080-d0454d46a3a8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BulkTransfers/RtgsMultiFuture/bulkTransferRtgsMultiFuture(Approver.Result)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0cbf53b9-24c6-4e27-a9ea-3081f2dac386</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/transStatus/transStatus</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2bff1bff-667a-4f8a-970c-6585280c716c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BulkTransfers/RtgsMultiFuture/bulkTransferRtgsMultiFuture(transStatus)</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
