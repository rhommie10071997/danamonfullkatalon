<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>BulkTransfer(Rtgs.Imme(Multi))</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-02-21T21:20:30</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>cb0afdc1-ab7f-40a2-a99c-edfe9f67b6db</testSuiteGuid>
   <testCaseLink>
      <guid>2d925e97-3595-47c3-aee6-c9d05486d5ad</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/url</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>424f567a-453b-4f29-9664-dfc7a4245a27</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BulkTransfers/RtgsMultiImme/bulkTransferRtgsMultiImme(Maker)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e98ad78a-bf46-4dbb-963c-4789e602c535</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BulkTransfers/RtgsMultiImme/bulkTransferRtgsMultiImme(Maker.Result)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>548c9384-d403-46c4-94c8-a3f08b1859ae</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/pendingTask/pendingTask</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1c5154ef-5185-451d-bdd8-c907c4898019</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BulkTransfers/RtgsMultiImme/bulkTransferRtgsMultiImme(Approver)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3146e5cc-2b95-4d52-8f05-9d5fd0cf103c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BulkTransfers/RtgsMultiImme/bulkTransferRtgsMultiImme(Approver.Result)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ea8a8cb5-c37c-4a64-9f31-739dc24841d2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/transStatus/transStatus</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ca6513ca-4e1e-447f-9500-4b30b90c45b7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BulkTransfers/RtgsMultiImme/bulkTransferRtgsMultiImme(TransStatus)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8ab07e42-da22-4d5f-a8ad-575b703a3190</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BulkTransfers/RtgsMultiImme/bulkTransferRtgsMultiImme(TransInquiry)</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
