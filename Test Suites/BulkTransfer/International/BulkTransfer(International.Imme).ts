<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>BulkTransfer(International.Imme)</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-02-21T23:20:34</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>25a68d96-0af1-49c0-8801-5179357e3ffd</testSuiteGuid>
   <testCaseLink>
      <guid>aa3786a1-eeae-46d0-a61f-5bf9f16f7010</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/url</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>216da65a-c5e6-4d47-89f6-2bebb27c8b50</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BulkTransfers/InternationalSingleImme/bulkTransferInternationalSingleImme(Maker)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0362f8b7-6de8-44a9-bb08-d3377f5a2b86</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BulkTransfers/InternationalSingleImme/bulkTransferInternationalSingleImme(Maker.Result)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b03daf57-1f45-4bd9-b126-86ea49a6ac16</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/transStatus/transStatus</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>814473e3-643f-4cf2-b9ef-b75c892148d4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BulkTransfers/InternationalSingleImme/bulkTransferInternationalSingleImme(TransStatus)</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
