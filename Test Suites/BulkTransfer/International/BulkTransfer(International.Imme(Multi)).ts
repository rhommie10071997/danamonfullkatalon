<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>BulkTransfer(International.Imme(Multi))</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-02-21T22:40:30</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>d35869fe-7d41-4cc2-adc0-7f92a75b6192</testSuiteGuid>
   <testCaseLink>
      <guid>a1ffebe5-3457-4a5f-b0e5-7b8d1ac2d9b4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/url</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>842f3a2c-141d-4e61-b9bc-1249fb30efbd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BulkTransfers/InternationalMultiImme/bulkTransferInternationalMultiImme(Maker)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c0eacdc8-723c-435a-b8d6-7eefcfffd92f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BulkTransfers/InternationalMultiImme/bulkTransferInternationalMultiImme(Maker.Result)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b7346dc5-5a8a-4b11-8531-bbec00bb8569</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/pendingTask/pendingTask</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4f794d64-230d-4bfb-b6f4-bd609bff5160</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BulkTransfers/InternationalMultiImme/bulkTransferInternationalMultiImme(Approver)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4fba7bc9-0c5a-424d-92c9-c1b6f85ca075</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BulkTransfers/InternationalMultiImme/bulkTransferInternationalMultiImme(Approver.Result)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4c4a71a6-a29d-4674-932e-0f790a4464bc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/transStatus/transStatus</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>be8ea302-f45d-4442-b032-0eff823b2251</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BulkTransfers/InternationalMultiImme/bulkTransferInternationalMultiImme(transStatus)</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
