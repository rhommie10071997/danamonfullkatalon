<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>BulkTransfer(International.Future(Multi))</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-02-21T23:00:40</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>558deecc-0ef3-4802-9437-ca1a8c3f324a</testSuiteGuid>
   <testCaseLink>
      <guid>b78ee69c-6a41-4572-b199-557ad8746217</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/url</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>12a69f36-b6b3-4bb9-95f8-441fb2a7da8d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BulkTransfers/InternationalMultiFuture/bulkTransferInternationalMultiFuture(Maker)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2424c22f-f0ef-42f0-9ec5-b91121591a6c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BulkTransfers/InternationalMultiFuture/bulkTransferInternationalMultiFuture(Maker.Result)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bd2f4189-aaa0-46ef-852f-740c2f6d14f3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/pendingTask/pendingTask</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b057d6b2-d1cc-449f-b8ec-c7d7091089e1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BulkTransfers/InternationalMultiFuture/bulkTransferInternationalMultiFuture(Approver)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1066d3a0-d351-4f3b-8420-b8d6977ef43f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BulkTransfers/InternationalMultiFuture/bulkTransferInternationalMultiFuture(Approver.Result)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b9736d26-3c28-4d6a-adcb-b7359ac6fa15</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/transStatus/transStatus</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>84338381-c334-447f-8e32-7d90737e7652</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BulkTransfers/InternationalMultiFuture/bulkTransferInternationalMultiFuture(transStatus)</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
