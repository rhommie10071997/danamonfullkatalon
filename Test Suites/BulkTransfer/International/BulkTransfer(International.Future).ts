<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>BulkTransfer(International.Future)</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-01-10T18:57:25</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>6dac8331-0312-4c41-8600-413d0328de1c</testSuiteGuid>
   <testCaseLink>
      <guid>5e55f4e2-f289-4045-8a92-cab2e58a6455</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/url</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a4583370-9478-4aaf-a7bb-ba80f4994103</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BulkTransfers/InternationalSingleFuture/bulkTransferInternationalSingleFuture(Maker)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2da111cd-d708-4fa8-b5a8-5f7c34fa3991</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BulkTransfers/InternationalSingleFuture/bulkTransferInternationalSingleFuture(Maker.Result)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>90c27a5f-3f21-4585-b5d2-2a310027ec7e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/transStatus/transStatus</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d2d71278-aeb5-4150-8919-d054372e9f2b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BulkTransfers/InternationalSingleFuture/bulkTransferInternationalSingleFuture(transStatus)</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
