<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>BulkTransfer(Inhouse.Future)</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-02-18T21:40:24</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>691dcefb-6eb8-4d0c-a142-79f3d9388fe5</testSuiteGuid>
   <testCaseLink>
      <guid>58f50b88-6e2f-4090-8bf1-7aa4fd53546a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/url</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f814f909-9deb-41f7-88e4-032f9c2acca5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BulkTransfers/InhouseSingleFuture/bulkTransferInhouseSingleFuture(Maker)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>99a56b1a-74a9-49b9-965b-965c2602067b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BulkTransfers/InhouseSingleFuture/bulkTransferInhouseSingleFuture(Maker.Result)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3dca9e95-d2cd-4923-8126-6f63fe4be91a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/transStatus/transStatus</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7ed6bacd-e0bb-407b-b6b7-15c9b6326a23</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BulkTransfers/InhouseSingleFuture/bulkTransferInhouseSingleFuture(TransStatus)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2a308fc6-f940-41de-b348-04226fd4ec26</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BulkTransfers/InhouseSingleFuture/bulkTransferInhouseSingleFuture(TransStatus.2)</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
