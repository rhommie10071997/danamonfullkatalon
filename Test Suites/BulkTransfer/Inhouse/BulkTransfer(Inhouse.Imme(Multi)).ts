<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>BulkTransfer(Inhouse.Imme(Multi))</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-03-13T16:38:38</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>93d8454c-43e9-445d-b60b-095b29ca282f</testSuiteGuid>
   <testCaseLink>
      <guid>443b7349-430d-41a4-999d-a62963c13b74</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/url</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b592d646-1456-400d-8d02-0bf9e55e808c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BulkTransfers/InhouseMultiImme/bulkTransferInhouseMultiImme(Maker)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>689ed9b5-6d3e-4c7c-b5fa-91b60d829656</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BulkTransfers/InhouseMultiImme/bulkTransferInhouseMultiImme(Maker.Result)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>905f90c3-4e1f-4043-b813-4816a97365b5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/pendingTask/pendingTask</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7575c29f-c54f-48f8-8053-2f8e83903571</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BulkTransfers/InhouseMultiImme/bulkTransferInhouseMultiImme(Approver)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ae57715f-9470-46f2-84c1-28fbf1b5159d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BulkTransfers/InhouseMultiImme/bulkTransferInhouseMultiImme(Approver.Result)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>51f553ec-6b7e-4564-96fc-aae366686798</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/transStatus/transStatus</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>734d9a11-dfb3-462a-bc0c-add114680b4d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BulkTransfers/InhouseMultiImme/bulkTransferInhouseMultiImme(TransStatus)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4c889446-2de1-4a9b-8c05-bc3045565a9d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BulkTransfers/InhouseMultiImme/bulkTransferInhouseMultiImme(TransStatus.2)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>82ea9aab-158c-4a8f-a319-9520182260f7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TransactionInquiry/TransInquiry</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
