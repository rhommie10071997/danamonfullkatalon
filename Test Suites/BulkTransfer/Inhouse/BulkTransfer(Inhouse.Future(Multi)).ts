<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>BulkTransfer(Inhouse.Future(Multi))</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-02-21T22:20:25</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>478e9c0f-3c98-46d0-b78c-1ed67a4ea15c</testSuiteGuid>
   <testCaseLink>
      <guid>fb8efdc9-9371-4ea5-9654-eb236f0f6285</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/url</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>28af25ff-068c-45d6-a468-a8ad643fa565</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BulkTransfers/InhouseMultiFuture/bulkTransferInhouseMultiFuture(Maker)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>91818e08-91bf-4fb1-85cf-058eb9ee3502</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BulkTransfers/InhouseMultiFuture/bulkTransferInhouseMultiFuture(Maker.Result)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f00cb91a-4abd-46e3-b99c-4006f2bb1803</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/pendingTask/pendingTask</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>636b8eb7-b6b1-4150-b3bd-60f657e9820d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BulkTransfers/InhouseMultiFuture/bulkTransferInhouseMultiFuture(Approver)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cf8802d6-01d2-4fd5-ad9b-dd4a770a0bfa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BulkTransfers/InhouseMultiFuture/bulkTransferInhouseMultiFuture(Approver.Result)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4d8de8b4-0d7c-4d9b-badc-118de9b476c7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/transStatus/transStatus</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>86cc72a0-60c0-4d41-b3ca-6c4089d46c4d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BulkTransfers/InhouseMultiFuture/bulkTransferInhouseMultiFuture(TransStatus)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b14518ac-21ee-46d4-8a0a-bb42475dc82b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BulkTransfers/InhouseMultiFuture/bulkTransferInhouseMultiFuture(TransStatus.2)</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
