<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>BulkTransfer(Inhouse.Imme)</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-02-21T21:00:35</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>bf6a3668-fdba-47d8-b316-48be2de538d7</testSuiteGuid>
   <testCaseLink>
      <guid>6340d130-43cf-40ce-ab6b-48df887b44c1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/url</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fbd1c766-0c63-44b4-b76e-46a792cf34be</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BulkTransfers/InhouseSingleImme/bulkTransferInhouseSingleImme(Maker)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9726272c-2dd5-4f89-bac9-97867c5947e4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BulkTransfers/InhouseSingleImme/bulkTransferInhouseSingleImme(Maker.Result)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>904d54ca-5743-47a0-8e1c-14ed5603bbce</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/transStatus/transStatus</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f9340d5e-331d-492e-b9f6-7f595b55cc67</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BulkTransfers/InhouseSingleImme/bulkTransferInhouseSingleImme(TransStatus)</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
