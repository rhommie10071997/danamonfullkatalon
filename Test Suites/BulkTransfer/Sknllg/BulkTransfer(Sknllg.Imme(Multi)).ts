<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>BulkTransfer(Sknllg.Imme(Multi))</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-02-21T21:41:44</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>a1bd5666-b4d7-4e61-a4d6-edd67fed77ae</testSuiteGuid>
   <testCaseLink>
      <guid>dfbcf9b2-3d71-44f0-8b42-ed0b941b3739</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/url</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>de435df4-accb-4bcb-9423-de9dd062dd94</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BulkTransfers/Skn.rrgMultiImme/bulkTransferSkn.rrgMultiImme(Maker)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>297255cc-be2c-40d5-bb92-c158a1bffe14</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BulkTransfers/Skn.rrgMultiImme/bulkTransferSkn.rrgMultiImme(Maker.Result)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2a63f63c-b1ef-4e79-a1cb-8ba001f5c93e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/pendingTask/pendingTask</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bab23670-7e93-4527-9808-af07ffe31879</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BulkTransfers/Skn.rrgMultiImme/bulkTransferSkn.rrgMultiImme(Approver)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1cb23547-400d-4441-b132-2adf31f56446</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BulkTransfers/Skn.rrgMultiImme/bulkTransferSkn.rrgMultiImme(Approver.Result)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>eaf16006-ac00-415a-9cdc-575a352d32ad</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/transStatus/transStatus</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c9b30d19-d340-4e27-be19-8d34fa472ea7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BulkTransfers/Skn.rrgMultiImme/bulkTransferSkn.rrgMultiImme(transStatus)</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
