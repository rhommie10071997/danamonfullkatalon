<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>BulkTransfer(Sknllg.Future(Multi))</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-02-21T23:40:26</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>1ef687da-42db-4c3a-9c00-13d52f398f52</testSuiteGuid>
   <testCaseLink>
      <guid>2f3bcde1-eac2-40db-9c0b-adb045461677</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/url</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e078d932-bf60-4206-a243-82dfac569aec</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BulkTransfers/Skn.llgMultiFuture/bulkTransferSkn.llgMultiFuture(Maker)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>38a37392-d667-4f3a-b9ad-38df3884340d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BulkTransfers/Skn.llgMultiFuture/bulkTransferSkn.llgMultiFuture(Maker.Result)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f042a5e9-fb95-4544-b57b-fc067acc7648</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/pendingTask/pendingTask</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d57df4dc-2734-47cd-9215-7cfb06ed7c98</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BulkTransfers/Skn.llgMultiFuture/bulkTransferSkn.llgMultiFuture(Approver)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b3a5a2e2-b647-450e-b286-4c7a5fcc4925</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BulkTransfers/Skn.llgMultiFuture/bulkTransferSkn.llgMultiFuture(Approver.Result)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e785e1b8-0537-4a36-b0c5-9726bc55407a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/transStatus/transStatus</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>13bb1a79-4564-4233-8a84-11b18a0fc359</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BulkTransfers/Skn.llgMultiFuture/bulkTransferSkn.rrgMultiIFuture(transStatus)</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
