<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>SingleUserFutureDateDomesticSKN</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-02-08T17:51:51</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>0e64a891-7948-4aee-a71b-84870e89172b</testSuiteGuid>
   <testCaseLink>
      <guid>6f9b7b78-5df5-43d2-b63f-4cbaff8e2a3b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Domestic(FutureDate)SKN/SingleUser-Domestic(FutureDate)SKNEntryScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>afb31156-45c0-4cfd-b397-9bd14630a20f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Domestic(FutureDate)SKN/SingleUser-Domestic(FutureDate)SKNEntryConfirmScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c775182a-d0f0-42da-86d5-0acace46d9d5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Domestic(FutureDate)SKN/SingleUser-Domestic(FutureDate)SKNEntryResultScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b3cd8c1d-5a98-4040-922b-647057da3f4a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Domestic(FutureDate)SKN/SingleUser-Domestic(FutureDate)SKNTransactionStatusEntryReffNoScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>07e6f989-b104-45ad-87a7-3860b203cecb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Domestic(FutureDate)SKN/SingleUser-Domestic(FutureDate)SKNTransactionStatusReffNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7038630f-f662-46e1-bbaa-9ba396ca86c1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Domestic(FutureDate)SKN/SingleUser-Domestic(FutureDate)SKNTransactionStatusEntryDocNoScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c6eb695a-183c-4894-8cea-f99d9ecbf6e8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Domestic(FutureDate)SKN/SingleUser-Domestic(FutureDate)SKNTransactionStatusDocNo</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
