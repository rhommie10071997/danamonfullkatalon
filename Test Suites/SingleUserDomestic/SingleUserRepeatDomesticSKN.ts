<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>SingleUserRepeatDomesticSKN</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-02-11T11:50:03</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>caca1760-c35b-495b-960f-8f38ead88167</testSuiteGuid>
   <testCaseLink>
      <guid>4812f919-512e-4013-8c81-97357199038b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Domestic(Repeat)SKN/SingleUser-Domestic(Repeat)CreateEntryScreenSKN</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a42832dd-8d52-4db5-a6c9-5849dcd4cdee</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Domestic(Repeat)SKN/SingleUser-Domestic(Repeat)CreateConfirmScreenSKN</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>687664eb-3067-4e98-8df0-44af00503cac</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Domestic(Repeat)SKN/SingleUser-Domestic(Repeat)CreateResultScreenSKN</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>db940b6f-3305-4eb5-bfae-42feeeb87c8b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Domestic(Repeat)SKN/SingleUser-Domestic(Repeat)TransactionStatusEntryScreenReffNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0b95e141-36e5-47f0-b303-348ad1dcb72b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Domestic(Repeat)SKN/SingleUser-Domestic(Repeat)TransactionStatusReffNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dc6ab72e-1f97-4784-98e6-0c1eeb68fd82</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Domestic(Repeat)SKN/SingleUser-Domestic(Repeat)TransactionStatusEntryScreenDocNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>07089ecb-382d-4a5e-98dc-442a36addad0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Domestic(Repeat)SKN/SingleUser-Domestic(Repeat)TransactionStatusDocNo</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
