<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>SingleUserImmediateDomesticSKN</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>14cee6df-1f36-4e53-9724-ab13a7f0828d</testSuiteGuid>
   <testCaseLink>
      <guid>b6e68c9c-2ad4-4645-8f56-536efa96d7bd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Domestic(Immediate)SKN/SingleUser-Domestic(Immediate)SKNEntry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>42012154-4c4d-4b42-acac-c77893a50e80</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Domestic(Immediate)SKN/SingleUser-Domestic(Immediate)SKNConfirmScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>508d785a-6ec6-4ca7-aad8-6794777ef77b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Domestic(Immediate)SKN/SingleUser-Domestic(Immediate)SKNWaitingForApprovalScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e9b4ad13-8361-4b49-a5f9-c9ba0f9ca8d0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Domestic(Immediate)SKN/SingleUser-Domestic(Immediate)SKNTransactionStatusReffNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0b5f96a7-c7a6-498b-bb12-8f35264a3534</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Domestic(Immediate)SKN/SingleUser-Domestic(Immediate)SKNTransactionStatusDocNo</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
