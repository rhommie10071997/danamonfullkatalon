<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>SingleUserFutureDateDomesticRTGS</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>148a841a-27dd-4e74-9b91-c427d399bfda</testSuiteGuid>
   <testCaseLink>
      <guid>37d407c2-3934-423d-aafe-f2ad9dd35e25</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Domestic(FutureDate)RTGS/SingleUser-Domestic(FutureDate)RTGSEntryScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0b13217a-a59a-4fea-bec2-1eb26d4044b4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Domestic(FutureDate)RTGS/SingleUser-Domestic(FutureDate)RTGSEntryConfirmScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4595b743-e452-4be3-925a-749b7643d567</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Domestic(FutureDate)RTGS/SingleUser-Domestic(FutureDate)RTGSEntryResultScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2ba8d955-a9f4-4a43-b95c-d8bd4d493e7a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Domestic(FutureDate)RTGS/SingleUser-Domestic(FutureDate)RTGSTransactionStatusEntryReffNoScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6053bdc2-866e-4e80-b247-e8efa0708a7d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Domestic(FutureDate)RTGS/SingleUser-Domestic(FutureDate)RTGSTransactionStatusReffNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>65f47b2e-566d-4eb0-b78b-1b368b3b681e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Domestic(FutureDate)RTGS/SingleUser-Domestic(FutureDate)RTGSTransactionStatusEntryDocNoScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>769f207f-e09c-4e72-8ebb-7722d03567c9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Domestic(FutureDate)RTGS/SingleUser-Domestic(FutureDate)RTGSTransactionStatusDocNo</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
