<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>SingleUserRepeatDomesticRTGS</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>24afbb56-488e-40a2-9475-1ad7aeae017f</testSuiteGuid>
   <testCaseLink>
      <guid>e9b1d39b-845a-4ba1-afcb-d12eebf42be1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Domestic(Repeat)RTGS/SingleUser-Domestic(Repeat)CreateEntryScreenRTGS</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>db932dff-61af-44bd-9997-bed7e6cbf3c9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Domestic(Repeat)RTGS/SingleUser-Domestic(Repeat)CreateConfirmScreenRTGS</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>48267d78-14da-4908-8236-5d5e1efbc111</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Domestic(Repeat)RTGS/SingleUser-Domestic(Repeat)CreateResultScreenRTGS</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>14b11815-b684-4ecd-a1eb-6ab6210ae2ec</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Domestic(Repeat)RTGS/SingleUser-Domestic(Repeat)TransactionStatusEntryScreenReffNoRTGS</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>843c849b-55b4-4f43-aa62-6df7b5766222</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Domestic(Repeat)RTGS/SingleUser-Domestic(Repeat)TransactionStatusReffNoRTGS</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fa134db0-16b6-4677-8d6c-edfd39c21c35</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Domestic(Repeat)RTGS/SingleUser-Domestic(Repeat)TransactionStatusEntryScreenDocNoRTGS</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>32aa77aa-2ea2-4b7f-8851-acdd0d39e53f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Domestic(Repeat)RTGS/SingleUser-Domestic(Repeat)TransactionStatusDocNoRTGS</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
