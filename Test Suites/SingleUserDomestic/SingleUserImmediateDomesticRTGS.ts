<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>SingleUserImmediateDomesticRTGS</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>7bf244ed-304c-4cb7-8bbb-69e62dadfd4c</testSuiteGuid>
   <testCaseLink>
      <guid>80377272-13b5-4013-938b-edb0b12c63ee</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Domestic(Immediate)RTGS/SingleUser-Domestic(Immediate)RTGSEntryScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>029548d4-4554-43fa-a203-3cc3a5b54ad0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Domestic(Immediate)RTGS/SingleUser-Domestic(Immediate)RTGSEntryConfirmScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bc0df0ea-228f-4825-87bc-f36f09a224b1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Domestic(Immediate)RTGS/SingleUser-Domestic(Immediate)RTGSEntryResultScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>38bb9095-855f-4dd2-ae3e-05ab6e97db2a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Domestic(Immediate)RTGS/SingleUser-Domestic(Immediate)RTGSTransactionStatusEntryReffNoScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>db71e800-a7e1-4f80-99dc-80403dbdc97f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Domestic(Immediate)RTGS/SingleUser-Domestic(Immediate)RTGSTransactionStatusReffNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fa23701a-cf36-4ee9-aeae-c38a8d899c6c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Domestic(Immediate)RTGS/SingleUser-Domestic(Immediate)RTGSTransactionStatusEntryDocNoScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9f3a7c18-4e5e-473c-9c0e-2570e1b8c651</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Domestic(Immediate)RTGS/SingleUser-Domestic(Immediate)RTGSTransactionStatusDocNo</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
