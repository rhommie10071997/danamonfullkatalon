<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Future</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-02-21T16:30:45</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>fa9d079c-04ee-48de-8b44-c854e7e43b45</testSuiteGuid>
   <testCaseLink>
      <guid>70ba61a2-5cc6-4b75-b174-be960c6eb3f2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cash Pooling/Multi/Future/Create/New Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4b149b13-8fe9-435d-bc0c-aa2e76c4f82f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cash Pooling/Multi/Future/Create/Confirm</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8c1e3f9f-caac-4acf-ac10-1185902a4001</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cash Pooling/Multi/Future/Create/Result</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a2e7f67b-6c31-49b5-8d22-0c80fa8c402f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cash Pooling/Multi/Future/Approve/Entry Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c2e3c390-381b-41f7-a14f-f30568dd95b3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cash Pooling/Multi/Future/Approve/Confirm Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>48ea8958-4a2c-4c91-8139-7a9a85f8b7fe</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cash Pooling/Multi/Future/Approve/Result Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c429437d-97fb-4ce1-adef-fda79055ad82</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cash Pooling/Multi/Future/Transaction Status/Transaction Status - Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>480e577a-103f-44e8-a450-f5cbfd2b5032</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cash Pooling/Multi/Future/Transaction Status/TS - Ref No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3544bcfa-825f-449c-9cbf-bda11b747054</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cash Pooling/Multi/Future/Transaction Status/TS - Doc No</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
