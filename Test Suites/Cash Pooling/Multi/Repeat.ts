<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Repeat</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-12-04T19:24:49</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>e8c24fa8-de12-4844-80ca-08a8b155cfd3</testSuiteGuid>
   <testCaseLink>
      <guid>0a4a0ffb-9ef0-43a4-8722-ae53f83265c0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cash Pooling/Multi/Repeat/Create/New Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e0e55247-0735-4509-b3b0-f85ee1c45768</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cash Pooling/Multi/Repeat/Create/Confirm</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1911a30d-6fb0-4324-94cd-4bff10649da7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cash Pooling/Multi/Repeat/Create/Result</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>47f88c5c-4e90-4125-8f12-0a3a5a37b535</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cash Pooling/Multi/Repeat/Approve/Entry Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d1c14a8c-817f-4fd9-9852-86eaf4efe071</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cash Pooling/Multi/Repeat/Approve/Confirm Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1f2e1347-2170-47ce-8a0c-ebd7090d71d3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cash Pooling/Multi/Repeat/Approve/Result Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ead3cf12-69e4-46d4-880d-cc8143144cd6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cash Pooling/Multi/Repeat/Transaction Status/Transaction Status - Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>96a7239f-ba12-4091-82eb-7b1af14d6475</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cash Pooling/Multi/Repeat/Transaction Status/TS - Ref No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6f16fe3d-b07a-47b8-9dec-66f857ed3275</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cash Pooling/Multi/Repeat/Transaction Status/TS - Doc No</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
