<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Immediate</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-02-21T16:13:35</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>aa5dc0ef-063e-479c-98f4-b1e391606ef6</testSuiteGuid>
   <testCaseLink>
      <guid>b1281c59-d007-4f0f-bfc9-cf9e091040c7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cash Pooling/Multi/Immediate/Create/New Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a27eb4db-6769-4762-afba-823a2a8e9689</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cash Pooling/Multi/Immediate/Create/Confirm</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0931be0f-6237-49dc-a337-23e225fe9838</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cash Pooling/Multi/Immediate/Create/Result</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>04ed228c-84fc-42dd-bdac-a2879398c123</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cash Pooling/Multi/Immediate/Approve/Entry Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f1d61fb6-34bf-48cd-9da2-2e94adef1813</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cash Pooling/Multi/Immediate/Approve/Confirm Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0f2d8d31-e3a6-4568-94cd-a18f060c9f84</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cash Pooling/Multi/Immediate/Approve/Result Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>37406bec-f543-4377-80a6-92789fb46b8c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cash Pooling/Multi/Immediate/Transaction Status/Transaction Status - Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cfe6c097-46c1-42c7-a142-60837bf8f3b0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cash Pooling/Multi/Immediate/Transaction Status/TS - Ref No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b6580592-a6ca-4a31-93df-557ec7945e69</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cash Pooling/Multi/Immediate/Transaction Status/TS - Doc No</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
