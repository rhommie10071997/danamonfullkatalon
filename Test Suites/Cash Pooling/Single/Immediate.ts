<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Immediate</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-12-04T16:11:34</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>b67520a0-f702-4020-825e-d8e32c6dd1a6</testSuiteGuid>
   <testCaseLink>
      <guid>495a271d-3c0f-4a7b-b0e1-e25732da8f5c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cash Pooling/Single/Immediate - Copy/Create/New Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9fc9660c-6a42-466b-88c0-695ef72637d8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cash Pooling/Single/Immediate - Copy/Create/Confirm</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3bc468d6-2ed9-44ea-91e8-2d5e38be9255</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cash Pooling/Single/Immediate - Copy/Create/Result</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e8ca8330-0aa9-420a-ba42-e14375eeb65a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cash Pooling/Single/Immediate - Copy/Transaction Status/Transaction Status - Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>03676bf0-302b-477c-aad3-526dad97de42</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cash Pooling/Single/Immediate - Copy/Transaction Status/TS - Ref No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9a5adc11-0d2c-4c65-9bbb-c34630391682</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cash Pooling/Single/Immediate - Copy/Transaction Status/TS - Doc No</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
