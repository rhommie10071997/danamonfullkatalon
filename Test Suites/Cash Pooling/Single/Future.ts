<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Future</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-12-04T16:53:47</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>80667c88-2049-448e-a74f-a058d56479d6</testSuiteGuid>
   <testCaseLink>
      <guid>8a23031f-3c5a-492f-8913-a9bacbe01b33</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cash Pooling/Single/Future/Create/New Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7c6b1a39-b11c-4199-9b63-a04903983e7e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cash Pooling/Single/Future/Create/Confirm</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>665a5edd-0ac8-4d09-b269-80d1a7bfa7ff</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cash Pooling/Single/Future/Create/Result</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b9b2ec06-1d45-4b63-a68f-1f8766234b9f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cash Pooling/Single/Future/Transaction Status/Transaction Status - Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8c8aab5a-f5c5-489c-85a7-c63fd99c2041</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cash Pooling/Single/Future/Transaction Status/TS - Ref No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6945d921-ad4e-4ea1-b472-3a3f26376e8a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cash Pooling/Single/Future/Transaction Status/TS - Doc No</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
