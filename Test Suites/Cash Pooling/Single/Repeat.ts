<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Repeat</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-12-06T15:04:32</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>1c5f498e-10cb-4b93-9746-78fc69e3d269</testSuiteGuid>
   <testCaseLink>
      <guid>3321cf88-ff11-4d83-be1f-51cfcf38e1ac</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cash Pooling/Single/Repeat/Create/New Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>82d7940c-5b40-44e3-b069-3690fe284462</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cash Pooling/Single/Repeat/Create/Confirm</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c36d6849-8961-4af1-bedf-d3246244620d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cash Pooling/Single/Repeat/Create/Result</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6bd219c8-6592-41b1-a911-d9af54695a2c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cash Pooling/Single/Repeat/Transaction Status/Transaction Status - Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>49a7caac-ed51-4085-bbe7-bdf71f47539e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cash Pooling/Single/Repeat/Transaction Status/TS - Ref No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>90e79730-8860-487b-95d8-d56669a326c0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cash Pooling/Single/Repeat/Transaction Status/TS - Doc No</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
