<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Balance Transfer Domestic Skn - Immediate</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-02-20T17:36:13</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>5ef8286e-a990-4301-aa63-9c3228b72144</testSuiteGuid>
   <testCaseLink>
      <guid>1656562f-b340-4b05-ba06-db33ac2a9246</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer Domestic SKN/Immediate/Create/Entry Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>80436296-a769-4aa6-9f6a-154c53e47fe1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer Domestic SKN/Immediate/Create/Confirm Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e514b20d-ae5d-413c-b994-5ee54a9c4f0f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer Domestic SKN/Immediate/Create/Result Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ecc23555-28e2-431b-b3c6-2249b82b5e1b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer Domestic SKN/Immediate/Approve/Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6596d0dc-c7aa-4597-8430-e0f68ee96f96</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer Domestic SKN/Immediate/Approve/Confirm</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>16411fda-15e3-4ec7-9ae4-0c4c272afd88</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer Domestic SKN/Immediate/Approve/Result</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d566db4d-f513-470a-919b-b9feeb41ff4f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer Domestic SKN/Immediate/Transaction Status/Transaction Status - Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>80c755a3-9659-4c1f-8bf6-15e6408642cb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer Domestic SKN/Immediate/Transaction Status/Transaction Status - Ref No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b0f5fd5d-5e00-45f8-8aad-de449956abcc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer Domestic SKN/Immediate/Transaction Status/Transaction Status - Doc No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>57f0bf41-d1aa-47c4-b2a1-d16fd8c57264</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer Domestic SKN/Immediate/Inquiry/Transaction Inquiry - Debit</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
