<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>International - Immediate</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-02-13T13:58:00</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>1ab2863e-0b98-410c-b534-ec2a76d880e7</testSuiteGuid>
   <testCaseLink>
      <guid>bbcf25bd-2443-40f2-866c-760d6e877530</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer International/Immediate/Create/Entry Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>26ffa3c1-3227-45fc-ac44-eb59ac7f8742</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer International/Immediate/Create/Confirm Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b9e9feff-67b0-4cc2-a31a-ed7ab0a069a8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer International/Immediate/Create/Result Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>11c4a558-2e76-4878-9691-2040be6676a0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer International/Immediate/Approve/Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>99ee6087-f8e9-48fe-b30c-39067d394035</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer International/Immediate/Approve/Confirm</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>35819f8b-2492-4bb5-80a9-508aaabdf9de</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer International/Immediate/Approve/Result</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3276eb56-0051-46d5-8893-2d0be2b28ba1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer International/Immediate/Transaction Status/Transaction Status - Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3d50978d-79de-46fe-a0a2-e96beaedcd8c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer International/Immediate/Transaction Status/Transaction Status - Ref No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b883f424-2500-46df-b4e7-a6a3a1e3c82b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer International/Immediate/Transaction Status/Transaction Status - Doc No</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
