<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>International - Future</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-02-14T16:47:37</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>3eadac23-3b2d-4c24-8d03-6fb7acd61957</testSuiteGuid>
   <testCaseLink>
      <guid>a5f1a0bb-3808-4034-ad2b-c84cb6a97a41</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer International/Future/Create/Entry Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>67e414b3-88c0-44eb-8ec2-a770f5f0927a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer International/Future/Create/Confirm Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8ab6f4db-8997-44b4-885a-5661725a080f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer International/Future/Create/Result Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c2c48bde-0b66-4e85-8563-a2e92ab9f847</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer International/Future/Approve/Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>74f409c4-80f8-4981-a301-79672121d88b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer International/Future/Approve/Confirm</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cf8b9432-a7ef-4099-90bd-26349b449a1b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer International/Future/Approve/Result</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>89a10911-3f63-473b-a7b8-2aa7eb24cd86</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer International/Future/Transaction Status/Transaction Status - Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>807f310c-90b0-4f01-9184-eb643559cd99</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer International/Future/Transaction Status/Transaction Status - Ref No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d98abcf3-4075-4f69-a719-42a2ce59948a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer International/Future/Transaction Status/Transaction Status - Doc No</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
