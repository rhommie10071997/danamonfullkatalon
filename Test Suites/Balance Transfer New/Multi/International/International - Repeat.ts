<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>International - Repeat</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-02-20T16:02:38</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>5b3896b8-161b-4a16-86c3-0cf40de90356</testSuiteGuid>
   <testCaseLink>
      <guid>233a0f34-02e7-4437-9863-fcbbcad785bf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer International/Repeat/Create/Entry Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>adeee55c-77c5-40a2-a160-aee946ec347c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer International/Repeat/Create/Confirm Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d32afe00-da05-4c8b-9632-2d042935b3d7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer International/Repeat/Create/Result Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3598c10c-45e0-42c8-8a82-003aa124d139</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer International/Repeat/Approve/Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>395d72c3-5596-4547-b505-3ac727d23afa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer International/Repeat/Approve/Confirm</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>24397fef-3a06-487e-991c-821a91c2d8f6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer International/Repeat/Approve/Result</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4a206edc-4ca3-4bca-9318-0cfb4d8d3e5d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer International/Repeat/Transaction Status/Transaction Status - Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4cdabde4-6502-4014-bd30-3b4ebbcb7078</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer International/Repeat/Transaction Status/Transaction Status - Ref No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d5f7ba60-5e4a-41d8-afd3-b28fde559308</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer International/Repeat/Transaction Status/Transaction Status - Doc No</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
