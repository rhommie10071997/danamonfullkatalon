<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Balance Transfer Domestic Rtgs - Immediate</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-02-20T16:39:58</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>9f01cf2e-21a5-47ac-a970-a4169bfd7b92</testSuiteGuid>
   <testCaseLink>
      <guid>f0ac9cfc-61ef-48aa-a537-2b0e5b6306db</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer Domestic RTGS/Immediate/Create/Entry Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9437ab06-16df-48d2-a464-b8c40600a26b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer Domestic RTGS/Immediate/Create/Confirm Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f493b013-8f2c-4f97-b090-27b861f9c366</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer Domestic RTGS/Immediate/Create/Result Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>26367450-5049-451e-a788-5dc19203dac2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer Domestic RTGS/Immediate/Approve/Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d88f5318-c7b7-4a78-8353-c97f2c50fc59</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer Domestic RTGS/Immediate/Approve/Confirm</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>945fa82f-39a5-42ad-84c4-525b786e519c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer Domestic RTGS/Immediate/Approve/Result</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0eec5402-62e8-4599-8b8b-7557aefaf05c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer Domestic RTGS/Immediate/Transaction Status/Transaction Status - Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4981d4b8-230d-4ad4-be0c-313d49a6086b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer Domestic RTGS/Immediate/Transaction Status/Transaction Status - Ref No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9607a6ba-c531-43d3-a385-06abca5d0f6e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer Domestic RTGS/Immediate/Transaction Status/Transaction Status - Doc No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fd3e5c8a-22f8-4ce6-81ea-aa7976b7c6d7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer Domestic RTGS/Immediate/Inquiry/Transaction Inquiry - Debit</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
