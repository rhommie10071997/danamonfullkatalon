<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Balance Transfer Domestic Rtgs - Repeat</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-02-20T16:50:36</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>8bc30f3c-bf91-4115-bbee-6815c626f767</testSuiteGuid>
   <testCaseLink>
      <guid>cc6e6b67-1c63-4f36-be1f-d075e277d7f1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer Domestic RTGS/Repeat/Create/Entry Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ad886b2b-4599-415d-bc0d-b39328cc9d44</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer Domestic RTGS/Repeat/Create/Confirm Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>44f0dc65-c8f7-4af3-887e-3b47944239bb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer Domestic RTGS/Repeat/Create/Result Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c41aa4c7-9328-41a1-9563-2e0d10db30a8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer Domestic RTGS/Repeat/Approve/Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ecd806b2-aec8-46bb-9567-d1e8c120a715</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer Domestic RTGS/Repeat/Approve/Confirm</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e2ab4330-b04f-4ea5-8870-0c12c79e4704</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer Domestic RTGS/Repeat/Approve/Result</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5bd1f4b6-a371-4e43-8e26-b11a96663b10</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer Domestic RTGS/Repeat/Transaction Status/Transaction Status - Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a4a14b3e-2408-43f4-8f66-4a77f71c80db</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer Domestic RTGS/Repeat/Transaction Status/Transaction Status - Ref No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e2d9e6e7-f514-40a8-95c7-3232e5838185</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer Domestic RTGS/Repeat/Transaction Status/Transaction Status - Doc No</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
