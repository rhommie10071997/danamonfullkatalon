<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Balance Transfer Domestic Rtgs - Future</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-02-20T16:26:38</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>0e498624-faad-4524-b3fb-a7904c5a26cf</testSuiteGuid>
   <testCaseLink>
      <guid>b30eaf0d-951e-4bb9-93f2-93c9a38ddd0c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer Domestic RTGS/Future/Create/Entry Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ea6422f1-c48d-4b39-8ff6-8b8fbbcbb14d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer Domestic RTGS/Future/Create/Confirm Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>47543406-e122-4e8a-963e-fcb2bdb6b36f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer Domestic RTGS/Future/Create/Result Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>03d6f568-ee20-4d27-a8c2-79edec2a0928</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer Domestic RTGS/Future/Approve/Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>35f92206-5439-406f-8650-945d29a00729</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer Domestic RTGS/Future/Approve/Confirm</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>550d8729-6bf6-4852-851c-25f7243e2f44</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer Domestic RTGS/Future/Approve/Result</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6b08198e-40fa-4021-b663-f8d8530a884a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer Domestic RTGS/Future/Transaction Status/Transaction Status - Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>27c40e00-c703-4af9-8ecc-940550eb6137</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer Domestic RTGS/Future/Transaction Status/Transaction Status - Ref No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a97cede5-6c0e-43cf-9f9a-46bbbf33ab14</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer Domestic RTGS/Future/Transaction Status/Transaction Status - Doc No</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
