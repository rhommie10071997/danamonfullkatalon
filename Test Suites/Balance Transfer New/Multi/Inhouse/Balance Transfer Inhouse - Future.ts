<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Balance Transfer Inhouse - Future</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-02-18T17:44:03</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>67b33965-0010-444a-bf26-c630e0af642c</testSuiteGuid>
   <testCaseLink>
      <guid>42694aa1-d91b-46cd-bf8d-bba9f6d50be5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer Inhouse/Future/Create/Entry Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4e993b24-26e9-45e3-8aac-f932d39dba83</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer Inhouse/Future/Create/Detail Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>94b5f44c-f022-4c4a-a9de-efce94bab229</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer Inhouse/Future/Create/Result Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>673fe2eb-e5d0-484f-ade8-abb8db60b78a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer Inhouse/Future/Approve/Entry Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>522b7e34-eb1c-4e24-9a22-1b8ccc348f53</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer Inhouse/Future/Approve/Detail Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>253ee3bb-fbda-406f-a4e6-df9bbe513452</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer Inhouse/Future/Approve/Result Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>865e3799-fe1f-4add-b3d9-c76c6c27e90b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer Inhouse/Future/Transaction Status/Transaction Status - Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5c6b2126-012a-4f91-b756-929658900439</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer Inhouse/Future/Transaction Status/Transaction Status - Ref No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ddd75477-6082-4952-807f-298968dd3a0e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer Inhouse/Future/Transaction Status/Transaction Status - Doc No</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
