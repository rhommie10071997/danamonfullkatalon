<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Balance Transfer Inhouse - Repeat</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-02-13T13:57:59</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>0d3e4806-56a2-441a-a733-54c6024e49be</testSuiteGuid>
   <testCaseLink>
      <guid>c1d58ff9-ed44-45b9-a427-6433137e7161</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer Inhouse/Repeat/Create/Entry Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1c625976-89db-4ef4-82a0-5dffc3304e0d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer Inhouse/Repeat/Create/Detail Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f8921020-87f1-4013-89d9-f8ec15a3d407</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer Inhouse/Repeat/Create/Result Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5a118668-4514-4660-aace-80d99a8a28b6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer Inhouse/Repeat/Approve/Entry Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0621b7c7-166b-4bfc-8fbf-cb8f2d01f407</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer Inhouse/Repeat/Approve/Detail Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8f1307be-02be-4dd7-99be-1e2643010d5c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer Inhouse/Repeat/Approve/Result Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f1a1bef2-13fc-4d6a-9189-1676200dd3df</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer Inhouse/Repeat/Transaction Status/Transaction Status - Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a195d708-030d-4c96-8381-92af94abf077</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer Inhouse/Repeat/Transaction Status/Transaction Status - Ref No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1bba9b1c-6f13-402c-8d87-8d224e732bd4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer Inhouse/Repeat/Transaction Status/Transaction Status - Doc No</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
