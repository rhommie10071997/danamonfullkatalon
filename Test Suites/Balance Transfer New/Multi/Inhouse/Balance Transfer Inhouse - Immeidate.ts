<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Balance Transfer Inhouse - Immeidate</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-02-13T13:57:59</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>4dcca51f-d889-48a0-945d-84d5be0dd2de</testSuiteGuid>
   <testCaseLink>
      <guid>2eb31568-e902-4f90-8874-7a3319ec852f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer Inhouse/Immediate/Create/Entry Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>89c12b5b-bc25-4608-a8ca-b5188764c66a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer Inhouse/Immediate/Create/Detail Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>95b7aa69-7ecd-4497-ac94-203eb6080759</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer Inhouse/Immediate/Create/Result Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e72e7809-90f9-4a2e-84fd-d56b828ce8f7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer Inhouse/Immediate/Approve/Entry Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>560b4b7e-c29f-4f76-910d-d6402627acdd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer Inhouse/Immediate/Approve/Detail Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>18e96147-019b-4295-88f9-e9f4b96cce9b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer Inhouse/Immediate/Approve/Result Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9e3730c5-abbc-4251-99bb-8ebb55e51570</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer Inhouse/Immediate/Transaction Status/Transaction Status - Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3677fb07-2190-4cb1-ba0f-c5b460167147</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer Inhouse/Immediate/Transaction Status/Transaction Status - Ref No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>26f68c29-2cca-4186-be8e-de575b151609</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Multi/Balance Transfer Inhouse/Immediate/Transaction Status/Transaction Status - Doc No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9aa64d90-7767-4690-9734-e861b39ac335</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Transaction Inquiry - Debit</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
