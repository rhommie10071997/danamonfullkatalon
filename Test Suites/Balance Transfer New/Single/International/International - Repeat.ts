<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>International - Repeat</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>738c5ba1-dc34-4c40-8012-a8d6946c5ee7</testSuiteGuid>
   <testCaseLink>
      <guid>09317a77-3615-4eb9-889d-30d939b6d381</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Single/Balance Transfer International/Repeat/Create/Entry Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c117c54c-5bcb-4e90-88fd-84ce38b4dc3e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Single/Balance Transfer International/Repeat/Create/Confirm Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>106c037b-c036-4cc3-8a62-018b7b828906</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Single/Balance Transfer International/Repeat/Create/Result Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6c9c70ae-11ae-4912-b2db-d3bd83a62ef8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Single/Balance Transfer International/Repeat/Transaction Status/Transaction Status - Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0d698d3c-aaab-4f71-8465-577d2a2249c3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Single/Balance Transfer International/Repeat/Transaction Status/Transaction Status - Ref No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>68e6f47f-ae42-4f25-811f-cd54e72f6614</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Single/Balance Transfer International/Repeat/Transaction Status/Transaction Status - Doc No</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
