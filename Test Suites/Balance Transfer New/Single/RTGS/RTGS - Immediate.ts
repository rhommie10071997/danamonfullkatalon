<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>RTGS - Immediate</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-01-30T14:11:25</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>ffbbc2f7-3c2d-4583-9b4c-c53f3c296215</testSuiteGuid>
   <testCaseLink>
      <guid>be541981-354d-47ea-9ee3-209e1029429e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Single/Balance Transfer Domestic RTGS/Immediate/Create/Entry Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9f7ae61b-b0c2-4a53-8f30-532085bb98d3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Single/Balance Transfer Domestic RTGS/Immediate/Create/Confirm Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4a82224f-0601-45b6-abc9-0c141894e785</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Single/Balance Transfer Domestic RTGS/Immediate/Create/Result Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c171b6fd-f057-47e2-8a38-4053e6303b1e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Single/Balance Transfer Domestic RTGS/Immediate/Transaction Status/Transaction Status - Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c1ccfa4b-24b7-4669-8296-e5fa3cdf782a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Single/Balance Transfer Domestic RTGS/Immediate/Transaction Status/Transaction Status - Ref No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d3a8cd01-c6c9-4609-b951-fc2b6eaf111c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Single/Balance Transfer Domestic RTGS/Immediate/Transaction Status/Transaction Status - Doc No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>57999526-e165-463b-ad99-cab84ebc206b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Single/Balance Transfer Domestic RTGS/Immediate/Inquiry/Transaction Inquiry - Debit</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
