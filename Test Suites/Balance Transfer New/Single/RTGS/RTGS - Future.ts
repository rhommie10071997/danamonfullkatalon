<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>RTGS - Future</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>b4111524-508e-42c6-b7ee-650631dd24ba</testSuiteGuid>
   <testCaseLink>
      <guid>57e9a935-5fe4-448c-be30-805fff7f1817</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Single/Balance Transfer Domestic RTGS/Future/Create/Entry Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d0abaf61-2342-4361-8b0b-127d6f0fff0e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Single/Balance Transfer Domestic RTGS/Future/Create/Confirm Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7040c6f8-2882-4fe0-a5af-7f6049460667</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Single/Balance Transfer Domestic RTGS/Future/Create/Result Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>70561f08-5ddd-45b6-ace7-6555f0e913e3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Single/Balance Transfer Domestic RTGS/Future/Transaction Status/Transaction Status - Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bb4bee13-054e-48c7-a089-e6c7fd4a0717</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Single/Balance Transfer Domestic RTGS/Future/Transaction Status/Transaction Status - Ref No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>898afd03-3451-400e-81e3-27a78aa70cb7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Single/Balance Transfer Domestic RTGS/Future/Transaction Status/Transaction Status - Doc No</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
