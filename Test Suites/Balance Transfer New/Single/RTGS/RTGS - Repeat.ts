<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>RTGS - Repeat</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>16b2a1ac-bfa6-4d6f-9ae9-1e4659a93091</testSuiteGuid>
   <testCaseLink>
      <guid>00aa7930-2ab9-4076-aa62-7c3308ed288f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Single/Balance Transfer Domestic RTGS/Repeat/Create/Entry Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>11042282-7a10-4722-981b-c12741496eb8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Single/Balance Transfer Domestic RTGS/Repeat/Create/Confirm Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6bc9c8b7-b3c9-476b-bce1-bc036c16554a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Single/Balance Transfer Domestic RTGS/Repeat/Create/Result Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c49b0a08-9635-449b-b580-5b65bc65d816</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Single/Balance Transfer Domestic RTGS/Repeat/Transaction Status/Transaction Status - Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>146e0c3c-276f-412d-8532-4bcc9bd3683a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Single/Balance Transfer Domestic RTGS/Repeat/Transaction Status/Transaction Status - Ref No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>87363398-6347-4585-aee2-1bd0724fce0f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Single/Balance Transfer Domestic RTGS/Repeat/Transaction Status/Transaction Status - Doc No</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
