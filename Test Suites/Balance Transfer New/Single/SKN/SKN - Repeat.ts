<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>SKN - Repeat</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-02-14T13:53:51</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>d2308127-3a36-4b87-b83a-a5bbb6e0c205</testSuiteGuid>
   <testCaseLink>
      <guid>82eb3184-9d54-4167-b31d-2c6873da2053</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Single/Balance Transfer Domestic SKN/Repeat/Create/Entry Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8bc2b631-af11-4fd8-a4b4-f9cd6dcedabc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Single/Balance Transfer Domestic SKN/Repeat/Create/Confirm Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>903ae3d8-becf-4dfd-aabe-0ec6fc8905bd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Single/Balance Transfer Domestic SKN/Repeat/Create/Result Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>642371d3-cfb0-44a5-a0cf-7e4c46b2234d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Single/Balance Transfer Domestic SKN/Repeat/Transaction Status/Transaction Status - Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>37331dee-5170-45c3-9793-61233c894bb4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Single/Balance Transfer Domestic SKN/Repeat/Transaction Status/Transaction Status - Ref No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>56e3ee77-3d99-4a97-9674-f26ea1b70abd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Single/Balance Transfer Domestic SKN/Repeat/Transaction Status/Transaction Status - Doc No</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
