<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>SKN - Immediate</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-02-18T13:49:05</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>9a3e2e59-4680-4fba-bc24-2e3a088d4b44</testSuiteGuid>
   <testCaseLink>
      <guid>d1096843-1dd5-4dc2-8776-445e1a23a23d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Single/Balance Transfer Domestic SKN/Immediate/Create/Entry Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bf0937e3-9cad-45fa-b0a0-a34cdfd4861c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Single/Balance Transfer Domestic SKN/Immediate/Create/Confirm Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>888b3f3d-dac6-4133-9634-1fd90d295d27</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Single/Balance Transfer Domestic SKN/Immediate/Create/Result Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>15f2409d-67e6-4b7c-83a5-eae7493e2f67</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Single/Balance Transfer Domestic SKN/Immediate/Transaction Status/Transaction Status - Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>92ee0c27-d764-4267-98ff-df64654f48ae</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Single/Balance Transfer Domestic SKN/Immediate/Transaction Status/Transaction Status - Ref No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5aebd34f-e093-415b-b7d8-da49412f7c0c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Single/Balance Transfer Domestic SKN/Immediate/Transaction Status/Transaction Status - Doc No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>10e2e632-a64e-46d5-8fec-2afa16898657</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Single/Balance Transfer Domestic SKN/Immediate/Inquiry/Transaction Inquiry - Debit</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
