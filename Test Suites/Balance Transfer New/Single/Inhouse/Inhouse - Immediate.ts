<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Inhouse - Immediate</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-02-20T18:07:14</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>1daf41b6-6313-4d68-bfad-835668c4efd2</testSuiteGuid>
   <testCaseLink>
      <guid>31f1e0b8-9b38-48d6-b7b6-efc3c0593cb7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Single/Balance Transfer Inhouse/Immediate/Create/Entry Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f767ee5a-8104-4d40-aecb-a79b3ec3c5ce</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Single/Balance Transfer Inhouse/Immediate/Create/Detail Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a902a1f2-b7f0-4ff7-a558-d4971dcd055b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Single/Balance Transfer Inhouse/Immediate/Create/Result Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d9ae98b8-8dcc-4e87-82cc-d856eb562837</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Single/Balance Transfer Inhouse/Immediate/Transaction Status/Transaction Status - Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d583c7dd-f37f-4c61-9954-0b4b97608b12</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Single/Balance Transfer Inhouse/Immediate/Transaction Status/Transaction Status - Ref No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dd01f1f4-498e-4e05-8540-663f7d80b3ac</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Single/Balance Transfer Inhouse/Immediate/Transaction Status/Transaction Status - Doc No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ed7ffa4d-746f-4e7b-adcf-100f83173761</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Balance Transfer - New/Single/Balance Transfer Inhouse/Immediate/Transaction Inquiry/Transaction Inquiry - Debit</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
