<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>autoDebitMultiImme</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-01-24T17:58:44</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>892a4b12-cf6d-4abd-8f3b-3c73e8b14beb</testSuiteGuid>
   <testCaseLink>
      <guid>3e4fabb8-029f-49e3-8c7e-08d586f0309e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/url</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>44822c83-df38-4be5-9a6a-c29e6ef4b9ac</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/autoDebit/autoDebitMultiImme/autoDebitMultiImme(Maker)</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>c9d71820-e464-4b95-b832-f93d80a95166</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>f1847044-4695-4c9b-8fdf-2989031aeb1c</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>48070fdd-fc95-48ff-b2c5-1bcfde42dcab</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/autoDebit/autoDebitMultiImme/autoDebitMultiImme(Maker.Result)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1aefe8da-1f01-469f-ae9b-a525a536bde3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/pendingTask/pendingTask</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>09a2b8dd-c838-4cdc-a3b7-ea114ed36dac</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/autoDebit/autoDebitMultiImme/autoDebitMultiImme(Approver)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6e736086-09fa-4d87-bfaf-8383e3c65a7e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/autoDebit/autoDebitMultiImme/autoDebitMultiImme(Approver.Result)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>889a8d9c-c98a-436d-be57-d97525d3f39f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/transStatus/transStatus</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9865379d-1a22-4680-94cd-e451e536fd44</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/autoDebit/autoDebitMultiImme/autoDebitMultiImme(TransStatus)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>477255de-99f5-4ae7-b189-2b8f14262109</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/autoDebit/autoDebitMultiImme/autoDebitMultiImme(TransStatus.2)</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
