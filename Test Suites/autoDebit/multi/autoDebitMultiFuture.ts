<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>autoDebitMultiFuture</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-01-25T13:26:15</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>1afa23ab-829d-406a-b720-d428e4b2697e</testSuiteGuid>
   <testCaseLink>
      <guid>69593b8e-193f-4869-bbd9-f3d2a61d1677</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/url</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9f0cb0c1-8f7a-45d6-ac1d-c6d1663b92e2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/autoDebit/autoDebitMultiFuture/autoDebitMultiFuture(Maker)</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>923fec63-7f8d-4cbc-9096-6787f26f5154</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>fca232df-b582-4c4c-a712-b3d827f9d996</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>16334ad9-6cbb-41fb-9e92-a0d7949fb4e2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/autoDebit/autoDebitMultiFuture/autoDebitMultiFuture(Maker.Result)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ff9e4101-6587-4643-b8f9-4be6ea5f3127</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/pendingTask/pendingTask</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1cacf634-b111-4523-aa1b-f09e3668f907</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/autoDebit/autoDebitMultiFuture/autoDebitMultiFuture(Approver)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2966aba3-8821-4076-a411-c12d6e0f614b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/autoDebit/autoDebitMultiFuture/autoDebitMultiFuture(Approver.Result)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5571518d-339e-43ba-92bd-1525bfa2f9cb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/transStatus/transStatus</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c5252d0c-5991-490c-99ee-d088204c7ae7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/autoDebit/autoDebitMultiFuture/autoDebitMultiFuture(TransStatus)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d4188bc3-ca4f-492d-8d24-bbab7326faca</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/autoDebit/autoDebitMultiFuture/autoDebitMultiFuture(TransStatus.2)</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
