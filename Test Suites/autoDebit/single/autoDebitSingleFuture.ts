<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>autoDebitSingleFuture</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-01-25T14:21:50</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>de02b074-b7ea-4986-b280-cf74457109aa</testSuiteGuid>
   <testCaseLink>
      <guid>e2bebf9e-aef5-47df-883e-4f8ce2148ea6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/url</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>28ec4204-ec56-4452-b624-96313b37113b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/autoDebit/autoDebitSingleFuture/autoDebitSingleFuture(Maker)</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>facbe9eb-c5f0-42d2-a4e0-bc23a2fea249</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>61d1859e-9abf-4106-a2bd-bd345c10aefb</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>cf1d995a-00f1-4568-a3c8-372eb61b607b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/autoDebit/autoDebitSingleFuture/autoDebitSingleFuture(Maker.Result)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>634c8c1f-5c4b-4ae8-8c9b-171e06db46fd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/transStatus/transStatus</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f2ea1bbe-dd52-4191-a8ee-54fb9cdaf8fe</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/autoDebit/autoDebitSingleFuture/autoDebitSingleFuture(TransStatus)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1b4218d1-7273-4576-91aa-5fbddcfcf580</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/autoDebit/autoDebitSingleFuture/autoDebitSingleFuture(TransStatus.2)</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
