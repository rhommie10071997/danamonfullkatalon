<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>autoDebitSingleImme</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-01-25T14:10:02</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>9f2b416d-cc3e-4499-8e4b-fb2f89e65355</testSuiteGuid>
   <testCaseLink>
      <guid>5d051cf5-0be9-43fa-9b5c-75ba58260304</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/url</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>70f396c3-bc38-4aab-9727-e8c0971c240f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/autoDebit/autoDebitSingleImme/autoDebitSingleImme(Maker)</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>3293caf5-9ef0-4e13-98e6-e139e3e7f09a</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>03dac74b-ab0a-43b1-9244-4d5af4f949e6</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>233277c2-5d12-4a41-af21-2ae67a54c807</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/autoDebit/autoDebitSingleImme/autoDebitSingleImme(Maker.Result)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4e1a8e83-8eb9-40af-8369-b309322d2e14</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/transStatus/transStatus</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c6e29c10-da5a-4c1f-b1a9-b6433bdc62b3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/autoDebit/autoDebitSingleImme/autoDebitSingleImme(TransStatus)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5698d70a-1105-42de-b048-4bb47b352659</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/autoDebit/autoDebitSingleImme/autoDebitSingleImme(TransStatus.2)</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
