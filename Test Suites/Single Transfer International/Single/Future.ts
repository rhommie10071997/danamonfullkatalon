<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Future</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-02-21T18:09:14</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>4a0e9b1d-dacf-44fb-88ed-677077e40785</testSuiteGuid>
   <testCaseLink>
      <guid>2d43fc63-ca16-48c1-b968-d57fdfc01f69</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Transfer/International/Single User Future/Create/Entry Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a775bd2b-f1b5-4894-8599-ba2f0bf65d9a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Transfer/International/Single User Future/Create/Confirm Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c16d2fa0-b273-4750-8e21-2e2dfbec3305</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Transfer/International/Single User Future/Create/Result Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bfbc0316-fb14-46b9-bf2f-dfea1d414ae2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Transfer/International/Single User Future/Transaction Status/Transaction Status - Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3ff1abb4-53ab-433b-9811-27276ec833a4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Transfer/International/Single User Future/Transaction Status/Transaction Status - Ref No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ba5b564d-fb69-4d93-b1d9-bcc6918d9f43</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Transfer/International/Single User Future/Transaction Status/Transaction Status - Doc No</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
