<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Immediate</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-02-21T15:43:08</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>30f8be5f-8590-4bea-a481-89082ebfba33</testSuiteGuid>
   <testCaseLink>
      <guid>d9d1f343-3f4c-47c4-846e-1d2b22bc1184</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Transfer/International/Single User Immediate/Create/Entry Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c16c6650-9610-4280-98fd-aed55f717a52</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Transfer/International/Single User Immediate/Create/Confirm Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>176a5d18-11b4-4d53-9e2e-94db15ac4b69</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Transfer/International/Single User Immediate/Create/Result Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>13d1fb5a-bd25-4161-a0dd-0ee4196a12d9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Transfer/International/Single User Immediate/Transaction Status/Transaction Status - Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>397b14a7-b7d0-4dc8-b888-a9a2a348e31d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Transfer/International/Single User Immediate/Transaction Status/Transaction Status - Ref No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c790a912-6bbf-4adf-8705-4165180c6bdc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Transfer/International/Single User Immediate/Transaction Status/Transaction Status - Doc No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>34289258-b7cf-40ab-b046-edee29fa14c6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Transfer/International/Single User Immediate/Inquiry/Transaction Inquiry - Debit</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
