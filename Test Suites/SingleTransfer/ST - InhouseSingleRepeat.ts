<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>ST - InhouseSingleRepeat</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>0c5f651d-8f83-4d91-b8c0-4b3f220b239f</testSuiteGuid>
   <testCaseLink>
      <guid>fba25295-7cce-4b97-8fe7-0bdfc3a320cf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Inhouse(Repeat)/SingleUser-Inhouse(Repeat)CreateEntryScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3a1dda25-b1b9-48b7-8917-2132741a4246</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Inhouse(Repeat)/SingleUser-Inhouse(Repeat)CreateConfirmScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>11f5f4d5-9240-4d2a-95cb-bb6a0fc98445</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Inhouse(Repeat)/SingleUser-Inhouse(Repeat)CreateResultScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6fe05e8a-304a-4119-a38c-84ba3c9fdece</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Inhouse(Repeat)/SingleUser-Inhouse(Repeat)TransactionStatusEntryScreenReffNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8322e893-6b24-4cfd-a6f1-352f99aebe6b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Inhouse(Repeat)/SingleUser-Inhouse(Repeat)TransactionStatusScreenReffNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>80059532-2d7b-4085-be77-7683148cd130</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Inhouse(Repeat)/SingleUser-Inhouse(Repeat)TransactionStatusEntryScreenDocNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ae7a605e-6a9c-40c5-a412-21de655f661e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Inhouse(Repeat)/SingleUser-Inhouse(Repeat)TransactionStatusScreenDocNo</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
