<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>ST - SknSingleImmediate</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>272ab2a9-5bc1-422c-b894-ec557f19c365</testSuiteGuid>
   <testCaseLink>
      <guid>76530fd0-4f05-4501-a615-a8048656fd5c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Domestic(Immediate)SKN/SingleUser-Domestic(Immediate)SKNEntry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9b543178-56b1-4cd6-980b-0f7fa8675c7d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Domestic(Immediate)SKN/SingleUser-Domestic(Immediate)SKNConfirmScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fd57dca3-13cb-4019-9831-77970606db72</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Domestic(Immediate)SKN/SingleUser-Domestic(Immediate)SKNWaitingForApprovalScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3235edc9-92a6-4bbe-b192-de254c0b8dca</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Domestic(Immediate)SKN/SingleUser-Domestic(Immediate)SKNTransactionStatusReffNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e76b4aa3-502b-42d2-9c07-62fcff1be3b8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Domestic(Immediate)SKN/SingleUser-Domestic(Immediate)SKNTransactionStatusDocNo</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
