<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>ST - InhouseMultiImmediate</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>aa2d0856-c277-4cdd-90c5-a6c258a5aedc</testSuiteGuid>
   <testCaseLink>
      <guid>a992776e-eb6e-48c6-9e3b-561bd4f549ee</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Inhouse(Immediate)/MultiUser-Inhouse(Immediate)EntryScreen</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>08b004d5-0af6-40c3-9d54-3e2e431da4a1</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>89cec893-6831-4e05-b43c-646f5eecc4e9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Inhouse(Immediate)/MultiUser-Inhouse(Immediate)ConfirmScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b57e04ce-378f-4586-9b33-3327a5df1735</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Inhouse(Immediate)/MultiUser-Inhouse(Immediate)ResultScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3ee3e1bf-9a87-43a0-902e-5632d782e647</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Inhouse(Immediate)/MultiUser-Inhouse(Immediate)ApproverConfirmScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b23ee342-abf3-4ebd-9a30-bf571d68a067</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Inhouse(Immediate)/MultiUser-Inhouse(Immediate)ApproverResultScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d30b0f44-4cec-4031-873c-83b285a26726</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Inhouse(Immediate)/MultiUser-Inhouse(Immediate)ReleaserConfirmScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0c6de09f-b44d-44ec-ba6f-3816b0ae5e0a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Inhouse(Immediate)/MultiUser-Inhouse(Immediate)ReleaserResultScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>441e8fcb-7c6b-487a-adcf-268f89124ea7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Inhouse(Immediate)/MultiUser-Inhouse(Immediate)TransactionStastusReffNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>16a0a3b9-4389-485f-a755-b0118e78fc34</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Inhouse(Immediate)/MultiUser-Inhouse(Immediate)TransactionStatusDocNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>17e62a51-3536-4e02-a0a4-27adcbe8e231</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Inhouse(Immediate)/MultiUser-Inhouse(Immediate)TransactionInquriySender</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4bc3fed9-3fe7-49da-8ffc-bcfb2af6e6d4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Inhouse(Immediate)/MultiUser-Inhouse(Immediate)TransactionInquriyReceiver</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
