<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>ST - RtgsMultiImme</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>23f8afe5-83d2-438f-8774-b2c53a5cbe85</testSuiteGuid>
   <testCaseLink>
      <guid>4c3f2c1f-29c5-4697-802d-fa9991cc5cd9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(Immediate)RTGS/MultiUser-Domestic(Immediate)RTGSEntryScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8857e27f-c370-42ae-9236-6441cce03f66</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(Immediate)RTGS/MultiUser-Domestic(Immediate)RTGSEntryConfirmScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5dcdf1f5-dc82-4641-974f-2c9492bbdd66</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(Immediate)RTGS/MultiUser-Domestic(Immediate)RTGSEntryResultScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4278eb1a-1cce-43f7-a6e6-9f4870f6e0c8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(Immediate)RTGS/MultiUser-Domestic(Immediate)RTGSApproverEntryscreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>35585769-7d67-426f-ac65-0e3fc99aef24</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(Immediate)RTGS/MultiUser-Domestic(Immediate)RTGSApproverConfirmScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7843d3be-59a6-49d3-85cf-05e2fc287648</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(Immediate)RTGS/MultiUser-Domestic(Immediate)RTGSApproverResultScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6e69b998-178b-4343-9ba7-b59e6262bb36</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(Immediate)RTGS/MultiUser-Domestic(Immediate)RTGSTransactionStatusEntryReffNoScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3f0ae0e8-afa2-4b9c-9307-b9f21fcf1a60</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(Immediate)RTGS/MultiUser-Domestic(Immediate)RTGSTransactionStatusReffNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>80059f34-23b9-45be-9d00-278fa2dffcb9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(Immediate)RTGS/MultiUser-Domestic(Immediate)RTGSTransactionStatusEntryDocNoScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6d18438d-1c12-4fa4-8ef5-95e9003512a2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(Immediate)RTGS/MultiUser-Domestic(Immediate)RTGSTransactionStatusDocNo</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
