<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>ST - RtgsMultiRepeat</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>79c0f77e-5e92-4a5f-b237-f04272551172</testSuiteGuid>
   <testCaseLink>
      <guid>378ddbba-d376-4611-9909-0f6211f4bf8e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(Repeat)RTGS/MultiUser-Domestic(Repeat)CreateEntryScreenRTGS</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5e029081-7506-491f-a8fd-7a8123e38bf2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(Repeat)RTGS/MultiUser-Domestic(Repeat)CreateConfirmScreenRTGS</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>aea77c9d-dbd7-4342-850c-eae361b76b76</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(Repeat)RTGS/MultiUser-Domestic(Repeat)CreateResultScreenRTGS</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>31d10ef6-17f9-4af1-87bf-e75c4b8a7c98</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(Repeat)RTGS/MultiUser-Domestic(Repeat)ApproverEntryScreenRTGS</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ccf5a3ec-8aad-49d1-85db-b6c6b8120ec8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(Repeat)RTGS/MultiUser-Domestic(Repeat)ApproverConfirmScreenRTGS</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3ba0da39-189c-411e-82c0-c92530dd9252</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(Repeat)RTGS/MultiUser-Domestic(Repeat)ApproverResultScreenRTGS</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0d1c1d10-4926-48a4-a7e1-07ed7b2b6891</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(Repeat)RTGS/MultiUser-Domestic(Repeat)TransactionStatusEntryScreenDocNoRTGS</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e16672ee-f44c-4445-b40d-d0f0bdcc08d5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(Repeat)RTGS/MultiUser-Domestic(Repeat)TransactionStatusDocNoRTGS</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2be7cf9a-fe2a-4240-a24b-c19ab6a8ae80</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(Repeat)RTGS/MultiUser-Domestic(Repeat)TransactionStatusEntryScreenReffNoRTGS</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>282d897f-1367-44d6-a0af-a40cbddc8984</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(Repeat)RTGS/MultiUser-Domestic(Repeat)TransactionStatusReffNoRTGS</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
