<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>ST - InhouseSingleFuture</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>f11c4b97-bff1-43b0-b6c7-33efbde1e1a8</testSuiteGuid>
   <testCaseLink>
      <guid>8ef0d4a8-e0a1-447d-9ce1-00a2d89872b2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Inhouse(FutureDate)/SingleUser-Inhouse(FutureDate)CreateEntryScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e8ac1239-e075-4ad8-95f8-834f667d3344</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Inhouse(FutureDate)/SingleUser-Inhouse(FutureDate)CreateConfirmScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>eff3f586-591e-4125-a76b-04b8335686fa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Inhouse(FutureDate)/SingleUser-Inhouse(FutureDate)CreateResultScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9b2fb4f9-bc65-4c13-95db-90b46c458f94</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Inhouse(FutureDate)/SingleUser-Inhouse(FutureDate)TransactionStatusReffNoEntryScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a6a09442-8ff2-42ed-99a1-f9e4bbeef03c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Inhouse(FutureDate)/SingleUser-Inhouse(FutureDate)TransactionStatusReffNoScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>69f3f721-e665-4ad2-9ed7-e999ae0500d2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Inhouse(FutureDate)/SingleUser-Inhouse(FutureDate)TransactionStatusDocNoEntryScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2f62341b-3650-4298-953e-9e51a3504cd8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Inhouse(FutureDate)/SingleUser-Inhouse(FutureDate)TransactionStatusDocNoScreen</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
