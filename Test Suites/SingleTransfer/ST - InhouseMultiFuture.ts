<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>ST - InhouseMultiFuture</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>534db8e6-6106-40fc-bc6f-3e99c9bf88df</testSuiteGuid>
   <testCaseLink>
      <guid>54da5dcc-3ca5-4340-b429-409827852e97</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Inhouse(FutureDate)/MultiUser-Inhouse(FutureDate)CreateEntryScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d60ae87b-f8c0-4e93-97c0-2468002cbc7a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Inhouse(FutureDate)/MultiUser-Inhouse(FutureDate)CreateConfirmScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0379d08c-37eb-49e6-822f-079dc450f0a6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Inhouse(FutureDate)/MultiUser-Inhouse(FutureDate)CreateResultScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d8b372a6-239b-46b9-a5f4-47e68a075c4b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Inhouse(FutureDate)/MultiUser-Inhouse(FutureDate)ApproverEntryScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8a63fed9-c606-4eb5-ac8a-34677e8f0596</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Inhouse(FutureDate)/MultiUser-Inhouse(FutureDate)ApproverConfirmScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5da95ede-7745-4ac8-95c6-78f0908ddf24</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Inhouse(FutureDate)/MultiUser-Inhouse(FutureDate)ApproverResultScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>33b4aaf2-a4e0-4edc-b755-0711014a653d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Inhouse(FutureDate)/MultiUser-Inhouse(FutureDate)TransactionStatusDocNoEntryScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8a54da1e-5ab2-49c4-9436-56c6f62c1d6e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Inhouse(FutureDate)/MultiUser-Inhouse(FutureDate)TransactionStatusDocNoScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3a801e11-5211-4cd8-974a-99e608d9d6cb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Inhouse(FutureDate)/MultiUser-Inhouse(FutureDate)TransactionStatusReffNoEntryScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c4e5355c-f938-4158-86f5-a6a4d6ae9be0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Inhouse(FutureDate)/MultiUser-Inhouse(FutureDate)TransactionStatusReffNoScreen</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
