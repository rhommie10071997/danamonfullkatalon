<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>ST - InhouseMultiRepeat</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>52197344-f36d-4835-b0b8-4df4d8910e24</testSuiteGuid>
   <testCaseLink>
      <guid>2754ba43-b58c-4c53-9d4c-b97832cb6dfe</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Inhouse(Repeat)/MultiUser-Inhouse(Repeat)CreateEntryScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3c70f761-31e5-42f0-a8bc-cb46af0bbf60</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Inhouse(Repeat)/MultiUser-Inhouse(Repeat)CreateConfirmScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8c27d0b2-66ae-4473-a127-1ff584e9ca8a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Inhouse(Repeat)/MultiUser-Inhouse(Repeat)CreateResultScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8b15453c-06e6-4908-bc12-d9012a3632f8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Inhouse(Repeat)/MultiUser-Inhouse(Repeat)ApproverEntryScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>98659cb5-9f55-4c0f-ae5d-f519b950d5d4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Inhouse(Repeat)/MultiUser-Inhouse(Repeat)ApproverConfirmScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d057b6f1-bfff-4e58-af5d-799f8fa06035</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Inhouse(Repeat)/MultiUser-Inhouse(Repeat)ApproverResultScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b8c73a70-b18f-42e3-a0c4-21b8f08774b5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Inhouse(Repeat)/MultiUser-Inhouse(Repeat)TransactionStatusEntryScreenReffNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1487b847-7342-478c-8976-f4649ecb8154</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Inhouse(Repeat)/MultiUser-Inhouse(Repeat)TransactionStatusScreenReffNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4929d4b8-000a-45bd-a78c-85a6fb2b4327</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Inhouse(Repeat)/MultiUser-Inhouse(Repeat)TransactionStatusEntryScreenDocNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>69e281e2-0c33-45fc-8013-995ab48e09ad</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Inhouse(Repeat)/MultiUser-Inhouse(Repeat)TransactionStatusScreenDocNo</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
