<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>ST - RtgsSingleRepeat</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>03afcb88-4b56-48f7-a4a6-db9853d8c832</testSuiteGuid>
   <testCaseLink>
      <guid>0547f69b-9f20-4b3c-8bfa-13ba54106a55</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Domestic(Repeat)RTGS/SingleUser-Domestic(Repeat)CreateEntryScreenRTGS</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0429cba0-ce92-4747-9c12-382a8c1dc823</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Domestic(Repeat)RTGS/SingleUser-Domestic(Repeat)CreateConfirmScreenRTGS</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>46a0f4ed-6db0-433a-b8ac-0ecfd58e76bc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Domestic(Repeat)RTGS/SingleUser-Domestic(Repeat)CreateResultScreenRTGS</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f7c4ad6c-d5a5-416d-a7c5-ef047f9e5341</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Domestic(Repeat)RTGS/SingleUser-Domestic(Repeat)TransactionStatusEntryScreenDocNoRTGS</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>49174ae3-ac8b-4e65-bb94-d57e5d1f817a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Domestic(Repeat)RTGS/SingleUser-Domestic(Repeat)TransactionStatusDocNoRTGS</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c7baa5c9-3689-441e-9164-a233723cf581</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Domestic(Repeat)RTGS/SingleUser-Domestic(Repeat)TransactionStatusEntryScreenReffNoRTGS</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4403d02f-491a-4a88-9362-ab731c6ed51e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Domestic(Repeat)RTGS/SingleUser-Domestic(Repeat)TransactionStatusReffNoRTGS</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
