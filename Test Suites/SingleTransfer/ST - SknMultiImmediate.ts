<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>ST - SknMultiImmediate</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>12f6062d-fb7e-4270-99a6-e80858320f88</testSuiteGuid>
   <testCaseLink>
      <guid>e41644c8-429f-480f-aab5-b20018d34625</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(Immediate)SKN/MultiUser-Domestic(Immediate)SKNEntry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b32bd929-1485-4720-b9f5-9bfe49b89027</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(Immediate)SKN/MultiUser-Domestic(Immediate)SKNConfirmScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5fb45418-1862-4b86-8843-db178d348ec6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(Immediate)SKN/MultiUser-Domestic(Immediate)SKNWaitingForApprovalScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bbbd9e7e-e348-4ff3-a5a8-9564232b8203</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(Immediate)SKN/MultiUser-Domestic(Immediate)SKNApproverConfirmScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ef401930-6eb6-4298-b4e1-f23bb06b7a65</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(Immediate)SKN/MultiUser-Domestic(Immediate)SKNApproverResultScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f75432b7-2c99-4229-8378-60df07f480be</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(Immediate)SKN/MultiUser-Domestic(Immediate)SKNTransactionStatusReffNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1d212483-ac20-4074-b7a5-04b6fdecee2f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(Immediate)SKN/MultiUser-Domestic(Immediate)SKNTransactionStatusDocNo</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
