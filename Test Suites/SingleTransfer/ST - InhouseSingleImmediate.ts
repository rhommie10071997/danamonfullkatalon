<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>ST - InhouseSingleImmediate</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>0d35557b-236f-4b21-8984-31daaecb245e</testSuiteGuid>
   <testCaseLink>
      <guid>445174d7-eb67-456d-ae2d-b7186f479175</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Inhouse(Immediate)/SingleUser-Inhouse(Immediate)EntryScreen</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>7408b128-efc8-4c2e-b893-6c56f71a5971</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>7d63926b-c83d-437c-9e53-e3f0644f1825</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Inhouse(Immediate)/SingleUser-Inhouse(Immediate)ConfirmScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9da80eda-253f-444e-a8a2-048ce9080248</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Inhouse(Immediate)/SingleUser-Inhouse(Immediate)ResultScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2076e2f1-8d2f-4a28-9bbe-eb33d05240b1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Inhouse(Immediate)/SingleUser-Inhouse(Immediate)TransactionStastusReffNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>72fa136b-b2c4-4c8c-9f3d-19d3fd9c76cf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Inhouse(Immediate)/SingleUser-Inhouse(Immediate)TransactionStatusDocNo</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
