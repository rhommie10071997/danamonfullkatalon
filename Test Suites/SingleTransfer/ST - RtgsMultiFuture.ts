<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>ST - RtgsMultiFuture</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>1f048556-b98e-443e-9786-b506198242f4</testSuiteGuid>
   <testCaseLink>
      <guid>9bd9e229-3445-47dd-b7ad-1b92b02c83db</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(FutureDate)RTGSS/MultiUser-Domestic(FutureDate)RTGSEntryScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>61839f89-bbd1-4a36-a879-d6b666d13b44</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(FutureDate)RTGSS/MultiUser-Domestic(FutureDate)RTGSEntryConfirmScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3af81e0c-06fa-4900-a19d-8fb0ebe56812</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(FutureDate)RTGSS/MultiUser-Domestic(FutureDate)RTGSEntryResultScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>11f71283-bfe3-48dc-bc6e-b80006f35b4a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(FutureDate)RTGSS/MultiUser-Domestic(FutureDate)RTGSApproverEntryscreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>855271ce-68fc-4dd5-ba4c-c91cefbe5301</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(FutureDate)RTGSS/MultiUser-Domestic(FutureDate)RTGSApproverConfirmScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b4667029-c44b-44c3-a8c9-b9fe7d278b96</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(FutureDate)RTGSS/MultiUser-Domestic(FutureDate)RTGSApproverResultScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>82f2b4bb-bb28-4d34-a71d-cdb26bd62629</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(FutureDate)RTGSS/MultiUser-Domestic(FutureDate)RTGSTransactionStatusEntryReffNoScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b1c39ec3-74ca-4b7c-825a-c4988a8013f5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(FutureDate)RTGSS/MultiUser-Domestic(FutureDate)RTGSTransactionStatusReffNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b1502b89-0916-4195-a010-dfaa64339af3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(FutureDate)RTGSS/MultiUser-Domestic(FutureDate)RTGSTransactionStatusEntryDocNoScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fedf00a9-eb57-447a-92f4-22a5a4296c97</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(FutureDate)RTGSS/MultiUser-Domestic(FutureDate)RTGSTransactionStatusDocNo</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
