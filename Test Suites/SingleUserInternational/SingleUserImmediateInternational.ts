<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>SingleUserImmediateInternational</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-10-12T15:05:35</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>55c340f5-d30b-4c24-b507-0d5685ebc54a</testSuiteGuid>
   <testCaseLink>
      <guid>b9f28d8a-126b-416e-8fea-275fee52f7cb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-International(Immediate)/SingleUser-International(Immediate)CreateEntryScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>46635aca-e770-427b-b8a4-b25a271f4424</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-International(Immediate)/SingleUser-International(Immediate)CreateConfirmScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b78f62d7-0673-4036-88a1-3b630315e810</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-International(Immediate)/SingleUser-International(Immediate)CreateResultScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>56e2d950-0213-4a35-8e0f-cf3243bcf0f0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-International(Immediate)/SingleUser-International(Immediate)TransactionStatusEntryScreenReffNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b20c0951-b827-45a6-a2cb-fb2bc9c9af0a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-International(Immediate)/SingleUser-International(Immediate)TransactionStatusReffNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>58c28b4c-c150-4820-8993-fea0974db527</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-International(Immediate)/SingleUser-International(Immediate)TransactionStatusEntryScreenDocNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>75e2895f-97ea-4fdb-8709-b2226c9f31cf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-International(Immediate)/SingleUser-International(Immediate)TransactionStatusDocNo</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
