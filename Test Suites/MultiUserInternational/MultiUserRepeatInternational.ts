<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>MultiUserRepeatInternational</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-02-20T23:30:17</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>55155a14-1142-4633-9260-1db08d8906ac</testSuiteGuid>
   <testCaseLink>
      <guid>69046468-8e09-407d-88d6-c932b85804b2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-International(Repeat)/MultiUser-International(Repeat)CreateEntryScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ac692948-fdf2-4c2d-9a82-c4186949bde8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-International(Repeat)/MultiUser-International(Repeat)CreateConfirmScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>21b6ebd5-3f84-4442-9251-c324b33f2d98</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-International(Repeat)/MultiUser-International(Repeat)CreateResultScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>78058a68-54b1-4d2c-be6e-213ec3ed17a5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-International(Repeat)/MultiUser-International(Repeat)ApproverEntryScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>85a9ef23-15e4-433a-9a9d-b537fe748ed1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-International(Repeat)/MultiUser-International(Repeat)ApproverConfirmScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>686eb89e-e88f-4060-8eca-5ed40c8321ac</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-International(Repeat)/MultiUser-International(Repeat)ApproverResultScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c38598aa-4a2f-46fe-a4ec-905435cf5e85</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-International(Repeat)/MultiUser-International(Repeat)TransactionStatusEntryScreenReffNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2ce7dbf4-6b7f-4c58-8e48-61e7e25870a2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-International(Repeat)/MultiUser-International(Repeat)TransactionStatusReffNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f5c51a1a-980b-495e-aeeb-71ea0d92c4d3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-International(Repeat)/MultiUser-International(Repeat)TransactionStatusEntryScreenDocNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>12d5279b-47c3-4804-8602-dee894c8b496</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-International(Repeat)/MultiUser-International(Repeat)TransactionStatusDocNo</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
