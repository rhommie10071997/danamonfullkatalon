<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>MultiUserImmediateInternational</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-02-20T22:15:25</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>36f7e119-e580-46a3-bd24-984bc219741e</testSuiteGuid>
   <testCaseLink>
      <guid>dd8d0eb8-ef4c-4f0c-a9d3-38b048bbe8f4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-International(Immediate)/MultiUser-International(Immediate)CreateEntryScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>711a373d-03bc-4311-8821-0b6a91fffc2a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-International(Immediate)/MultiUser-International(Immediate)CreateConfirmScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>77036113-d6aa-4276-bbb3-55ab1118eaae</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-International(Immediate)/MultiUser-International(Immediate)CreateResultScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c0f50f01-1d20-40bb-8912-04c9bda694fe</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-International(Immediate)/MultiUser-International(Immediate)ApproverEntryScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8ff7df65-64a9-4927-9547-cad64a0d5707</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-International(Immediate)/MultiUser-International(Immediate)ApproverConfirmScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>67045020-bb5d-4549-ba00-7aba60cc95e2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-International(Immediate)/MultiUser-International(Immediate)ApproverResultScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f032b86e-1cd0-4ce6-91f1-9c65062879e3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-International(Immediate)/MultiUser-International(Immediate)TransactionStatusEntryScreenReffNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cdb0f931-d751-4bf0-8410-885ab5dea93c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-International(Immediate)/MultiUser-International(Immediate)TransactionStatusReffNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>71128110-49c4-481d-a9e8-f7fcc17cceb9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-International(Immediate)/MultiUser-International(Immediate)TransactionStatusEntryScreenDocNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fa2166c9-db9e-4911-8575-1a2bc933fc07</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-International(Immediate)/MultiUser-International(Immediate)TransactionStatusDocNo</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
