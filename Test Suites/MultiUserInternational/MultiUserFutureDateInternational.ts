<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>MultiUserFutureDateInternational</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-02-20T23:15:15</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>ae017b39-edd4-4449-8265-cf05863d2676</testSuiteGuid>
   <testCaseLink>
      <guid>08863ef4-bf55-467a-a1b6-0844288e7075</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-International(FutureDate)/MultiUser-International(FutureDate)CreateEntryScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f539671d-ced3-419b-91bc-152837604c9e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-International(FutureDate)/MultiUser-International(FutureDate)CreateConfirmScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>815ce5e6-462c-4800-a215-5747201aad4f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-International(FutureDate)/MultiUser-International(FutureDate)CreateResultScreen12345</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b8149569-ff2b-472f-9960-4d16688bbda9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-International(FutureDate)/MultiUser-International(FutureDate)ApproverEntryScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fea0c499-2c2b-4553-b83e-681478519b66</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-International(FutureDate)/MultiUser-International(FutureDate)ApproverConfirmScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0d29eba1-a3b3-4abb-b680-f4e9a6d68aff</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-International(FutureDate)/MultiUser-International(FutureDate)ApproverResultScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2c12fb41-c52b-43d6-8f7b-df60960040e4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-International(FutureDate)/MultiUser-International(FutureDate)TransactionStatusEntryScreenReffNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d7a446fa-ff2b-4a58-83b1-1f32c3f16cad</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-International(FutureDate)/MultiUser-International(FutureDate)TransactionStatusReffNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6e6c516b-a69b-4f2b-9ce4-40a12a370846</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-International(FutureDate)/MultiUser-International(FutureDate)TransactionStatusEntryScreenDocNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6f526e64-7a4a-4c31-ba7d-7e6e5a01a1c5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-International(FutureDate)/MultiUser-International(FutureDate)TransactionStatusDocNo</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
