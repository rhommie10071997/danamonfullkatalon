<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>SingleUserDomesticRTGS(Immediate)</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-02-25T12:06:20</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>917e6eca-3f2a-4060-a4e2-90c067f07800</testSuiteGuid>
   <testCaseLink>
      <guid>9f29086b-850e-4af2-b15e-1574d6008276</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-Domestic(Immediate)RTGS/SingleUserPayroll-DomesticRTGS(Immediate)CreateEntryScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2a2399fc-5dd2-4238-8d28-677b69b9fa1e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-Domestic(Immediate)RTGS/SingleUserPayroll-DomesticRTGS(Immediate)BucketListLoop</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>554e1f7f-4385-4a1d-ab35-0726eb0e722e</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>16878946-1759-4377-98e2-facf4c4bcbe9</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>ae9fa9ff-657b-46f5-ac68-6f1ccaa1518f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-Domestic(Immediate)RTGS/SingleUserPayroll-DomesticRTGS(Immediate)BucketDetails</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>ccba633a-1dd5-4a48-a64a-19cdd6c8fe5a</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>e74e3bba-1fac-4332-af4d-93ea6c44866a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-Domestic(Immediate)RTGS/SingleUserPayroll-DomesticRTGS(Immediate)BucketConfirm</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>ac9e0bb8-f608-4945-b9fe-044f3acabb1d</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>1ea6c908-e044-48d1-836a-11c102f0bac1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-Domestic(Immediate)RTGS/SingleUserPayroll-DomesticRTGS(Immediate)CreateResultScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c13f059f-054e-48f6-8a8c-6c8b75ad6dda</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-Domestic(Immediate)RTGS/SingleUserPayroll-DomesticRTGS(Immediate)TransactionStatusEntryScreenReffNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8615036f-9614-4601-b16c-5fe8e577e89f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-Domestic(Immediate)RTGS/SingleUserPayroll-DomesticRTGS(Immediate)TransactionStatusReffNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>59f45ed6-cc6b-4fbc-a778-adb1ed68335d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-Domestic(Immediate)RTGS/SingleUserPayroll-DomesticRTGS(Immediate)TransactionStatusEntryScreenDocNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a24ea4e2-96f2-4ce5-bef9-167c3bef320f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-Domestic(Immediate)RTGS/SingleUserPayroll-DomesticRTGS(Immediate)TransactionStatusDocNo</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
