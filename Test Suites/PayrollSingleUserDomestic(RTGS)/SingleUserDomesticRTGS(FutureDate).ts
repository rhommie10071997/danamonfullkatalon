<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>SingleUserDomesticRTGS(FutureDate)</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-01-24T15:50:10</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>d36d8608-f0d1-4fd8-ad84-655cf37e2019</testSuiteGuid>
   <testCaseLink>
      <guid>00ff853b-a6c3-4321-ab5e-28637f4c7559</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-Domestic(FutureDate)RTGS/SingleUserPayroll-DomesticRTGS(FutureDate)CreateEntryScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>130b4384-8dfd-45b6-9191-08d6254f5ba0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-Domestic(FutureDate)RTGS/SingleUserPayroll-DomesticRTGS(FutureDate)BucketListLoop</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>88acbeca-30dc-4da5-846a-34e8406d345f</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>dc62ec27-a931-4811-84f6-a8935b4af3d5</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>b4ad762e-c8a6-46b5-82c8-e54f8676b4a5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-Domestic(FutureDate)RTGS/SingleUserPayroll-DomesticRTGS(FutureDate)BucketDetails</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>18d6402d-3ed0-44d6-83f2-4d9cf21bb1cf</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>6f5c6ab1-c28f-4d0f-9320-f5c32ac041ef</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-Domestic(FutureDate)RTGS/SingleUserPayroll-DomesticRTGS(FutureDate)BucketConfirm</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>ef2b5e3d-148f-421c-86fd-0cca19442982</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>025dfca8-807b-4caf-9067-89ed82d28183</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-Domestic(FutureDate)RTGS/SingleUserPayroll-DomesticRTGS(FutureDate)CreateResultScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2d7a1fee-599c-47d5-9561-1c85ab928638</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-Domestic(FutureDate)RTGS/SingleUserPayroll-DomesticRTGS(FutureDate)TransactionStatusEntryScreenReffNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4794edb0-772a-4cee-a083-dbc7f2c4bd29</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-Domestic(FutureDate)RTGS/SingleUserPayroll-DomesticRTGS(FutureDate)TransactionStatusReffNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bf111dfc-370b-4de8-8f69-8e50e58a395d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-Domestic(FutureDate)RTGS/SingleUserPayroll-DomesticRTGS(FutureDate)TransactionStatusEntryScreenDocNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4a00ffd2-01d0-4921-bc92-63583973070d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-Domestic(FutureDate)RTGS/SingleUserPayroll-DomesticRTGS(FutureDate)TransactionStatusDocNo</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
