<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>BeneficiaryListAdd</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-02-27T13:19:32</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>3cafd983-9ef2-4dc5-a2c4-2a1dc196e438</testSuiteGuid>
   <testCaseLink>
      <guid>f5832c45-d9a5-49f0-ab4f-0bc22b401559</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/BeneficiaryListAdd</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>29fde5dd-ed7e-4c08-914d-55d8c0abf890</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/BeneficiaryListAddConfirmScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0bc461a5-56da-410b-946f-46b58fff6b0e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/BeneficiaryListAddConfrimScreenDomestic</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2172d4ac-5207-41fd-8f0c-f7e50ec2a29e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/BeneficiaryList/BeneficiaryListAddConfrimScreenInternational</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>908ff4f6-baef-404e-ae7f-346964dca2cf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/BeneficiaryList/BeneficiaryListAddEntryResultScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>06b399b2-5401-467c-98f4-a789f26e8898</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/BeneficiaryList/BeneficiaryListAddEntryResultScreenDomestic</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>afc1ca93-2173-4b3e-998b-aaf609fd2c86</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/BeneficiaryList/BeneficiaryListAddEntryResultScreenInternational</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3f9e42e0-7cf7-4b2c-9f96-834cfef07cc4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/BeneficiaryList/BeneficiaryListAddApproverEntryScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2ddd2da9-312e-414c-b20c-84c40f08587c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/BeneficiaryList/BeneficiaryListAddForLooping</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>15432f7b-604e-4a68-a073-0e120388cb07</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>7f51c7f0-e10b-4863-855a-23e1a0bd7731</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>afdf86df-d5d6-4a8c-9312-0693f557fba3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/BeneficiaryList/BeneficiaryListAddApproverConfirmScreenInhouse(frontconfrimation)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1a6e2eb8-2ea4-4f3b-b756-ed76d29735f3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/BeneficiaryList/BeneficiaryListAddApproverConfirmScreenDomestic</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>25fdbb2d-ef51-486a-8fd5-870a0b689236</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/BeneficiaryList/BeneficiaryListAddApproverConfrimScreenInternational</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>408c6e29-27df-46b0-a510-7ee759d7fac8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/BeneficiaryList/BeneficiaryListAddApproverResultScreenInhouse(frontConfrimation)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fd4a821a-4cb1-411e-a731-c443ea3b9ab1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/BeneficiaryList/BeneficiaryListAddApproverScreenResultDomestic</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4c919bd1-bfca-4a2c-a189-46398c7ade7d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/BeneficiaryList/BeneficiaryListAddApproverResultScreenInternational</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
