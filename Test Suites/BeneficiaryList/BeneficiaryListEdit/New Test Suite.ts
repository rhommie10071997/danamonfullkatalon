<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>New Test Suite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-02-27T14:16:15</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>a0b1dcb1-245e-4942-a73f-e963ff9259c4</testSuiteGuid>
   <testCaseLink>
      <guid>4ffc4a67-55bb-4991-b747-fe9eb60f2eb4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/BeneficiaryListEdit/BeneficiaryListEditEntryScreenn</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>67c7d269-b8a0-4a7e-a14b-ff5918b9baba</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>73bb2f90-e384-44ef-9c83-0da23134a046</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/BeneficiaryListEdit/BeneficiaryListEditConfirmScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c1fde8cc-f7f2-4091-a1eb-e47c931c9323</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/BeneficiaryListEdit/BeneficiaryListEditConfrimScreenDomestic</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>027c1d4d-3106-430f-bf71-6e150b505295</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/BeneficiaryListEdit/BeneficiaryListEditConfrimScreenInternational</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1894ae21-7c67-46e3-942e-6476ff7cf6ed</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/BeneficiaryListEdit/BeneficiriaryListEditEntryTheEdtiDataDomestic</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7323f9df-3240-4034-a417-cd3fb61c38b4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/BeneficiaryListEdit/BeneficiaryListEditEntryTheEditDataInternational</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a57ec08e-6697-44b4-84b8-dd2031b47f32</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/BeneficiaryListEdit/BeneficiaryListEditConfirmScreenAfterEdit</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b7ddc759-f47a-462f-b54c-38b5191091c6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/BeneficiaryListEdit/BeneficiaryListEditConfrimScreenDomesticAfterEdit</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a0b9d1d2-0c41-4adb-8692-36d59e6ff641</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/BeneficiaryListEdit/BeneficiaryListEditConfrimScreenInternationalAfterEdit</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>30e71bf8-5d98-40ac-8538-9f7840993a60</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/BeneficiaryListEdit/BeneficiaryListEditEntryResultScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cd342747-7643-485e-864f-f48909a7bee8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/BeneficiaryListEdit/BeneficiaryListEditEntryResultScreenDomestic</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>41bee306-6ddb-4441-b4fe-cc16d0a57538</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/BeneficiaryListEdit/BeneficiaryListEditEntryResultScreenInternational</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>da6041d1-6a4a-411e-a0e6-87662427c4ce</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/BeneficiaryListEdit/BeneficiaryListEditApproverEntryScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>deaeecfa-cc0f-4813-a556-8c412168a932</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/BeneficiaryListEdit/BeneficiaryListEditApproverConfirmScreenInhouse(frontconfrimation)(oldValue)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ef858687-85ef-4b99-b65a-327ee8c7f368</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/BeneficiaryListEdit/BeneficiaryListEditApproverConfirmScreenDomestic(oldValue)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>79a44fc6-7d44-42fe-bcb5-0437b2f6b9c9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/BeneficiaryListEdit/BeneficiaryListEditApproverConfrimScreenInternational(oldValue)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0c331303-095d-465a-a8e9-51ed7a16cc1e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/BeneficiaryListEdit/BeneficiaryListEditApproverConfirmScreenInhouse(frontconfrimation)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>750fbaa9-110f-407b-9183-66c5f23d9bbe</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/BeneficiaryListEdit/BeneficiaryListEditApproverConfirmScreenDomestic</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0fd0051b-1c2c-439a-813e-3ab46507cb72</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/BeneficiaryListEdit/BeneficiaryListEditApproverConfrimScreenInternational</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>40bd3149-05ae-46c8-8498-9b66d70c5f3d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/BeneficiaryListEdit/BeneficiaryListEditApproverResultScreenInhouse(frontConfrimation)(oldValue)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7a99b10d-da6f-46ef-8ad6-b2eea5dfe13c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/BeneficiaryListEdit/BeneficiaryListEditApproverScreenResultDomestic(oldValue)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ecec05d7-4cbe-44d3-b3b3-64decfe5871a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/BeneficiaryListEdit/BeneficiaryListEditApproverResultScreenInternational(oldValue)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>18c77341-3192-4d1d-bb73-f9395c654f9c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/BeneficiaryListEdit/BeneficiaryListEditApproverResultScreenInhouse(frontConfrimation)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>41521e36-9558-48f6-a963-b225dd605010</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/BeneficiaryListEdit/BeneficiaryListEditApproverScreenResultDomestic</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a759483a-fe0c-4002-8cc7-0307d08499d8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/BeneficiaryListEdit/BeneficiaryListEditApproverResultScreenInternational</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
