<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>testingApprover</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-01-30T15:32:34</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>2dd0abf4-1144-4e13-815c-dc165a403e94</testSuiteGuid>
   <testCaseLink>
      <guid>60b59f0f-a213-4283-b431-11507ff1e36c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/BeneficiaryListEdit/BeneficiaryListEditApproverEntryScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>45869099-d8c7-42b6-af30-86a2a399e20a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/BeneficiaryListEdit/BeneficiaryListEditApproverConfirmScreenInhouse(frontconfrimation)(oldValue)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8ec24a65-8980-4ce5-900c-f242f203a33d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/BeneficiaryListEdit/BeneficiaryListEditApproverConfirmScreenDomestic(oldValue)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ad1efbbc-f4bd-4d64-bb13-61b3fcb0bca1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/BeneficiaryListEdit/BeneficiaryListEditApproverConfrimScreenInternational(oldValue)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1892c9b5-575f-405e-b53c-72af650f03fe</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/BeneficiaryListEdit/BeneficiaryListEditApproverConfirmScreenInhouse(frontconfrimation)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>76c08006-f45f-4526-abda-61e597c58428</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/BeneficiaryListEdit/BeneficiaryListEditApproverConfirmScreenDomestic</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7a2d6949-6d7b-447a-a976-3e5f0ab74430</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/BeneficiaryListEdit/BeneficiaryListEditApproverConfrimScreenInternational</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>87cc0010-221a-47dc-9316-ad9118147d93</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/BeneficiaryListEdit/BeneficiaryListEditApproverResultScreenInhouse(frontConfrimation)(oldValue)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ae987bdd-8bd9-45c9-891c-ce7a6e141059</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/BeneficiaryListEdit/BeneficiaryListEditApproverScreenResultDomestic(oldValue)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1ffa20a8-0093-43f0-9e15-4b79231752e6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/BeneficiaryListEdit/BeneficiaryListEditApproverResultScreenInternational(oldValue)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>80cb35a9-0fa4-49d2-a0a5-9c6e024c7c41</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/BeneficiaryListEdit/BeneficiaryListEditApproverResultScreenInhouse(frontConfrimation)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>76cb7154-b88b-47d9-915f-39bbeb52666f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/BeneficiaryListEdit/BeneficiaryListEditApproverScreenResultDomestic</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9c6b354f-5293-46e2-9057-08cfecad0ec6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/BeneficiaryListEdit/BeneficiaryListEditApproverResultScreenInternational</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
