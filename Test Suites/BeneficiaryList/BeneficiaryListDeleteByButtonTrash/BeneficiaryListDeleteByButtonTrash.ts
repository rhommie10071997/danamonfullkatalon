<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>BeneficiaryListDeleteByButtonTrash</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-11-26T16:05:06</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>9e7834c2-476f-4efd-b1e2-00d9637c35a5</testSuiteGuid>
   <testCaseLink>
      <guid>8fd9e663-3423-4917-a1fc-87a20a405323</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryListDeleteByButtonTrash/BeneficiaryListDeleteEntryScreenByButtonTrash</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>8f04b83c-e232-47cf-a3d4-865788b90115</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>80f89b56-2877-4e46-8ed9-916a5addf87e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryListDeleteByButtonTrash/BeneficiaryListDeleteApproverEntryScreenByButtonTrash</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>924c651b-5072-46a9-bb51-cd3484be3e31</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryListDeleteByButtonTrash/BeneficiaryListDeleteForLoopingByButtonTrash</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>27843d77-edae-4b56-8558-a3fd43a26535</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>b469ac89-f2c1-4774-890a-ca438a372b48</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>3f65ba7d-8ee6-4a75-a7f9-ef22bd038e09</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryListDeleteByButtonTrash/BeneficiaryListDeleteApproverConfirmScreenInhouse(frontconfrimation)ByButtonTrash</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dea101b5-11ed-4c82-82a4-5660cecea8be</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryListDeleteByButtonTrash/BeneficiaryListDeleteApproverConfirmScreenDomesticByButtonTrash</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1eab97e0-68a6-491c-a6e0-5e0591033263</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryListDeleteByButtonTrash/BeneficiaryListDeleteApproverConfrimScreenInternationalByButtonTrash</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f73b2d9f-4f3d-4e79-b203-079f20dea51d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryListDeleteByButtonTrash/BeneficiaryListDeleteApproverResultScreenInhouse(frontConfrimation)ByButtonTrash</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>759d581b-235c-4c2e-80dc-1c742b1747e6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryListDeleteByButtonTrash/BeneficiaryListDeleteApproverScreenResultDomesticByButtonTrash</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4a063bcd-5bce-4d9c-91e3-b277f3df49bb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryListDeleteByButtonTrash/BeneficiaryListDeleteApproverResultScreenInternationalByButtonTrash</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
