<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>BeneficiaryListDelete</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-12-19T13:47:35</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>f6840386-eade-4f97-8af8-3d3935decb61</testSuiteGuid>
   <testCaseLink>
      <guid>96081a94-56b0-44f6-bf0a-ced9f2d3554b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/BeneficiaryListDelete/BeneficiaryListDeleteEntryScreen</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>948c1d24-e5ea-444e-9b81-080ac152ea06</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>af3b42b3-09f9-4ded-bfea-a8ea0ea9421a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/BeneficiaryListDelete/BeneficiaryListDeleteConfrimScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8c644263-e6a1-46dc-ae1d-f8bd145b47a4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/BeneficiaryListDelete/BeneficiaryListDeleteDomesticDetailsConfrimScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>00b92f21-e51b-4419-8214-e79b0a42cde9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/BeneficiaryListDelete/BeneficiaryListDeleteInternationalDetailsConfrimScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4cad44d8-b629-4272-8064-4b14c5ce7ce1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/BeneficiaryListDelete/BeneficiaryListDeleteResultScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>06fedc10-82c9-4293-b431-d3b4a4ddfdfb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/BeneficiaryListDelete/BeneficiaryListDeleteResultScreenDomestic</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4dc8e28e-d4ee-4f64-9b45-10949f113527</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/BeneficiaryListDelete/BeneficiaryListDeleteResultScreenInternational</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>722323f7-6597-4bb7-9606-122a431f384e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/BeneficiaryListDelete/BeneficiaryListDeleteApproverEntryScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f0b815e5-e2ac-4467-9197-5461b87df0dd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/BeneficiaryListDelete/BeneficiaryListDeleteForLooping</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>02f12699-d09a-4851-9c53-e600587a9d16</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>8687652e-6310-4f8a-8996-47f811349889</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>faf997e1-e093-46ee-ae91-3c999c083182</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/BeneficiaryListDelete/BeneficiaryListDeleteApproverConfirmScreenInhouse(frontconfrimation)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c05e1405-fd95-468a-ade3-866225b2f1e5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/BeneficiaryListDelete/BeneficiaryListDeleteApproverConfirmScreenDomestic</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>aa84d249-5638-4709-b7d9-91bcc1e3f52c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/BeneficiaryListDelete/BeneficiaryListDeleteApproverConfrimScreenInternational</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>36bbe909-4d73-474c-b925-2eb65b2ccedf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/BeneficiaryListDelete/BeneficiaryListDeleteApproverResultScreenInhouse(frontConfrimation)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2ae34516-3667-4a4f-b99c-1ecc89e7c966</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/BeneficiaryListDelete/BeneficiaryListDeleteApproverScreenResultDomestic</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7812d962-b633-40d2-8a8e-bc2182270cad</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/BeneficiaryListDelete/BeneficiaryListDeleteApproverResultScreenInternational</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
