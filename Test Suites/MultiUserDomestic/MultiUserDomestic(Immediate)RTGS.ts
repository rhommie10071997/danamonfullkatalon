<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>MultiUserDomestic(Immediate)RTGS</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>a5898869-0289-49ea-86d0-e7902f9b30ad</testSuiteGuid>
   <testCaseLink>
      <guid>c43d6a82-a8d1-46f3-a1b1-d264bde87ff3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(Immediate)RTGS/MultiUser-Domestic(Immediate)RTGSEntryScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>53f864b7-f585-455b-bc62-05c014e1d3ed</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(Immediate)RTGS/MultiUser-Domestic(Immediate)RTGSEntryConfirmScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e4117dd5-a7b0-43d7-ae5b-66895d1773f3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(Immediate)RTGS/MultiUser-Domestic(Immediate)RTGSEntryResultScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cfcad11a-29ea-4eac-bc8e-b8f3286ecec1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(Immediate)RTGS/MultiUser-Domestic(Immediate)RTGSApproverEntryscreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2e161802-5a18-47ae-812a-86d46f718034</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(Immediate)RTGS/MultiUser-Domestic(Immediate)RTGSApproverConfirmScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cf3f3360-fb87-4bc0-a9de-3bcd09a624dc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(Immediate)RTGS/MultiUser-Domestic(Immediate)RTGSApproverResultScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4d5ae4bc-cbc4-40c7-8f58-a0c6e90bcda1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(Immediate)RTGS/MultiUser-Domestic(Immediate)RTGSTransactionStatusEntryReffNoScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>80b0efd1-66cf-415f-b961-1f1572cd304a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(Immediate)RTGS/MultiUser-Domestic(Immediate)RTGSTransactionStatusReffNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8955cb18-075e-42a7-b7cb-d2d89bc5dfc0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(Immediate)RTGS/MultiUser-Domestic(Immediate)RTGSTransactionStatusEntryDocNoScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7c284ec8-0a28-435a-a1aa-ddd8986c926c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(Immediate)RTGS/MultiUser-Domestic(Immediate)RTGSTransactionStatusDocNo</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
