<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>MultiUserDomestic(Repeat)RTGS</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>2f79abe8-9b52-4fba-b845-d011189d8897</testSuiteGuid>
   <testCaseLink>
      <guid>2b156449-f0b9-46c3-96d1-8ebf1fdf358f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(Repeat)RTGS/MultiUser-Domestic(Repeat)CreateEntryScreenRTGS</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0459ec99-0a81-4481-9e6a-e38a8fc23f68</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(Repeat)RTGS/MultiUser-Domestic(Repeat)CreateConfirmScreenRTGS</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c2977fe8-622d-4bb3-a249-f5998ba5d3ff</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(Repeat)RTGS/MultiUser-Domestic(Repeat)CreateResultScreenRTGS</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ddffd483-fd83-4475-b3af-68a9eb90e704</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(Repeat)RTGS/MultiUser-Domestic(Repeat)ApproverEntryScreenRTGS</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c094a14e-6e9b-4070-8744-fcb34aceacb1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(Repeat)RTGS/MultiUser-Domestic(Repeat)ApproverConfirmScreenRTGS</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>24edc3b6-e670-4c43-b9c4-9b29b27147b1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(Repeat)RTGS/MultiUser-Domestic(Repeat)ApproverResultScreenRTGS</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>17d55aa2-709d-413e-82c9-09a3d01e0885</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(Repeat)RTGS/MultiUser-Domestic(Repeat)TransactionStatusEntryScreenReffNoRTGS</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4e1cd4f9-d6d2-4e7a-a964-3abaea5781d5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(Repeat)RTGS/MultiUser-Domestic(Repeat)TransactionStatusReffNoRTGS</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5b4609d3-0f0b-4373-ac72-e63eeaf829e4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(Repeat)RTGS/MultiUser-Domestic(Repeat)TransactionStatusEntryScreenDocNoRTGS</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c560e5a4-158b-4a6b-b6fc-835ecd71bc56</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(Repeat)RTGS/MultiUser-Domestic(Repeat)TransactionStatusDocNoRTGS</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
