<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>MultiUserDomestic(FutureDate)SKN</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-02-14T22:00:15</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>4bfc2c90-3e47-4a34-afa5-8caa4c5cd451</testSuiteGuid>
   <testCaseLink>
      <guid>8c283dd2-1a47-484a-beda-439f1e037cc9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(FutureDate)SKN/MultiUser-Domestic(FutureDate)SKNEntryScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5a367721-22f1-4e07-a8ff-7d59b3c39149</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(FutureDate)SKN/MultiUser-Domestic(FutureDate)SKNEntryConfirmScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>78f6bd59-4019-4a4e-8cb4-b5cf6a7c8670</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(FutureDate)SKN/MultiUser-Domestic(FutureDate)SKNEntryResultScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8b8f40d2-8b25-4142-a023-6f23df51fd0f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(FutureDate)SKN/MultiUser-Domestic(FutureDate)SKNApproverEntryscreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>051ee34d-0c39-4134-88fa-7bd8c33af7f3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(FutureDate)SKN/MultiUser-Domestic(FutureDate)SKNApproverConfirmScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>81c5e750-0707-46a2-8189-f582f3a8926f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(FutureDate)SKN/MultiUser-Domestic(FutureDate)SKNApproverResultScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a112ca44-9f09-404e-8988-597853ecca58</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(FutureDate)SKN/MultiUser-Domestic(FutureDate)SKNTransactionStatusEntryReffNoScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9a305958-ffb6-436b-a6e8-de232abbe32f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(FutureDate)SKN/MultiUser-Domestic(FutureDate)SKNTransactionStatusReffNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ae03f0d3-d207-4681-81bb-f86ef374f78c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(FutureDate)SKN/MultiUser-Domestic(FutureDate)SKNTransactionStatusEntryDocNoScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>482340c2-456c-440f-a7b1-e68513e69131</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(FutureDate)SKN/MultiUser-Domestic(FutureDate)SKNTransactionStatusDocNo</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
