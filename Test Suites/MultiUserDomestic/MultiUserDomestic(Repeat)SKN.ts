<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>MultiUserDomestic(Repeat)SKN</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-02-18T22:15:27</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>a46f39da-3659-419a-9e6c-6f8e112f93e7</testSuiteGuid>
   <testCaseLink>
      <guid>9dbce2bc-3248-4ddf-9d7a-0afe8d5c254f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(Repeat)SKN/MultiUser-Domestic(Repeat)CreateEntryScreenSKN</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b66033b6-08f6-4acf-a36d-53bb2d193089</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(Repeat)SKN/MultiUser-Domestic(Repeat)CreateConfirmScreenSKN</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2e04e0c0-d6ae-4a38-a6e9-c49a35e4cd8c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(Repeat)SKN/MultiUser-Domestic(Repeat)CreateResultScreenSKN</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0658cf1f-85cc-4237-a491-b8ce09747f21</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(Repeat)SKN/MultiUser-Domestic(Repeat)ApproverEntryScreenSKN</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e540d267-a5ff-43c4-8fb2-c736a21f148b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(Repeat)SKN/MultiUser-Domestic(Repeat)ApproverConfirmScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a07c0560-9cf7-49c2-a3ce-51a91775e776</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(Repeat)SKN/MultiUser-Domestic(Repeat)ApproverResultScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2c125e1b-cb73-4a73-966f-6c83f4f70143</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(Repeat)SKN/MultiUser-Domestic(Repeat)TransactionStatusEntryScreenReffNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8b0ba40a-9503-48d4-93e7-142442c3b023</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(Repeat)SKN/MultiUser-Domestic(Repeat)TransactionStatusReffNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>50f27349-ed04-447d-8675-fc3e7c7cb45c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(Repeat)SKN/MultiUser-Domestic(Repeat)TransactionStatusEntryScreenDocNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6650910a-d777-421b-877d-e78c0a8f4a97</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(Repeat)SKN/MultiUser-Domestic(Repeat)TransactionStatusDocNo</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
