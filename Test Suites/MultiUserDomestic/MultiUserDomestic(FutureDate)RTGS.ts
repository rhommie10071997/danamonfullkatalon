<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>MultiUserDomestic(FutureDate)RTGS</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>5eccb99b-338d-45a3-b6f8-6380934d0d3e</testSuiteGuid>
   <testCaseLink>
      <guid>6291bb00-982f-4846-a670-f89a3d81a387</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(FutureDate)RTGSS/MultiUser-Domestic(FutureDate)RTGSEntryScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c6c1ffdd-396d-4f7e-b2ab-f9cac28156db</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(FutureDate)RTGSS/MultiUser-Domestic(FutureDate)RTGSEntryConfirmScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ad394051-ac1e-458f-a80e-5c96b17c68e6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(FutureDate)RTGSS/MultiUser-Domestic(FutureDate)RTGSEntryResultScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cbdbee07-8bf9-4809-96d7-11d17a2a740b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(FutureDate)RTGSS/MultiUser-Domestic(FutureDate)RTGSApproverEntryscreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>877e7343-6cd3-4c39-af76-d00cd3ea9980</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(FutureDate)RTGSS/MultiUser-Domestic(FutureDate)RTGSApproverConfirmScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5904f69a-7f72-41e9-b56c-6abc2e6a8d7a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(FutureDate)RTGSS/MultiUser-Domestic(FutureDate)RTGSApproverResultScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6764000f-ecc5-4926-ae6a-fcc2ed0fd703</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(FutureDate)RTGSS/MultiUser-Domestic(FutureDate)RTGSTransactionStatusEntryReffNoScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ccd9c74f-83c0-4c44-a300-c7cc198c5258</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(FutureDate)RTGSS/MultiUser-Domestic(FutureDate)RTGSTransactionStatusReffNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ee58b3cc-0a30-4b69-b899-42860a068742</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(FutureDate)RTGSS/MultiUser-Domestic(FutureDate)RTGSTransactionStatusEntryDocNoScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>729c49ba-7d8b-4d63-b7a7-149d454ea6e6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(FutureDate)RTGSS/MultiUser-Domestic(FutureDate)RTGSTransactionStatusDocNo</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
