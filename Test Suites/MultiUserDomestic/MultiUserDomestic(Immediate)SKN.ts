<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>MultiUserDomestic(Immediate)SKN</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>d79cd43b-f97d-4cae-8f45-9bb5e27981c1</testSuiteGuid>
   <testCaseLink>
      <guid>8d9867c4-9494-450f-8050-5f92b8e5b97c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(Immediate)SKN/MultiUser-Domestic(Immediate)SKNEntry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f5edf967-e526-4595-9487-d4a036d035f7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(Immediate)SKN/MultiUser-Domestic(Immediate)SKNConfirmScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>df3251d8-70cc-473b-bf2b-8c986d30f10d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(Immediate)SKN/MultiUser-Domestic(Immediate)SKNWaitingForApprovalScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d532d32e-36ca-4951-b19a-55a984f05dea</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(Immediate)SKN/MultiUser-Domestic(Immediate)SKNApproverConfirmScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>07787c4f-92f5-43c0-8f63-b5c78b18ccd0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(Immediate)SKN/MultiUser-Domestic(Immediate)SKNApproverResultScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>049f80bd-3488-4a34-bf28-2b9f9c511685</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(Immediate)SKN/MultiUser-Domestic(Immediate)SKNTransactionStatusReffNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>10703356-488a-43dc-8e98-f8baa04e1942</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/MultiUser-Domestic(Immediate)SKN/MultiUser-Domestic(Immediate)SKNTransactionStatusDocNo</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
