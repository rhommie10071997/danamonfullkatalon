<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>SingleUserDomestic(Immediate)SKN</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-01-23T18:14:02</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>e8308c29-a509-4f9c-80ab-cefd93662dea</testSuiteGuid>
   <testCaseLink>
      <guid>f40bb1bf-6e82-41b0-90c3-0a48811d5bf0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-Domestic(Immediate)SKN/SingleUserPayroll-DomesticSKN(Immediate)CreateEntryScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f64350e0-75f5-49c7-8bd8-6d2b68241773</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-Domestic(Immediate)SKN/SingleUserPayroll-DomesticSKN(Immediate)BucketListLoop</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>32e02e41-3c65-422e-8675-69f66e3af7c7</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>4c183483-4db3-4611-a6e2-a03c01bcff59</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>ca4717e0-bdff-4b4f-bee6-11caf246411c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-Domestic(Immediate)SKN/SingleUserPayroll-DomesticSKN(Immediate)BucketDetails</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>f79dee8e-dcd1-4bfe-9d2f-0e24b97c620d</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>0134e463-33be-4035-9b6c-85a76c541298</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-Domestic(Immediate)SKN/SingleUserPayroll-DomesticSKN(Immediate)BucketConfirm</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>1eb8a297-c0af-460a-98cb-b33eb6d8eb22</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>508eaa96-8644-44b9-9b3e-bc1658c645b5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-Domestic(Immediate)SKN/SingleUserPayroll-DomesticSKN(Immediate)CreateResultScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d63a55da-a0ac-4294-b59f-23363a5bf821</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-Domestic(Immediate)SKN/SingleUserPayroll-DomesticSKN(Immediate)TransactionStatusEntryScreenReffNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>abf93b3f-9051-4050-9e0d-b75910b9d26f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-Domestic(Immediate)SKN/SingleUserPayroll-DomesticSKN(Immediate)TransactionStatusReffNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cb7e26e4-9fa5-44e9-bb21-f76dd3af97af</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-Domestic(Immediate)SKN/SingleUserPayroll-DomesticSKN(Immediate)TransactionStatusEntryScreenDocNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>44c2493d-017c-49c0-95c8-74191d54250e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-Domestic(Immediate)SKN/SingleUserPayroll-DomesticSKN(Immediate)TransactionStatusDocNo</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
