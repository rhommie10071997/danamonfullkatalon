<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>SingleUserDomestic(FutureDate)SKN</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-01-24T16:03:33</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>db9cd5b2-218f-474a-959d-1c72a0f30a3a</testSuiteGuid>
   <testCaseLink>
      <guid>2cf3b1b5-b5a6-42bd-b1be-a92bf60ad8ae</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-Domestic(FutureDate)SKN/SingleUserPayroll-DomesticSKN(FutureDate)CreateEntryScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4706852c-e02e-47f6-a17a-4995ec293a71</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-Domestic(FutureDate)SKN/SingleUserPayroll-DomesticSKN(FutureDate)BucketListLoop</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>f3e783d2-5cd5-4ca4-a25f-eccbb00eb74a</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>566c343a-91e9-4c61-a4dd-1dfb7197e7d7</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>4d7f1661-1426-41d0-80c4-28bfb1bd647f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-Domestic(FutureDate)SKN/SingleUserPayroll-DomesticSKN(FutureDate)BucketDetails</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>30f59c6f-33e5-4fe7-935b-5f26f3d79af2</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>5d354e54-4bdc-4d72-9039-3aa696ed0bed</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-Domestic(FutureDate)SKN/SingleUserPayroll-DomesticSKN(FutureDate)BucketConfirm</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>22e02262-9c78-4876-8972-27108342fa40</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>25f8fa03-98d5-4f24-ab7e-e63e5eab17d5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-Domestic(FutureDate)SKN/SingleUserPayroll-DomesticSKN(FutureDate)CreateResultScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6b35dfb0-c5ee-4622-9259-34f864528d4e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-Domestic(FutureDate)SKN/SingleUserPayroll-DomesticSKN(FutureDate)TransactionStatusEntryScreenReffNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>53d7ec22-f4f7-4d28-89ac-26baf1bcc583</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-Domestic(FutureDate)SKN/SingleUserPayroll-DomesticSKN(FutureDate)TransactionStatusReffNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0e637cbe-5b79-4c97-95cf-9f3518147996</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-Domestic(FutureDate)SKN/SingleUserPayroll-DomesticSKN(FutureDate)TransactionStatusEntryScreenDocNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ec53967a-456f-43f8-8900-37336ee69dfe</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/SingleUserPayroll-Domestic(FutureDate)SKN/SingleUserPayroll-DomesticSKN(FutureDate)TransactionStatusDocNo</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
