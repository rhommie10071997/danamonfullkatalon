<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Inhouse - Future</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>8b9ae8bb-5122-4599-bc23-c953477f09f5</testSuiteGuid>
   <testCaseLink>
      <guid>ab90ad07-228c-4586-a592-0d3974be2439</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Multi/Bulk Upload Inhouse/Future/Create/New Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>10afc6b3-3268-4011-bbfe-1a32e4940abe</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Multi/Bulk Upload Inhouse/Future/Create/test loop</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>3373cc70-30b7-4887-8a44-a29d36827ee8</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>1749c388-c233-4297-baff-dddb5849c571</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>d28d55e0-7bef-4016-a6e6-cf0b9d3931f7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Multi/Bulk Upload Inhouse/Future/Create/Bucket Detail</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6e82e5ce-2179-4dfd-9c48-7a9ed9f32e70</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Multi/Bulk Upload Inhouse/Future/Create/Bucket Confirm</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b9e90539-3592-4846-bd42-bccab6582210</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Multi/Bulk Upload Inhouse/Future/Create/Result Screen Maker</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>90341257-b15a-4dfb-bb27-cdc261eccbc4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Multi/Bulk Upload Inhouse/Future/Approve/Entry Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>07896b6a-7965-4efe-903d-0d73c919c089</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Multi/Bulk Upload Inhouse/Future/Approve/Confirm Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3721efe6-0d24-4a08-b0fd-adda0b456aba</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Multi/Bulk Upload Inhouse/Future/Approve/Result Screen Approver</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0db27eb3-40fb-4434-afc3-0ca33c6c55e8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Multi/Bulk Upload Inhouse/Future/Transaction Status/Transaction Status - Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>055ce3ce-8391-400c-b448-a147da732ff9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Multi/Bulk Upload Inhouse/Future/Transaction Status/TS - Ref No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>522d2c6a-32ea-4c5a-81a5-bc9886cb546c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Multi/Bulk Upload Inhouse/Future/Transaction Status/TS - Doc No</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
