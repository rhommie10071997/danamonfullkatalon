<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Skn - Future</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>a41f2a7f-2c9b-43cd-b1cc-35b1afed91e9</testSuiteGuid>
   <testCaseLink>
      <guid>1478707f-aeb4-44f7-97f3-9b9b1619f629</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Multi/SKN/Future Date/Create/New Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d8f9854e-c22e-4c22-aea1-d0cadf0ff3c1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Multi/SKN/Future Date/Create/test loop</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>1cf2e003-ea4e-4b14-bf14-2a370d14db64</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>2b0da4f8-d69e-432b-b319-1f225e7058cf</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>e2cd2c2d-032d-426d-909a-4f49fd76d890</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Multi/SKN/Future Date/Create/Bucket Detail</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1ed05886-32eb-4e58-8a76-9be3184e1fa8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Multi/SKN/Future Date/Create/Bucket Confirm</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b286870d-c088-4c44-8d9b-c91d5f1c729c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Multi/SKN/Future Date/Create/Result Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4f534e16-4e51-4e8e-9255-dce487180bb1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Multi/SKN/Future Date/Approve/Entry Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>616e4ae0-921e-4564-9b4a-ba9e578342bd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Multi/SKN/Future Date/Approve/Confirm Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dadaed6a-1d4c-49fa-a054-bf5b6bcf2b00</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Multi/SKN/Future Date/Approve/Result Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e0123f86-b2b5-457b-9e1e-ded08468ea16</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Multi/SKN/Future Date/Transaction Status/Transaction Status - Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1f72117a-009d-474b-a5e9-1cd9b8f4b4a0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Multi/SKN/Future Date/Transaction Status/TS - Ref No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>793c11dc-26ae-4023-bfc6-68e9ae0463ec</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Multi/SKN/Future Date/Transaction Status/TS - Doc No</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
