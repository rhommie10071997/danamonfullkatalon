<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Rtgs - Future</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>e2491fb7-f004-4a7c-89aa-10b34b25e2ed</testSuiteGuid>
   <testCaseLink>
      <guid>a1e1981c-18d6-454b-91f7-9e692bc29fe3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Multi/RTGS/Future/Create/New Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5d8e70bf-fd20-46c8-9486-197e54b7e444</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Multi/RTGS/Future/Create/test loop</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>af290e18-4d9a-49bf-8f2c-cdef0870ce6a</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>b5337032-dbeb-48a9-bcc1-b770a201e9db</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>3fe6c450-65d8-4e43-b1df-6c35b5087fc9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Multi/RTGS/Future/Create/Bucket Detail</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a9a430d6-00ad-4d82-af50-f4e578eeaa2a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Multi/RTGS/Future/Create/Bucket Confirm</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>472f026e-267a-4519-b41e-a6ac8ce4c650</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Multi/RTGS/Future/Create/Result Screen Maker</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>892092ae-b9a5-4632-bccc-780038005fec</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Multi/RTGS/Future/Approve/Entry Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9cbc15a8-a8b4-4849-92d7-8b8030ce38c2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Multi/RTGS/Future/Approve/Confirm Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b691f510-9183-4ea3-af1f-bbf14bca6183</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Multi/RTGS/Future/Approve/Result Screen Approver</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e9759ef5-63e6-441c-b3fe-44b06294fdff</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Multi/RTGS/Future/Transaction Status/Transaction Status - Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7aba2d63-0f48-4644-a063-ce61c60d4ed0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Multi/RTGS/Future/Transaction Status/TS - Ref No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d81a70cc-e805-4f54-9eba-cfce4c3b2be2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Multi/RTGS/Future/Transaction Status/TS - Doc No</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
