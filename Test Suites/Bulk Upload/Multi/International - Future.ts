<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>International - Future</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>b59a167e-9f99-4c5e-a35a-cb1731bd965b</testSuiteGuid>
   <testCaseLink>
      <guid>7a002191-6d72-4d00-aa86-18ac90a56389</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Multi/International/Future/Create/New Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>94ed5c75-466e-4d45-a01f-ec96926943c6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Multi/International/Future/Create/test loop</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>1fe9c848-2b7f-4ec5-ab10-aa16ca7de4f5</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>b441262f-d3b7-44c3-92f0-634b0efb353c</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>a836b1e4-8cc7-47b8-8677-5dd5e6a28e56</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Multi/International/Future/Create/Bucket Detail</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>651eece9-eecc-4fef-8e47-70d77907b20a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Multi/International/Future/Create/Bucket Confirm</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>07e59da5-3f04-4b3d-9147-60ab6e4ca3af</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Multi/International/Future/Create/Result Screen Maker</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>069d5c24-dab2-482d-9391-3bed9530834c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Multi/International/Future/Approve/Entry Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7c55e4b0-4686-48df-baa6-ec80abc04cc4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Multi/International/Future/Approve/Confirm Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c3676324-e6fc-40b1-89b1-2a8a5b205ccf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Multi/International/Future/Approve/Result Screen Approver</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cc5420da-62c7-4239-a796-9040bb144480</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Multi/International/Future/Transaction Status/Transaction Status - Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e61bbbe2-9611-45a2-affc-544a9768fe7e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Multi/International/Future/Transaction Status/TS - Ref No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>594e9133-6333-4265-a9c9-6a43133ecd54</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Multi/International/Future/Transaction Status/TS - Doc No</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
