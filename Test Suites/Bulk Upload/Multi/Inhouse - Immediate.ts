<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Inhouse - Immediate</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>5e9fc46a-2eb2-44d7-8964-f4157e1b4a06</testSuiteGuid>
   <testCaseLink>
      <guid>ea07c2ae-d043-4d2d-a984-cdc1ca8a8841</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Multi/Bulk Upload Inhouse/Immediate/Create/New Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6dd12c46-949d-4a42-96c0-42a08ccd103a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Multi/Bulk Upload Inhouse/Immediate/Create/test loop</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>b68022f5-722f-4315-9f92-44f0db88660b</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>8ce6effb-8540-4b3f-8656-4a436020e343</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>25b8fc26-67a1-4c4e-b943-2d9fcc9df8af</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Multi/Bulk Upload Inhouse/Immediate/Create/Bucket Detail</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5126b06c-247a-4361-bff6-edda77570b5a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Multi/Bulk Upload Inhouse/Immediate/Create/Bucket Confirm</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d74929b4-3406-49da-b728-9fd4a30b331e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Multi/Bulk Upload Inhouse/Immediate/Create/Result Screen Maker</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d1a99df7-8a55-470d-8ecd-354f5f48f072</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Multi/Bulk Upload Inhouse/Immediate/Approve/Entry Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>29bb907d-1a73-42a7-a562-2a80b88a2eb9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Multi/Bulk Upload Inhouse/Immediate/Approve/Confirm Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>51650321-ab9b-43b0-be2f-c9a5ecf46f54</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Multi/Bulk Upload Inhouse/Immediate/Approve/Result Screen Approver</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>40822165-8b4c-49bb-a206-ef5db329899e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Multi/Bulk Upload Inhouse/Immediate/Transaction Status/Transaction Status - Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6c8b4ce9-6953-4ba4-842d-599a2295b6a0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Multi/Bulk Upload Inhouse/Immediate/Transaction Status/TS - Ref No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6d5c48a4-8432-4c46-bf08-02cb44063631</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Multi/Bulk Upload Inhouse/Immediate/Transaction Status/TS - Doc No</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
