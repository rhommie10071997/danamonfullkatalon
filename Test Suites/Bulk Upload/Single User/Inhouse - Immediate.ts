<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Inhouse - Immediate</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>f0381946-0811-42a7-9498-d4e513b59655</testSuiteGuid>
   <testCaseLink>
      <guid>04fe757f-dfab-4334-824c-6bcfbc38e68f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Single/Bulk Upload Inhouse/Immediate/Create/New Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>de771b05-153f-46f9-8d06-55094c1dcabb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Single/Bulk Upload Inhouse/Immediate/Create/test loop</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>d9da9891-3442-4b45-ac26-edb939e02b34</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>7c41ed76-a8b1-4ac4-83a5-b9389520210d</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>52899134-8aaf-447a-b553-93e65fdc0b32</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Single/Bulk Upload Inhouse/Immediate/Create/Bucket Detail</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>080eb857-7b6e-4e48-af50-6dd3360c5c6b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Single/Bulk Upload Inhouse/Immediate/Create/Bucket Confirm</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f809b01f-e901-4f03-9f2a-159a7e366a6e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Single/Bulk Upload Inhouse/Immediate/Create/Result Screen Maker</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5b4496c6-709d-4adf-94f2-aacd83a5ac4c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Single/Bulk Upload Inhouse/Immediate/Transaction Status/Transaction Status - Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>db08e9c7-8d53-40d1-831b-af742130bc90</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Single/Bulk Upload Inhouse/Immediate/Transaction Status/TS - Ref No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1e6ed78d-c406-437d-84db-3d2c18504685</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Single/Bulk Upload Inhouse/Immediate/Transaction Status/TS - Doc No</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
