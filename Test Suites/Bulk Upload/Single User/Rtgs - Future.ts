<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Rtgs - Future</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>f594f6a1-ba36-4ed1-aad3-701e0e8c600f</testSuiteGuid>
   <testCaseLink>
      <guid>42c36d4d-b310-4bb7-a98a-2faff3979380</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Single/RTGS/Future/Create/New Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cc3f896e-e4c2-4d8e-919b-ef68e051ae65</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Single/RTGS/Future/Create/test loop</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>7fccacb6-4a20-419b-8614-b4e82505621f</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>cfad77b5-208a-4c8b-912f-6cfae904ccf5</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>a2679685-b510-483f-930f-895d301891ef</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Single/RTGS/Future/Create/Bucket Detail</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2d7ae2b2-f17f-4ab2-804a-d85c9b98eccc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Single/RTGS/Future/Create/Bucket Confirm</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6de9f72b-2427-4419-ae19-0c9f831002f1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Single/RTGS/Future/Create/Result Screen Maker</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>497a5283-3b64-4138-a89c-53037821b554</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Single/RTGS/Future/Transaction Status/Transaction Status - Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e0828c55-795c-49a4-a04e-8a1253cdeb1f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Single/RTGS/Future/Transaction Status/TS - Ref No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c26e36e5-204e-4dff-95db-e0a39c14e120</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Single/RTGS/Future/Transaction Status/TS - Doc No</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
