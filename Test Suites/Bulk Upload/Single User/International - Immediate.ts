<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>International - Immediate</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>5fe7288f-a8f0-4934-bee5-a0a85eb9d3e3</testSuiteGuid>
   <testCaseLink>
      <guid>1a9867a1-4b9b-4430-b9b1-ee165690172d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Single/International/Immediate/Create/New Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>aa51da0f-d8bb-4323-9a48-e21f8675a7cd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Single/International/Immediate/Create/test loop</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>68a6e771-2d92-4ede-bad3-26d9cf38dd46</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>289a4c8f-fb6d-4073-9c55-f4721f4d601f</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>4f7864ca-c73d-4036-8249-aac1d02e1ad2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Single/International/Immediate/Create/Bucket Detail</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bba4c4f9-4b14-438c-b0cd-626108a1d87f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Single/International/Immediate/Create/Bucket Confirm</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>86ef2d47-b725-4b47-894f-9ca8cc7ccaa8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Single/International/Immediate/Create/Result Screen Maker</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f4954f48-7532-4f76-a3ce-015106eb8153</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Single/International/Immediate/Transaction Status/Transaction Status - Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>808f91e2-7eaf-4505-b2f8-7ab8421dfe8d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Single/International/Immediate/Transaction Status/TS - Ref No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4c595145-d894-4a95-891c-12061ad075e6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Single/International/Immediate/Transaction Status/TS - Doc No</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
