<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Skn - Immediate</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>17a2426d-11ff-4854-869c-37cb57c82e78</testSuiteGuid>
   <testCaseLink>
      <guid>f8ee5c22-06af-4299-b19b-8075ef6d4760</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Single/Skn/Immediate/Create/New Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ef6abe5c-3694-4016-88ac-16910b0fd74f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Single/Skn/Immediate/Create/test loop</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>5fe21c24-c483-47ca-b8cd-3909b70c267a</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>1a671fe9-a217-42d7-bcf2-0bb87d115803</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>12ba2656-101e-4e18-b01b-096d6c4ace0e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Single/Skn/Immediate/Create/Bucket Detail</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d831a387-533a-4706-8d76-b74faa84f3b0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Single/Skn/Immediate/Create/Bucket Confirm</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>02d20aad-3949-49a1-a2d5-fc2282122d64</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Single/Skn/Immediate/Create/Result Screen Maker</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dba42c5c-782e-4841-aebf-746826db8da3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Single/Skn/Immediate/Transaction Status/Transaction Status - Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8702bba3-356e-4a81-8b20-9c25b56940d4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Single/Skn/Immediate/Transaction Status/TS - Ref No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2def3916-9275-42c9-9d4f-7b211b8e6b92</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bulk Upload/Single/Skn/Immediate/Transaction Status/TS - Doc No</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
