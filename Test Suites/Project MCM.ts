<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Project MCM</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-09-06T16:37:28</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>ff1b3e6e-093c-4a69-b9a5-cca44fa2184f</testSuiteGuid>
   <testCaseLink>
      <guid>dc49c04c-5981-439d-84f7-547a346f3153</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/url2</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e61d5caa-a2c5-4db5-8476-b9f95b51a380</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/url</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8a60fade-a653-4747-982d-06c8cd7359a4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/BulkTransfers/InternationalSingleImme/bulkTransferInternationalSingleImme(Maker)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>35859383-b490-4c2a-836f-ca542ade2b31</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/BulkTransfers/InhouseMultiSpecific/bulkTransferInhouseSingleSpecific</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>38bae472-d7e5-4272-95bc-2b8cce312962</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/BulkTransfers/InhouseMultiImme/bulkTransferInhouseMultiImme(Maker)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7f782dec-64ca-4636-bb94-e1484ebf7337</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/BulkTransfers/RtgsSingleImme/bulkTransferRtgsSingleImme(Maker)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>af75fd71-59fa-4412-9f02-793f86dab070</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/BulkTransfers/Skn.rrgSingleImme/bulkTransferSkn.RrgSingleImme</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f27f57d9-57b5-48e0-a1b4-19024d3816c5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/BulkTransfers/RtgsSingleFuture/bulkTransferRtgsSingleFuture(Maker)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3dae9bc6-bc7d-46c8-a5a1-869785601996</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/BulkTransfers/InternationalMultiImme/bulkTransferInternationalMultiImme(Maker)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>95de84cc-33df-4016-96c7-f5109f457b4d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/BulkTransfers/RtgsSingleImme/testingBray</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d89341f4-1aba-4edc-8979-eaefaf7830c3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/BulkTransfers/InhouseMultiImme/bulkTransferInhouseMultiImme(TransInquiry)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6abae175-99db-467f-bef1-776e27d1436e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TransactionInquiry/InquiryChecker</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0f687a88-6098-408c-a318-fcf79f9ef254</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/BulkTransfers/InternationalMultiImme/bulkTransferInternationalMultiImme(transStatus)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a0b44614-b312-45fe-b13a-690db0701ac7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/BulkTransfers/InternationalSingleImme/InquiryChecker</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>a126ac79-8ebf-4552-aed0-2c1f4240b7c5</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>2a180aa5-8d28-42d1-9832-99223c47be56</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>1a485cce-5849-4f3b-b5d4-d2f448cca965</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>276d0ad3-9ab6-443c-a5de-f90749979d4e</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>adf35049-c5a7-4540-8ff2-3cd86ec49eff</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>a735ffd4-0888-4f9b-8a24-ef5d9afff8d5</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>b61eafd5-48e6-42eb-abca-1ae32e868520</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BulkTransfers/InhouseSingleImme/bulkTransferInhouseSingleImme(Maker)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b5ad632c-16b8-4929-8ce9-3edfdc929a05</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BulkTransfers/InhouseSingleImme/bulkTransferInhouseSingleImme(TransStatus)</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
