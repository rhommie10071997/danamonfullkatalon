<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>MultiBIllingGenerateMultiImme</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-01-31T11:33:06</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>f99247be-0904-43f7-8ba7-ad99b0ceebcc</testSuiteGuid>
   <testCaseLink>
      <guid>cdf287a8-e49b-4853-bc57-6400933cac74</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/url</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>35b97f80-5aaf-4b34-9560-b01366c9a91d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/multiBilling/GenerateMultiImme/MultiBillingGenerateMultiImme(Maker)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b19534b8-f987-4727-88eb-68ee4c31bd5b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/multiBilling/GenerateMultiImme/MultiBillingGenerateMultiImme(Maker.Result)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4e120560-6d31-407d-a32c-9ac671c875ed</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/pendingTask/pendingTask</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>39e0ec7b-58bc-460e-b730-e12c1765c722</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/multiBilling/GenerateMultiImme/MultiBillingGenerateMultiImme(Approver)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a80db421-2d93-45df-aec7-d6c3501f14bb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/multiBilling/GenerateMultiImme/MultiBillingGenerateMultiImme(Approver.Result)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dab1e9d7-36ca-451c-9f27-acda01ff848b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/transStatus/transStatus</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f04e6e25-d8c9-439b-aced-0f521757d4fc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/multiBilling/GenerateMultiImme/MultiBillingGenerateMultiImme(transStatus)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ec2e22fc-211f-42d8-b375-464f8343d660</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/multiBilling/GenerateMultiImme/MultiBillingGenerateMultiImme(transStatus_2)</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
