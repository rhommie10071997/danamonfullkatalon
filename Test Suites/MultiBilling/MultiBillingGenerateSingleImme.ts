<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>MultiBillingGenerateSingleImme</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-01-31T15:12:01</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>21c1cc2d-ba98-4371-8992-df060f29f87e</testSuiteGuid>
   <testCaseLink>
      <guid>573707a7-c864-47b7-a344-503e4e79c5db</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/url</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2a902fb9-72b8-4a06-8b2a-76d5288fe074</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/multiBilling/GenerateSingleImme/MultiBillingGenerateSingleImme(Maker)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0bf8b214-28a3-4b98-a43c-859239ee39ea</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/multiBilling/GenerateSingleImme/MultiBillingGenerateSingleImme(Maker.Result)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>462460d1-19f7-4448-93bf-2683c7d2906c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/transStatus/transStatus</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c407ce0a-c924-46d2-861e-f38a4b8c9895</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/multiBilling/GenerateSingleImme/MultiBillingGenerateSingleImme(transStatus)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1101792a-172a-4c50-a621-ff6f3a45a28d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/multiBilling/GenerateSingleImme/MultiBillingGenerateSingleImme(transStatus_2)</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
