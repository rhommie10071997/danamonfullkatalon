<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>MultiBillingGenerateMultiFuture</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-01-31T14:32:04</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>231ff985-98f3-4116-9aa0-021d375c128b</testSuiteGuid>
   <testCaseLink>
      <guid>68d68ac0-891b-4f5b-a889-a3353623cbe2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/url</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>686bec12-9b89-4aa8-8dee-2e1f37c60ad2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/multiBilling/GenerateMultiFuture/MultiBillingGenerateMultiFuture(Maker)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>12bd5381-53d7-4765-b6a3-bfd186fd60aa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/multiBilling/GenerateMultiFuture/MultiBillingGenerateMultiFuture(Maker.Result)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>59956104-4a4c-409a-af39-2b91d829e505</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/pendingTask/pendingTask</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>58b138b6-6876-4cd9-91de-bd00a1c8556c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/multiBilling/GenerateMultiFuture/MultiBillingGenerateMultiFuture(Approver)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2a05768b-c508-4f65-a331-99c2dc9c2c27</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/multiBilling/GenerateMultiFuture/MultiBillingGenerateMultiFuture(Approver.Result)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e41fdddb-1920-4a43-af10-e14bce091661</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/transStatus/transStatus</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8a81f0df-77ad-47bc-ae29-b9b3819f5187</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/multiBilling/GenerateMultiFuture/MultiBillingGenerateMultiFuture(transStatus)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>96e59179-53bb-43e1-a8b2-336cf9ac47d2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/multiBilling/GenerateMultiFuture/MultiBillingGenerateMultiFuture(transStatus_2)</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
