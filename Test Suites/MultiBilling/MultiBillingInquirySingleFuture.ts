<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>MultiBillingInquirySingleFuture</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-01-30T16:19:46</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>bf2a4b17-59ad-4cb4-8dbf-9734d0a1b632</testSuiteGuid>
   <testCaseLink>
      <guid>5e903457-0951-420b-8d87-e5b93121f1b3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/url</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>13e6f8a8-6b96-4993-86ed-6280ddc86ddd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/multiBilling/InquirySingleFuture/MultiBillingInquirySingleFuture(Maker)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>90ce6768-8fd3-4311-8ac7-e144643b16a6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/multiBilling/InquirySingleFuture/MultiBillingInquirySingleFuture(Maker.Result)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4afadef1-f6ec-4a52-b2fe-3a16ccaecd6c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/transStatus/transStatus</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2a36ed3b-0bb2-4e81-b0ec-5c07df26d445</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/multiBilling/InquirySingleFuture/MultiBillingInquirySingleFuture(transStatus)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a9211077-9118-4fec-abe6-81f48a6d9f37</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/multiBilling/InquirySingleFuture/MultiBillingInquirySingleFuture(transStatus_2)</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
