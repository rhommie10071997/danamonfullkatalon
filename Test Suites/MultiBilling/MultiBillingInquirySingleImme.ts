<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>MultiBillingInquirySingleImme</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-01-30T15:39:21</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>92436d23-74e8-4db4-aa7c-04551a725df1</testSuiteGuid>
   <testCaseLink>
      <guid>06900972-d75b-4e25-b170-78070a926a58</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/url</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e83c4222-eaa0-45a9-bdd5-5c616c954069</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/multiBilling/InquirySingleImme/MultiBillingInquirySingleImme(Maker)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5a1b1a26-1c71-4023-9313-dd8b9cb24592</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/multiBilling/InquirySingleImme/MultiBillingInquirySingleImme(Maker.Result)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>507c55ba-6289-4fd2-9054-75bba7f833a2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/transStatus/transStatus</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1cf42fa9-a1d0-4fe3-a8da-e27f38805aa6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/multiBilling/InquirySingleImme/MultiBillingInquirySingleImme(transStatus)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5c36954d-c4aa-4f2d-bfbf-13bf1da6745e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/multiBilling/InquirySingleImme/MultiBillingInquirySingleImme(transStatus_2)</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
