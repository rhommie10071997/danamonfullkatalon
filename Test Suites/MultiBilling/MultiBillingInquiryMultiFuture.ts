<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>MultiBillingInquiryMultiFuture</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-01-30T15:01:32</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>3af08b37-1eb7-4db2-b155-69f608e9b02e</testSuiteGuid>
   <testCaseLink>
      <guid>f0c786b9-ed99-489f-b495-106484490c95</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/url</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e4b9d1e1-3c3c-4606-8cc1-1c9403a0aff8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/multiBilling/InquiryMultiFuture/MultiBillingInquiryMultiFuture(Maker)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>33df835e-49a9-4b51-968b-62ab8097fee0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/multiBilling/InquiryMultiFuture/MultiBillingInquiryMultiFuture(Maker.Result)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a4a43ffd-fe7e-40dc-838a-88dc8d3e726d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/pendingTask/pendingTask</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5c2901fe-7594-4ef0-b68c-cb1825d317a0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/multiBilling/InquiryMultiFuture/MultiBillingInquiryMultiFuture(Approver)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a6f0e05f-0c7a-4e88-84f3-e8c89323a2a2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/multiBilling/InquiryMultiFuture/MultiBillingInquiryMultiFuture(Approver.Result)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3a92ffe4-8d4d-4af8-bd5f-58510a1bd76b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/transStatus/transStatus</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>097bf60f-018f-43a9-b583-45c69bdcb819</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/multiBilling/InquiryMultiFuture/MultiBillingInquiryMultiFuture(transStatus)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5735b106-6135-471d-a3b6-993de3964e25</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/multiBilling/InquiryMultiFuture/MultiBillingInquiryMultiFuture(transStatus_2)</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
