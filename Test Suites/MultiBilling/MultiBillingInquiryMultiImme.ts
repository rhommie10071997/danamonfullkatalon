<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>MultiBillingInquiryMultiImme</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-01-31T15:50:40</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>742d4605-6b1b-493e-af6c-81e7604bd327</testSuiteGuid>
   <testCaseLink>
      <guid>65b0ecd0-0ebf-4c1b-9f55-b0e796dfc0ec</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/url</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0b2c7a90-43f2-4ad5-9151-a8d7552755e3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/multiBilling/InquiryMultiImme/MultiBillingInquiryMultiImme(Maker)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d93e7c91-e5e1-4c04-abf8-ca6f89504eda</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/multiBilling/InquiryMultiImme/MultiBillingInquiryMultiImme(Maker.Result)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0b32bbd0-254f-4285-81dd-a11c4b1e7221</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/pendingTask/pendingTask</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>835871b1-d3f0-4361-9800-95c4fe559a71</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/multiBilling/InquiryMultiImme/MultiBillingInquiryMultiImme(Approver)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e5b01f64-26ce-46e9-953c-7bdd5f716da8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/multiBilling/InquiryMultiImme/MultiBillingInquiryMultiImme(Approver.Result)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cf27c125-5816-4718-90e3-4d84f4a070c2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/transStatus/transStatus</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6a4acfb7-9a57-444b-b541-60448cb55c11</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/multiBilling/InquiryMultiImme/MultiBillingInquiryMultiImme(transStatus)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d005a245-033f-4db2-8b9e-060523368644</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/multiBilling/InquiryMultiImme/MultiBillingInquiryMultiImme(transStatus_2)</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
