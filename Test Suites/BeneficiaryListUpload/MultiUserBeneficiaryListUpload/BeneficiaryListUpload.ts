<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>BeneficiaryListUpload</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-01-28T11:25:29</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>c26d5402-5817-4a7f-b051-ccc6994a6109</testSuiteGuid>
   <testCaseLink>
      <guid>d0c6e80e-968c-43a5-a5a8-3495b1cc5092</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryListUpload/MultiUserBeneficiaryListUpload/MultiUserBeneficiaryListUploadEntryScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ba8de561-6370-41f8-92f3-4b7269f68869</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryListUpload/MultiUserBeneficiaryListUpload/MultiUserBeneficiaryListUploadBucketListLoop</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>b1336799-f8a3-40bb-9079-6e132c1244c4</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>fe27aa04-85e8-4b9a-b9ad-c585e4cea59d</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>9441a689-bb65-4a56-af50-7f1061ab650c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryListUpload/MultiUserBeneficiaryListUpload/MultiUserBeneficiaryListUploadBucketDetails</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>c521dcb8-1a73-4244-a5bc-2faa5a7020ef</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>60b955f6-0d90-4f52-ab99-f079bf6a45bd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryListUpload/MultiUserBeneficiaryListUpload/MultiUserBeneficiaryListUploadBucketConfirm(MyBank)</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>bae7484e-80aa-4c83-98e0-31dab2c902ea</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>d3ad3e20-b567-416c-a779-1ae03f9cd9aa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryListUpload/MultiUserBeneficiaryListUpload/MultiUserBeneficiaryListUploadBucketConfirm(DomesticBank)</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>a873e667-1d50-4af2-b990-857520782aa0</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>86c2ea50-604d-4bca-85d3-6c567056890c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryListUpload/MultiUserBeneficiaryListUpload/MultiUserBeneficiaryListUploadBucketConfirm(InternationalBank)</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>bd2bcd92-8b7e-4e3e-a5cc-912372a347c8</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>dc14ac47-1a65-4203-9618-50d72f34e159</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryListUpload/MultiUserBeneficiaryListUpload/MultiUserBeneficiaryListUploadBucketResult(MyBank)</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>5ea70b46-9138-4c55-9a51-1ead65337ad2</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>1937b167-e044-4fe2-8d83-47ccbf8922c6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryListUpload/MultiUserBeneficiaryListUpload/MultiUserBeneficiaryListUploadBucketResult(DomesticBank)</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>ba86e82d-3bfa-4f6d-b358-cfe3fd388271</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>4ef9a827-0b7d-4148-8622-03d9d3b6a69c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryListUpload/MultiUserBeneficiaryListUpload/MultiUserBeneficiaryListUploadBucketResult(InternationalBank)</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>05d185b6-5d8c-4d87-9387-1491812169ad</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>28788536-ab67-4971-9df9-477280fab069</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryListUpload/MultiUserBeneficiaryListUpload/MultiUserBeneficiaryListUploadApproverEntryScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4e6f8d85-82da-4569-a479-f7618e625b2f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryListUpload/MultiUserBeneficiaryListUpload/MultiUserBeneficiaryListUploadApprover(MyBank)</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>9c2880fb-c59b-4451-853a-5a40f710be9b</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>be358937-62f9-4231-a6fd-ccb5ceda7c73</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryListUpload/MultiUserBeneficiaryListUpload/MultiUserBeneficiaryListUploadApprover(DomesticBank)</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>70ebe15c-9c47-4add-8e51-40cefcd54eff</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>6f94b4fa-0f9c-461d-b2ca-90f306932153</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryListUpload/MultiUserBeneficiaryListUpload/MultiUserBeneficiaryListUploadApprover(InternationalBank)</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>6c364cde-2179-43f3-81f4-f55ef24458bc</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>6254289e-3311-4818-adc7-424c9ca7736f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryListUpload/MultiUserBeneficiaryListUpload/MultiUserBeneficiaryListUploadApproverResult(MyBank)</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>c2411b91-89e6-4deb-8ad7-533e14f843c0</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>16532d1b-a392-4286-b05e-8fdf44978231</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryListUpload/MultiUserBeneficiaryListUpload/MultiUserBeneficiaryListUploadApproverResult(DomesticBank)</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>89e267b0-4f32-4d8a-b137-ac397d6a28a7</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>809d729f-bada-42d2-b697-5b80d3b91765</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryListUpload/MultiUserBeneficiaryListUpload/MultiUserBeneficiaryListUploadApproverResult(InternationalBank)</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>2c6240e9-1faa-41b6-92d3-cb34934cb21c</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
