<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>SingleUserRepeat</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>6a235098-c24c-44a3-a0e5-91fc8055a7dd</testSuiteGuid>
   <testCaseLink>
      <guid>825ddc58-3018-406d-9956-c110278a0e32</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Inhouse(Repeat)/SingleUser-Inhouse(Repeat)CreateEntryScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3cf19138-477f-4dd7-9227-14eb9325ec9e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Inhouse(Repeat)/SingleUser-Inhouse(Repeat)CreateConfirmScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f77d035b-d596-4766-b664-c57c1d6e63c2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Inhouse(Repeat)/SingleUser-Inhouse(Repeat)CreateResultScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>03ca1102-0213-4d90-9e1d-8cded3c82d11</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Inhouse(Repeat)/SingleUser-Inhouse(Repeat)TransactionStatusEntryScreenReffNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>95faf1d8-bf41-4946-a7c3-f9aa43b4b96f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Inhouse(Repeat)/SingleUser-Inhouse(Repeat)TransactionStatusScreenReffNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e0958d54-7410-4001-a99a-0786b2d7fe8b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Inhouse(Repeat)/SingleUser-Inhouse(Repeat)TransactionStatusEntryScreenDocNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a2f7ce33-2d4d-4eb0-8499-b4c16c2662fa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Inhouse(Repeat)/SingleUser-Inhouse(Repeat)TransactionStatusScreenDocNo</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
