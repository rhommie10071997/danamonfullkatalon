<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>SingleUserFutureDate</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>58abea9a-b453-4fbb-8973-3ecb76a4723b</testSuiteGuid>
   <testCaseLink>
      <guid>3b74cd93-aa01-497b-ad13-84e0bc1b3247</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Inhouse(FutureDate)/SingleUser-Inhouse(FutureDate)CreateEntryScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1027a43c-15da-4bde-ad42-105248d59e19</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Inhouse(FutureDate)/SingleUser-Inhouse(FutureDate)CreateConfirmScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>aef16a59-ff1e-42cf-92ee-7820371f1871</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Inhouse(FutureDate)/SingleUser-Inhouse(FutureDate)CreateResultScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>652dca4a-72a4-4514-9ab3-4257a2d46264</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Inhouse(FutureDate)/SingleUser-Inhouse(FutureDate)TransactionStatusReffNoEntryScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a52a5fcd-f70a-4e06-a585-327cc861a9c2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Inhouse(FutureDate)/SingleUser-Inhouse(FutureDate)TransactionStatusReffNoScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f53eef50-519f-4601-856b-89416b065291</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Inhouse(FutureDate)/SingleUser-Inhouse(FutureDate)TransactionStatusDocNoEntryScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bc0f303b-2835-4fa4-b2d0-b7fadbfb1fbf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Inhouse(FutureDate)/SingleUser-Inhouse(FutureDate)TransactionStatusDocNoScreen</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
