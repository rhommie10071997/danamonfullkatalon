<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>SingleUserImmediate</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>2b0932a5-de60-4278-8047-7c37c9789042</testSuiteGuid>
   <testCaseLink>
      <guid>ee29b38b-2a16-44e2-959a-9b29d99dc239</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Inhouse(Immediate)/SingleUser-Inhouse(Immediate)EntryScreen</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>7408b128-efc8-4c2e-b893-6c56f71a5971</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>c9b9c693-fe52-41f2-8ef6-dc41a7de52c0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Inhouse(Immediate)/SingleUser-Inhouse(Immediate)ConfirmScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1497d207-37a1-4202-a67b-a8bc7ef97566</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Inhouse(Immediate)/SingleUser-Inhouse(Immediate)ResultScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c004c401-076e-42d1-8da9-49dd4bd69d49</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Inhouse(Immediate)/SingleUser-Inhouse(Immediate)TransactionStastusReffNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6ea87c9d-18be-4c80-9c2a-fbc51db7eaeb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/SingleTransfer/SingleUser-Inhouse(Immediate)/SingleUser-Inhouse(Immediate)TransactionStatusDocNo</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
