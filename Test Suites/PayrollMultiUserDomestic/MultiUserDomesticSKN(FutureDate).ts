<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>MultiUserDomesticSKN(FutureDate)</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-02-20T15:45:01</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>ab65f216-4d62-49db-a05d-2823ce37dbba</testSuiteGuid>
   <testCaseLink>
      <guid>5ec0b609-cf7a-48fc-ab9f-6736061581f7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Domestic(FutureDate)SKN/MultiUserPayroll-DomesticSKN(FutureDate)CreateEntryScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2e270140-7265-41b6-9e4b-e7e63cddac45</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Domestic(FutureDate)SKN/MultiUserPayroll-DomesticSKN(FutureDate)BucketListLoop</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>11175820-4463-4bb2-b35e-8dd093550946</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>c3982cd0-840a-40a6-902a-f0d402fbf662</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>1193c0a2-f6be-42db-9b27-c7719369dee8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Domestic(FutureDate)SKN/MultiUserPayroll-DomesticSKN(FutureDate)BucketDetails</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>7d8fb65e-622e-4615-b1c5-6634939e1eac</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>2075c73f-458c-4804-a484-bb0af19393e2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Domestic(FutureDate)SKN/MultiUserPayroll-DomesticSKN(FutureDate)BucketConfirm</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>ba5691a2-0c16-416b-ab96-79ce8b99dda0</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>e798e900-3a9d-4314-a3fe-3be39557a036</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Domestic(FutureDate)SKN/MultiUserPayroll-DomesticSKN(FutureDate)CreateResultScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>63b5f85f-8b1c-4546-8e44-75360481aaaa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Domestic(FutureDate)SKN/MultiUserPayroll-DomesticSKN(FutureDate)ApproverEntryScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>85caccbe-41b3-4552-8561-3b72c4c2947c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Domestic(FutureDate)SKN/MultiUserPayroll-DomesticSKN(FutureDate)ApproverConfrimScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1befe285-d826-458c-be84-9c2992edf41c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Domestic(FutureDate)SKN/MultiUserPayroll-DomesticSKN(FutureDate)ApproverResultScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9e0060aa-ee5a-454f-ba2e-172c71d0df25</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Domestic(FutureDate)SKN/MultiUserPayroll-DomesticSKN(FutureDate)TransactionStatusEntryScreenReffNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d29dac57-6c9a-439b-ad83-aad1dd75ee79</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Domestic(FutureDate)SKN/MultiUserPayroll-DomesticSKN(FutureDate)TransactionStatusReffNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c20bbbac-7b32-4174-8b1c-216c2f852d2d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Domestic(FutureDate)SKN/MultiUserPayroll-DomesticSKN(FutureDate)TransactionStatusEntryScreenDocNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>556c578f-4778-45d2-a638-964a7659d398</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Domestic(FutureDate)SKN/MultiUserPayroll-DomesticSKN(FutureDate)TransactionStatusDocNo</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
