<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>MultiUserDomesticSKN(Immediate)</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-02-20T22:30:17</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>7a2b2b8b-5fdd-40ec-a879-b1ed74317a02</testSuiteGuid>
   <testCaseLink>
      <guid>d6beb446-cc25-4cec-a57d-3eb725e09ae5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Domestic(Immediate)SKN/MultiUserPayroll-DomesticSKN(Immediate)CreateEntryScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c75f4b01-f007-4ee8-ae06-398742b5d181</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Domestic(Immediate)SKN/MultiUserPayroll-DomesticSKN(Immediate)BucketListLoop</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>fea949ea-b2ac-4a41-8054-e72d18ee108f</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>b703d70b-c3e5-44d3-b071-0a2ee73a1aa9</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>f9226866-4b49-4a70-8bd8-cd8d8a1ffff6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Domestic(Immediate)SKN/MultiUserPayroll-DomesticSKN(Immediate)BucketDetails</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>ee3fb4b1-875b-4437-a55c-c77289dcc631</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>c2a5e9b0-acd1-4e5f-a301-1a67af160177</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Domestic(Immediate)SKN/MultiUserPayroll-DomesticSKN(Immediate)BucketConfirm</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>cd680d0c-2352-4e42-9acc-7096644bcb1d</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>a5e0697c-5064-4c63-976d-165f76584a7c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Domestic(Immediate)SKN/MultiUserPayroll-DomesticSKN(Immediate)CreateResultScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ab89a802-7272-4486-b1c7-cc4c51e0fa43</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Domestic(Immediate)SKN/MultiUserPayroll-DomesticSKN(Immediate)ApproverEntryScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4fcb974b-5611-4ceb-af96-2928338a0f7c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Domestic(Immediate)SKN/MultiUserPayroll-DomesticSKN(Immediate)ApproverConfrimScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9828c55e-4ef1-41e9-9cd7-1326b3352103</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Domestic(Immediate)SKN/MultiUserPayroll-DomesticSKN(Immediate)ApproverResultScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8be761b6-8238-4752-9ea7-8d7e4f89e441</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Domestic(Immediate)SKN/MultiUserPayroll-DomesticSKN(Immediate)TransactionStatusEntryScreenReffNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7aac52a2-1264-4e72-bfd6-6dfb177e47db</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Domestic(Immediate)SKN/MultiUserPayroll-DomesticSKN(Immediate)TransactionStatusReffNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c644493d-8f1f-43c5-adc0-b0e00eae93d1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Domestic(Immediate)SKN/MultiUserPayroll-DomesticSKN(Immediate)TransactionStatusEntryScreenDocNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>42f5652a-a123-4bd0-afcb-05df38a264b2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Domestic(Immediate)SKN/MultiUserPayroll-DomesticSKN(Immediate)TransactionStatusDocNo</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
