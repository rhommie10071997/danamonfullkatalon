<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>MultiUserDomesticRTGS(FutureDate)</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-02-20T17:40:33</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>54503e67-9b10-45b8-bebb-4d25833d0eb0</testSuiteGuid>
   <testCaseLink>
      <guid>674c24b2-2247-467f-a764-45d06191c4a2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Domestic(FutureDate)RTGS/MultiUserPayroll-DomesticRTGS(FutureDate)CreateEntryScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6493c48a-df87-4999-a308-b3a0f4e2a901</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Domestic(FutureDate)RTGS/MultiUserPayroll-DomesticRTGS(FutureDate)BucketListLoop</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>7806d838-38b9-415c-8175-c19641a61851</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>5367386f-b8c9-482e-89e6-4f3d431a681b</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>4bf51e40-11a8-4d94-8c5f-e089a9e5c92c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Domestic(FutureDate)RTGS/MultiUserPayroll-DomesticRTGS(FutureDate)BucketDetails</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>99283609-3bc1-4661-802c-7520b89df7f8</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>8ddc9fdc-6f98-4ef9-868e-a32a16909b83</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Domestic(FutureDate)RTGS/MultiUserPayroll-DomesticRTGS(FutureDate)BucketConfirm</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>6c354b6d-079a-4136-b936-d3326cd36489</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>a423dcb0-919d-4bc7-b8c6-bbc3742830df</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Domestic(FutureDate)RTGS/MultiUserPayroll-DomesticRTGS(FutureDate)CreateResultScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>24e33b6d-3b13-464c-bcdb-8b1aab188fb3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Domestic(FutureDate)RTGS/MultiUserPayroll-DomesticRTGS(FutureDate)ApproverEntryScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3968e2ee-d425-4f13-b149-596d70af71eb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Domestic(FutureDate)RTGS/MultiUserPayroll-DomesticRTGS(FutureDate)ApproverConfrimScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6f7d692e-8ff5-4464-a7cd-560709ca6e7f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Domestic(FutureDate)RTGS/MultiUserPayroll-DomesticRTGS(FutureDate)ApproverResultScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>36867bd0-0985-4437-aaaa-58e9c2118880</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Domestic(FutureDate)RTGS/MultiUserPayroll-DomesticRTGS(FutureDate)TransactionStatusEntryScreenReffNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cf2c4588-c067-465e-946b-8e7710a229aa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Domestic(FutureDate)RTGS/MultiUserPayroll-DomesticRTGS(FutureDate)TransactionStatusReffNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ac6b754e-7f27-46ec-a7bc-4bdb2b6dcf6c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Domestic(FutureDate)RTGS/MultiUserPayroll-DomesticRTGS(FutureDate)TransactionStatusEntryScreenDocNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0f9e6552-f9f1-46d7-bcad-97218fadadc0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Domestic(FutureDate)RTGS/MultiUserPayroll-DomesticRTGS(FutureDate)TransactionStatusDocNo</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
