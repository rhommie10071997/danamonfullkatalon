<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>MultiUserDomesticRTGS(Immediate)</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-02-20T16:17:50</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>22d5a66a-9ea7-468e-8102-af6a377b9892</testSuiteGuid>
   <testCaseLink>
      <guid>db1f8ae5-0925-4eef-886f-699a8b63eb64</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Domestic(Immediate)RTGS/MultiUserPayroll-DomesticRTGS(Immediate)CreateEntryScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>739eb8d1-6915-4dc0-8bcb-4f9710695a58</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Domestic(Immediate)RTGS/MultiUserPayroll-DomesticRTGS(Immediate)BucketListLoop</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>ed0ae90e-2675-4c1d-b394-6da29eaca4b3</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>420be5d3-4ea9-498b-ac22-e143f87b681b</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>ba76da04-368a-4596-a5a2-0b8efe6b86d3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Domestic(Immediate)RTGS/MultiUserPayroll-DomesticRTGS(Immediate)BucketDetails</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>2900e0db-2aaa-4493-9749-e45cbf66196b</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>8bd0398e-49f9-4a96-a95a-2de94412ab85</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Domestic(Immediate)RTGS/MultiUserPayroll-DomesticRTGS(Immediate)BucketConfirm</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>714aa734-2050-41b1-979e-8dd5a013c62c</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>37747f77-ae79-4ca8-82eb-8de8ddb15ab0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Domestic(Immediate)RTGS/MultiUserPayroll-DomesticRTGS(Immediate)CreateResultScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8311a494-5f01-42c8-89bd-5f001ea173a8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Domestic(Immediate)RTGS/MultiUserPayroll-DomesticRTGS(Immediate)ApproverEntryScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>602879db-fcec-40aa-a5c9-899ba7fec113</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Domestic(Immediate)RTGS/MultiUserPayroll-DomesticRTGS(Immediate)ApproverConfrimScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a5ddea0c-9a04-46ff-bb75-71945793c481</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Domestic(Immediate)RTGS/MultiUserPayroll-DomesticRTGS(Immediate)ApproverResultScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9e3b3de3-922f-426d-8599-ad1553274463</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Domestic(Immediate)RTGS/MultiUserPayroll-DomesticRTGS(Immediate)TransactionStatusEntryScreenReffNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7480c05f-d22f-4293-a404-292298e5152a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Domestic(Immediate)RTGS/MultiUserPayroll-DomesticRTGS(Immediate)TransactionStatusReffNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>869afd3e-4682-4f1e-83fe-36c7179d5c5c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Domestic(Immediate)RTGS/MultiUserPayroll-DomesticRTGS(Immediate)TransactionStatusEntryScreenDocNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5a400246-515f-4b29-bca6-df4e2628f174</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Domestic(Immediate)RTGS/MultiUserPayroll-DomesticRTGS(Immediate)TransactionStatusDocNo</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
