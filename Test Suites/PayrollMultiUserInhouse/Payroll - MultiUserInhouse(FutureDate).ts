<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Payroll - MultiUserInhouse(FutureDate)</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>28f44049-1a79-4e5c-9370-1c5e32250b3a</testSuiteGuid>
   <testCaseLink>
      <guid>c6bf14d0-fe9a-455c-994f-9aca0237fe48</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Inhouse(FutureDate)/MultiUserPayroll-Inhouse(FutureDate)CreateEntryScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>242f7f71-690f-4f99-96cc-4d4cb884e7cf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Inhouse(FutureDate)/MultiUserPayroll-Inhouse(FutureDate)BucketListLoop</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>36266b9b-dc6f-4f70-9858-1a20bd1ce5e8</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>9fdec0df-e388-45cc-88ee-4844d7e35778</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>afaf8008-7d4b-4b16-b462-cb13ab3cbeaa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Inhouse(FutureDate)/MultiUserPayroll-Inhouse(FutureDate)BucketDetails</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>2fb36c2b-f545-469e-a880-00455c469be1</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>80dbed94-4017-48b8-9efa-588a578d0708</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Inhouse(FutureDate)/MultiUserPayroll-Inhouse(FutureDate)BucketConfirm</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>1166900c-e774-414a-b7e1-1c23d225cf6f</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>9dc9798c-d71d-447b-93c7-91bf14411c51</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Inhouse(FutureDate)/MultiUserPayroll-Inhouse(FutureDate)CreateResultScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4e690955-d790-483f-9eb7-ed65fdb46f75</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Inhouse(FutureDate)/MultiUserPayroll-Inhouse(FutureDate)ApproverEntryScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e50d5f4e-e13e-49fe-b97f-c2796751f233</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Inhouse(FutureDate)/MultiUserPayroll-Inhouse(FutureDate)ApproverConfrimScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>db2629e6-f0ea-4a67-bd41-6bc1fa0ceeb9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Inhouse(FutureDate)/MultiUserPayroll-Inhouse(FutureDate)ApproverResultScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c1e0a28b-785f-40eb-a29a-1e81c2f81760</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Inhouse(FutureDate)/MultiUserPayroll-Inhouse(FutureDate)TransactionStatusEntryScreenReffNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a874c92a-d157-43e6-bd80-3898fb201e6b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Inhouse(FutureDate)/MultiUserPayroll-Inhouse(FutureDate)TransactionStatusReffNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>56e54db8-d2d5-4c31-b67d-523e166c08fa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Inhouse(FutureDate)/MultiUserPayroll-Inhouse(FutureDate)TransactionStatusEntryScreenDocNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2dbabad7-e91d-4767-89bb-541dbd9fec89</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Inhouse(FutureDate)/MultiUserPayroll-Inhouse(FutureDate)TransactionStatusDocNo</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
