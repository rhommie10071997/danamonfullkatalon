<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Payroll - MultiUserInhouse(Immediate)</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>1ec8942c-a7b4-479b-b347-2a0290b7edb7</testSuiteGuid>
   <testCaseLink>
      <guid>ddab9a15-fb01-4e86-96a8-26ec17b29933</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Inhouse(Immediate)/MultiUserPayroll-Inhouse(Immediate)CreateEntryScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4adff347-29ee-419a-92b2-223e0a7b073e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Inhouse(Immediate)/MultiUserPayroll-Inhouse(Immediate)BucketListLoop</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>80be7c07-873d-48b7-b56a-1e9df8666178</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>8a32c511-4a6b-4cf0-b373-7c3f0442af6e</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>51d6c732-7126-46fc-a8dc-411c6bd9466d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Inhouse(Immediate)/MultiUserPayroll-Inhouse(Immediate)BucketDetails</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>0f65b681-b5ec-418c-90c5-96c249f16b72</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>a81374f3-6f42-471d-bebd-30739988b322</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Inhouse(Immediate)/MultiUserPayroll-Inhouse(Immediate)BucketConfirm</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>d41fcc35-7b42-4bba-abd8-6aeecbc8964a</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>5b1cb55f-0434-4ff0-96ef-0a7afe5f16f0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Inhouse(Immediate)/MultiUserPayroll-Inhouse(Immediate)CreateResultScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>623941cd-9f93-4267-8b73-f3ddeb03c63d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Inhouse(Immediate)/MultiUserPayroll-Inhouse(Immediate)ApproverEntryScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d1f25808-6b35-4bf3-b7ef-59b95a975798</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Inhouse(Immediate)/MultiUserPayroll-Inhouse(Immediate)ApproverConfrimScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>508c8162-b218-4d71-bac4-797c9d54a285</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Inhouse(Immediate)/MultiUserPayroll-Inhouse(Immediate)ApproverResultScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6a59bf15-1f75-499b-9c0b-e24216278fdb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Inhouse(Immediate)/MultiUserPayroll-Inhouse(Immediate)TransactionStatusEntryScreenReffNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bac0cb94-4610-4c98-83c5-c2bfbb6e0bfb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Inhouse(Immediate)/MultiUserPayroll-Inhouse(Immediate)TransactionStatusReffNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8ca266ed-89d5-4a1e-bdc1-1af50dd3e27b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Inhouse(Immediate)/MultiUserPayroll-Immediate(Immediate)TransactionStatusEntryScreenDocNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ff2e2b3b-8f57-4907-be1c-122e0f6050e2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Payroll/MultiUserPayroll-Inhouse(Immediate)/MultiUserPayroll-Inhouse(Immediate)TransactionStatusDocNo</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
