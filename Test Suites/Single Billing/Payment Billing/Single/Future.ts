<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Future</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-12-10T14:29:18</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>e4be490a-f3ae-4225-8fab-c552abbf2cda</testSuiteGuid>
   <testCaseLink>
      <guid>c3823952-9a45-42e9-9c80-bed8ab126f07</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Payment Billing ID/Single/Future/Create/New Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7bfc26e8-8043-4360-bdc5-78421a3b14ee</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Payment Billing ID/Single/Future/Create/Confirm</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cb7bf5b5-e366-4e98-967f-f5a9baa4a3f0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Payment Billing ID/Single/Future/Create/Result</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6dc146eb-cf5a-425a-a37b-9783913126b0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Payment Billing ID/Single/Future/Transaction Status/Transaction Status - Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>116c1c1a-2425-4eea-95da-de09a8f7fdee</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Payment Billing ID/Single/Future/Transaction Status/Ref No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>200d916d-3c02-4756-a90b-753b8b1da107</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Payment Billing ID/Single/Future/Transaction Status/Transaction Status - Doc Num Detail</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
