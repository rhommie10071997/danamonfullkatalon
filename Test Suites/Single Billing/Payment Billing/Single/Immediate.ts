<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Immediate</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-12-10T14:03:33</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>27f6f42d-e893-4116-9c63-a9e5ab3502b5</testSuiteGuid>
   <testCaseLink>
      <guid>80beaabb-4c1b-4010-ae78-fa18d8dd8295</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Payment Billing ID/Single/Immediate/Create/New Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9ff85e3b-b9fa-48a5-9698-e9236d2efcdc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Payment Billing ID/Single/Immediate/Create/Confirm</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5e1ed58d-0362-48f1-900e-d38c01980d91</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Payment Billing ID/Single/Immediate/Create/Result</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>83af6157-c740-4474-af7e-139b831899fb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Payment Billing ID/Single/Immediate/Transaction Status/Transaction Status - Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>40b2253a-71c8-4d81-9fa9-0ab2180858ac</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Payment Billing ID/Single/Immediate/Transaction Status/Ref No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5872f2dc-048a-4f44-b793-2d2669c51aaf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Payment Billing ID/Single/Immediate/Transaction Status/Transaction Status - Doc Num Detail</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
