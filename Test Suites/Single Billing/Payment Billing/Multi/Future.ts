<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Future</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-12-10T12:19:44</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>43ce71fa-ec70-4b39-b296-3b6a801ab39a</testSuiteGuid>
   <testCaseLink>
      <guid>7d0c738f-5ea6-49f1-97a1-b4cbb9e63f6e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Payment Billing ID/Multi/Future/Create/New Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8b3a4a0a-3234-4d57-8dde-20f987f35a8c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Payment Billing ID/Multi/Future/Create/Confirm</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6eca0edd-632e-47e0-8e6d-16b0915b5041</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Payment Billing ID/Multi/Future/Create/Result</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>49c75569-4156-4022-8a6b-416dab74be2c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Payment Billing ID/Multi/Future/Approve/Entry Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ccc89c28-3a7a-47d9-8d08-54d20b4dedb4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Payment Billing ID/Multi/Future/Approve/Detail</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bbae6d58-2e99-4b08-a438-ecb1307ad8c7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Payment Billing ID/Multi/Future/Approve/Result</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>477c11c9-2584-4375-9e49-6b635dd2fe81</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Payment Billing ID/Multi/Future/Transaction Status/Transaction Status - Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>94208194-d5a8-4309-8d2b-9ec1f9c8ac65</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Payment Billing ID/Multi/Future/Transaction Status/Ref No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f781a006-64e2-49dd-a84c-abd4fdbdcdb8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Payment Billing ID/Multi/Future/Transaction Status/Transaction Status - Doc Num Detail</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
