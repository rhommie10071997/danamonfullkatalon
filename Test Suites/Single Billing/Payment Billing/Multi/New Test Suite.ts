<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>New Test Suite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-01-07T11:58:13</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>881116f3-fdad-467b-9758-accbfa29864d</testSuiteGuid>
   <testCaseLink>
      <guid>a2990d1e-e5a4-4c8d-a720-db5aa02defbb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Payment Billing ID/Multi/New Immediate/Create/New Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0f49c7b9-6d21-4d60-a6e9-6bb4d3acb790</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Payment Billing ID/Multi/New Immediate/Create/Confirm</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>80ce95c1-4f4b-4321-89ac-4bd621f070e3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Payment Billing ID/Multi/New Immediate/Create/Result</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d7fd401f-feb9-4024-a2ff-d64ac7ee0d69</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Payment Billing ID/Multi/New Immediate/Approve/Entry Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f2024c5d-245c-4e01-9411-d54a46e318bc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Payment Billing ID/Multi/New Immediate/Approve/Detail</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7ec9b024-2923-4b86-b60e-e2f5998a2aa5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Payment Billing ID/Multi/New Immediate/Approve/Result</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>df1ccb3e-eb12-4660-a5b4-b2fc1a90eb35</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Payment Billing ID/Multi/New Immediate/Transaction Status/Transaction Status - Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4c6ef179-2c93-4481-9e3d-bb5158741293</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Payment Billing ID/Multi/New Immediate/Transaction Status/Ref No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>15d3b197-972c-4ecb-ba33-f94ce7e9c2a9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Payment Billing ID/Multi/New Immediate/Transaction Status/Transaction Status - Doc Num Detail</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
