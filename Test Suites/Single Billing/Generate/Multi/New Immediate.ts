<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>New Immediate</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-12-27T14:57:49</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>65acfc68-3223-46bd-bad2-b53d365a774d</testSuiteGuid>
   <testCaseLink>
      <guid>d1f07a74-eb92-49b9-b150-d3df87ad8b10</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Generate Billing ID/Multi/New Immediate/Create/Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>414210ee-b883-4660-8e2b-9bc741c7e78e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Generate Billing ID/Multi/New Immediate/Create/Confirm</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7576643f-388e-465e-88d0-73fd78ea905e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Generate Billing ID/Multi/New Immediate/Create/Result</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1d57ee44-eb4b-4dee-a19d-a3c482c136df</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Generate Billing ID/Multi/New Immediate/Approve/Entry Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>decd101d-9f53-4414-a5dd-647877aae3ca</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Generate Billing ID/Multi/New Immediate/Approve/Detail</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1edb5206-741e-4691-85a0-05d87f760ac5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Generate Billing ID/Multi/New Immediate/Approve/Result</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>50a5d465-9ba3-46a4-8776-4ed572c4b0f1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Generate Billing ID/Multi/New Immediate/Transaction Status/Transaction Status - Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>99a479e1-badd-49cf-b0a2-c726303806e7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Generate Billing ID/Multi/New Immediate/Transaction Status/Transaction Status - Ref No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cdb960ed-c5fd-4039-a3bc-244be6b5f21b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Generate Billing ID/Multi/New Immediate/Transaction Status/Transaction Status - Doc No</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
