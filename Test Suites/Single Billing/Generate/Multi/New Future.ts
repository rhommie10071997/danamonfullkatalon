<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>New Future</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-12-27T15:42:43</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>883ee25e-ef10-4b9d-b0ec-1d2edccc57a3</testSuiteGuid>
   <testCaseLink>
      <guid>14a51c55-e94d-47d3-8422-81977c608f59</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Generate Billing ID/Multi/New Future/Create/Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>53e93908-ddb8-4a61-bf1d-afe3a5cf6054</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Generate Billing ID/Multi/New Future/Create/Confirm</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>72336abc-863f-4277-909c-526d9954bc52</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Generate Billing ID/Multi/New Future/Create/Result</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b6807d65-6610-461a-b898-0f39fd5543f9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Generate Billing ID/Multi/New Future/Approve/Entry Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ac8d62fa-fc06-42ce-beab-1dfd44b1905b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Generate Billing ID/Multi/New Future/Approve/Detail</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>905379fb-696f-4abe-b4da-857e6a009072</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Generate Billing ID/Multi/New Future/Approve/Result</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1f03c4a4-05bf-4369-933f-42a5f51dffb9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Generate Billing ID/Multi/New Future/Transaction Status/Transaction Status - Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b689d548-4b25-4e36-9be3-329801e9952c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Generate Billing ID/Multi/New Future/Transaction Status/Transaction Status - Ref No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>71cb3e6a-425b-4a4a-b0ed-6ef4a90c431c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Generate Billing ID/Multi/New Future/Transaction Status/Transaction Status - Doc No</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
