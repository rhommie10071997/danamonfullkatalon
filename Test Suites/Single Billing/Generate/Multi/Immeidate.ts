<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Immeidate</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-12-07T16:23:18</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>0944bf81-925c-4275-8588-3ff43c278871</testSuiteGuid>
   <testCaseLink>
      <guid>c286b7c9-8df7-4996-a831-53b0a601784e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Generate Billing ID/Multi/Immediate/Create/Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>00ebaaec-defb-4867-9010-424176abd5d5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Generate Billing ID/Multi/Immediate/Create/Confirm</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0725f757-2bef-4528-b570-13d3cdccd8fb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Generate Billing ID/Multi/Immediate/Create/Result</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d09c6ed7-c6c1-4926-afbd-7ddc7490a9b5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Generate Billing ID/Multi/Immediate/Approve/Entry Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6ccb5fd4-9d33-499b-b8a4-6d49fa817040</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Generate Billing ID/Multi/Immediate/Approve/Detail</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>370133ae-0136-4a3d-a26b-27c1691995fa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Generate Billing ID/Multi/Immediate/Approve/Result</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e7704496-336f-4159-80c9-3ad188ac95e9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Generate Billing ID/Multi/Immediate/Transaction Status/Transaction Status - Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3bf11d28-48a2-4862-a254-1c6ff20e5604</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Generate Billing ID/Multi/Immediate/Transaction Status/Transaction Status - Ref No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0106bbdb-2c5f-4677-9276-47c7df82d06c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Generate Billing ID/Multi/Immediate/Transaction Status/Transaction Status - Doc No</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
