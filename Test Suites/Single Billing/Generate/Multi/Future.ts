<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Future</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-11-30T15:16:17</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>8dfea421-3bff-4408-9c68-6cebf32b37b8</testSuiteGuid>
   <testCaseLink>
      <guid>aead7c61-ccf0-450a-ad20-746a7249d102</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Generate Billing ID/Multi/Future/Create/Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b7f14aac-7724-4b38-9c76-50f6f1ed83a2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Generate Billing ID/Multi/Future/Create/Confirm</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3d4ac587-7ba4-4c95-89ff-04a019ecf2bd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Generate Billing ID/Multi/Future/Create/Result</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>42ade118-5edd-4007-8f0b-8d8a9caa405d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Generate Billing ID/Multi/Future/Approve/Entry Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a2515c22-2d96-4a4f-b2f8-ebeba4374786</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Generate Billing ID/Multi/Future/Approve/Detail</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>85122c4a-ae69-4566-ac7e-bf6de0ef42b5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Generate Billing ID/Multi/Future/Approve/Result</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>27100fc9-5592-4dc1-8e74-27323877710a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Generate Billing ID/Multi/Future/Transaction Status/Transaction Status - Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>739da310-141c-4fb4-a569-973837a2ab26</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Generate Billing ID/Multi/Future/Transaction Status/Transaction Status - Ref No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>10cafac0-4409-4a09-9abc-4c0f12063b53</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Generate Billing ID/Multi/Future/Transaction Status/Transaction Status - Doc No</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
