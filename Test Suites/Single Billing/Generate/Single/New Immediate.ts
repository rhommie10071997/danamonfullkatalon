<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>New Immediate</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-12-27T16:31:42</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>fe4b9cc7-d9ee-4f76-a4a2-fe713ddf8667</testSuiteGuid>
   <testCaseLink>
      <guid>53f93ef2-3f8c-482f-8abf-b324b8c744e5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Generate Billing ID/Single/New Immediate/Create/Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bf5b8091-d457-4f15-9608-f4f74ca22b06</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Generate Billing ID/Single/New Immediate/Create/Confirm</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d5c79783-3c4d-4310-bb75-bb9ca9b895e3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Generate Billing ID/Single/New Immediate/Create/Result</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2635c3e2-1dd5-4d59-9048-1b4c2ecb1fa2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Generate Billing ID/Single/New Immediate/Transaction Status/Transaction Status - Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>35409239-03a7-405b-8d53-146013de250c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Generate Billing ID/Single/New Immediate/Transaction Status/Transaction Status - Ref No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ade74db8-83de-47d4-b19b-bd25ea5f54c9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Generate Billing ID/Single/New Immediate/Transaction Status/Transaction Status - Doc No</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
