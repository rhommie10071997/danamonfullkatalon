<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Future</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-12-07T16:14:57</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>df84483e-1af5-4ef9-a3eb-c5c4ce0c12e3</testSuiteGuid>
   <testCaseLink>
      <guid>c76e67c9-83a7-46ec-8622-25f91203d6d7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Generate Billing ID/Single/Future Date/Create/Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e02dc745-5f1b-4dc1-bbbf-6c2cecb0aa25</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Generate Billing ID/Single/Future Date/Create/Confirm</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>90e11ab0-4c9f-447a-a9b1-6554550fd720</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Generate Billing ID/Single/Future Date/Create/Result</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>18ac6e1c-b8bc-4652-9073-e010e533560f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Generate Billing ID/Single/Future Date/Transaction Status/Transaction Status - Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8edd6071-d449-4ba2-ba5b-f5de08a7b1cd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Generate Billing ID/Single/Future Date/Transaction Status/Transaction Status - Ref No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>456ba562-0dba-49e3-a766-863b2eb4737a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Generate Billing ID/Single/Future Date/Transaction Status/Transaction Status - Doc No</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
