<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>New Future</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-12-28T13:39:37</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>2d064368-f66b-4160-ad2a-176795fa3d43</testSuiteGuid>
   <testCaseLink>
      <guid>e507d570-df59-484c-a73e-1b83f0c234c2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Generate Billing ID/Single/New Future/Create/Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f38984ee-3f0c-401f-b91f-1a383d9fa2fb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Generate Billing ID/Single/New Future/Create/Confirm</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e2aeb3fa-861b-46f9-9695-78cecfe2b9c2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Generate Billing ID/Single/New Future/Create/Result</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>25ad9392-65a6-4d4a-ad03-9c4b377cc91d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Generate Billing ID/Single/New Future/Transaction Status/Transaction Status - Entry</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c7587983-64d0-47a6-8701-22561966c993</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Generate Billing ID/Single/New Future/Transaction Status/Transaction Status - Ref No</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fe8f408f-61cf-4c83-af35-fe369a5ec492</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Single Billing/Generate Billing ID/Single/New Future/Transaction Status/Transaction Status - Doc No</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
