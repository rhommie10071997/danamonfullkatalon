<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>SingleUserBeneficiaryListDelete</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-11-27T13:28:40</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>2b3ac410-00e2-4b11-bf18-d96a090f4ea2</testSuiteGuid>
   <testCaseLink>
      <guid>61761a10-1d61-4023-8eeb-36fdab80009d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/SingleUserBeneficiaryListDelete/SingleUserBeneficiaryListDeleteEntryScreen</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>f3fcdc18-95f3-4cfa-b9f5-036549e612e9</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>c9209350-9062-4847-a8b5-fa8888254e4e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/SingleUserBeneficiaryListDelete/SingleUserBeneficiaryListDeleteConfrimScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>56d26b64-da2d-4cf0-9940-660f6d00664c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/SingleUserBeneficiaryListDelete/SingleUserBeneficiaryListDeleteDomesticDetailsConfrimScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>53afc4b1-e2ed-41ba-8bf3-b167e22f8ee6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/SingleUserBeneficiaryListDelete/SingleUserBeneficiaryListDeleteInternationalDetailsConfrimScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>524beedd-6038-403f-824c-ec20cd6eee0a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/SingleUserBeneficiaryListDelete/SingleUserBeneficiaryListDeleteResultScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>81b70ec7-ac66-4f0b-bf12-31796657f43c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/SingleUserBeneficiaryListDelete/SingleUSerBeneficiaryListDeleteResultScreenDomestic</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>10fa8555-1273-4664-ad81-97c5ee5ff08c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/SingleUserBeneficiaryListDelete/SingleUserBeneficiaryListDeleteResultScreenInternational</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
