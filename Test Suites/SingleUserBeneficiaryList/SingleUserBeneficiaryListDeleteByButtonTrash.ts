<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>SingleUserBeneficiaryListDeleteByButtonTrash</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-11-27T14:02:30</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>9aa29ac3-af0e-42b8-a333-7e53bf756be5</testSuiteGuid>
   <testCaseLink>
      <guid>ae9bd90b-6f24-41c3-9079-a041c29c9ce5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/SingleUserBeneficiaryListDeleteByButtonTrash/SingleUserBeneficiaryListDeleteEntryScreenByButtonTrash</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>5f8045dd-6f54-4446-91d9-6545b780a525</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>fc716af2-52b7-4536-8497-b988bd67e62e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/SingleUserBeneficiaryListDeleteByButtonTrash/SingleUserBeneficiaryListDeleteResultScreenByButtonTrush</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5acad462-f6a8-47aa-9440-52749ded2537</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/SingleUserBeneficiaryListDeleteByButtonTrash/SingleUSerBeneficiaryListDeleteResultScreenDomesticByButtonTrush</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>798f9617-c2b3-44c5-a200-49eb24a54049</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/SingleUserBeneficiaryListDeleteByButtonTrash/SingleUserBeneficiaryListDeleteResultScreenInternationalByButtonTrush</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
