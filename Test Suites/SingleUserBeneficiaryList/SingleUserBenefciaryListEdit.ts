<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>SingleUserBenefciaryListEdit</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-02-01T18:47:42</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>b895973d-d0e2-4cfb-a034-7c93d94a1c71</testSuiteGuid>
   <testCaseLink>
      <guid>a4251945-eadf-4005-b548-6341f0bd8b2d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/SingleUserBeneficiaryListEdit/SingleUserBeneficiaryListEditEntryScreenn</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>d8c2a9ef-e17c-41c5-b0d2-a56bc895b3a2</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>988ad394-c441-4413-ac2b-7dc644f0b1b5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/SingleUserBeneficiaryListEdit/SingleUserBeneficiaryListEditConfirmScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>147afaac-217d-454a-b086-6aa49804d741</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/SingleUserBeneficiaryListEdit/SingleUserBeneficiaryListEditConfrimScreenDomestic</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>34e0a630-69a7-4382-aa45-03b1faad1421</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/SingleUserBeneficiaryListEdit/SingleUserBeneficiaryListEditConfrimScreenInternational</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>856e694a-ac0a-41b6-91e5-86e90ca3acc0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/SingleUserBeneficiaryListEdit/SingleUserBeneficiriaryListEditEntryTheEdtiDataDomestic</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e8032a02-b940-40f4-b3bd-19406db62237</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/SingleUserBeneficiaryListEdit/SingleUserBeneficiaryListEditEntryTheEditDataInternational</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>10ec3b70-1c60-4f2c-9c2c-d62f15cb813b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/SingleUserBeneficiaryListEdit/SingleUserBeneficiaryListEditConfirmScreenAfterEdit</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>995b62d8-1ca6-482c-ade3-ee6a3a5ddef2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/SingleUserBeneficiaryListEdit/SingleUserBeneficiaryListEditConfrimScreenDomesticAfterEdit</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b24d7340-5f00-4876-bfb2-0cdb54c64758</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/SingleUserBeneficiaryListEdit/SingleUserBeneficiaryListEditConfrimScreenInternationalAfterEdit</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>461ddadf-492e-4561-b9e2-0f7e6de013cd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/SingleUserBeneficiaryListEdit/SingleUserBeneficiaryListEditEntryResultScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c10eb630-3bb2-4ef7-b789-4d4cfa464a5a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/SingleUserBeneficiaryListEdit/SingleUserBeneficiaryListEditEntryResultScreenDomestic</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d9eb217e-c714-4080-9d5a-7ccef3f6af85</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/SingleUserBeneficiaryListEdit/SingleUserBeneficiaryListEditEntryResultScreenInternational</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
