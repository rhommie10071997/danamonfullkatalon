<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>SingleUserBeneficiaryListadd</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-11-27T12:55:50</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>3be5cb06-ea61-4cb3-8724-c99bd5e471c2</testSuiteGuid>
   <testCaseLink>
      <guid>95e532f0-945c-483e-a837-ae8abc006f17</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/SingleUserBeneficiaryListAdd/SingleUserBeneficiaryListAdd</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>61afdd18-e0ee-4fa9-9aa8-e4c4c9a54550</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/SingleUserBeneficiaryListAdd/SingleUserBeneficiaryListAddConfirmScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cc66ac7a-1de1-4b10-8986-739a91b7c25e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/SingleUserBeneficiaryListAdd/SingleUserBeneficiaryListAddConfrimScreenDomestic</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>edcc8d40-53af-42c1-98c3-942068c15b6b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/SingleUserBeneficiaryListAdd/SingleUserBeneficiaryListAddConfrimScreenInternational</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>15bb44e5-988d-43da-9617-08b069f21096</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/SingleUserBeneficiaryListAdd/SingleUserBeneficiaryListAddEntryResultScreen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6f4f7b1c-dda5-49cf-adb2-70310ab05d21</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/SingleUserBeneficiaryListAdd/SingleUserBeneficiaryListAddEntryResultScreenDomestic</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>28ec6d11-8deb-4990-b22d-2d2b8845aea8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BeneficiaryList/SingleUserBeneficiaryListAdd/SingleUserBeneficiaryListAddEntryResultScreenInternational</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
