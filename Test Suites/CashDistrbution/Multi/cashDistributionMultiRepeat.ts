<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>cashDistributionMultiRepeat</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-01-15T11:59:57</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>9b98933b-bc8b-430d-8315-0f78b5124acd</testSuiteGuid>
   <testCaseLink>
      <guid>d4d4b1b4-c45d-4567-baea-2c3d83cf241c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/url</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9bb3a96a-5d00-4a44-84c3-3bf7684c35ea</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CashDistribution/cashDistributionMultiRepeat/cashDistributionMultiRepeat(Maker)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cccf0a68-40ec-40aa-86fa-ea5d614e270e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CashDistribution/cashDistributionMultiRepeat/cashDistributionMultiRepeat(Maker.Result)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>74a05819-9c7a-4bac-9ec6-2193b3fa34ee</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/pendingTask/pendingTask</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d1c8d6c0-6a74-4369-856a-91e06b4e6358</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CashDistribution/cashDistributionMultiRepeat/cashDistributionMultiRepeat(Approver)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>68276494-640b-48f1-ae15-dabfd8f0f79d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CashDistribution/cashDistributionMultiRepeat/cashDistributionMultiRepeat(Approver.Result)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6fc4d85c-d902-4738-ad89-6e512927b205</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/transStatus/transStatus</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>72b382a4-5eb4-494c-9be7-803ed6bb6241</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CashDistribution/cashDistributionMultiRepeat/cashDistributionMultiRepeat(TransStatus)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8b735670-0b63-4482-b093-b837cdf6d257</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CashDistribution/cashDistributionMultiRepeat/cashDistributionMultiRepeat(TransStatus_2)</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
