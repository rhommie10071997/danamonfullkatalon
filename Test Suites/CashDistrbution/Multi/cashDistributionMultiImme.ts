<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>cashDistributionMultiImme</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-01-14T12:12:53</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>cd1e7876-fa88-4771-9cef-e95d679843e6</testSuiteGuid>
   <testCaseLink>
      <guid>064fb1f4-4137-499c-b5de-3d1f6e80c97f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/url</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0b5f7e51-d173-49ee-8631-b14ffee4cc18</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CashDistribution/cashDistributionMultiImme/cashDistributionMultiImme(Maker)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>18181d4b-55fe-40bf-92c6-f0fb2e2d812e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CashDistribution/cashDistributionMultiImme/cashDistributionMultiImme(Maker.Result)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9d2d580d-7f81-41a9-9c13-ad9e336b8fe6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/pendingTask/pendingTask</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a6d5e40d-3c6e-4049-bc87-3f8a3a72d3ab</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CashDistribution/cashDistributionMultiImme/cashDistributionMultiImme(Approver)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0eb7d695-16f8-4b0d-826f-1439353799b3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CashDistribution/cashDistributionMultiImme/cashDistributionMultiImme(Approver.Result)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9a37fa13-e2e1-4d0d-a55d-5c14cdeb6062</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/transStatus/transStatus</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0cf4702f-d9b8-47db-bc2c-1e1ca62a1ef0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CashDistribution/cashDistributionMultiImme/cashDistributionMultiImme(TransStatus)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>135e6c45-c6e1-47e9-bb50-843ef78c6d8b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CashDistribution/cashDistributionMultiImme/cashDistributionMultiImme(TransStatus_2)</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
