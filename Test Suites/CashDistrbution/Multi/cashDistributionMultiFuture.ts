<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>cashDistributionMultiFuture</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-01-14T15:52:06</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>3eedcc80-2880-45f9-9b25-e8e7273ded00</testSuiteGuid>
   <testCaseLink>
      <guid>3e8cc38f-a470-4d08-8de5-a070d2149523</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/url</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>98c619c9-7eb6-42a6-a1a7-538de05bdcc5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CashDistribution/cashDistributionMultiFuture/cashDistributionMultiFuture(Maker)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e95baf9b-76fe-496e-9a43-104919f8ca61</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CashDistribution/cashDistributionMultiFuture/cashDistributionMultiFuture(Maker.Result)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0af11bc3-665e-4478-8bef-1ec6d1704378</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/pendingTask/pendingTask</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b9de4129-1eff-40d4-9f63-6a340a945e02</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CashDistribution/cashDistributionMultiFuture/cashDistributionMultiFuture(Approver)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2b3d8ed4-fa72-4418-a336-92166c135fc9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CashDistribution/cashDistributionMultiFuture/cashDistributionMultiFuture(Approver.Result)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a0c393b4-7fc0-41df-ab82-07e051acac2d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/transStatus/transStatus</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7dbb1cc4-7dbe-47dc-9d81-cf8e8900eaa4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CashDistribution/cashDistributionMultiFuture/cashDistributionMultiFuture(TransStatus)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4293ad23-60fb-4d8c-902a-a69d98b507ba</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CashDistribution/cashDistributionMultiFuture/cashDistributionMultiFuture(TransStatus_2)</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
