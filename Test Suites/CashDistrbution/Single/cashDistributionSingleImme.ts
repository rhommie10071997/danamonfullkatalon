<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>cashDistributionSingleImme</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-01-15T13:55:10</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>2bca068d-ed0a-4470-8e09-08ca03f76240</testSuiteGuid>
   <testCaseLink>
      <guid>45585106-6e8a-4367-a0bc-90db2799c1dc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/url</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>34543bb6-42d9-4448-a646-686961f70525</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CashDistribution/cashDistributionSingleImme/cashDistributionSingleImme(Maker)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>04638686-8530-4c92-9f76-fe3e6ed47ad0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CashDistribution/cashDistributionSingleImme/cashDistributionSingleImme(Maker.Result)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dc85976a-c129-4625-a31f-22d11fcd4659</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/transStatus/transStatus</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e227a9a6-1a40-49df-b7e0-e2d13fbe343e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CashDistribution/cashDistributionSingleImme/cashDistributionSingleImme(TransStatus)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a2a259bc-2782-4c13-b460-db2c83d8c0f8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CashDistribution/cashDistributionSingleImme/cashDistributionSingleImme(TransStatus_2)</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
