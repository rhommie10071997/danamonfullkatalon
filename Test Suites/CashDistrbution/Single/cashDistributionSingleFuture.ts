<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>cashDistributionSingleFuture</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2019-01-15T14:28:50</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>2e84676b-cc08-4cae-a98e-e620afd96a00</testSuiteGuid>
   <testCaseLink>
      <guid>107a21d3-e8e9-4453-84ce-f9640da6ed33</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/url</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>117de014-0e2f-4073-9116-dfb4adc7207c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CashDistribution/cashDistributionSingleFuture/cashDistributionSingleFuture(Maker)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>82172f84-a566-47ce-a65c-f06a9cb3d002</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CashDistribution/cashDistributionSingleFuture/cashDistributionSingleFuture(Maker.Result)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3264dd6f-67db-4c2e-9809-f332584bb245</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/transStatus/transStatus</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d370a8cc-ca02-4daa-b182-482786a8407d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CashDistribution/cashDistributionSingleFuture/cashDistributionSingleFuture(TransStatus)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8f220850-05d4-46ad-8ccc-9af36d446929</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CashDistribution/cashDistributionSingleFuture/cashDistributionSingleFuture(TransStatus_2)</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
