import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

String SetTextFileTemplate = 'MODULEAUTODEBITDNM'

String SetTextFileDescription = 'autoDebitImme'

String FolderAutoDebit = 'C:\\Users\\user\\Desktop\\Csv\\autodebityangbenar!.csv'

WebUI.setText(findTestObject('homelogin/homelogin.corporateid'), 'PCMKILLUA')

WebUI.setText(findTestObject('homelogin/homelogin.userid'), 'killua')

WebUI.setText(findTestObject('homelogin/homelogin.password'), 'Password123')

Random rand = new Random()

WebUI.click(findTestObject('homelogin/homelogin.submit'))

not_run: WebUI.waitForElementVisible(findTestObject('menu/BTN - clickMenu'), 0)

WebUI.delay(6)

WebUI.click(findTestObject('menu/BTN - clickMenu'))

WebUI.delay(2)

WebUI.click(findTestObject('menu/collectionManagement/BTN - CollMaClick'))

WebUI.delay(2)

WebUI.click(findTestObject('Auto Debit/BTN - AutoDebitClick'))

WebUI.delay(6)

WebUI.click(findTestObject('Auto Debit/DL - FileTemplateClick'))

WebUI.delay(2)

WebUI.setText(findTestObject('Auto Debit/New Entry/TF - Set Text Drop List'), SetTextFileTemplate, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Auto Debit/BTN - choicesAll(PalingAtas)'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Auto Debit/New Entry/BTN - File Upload - Choose File'), FolderAutoDebit, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

int RandFileDesc

RandFileDesc = rand.nextInt(9999)

RandFileDesc1 = (('' + SetTextFileDescription) + RandFileDesc)

WebUI.setText(findTestObject('Auto Debit/New Entry/TF - File Description'), RandFileDesc1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2)

WebUI.click(findTestObject('Auto Debit/New Entry/BTN - Continue'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Auto Debit/New Entry/BTN - Continue'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(12)

WebUI.click(findTestObject('Auto Debit/Bucket/BTN - Bucket'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

WebUI.click(findTestObject('Auto Debit/Bucket/BTN - File Name - Next'))

WebUI.delay(2)

LabelBucketDB = WebUI.getText(findTestObject('Auto Debit/BucketDetail/Label - BucketDetail'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('LabelBucketDB', LabelBucketDB)

WebUI.verifyMatch(LabelBucketDB, GlobalVariable.confirmScreens.get('LabelBucketDB'), false)

FileUploadStatusDB = WebUI.getText(findTestObject('Auto Debit/BucketDetail/Label - File Upload Status - Comp'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('FileUploadStatusDB', FileUploadStatusDB)

WebUI.verifyMatch(FileUploadStatusDB, GlobalVariable.confirmScreens.get('FileUploadStatusDB'), false)

FileTypeDB = WebUI.getText(findTestObject('Auto Debit/BucketDetail/Label - File Type'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('FileTypeDB', FileTypeDB)

WebUI.verifyMatch(FileTypeDB, GlobalVariable.confirmScreens.get('FileTypeDB'), false)

FileTemplateDB = WebUI.getText(findTestObject('Auto Debit/BucketDetail/Label - File Format'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('FileTemplateDB', FileTemplateDB)

WebUI.verifyMatch(FileTemplateDB, GlobalVariable.confirmScreens.get('FileTemplateDB'), false)

FileUploadDB = WebUI.getText(findTestObject('Auto Debit/BucketDetail/Label - File Upload'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('FileUploadDB', FileUploadDB)

WebUI.verifyMatch(FileUploadDB, GlobalVariable.confirmScreens.get('FileUploadDB'), false)

FileDescriptionDB = WebUI.getText(findTestObject('Auto Debit/BucketDetail/Label - File Description'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('FileDescriptionDB', FileDescriptionDB)

WebUI.verifyMatch(FileDescriptionDB, GlobalVariable.confirmScreens.get('FileDescriptionDB'), false)

TransactionTypeDB = WebUI.getText(findTestObject('Auto Debit/BucketDetail/Label - Transaction Type'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('TransactionTypeDB', TransactionTypeDB)

WebUI.verifyMatch(TransactionTypeDB, GlobalVariable.confirmScreens.get('TransactionTypeDB'), false)

TotalRecordInFileDB = WebUI.getText(findTestObject('Auto Debit/BucketDetail/Label - Total Record In File'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('TotalRecordInFileDB', TotalRecordInFileDB)

WebUI.verifyMatch(TotalRecordInFileDB, GlobalVariable.confirmScreens.get('TotalRecordInFileDB'), false)

TotalTransactionRecordDB = WebUI.getText(findTestObject('Auto Debit/BucketDetail/Label - Total Transaction Record'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('TotalTransactionRecordDB', TotalTransactionRecordDB)

WebUI.verifyMatch(TotalTransactionRecordDB, GlobalVariable.confirmScreens.get('TotalTransactionRecordDB'), false)

TotalSuccesDB = WebUI.getText(findTestObject('Auto Debit/BucketDetail/Label - Total Succes'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('TotalSuccesDB', TotalSuccesDB)

WebUI.verifyMatch(TotalSuccesDB, GlobalVariable.confirmScreens.get('TotalSuccesDB'), false)

TotalFailedDB = WebUI.getText(findTestObject('Auto Debit/BucketDetail/Label - Total Failed'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('TotalFailedDB', TotalFailedDB)

WebUI.verifyMatch(TotalFailedDB, GlobalVariable.confirmScreens.get('TotalFailedDB'), false)

TotalUploadAmountInIDRDB = WebUI.getText(findTestObject('Auto Debit/Bucket Confirm/Label - Total Transaction AmountinIDR'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('TotalUploadAmountInIDRDB', TotalUploadAmountInIDRDB)

WebUI.verifyMatch(TotalUploadAmountInIDRDB, GlobalVariable.confirmScreens.get('TotalUploadAmountInIDRDB'), false)

WebUI.click(findTestObject('Auto Debit/BucketDetail/BTN - Confirm'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

FileTypeD = WebUI.getText(findTestObject('Auto Debit/BucketDetail/Label - File Type'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('FileTypeD', FileTypeD)

WebUI.verifyMatch(FileTypeD, GlobalVariable.confirmScreens.get('FileTypeD'), false)

FileFormatD = WebUI.getText(findTestObject('Auto Debit/BucketDetail/Label - File Format'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('FileFormatD', FileFormatD)

WebUI.verifyMatch(FileFormatD, GlobalVariable.confirmScreens.get('FileFormatD'), false)

FileUploadD = WebUI.getText(findTestObject('Auto Debit/BucketDetail/Label - File Upload'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('FileUploadD', FileUploadD)

WebUI.verifyMatch(FileUploadD, GlobalVariable.confirmScreens.get('FileUploadD'), false)

FileDescriptionD = WebUI.getText(findTestObject('Auto Debit/BucketDetail/Label - File Description'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('FileDescriptionD', FileDescriptionD)

WebUI.verifyMatch(FileDescriptionD, GlobalVariable.confirmScreens.get('FileDescriptionD'), false)

TotalRecordInFileD = WebUI.getText(findTestObject('Auto Debit/BucketDetail/Label - Total Record In File'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('TotalRecordInFileD', TotalRecordInFileD)

WebUI.verifyMatch(TotalRecordInFileD, GlobalVariable.confirmScreens.get('TotalRecordInFileD'), false)

TotalTransactionRecordD = WebUI.getText(findTestObject('Auto Debit/BucketDetail/Label - Total Transaction Record'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('TotalTransactionRecordD', TotalTransactionRecordD)

WebUI.verifyMatch(TotalTransactionRecordD, GlobalVariable.confirmScreens.get('TotalTransactionRecordD'), false)

TotalUploadAmountInIDRD = WebUI.getText(findTestObject('Auto Debit/BucketDetail/Label - Total Upload Amount'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('TotalUploadAmountInIDRD', TotalUploadAmountInIDRD)

WebUI.verifyMatch(TotalUploadAmountInIDRD, GlobalVariable.confirmScreens.get('TotalUploadAmountInIDRD'), false)

FileTypeD2 = WebUI.getText(findTestObject('Auto Debit/BucketDetail/Label - File Type - Copy'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('FileTypeD2', FileTypeD2)

WebUI.verifyMatch(FileTypeD2, GlobalVariable.confirmScreens.get('FileTypeD2'), false)

FileTemplateD2 = WebUI.getText(findTestObject('Auto Debit/BucketDetail/Label - File Format - Copy'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('FileTemplateD2', FileTemplateD2)

WebUI.verifyMatch(FileTemplateD2, GlobalVariable.confirmScreens.get('FileTemplateD2'), false)

FileUploadD2 = WebUI.getText(findTestObject('Auto Debit/BucketDetail/Label - File Upload - Copy'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('FileUploadD2', FileUploadD2)

WebUI.verifyMatch(FileUploadD2, GlobalVariable.confirmScreens.get('FileUploadD2'), false)

FileDescriptionD2 = WebUI.getText(findTestObject('Auto Debit/BucketDetail/Label - File Description - Copy'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('FileDescriptionD2', FileDescriptionD2)

WebUI.verifyMatch(FileDescriptionD2, GlobalVariable.confirmScreens.get('FileDescriptionD2'), false)

DebitAccountD = WebUI.getText(findTestObject('Auto Debit/Bucket Confirm/Label - Credit Account'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('DebitAccountD', DebitAccountD)

WebUI.verifyMatch(DebitAccountD, GlobalVariable.confirmScreens.get('DebitAccountD'), false)

insDateD = WebUI.getText(findTestObject('Auto Debit/Bucket Confirm/Label - instructionDateChecker'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('insDateD', insDateD)

WebUI.verifyMatch(insDateD, GlobalVariable.confirmScreens.get('insDateD'), false)

TotalTransactionRecordD = WebUI.getText(findTestObject('Auto Debit/Bucket Confirm/Label - Total Transaction Record - 2'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('TotalTransactionRecordD', TotalTransactionRecordD)

WebUI.verifyMatch(TotalTransactionRecordD, GlobalVariable.confirmScreens.get('TotalTransactionRecordD'), false)

TotalTransactionAmountD = WebUI.getText(findTestObject('Auto Debit/Bucket Confirm/Label - Total Transaction AmountinIDR'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('TotalTransactionAmountD', TotalTransactionAmountD)

WebUI.verifyMatch(TotalTransactionAmountD, GlobalVariable.confirmScreens.get('TotalTransactionAmountD'), false)

ExchangeRateD = WebUI.getText(findTestObject('Auto Debit/Bucket Confirm/Label - Exchange Rate'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('ExchangeRateD', ExchangeRateD)

WebUI.verifyMatch(ExchangeRateD, GlobalVariable.confirmScreens.get('ExchangeRateD'), false)

AutoDebitFeeD = WebUI.getText(findTestObject('Auto Debit/Bucket Confirm/Label - Autodebit Fee'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('AutoDebitFeeD', AutoDebitFeeD)

WebUI.verifyMatch(AutoDebitFeeD, GlobalVariable.confirmScreens.get('AutoDebitFeeD'), false)

TotalChargesD = WebUI.getText(findTestObject('Auto Debit/Bucket Confirm/Label - Total Charges'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('TotalChargesD', TotalChargesD)

WebUI.verifyMatch(TotalChargesD, GlobalVariable.confirmScreens.get('TotalChargesD'), false)

TotalDebitAmountD = WebUI.getText(findTestObject('Auto Debit/Bucket Confirm/Label - Total Debit Amount'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('TotalDebitAmountD', TotalDebitAmountD)

WebUI.verifyMatch(TotalDebitAmountD, GlobalVariable.confirmScreens.get('TotalDebitAmountD'), false)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Auto Debit/Bucket Confirm/BTN - Total Transaction Record - See Detail Record'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

CreditAccountRL = WebUI.getText(findTestObject('Auto Debit/List Record/Label - Credit Account'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('CreditAccountRL', CreditAccountRL)

WebUI.verifyMatch(CreditAccountRL, GlobalVariable.confirmScreens.get('CreditAccountRL'), false)

InstructionDateRL = WebUI.getText(findTestObject('Auto Debit/List Record/Label - Instruction Date'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('InstructionDateRL', InstructionDateRL)

WebUI.verifyMatch(InstructionDateRL, GlobalVariable.confirmScreens.get('InstructionDateRL'), false)

TotalTransactionRecordRL = WebUI.getText(findTestObject('Auto Debit/List Record/Label - Total Transaction Record'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('TotalTransactionRecordRL', TotalTransactionRecordRL)

WebUI.verifyMatch(TotalTransactionRecordRL, GlobalVariable.confirmScreens.get('TotalTransactionRecordRL'), false)

TotalTransactionAmountRL = WebUI.getText(findTestObject('Auto Debit/List Record/Label - Total Transaction AmountinIDR'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('TotalTransactionAmountRL', TotalTransactionAmountRL)

WebUI.verifyMatch(TotalTransactionAmountRL, GlobalVariable.confirmScreens.get('TotalTransactionAmountRL'), false)

AutoDebitFeeRL = WebUI.getText(findTestObject('Auto Debit/List Record/Label - Transfer Fee'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('AutoDebitFeeRL', AutoDebitFeeRL)

WebUI.verifyMatch(AutoDebitFeeRL, GlobalVariable.confirmScreens.get('AutoDebitFeeRL'), false)

TotalChargesRL = WebUI.getText(findTestObject('Auto Debit/List Record/Label - Total Charges'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('TotalChargesRL', TotalChargesRL)

WebUI.verifyMatch(TotalChargesRL, GlobalVariable.confirmScreens.get('TotalChargesRL'), false)

TotalDebitAmountRL = WebUI.getText(findTestObject('Auto Debit/List Record/Label - Total Debit Amount'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('TotalDebitAmountRL', TotalDebitAmountRL)

WebUI.verifyMatch(TotalDebitAmountRL, GlobalVariable.confirmScreens.get('TotalDebitAmountRL'), false)

WebUI.getAttribute(findTestObject('Auto Debit/List Record/BTN - Next Page - Kosong'), 'class', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.callTestCase(findTestCase('autoDebit/autoDebitMultiImme/autoDebitMultiImme(getTable)'), [('btnnext') : findTestObject(
            'Auto Debit/List Record/BTN - Next Page'), ('btnnextattribute') : findTestObject('Auto Debit/List Record/BTN - Next Page - Kosong')
        , ('canColumn') : 2, ('danColumn') : 3], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Auto Debit/List Record/BTN - Close'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2)

WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/responseCode'), '123456')

WebUI.scrollToElement(findTestObject('menu/trfManagement/bulkTransfer/BTN - btnSubmit'), 0)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/BTN - btnSubmit'))

WebUI.delay(2)

WebUI.click(findTestObject('Auto Debit/BTN - submitEnding(Ok)'))

WebUI.delay(19)

