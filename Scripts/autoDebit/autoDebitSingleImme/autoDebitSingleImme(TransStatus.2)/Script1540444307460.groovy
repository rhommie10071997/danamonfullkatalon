import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(9, FailureHandling.CONTINUE_ON_FAILURE)

TransactionReferenceNo = WebUI.getText(findTestObject('menu/report/transStatus/checker(autoDebit)/Label - transRefChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TransactionReferenceNo, GlobalVariable.confirmScreens.get('refNum'), false, FailureHandling.CONTINUE_ON_FAILURE)

MenuD = WebUI.getText(findTestObject('menu/report/transStatus/checker(autoDebit)/Label - menuChecker'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(MenuD, GlobalVariable.confirmScreens.get('MenuD'), false, FailureHandling.CONTINUE_ON_FAILURE)

Product = WebUI.getText(findTestObject('menu/report/transStatus/checker(autoDebit)/Label - productChecker'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(Product, GlobalVariable.confirmScreens.get('ProductD'), false, FailureHandling.CONTINUE_ON_FAILURE)

fileType = WebUI.getText(findTestObject('menu/report/transStatus/checker(autoDebit)/Label - fileTypeChecker'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(fileType, GlobalVariable.confirmScreens.get('FileTypeD'), false, FailureHandling.CONTINUE_ON_FAILURE)

fileFormat = WebUI.getText(findTestObject('menu/report/transStatus/checker(autoDebit)/Label - fileFormatChecker'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(fileFormat, GlobalVariable.confirmScreens.get('FileFormatD'), false, FailureHandling.CONTINUE_ON_FAILURE)

FileUpload = WebUI.getText(findTestObject('menu/report/transStatus/checker(autoDebit)/Label - fileUploadChecker'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(FileUpload, GlobalVariable.confirmScreens.get('FileUploadD'), false, FailureHandling.CONTINUE_ON_FAILURE)

FileDescription = WebUI.getText(findTestObject('menu/report/transStatus/checker(autoDebit)/Label - fileDescriptionChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(FileDescription, GlobalVariable.confirmScreens.get('FileDescriptionD'), false, FailureHandling.CONTINUE_ON_FAILURE)

creditAccountChecker = WebUI.getText(findTestObject('menu/report/transStatus/checker(autoDebit)/Label - creditAccountChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(creditAccountChecker, GlobalVariable.confirmScreens.get('creditAccountChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

TotalTransactionRecord = WebUI.getText(findTestObject('menu/report/transStatus/checker(autoDebit)/Label - totalTransactionRecordChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TotalTransactionRecord, GlobalVariable.confirmScreens.get('TotalTransactionRecordD'), false, FailureHandling.CONTINUE_ON_FAILURE)

TotalTransactionAmount = WebUI.getText(findTestObject('menu/report/transStatus/checker(autoDebit)/Label - totalTransactionAmountChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TotalTransactionAmount, GlobalVariable.confirmScreens.get('TotalTransactionAmountD'), false, FailureHandling.CONTINUE_ON_FAILURE)

exchangeRate = WebUI.getText(findTestObject('menu/report/transStatus/checker(autoDebit)/Label - exchangeRateChecker'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(exchangeRate, GlobalVariable.confirmScreens.get('ExchangeRateD'), false, FailureHandling.CONTINUE_ON_FAILURE)

autoDebit = WebUI.getText(findTestObject('menu/report/transStatus/checker(autoDebit)/Label - autoDebitFeeChecker'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(autoDebit, GlobalVariable.confirmScreens.get('AutoDebitFeeD'), false, FailureHandling.CONTINUE_ON_FAILURE)

totalCharges = WebUI.getText(findTestObject('menu/report/transStatus/checker(autoDebit)/Label - totalChargesChecker'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(totalCharges, GlobalVariable.confirmScreens.get('TotalChargesD'), false, FailureHandling.CONTINUE_ON_FAILURE)

totalDebitAmount = WebUI.getText(findTestObject('menu/report/transStatus/checker(autoDebit)/Label - totalDebitAmountChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(totalDebitAmount, GlobalVariable.confirmScreens.get('TotalDebitAmountD'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/report/transStatus/checker(autoDebit)/Label - recordListClick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

CreditAccountRL = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(autoDebit)/recordList/Label - creditAccountChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

insDateRL = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(autoDebit)/recordList/Label - insDateChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TotalTransactionRecordRL = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(autoDebit)/recordList/Label - totalTransRecordChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TotalTransactionAmountRL = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(autoDebit)/recordList/Label - totalTransactionAmount'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TransferFeeRL = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(autoDebit)/recordList/Label - autoDebitFeeChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TotalDebitAmountRL = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(autoDebit)/recordList/Label - totalDebitAmountChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TotalChargesRL = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(autoDebit)/recordList/Label - totalChargesChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(CreditAccountRL, GlobalVariable.confirmScreens.get('CreditAccountRL'), false, FailureHandling.CONTINUE_ON_FAILURE)

insDateRL = insDateRL.replace(':00.0', '')

WebUI.verifyMatch(insDateRL, GlobalVariable.confirmScreens.get('InstructionDateRL'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TotalTransactionRecordRL, GlobalVariable.confirmScreens.get('TotalTransactionRecordRL'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TotalTransactionAmountRL, GlobalVariable.confirmScreens.get('TotalTransactionAmountRL'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TransferFeeRL, GlobalVariable.confirmScreens.get('AutoDebitFeeRL'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TotalChargesRL, GlobalVariable.confirmScreens.get('TotalChargesRL'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TotalDebitAmountRL, GlobalVariable.confirmScreens.get('TotalDebitAmountRL'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.callTestCase(findTestCase('autoDebit/autoDebitMultiImme/autoDebitMultiImme(verifedTable.TransStatus)'), [('btnnext') : findTestObject(
            'Auto Debit/List Record/BTN - Next Page'), ('btnnextattribute') : findTestObject('Auto Debit/List Record/BTN - Next Page - Kosong')
        , ('canColumn') : 2, ('danColumn') : 3], FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(6, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/report/transStatus/recordList/recordListClose'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(20, FailureHandling.CONTINUE_ON_FAILURE)

