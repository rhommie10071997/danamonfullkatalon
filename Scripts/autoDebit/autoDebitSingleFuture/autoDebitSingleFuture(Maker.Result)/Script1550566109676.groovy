import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

confirmScreenStatus = WebUI.getText(findTestObject('Auto Debit/confirmScreenStatus/Label - confirmScreenStatus'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(confirmScreenStatus, confirmScreenStatus, false, FailureHandling.CONTINUE_ON_FAILURE)

ReferenceNumberD = WebUI.getText(findTestObject('Auto Debit/Bucket Confirm/Label - ReferenceNumber'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('ReferenceNumberD', ReferenceNumberD)

WebUI.verifyMatch(ReferenceNumberD, GlobalVariable.confirmScreens.get('ReferenceNumberD'), false, FailureHandling.CONTINUE_ON_FAILURE)

SubmitDateD = WebUI.getText(findTestObject('Auto Debit/Bucket Confirm/Label - SubmitDate'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('SubmitDateD', SubmitDateD)

WebUI.verifyMatch(SubmitDateD, GlobalVariable.confirmScreens.get('SubmitDateD'), false, FailureHandling.CONTINUE_ON_FAILURE)

FileTypeD = WebUI.getText(findTestObject('Auto Debit/BucketDetail/Label - File Type'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(FileTypeD, GlobalVariable.confirmScreens.get('FileTypeD'), false, FailureHandling.CONTINUE_ON_FAILURE)

FileFormatD = WebUI.getText(findTestObject('Auto Debit/BucketDetail/Label - File Format'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(FileFormatD, GlobalVariable.confirmScreens.get('FileFormatD'), false, FailureHandling.CONTINUE_ON_FAILURE)

FileUploadD = WebUI.getText(findTestObject('Auto Debit/BucketDetail/Label - File Upload'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(FileUploadD, GlobalVariable.confirmScreens.get('FileUploadD'), false, FailureHandling.CONTINUE_ON_FAILURE)

FileDescriptionD = WebUI.getText(findTestObject('Auto Debit/BucketDetail/Label - File Description'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(FileDescriptionD, GlobalVariable.confirmScreens.get('FileDescriptionD'), false, FailureHandling.CONTINUE_ON_FAILURE)

DebitAccountD = WebUI.getText(findTestObject('Auto Debit/Bucket Confirm/Label - Credit Account'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(DebitAccountD, GlobalVariable.confirmScreens.get('DebitAccountD'), false, FailureHandling.CONTINUE_ON_FAILURE)

insDateD = WebUI.getText(findTestObject('Auto Debit/Bucket Confirm/Label - instructionDateChecker'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(insDateD, GlobalVariable.confirmScreens.get('insDateD'), false, FailureHandling.CONTINUE_ON_FAILURE)

TotalTransactionRecordD = WebUI.getText(findTestObject('Auto Debit/BucketDetail/Label - Total Transaction Record'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TotalTransactionRecordD, GlobalVariable.confirmScreens.get('TotalTransactionRecordD'), false, FailureHandling.CONTINUE_ON_FAILURE)

TotalTransactionAmountD = WebUI.getText(findTestObject('Auto Debit/Bucket Confirm/Label - Total Transaction AmountinIDR'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TotalTransactionAmountD, GlobalVariable.confirmScreens.get('TotalTransactionAmountD'), false, FailureHandling.CONTINUE_ON_FAILURE)

ExchangeRateD = WebUI.getText(findTestObject('Auto Debit/Bucket Confirm/Label - Exchange Rate'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(ExchangeRateD, GlobalVariable.confirmScreens.get('ExchangeRateD'), false, FailureHandling.CONTINUE_ON_FAILURE)

AutoDebitFeeD = WebUI.getText(findTestObject('Auto Debit/Bucket Confirm/Label - Autodebit Fee'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(AutoDebitFeeD, GlobalVariable.confirmScreens.get('AutoDebitFeeD'), false, FailureHandling.CONTINUE_ON_FAILURE)

TotalChargesD = WebUI.getText(findTestObject('Auto Debit/Bucket Confirm/Label - Total Charges'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TotalChargesD, GlobalVariable.confirmScreens.get('TotalChargesD'), false, FailureHandling.CONTINUE_ON_FAILURE)

TotalDebitAmountD = WebUI.getText(findTestObject('Auto Debit/Bucket Confirm/Label - Total Debit Amount'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TotalDebitAmountD, GlobalVariable.confirmScreens.get('TotalDebitAmountD'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Auto Debit/Bucket Confirm/BTN - Total Transaction Record - See Detail Record'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

CreditAccountRL = WebUI.getText(findTestObject('Auto Debit/List Record/Label - Credit Account'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(CreditAccountRL, GlobalVariable.confirmScreens.get('CreditAccountRL'), false, FailureHandling.CONTINUE_ON_FAILURE)

InstructionDateRL = WebUI.getText(findTestObject('Auto Debit/List Record/Label - Instruction Date'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(InstructionDateRL, GlobalVariable.confirmScreens.get('InstructionDateRL'), false, FailureHandling.CONTINUE_ON_FAILURE)

TotalTransactionRecordRL = WebUI.getText(findTestObject('Auto Debit/List Record/Label - Total Transaction Record'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TotalTransactionRecordRL, GlobalVariable.confirmScreens.get('TotalTransactionRecordRL'), false, FailureHandling.CONTINUE_ON_FAILURE)

TotalTransactionAmountRL = WebUI.getText(findTestObject('Auto Debit/List Record/Label - Total Transaction AmountinIDR'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TotalTransactionAmountRL, GlobalVariable.confirmScreens.get('TotalTransactionAmountRL'), false, FailureHandling.CONTINUE_ON_FAILURE)

AutoDebitFeeRL = WebUI.getText(findTestObject('Auto Debit/List Record/Label - Transfer Fee'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(AutoDebitFeeRL, GlobalVariable.confirmScreens.get('AutoDebitFeeRL'), false, FailureHandling.CONTINUE_ON_FAILURE)

TotalChargesRL = WebUI.getText(findTestObject('Auto Debit/List Record/Label - Total Charges'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TotalChargesRL, GlobalVariable.confirmScreens.get('TotalChargesRL'), false, FailureHandling.CONTINUE_ON_FAILURE)

TotalDebitAmountRL = WebUI.getText(findTestObject('Auto Debit/List Record/Label - Total Debit Amount'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TotalDebitAmountRL, GlobalVariable.confirmScreens.get('TotalDebitAmountRL'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.callTestCase(findTestCase('autoDebit/autoDebitMultiImme/autoDebitMultiImme(verifedTable)'), [('btnnext') : findTestObject(
            'Auto Debit/List Record/BTN - Next Page'), ('btnnextattribute') : findTestObject('Auto Debit/List Record/BTN - Next Page - Kosong')
        , ('canColumn') : 2, ('danColumn') : 3], FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Auto Debit/List Record/BTN - Close'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

refNum = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/checker/Label - refNumChecker'), FailureHandling.CONTINUE_ON_FAILURE)

String[] refNumSplit = refNum.split(' ')

GlobalVariable.confirmScreens.put('refNum', refNumSplit[2])

docNo = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/checker/Label - DocChecker'), FailureHandling.CONTINUE_ON_FAILURE)

String[] docNoSplit = docNo.split(' ')

GlobalVariable.confirmScreens.put('docNo', docNoSplit[2])

submitDate = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/checker/Label - submitDateChecker'), FailureHandling.CONTINUE_ON_FAILURE)

submitDate = submitDate.replace('Submitted Date ', '')

GlobalVariable.confirmScreens.put('submitDate', submitDate)

WebUI.delay(7, FailureHandling.CONTINUE_ON_FAILURE)

