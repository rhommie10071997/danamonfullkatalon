import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(15, FailureHandling.CONTINUE_ON_FAILURE)

confirmScreenStatus = WebUI.getText(findTestObject('Auto Debit/confirmScreenStatus/Label - confirmScreenStatus(Releaser)'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(confirmScreenStatus, confirmScreenStatus, true, FailureHandling.CONTINUE_ON_FAILURE)

MenuD = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(autoDebit)/Label - menuChecker'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(MenuD, GlobalVariable.confirmScreens.get('MenuD'), false, FailureHandling.CONTINUE_ON_FAILURE)

ProductD = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(autoDebit)/Label - productChecker'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(ProductD, GlobalVariable.confirmScreens.get('ProductD'), false, FailureHandling.CONTINUE_ON_FAILURE)

TransactionReferenceNoD = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(autoDebit)/Label - refNumChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TransactionReferenceNoD, GlobalVariable.confirmScreens.get('refNum'), false, FailureHandling.CONTINUE_ON_FAILURE)

DocumentNoD = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(autoDebit)/Label - documentCodeChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(DocumentNoD, GlobalVariable.confirmScreens.get('docNo'), false, FailureHandling.CONTINUE_ON_FAILURE)

SubmitDateD = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(autoDebit)/Label - submitDateChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(SubmitDateD, GlobalVariable.confirmScreens.get('submitDate'), false, FailureHandling.CONTINUE_ON_FAILURE)

FileTypeD = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(autoDebit)/Label - fileTypeChecker'), FailureHandling.CONTINUE_ON_FAILURE)

FileFormatD = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(autoDebit)/Label - fileFormatChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

FileUploadD = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(autoDebit)/Label - fileUploadChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

FileDescriptionD = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(autoDebit)/Label - fileDescrtiptionChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

creditAccountD = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(autoDebit)/Label - creditAccountChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

insDateD = WebUI.getText(findTestObject('Auto Debit/Bucket Confirm/Label - instructionDateChecker'), FailureHandling.CONTINUE_ON_FAILURE)

TotalTransactionRecordD = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(autoDebit)/Label - totalTransactionChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TotalTransactionAmountD = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(autoDebit)/Label - totalTransactionAmount'), 
    FailureHandling.CONTINUE_ON_FAILURE)

ExchangeRateD = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(autoDebit)/Label - exchangeRateChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

AutoDebitFeeD = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(autoDebit)/Label - autoDebitFeeChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TotalChargesD = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(autoDebit)/Label - totalChargesChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TotalDebitAmountD = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(autoDebit)/Label - totalDebitAmountChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(FileTypeD, GlobalVariable.confirmScreens.get('FileTypeD'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(FileFormatD, GlobalVariable.confirmScreens.get('FileFormatD'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(FileUploadD, GlobalVariable.confirmScreens.get('FileUploadD'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(FileDescriptionD, GlobalVariable.confirmScreens.get('FileDescriptionD'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(creditAccountD, GlobalVariable.confirmScreens.get('DebitAccountD'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(insDateD, GlobalVariable.confirmScreens.get('insDateD'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TotalTransactionRecordD, GlobalVariable.confirmScreens.get('TotalTransactionRecordD'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TotalTransactionAmountD, GlobalVariable.confirmScreens.get('TotalTransactionAmountD'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(ExchangeRateD, GlobalVariable.confirmScreens.get('ExchangeRateD'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(AutoDebitFeeD, GlobalVariable.confirmScreens.get('AutoDebitFeeD'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TotalChargesD, GlobalVariable.confirmScreens.get('TotalChargesD'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TotalDebitAmountD, GlobalVariable.confirmScreens.get('TotalDebitAmountD'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Auto Debit/Bucket Confirm/BTN - Total Transaction Record - See Detail Record'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

CreditAccountRL = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(autoDebit)/recordList/Label - creditAccountChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

insDateRL = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(autoDebit)/recordList/Label - insDateChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TotalTransactionRecordRL = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(autoDebit)/recordList/Label - totalTransRecordChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TotalTransactionAmountRL = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(autoDebit)/recordList/Label - totalTransactionAmount'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TransferFeeRL = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(autoDebit)/recordList/Label - autoDebitFeeChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TotalDebitAmountRL = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(autoDebit)/recordList/Label - totalDebitAmountChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TotalChargesRL = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(autoDebit)/recordList/Label - totalChargesChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(CreditAccountRL, GlobalVariable.confirmScreens.get('CreditAccountRL'), false)

WebUI.verifyMatch(insDateRL, GlobalVariable.confirmScreens.get('InstructionDateRL'), false)

WebUI.verifyMatch(TotalTransactionRecordRL, GlobalVariable.confirmScreens.get('TotalTransactionRecordRL'), false)

WebUI.verifyMatch(TotalTransactionAmountRL, GlobalVariable.confirmScreens.get('TotalTransactionAmountRL'), false)

WebUI.verifyMatch(TransferFeeRL, GlobalVariable.confirmScreens.get('AutoDebitFeeRL'), false)

WebUI.verifyMatch(TotalChargesRL, GlobalVariable.confirmScreens.get('TotalChargesRL'), false)

WebUI.verifyMatch(TotalDebitAmountRL, GlobalVariable.confirmScreens.get('TotalDebitAmountRL'), false)

WebUI.callTestCase(findTestCase('autoDebit/autoDebitMultiImme/autoDebitMultiImme(verifedTable)'), [('btnnext') : findTestObject(
            'Auto Debit/List Record/BTN - Next Page'), ('btnnextattribute') : findTestObject('Auto Debit/List Record/BTN - Next Page - Kosong')
        , ('canColumn') : 2, ('danColumn') : 3], FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/recordList/BTN - recordListClose(Cancel)'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(10, FailureHandling.CONTINUE_ON_FAILURE)

