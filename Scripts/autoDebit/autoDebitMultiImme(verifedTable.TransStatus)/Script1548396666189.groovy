import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint

totalpage = WebUI.getText(findTestObject('Auto Debit/List Record/Label - Total Page'))

int totalpageNumber = Integer.parseInt(totalpage)

getTable = new ArrayList()

WebDriver driver = DriverFactory.getWebDriver()

driver.switchTo().frame('login')

driver.switchTo().frame('detailRecordFrame')

for(int i=0;i<totalpageNumber;i++) {
	WebElement tables = driver.findElement(By.xpath('//table[@id="globalTableXX"]/tbody'))
	
	List<WebElement> rows = tables.findElements(By.tagName('tr'))
	
	table: for(int j=0;j<rows.size();j++){
		List<WebElement> cols = rows.get(j).findElements(By.tagName('td'))
		
		
		DAN = ''+cols.get(danColumn).getText()
		
		getTable.add(DAN)
		
	}

	
	
	WebUI.switchToDefaultContent()
	
	while(WebUI.getAttribute(btnnextattribute, 'class')=='next btn grid-nextpage'){
		WebUI.click(btnnext)
	}
	
}

driver.switchTo().frame('login')

driver.switchTo().frame('detailRecordFrame')

int total = Integer.parseInt(GlobalVariable.confirmScreens.get('TotalTransactionRecordRL'))
int count = 1
for(int k=0;k<4;k++){
	String GLgetTable = GlobalVariable.confirmScreens.get('getTable'+count)
	String []GLgetTableSplit = GLgetTable.split(" ")
	String GLgetTableResult = GLgetTableSplit[1]
	
	WebUI.verifyMatch(getTable.get(k), GLgetTableResult, false, FailureHandling.CONTINUE_ON_FAILURE)
	
	count++
}


WebUI.switchToDefaultContent()






