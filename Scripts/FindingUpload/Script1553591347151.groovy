import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.interactions.Actions as Actions
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW

WebDriver driver = DriverFactory.getWebDriver()

Actions action = new Actions(driver)

int count = 1

int flag = 0

String result = ''

while (flag == 0) {
    driver.switchTo().frame('login')

    driver.switchTo().frame('mainFrame')

    WebElement Table = driver.findElement(By.xpath(('//table[@id="globalTable"]/tbody/tr[' + count) + ']/td[3]/a'))

    result = Table.getText()

    data_1 = (('' + result) + '.csv')

    data_2 = ('' + GlobalVariable.confirmScreens.get('FileUploadName'))

    not_run: WebUI.verifyMatch(data_1, data_2, false, FailureHandling.CONTINUE_ON_FAILURE)

    if (data_1.equals(data_2)) {
        action.doubleClick(Table).perform()

        WebUI.switchToDefaultContent()

        break
    }
    
    count++

    WebUI.switchToDefaultContent()

    WebUI.delay(1)
}

