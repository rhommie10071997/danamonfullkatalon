import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.interactions.Actions as Actions
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import org.openqa.selenium.Keys as Keys

WebDriver driver = DriverFactory.getWebDriver()
Actions action = new Actions(driver)

flag = 0
count = 1


while (flag == 0) {
	driver.switchTo().frame('login')	
	driver.switchTo().frame('mainFrame')
	
   
	WebElement Table = driver.findElement(By.xpath('//table[@id="globalTable"]/tbody/tr['+ count +']/td[2]'))
	action.doubleClick(Table).perform()
	WebUI.switchToDefaultContent()
	alName = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreen/V-aliasName'), FailureHandling.CONTINUE_ON_FAILURE)
	alNameResult = alName.replace(': ', '')
	
    if (alNameResult.equals("minang kabau boing 123") ) {
		break
		
    }
        WebUI.click(findTestObject('BeneficiaryListAdd/BeneficiaryListApproverConfrimScreen/B-back'))
		count++
		
		WebUI.delay(2)
		
		WebUI.click(findTestObject('BeneficiaryListAdd/BeneficiaryListApproverEntryScreen/RB-dateRange'))
		
		WebUI.click(findTestObject('BeneficiaryListAdd/BeneficiaryListApproverEntryScreen/DL-menuSearch'))
		
		WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), 'Beneficiary List')
		
		WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), Keys.chord(Keys.ENTER))
		
		WebUI.delay(2)
   
		WebUI.switchToDefaultContent()
		
					while (WebUI.getAttribute(btnnextattribute, 'class') == 'next btn grid-nextpage') {
						WebUI.click(btnnext)
				}
}				

