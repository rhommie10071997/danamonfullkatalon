import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.junit.After as After
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

alNamecontoh = WebUI.getAttribute(findTestObject('BeneficiaryListEdit/ConfrimScreen/V-aliasName'), 'value', FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('aliasName',alNamecontoh)

WebUI.verifyMatch(alNamecontoh, GlobalVariable.UniversalVariable.get('aliasName'), true, FailureHandling.CONTINUE_ON_FAILURE)

email = WebUI.getAttribute(findTestObject('BeneficiaryListEdit/ConfrimScreen/V-email'), 'value', FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('email',email)
 
WebUI.verifyMatch(email, GlobalVariable.UniversalVariable.get('email'), true, FailureHandling.CONTINUE_ON_FAILURE)

sms1 = WebUI.getAttribute(findTestObject('BeneficiaryListEdit/ConfrimScreen/V-sms'), 'value', FailureHandling.CONTINUE_ON_FAILURE)

sms = sms1.replace(': ', '')

GlobalVariable.UniversalVariable.put('sms',sms)

WebUI.verifyMatch( sms, GlobalVariable.UniversalVariable.get('sms'), true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

bankType = WebUI.getText(findTestObject('BeneficiaryListEdit/ConfrimScreen/V-bankType'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('bankType',bankType)
 
WebUI.verifyMatch(bankType, GlobalVariable.UniversalVariable.get('bankType'), false, FailureHandling.CONTINUE_ON_FAILURE)

accNum = WebUI.getText(findTestObject('BeneficiaryListEdit/ConfrimScreen/V-accountNumber'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('accNum',accNum)
 
WebUI.verifyMatch(accNum, GlobalVariable.UniversalVariable.get('accNum'), false, FailureHandling.CONTINUE_ON_FAILURE)

accNam = WebUI.getText(findTestObject('BeneficiaryListEdit/ConfrimScreen/V-accountName'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('accNam',accNam)
 
WebUI.verifyMatch(accNam, GlobalVariable.UniversalVariable.get('accNam'), false, FailureHandling.CONTINUE_ON_FAILURE)

curr = WebUI.getText(findTestObject('BeneficiaryListEdit/ConfrimScreen/V-currency'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('curr',curr)
 
WebUI.verifyMatch(curr, GlobalVariable.UniversalVariable.get('curr'), false, FailureHandling.CONTINUE_ON_FAILURE)

domBankType = WebUI.getText(findTestObject('BeneficiaryListEdit/ConfrimScreen/V-domesticBankType'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('domBankType',domBankType)
 
WebUI.verifyMatch(domBankType, GlobalVariable.UniversalVariable.get('domBankType'), false, FailureHandling.CONTINUE_ON_FAILURE)

domAccNum = WebUI.getText(findTestObject('BeneficiaryListEdit/ConfrimScreen/V-domesticAccountNumber'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('domAccNum',domAccNum)
 
WebUI.verifyMatch(domAccNum,GlobalVariable.UniversalVariable.get('domAccNum'), false, FailureHandling.CONTINUE_ON_FAILURE)

domAccNam = WebUI.getText(findTestObject('BeneficiaryListEdit/ConfrimScreen/V-domesticAccountName'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('domAccNam',domAccNam)
 
WebUI.verifyMatch(domAccNam, GlobalVariable.UniversalVariable.get('domAccNam'), false, FailureHandling.CONTINUE_ON_FAILURE)

domBank = WebUI.getText(findTestObject('BeneficiaryListEdit/ConfrimScreen/V-domesticBank'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('domBank',domBank)
 
WebUI.verifyMatch(domBank, GlobalVariable.UniversalVariable.get('domBank'), false, FailureHandling.CONTINUE_ON_FAILURE)

intBankType = WebUI.getText(findTestObject('BeneficiaryListEdit/ConfrimScreen/V-InternationalBankType'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('intBankType',intBankType)
 

WebUI.verifyMatch(intBankType, GlobalVariable.UniversalVariable.get('intBankType'), false, FailureHandling.CONTINUE_ON_FAILURE)

intAccNum = WebUI.getText(findTestObject('BeneficiaryListEdit/ConfrimScreen/V-internationalAccountNumber'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('intAccNum',intAccNum)
 
WebUI.verifyMatch(intAccNum, GlobalVariable.UniversalVariable.get('intAccNum'), false, FailureHandling.CONTINUE_ON_FAILURE)

intAccNam = WebUI.getText(findTestObject('BeneficiaryListEdit/ConfrimScreen/V-internationalAccountName'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('intAccNam',intAccNam)

WebUI.verifyMatch(intAccNam , GlobalVariable.UniversalVariable.get('intAccNam'), false, FailureHandling.CONTINUE_ON_FAILURE)

intBank = WebUI.getText(findTestObject('BeneficiaryListEdit/ConfrimScreen/V-internationalBank'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('intBank',intBank)
 

WebUI.verifyMatch( intBank, GlobalVariable.UniversalVariable.get('intBank'), false, FailureHandling.CONTINUE_ON_FAILURE)

intCoun = WebUI.getText(findTestObject('BeneficiaryListEdit/ConfrimScreen/V-internationalCountry'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('intCoun',intCoun)
 

WebUI.verifyMatch(intCoun, GlobalVariable.UniversalVariable.get('intCoun'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('BeneficiaryListEdit/SortingTableAscending'))

