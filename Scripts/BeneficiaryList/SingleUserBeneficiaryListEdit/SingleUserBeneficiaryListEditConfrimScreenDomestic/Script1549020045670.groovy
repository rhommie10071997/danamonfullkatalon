import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.junit.After as After
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.click(findTestObject('BeneficiaryListEdit/editScreen/domesticAccountNumber'), FailureHandling.CONTINUE_ON_FAILURE)

alNameDetails = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreenDetails/V-aliasNameDetails'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(alNameDetails, GlobalVariable.UniversalVariable.get('aliasName'), true, FailureHandling.CONTINUE_ON_FAILURE)

email = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreenDetails/V-emailDetails'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(email, GlobalVariable.UniversalVariable.get('email'), true, FailureHandling.CONTINUE_ON_FAILURE)

sms = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreenDetails/V-smsDetails'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(sms, GlobalVariable.UniversalVariable.get('sms'), true, FailureHandling.CONTINUE_ON_FAILURE)

bankTypeDetails = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreenDetails/V-bankTypeDetails'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(bankTypeDetails, GlobalVariable.UniversalVariable.get('domBankType'), true, FailureHandling.CONTINUE_ON_FAILURE)

accNumDetails = WebUI.getAttribute(findTestObject('BeneficiaryListEdit/ConfrimScreenDetails/V-accountNumberDetails'), 'value', 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(accNumDetails, GlobalVariable.UniversalVariable.get('domAccNum'), true, FailureHandling.CONTINUE_ON_FAILURE)

accNameDetails = WebUI.getAttribute(findTestObject('BeneficiaryListEdit/ConfrimScreenDetails/V-accountName'), 'value', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(accNameDetails, GlobalVariable.UniversalVariable.get('domAccNam'), true, FailureHandling.CONTINUE_ON_FAILURE)

add1 = WebUI.getAttribute(findTestObject('BeneficiaryListEdit/ConfrimScreenDetails/V-address1Details'), 'value', FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('add1', add1)

WebUI.verifyMatch(add1, GlobalVariable.UniversalVariable.get('add1'), true, FailureHandling.CONTINUE_ON_FAILURE)

add2 = WebUI.getAttribute(findTestObject('BeneficiaryListEdit/ConfrimScreenDetails/V-address2Details'), 'value', FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('add2', add2)

WebUI.verifyMatch(add2, GlobalVariable.UniversalVariable.get('add2'), true, FailureHandling.CONTINUE_ON_FAILURE)

add3 = WebUI.getAttribute(findTestObject('BeneficiaryListEdit/ConfrimScreenDetails/V-address3Details'), 'value', FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('add3', add3)

WebUI.verifyMatch(add3, GlobalVariable.UniversalVariable.get('add3'), true, FailureHandling.CONTINUE_ON_FAILURE)

beneResStatus = WebUI.getText(findTestObject('BeneficiaryListEdit/ConfrimScreenDetails/V-beneficiaryResidentStatusDetails'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('beneResStatus', beneResStatus)

WebUI.verifyMatch(beneResStatus, GlobalVariable.UniversalVariable.get('beneResStatus'), true, FailureHandling.CONTINUE_ON_FAILURE)

bankType = WebUI.getText(findTestObject('BeneficiaryListEdit/ConfrimScreenDetails/V-beneficiaryTypeDetails'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('bankType', bankType)

WebUI.verifyMatch(bankType, GlobalVariable.UniversalVariable.get('bankType'), true, FailureHandling.CONTINUE_ON_FAILURE)

beneType = WebUI.getText(findTestObject('BeneficiaryListEdit/ConfrimScreenDetails/V-beneficiaryBank'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('beneType', beneType)

WebUI.verifyMatch(beneType, GlobalVariable.UniversalVariable.get('beneType'), true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('BeneficiaryListEdit/ConfrimScreen/B-submit'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('BeneficiaryListEdit/SortingTableAscending'), FailureHandling.CONTINUE_ON_FAILURE)

