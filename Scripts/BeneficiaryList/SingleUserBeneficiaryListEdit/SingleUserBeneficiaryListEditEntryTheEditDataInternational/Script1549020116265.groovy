import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('BeneficiaryListEdit/editScreen/internationalAccountNumber'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('BeneficiaryListEdit/EntryEditDetailsScreen/TF-accountNumberInternational'), '3213213210', 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('BeneficiaryListEdit/EntryEditDetailsScreen/TF-accountNameInternational'), 'InternationalBankUpdate', 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('BeneficiaryListEdit/EntryEditDetailsScreen/DL-beneficiaryBankCountryInternational'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('BeneficiaryListEdit/EntryEditDetailsScreen/TF-ValueForDetails'), 'AUSTRALIA', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('BeneficiaryListEdit/EntryEditDetailsScreen/TF-ValueForDetails'), Keys.chord(Keys.ENTER), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('BeneficiaryListEdit/EntryEditDetailsScreen/DL-NationalOrganizationDirectory'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('BeneficiaryListEdit/EntryEditDetailsScreen/TF-ValueForDetails'), 'FED', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('BeneficiaryListEdit/EntryEditDetailsScreen/TF-ValueForDetails'), Keys.chord(Keys.ENTER), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('BeneficiaryListEdit/EntryEditDetailsScreen/DL-beneficiaryBankInternational'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('BeneficiaryListEdit/EntryEditDetailsScreen/TF-ValueForDetails'), 'bankai', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('BeneficiaryListEdit/EntryEditDetailsScreen/TF-ValueForDetails'), Keys.chord(Keys.ENTER), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2)

WebUI.click(findTestObject('BeneficiaryListEdit/ConfrimScreen/B-submit'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2)

WebUI.click(findTestObject('BeneficiaryListEdit/ApproverScreen/ConfrimScreenDetails/B-confrim'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-ApproverConfrimScreen/TF-response'), '123456', 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('BeneficiaryListEdit/ApproverScreen/ConfrimScreen/V-submit'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(4, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-ApproverConfrimScreen/V-okAfterApprove'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

