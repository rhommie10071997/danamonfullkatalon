import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.junit.After as After
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('BeneficiaryListEdit/ResultScreen(afterEdit)/ConfrimScreen/V-domesticAccountNumberClick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

alNameDetails1 = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreenDetails/V-aliasNameDetails'), 
    FailureHandling.CONTINUE_ON_FAILURE)

alNameDetails = alNameDetails1.replace(': ', '')

WebUI.verifyMatch(alNameDetails, GlobalVariable.UniversalVariable.get('aliasName'), true, FailureHandling.CONTINUE_ON_FAILURE)

email = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreenDetails/V-emailDetails'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(email, GlobalVariable.UniversalVariable.get('email'), true, FailureHandling.CONTINUE_ON_FAILURE)

sms = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreenDetails/V-smsDetails'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(sms, GlobalVariable.UniversalVariable.get('sms'), true, FailureHandling.CONTINUE_ON_FAILURE)

bankTypeDetails = WebUI.getText(findTestObject('BeneficiaryListEdit/ResultScreen(afterEdit)/ConfrimScreenDetails/V-bankTypeDetails'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(bankTypeDetails, GlobalVariable.UniversalVariable.get('domBankTypeEdited'), true, FailureHandling.CONTINUE_ON_FAILURE)

accNumDetails = WebUI.getText(findTestObject('BeneficiaryListEdit/ResultScreen(afterEdit)/ConfrimScreenDetails/V-accountNumberDetails'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(accNumDetails, GlobalVariable.UniversalVariable.get('domAccNumEdited'), true, FailureHandling.CONTINUE_ON_FAILURE)

accNameDetails = WebUI.getText(findTestObject('BeneficiaryListEdit/ResultScreen(afterEdit)/ConfrimScreenDetails/V-accountName'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(accNameDetails, GlobalVariable.UniversalVariable.get('domAccNamEdited'), true, FailureHandling.CONTINUE_ON_FAILURE)

add1 = WebUI.getText(findTestObject('BeneficiaryListEdit/ResultScreen(afterEdit)/ConfrimScreenDetails/V-address1Details'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(add1, GlobalVariable.UniversalVariable.get('add1Edited'), true, FailureHandling.CONTINUE_ON_FAILURE)

add2 = WebUI.getText(findTestObject('BeneficiaryListEdit/ResultScreen(afterEdit)/ConfrimScreenDetails/V-address2Details'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(add2, GlobalVariable.UniversalVariable.get('add2Edited'), true, FailureHandling.CONTINUE_ON_FAILURE)

add3 = WebUI.getText(findTestObject('BeneficiaryListEdit/ResultScreen(afterEdit)/ConfrimScreenDetails/V-address3Details'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(add3, GlobalVariable.UniversalVariable.get('add3Edited'), true, FailureHandling.CONTINUE_ON_FAILURE)

beneResStatus = WebUI.getText(findTestObject('BeneficiaryListEdit/ResultScreen(afterEdit)/ConfrimScreenDetails/V-beneficiaryResidentStatusDetails'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(beneResStatus, GlobalVariable.UniversalVariable.get('beneResStatusEdited'), true, FailureHandling.CONTINUE_ON_FAILURE)

beneBank = WebUI.getText(findTestObject('BeneficiaryListEdit/ResultScreen(afterEdit)/ConfrimScreenDetails/V-beneficiaryBank'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(beneBank, GlobalVariable.UniversalVariable.get('beneBankEdited'), true, FailureHandling.CONTINUE_ON_FAILURE)

beneType = WebUI.getText(findTestObject('BeneficiaryListEdit/ResultScreen(afterEdit)/ConfrimScreenDetails/V-beneficiaryBank'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('beneTypeEdited', beneType)

WebUI.verifyMatch(beneType, GlobalVariable.UniversalVariable.get('beneTypeEdited'), true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('BeneficiaryListAdd/B-closeForDetailsFrame'), FailureHandling.CONTINUE_ON_FAILURE)

