import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.junit.After as After
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(2)

WebUI.click(findTestObject('BeneficiaryListEdit/ApproverScreen/ConfrimScreen/V-IntAccNum(Extended)'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2)

alNameDetails1 = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreenDetails/V-aliasNameDetails'), 
    FailureHandling.CONTINUE_ON_FAILURE)

alNameDetails = alNameDetails1.replace(': ', '')

WebUI.verifyMatch(alNameDetails, GlobalVariable.UniversalVariable.get('aliasName'), true, FailureHandling.CONTINUE_ON_FAILURE)

email1 = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreenDetails/V-emailDetails'), FailureHandling.CONTINUE_ON_FAILURE)

email = email1.replace(': ', '')

WebUI.verifyMatch(email, GlobalVariable.UniversalVariable.get('email'), true, FailureHandling.CONTINUE_ON_FAILURE)

sms1 = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreenDetails/V-smsDetails'), FailureHandling.CONTINUE_ON_FAILURE)

sms = sms1.replace(': ', '')

WebUI.verifyMatch(sms, GlobalVariable.UniversalVariable.get('sms'), true, FailureHandling.CONTINUE_ON_FAILURE)

bankTypeDetails1 = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreenDetails/V-bankTypeDetails'), 
    FailureHandling.CONTINUE_ON_FAILURE)

bankTypeDetails = bankTypeDetails1

WebUI.verifyMatch(bankTypeDetails, GlobalVariable.UniversalVariable.get('intBankType'), true, FailureHandling.CONTINUE_ON_FAILURE)

accNumDetails1 = WebUI.getAttribute(findTestObject('BeneficiaryListEdit/ConfrimScreenDetails/V-accountNumberDetails'), 'value', 
    FailureHandling.CONTINUE_ON_FAILURE)

accNumDetails = accNumDetails1

WebUI.verifyMatch(accNumDetails, GlobalVariable.UniversalVariable.get('intAccNum'), true, FailureHandling.CONTINUE_ON_FAILURE)

accNameDetails1 = WebUI.getAttribute(findTestObject('BeneficiaryListEdit/ConfrimScreenDetails/V-accountName'), 'value', 
    FailureHandling.CONTINUE_ON_FAILURE)

accNameDetails = accNameDetails1.replace(': ', '')

WebUI.verifyMatch(accNameDetails, GlobalVariable.UniversalVariable.get('intAccNam'), true, FailureHandling.CONTINUE_ON_FAILURE)

add11 = WebUI.getAttribute(findTestObject('BeneficiaryListEdit/ConfrimScreenDetails/V-address1Details'), 'value', FailureHandling.CONTINUE_ON_FAILURE)

intAdd1 = add11.replace(': ', '')

GlobalVariable.UniversalVariable.put('intAdd1', intAdd1)

WebUI.verifyMatch(intAdd1, GlobalVariable.UniversalVariable.get('intAdd1'), true, FailureHandling.CONTINUE_ON_FAILURE)

add22 = WebUI.getAttribute(findTestObject('BeneficiaryListEdit/ConfrimScreenDetails/V-address2Details'), 'value', FailureHandling.CONTINUE_ON_FAILURE)

intAdd2 = add22.replace(': ', '')

GlobalVariable.UniversalVariable.put('intAdd2', intAdd2)

WebUI.verifyMatch(intAdd2, GlobalVariable.UniversalVariable.get('intAdd2'), true, FailureHandling.CONTINUE_ON_FAILURE)

beneResStatus = WebUI.getText(findTestObject('BeneficiaryListEdit/ConfrimScreenDetails/V-beneficiaryResidentStatusDetails'))

GlobalVariable.UniversalVariable.put('beneResStatus', beneResStatus)

WebUI.verifyMatch(beneResStatus, GlobalVariable.UniversalVariable.get('beneResStatus'), true, FailureHandling.CONTINUE_ON_FAILURE)

beneBankCou = WebUI.getText(findTestObject('BeneficiaryListEdit/ConfrimScreenDetails/V-beneficiaryBankCounty'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('beneBankCou', beneBankCou)

WebUI.verifyMatch(beneBankCou, GlobalVariable.UniversalVariable.get('beneBankCou'), true, FailureHandling.CONTINUE_ON_FAILURE)

natOrgaDirec = WebUI.getText(findTestObject('BeneficiaryListEdit/ConfrimScreenDetails/V-nationalOrganizationDirectory'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('natOrgaDirec', natOrgaDirec)

WebUI.verifyMatch(natOrgaDirec, GlobalVariable.UniversalVariable.get('natOrgaDirec'), true, FailureHandling.CONTINUE_ON_FAILURE)

intBeneType = WebUI.getText(findTestObject('BeneficiaryListEdit/ConfrimScreenDetails/V-beneficiaryBankInternationalDetails'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('intBeneType', intBeneType)

WebUI.verifyMatch(intBeneType, GlobalVariable.UniversalVariable.get('intBeneType'), true, FailureHandling.CONTINUE_ON_FAILURE)

intermBank = WebUI.getText(findTestObject('BeneficiaryListEdit/ConfrimScreenDetails/V-intermediaryBank'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('intermBank', intermBank)

WebUI.verifyMatch(intermBank, GlobalVariable.UniversalVariable.get('intermBank'), true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2)

WebUI.click(findTestObject('BeneficiaryListEdit/ConfrimScreen/B-submit'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2)

WebUI.click(findTestObject('BeneficiaryListEdit/SortingTableAscending'))

