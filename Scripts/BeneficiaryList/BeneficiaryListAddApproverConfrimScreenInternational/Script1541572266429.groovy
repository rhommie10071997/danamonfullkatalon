import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.junit.After as After
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.click(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreenDetails/V-internationalAccountNumber'), FailureHandling.CONTINUE_ON_FAILURE)

alNameDetails = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreenDetails/V-aliasNameDetails'), 
    FailureHandling.CONTINUE_ON_FAILURE)

AliasNameDetails = alNameDetails.replace(': ', '')

WebUI.verifyMatch(AliasNameDetails, GlobalVariable.UniversalVariable.get('aliasName'), true, FailureHandling.CONTINUE_ON_FAILURE)

email = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreenDetails/V-emailDetails'), FailureHandling.CONTINUE_ON_FAILURE)

Email = email.replace(': ', '')

WebUI.verifyMatch(Email, GlobalVariable.UniversalVariable.get('email'), true, FailureHandling.CONTINUE_ON_FAILURE)

sms = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreenDetails/V-smsDetails'), FailureHandling.CONTINUE_ON_FAILURE)

Sms = sms.replace(': ', '')

WebUI.verifyMatch(Sms, GlobalVariable.UniversalVariable.get('sms'), true, FailureHandling.CONTINUE_ON_FAILURE)

bankTypeDetails = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreenDetails/V-bankTypeDetails'), 
    FailureHandling.CONTINUE_ON_FAILURE)

BankTypeDetails = bankTypeDetails

WebUI.verifyMatch(BankTypeDetails, GlobalVariable.UniversalVariable.get('InternationalBankType'), true, FailureHandling.CONTINUE_ON_FAILURE)

accNumDetails = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreenDetails/V-accountNumberDetails'), 
    FailureHandling.CONTINUE_ON_FAILURE)

AccountNumberDetails = accNumDetails

WebUI.verifyMatch(AccountNumberDetails, GlobalVariable.UniversalVariable.get('accNumberInternational'), true, FailureHandling.CONTINUE_ON_FAILURE)

accNameDetails = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreenDetails/V-accountName'), FailureHandling.CONTINUE_ON_FAILURE)

AccountNameDetails = accNameDetails.replace(': ', '')

WebUI.verifyMatch(AccountNameDetails, GlobalVariable.UniversalVariable.get('accNameInternational'), true, FailureHandling.CONTINUE_ON_FAILURE)

add1 = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreenDetails/V-address1Details'), FailureHandling.CONTINUE_ON_FAILURE)

Address1Details = add1.replace(': ', '')

WebUI.verifyMatch(Address1Details, GlobalVariable.UniversalVariable.get('internationalAdd1'), true, FailureHandling.CONTINUE_ON_FAILURE)

add2 = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreenDetails/V-address2Details'), FailureHandling.CONTINUE_ON_FAILURE)

Address2Details = add2.replace(': ', '')

WebUI.verifyMatch(Address2Details, GlobalVariable.UniversalVariable.get('internationalAdd2'), true, FailureHandling.CONTINUE_ON_FAILURE)

beneResStatus = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreenDetails/V-beneficiaryResidentStatusDetails'), 
    FailureHandling.CONTINUE_ON_FAILURE)

BeneficiaryResidentStatusDetails = beneResStatus

WebUI.verifyMatch(BeneficiaryResidentStatusDetails, GlobalVariable.UniversalVariable.get('BeneficiaryResidentStatusDetails'), 
    true, FailureHandling.CONTINUE_ON_FAILURE)

beneBankCou = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreenDetails/V-beneficiaryBankCounty'), 
    FailureHandling.CONTINUE_ON_FAILURE)

BeneficiaryBankCountryDetails = beneBankCou

WebUI.verifyMatch(BeneficiaryBankCountryDetails, GlobalVariable.UniversalVariable.get('InternationalCountry'), true, FailureHandling.CONTINUE_ON_FAILURE)

natOrgaDirec = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreenDetails/V-nationalOrganizationDirectory'), 
    FailureHandling.CONTINUE_ON_FAILURE)

NationalOrganizationDirectoryDetails = natOrgaDirec

WebUI.verifyMatch(NationalOrganizationDirectoryDetails, GlobalVariable.UniversalVariable.get('nationalOrganizationDirectory'), 
    true, FailureHandling.CONTINUE_ON_FAILURE)

beneType = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreenDetails/V-beneficiaryBankInternationalDetails'), 
    FailureHandling.CONTINUE_ON_FAILURE)

BeneficiaryTypeDetails = beneType

WebUI.verifyMatch(BeneficiaryTypeDetails, GlobalVariable.UniversalVariable.get(''), true, FailureHandling.CONTINUE_ON_FAILURE)

intermBank = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreenDetails/V-intermediaryBank'), 
    FailureHandling.CONTINUE_ON_FAILURE)

IntermediaryBankDetails = intermBank

WebUI.verifyMatch(IntermediaryBankDetails, GlobalVariable.UniversalVariable.get('IntermediaryBankDetails'), true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('BeneficiaryListAdd/B-closeForDetailsFrame'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('SingleUserInhouse(immediate)/EntryScreen/B-Submit'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('SingleUserInhouse(immediate)/EntryScreen/B-Submit2'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('B-Approve'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-ApproverConfrimScreen/TF-response'), '123456')

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('B-Approve'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(4, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-ApproverConfrimScreen/V-okAfterApprove'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(10, FailureHandling.CONTINUE_ON_FAILURE)

