import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.junit.After as After
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.click(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreenDetails/V-doubleClick'), FailureHandling.CONTINUE_ON_FAILURE)

alNameDetails = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreenDetails/V-aliasNameDetails'), 
    FailureHandling.CONTINUE_ON_FAILURE)

AliasNameDetails = alNameDetails.replace(': ', '')

WebUI.verifyMatch(AliasNameDetails, GlobalVariable.UniversalVariable.get('aliasName'), true, FailureHandling.CONTINUE_ON_FAILURE)

email = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreenDetails/V-emailDetails'), FailureHandling.CONTINUE_ON_FAILURE)

Email = email.replace(': ', '')

WebUI.verifyMatch(Email, GlobalVariable.UniversalVariable.get('email'), true, FailureHandling.CONTINUE_ON_FAILURE)

sms = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreenDetails/V-smsDetails'), FailureHandling.CONTINUE_ON_FAILURE)

Sms = sms.replace(': ', '')

WebUI.verifyMatch(Sms, GlobalVariable.UniversalVariable.get('sms'), true, FailureHandling.CONTINUE_ON_FAILURE)

bankTypeDetails = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreenDetails/V-bankTypeDetails'), 
    FailureHandling.CONTINUE_ON_FAILURE)

BankTypeDetails = bankTypeDetails

WebUI.verifyMatch(BankTypeDetails, GlobalVariable.UniversalVariable.get('DomesticBankType'), true, FailureHandling.CONTINUE_ON_FAILURE)

accNumDetails = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreenDetails/V-accountNumberDetails'), 
    FailureHandling.CONTINUE_ON_FAILURE)

AccountNumberDetails = accNumDetails

WebUI.verifyMatch(AccountNumberDetails, GlobalVariable.UniversalVariable.get('domesticAccNumberDomestic'), true, FailureHandling.CONTINUE_ON_FAILURE)

accNameDetails = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreenDetails/V-accountName'), FailureHandling.CONTINUE_ON_FAILURE)

AccountNumberDetails = accNameDetails.replace(': ', '')

WebUI.verifyMatch(AccountNumberDetails, GlobalVariable.UniversalVariable.get('domesticAccNameDomestic'), true, FailureHandling.CONTINUE_ON_FAILURE)

add1 = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreenDetails/V-address1Details'), FailureHandling.CONTINUE_ON_FAILURE)

Address1Details = add1.replace(': ', '')

WebUI.verifyMatch(Address1Details, GlobalVariable.UniversalVariable.get('domesticAdd1'), true, FailureHandling.CONTINUE_ON_FAILURE)

add2 = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreenDetails/V-address2Details'), FailureHandling.CONTINUE_ON_FAILURE)

Address2Details = add2.replace(': ', '')

WebUI.verifyMatch(Address2Details, GlobalVariable.UniversalVariable.get('domesticAdd2'), true, FailureHandling.CONTINUE_ON_FAILURE)

add3 = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreenDetails/V-address3Details'), FailureHandling.CONTINUE_ON_FAILURE)

Address3Details = add3

WebUI.verifyMatch(Address3Details, GlobalVariable.UniversalVariable.get('domesticAdd3'), true, FailureHandling.CONTINUE_ON_FAILURE)

beneResStatus = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreenDetails/V-beneficiaryResidentStatusDetails'), 
    FailureHandling.CONTINUE_ON_FAILURE)

BeneficiaryResidentStatusDetails = beneResStatus

GlobalVariable.UniversalVariable.put('BeneficiaryResidentStatusDetails', BeneficiaryResidentStatusDetails)

WebUI.verifyMatch(BeneficiaryResidentStatusDetails, GlobalVariable.UniversalVariable.get('BeneficiaryResidentStatusDetails'), 
    true, FailureHandling.CONTINUE_ON_FAILURE)

bankType = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreenDetails/V-beneficiaryTypeDetails'), 
    FailureHandling.CONTINUE_ON_FAILURE)

BankTypeDetails = bankType

WebUI.verifyMatch(BankTypeDetails, GlobalVariable.UniversalVariable.get('DomesticBankType'), true, FailureHandling.CONTINUE_ON_FAILURE)

beneType = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreenDetails/V-beneficiaryBank'), FailureHandling.CONTINUE_ON_FAILURE)

BeneficiaryTypeDetails = beneType

WebUI.verifyMatch(BeneficiaryTypeDetails, GlobalVariable.UniversalVariable.get('domesticBeneType'), true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('BeneficiaryListAdd/B-closeForDetailsFrame'), FailureHandling.CONTINUE_ON_FAILURE)

