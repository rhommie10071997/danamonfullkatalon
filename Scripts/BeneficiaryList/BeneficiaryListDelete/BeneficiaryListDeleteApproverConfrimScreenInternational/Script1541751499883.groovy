import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.junit.After as After
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(2)

WebUI.click(findTestObject('BeneficiaryListDelete/BeneficiaryListDeleteConfrimScreen/Value-Delete/V-forInternationalAccountNumberClick'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2)

alNameDetails = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreenDetails/V-aliasNameDetails'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.ValueAliasNameDetails = alNameDetails.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.ValueAliasNameDetails, 'minang kabau boing 123', true, FailureHandling.CONTINUE_ON_FAILURE)

email = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreenDetails/V-emailDetails'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Email = email.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.Email, 'BottleMinum@gmail.com', true, FailureHandling.CONTINUE_ON_FAILURE)

sms = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreenDetails/V-smsDetails'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Sms = sms.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.Sms, '123123123', true, FailureHandling.CONTINUE_ON_FAILURE)

bankTypeDetails = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreenDetails/V-bankTypeDetails'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.ValueBankTypeDetails = bankTypeDetails

WebUI.verifyMatch(GlobalVariable.ValueBankTypeDetails, 'International Bank', true, FailureHandling.CONTINUE_ON_FAILURE)

accNumDetails = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreenDetails/V-accountNumberDetails'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.ValueAccountNumberDetails = accNumDetails

WebUI.verifyMatch(GlobalVariable.ValueAccountNumberDetails, '321321321', true, FailureHandling.CONTINUE_ON_FAILURE)

accNameDetails = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreenDetails/V-accountName'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.ValueAccountNumberDetails = accNameDetails.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.ValueAccountNumberDetails, 'InternationalBank', true, FailureHandling.CONTINUE_ON_FAILURE)

add1 = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreenDetails/V-address1Details'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.ValueAddress1Details = add1.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.ValueAddress1Details, 'JL Kebon', true, FailureHandling.CONTINUE_ON_FAILURE)

add2 = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreenDetails/V-address2Details'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.ValueAddress2Details = add2.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.ValueAddress2Details, 'JL Sirih', true, FailureHandling.CONTINUE_ON_FAILURE)

beneResStatus = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreenDetails/V-beneficiaryResidentStatusDetails'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.ValueBeneficiaryResidentStatusDetails = beneResStatus

WebUI.verifyMatch(GlobalVariable.ValueBeneficiaryResidentStatusDetails, 'Resident', true, FailureHandling.CONTINUE_ON_FAILURE)

beneBankCou = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreenDetails/V-beneficiaryBankCounty'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.ValueBeneficiaryBankCountryDetails = beneBankCou

WebUI.verifyMatch(GlobalVariable.ValueBeneficiaryBankCountryDetails, 'INDONESIA', true, FailureHandling.CONTINUE_ON_FAILURE)

natOrgaDirec = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreenDetails/V-nationalOrganizationDirectory'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.ValueNationalOrganizationDirectoryDetails = natOrgaDirec

WebUI.verifyMatch(GlobalVariable.ValueNationalOrganizationDirectoryDetails, 'FED1', true, FailureHandling.CONTINUE_ON_FAILURE)

beneType = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreenDetails/V-beneficiaryBankInternationalDetails'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.ValueBeneficiaryTypeDetails = beneType

WebUI.verifyMatch(GlobalVariable.ValueBeneficiaryTypeDetails, 'IND - BANKCIMB123', true, FailureHandling.CONTINUE_ON_FAILURE)

intermBank = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreenDetails/V-intermediaryBank'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.ValueIntermediaryBankDetails = intermBank

WebUI.verifyMatch(GlobalVariable.ValueIntermediaryBankDetails, 'rere123 - hgju ju', true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('BeneficiaryListAdd/B-closeForDetailsFrame'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('B-Approve'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-ApproverConfrimScreen/TF-response'), '123456')

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('B-Approve'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(4, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-ApproverConfrimScreen/V-okAfterApprove'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(10, FailureHandling.CONTINUE_ON_FAILURE)

