import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.junit.After as After
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

menName = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListApproverConfrimScreen/V-menuName'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.ValueMenuName = menName

WebUI.verifyMatch(GlobalVariable.ValueMenuName, 'Beneficiary List', true, FailureHandling.CONTINUE_ON_FAILURE)

subDate = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListApproverConfrimScreen/V-submittedDate'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.ValueSubmittedDate = subDate

WebUI.verifyMatch(GlobalVariable.ValueSubmittedDate, GlobalVariable.ValueSubmittedDate, false, FailureHandling.CONTINUE_ON_FAILURE)

alName = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreen/V-aliasName'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.aliasName = alName.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.aliasName, 'minang kabau boing 123', true, FailureHandling.CONTINUE_ON_FAILURE)

email = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreen/V-email'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Email = email.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.Email, 'BottleMinum@gmail.com', true, FailureHandling.CONTINUE_ON_FAILURE)

sms = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreen/V-sms'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Sms = sms.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.Sms, '123123123', true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

bankType = WebUI.getText(findTestObject('BeneficiaryListDelete/BeneficiaryListDeleteConfrimScreen/Value-Delete/V-bankType'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.ValueBankType = bankType

WebUI.verifyMatch(GlobalVariable.ValueBankType, 'My Bank', false, FailureHandling.CONTINUE_ON_FAILURE)

accNum = WebUI.getText(findTestObject('BeneficiaryListDelete/BeneficiaryListDeleteConfrimScreen/Value-Delete/V-accountNumber'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.ValueAccountNumber = accNum

WebUI.verifyMatch(GlobalVariable.ValueAccountNumber, '000002018547', false, FailureHandling.CONTINUE_ON_FAILURE)

accNam = WebUI.getText(findTestObject('BeneficiaryListDelete/BeneficiaryListDeleteConfrimScreen/Value-Delete/V-accountName'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.ValueAccountName = accNam

WebUI.verifyMatch(GlobalVariable.ValueAccountName, 'ABDUL MANNANG', false, FailureHandling.CONTINUE_ON_FAILURE)

curr = WebUI.getText(findTestObject('BeneficiaryListDelete/BeneficiaryListDeleteConfrimScreen/Value-Delete/V-currency'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.ValueCurrency = curr

WebUI.verifyMatch(GlobalVariable.ValueCurrency, 'IDR', false, FailureHandling.CONTINUE_ON_FAILURE)

domBankType = WebUI.getText(findTestObject('BeneficiaryListDelete/BeneficiaryListDeleteConfrimScreen/Value-Delete/V-domesticBankType'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.ValueDomesticBankType = domBankType

WebUI.verifyMatch(GlobalVariable.ValueDomesticBankType, 'Domestic Bank', false, FailureHandling.CONTINUE_ON_FAILURE)

domAccNum = WebUI.getText(findTestObject('BeneficiaryListDelete/BeneficiaryListDeleteConfrimScreen/Value-Delete/V-domesticAccountNumber'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.ValueDomesticAccountNumber = domAccNum

WebUI.verifyMatch(GlobalVariable.ValueDomesticAccountNumber, '123123123', false, FailureHandling.CONTINUE_ON_FAILURE)

domAccNam = WebUI.getText(findTestObject('BeneficiaryListDelete/BeneficiaryListDeleteConfrimScreen/Value-Delete/V-domesticAccountName'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.ValueDomesticAccountName = domAccNam

WebUI.verifyMatch(GlobalVariable.ValueDomesticAccountName, 'DomesticBank', false, FailureHandling.CONTINUE_ON_FAILURE)

domBank = WebUI.getText(findTestObject('BeneficiaryListDelete/BeneficiaryListDeleteConfrimScreen/Value-Delete/V-domesticBank'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.ValueDomesticBank = domBank

WebUI.verifyMatch(GlobalVariable.ValueDomesticBank, 'BNI', false, FailureHandling.CONTINUE_ON_FAILURE)

intBankType = WebUI.getText(findTestObject('BeneficiaryListDelete/BeneficiaryListDeleteConfrimScreen/Value-Delete/V-InternationalBankType'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.ValueInternationalBankType = intBankType

WebUI.verifyMatch(GlobalVariable.ValueInternationalBankType, 'International Bank', false, FailureHandling.CONTINUE_ON_FAILURE)

intAccNum = WebUI.getText(findTestObject('BeneficiaryListDelete/BeneficiaryListDeleteConfrimScreen/Value-Delete/V-internationalAccountNumber'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.ValueInternationalAccountNumber = intAccNum

WebUI.verifyMatch(GlobalVariable.ValueInternationalAccountNumber, '321321321', false, FailureHandling.CONTINUE_ON_FAILURE)

intAccNam = WebUI.getText(findTestObject('BeneficiaryListDelete/BeneficiaryListDeleteConfrimScreen/Value-Delete/V-internationalAccountName'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.ValueInternationalAccountName = intAccNam

WebUI.verifyMatch(GlobalVariable.ValueInternationalAccountName, 'InternationalBank', false, FailureHandling.CONTINUE_ON_FAILURE)

intBank = WebUI.getText(findTestObject('BeneficiaryListDelete/BeneficiaryListDeleteConfrimScreen/Value-Delete/V-internationalBank'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.ValueInternationalBank = intBank

WebUI.verifyMatch(GlobalVariable.ValueInternationalBank, 'INDONESIA', false, FailureHandling.CONTINUE_ON_FAILURE)

intCoun = WebUI.getText(findTestObject('BeneficiaryListDelete/BeneficiaryListDeleteConfrimScreen/Value-Delete/V-internationalCountry'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.ValueInternationalCountry = intCoun

WebUI.verifyMatch(GlobalVariable.ValueInternationalCountry, 'INDONESIA', false, FailureHandling.CONTINUE_ON_FAILURE)

