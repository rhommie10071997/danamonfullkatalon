import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.junit.After as After
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.click(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreenDetails/V-doubleClick'), FailureHandling.CONTINUE_ON_FAILURE)

alNameDetails = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreenDetails/V-aliasNameDetails'),
	FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.ValueAliasNameDetails = alNameDetails.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.ValueAliasNameDetails, 'minang kabau boing 123', true, FailureHandling.CONTINUE_ON_FAILURE)

email = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreenDetails/V-emailDetails'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Email = email.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.Email, 'BottleMinum@gmail.com', true, FailureHandling.CONTINUE_ON_FAILURE)

sms = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreenDetails/V-smsDetails'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Sms = sms.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.Sms, '123123123', true, FailureHandling.CONTINUE_ON_FAILURE)

bankTypeDetails = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreenDetails/V-bankTypeDetails'),
	FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.ValueBankTypeDetails = bankTypeDetails

WebUI.verifyMatch(GlobalVariable.ValueBankTypeDetails, 'Domestic Bank', true, FailureHandling.CONTINUE_ON_FAILURE)

accNumDetails = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreenDetails/V-accountNumberDetails'),
	FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.ValueAccountNumberDetails = accNumDetails

WebUI.verifyMatch(GlobalVariable.ValueAccountNumberDetails, '123123123', true, FailureHandling.CONTINUE_ON_FAILURE)

accNameDetails = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreenDetails/V-accountName'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.ValueAccountNumberDetails = accNameDetails.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.ValueAccountNumberDetails, 'DomesticBank', true, FailureHandling.CONTINUE_ON_FAILURE)

add1 = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreenDetails/V-address1Details'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.ValueAddress1Details = add1.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.ValueAddress1Details, 'Jl Wisma', true, FailureHandling.CONTINUE_ON_FAILURE)

add2 = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreenDetails/V-address2Details'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.ValueAddress2Details = add2.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.ValueAddress2Details, 'Jl Barito', true, FailureHandling.CONTINUE_ON_FAILURE)

add3 = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreenDetails/V-address3Details'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.ValueAddress3Details = add3

WebUI.verifyMatch(GlobalVariable.ValueAddress3Details, 'Jl Pasific', true, FailureHandling.CONTINUE_ON_FAILURE)

beneResStatus = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreenDetails/V-beneficiaryResidentStatusDetails'),
	FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.ValueBeneficiaryResidentStatusDetails = beneResStatus

WebUI.verifyMatch(GlobalVariable.ValueBeneficiaryResidentStatusDetails, 'Resident', true, FailureHandling.CONTINUE_ON_FAILURE)

bankType = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreenDetails/V-beneficiaryTypeDetails'),
	FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.ValueBankTypeDetails = bankType

WebUI.verifyMatch(GlobalVariable.ValueBankTypeDetails, 'Individual', true, FailureHandling.CONTINUE_ON_FAILURE)

beneType = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreenDetails/V-beneficiaryBank'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.ValueBeneficiaryTypeDetails = beneType

WebUI.verifyMatch(GlobalVariable.ValueBeneficiaryTypeDetails, 'BNI', true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('BeneficiaryListAdd/B-closeForDetailsFrame'), FailureHandling.CONTINUE_ON_FAILURE)

