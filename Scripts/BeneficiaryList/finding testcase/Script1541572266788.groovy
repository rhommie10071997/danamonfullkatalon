import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint


WebDriver driver = DriverFactory.getWebDriver()
WebElement Table = driver.findElement(By.xpath('//table[@id="globalTableAll"]/tbody'))

List<WebElement> Rows = Table.findElements(By.tagName('tr'))
	
table: for(int j = 0; j<Rows.size(); j++){
	List<WebElement> Cols = Rows.get(j).findElements(By.tagName('td'))
		GlobalVariable.ValueBankType =''+Cols.get(BankType).getText()
		GlobalVariable.ValueAccountNumber =''+Cols.get(AccountNumber).getText()
		GlobalVariable.ValueAccountName =''+Cols.get(AccountName).getText()
		GlobalVariable.ValueCurrency =''+Cols.get(Currency).getText()
	}
WebUI.switchToDefaultContent()