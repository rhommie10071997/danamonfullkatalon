import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

////////////////////////////////////////////////////////////
aliasName = 'minang kabau boing 123'
email = 'BottleMinum@gmail.com'
sms = '123123123'
accNumber = '000002018547'

domesticAccNumberDomestic = '123123123'
domesticAccNameDomestic = 'DomesticBank'
domesticAdd1 = 'Jl Wisma'
domesticAdd2 = 'Jl Barito'
domesticAdd3 = 'Jl Pasific'
domesticBeneType = 'Individual'
domesticBeneBank = 'BNI'

accNumberInternational = '321321321'
accNameInternational = 'InternationalBank'
internationalAdd1 = 'JL Kebon'
internationalAdd2 = 'JL Sirih'
beneBankCountry = 'ID'
nationalOrganizationDirectory = 'FED'
beneBank ='IND'
intermediaryBank = 'rere'
////////////////////////////////////////////////////////////


WebUI.openBrowser('', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.navigateToUrl('https://10.194.8.106:39990/', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-CorpID'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-CorpID'), GlobalVariable.MultiUserLoginCorpregId, 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-UserID'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-UserID'), GlobalVariable.MultiUserLoginId, 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-UserPass'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-UserPass'), GlobalVariable.MultiUserPass, 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/B-Login'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-MenuSearch'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-MenuSearch'), 'Beneficiary List', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/B-Menu'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('BeneficiaryListAdd/T-beneficiaryList'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/B-Menu'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(3, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('BeneficiaryListAdd/B-add'), FailureHandling.CONTINUE_ON_FAILURE)
WebUI.delay(2)
WebUI.setText(findTestObject('BeneficiaryListAdd/TF-aliasName'), aliasName, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('BeneficiaryListAdd/TF-email'), email, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('BeneficiaryListAdd/TF-sms'), sms, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('BeneficiaryListAdd/TF-accountNumber'), accNumber, FailureHandling.CONTINUE_ON_FAILURE)


WebUI.delay(2)

WebUI.click(findTestObject('BeneficiaryListAdd/B-addToList'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('BeneficiaryListAdd/B-addToList'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('BeneficiaryListAdd/B-domesticBank'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('BeneficiaryListAdd/TF-accountNumberDomestic'), domesticAccNumberDomestic, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('BeneficiaryListAdd/TF-accountNameDomestic'), domesticAccNameDomestic, FailureHandling.CONTINUE_ON_FAILURE)



WebUI.delay(2)

WebUI.setText(findTestObject('BeneficiaryListAdd/TF-accountAddress1'), domesticAdd1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2)

WebUI.setText(findTestObject('BeneficiaryListAdd/TF-accountAddress2'), domesticAdd2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2)

WebUI.setText(findTestObject('BeneficiaryListAdd/TF-accountAddress3'), domesticAdd3, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2)

WebUI.click(findTestObject('BeneficiaryListAdd/DL-beneficiaryType'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2)

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), domesticBeneType, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), Keys.chord(Keys.ENTER), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('BeneficiaryListAdd/DL-beneficiaryBank'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), domesticBeneBank, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), Keys.chord(Keys.ENTER), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('BeneficiaryListAdd/B-addToList'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(3, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('BeneficiaryListAdd/B-internationalBank'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('BeneficiaryListAdd/TF-accountNumberInternational'),accNumberInternational, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('BeneficiaryListAdd/TF-accountNameInternational'), accNameInternational, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2)

WebUI.setText(findTestObject('BeneficiaryListAdd/TF-addressInternational1'), internationalAdd1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2)

WebUI.setText(findTestObject('BeneficiaryListAdd/TF-addressInternational2'), internationalAdd2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2)

WebUI.click(findTestObject('BeneficiaryListAdd/DL-beneficiaryBankCountry'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), beneBankCountry, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), Keys.chord(Keys.ENTER), 
    FailureHandling.CONTINUE_ON_FAILURE)


WebUI.click(findTestObject('BeneficiaryListAdd/DL-nationalOrganizationDirectory'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), nationalOrganizationDirectory, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), Keys.chord(Keys.ENTER), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('BeneficiaryListAdd/DL-beneficiaryBankInternational'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'),beneBank, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), Keys.chord(Keys.ENTER), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('BeneficiaryListAdd/DL-intermediaryBank'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), intermediaryBank, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), Keys.chord(Keys.ENTER), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('BeneficiaryListAdd/B-addToList'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('BeneficiaryListAdd/B-confirm'))

WebUI.delay(5)
GlobalVariable.UniversalVariable.put('aliasName',aliasName)
 
GlobalVariable.UniversalVariable.put('email',email)
 
GlobalVariable.UniversalVariable.put('sms',sms)
 
GlobalVariable.UniversalVariable.put('accNumber',accNumber)
 
GlobalVariable.UniversalVariable.put('domesticAccNumberDomestic',domesticAccNumberDomestic)
 
GlobalVariable.UniversalVariable.put('domesticAccNameDomestic',domesticAccNameDomestic)
 
GlobalVariable.UniversalVariable.put('domesticAdd1',domesticAdd1)
 
GlobalVariable.UniversalVariable.put('domesticAdd2',domesticAdd2)
 
GlobalVariable.UniversalVariable.put('domesticAdd3',domesticAdd3)
 
GlobalVariable.UniversalVariable.put('domesticBeneType',domesticBeneType)
  
GlobalVariable.UniversalVariable.put('domesticBeneBank',domesticBeneBank)
 
GlobalVariable.UniversalVariable.put('accNumberInternational',accNumberInternational)
 
GlobalVariable.UniversalVariable.put('accNameInternational',accNameInternational)
 
GlobalVariable.UniversalVariable.put('internationalAdd1',internationalAdd1)
 
GlobalVariable.UniversalVariable.put('internationalAdd2',internationalAdd2)
 
GlobalVariable.UniversalVariable.put('beneBankCountry',beneBankCountry)
 
GlobalVariable.UniversalVariable.put('nationalOrganizationDirectory',nationalOrganizationDirectory)
 
GlobalVariable.UniversalVariable.put('beneBank',beneBank)
 
GlobalVariable.UniversalVariable.put('intermediaryBank',intermediaryBank)
 




