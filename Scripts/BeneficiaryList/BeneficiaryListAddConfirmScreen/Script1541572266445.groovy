import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.junit.After as After
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

alName = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreen/V-aliasName'), FailureHandling.CONTINUE_ON_FAILURE)

aliasName = alName.replace(': ', '')

WebUI.verifyMatch(aliasName, GlobalVariable.UniversalVariable.get('aliasName'), true, FailureHandling.CONTINUE_ON_FAILURE)

email = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreen/V-email'), FailureHandling.CONTINUE_ON_FAILURE)

Email = email.replace(': ', '')

WebUI.verifyMatch(Email, GlobalVariable.UniversalVariable.get('email'), true, FailureHandling.CONTINUE_ON_FAILURE)

sms = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreen/V-sms'), FailureHandling.CONTINUE_ON_FAILURE)

Sms = sms.replace(': ', '')

WebUI.verifyMatch(Sms, GlobalVariable.UniversalVariable.get('sms'), true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

bankType = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreen/V-bankType'), FailureHandling.CONTINUE_ON_FAILURE)

bankType = bankType

GlobalVariable.UniversalVariable.put('bankType', bankType)

WebUI.verifyMatch(bankType, GlobalVariable.UniversalVariable.get('bankType'), false, FailureHandling.CONTINUE_ON_FAILURE)

accNum = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreen/V-accountNumber'), FailureHandling.CONTINUE_ON_FAILURE)

AccountNumber = accNum

WebUI.verifyMatch(AccountNumber, GlobalVariable.UniversalVariable.get('accNumber'), false, FailureHandling.CONTINUE_ON_FAILURE)

accNam = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreen/V-accountName'), FailureHandling.CONTINUE_ON_FAILURE)

AccountName = accNam

GlobalVariable.UniversalVariable.put('AccountName', AccountName)

WebUI.verifyMatch(AccountName, GlobalVariable.UniversalVariable.get('AccountName'), false, FailureHandling.CONTINUE_ON_FAILURE)

Curency = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreen/V-currency'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('Curency', Curency)

WebUI.verifyMatch(Curency, GlobalVariable.UniversalVariable.get('Curency'), false, FailureHandling.CONTINUE_ON_FAILURE)

domBankType = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreen/V-domesticBankType'), FailureHandling.CONTINUE_ON_FAILURE)

DomesticBankType = domBankType

GlobalVariable.UniversalVariable.put('DomesticBankType', DomesticBankType)

WebUI.verifyMatch(DomesticBankType, GlobalVariable.UniversalVariable.get('DomesticBankType'), false, FailureHandling.CONTINUE_ON_FAILURE)

domAccNum = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreen/V-domesticAccountNumber'), FailureHandling.CONTINUE_ON_FAILURE)

DomesticAccountNumber = domAccNum

WebUI.verifyMatch(DomesticAccountNumber, GlobalVariable.UniversalVariable.get('domesticAccNumberDomestic'), false, FailureHandling.CONTINUE_ON_FAILURE)

domAccNam = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreen/V-domesticAccountName'), FailureHandling.CONTINUE_ON_FAILURE)

DomesticAccountName = domAccNam

WebUI.verifyMatch(DomesticAccountName, GlobalVariable.UniversalVariable.get('domesticAccNameDomestic'), false, FailureHandling.CONTINUE_ON_FAILURE)

domBank = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreen/V-domesticBank'), FailureHandling.CONTINUE_ON_FAILURE)

DomesticBank = domBank

GlobalVariable.UniversalVariable.put('DomesticBank', DomesticBank)

WebUI.verifyMatch(DomesticBank, GlobalVariable.UniversalVariable.get('DomesticBank'), false, FailureHandling.CONTINUE_ON_FAILURE)

intBankType = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreen/V-InternationalBankType'), FailureHandling.CONTINUE_ON_FAILURE)

InternationalBankType = intBankType

GlobalVariable.UniversalVariable.put('InternationalBankType', InternationalBankType)

WebUI.verifyMatch(InternationalBankType, GlobalVariable.UniversalVariable.get('InternationalBankType'), false, FailureHandling.CONTINUE_ON_FAILURE)

intAccNum = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreen/V-internationalAccountNumber'), 
    FailureHandling.CONTINUE_ON_FAILURE)

InternationalAccountNumber = intAccNum

WebUI.verifyMatch(InternationalAccountNumber, GlobalVariable.UniversalVariable.get('accNumberInternational'), false, FailureHandling.CONTINUE_ON_FAILURE)

intAccNam = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreen/V-internationalAccountName'), 
    FailureHandling.CONTINUE_ON_FAILURE)

InternationalAccountName = intAccNam

WebUI.verifyMatch(InternationalAccountName, GlobalVariable.UniversalVariable.get('accNameInternational'), false, FailureHandling.CONTINUE_ON_FAILURE)

intBank = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreen/V-internationalBank'), FailureHandling.CONTINUE_ON_FAILURE)

InternationalBank = intBank

GlobalVariable.UniversalVariable.put('InternationalBank', InternationalBank)

WebUI.verifyMatch(InternationalBank, GlobalVariable.UniversalVariable.get('InternationalBank'), false, FailureHandling.CONTINUE_ON_FAILURE)

intCoun = WebUI.getText(findTestObject('BeneficiaryListAdd/BeneficiaryListConfirmScreen/V-internationalCountry'), FailureHandling.CONTINUE_ON_FAILURE)

InternationalCountry = intCoun

GlobalVariable.UniversalVariable.put('InternationalCountry', InternationalCountry)

WebUI.verifyMatch(InternationalCountry, GlobalVariable.UniversalVariable.get('InternationalCountry'), false, FailureHandling.CONTINUE_ON_FAILURE)

