import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(5)

WebUI.setText(findTestObject('homelogin/homelogin.corporateid'), 'PCMSILK001')

WebUI.setText(findTestObject('homelogin/homelogin.userid'), 'Silk04')

WebUI.setText(findTestObject('homelogin/homelogin.password'), 'Password123')

WebUI.click(findTestObject('homelogin/homelogin.submit'))

WebUI.delay(3)

WebUI.waitForElementVisible(findTestObject('menu/BTN - clickMenu'), 0)

WebUI.click(findTestObject('menu/BTN - clickMenu'))

WebUI.click(findTestObject('menu/myTask/BTN - myTaskClick'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('menu/myTask/pendingTask/BTN - pendingTaskClick'))

WebUI.delay(5)

WebUI.click(findTestObject('menu/myTask/pendingTask/displayOption/displayOptionClick'))

WebUI.delay(5)

WebUI.click(findTestObject('menu/myTask/pendingTask/TransactionRefNo/radioButtonTrRefClick'))

WebUI.setText(findTestObject('menu/myTask/pendingTask/TransactionRefNo/textfieldTrRef'), GlobalVariable.confirmScreens.get('refNum'))

WebUI.waitForElementVisible(findTestObject('menu/myTask/pendingTask/BTN - btnShow'), 0)

WebUI.click(findTestObject('menu/myTask/pendingTask/BTN - btnShow'))

WebUI.delay(10)

WebUI.waitForElementVisible(findTestObject('menu/myTask/pendingTask/TD - clickData (Multi)'), 0)

WebUI.doubleClick(findTestObject('menu/myTask/pendingTask/TD - clickData (Multi)'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(15)

