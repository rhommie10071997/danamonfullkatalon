import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

SubTitle = WebUI.getText(findTestObject('Pending Task/Balance Transfer/Inhouse/Detail/Label - Sub Title'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(SubTitle, 'Reference Number Detail', true, FailureHandling.CONTINUE_ON_FAILURE)

TransactionReferenceNumber = WebUI.getText(findTestObject('Transaction Status/Single Transfer/International/Ref No/Label - Transaction Reference Number'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TransactionReferenceNumber = TransactionReferenceNumber.replace(': ', '')

WebUI.verifyMatch(TransactionReferenceNumber, GlobalVariable.Map123.get('RefNo'), true, FailureHandling.CONTINUE_ON_FAILURE)

Menu = WebUI.getText(findTestObject('Transaction Status/Single Transfer/International/Ref No/Label - Menu'), FailureHandling.CONTINUE_ON_FAILURE)

Menu = Menu.replace(': ', '')

GlobalVariable.Map123.put('Menu', 'Single Transfer')

WebUI.verifyMatch(Menu, GlobalVariable.Map123.get('Menu'), true, FailureHandling.CONTINUE_ON_FAILURE)

Product = WebUI.getText(findTestObject('Transaction Status/Single Transfer/International/Ref No/Label - Product'), FailureHandling.CONTINUE_ON_FAILURE)

Product = Product.replace(': ', '')

GlobalVariable.Map123.put('Product', 'International Foreign Currency Transfer')

WebUI.verifyMatch(Product, GlobalVariable.Map123.get('Product'), true, FailureHandling.CONTINUE_ON_FAILURE)

TransferFrom = WebUI.getText(findTestObject('Transaction Status/Single Transfer/International/Ref No/Label - Transfer From'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TransferFrom = TransferFrom.replace(': ', '')

WebUI.verifyMatch(TransferFrom, GlobalVariable.Map123.get('TransferFrom'), false, FailureHandling.CONTINUE_ON_FAILURE)

FromAccountDescription = WebUI.getText(findTestObject('Transaction Status/Single Transfer/International/Ref No/Label - From Account Description'), 
    FailureHandling.CONTINUE_ON_FAILURE)

FromAccountDescription = FromAccountDescription.replace(': ', '')

WebUI.verifyMatch(FromAccountDescription, GlobalVariable.Map123.get('FromAccountDescription'), false, FailureHandling.CONTINUE_ON_FAILURE)

TransferTo = WebUI.getText(findTestObject('Transaction Status/Single Transfer/International/Ref No/Label - Transfer To'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TransferTo = TransferTo.replace(': ', '')

WebUI.verifyMatch(TransferTo, GlobalVariable.Map123.get('TransferTo'), false, FailureHandling.CONTINUE_ON_FAILURE)

if (GlobalVariable.Map123.get('TransferTo') == 'Beneficiary List') {
    AccountNumber = WebUI.getText(findTestObject('Transaction Status/Single Transfer/International/Ref No/Label - Account Number'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    AccountNumber = AccountNumber.replace(': ', '')

    WebUI.verifyMatch(AccountNumber, GlobalVariable.Map123.get('BeneficiaryList'), false, FailureHandling.CONTINUE_ON_FAILURE)
} else {
    BeneficiaryBankCountry = WebUI.getText(findTestObject('Transaction Status/Single Transfer/International/Ref No/Label - Beneficiary Bank Country'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    BeneficiaryBankCountry = BeneficiaryBankCountry.replace(': ', '')

    WebUI.verifyMatch(BeneficiaryBankCountry, GlobalVariable.Map123.get('BeneficiaryBankCountry'), false, FailureHandling.CONTINUE_ON_FAILURE)

    NationalOrganizationDirectory = WebUI.getText(findTestObject('Transaction Status/Single Transfer/International/Ref No/Label - National Organization Directory'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    NationalOrganizationDirectory = NationalOrganizationDirectory.replace(': ', '')

    WebUI.verifyMatch(NationalOrganizationDirectory, GlobalVariable.Map123.get('NationalOrganizationDirectory'), false, 
        FailureHandling.CONTINUE_ON_FAILURE)

    BeneficiaryBank = WebUI.getText(findTestObject('Transaction Status/Single Transfer/International/Ref No/Label - Beneficiary Bank'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    BeneficiaryBank = BeneficiaryBank.replace(': ', '')

    WebUI.verifyMatch(BeneficiaryBank, GlobalVariable.Map123.get('BeneficiaryBank'), false, FailureHandling.CONTINUE_ON_FAILURE)

    BankCity = WebUI.getText(findTestObject('Transaction Status/Single Transfer/International/Ref No/Label - Bank City'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    BankCity = BankCity.replace(': ', '')

    WebUI.verifyMatch(BankCity, GlobalVariable.Map123.get('BankCity'), false, FailureHandling.CONTINUE_ON_FAILURE)

    IntermediaryBank = WebUI.getText(findTestObject('Transaction Status/Single Transfer/International/Ref No/Label - Intermediary Bank'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    IntermediaryBank = IntermediaryBank.replace(': ', '')

    WebUI.verifyMatch(IntermediaryBank, GlobalVariable.Map123.get('IntermediaryBank'), false, FailureHandling.CONTINUE_ON_FAILURE)

    AccountNumber = WebUI.getText(findTestObject('Transaction Status/Single Transfer/International/Ref No/Label - Account Number'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    AccountNumber = AccountNumber.replace(': ', '')

    WebUI.verifyMatch(AccountNumber, GlobalVariable.Map123.get('BeneficiaryAccountNumber'), false, FailureHandling.CONTINUE_ON_FAILURE)

    SavetoBeneficiaryList = WebUI.getText(findTestObject('Transaction Status/Single Transfer/International/Ref No/Label - Save to Beneficiary List'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    SavetoBeneficiaryList = SavetoBeneficiaryList.replace(': ', '')

    if (GlobalVariable.Map123.get('FlagSavetoBeneficiaryList') == 0) {
        WebUI.verifyMatch(SavetoBeneficiaryList, 'Yes', false, FailureHandling.CONTINUE_ON_FAILURE)

        AliasName = WebUI.getText(findTestObject('Transaction Status/Single Transfer/International/Ref No/Label - Alias Name'), 
            FailureHandling.CONTINUE_ON_FAILURE)

        AliasName = AliasName.replace(': ', '')

        WebUI.verifyMatch(AliasName, GlobalVariable.Map123.get('AliasName'), false, FailureHandling.CONTINUE_ON_FAILURE)
    } else {
        WebUI.verifyMatch(SavetoBeneficiaryList, 'No', false, FailureHandling.CONTINUE_ON_FAILURE)
    }
    
    AccountName = WebUI.getText(findTestObject('Transaction Status/Single Transfer/International/Ref No/Label - Account Name'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    AccountName = AccountName.replace(': ', '')

    WebUI.verifyMatch(AccountName, GlobalVariable.Map123.get('BeneficiaryAccountName'), false, FailureHandling.CONTINUE_ON_FAILURE)

    Address1 = WebUI.getText(findTestObject('Transaction Status/Single Transfer/International/Ref No/Label - Address 1'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    Address1 = Address1.replace(': ', '')

    WebUI.verifyMatch(Address1, GlobalVariable.Map123.get('Address1'), false, FailureHandling.CONTINUE_ON_FAILURE)

    Address2 = WebUI.getText(findTestObject('Transaction Status/Single Transfer/International/Ref No/Label - Address 2'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    Address2 = Address2.replace(': ', '')

    WebUI.verifyMatch(Address2, GlobalVariable.Map123.get('Address2'), false, FailureHandling.CONTINUE_ON_FAILURE)

    Email = WebUI.getText(findTestObject('Transaction Status/Single Transfer/International/Ref No/Label - Email'), FailureHandling.CONTINUE_ON_FAILURE)

    String[] EmailSplit = Email.split(' : ')

    WebUI.verifyMatch(EmailSplit[1], GlobalVariable.Map123.get('Email'), false, FailureHandling.CONTINUE_ON_FAILURE)

    Phone = WebUI.getText(findTestObject('Transaction Status/Single Transfer/International/Ref No/Label - Phone'), FailureHandling.CONTINUE_ON_FAILURE)

    String[] PhoneSplit = Phone.split(' : ')

    WebUI.verifyMatch(PhoneSplit[1], GlobalVariable.Map123.get('Phone'), false, FailureHandling.CONTINUE_ON_FAILURE)
}

BeneficiaryReferenceNo = WebUI.getText(findTestObject('Transaction Status/Single Transfer/International/Ref No/Label - Beneficiary Reference No'), 
    FailureHandling.CONTINUE_ON_FAILURE)

BeneficiaryReferenceNo = BeneficiaryReferenceNo.replace(': ', '')

WebUI.verifyMatch(BeneficiaryReferenceNo, GlobalVariable.Map123.get('BeneficiaryReferenceNo'), false, FailureHandling.CONTINUE_ON_FAILURE)

ToAccountDescription = WebUI.getText(findTestObject('Transaction Status/Single Transfer/International/Ref No/Label - To Account Description'), 
    FailureHandling.CONTINUE_ON_FAILURE)

ToAccountDescription = ToAccountDescription.replace(': ', '')

WebUI.verifyMatch(ToAccountDescription, GlobalVariable.Map123.get('ToAccountDescription'), false, FailureHandling.CONTINUE_ON_FAILURE)

RemittanceCurrency = WebUI.getText(findTestObject('Transaction Status/Single Transfer/International/Ref No/Label - Remittance Currency'), 
    FailureHandling.CONTINUE_ON_FAILURE)

RemittanceCurrency = RemittanceCurrency.replace(': ', '')

WebUI.verifyMatch(RemittanceCurrency, GlobalVariable.Map123.get('RemittanceCurrency'), false, FailureHandling.CONTINUE_ON_FAILURE)

Amount = WebUI.getText(findTestObject('Transaction Status/Single Transfer/International/Ref No/Label - Amount'), FailureHandling.CONTINUE_ON_FAILURE)

Amount = Amount.replace(': ', '')

WebUI.verifyMatch(Amount, GlobalVariable.Map123.get('Amount'), false, FailureHandling.CONTINUE_ON_FAILURE)

FullAmountCharge = WebUI.getText(findTestObject('Transaction Status/Single Transfer/International/Ref No/Label - Full Amount Charge'), 
    FailureHandling.CONTINUE_ON_FAILURE)

FullAmountCharge = FullAmountCharge.replace(': ', '')

WebUI.verifyMatch(FullAmountCharge, GlobalVariable.Map123.get('FullAmountCharge'), false, FailureHandling.CONTINUE_ON_FAILURE)

DebitCharge = WebUI.getText(findTestObject('Transaction Status/Single Transfer/International/Ref No/Label - Debit Charge'), 
    FailureHandling.CONTINUE_ON_FAILURE)

DebitCharge = DebitCharge.replace(': ', '')

WebUI.verifyMatch(DebitCharge, GlobalVariable.Map123.get('DebitCharge'), false, FailureHandling.CONTINUE_ON_FAILURE)

FullAmountCharge2 = WebUI.getText(findTestObject('Transaction Status/Single Transfer/International/Ref No/Label - Full Amount Charge 2'), 
    FailureHandling.CONTINUE_ON_FAILURE)

FullAmountCharge2 = FullAmountCharge2.replace(': ', '')

WebUI.verifyMatch(FullAmountCharge2, GlobalVariable.Map123.get('FullAmountCharge2'), false, FailureHandling.CONTINUE_ON_FAILURE)

CableFee = WebUI.getText(findTestObject('Transaction Status/Single Transfer/International/Ref No/Label - Cable Fee'), FailureHandling.CONTINUE_ON_FAILURE)

CableFee = CableFee.replace(': ', '')

WebUI.verifyMatch(CableFee, GlobalVariable.Map123.get('CableFee'), false, FailureHandling.CONTINUE_ON_FAILURE)

if (CableFee.substring(0, 3) == 'IDR') {
    CableFeeEquivalent = WebUI.getText(findTestObject('Transaction Status/Single Transfer/International/Ref No/Label - Cable Fee Equivalent'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    CableFeeEquivalent = CableFeeEquivalent.replace(': ', '')

    WebUI.verifyMatch(CableFeeEquivalent, GlobalVariable.Map123.get('CableFeeEquivalent'), false, FailureHandling.CONTINUE_ON_FAILURE)
}

Provision = WebUI.getText(findTestObject('Transaction Status/Single Transfer/International/Ref No/Label - Provision'), FailureHandling.CONTINUE_ON_FAILURE)

Provision = Provision.replace(': ', '')

WebUI.verifyMatch(Provision, GlobalVariable.Map123.get('Provision'), false, FailureHandling.CONTINUE_ON_FAILURE)

if (Provision.substring(0, 3) == 'IDR') {
    ProvisionEquivalent = WebUI.getText(findTestObject('Transaction Status/Single Transfer/International/Ref No/Label - Provision Equivalent'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    ProvisionEquivalent = ProvisionEquivalent.replace(': ', '')

    WebUI.verifyMatch(ProvisionEquivalent, GlobalVariable.Map123.get('ProvisionEquivalent'), false, FailureHandling.CONTINUE_ON_FAILURE)
}

CorrespondentBankFee = WebUI.getText(findTestObject('Transaction Status/Single Transfer/International/Ref No/Label - Correspondent Bank Fee'), 
    FailureHandling.CONTINUE_ON_FAILURE)

CorrespondentBankFee = CorrespondentBankFee.replace(': ', '')

WebUI.verifyMatch(CorrespondentBankFee, GlobalVariable.Map123.get('CorrespondentBankFee'), false, FailureHandling.CONTINUE_ON_FAILURE)

if (CorrespondentBankFee.substring(0, 3) == 'IDR') {
    CorrespondentBankFeeEquivalent = WebUI.getText(findTestObject('Transaction Status/Single Transfer/International/Ref No/Label - Correspondent Bank Fee Equivalent'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    CorrespondentBankFeeEquivalent = CorrespondentBankFeeEquivalent.replace(': ', '')

    WebUI.verifyMatch(CorrespondentBankFeeEquivalent, GlobalVariable.Map123.get('CorrespondentBankFeeEquivalent'), false, 
        FailureHandling.CONTINUE_ON_FAILURE)
}

InLieu = WebUI.getText(findTestObject('Transaction Status/Single Transfer/International/Ref No/Label - In Lieu'), FailureHandling.CONTINUE_ON_FAILURE)

InLieu = InLieu.replace(': ', '')

WebUI.verifyMatch(InLieu, GlobalVariable.Map123.get('InLieu'), false, FailureHandling.CONTINUE_ON_FAILURE)

if (InLieu.substring(0, 3) == 'IDR') {
    InLieuEquivalent = WebUI.getText(findTestObject('Transaction Status/Single Transfer/International/Ref No/Label - In Lieu Equivalent'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    InLieuEquivalent = InLieuEquivalent.replace(': ', '')

    WebUI.verifyMatch(InLieuEquivalent, GlobalVariable.Map123.get('InLieuEquivalent'), false, FailureHandling.CONTINUE_ON_FAILURE)
}

MinimumTransactionFee = WebUI.getText(findTestObject('Transaction Status/Single Transfer/International/Ref No/Label - Minimum Transaction Fee'), 
    FailureHandling.CONTINUE_ON_FAILURE)

MinimumTransactionFee = MinimumTransactionFee.replace(': ', '')

WebUI.verifyMatch(MinimumTransactionFee, GlobalVariable.Map123.get('MinimumTransactionFee'), false, FailureHandling.CONTINUE_ON_FAILURE)

if (InLieu.substring(0, 3) == 'IDR') {
    MinimumTransactionFeeEquivalent = WebUI.getText(findTestObject('Transaction Status/Single Transfer/International/Ref No/Label - Minimum Transaction Fee Equivalent'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    MinimumTransactionFeeEquivalent = MinimumTransactionFeeEquivalent.replace(': ', '')

    WebUI.verifyMatch(MinimumTransactionFeeEquivalent, GlobalVariable.Map123.get('MinimumTransactionFeeEquivalent'), false, 
        FailureHandling.CONTINUE_ON_FAILURE)
}

TotalCharge = WebUI.getText(findTestObject('Transaction Status/Single Transfer/International/Ref No/Label - Total Charge'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TotalCharge = TotalCharge.replace(': ', '')

WebUI.verifyMatch(TotalCharge, GlobalVariable.Map123.get('TotalCharge'), false, FailureHandling.CONTINUE_ON_FAILURE)

ChargetoRemitter = WebUI.getText(findTestObject('Transaction Status/Single Transfer/International/Ref No/Label - Charge to Remitter'), 
    FailureHandling.CONTINUE_ON_FAILURE)

ChargetoRemitter = ChargetoRemitter.replace(': ', '')

WebUI.verifyMatch(ChargetoRemitter, GlobalVariable.Map123.get('ChargetoRemitter'), false, FailureHandling.CONTINUE_ON_FAILURE)

ChargetoBeneficiary = WebUI.getText(findTestObject('Transaction Status/Single Transfer/International/Ref No/Label - Charge to Beneficiary'), 
    FailureHandling.CONTINUE_ON_FAILURE)

ChargetoBeneficiary = ChargetoBeneficiary.replace(': ', '')

WebUI.verifyMatch(ChargetoBeneficiary, GlobalVariable.Map123.get('ChargetoBeneficiary'), false, FailureHandling.CONTINUE_ON_FAILURE)

TotalDebitAmount = WebUI.getText(findTestObject('Transaction Status/Single Transfer/International/Ref No/Label - Total Debit Amount'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TotalDebitAmount = TotalDebitAmount.replace(': ', '')

WebUI.verifyMatch(TotalDebitAmount, GlobalVariable.Map123.get('TotalDebitAmount'), false, FailureHandling.CONTINUE_ON_FAILURE)

BeneficiaryResidentStatus = WebUI.getText(findTestObject('Transaction Status/Single Transfer/International/Ref No/Label - Beneficiary Resident Status'), 
    FailureHandling.CONTINUE_ON_FAILURE)

BeneficiaryResidentStatus = BeneficiaryResidentStatus.replace(': ', '')

WebUI.verifyMatch(BeneficiaryResidentStatus, GlobalVariable.Map123.get('BeneficiaryResidentStatus'), false, FailureHandling.CONTINUE_ON_FAILURE)

BeneficiaryCategory = WebUI.getText(findTestObject('Transaction Status/Single Transfer/International/Ref No/Label - Beneficiary Category'), 
    FailureHandling.CONTINUE_ON_FAILURE)

BeneficiaryCategory = BeneficiaryCategory.replace(': ', '')

WebUI.verifyMatch(BeneficiaryCategory, GlobalVariable.Map123.get('BeneficiaryCategory'), false, FailureHandling.CONTINUE_ON_FAILURE)

TransactorRelationship = WebUI.getText(findTestObject('Transaction Status/Single Transfer/International/Ref No/Label - Transactor Relationship'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TransactorRelationship = TransactorRelationship.replace(': ', '')

WebUI.verifyMatch(TransactorRelationship, GlobalVariable.Map123.get('TransactorRelationship'), false, FailureHandling.CONTINUE_ON_FAILURE)

IdenticalStatus = WebUI.getText(findTestObject('Transaction Status/Single Transfer/International/Ref No/Label - Identical Status'), 
    FailureHandling.CONTINUE_ON_FAILURE)

IdenticalStatus = IdenticalStatus.replace(': ', '')

WebUI.verifyMatch(IdenticalStatus, GlobalVariable.Map123.get('IdenticalStatus'), false, FailureHandling.CONTINUE_ON_FAILURE)

PurposeOfTransaction = WebUI.getText(findTestObject('Transaction Status/Single Transfer/International/Ref No/Label - Purpose Of Transaction'), 
    FailureHandling.CONTINUE_ON_FAILURE)

PurposeOfTransaction = PurposeOfTransaction.replace(': ', '')

WebUI.verifyMatch(PurposeOfTransaction, GlobalVariable.Map123.get('PurposeOfTransaction'), false, FailureHandling.CONTINUE_ON_FAILURE)

LHBUPurposeCode = WebUI.getText(findTestObject('Transaction Status/Single Transfer/International/Ref No/Label - LHBU Purpose Code'), 
    FailureHandling.CONTINUE_ON_FAILURE)

LHBUPurposeCode = LHBUPurposeCode.replace(': ', '')

WebUI.verifyMatch(LHBUPurposeCode, GlobalVariable.Map123.get('LHBUPurposeCode'), false, FailureHandling.CONTINUE_ON_FAILURE)

LHBUDocumentType = WebUI.getText(findTestObject('Transaction Status/Single Transfer/International/Ref No/Label - LHBU Document Type'), 
    FailureHandling.CONTINUE_ON_FAILURE)

LHBUDocumentType = LHBUDocumentType.replace(': ', '')

WebUI.verifyMatch(LHBUDocumentType, GlobalVariable.Map123.get('LHBUDocumentType'), false, FailureHandling.CONTINUE_ON_FAILURE)

LHBUDocumentTypeDescription = WebUI.getText(findTestObject('Transaction Status/Single Transfer/International/Ref No/Label - LHBU Document Type Description'), 
    FailureHandling.CONTINUE_ON_FAILURE)

LHBUDocumentTypeDescription = LHBUDocumentTypeDescription.replace(': ', '')

WebUI.verifyMatch(LHBUDocumentTypeDescription, GlobalVariable.Map123.get('LHBUDocumentTypeDescription'), false, FailureHandling.CONTINUE_ON_FAILURE)

InstructionMode = WebUI.getText(findTestObject('Transaction Status/Single Transfer/International/Ref No/Label - Instruction Mode'), 
    FailureHandling.CONTINUE_ON_FAILURE)

InstructionMode = InstructionMode.replace(': ', '')

WebUI.verifyMatch(InstructionMode, GlobalVariable.Map123.get('InstructionMode'), false, FailureHandling.CONTINUE_ON_FAILURE)

RepeatOn = WebUI.getText(findTestObject('Transaction Status/Single Transfer/International/Ref No/Label - Repeat - On'), 
    FailureHandling.CONTINUE_ON_FAILURE)

RepeatOn = RepeatOn.replace(': ', '')

WebUI.verifyMatch(RepeatOn, GlobalVariable.Map123.get('RepeatOn'), false, FailureHandling.CONTINUE_ON_FAILURE)

RepeatEvery = WebUI.getText(findTestObject('Transaction Status/Single Transfer/International/Ref No/Label - Repeat - Every'), FailureHandling.CONTINUE_ON_FAILURE)

RepeatEvery = RepeatEvery.replace(': ', '')

WebUI.verifyMatch(RepeatEvery, GlobalVariable.Map123.get('RepeatEvery'), false, FailureHandling.CONTINUE_ON_FAILURE)

RepeatAt = WebUI.getText(findTestObject('Transaction Status/Single Transfer/International/Ref No/Label - Repeat - At'), FailureHandling.CONTINUE_ON_FAILURE)

RepeatAt = RepeatAt.replace(': ', '')

WebUI.verifyMatch(RepeatAt, GlobalVariable.Map123.get('RepeatAt'), false, FailureHandling.CONTINUE_ON_FAILURE)

RepeatStart = WebUI.getText(findTestObject('Transaction Status/Single Transfer/International/Ref No/Label - Repeat - Start'), FailureHandling.CONTINUE_ON_FAILURE)

RepeatStart = RepeatStart.replace(': ', '')

WebUI.verifyMatch(RepeatStart, GlobalVariable.Map123.get('RepeatStart'), false, FailureHandling.CONTINUE_ON_FAILURE)

RepeatEnd = WebUI.getText(findTestObject('Transaction Status/Single Transfer/International/Ref No/Label - Repeat - End'), FailureHandling.CONTINUE_ON_FAILURE)

RepeatEnd = RepeatEnd.replace(': ', '')

WebUI.verifyMatch(RepeatEnd, GlobalVariable.Map123.get('RepeatEnd'), false, FailureHandling.CONTINUE_ON_FAILURE)

InstructionOnHoliday = WebUI.getText(findTestObject('Transaction Status/Single Transfer/International/Ref No/Label - Repeat - Instruction On Holiday'), 
    FailureHandling.CONTINUE_ON_FAILURE)

InstructionOnHoliday = InstructionOnHoliday.replace(': ', '')

WebUI.verifyMatch(InstructionOnHoliday, GlobalVariable.Map123.get('NonWorkingDayInstruction'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.scrollToElement(findTestObject('Transaction Status/Balance Transfer - Inhouse/New - Ref No Detail/BTN - Document Number'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Transaction Status/Balance Transfer - Inhouse/New - Ref No Detail/BTN - Document Number'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

