import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import java.text.DecimalFormat as DecimalFormat
import java.text.NumberFormat as NumberFormat
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

Random rand = new Random()

'----------------------------------------------------------------------------------------------------------------------------------------------------------'

'Transfer From bisa di isi pake Nama atau Nomor dia'
TransferFrom = 'askar'

'FromAccountDescription'
FromAccountDescription = 'FromAccountDescriptionArdo'

'Transfer To , Flag 0 = Beneficiary List, Flag 1 = New Beneficiary'
TransferTo = 1

'----------------------------------------------------------------------------------------------------------------------------------------------------------'

'Beneficiary List'
BeneficiaryList = 'raja'

'----------------------------------------------------------------------------------------------------------------------------------------------------------'

'Flag Save to Beneficiary List Flag 0 = Save, Flag 1 = tidak save'
FlagSavetoBeneficiaryList = 0

'Alias Name'
AliasName = 'AliasName Ardo lets kuy'

'Beneficiary Bank Country'
BeneficiaryBankCountry = 'BE'

'National Organization Directory'
NationalOrganizationDirectory = 'FED - FED1'

'Beneficiary Bank'
BeneficiaryBank = 'asalbank'

'Intermediary Bank'
IntermediaryBank = 'jrij'

'BeneficiaryAccountNumber'
int RandomBeneficiary

'kalau mau di random. Kalau gak mau silahakan di tambah // di depan script'
RandomBeneficiary = rand.nextInt(99999999)

BeneficiaryAccountNumber = ('' + RandomBeneficiary)

'BeneficiaryAccountName'
BeneficiaryAccountName = 'BenefAccName Ardo'

'Address 1'
Address1 = 'Address1 asdasdasd'

'Address 2'
Address2 = 'Address2 1234123asdasd'

'Email'
Email = 'Email12@Email.com'

'Phone'
Phone = '08531231231'

'----------------------------------------------------------------------------------------------------------------------------------------------------------'

'To Account Description ku samain kayak From Account Description'

//ToAccountDescription = From account description
'Beneficiary Reference No'
BeneficiaryReferenceNo = '012394123741'

'RemittanceCurrency'
RemittanceCurrency = 'USD'

'Amount'
AmountCurrency = 'USD'

'Amount yang saya buat itu 1 digit angka di tambah 3 digit random'
AmountRandom = rand.nextInt(999)

String Amount = '1' + AmountRandom

'Full Amount Charge'
FullAmountCharge = 'Full amount (Our)'

'Beneficiary Category'
BeneficiaryCategory = '3 - Pemerintah'

'Transactor Relationship'
TransactorRelationship = 'G - Group'

'Purpose Of Transaction'
PurposeOfTransaction = '2011'

'LHBU Purpose Code'
LHBUPurposeCode = '00 - 00'

'LHBU Document Type'
LHBUDocumentType = '001 - 001'

'LHBU Document Type Description'
LHBUDocumentTypeDescription = 'LHBU Lets gow'

'----------------------------------------------------------------------------------------------------------------------------------------------------------'
GlobalVariable.Map123.put('FlagSavetoBeneficiaryList', FlagSavetoBeneficiaryList)

GlobalVariable.Map123.put('DebitCharge', 'Combine')

GlobalVariable.Map123.put('BeneficiaryResidentStatus', 'Resident')

GlobalVariable.Map123.put('IdenticalStatus', 'Identical')

'----------------------------------------------------------------------------------------------------------------------------------------------------------'
WebUI.openBrowser('https://10.194.8.106:29990/', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.maximizeWindow()

WebUI.waitForAngularLoad(0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('1 - Frame/1 - iframe login'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Log in/corpid'), 'aprismasm2', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Log in/userid'), 'mobile', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Log in/password'), 'Password123', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Log in/login'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(3, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementPresent(findTestObject('Menu/BTN - Menu Object'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Menu/BTN - Menu Object'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementClickable(findTestObject('Menu/BTN - Menu Object'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Menu/BTN - Menu Object'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Menu/BTN - Transfer Management'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Menu/BTN - Transfer Management'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Menu/BTN - Single Transfer'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Menu/BTN - Single Transfer'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Single Transfer/International/Create/BTN - International Foreign Currency Transfer'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Single Transfer/International/Create/BTN - International Foreign Currency Transfer'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Single Transfer/International/Create/DropList - Transfer From'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Single Transfer/International/Create/DropList - Transfer From'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Single Transfer/International/Create/TextField - Input'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Single Transfer/International/Create/TextField - Input'), TransferFrom, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Single Transfer/International/Create/DropList - Highlighted'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Single Transfer/International/Create/DropList - Highlighted'), FailureHandling.CONTINUE_ON_FAILURE)

TransferFrom = WebUI.getText(findTestObject('Single Transfer/International/Create/DropList - Transfer From'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Map123.put('TransferFrom', TransferFrom)

int RandFromAccDes

RandFromAccDes = rand.nextInt(999)

WebUI.setText(findTestObject('Single Transfer/International/Create/TextField - From Account Description'), FromAccountDescription + 
    RandFromAccDes, FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Map123.put('FromAccountDescription', FromAccountDescription + RandFromAccDes)

if (TransferTo == 0) {
    GlobalVariable.Map123.put('TransferTo', 'Beneficiary List')

    WebUI.click(findTestObject('Single Transfer/International/Create/BTN - Transfer To - Beneficiary List'), FailureHandling.STOP_ON_FAILURE)

    WebUI.click(findTestObject('Single Transfer/International/Create/DropList - Beneficiary List'), FailureHandling.STOP_ON_FAILURE)

    WebUI.waitForElementVisible(findTestObject('Single Transfer/International/Create/TextField - Input'), 0)

    WebUI.setText(findTestObject('Single Transfer/International/Create/TextField - Input'), BeneficiaryList)

    WebUI.waitForElementVisible(findTestObject('Single Transfer/International/Create/DropList - Highlighted'), 0)

    WebUI.click(findTestObject('Single Transfer/International/Create/DropList - Highlighted'), FailureHandling.STOP_ON_FAILURE)

    BeneficiaryList = WebUI.getText(findTestObject('Single Transfer/International/Create/DropList - Beneficiary List'))

    GlobalVariable.Map123.put('BeneficiaryList', BeneficiaryList)
} else {
    GlobalVariable.Map123.put('TransferTo', 'New Beneficiary')

    WebUI.click(findTestObject('Single Transfer/International/Create/BTN - Transfer To - New Beneficiary'), FailureHandling.STOP_ON_FAILURE)

    if (GlobalVariable.Map123.get('FlagSavetoBeneficiaryList') == 0) {
        WebUI.click(findTestObject('Single Transfer/International/Create/CheckBox - Save to Beneficiary List'), FailureHandling.STOP_ON_FAILURE)

        WebUI.setText(findTestObject('Single Transfer/International/Create/TextField - Alias Name'), AliasName + RandFromAccDes)

        GlobalVariable.Map123.put('AliasName', AliasName + RandFromAccDes)
    }
    
    WebUI.click(findTestObject('Single Transfer/International/Create/DropList - Beneficiary Bank Country'), FailureHandling.STOP_ON_FAILURE)

    WebUI.waitForElementVisible(findTestObject('Single Transfer/International/Create/TextField - Input'), 0)

    WebUI.setText(findTestObject('Single Transfer/International/Create/TextField - Input'), BeneficiaryBankCountry)

    WebUI.waitForElementVisible(findTestObject('Single Transfer/International/Create/DropList - Highlighted'), 0)

    WebUI.click(findTestObject('Single Transfer/International/Create/DropList - Highlighted'), FailureHandling.STOP_ON_FAILURE)

    BeneficiaryBankCountry = WebUI.getText(findTestObject('Single Transfer/International/Create/DropList - Beneficiary Bank Country'))

    GlobalVariable.Map123.put('BeneficiaryBankCountry', BeneficiaryBankCountry)

    WebUI.click(findTestObject('Single Transfer/International/Create/DropList - National Organization Directory'), FailureHandling.STOP_ON_FAILURE)

    WebUI.waitForElementVisible(findTestObject('Single Transfer/International/Create/TextField - Input'), 0)

    WebUI.setText(findTestObject('Single Transfer/International/Create/TextField - Input'), NationalOrganizationDirectory)

    WebUI.waitForElementVisible(findTestObject('Single Transfer/International/Create/DropList - Highlighted'), 0)

    WebUI.click(findTestObject('Single Transfer/International/Create/DropList - Highlighted'), FailureHandling.STOP_ON_FAILURE)

    NationalOrganizationDirectory = WebUI.getText(findTestObject('Single Transfer/International/Create/DropList - National Organization Directory'))

    GlobalVariable.Map123.put('NationalOrganizationDirectory', NationalOrganizationDirectory)

    WebUI.click(findTestObject('Single Transfer/International/Create/DropList - Beneficiary Bank'), FailureHandling.STOP_ON_FAILURE)

    WebUI.waitForElementVisible(findTestObject('Single Transfer/International/Create/TextField - Input'), 0)

    WebUI.setText(findTestObject('Single Transfer/International/Create/TextField - Input'), BeneficiaryBank)

    WebUI.waitForElementVisible(findTestObject('Single Transfer/International/Create/DropList - Highlighted'), 0)

    WebUI.click(findTestObject('Single Transfer/International/Create/DropList - Highlighted'), FailureHandling.STOP_ON_FAILURE)

    BeneficiaryBank = WebUI.getText(findTestObject('Single Transfer/International/Create/DropList - Beneficiary Bank'))

    GlobalVariable.Map123.put('BeneficiaryBank', BeneficiaryBank)

    WebUI.click(findTestObject('Single Transfer/International/Create/DropList - Intermediary Bank'), FailureHandling.STOP_ON_FAILURE)

    WebUI.waitForElementVisible(findTestObject('Single Transfer/International/Create/TextField - Input'), 0)

    WebUI.setText(findTestObject('Single Transfer/International/Create/TextField - Input'), IntermediaryBank)

    WebUI.waitForElementVisible(findTestObject('Single Transfer/International/Create/DropList - Highlighted'), 0)

    WebUI.click(findTestObject('Single Transfer/International/Create/DropList - Highlighted'), FailureHandling.STOP_ON_FAILURE)

    IntermediaryBank = WebUI.getText(findTestObject('Single Transfer/International/Create/DropList - Intermediary Bank'))

    GlobalVariable.Map123.put('IntermediaryBank', IntermediaryBank)

    WebUI.setText(findTestObject('Single Transfer/International/Create/TextField - Beneficiary Account Number'), BeneficiaryAccountNumber)

    GlobalVariable.Map123.put('BeneficiaryAccountNumber', BeneficiaryAccountNumber)

    WebUI.setText(findTestObject('Single Transfer/International/Create/TextField - Beneficiary Account Name'), BeneficiaryAccountName)

    GlobalVariable.Map123.put('BeneficiaryAccountName', BeneficiaryAccountName)

    WebUI.setText(findTestObject('Single Transfer/International/Create/TextField - Address 1'), Address1)

    GlobalVariable.Map123.put('Address1', Address1)

    WebUI.setText(findTestObject('Single Transfer/International/Create/TextField - Address 2'), Address2)

    GlobalVariable.Map123.put('Address2', Address2)

    WebUI.setText(findTestObject('Single Transfer/International/Create/TextField - Email'), Email)

    GlobalVariable.Map123.put('Email', Email)

    WebUI.setText(findTestObject('Single Transfer/International/Create/TextField - Phone'), Phone)

    GlobalVariable.Map123.put('Phone', Phone)
}

WebUI.setText(findTestObject('Single Transfer/International/Create/TextField - To Account Description'), FromAccountDescription + 
    RandFromAccDes, FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Map123.put('ToAccountDescription', FromAccountDescription + RandFromAccDes)

WebUI.setText(findTestObject('Single Transfer/International/Create/TextField - Beneficiary Reference No'), BeneficiaryReferenceNo, 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Map123.put('BeneficiaryReferenceNo', BeneficiaryReferenceNo)

WebUI.click(findTestObject('Single Transfer/International/Create/DropList - Remittance Currency'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Single Transfer/International/Create/TextField - Input'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Single Transfer/International/Create/TextField - Input'), RemittanceCurrency, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Single Transfer/International/Create/DropList - Highlighted'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Single Transfer/International/Create/DropList - Highlighted'), FailureHandling.CONTINUE_ON_FAILURE)

RemittanceCurrency = WebUI.getText(findTestObject('Single Transfer/International/Create/DropList - Remittance Currency'), 
    FailureHandling.CONTINUE_ON_FAILURE)

String[] RemittanceCurrency1 = RemittanceCurrency.split(' - ')

GlobalVariable.Map123.put('RemittanceCurrency', RemittanceCurrency1[0])

WebUI.click(findTestObject('Single Transfer/International/Create/DropList - Amount'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Single Transfer/International/Create/TextField - Input'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Single Transfer/International/Create/TextField - Input'), AmountCurrency, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Single Transfer/International/Create/DropList - Highlighted'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Single Transfer/International/Create/DropList - Highlighted'), FailureHandling.CONTINUE_ON_FAILURE)

AmountCurrency = WebUI.getText(findTestObject('Single Transfer/International/Create/DropList - Amount'), FailureHandling.CONTINUE_ON_FAILURE)

String[] AmountCurrency1 = AmountCurrency.split(' - ')

GlobalVariable.Map123.put('AmountCurrency', AmountCurrency1[0])

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Single Transfer/International/Create/TextField - Amount'), Amount, FailureHandling.CONTINUE_ON_FAILURE)

Locale local1 = new Locale('en', 'UK')

DecimalFormat amountformat = ((NumberFormat.getNumberInstance(local1)) as DecimalFormat)

amountformat.applyPattern('###,###,##0.00')

String amountinquiry12 = amountformat.format(new BigDecimal(Amount))

GlobalVariable.Map123.put('Amount', amountinquiry12)

WebUI.click(findTestObject('Single Transfer/International/Create/DropList - Full Amount Charge'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Single Transfer/International/Create/TextField - Input'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Single Transfer/International/Create/TextField - Input'), FullAmountCharge, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Single Transfer/International/Create/DropList - Highlighted'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Single Transfer/International/Create/DropList - Highlighted'), FailureHandling.CONTINUE_ON_FAILURE)

FullAmountCharge = WebUI.getText(findTestObject('Single Transfer/International/Create/DropList - Full Amount Charge'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Map123.put('FullAmountCharge', FullAmountCharge)

WebUI.click(findTestObject('Single Transfer/International/Create/DropList - Beneficiary Category'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Single Transfer/International/Create/TextField - Input'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Single Transfer/International/Create/TextField - Input'), BeneficiaryCategory, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Single Transfer/International/Create/DropList - Highlighted'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Single Transfer/International/Create/DropList - Highlighted'), FailureHandling.CONTINUE_ON_FAILURE)

BeneficiaryCategory = WebUI.getText(findTestObject('Single Transfer/International/Create/DropList - Beneficiary Category'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Map123.put('BeneficiaryCategory', BeneficiaryCategory)

WebUI.click(findTestObject('Single Transfer/International/Create/DropList - Transactor Relationship'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Single Transfer/International/Create/TextField - Input'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Single Transfer/International/Create/TextField - Input'), TransactorRelationship, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Single Transfer/International/Create/DropList - Highlighted'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Single Transfer/International/Create/DropList - Highlighted'), FailureHandling.CONTINUE_ON_FAILURE)

TransactorRelationship = WebUI.getText(findTestObject('Single Transfer/International/Create/DropList - Transactor Relationship'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Map123.put('TransactorRelationship', TransactorRelationship)

WebUI.click(findTestObject('Single Transfer/International/Create/DropList - Purpose Of Transaction'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Single Transfer/International/Create/TextField - Input'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Single Transfer/International/Create/TextField - Input'), PurposeOfTransaction, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Single Transfer/International/Create/DropList - Highlighted'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Single Transfer/International/Create/DropList - Highlighted'), FailureHandling.CONTINUE_ON_FAILURE)

PurposeOfTransaction = WebUI.getText(findTestObject('Single Transfer/International/Create/DropList - Purpose Of Transaction'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Map123.put('PurposeOfTransaction', PurposeOfTransaction)

WebUI.click(findTestObject('Single Transfer/International/Create/DropList - LHBU Purpose Code'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Single Transfer/International/Create/TextField - Input'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Single Transfer/International/Create/TextField - Input'), LHBUPurposeCode, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Single Transfer/International/Create/DropList - Highlighted'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Single Transfer/International/Create/DropList - Highlighted'), FailureHandling.CONTINUE_ON_FAILURE)

LHBUPurposeCode = WebUI.getText(findTestObject('Single Transfer/International/Create/DropList - LHBU Purpose Code'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Map123.put('LHBUPurposeCode', LHBUPurposeCode)

WebUI.click(findTestObject('Single Transfer/International/Create/DropList - LHBU Document Type'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Single Transfer/International/Create/TextField - Input'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Single Transfer/International/Create/TextField - Input'), LHBUDocumentType, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Single Transfer/International/Create/DropList - Highlighted'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Single Transfer/International/Create/DropList - Highlighted'), FailureHandling.CONTINUE_ON_FAILURE)

LHBUDocumentType = WebUI.getText(findTestObject('Single Transfer/International/Create/DropList - LHBU Document Type'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Map123.put('LHBUDocumentType', LHBUDocumentType)

WebUI.setText(findTestObject('Single Transfer/International/Create/TextField - LHBU Document Type Description'), LHBUDocumentTypeDescription, 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Map123.put('LHBUDocumentTypeDescription', LHBUDocumentTypeDescription)

WebUI.click(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/BTN - Specific Date'))

WebUI.delay(1)

WebUI.waitForElementVisible(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/DropList - Specific Date - Future Date'), 
    0)

WebUI.click(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/DropList - Specific Date - Future Date'))

WebUI.waitForElementVisible(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/DropList - Specific Date - Future Date - Date'), 
    0)

WebUI.click(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/DropList - Specific Date - Future Date - Date'), 
    FailureHandling.STOP_ON_FAILURE)

WebUI.delay(1)

WebUI.click(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/DropList - Specific Date - At'))

WebUI.waitForElementVisible(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/Text Field - Set Text'), 
    0)

WebUI.setText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/Text Field - Set Text'), 
    '10')

WebUI.waitForElementVisible(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/DropList - Select Account - Highlight'), 
    0)

WebUI.click(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/DropList - Select Account - Highlight'))

SpecificAt = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/DropList - Specific Date - At'))

String[] SpecificA1 = SpecificAt.split(' - ')

GlobalVariable.Map123.put('SpecificAt', SpecificA1[1])

InstructionModeActive = WebUI.getText(findTestObject('Cash Pooling/New Entry/Label - Instruction Mode'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Map123.put('InstructionMode', InstructionModeActive)

WebUI.click(findTestObject('Single Transfer/International/Create/CheckBox - Agree the term and condition'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Single Transfer/International/Create/BTN - Confirm'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Single Transfer/International/Create/BTN - Submit (Pop Up)'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Single Transfer/International/Create/BTN - Submit (Pop Up)'), FailureHandling.CONTINUE_ON_FAILURE)

