import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.waitForElementVisible(findTestObject('Single Transfer/International/Confirm/BTN - Submit'), 0, FailureHandling.CONTINUE_ON_FAILURE)

SubLabel = WebUI.getText(findTestObject('Single Transfer/International/Confirm/Label - Sub Label'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(SubLabel, 'Confirm', false, FailureHandling.CONTINUE_ON_FAILURE)

TransferFrom = WebUI.getText(findTestObject('Single Transfer/International/Confirm/Label - Transfer From'), FailureHandling.CONTINUE_ON_FAILURE)

TransferFrom = TransferFrom.replace(': ', '')

RevTransferFrom = GlobalVariable.Map123.get('TransferFrom')

RevTransferFrom = RevTransferFrom.replace('(', ' (')

RevTransferFrom = RevTransferFrom.replace('-', '/')

WebUI.verifyMatch(TransferFrom, RevTransferFrom, false, FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Map123.put('TransferFrom', RevTransferFrom)

FromAccountDescription = WebUI.getText(findTestObject('Single Transfer/International/Confirm/Label - From Account Description'), 
    FailureHandling.CONTINUE_ON_FAILURE)

FromAccountDescription = FromAccountDescription.replace(': ', '')

WebUI.verifyMatch(FromAccountDescription, GlobalVariable.Map123.get('FromAccountDescription'), false, FailureHandling.CONTINUE_ON_FAILURE)

TransferTo = WebUI.getText(findTestObject('Single Transfer/International/Confirm/Label - Transfer To'), FailureHandling.CONTINUE_ON_FAILURE)

TransferTo = TransferTo.replace(': ', '')

WebUI.verifyMatch(TransferTo, GlobalVariable.Map123.get('TransferTo'), false, FailureHandling.CONTINUE_ON_FAILURE)

if (GlobalVariable.Map123.get('TransferTo') == 'Beneficiary List') {
    AccountNumber = WebUI.getText(findTestObject('Single Transfer/International/Confirm/Label - Account Number'), FailureHandling.CONTINUE_ON_FAILURE)

    AccountNumber = AccountNumber.replace(': ', '')

    WebUI.verifyMatch(AccountNumber, GlobalVariable.Map123.get('BeneficiaryList'), false, FailureHandling.CONTINUE_ON_FAILURE)
} else {
    BeneficiaryBankCountry = WebUI.getText(findTestObject('Single Transfer/International/Confirm/Label - Beneficiary Bank Country'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    BeneficiaryBankCountry = BeneficiaryBankCountry.replace(': ', '')

    WebUI.verifyMatch(BeneficiaryBankCountry, GlobalVariable.Map123.get('BeneficiaryBankCountry'), false, FailureHandling.CONTINUE_ON_FAILURE)

    NationalOrganizationDirectory = WebUI.getText(findTestObject('Single Transfer/International/Confirm/Label - National Organization Directory'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    NationalOrganizationDirectory = NationalOrganizationDirectory.replace(': ', '')

    WebUI.verifyMatch(NationalOrganizationDirectory, GlobalVariable.Map123.get('NationalOrganizationDirectory'), false, 
        FailureHandling.CONTINUE_ON_FAILURE)

    BeneficiaryBank = WebUI.getText(findTestObject('Single Transfer/International/Confirm/Label - Beneficiary Bank'), FailureHandling.CONTINUE_ON_FAILURE)

    BeneficiaryBank = BeneficiaryBank.replace(': ', '')

    WebUI.verifyMatch(BeneficiaryBank, GlobalVariable.Map123.get('BeneficiaryBank'), false, FailureHandling.CONTINUE_ON_FAILURE)

    BankCity = WebUI.getText(findTestObject('Single Transfer/International/Confirm/Label - Bank City'), FailureHandling.CONTINUE_ON_FAILURE)

    BankCity = BankCity.replace(': ', '')

    GlobalVariable.Map123.put('BankCity', BankCity)

    IntermediaryBank = WebUI.getText(findTestObject('Single Transfer/International/Confirm/Label - Intermediary Bank'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    IntermediaryBank = IntermediaryBank.replace(': ', '')

    WebUI.verifyMatch(IntermediaryBank, GlobalVariable.Map123.get('IntermediaryBank'), false, FailureHandling.CONTINUE_ON_FAILURE)

    AccountNumber = WebUI.getText(findTestObject('Single Transfer/International/Confirm/Label - Account Number'), FailureHandling.CONTINUE_ON_FAILURE)

    AccountNumber = AccountNumber.replace(': ', '')

    WebUI.verifyMatch(AccountNumber, GlobalVariable.Map123.get('BeneficiaryAccountNumber'), false, FailureHandling.CONTINUE_ON_FAILURE)

    SavetoBeneficiaryList = WebUI.getText(findTestObject('Single Transfer/International/Confirm/Label - Save to Beneficiary List'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    SavetoBeneficiaryList = SavetoBeneficiaryList.replace(': ', '')

    if (GlobalVariable.Map123.get('FlagSavetoBeneficiaryList') == 0) {
        WebUI.verifyMatch(SavetoBeneficiaryList, 'Yes', false)

        AliasName = WebUI.getText(findTestObject('Single Transfer/International/Confirm/Label - Alias Name'))

        AliasName = AliasName.replace(': ', '')

        WebUI.verifyMatch(AliasName, GlobalVariable.Map123.get('AliasName'), false)
    } else {
        WebUI.verifyMatch(SavetoBeneficiaryList, 'No', false)
    }
    
    AccountName = WebUI.getText(findTestObject('Single Transfer/International/Confirm/Label - Account Name'), FailureHandling.CONTINUE_ON_FAILURE)

    AccountName = AccountName.replace(': ', '')

    WebUI.verifyMatch(AccountName, GlobalVariable.Map123.get('BeneficiaryAccountName'), false, FailureHandling.CONTINUE_ON_FAILURE)

    Address1 = WebUI.getText(findTestObject('Single Transfer/International/Confirm/Label - Address 1'), FailureHandling.CONTINUE_ON_FAILURE)

    Address1 = Address1.replace(': ', '')

    WebUI.verifyMatch(Address1, GlobalVariable.Map123.get('Address1'), false, FailureHandling.CONTINUE_ON_FAILURE)

    Address2 = WebUI.getText(findTestObject('Single Transfer/International/Confirm/Label - Address 2'), FailureHandling.CONTINUE_ON_FAILURE)

    Address2 = Address2.replace(': ', '')

    WebUI.verifyMatch(Address2, GlobalVariable.Map123.get('Address2'), false, FailureHandling.CONTINUE_ON_FAILURE)

    Email = WebUI.getText(findTestObject('Single Transfer/International/Confirm/Label - Email'), FailureHandling.CONTINUE_ON_FAILURE)

    String[] EmailSplit = Email.split(' :  ')

    WebUI.verifyMatch(EmailSplit[1], GlobalVariable.Map123.get('Email'), false, FailureHandling.CONTINUE_ON_FAILURE)

    GlobalVariable.Map123.put('Email', ' ' + (EmailSplit[1]))

    Phone = WebUI.getText(findTestObject('Single Transfer/International/Confirm/Label - Phone'), FailureHandling.CONTINUE_ON_FAILURE)

    String[] PhoneSplit = Phone.split(' :  ')

    WebUI.verifyMatch(PhoneSplit[1], GlobalVariable.Map123.get('Phone'), false, FailureHandling.CONTINUE_ON_FAILURE)

    GlobalVariable.Map123.put('Phone', ' ' + (PhoneSplit[1]))
}

BeneficiaryReferenceNo = WebUI.getText(findTestObject('Single Transfer/International/Confirm/Label - Beneficiary Reference No'), 
    FailureHandling.CONTINUE_ON_FAILURE)

BeneficiaryReferenceNo = BeneficiaryReferenceNo.replace(': ', '')

WebUI.verifyMatch(BeneficiaryReferenceNo, GlobalVariable.Map123.get('BeneficiaryReferenceNo'), false, FailureHandling.CONTINUE_ON_FAILURE)

ToAccountDescription = WebUI.getText(findTestObject('Single Transfer/International/Confirm/Label - To Account Description'), 
    FailureHandling.CONTINUE_ON_FAILURE)

ToAccountDescription = ToAccountDescription.replace(': ', '')

WebUI.verifyMatch(ToAccountDescription, GlobalVariable.Map123.get('ToAccountDescription'), false, FailureHandling.CONTINUE_ON_FAILURE)

RemittanceCurrency = WebUI.getText(findTestObject('Single Transfer/International/Confirm/Label - Remittance Currency'), 
    FailureHandling.CONTINUE_ON_FAILURE)

RemittanceCurrency = RemittanceCurrency.replace(': ', '')

WebUI.verifyMatch(RemittanceCurrency, GlobalVariable.Map123.get('RemittanceCurrency'), false, FailureHandling.CONTINUE_ON_FAILURE)

Amount = WebUI.getText(findTestObject('Single Transfer/International/Confirm/Label - Amount'), FailureHandling.CONTINUE_ON_FAILURE)

Amount = Amount.replace(': ', '')

WebUI.verifyMatch(Amount, (GlobalVariable.Map123.get('AmountCurrency') + '   ') + GlobalVariable.Map123.get('Amount'), false, 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Map123.put('Amount', Amount)

FullAmountCharge = WebUI.getText(findTestObject('Single Transfer/International/Confirm/Label - Full Amount Charge'), FailureHandling.CONTINUE_ON_FAILURE)

FullAmountCharge = FullAmountCharge.replace(': ', '')

WebUI.verifyMatch(FullAmountCharge, GlobalVariable.Map123.get('FullAmountCharge'), false, FailureHandling.CONTINUE_ON_FAILURE)

DebitCharge = WebUI.getText(findTestObject('Single Transfer/International/Confirm/Label - Debit Charge'), FailureHandling.CONTINUE_ON_FAILURE)

DebitCharge = DebitCharge.replace(': ', '')

WebUI.verifyMatch(DebitCharge, GlobalVariable.Map123.get('DebitCharge'), false, FailureHandling.CONTINUE_ON_FAILURE)

FullAmountCharge2 = WebUI.getText(findTestObject('Single Transfer/International/Confirm/Label - Full Amount Charge 2'), 
    FailureHandling.CONTINUE_ON_FAILURE)

FullAmountCharge2 = FullAmountCharge2.replace(': ', '')

GlobalVariable.Map123.put('FullAmountCharge2', FullAmountCharge2)

CableFee = WebUI.getText(findTestObject('Single Transfer/International/Confirm/Label - Cable Fee'), FailureHandling.CONTINUE_ON_FAILURE)

CableFee = CableFee.replace(': ', '')

GlobalVariable.Map123.put('CableFee', CableFee)

if (CableFee.substring(0, 3) == 'IDR') {
    CableFeeEquivalent = WebUI.getText(findTestObject('Single Transfer/International/Confirm/Label - Cable Fee Equivalent'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    CableFeeEquivalent = CableFeeEquivalent.replace(': ', '')

    GlobalVariable.Map123.put('CableFeeEquivalent', CableFeeEquivalent)
}

Provision = WebUI.getText(findTestObject('Single Transfer/International/Confirm/Label - Provision'), FailureHandling.CONTINUE_ON_FAILURE)

Provision = Provision.replace(': ', '')

GlobalVariable.Map123.put('Provision', Provision)

if (Provision.substring(0, 3) == 'IDR') {
    ProvisionEquivalent = WebUI.getText(findTestObject('Single Transfer/International/Confirm/Label - Provision Equivalent'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    ProvisionEquivalent = ProvisionEquivalent.replace(': ', '')

    GlobalVariable.Map123.put('ProvisionEquivalent', ProvisionEquivalent)
}

CorrespondentBankFee = WebUI.getText(findTestObject('Single Transfer/International/Confirm/Label - Correspondent Bank Fee'), 
    FailureHandling.CONTINUE_ON_FAILURE)

CorrespondentBankFee = CorrespondentBankFee.replace(': ', '')

GlobalVariable.Map123.put('CorrespondentBankFee', CorrespondentBankFee)

if (CorrespondentBankFee.substring(0, 3) == 'IDR') {
    CorrespondentBankFeeEquivalent = WebUI.getText(findTestObject('Single Transfer/International/Confirm/Label - Correspondent Bank Fee Equivalent'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    CorrespondentBankFeeEquivalent = CorrespondentBankFeeEquivalent.replace(': ', '')

    GlobalVariable.Map123.put('CorrespondentBankFeeEquivalent', CorrespondentBankFeeEquivalent)
}

InLieu = WebUI.getText(findTestObject('Single Transfer/International/Confirm/Label - In Lieu'), FailureHandling.CONTINUE_ON_FAILURE)

InLieu = InLieu.replace(': ', '')

GlobalVariable.Map123.put('InLieu', InLieu)

if (InLieu.substring(0, 3) == 'IDR') {
    InLieuEquivalent = WebUI.getText(findTestObject('Single Transfer/International/Confirm/Label - In Lieu Equivalent'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    InLieuEquivalent = InLieuEquivalent.replace(': ', '')

    GlobalVariable.Map123.put('InLieuEquivalent', InLieuEquivalent)
}

MinimumTransactionFee = WebUI.getText(findTestObject('Single Transfer/International/Confirm/Label - Minimum Transaction Fee'), 
    FailureHandling.CONTINUE_ON_FAILURE)

MinimumTransactionFee = MinimumTransactionFee.replace(': ', '')

GlobalVariable.Map123.put('MinimumTransactionFee', MinimumTransactionFee)

if (InLieu.substring(0, 3) == 'IDR') {
    MinimumTransactionFeeEquivalent = WebUI.getText(findTestObject('Single Transfer/International/Confirm/Label - Minimum Transaction Fee Equivalent'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    MinimumTransactionFeeEquivalent = MinimumTransactionFeeEquivalent.replace(': ', '')

    GlobalVariable.Map123.put('MinimumTransactionFeeEquivalent', MinimumTransactionFeeEquivalent)
}

TotalCharge = WebUI.getText(findTestObject('Single Transfer/International/Confirm/Label - Total Charge'), FailureHandling.CONTINUE_ON_FAILURE)

TotalCharge = TotalCharge.replace(': ', '')

GlobalVariable.Map123.put('TotalCharge', TotalCharge)

ChargetoRemitter = WebUI.getText(findTestObject('Single Transfer/International/Confirm/Label - Charge to Remitter'), FailureHandling.CONTINUE_ON_FAILURE)

ChargetoRemitter = ChargetoRemitter.replace(': ', '')

GlobalVariable.Map123.put('ChargetoRemitter', ChargetoRemitter)

ChargetoBeneficiary = WebUI.getText(findTestObject('Single Transfer/International/Confirm/Label - Charge to Beneficiary'), 
    FailureHandling.CONTINUE_ON_FAILURE)

ChargetoBeneficiary = ChargetoBeneficiary.replace(': ', '')

GlobalVariable.Map123.put('ChargetoBeneficiary', ChargetoBeneficiary)

TotalDebitAmount = WebUI.getText(findTestObject('Single Transfer/International/Confirm/Label - Total Debit Amount'), FailureHandling.CONTINUE_ON_FAILURE)

TotalDebitAmount = TotalDebitAmount.replace(': ', '')

GlobalVariable.Map123.put('TotalDebitAmount', TotalDebitAmount)

BeneficiaryResidentStatus = WebUI.getText(findTestObject('Single Transfer/International/Confirm/Label - Beneficiary Resident Status'), 
    FailureHandling.CONTINUE_ON_FAILURE)

BeneficiaryResidentStatus = BeneficiaryResidentStatus.replace(': ', '')

WebUI.verifyMatch(BeneficiaryResidentStatus, GlobalVariable.Map123.get('BeneficiaryResidentStatus'), false, FailureHandling.CONTINUE_ON_FAILURE)

BeneficiaryCategory = WebUI.getText(findTestObject('Single Transfer/International/Confirm/Label - Beneficiary Category'), 
    FailureHandling.CONTINUE_ON_FAILURE)

BeneficiaryCategory = BeneficiaryCategory.replace(': ', '')

WebUI.verifyMatch(BeneficiaryCategory, GlobalVariable.Map123.get('BeneficiaryCategory'), false, FailureHandling.CONTINUE_ON_FAILURE)

TransactorRelationship = WebUI.getText(findTestObject('Single Transfer/International/Confirm/Label - Transactor Relationship'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TransactorRelationship = TransactorRelationship.replace(': ', '')

WebUI.verifyMatch(TransactorRelationship, GlobalVariable.Map123.get('TransactorRelationship'), false, FailureHandling.CONTINUE_ON_FAILURE)

IdenticalStatus = WebUI.getText(findTestObject('Single Transfer/International/Confirm/Label - Identical Status'), FailureHandling.CONTINUE_ON_FAILURE)

IdenticalStatus = IdenticalStatus.replace(': ', '')

WebUI.verifyMatch(IdenticalStatus, GlobalVariable.Map123.get('IdenticalStatus'), false, FailureHandling.CONTINUE_ON_FAILURE)

PurposeOfTransaction = WebUI.getText(findTestObject('Single Transfer/International/Confirm/Label - Purpose Of Transaction'), 
    FailureHandling.CONTINUE_ON_FAILURE)

PurposeOfTransaction = PurposeOfTransaction.replace(': ', '')

WebUI.verifyMatch(PurposeOfTransaction, GlobalVariable.Map123.get('PurposeOfTransaction'), false, FailureHandling.CONTINUE_ON_FAILURE)

LHBUPurposeCode = WebUI.getText(findTestObject('Single Transfer/International/Confirm/Label - LHBU Purpose Code'), FailureHandling.CONTINUE_ON_FAILURE)

LHBUPurposeCode = LHBUPurposeCode.replace(': ', '')

WebUI.verifyMatch(LHBUPurposeCode, GlobalVariable.Map123.get('LHBUPurposeCode'), false, FailureHandling.CONTINUE_ON_FAILURE)

LHBUDocumentType = WebUI.getText(findTestObject('Single Transfer/International/Confirm/Label - LHBU Document Type'), FailureHandling.CONTINUE_ON_FAILURE)

LHBUDocumentType = LHBUDocumentType.replace(': ', '')

WebUI.verifyMatch(LHBUDocumentType, GlobalVariable.Map123.get('LHBUDocumentType'), false, FailureHandling.CONTINUE_ON_FAILURE)

LHBUDocumentTypeDescription = WebUI.getText(findTestObject('Single Transfer/International/Confirm/Label - LHBU Document Type Description'), 
    FailureHandling.CONTINUE_ON_FAILURE)

LHBUDocumentTypeDescription = LHBUDocumentTypeDescription.replace(': ', '')

WebUI.verifyMatch(LHBUDocumentTypeDescription, GlobalVariable.Map123.get('LHBUDocumentTypeDescription'), false, FailureHandling.CONTINUE_ON_FAILURE)

InstructionMode = WebUI.getText(findTestObject('Single Transfer/International/Confirm/Label - Instruction Mode'), FailureHandling.CONTINUE_ON_FAILURE)

InstructionMode = InstructionMode.replace(': ', '')

WebUI.verifyMatch(InstructionMode, 'Immediate', false, FailureHandling.CONTINUE_ON_FAILURE)

Expiredon = WebUI.getText(findTestObject('Single Transfer/International/Confirm/Label - Expired on'), FailureHandling.CONTINUE_ON_FAILURE)

Expiredon = Expiredon.replace(': ', '')

GlobalVariable.Map123.put('Expiredon', Expiredon)

WebUI.waitForElementVisible(findTestObject('Pending Task/Balance Transfer/Inhouse/Detail/TextField - Response Code'), 0, 
    FailureHandling.CONTINUE_ON_FAILURE)

String abc = '123456'

WebUI.setText(findTestObject('Pending Task/Balance Transfer/Inhouse/Detail/TextField - Response Code'), abc, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Balance Transfer/Balance Transfer - International New/Confirm/BTN - Submit'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Balance Transfer/Balance Transfer - International New/Confirm/BTN - Submit (Pop Up)'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Balance Transfer/Balance Transfer - International New/Confirm/BTN - Submit (Pop Up)'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

