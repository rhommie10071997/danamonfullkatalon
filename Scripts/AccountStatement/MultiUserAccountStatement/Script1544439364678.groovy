import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.navigateToUrl('https://10.194.8.106:39990/', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-CorpID'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-CorpID'), GlobalVariable.MultiUserLoginCorpregId, 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-UserID'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-UserID'), GlobalVariable.MultiUserLoginId, 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-UserPass'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-UserPass'), GlobalVariable.MultiUserPass, 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/B-Login'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-MenuSearch'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-MenuSearch'), 'Account Statement', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('AccountStatement/EntryScreen/T-accountStatement'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/B-Menu'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(3, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('AccountStatement/EntryScreen/B-accountStatement'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('AccountStatement/EntryScreen/B-searchForAccount'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('AccountStatement/EntryScreen/TF-accountName'), 'aang', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('AccountStatement/EntryScreen/B-detailsSearch'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('AccountStatement/EntryScreen/CB-account'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('AccountStatement/EntryScreen/B-detailsShow'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('AccountStatement/EntryScreen/TF-month'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), 'october', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), Keys.chord(Keys.ENTER), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('AccountStatement/EntryScreen/B-display'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('AccountStatement/EntryScreen/TF-fileFormat '), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), 'pdf', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), Keys.chord(Keys.ENTER), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('AccountStatement/EntryScreen/TF-reportName'), 'FileTesting', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('AccountStatement/EntryScreen/TF-fileMapping'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), Keys.chord(Keys.ENTER), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('AccountStatement/EntryScreen/B-download'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-MenuSearch'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-MenuSearch'), 'Download Report', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('AccountStatement/EntryScreen/T-downloadReport'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/B-Menu'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('AccountStatement/EntryScreen/TF-insertFileName'), 'FileTesting', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('AccountStatement/EntryScreen/B-searchResult'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(15, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.executeJavaScript('document.getElementByTagName("navbar navbar-inverse navbar-fixed-top bs-docs-nav")[0].style.position="initial";', [])

CustomKeywords.'get.Screencapture.getEntirePage'(LoginSuccess)

WebUI.click(findTestObject('AccountStatement/EntryScreen/I-download'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

