import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(10, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/BTN - clickMenu'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.scrollToElement(findTestObject('menu/report/reportCLick'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/report/reportCLick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('menu/report/transStatus/Td - transStatusClick'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/report/transStatus/Td - transStatusClick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(9, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/report/transStatus/rbTransactionReferenceNo/rbTransactionReferenceNo'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(4, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('menu/report/transStatus/referenceTransaction/refTextfield'), GlobalVariable.confirmScreens.get(
        'refNum'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/report/transStatus/btnShow/btnShow'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(7, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/report/transStatus/referenceTransaction/refClick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(9, FailureHandling.CONTINUE_ON_FAILURE)

