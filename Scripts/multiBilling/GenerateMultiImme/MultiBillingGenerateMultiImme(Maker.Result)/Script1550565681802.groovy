import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(10, FailureHandling.CONTINUE_ON_FAILURE)

confirmScreenStatus = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - confirmScreenStatus'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(confirmScreenStatus, 'Your transaction is waiting for approval', false, FailureHandling.CONTINUE_ON_FAILURE)

refNo2 = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - refNo(Generate)'), FailureHandling.CONTINUE_ON_FAILURE)

String[] refNo2Split = refNo2.split(' ')

String refNo2Result = refNo2Split[2]

GlobalVariable.confirmScreens.put('refNum', refNo2Result)

submitDateChecker = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - submitDateTime(Generate)'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('submitDateChecker', submitDateChecker)

WebUI.verifyMatch(submitDateChecker, GlobalVariable.confirmScreens.get('submitDateChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

FileTypeCheckerD = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - FileType'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(FileTypeCheckerD, GlobalVariable.confirmScreens.get('FileTypeChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

FileTemplateD = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - FileTemplate'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(FileTemplateD, GlobalVariable.confirmScreens.get('FileTemplateChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

TaxBillingTypeCheckerD = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - taxBillingType'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TaxBillingTypeCheckerD, GlobalVariable.confirmScreens.get('TaxBillingTypeChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

FileNameCheckerCon = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - FileName'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(FileNameCheckerCon, GlobalVariable.confirmScreens.get('FileNameChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

FileDescriptionCheckerCon = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - FileDescription'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(FileDescriptionCheckerCon, GlobalVariable.confirmScreens.get('FileDescriptionChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

totalTransactionRecordCheckerCon = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - totalTransactionRecord (Generate)'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('totalTransactionRecordCheckerCon', totalTransactionRecordCheckerCon)

WebUI.verifyMatch(totalTransactionRecordCheckerCon, GlobalVariable.confirmScreens.get('totalTransactionRecordCheckerCon'), 
    false, FailureHandling.CONTINUE_ON_FAILURE)

trfFormCheckerCon = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - trfFrom'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(trfFormCheckerCon, GlobalVariable.confirmScreens.get('trfFormCheckerCon'), false, FailureHandling.CONTINUE_ON_FAILURE)

insModeCheckerCon = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - insMode'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(insModeCheckerCon, GlobalVariable.confirmScreens.put('insModeCheckerCon', insModeCheckerCon), false, FailureHandling.CONTINUE_ON_FAILURE)

expiredCheckerCon = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - expiredOn'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(expiredCheckerCon, GlobalVariable.confirmScreens.get('expiredCheckerCon'), false, FailureHandling.CONTINUE_ON_FAILURE)

amountCheckerCon = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - totalAmount'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(amountCheckerCon, GlobalVariable.confirmScreens.get('amountCheckerCon'), false, FailureHandling.CONTINUE_ON_FAILURE)

totalChargesCheckerCon = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - totalCharges'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(totalChargesCheckerCon, GlobalVariable.confirmScreens.get('totalChargesCheckerCon'), false, FailureHandling.CONTINUE_ON_FAILURE)

totalDebitAmountCheckerCon = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - totalDebitAmount'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(totalDebitAmountCheckerCon, GlobalVariable.confirmScreens.get('totalDebitAmountCheckerCon'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(6, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/MultiBilling/BTN - SeeMoreRecord'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(4, FailureHandling.CONTINUE_ON_FAILURE)

TaxBillingTypeCheckerDet = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/Label - taxBillingType'), FailureHandling.CONTINUE_ON_FAILURE)

trfFormCheckerDet = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/Label - trfFrom'), FailureHandling.CONTINUE_ON_FAILURE)

insDateCheckerDet = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/Label - insDate'), FailureHandling.CONTINUE_ON_FAILURE)

totalRecordCheckerDet = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/Label - totalTransactionRecord'), FailureHandling.CONTINUE_ON_FAILURE)

totalAmountCheckerDet = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/Label - totalAmount'), FailureHandling.CONTINUE_ON_FAILURE)

paymentFeeCheckerDet = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/Label - serviceFee'), FailureHandling.CONTINUE_ON_FAILURE)

totalChargesCheckerDet = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/Label - totalCharges'), FailureHandling.CONTINUE_ON_FAILURE)

totalDebitAmountCheckerDet = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/Label - totalDebitAmount'), FailureHandling.CONTINUE_ON_FAILURE)

AllTablesCheckerDet = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/Tbody - AllAccountGetText - DetailTable'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TaxBillingTypeCheckerDet, GlobalVariable.confirmScreens.get('TaxBillingTypeCheckerDet'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(trfFormCheckerDet, GlobalVariable.confirmScreens.get('trfFormCheckerDet'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(insDateCheckerDet, GlobalVariable.confirmScreens.get('insDateCheckerDet'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(totalRecordCheckerDet, GlobalVariable.confirmScreens.get('totalRecordCheckerDet'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(totalAmountCheckerDet, GlobalVariable.confirmScreens.get('totalAmountCheckerDet'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(paymentFeeCheckerDet, GlobalVariable.confirmScreens.get('paymentFeeCheckerDet'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(totalChargesCheckerDet, GlobalVariable.confirmScreens.get('totalChargesCheckerDet'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(totalDebitAmountCheckerDet, GlobalVariable.confirmScreens.get('totalDebitAmountCheckerDet'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(AllTablesCheckerDet, GlobalVariable.confirmScreens.get('getTable'), false, FailureHandling.CONTINUE_ON_FAILURE)

TdNpwp = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/Td - npwp - detailTable(Generate)'), FailureHandling.CONTINUE_ON_FAILURE)

TdAkunCode = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/Td - AkunCode - detailTable(Generate)'), FailureHandling.CONTINUE_ON_FAILURE)

TdKodeJenisSetoran = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/Td - KodeJenisSetoran - detailTable(Generate)'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TdAmount = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/Td - Amount - detailTable(Generate)'), FailureHandling.CONTINUE_ON_FAILURE)

TdTaxPeriod = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/Td - TaxPeriod - detailTable(Generate)'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TdNpwp, GlobalVariable.confirmScreens.get('smNpwp'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TdAkunCode, GlobalVariable.confirmScreens.get('smAkunCode'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TdKodeJenisSetoran, GlobalVariable.confirmScreens.get('smKodeJenisSetoran'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TdAmount, GlobalVariable.confirmScreens.get('smAmount'), false, FailureHandling.CONTINUE_ON_FAILURE)

not_run: WebUI.verifyMatch(TdTaxPeriod, GlobalVariable.confirmScreens.get('smTaxPeriod'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(4, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/MultiBilling/BTN - Close - DetailRecord'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(3, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/BTN - logoutClick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(20, FailureHandling.CONTINUE_ON_FAILURE)

