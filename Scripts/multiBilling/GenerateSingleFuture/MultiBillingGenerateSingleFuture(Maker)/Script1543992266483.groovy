import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

String similarFlag = 'On'

String Directory = 'C:\\Users\\user\\Desktop\\Csv\\multi billing\\Generate001 - futureSingle.csv'

String FileDescription = 'GenerateSingleFuture'

WebUI.setText(findTestObject('homelogin/homelogin.corporateid'), 'PCMKILLUA', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('homelogin/homelogin.userid'), 'killua', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('homelogin/homelogin.password'), 'Password123', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('homelogin/homelogin.submit'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(10, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('menu/BTN - clickMenu'), 0)

WebUI.click(findTestObject('menu/BTN - clickMenu'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(4, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/MultiBilling/BTN - TaxPaymentClick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(4, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.scrollToElement(findTestObject('menu/MultiBilling/BTN - MultiBillingClick'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/MultiBilling/BTN - MultiBillingClick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('menu/MultiBilling/BTN - GenarateBillingClick'), 0)

WebUI.click(findTestObject('menu/MultiBilling/BTN - GenarateBillingClick'))

WebUI.delay(4, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/MultiBilling/DL - FileTemplateClick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/MultiBilling/BTN - Choices(1)'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(4, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('menu/MultiBilling/Value - ChooseFile'), Directory, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(4, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('menu/MultiBilling/Value - FileDescription'), FileDescription, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(4, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('menu/MultiBilling/CheckBox - Terms'), 0, FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/MultiBilling/CheckBox - Terms'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/MultiBilling/BTN - Continue'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(12, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/MultiBilling/BTN - BucketClick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/MultiBilling/BTN - TableClick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

FileUploadStatusChecker = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - FileUploadStatus'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('FileUploadStatusChecker', FileUploadStatusChecker)

WebUI.verifyMatch(FileUploadStatusChecker, GlobalVariable.confirmScreens.get('FileUploadStatusChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

FileTypeChecker = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - FileType'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('FileTypeChecker', FileTypeChecker)

WebUI.verifyMatch(FileTypeChecker, GlobalVariable.confirmScreens.get('FileTypeChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

FileTemplateChecker = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - FileTemplate'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('FileTemplateChecker', FileTemplateChecker)

WebUI.verifyMatch(FileTemplateChecker, GlobalVariable.confirmScreens.get('FileTemplateChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

TaxBillingTypeChecker = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - taxBillingType'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('TaxBillingTypeChecker', TaxBillingTypeChecker)

WebUI.verifyMatch(TaxBillingTypeChecker, GlobalVariable.confirmScreens.get('TaxBillingTypeChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

FileNameChecker = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - FileName'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('FileNameChecker', FileNameChecker)

WebUI.verifyMatch(FileNameChecker, GlobalVariable.confirmScreens.get('FileNameChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

FileDescriptionChecker = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - FileDescription'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('FileDescriptionChecker', FileDescriptionChecker)

WebUI.verifyMatch(FileDescriptionChecker, GlobalVariable.confirmScreens.get('FileDescriptionChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

totalRecordChecker = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - totalRecord'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('totalRecordChecker', totalRecordChecker)

WebUI.verifyMatch(totalRecordChecker, GlobalVariable.confirmScreens.get('totalRecordChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

TotalSuccessChecker = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - totalSuccess'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('TotalSuccessChecker', TotalSuccessChecker)

WebUI.verifyMatch(TotalSuccessChecker, GlobalVariable.confirmScreens.get('TotalSuccessChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

TotalFailedChecker = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - totalFailed'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('TotalFailedChecker', TotalFailedChecker)

WebUI.verifyMatch(TotalFailedChecker, GlobalVariable.confirmScreens.get('TotalFailedChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

TotalAmountChecker = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - totalAmount'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('TotalAmountChecker', TotalAmountChecker)

WebUI.verifyMatch(TotalAmountChecker, GlobalVariable.confirmScreens.get('TotalAmountChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/MultiBilling/BTN - Confirm'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(6, FailureHandling.CONTINUE_ON_FAILURE)

if (similarFlag.equals('On')) {
    WebUI.delay(6, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.scrollToElement(findTestObject('menu/MultiBilling/DetailRecord/Checkbox - ClickCheckboxTable (Generate)'), 0, 
        FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.click(findTestObject('menu/MultiBilling/DetailRecord/Checkbox - ClickCheckboxTable (Generate)'), FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    not_run: smBillingId = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/SimilarTransaction/Td - BillingId'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    smNpwp = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/SimilarTransaction/Td - Npwp(DetailRecord)'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    smAkunCode = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/SimilarTransaction/Td - AkunCode'), FailureHandling.CONTINUE_ON_FAILURE)

    smKodeJenisSetoran = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/SimilarTransaction/Td - KodeJenisSetoran'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    smTaxPeriod = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/SimilarTransaction/Td - TaxPeriod'), FailureHandling.CONTINUE_ON_FAILURE)

    smAmount = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/SimilarTransaction/Td - Amount'), FailureHandling.CONTINUE_ON_FAILURE)

    GlobalVariable.confirmScreens.put('smNpwp', smNpwp)

    GlobalVariable.confirmScreens.put('smAkunCode', smAkunCode)

    GlobalVariable.confirmScreens.put('smKodeJenisSetoran', smKodeJenisSetoran)

    if (smTaxPeriod.equals('01012018')) {
        GlobalVariable.confirmScreens.put('smTaxPeriod', 'Monthly')
    } else if (smTaxPeriod.equals('01122018')) {
        GlobalVariable.confirmScreens.put('smTaxPeriod', 'Period')
    } else {
        GlobalVariable.confirmScreens.put('smTaxPeriod', 'Annual')
    }
    
    GlobalVariable.confirmScreens.put('smAmount', smAmount)

    WebUI.delay(4, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.click(findTestObject('menu/MultiBilling/BTN - Continue - DetailRecord'), FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.delay(4, FailureHandling.CONTINUE_ON_FAILURE)
} else {
    WebUI.delay(6, FailureHandling.CONTINUE_ON_FAILURE)
}

WebUI.delay(4, FailureHandling.CONTINUE_ON_FAILURE)

FileTypeCheckerD = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - FileType'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(FileTypeCheckerD, GlobalVariable.confirmScreens.get('FileTypeChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

FileTemplateCheckerD = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - FileTemplate'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(FileTemplateCheckerD, GlobalVariable.confirmScreens.get('FileTemplateChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

TaxBillingTypeCheckerD = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - taxBillingType'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TaxBillingTypeCheckerD, GlobalVariable.confirmScreens.get('TaxBillingTypeChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

FileNameCheckerCon = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - FileName'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(FileNameCheckerCon, GlobalVariable.confirmScreens.get('FileNameChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

FileDescriptionCheckerCon = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - FileDescription'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(FileDescriptionCheckerCon, GlobalVariable.confirmScreens.get('FileDescriptionChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

totalRecordCheckerCon = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - totalRecord'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(totalRecordCheckerCon, GlobalVariable.confirmScreens.get('totalRecordChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

totalTransactionRecordCheckerCon = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - totalTransactionRecord (Generate)'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('totalTransactionRecordCheckerCon', totalTransactionRecordCheckerCon)

WebUI.verifyMatch(totalTransactionRecordCheckerCon, GlobalVariable.confirmScreens.get('totalTransactionRecordCheckerCon'), 
    false, FailureHandling.CONTINUE_ON_FAILURE)

totalAmountInIDRCheckerCon = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - totalAmountInIDR'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('totalAmountInIDRCheckerCon', totalAmountInIDRCheckerCon)

WebUI.verifyMatch(totalAmountInIDRCheckerCon, GlobalVariable.confirmScreens.get('totalAmountInIDRCheckerCon'), false, FailureHandling.CONTINUE_ON_FAILURE)

trfFormCheckerCon = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - trfFrom'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('trfFormCheckerCon', trfFormCheckerCon)

WebUI.verifyMatch(trfFormCheckerCon, GlobalVariable.confirmScreens.get('trfFormCheckerCon'), false, FailureHandling.CONTINUE_ON_FAILURE)

insModeCheckerCon = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - insMode'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('insModeCheckerCon', insModeCheckerCon)

WebUI.verifyMatch(insModeCheckerCon, GlobalVariable.confirmScreens.put('insModeCheckerCon', insModeCheckerCon), false, FailureHandling.CONTINUE_ON_FAILURE)

onCheckerCon = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - on - Copy'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('onCheckerCon', onCheckerCon)

WebUI.verifyMatch(onCheckerCon, GlobalVariable.confirmScreens.get('onCheckerCon'), false, FailureHandling.CONTINUE_ON_FAILURE)

expiredCheckerCon = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - expiredOn'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('expiredCheckerCon', expiredCheckerCon)

WebUI.verifyMatch(expiredCheckerCon, GlobalVariable.confirmScreens.get('expiredCheckerCon'), false, FailureHandling.CONTINUE_ON_FAILURE)

totalTrxRecordCheckerCon = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - totalTransactionRecord (Generate.2)'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('totalTrxRecordCheckerCon', totalTrxRecordCheckerCon)

WebUI.verifyMatch(totalTrxRecordCheckerCon, GlobalVariable.confirmScreens.get('totalTrxRecordCheckerCon'), false, FailureHandling.CONTINUE_ON_FAILURE)

amountCheckerCon = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - totalAmount'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('amountCheckerCon', amountCheckerCon)

WebUI.verifyMatch(amountCheckerCon, GlobalVariable.confirmScreens.get('amountCheckerCon'), false, FailureHandling.CONTINUE_ON_FAILURE)

totalChargesCheckerCon = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - totalCharges'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('totalChargesCheckerCon', totalChargesCheckerCon)

WebUI.verifyMatch(totalChargesCheckerCon, GlobalVariable.confirmScreens.get('totalChargesCheckerCon'), false, FailureHandling.CONTINUE_ON_FAILURE)

totalDebitAmountCheckerCon = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - totalDebitAmount'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('totalDebitAmountCheckerCon', totalDebitAmountCheckerCon)

WebUI.verifyMatch(totalDebitAmountCheckerCon, GlobalVariable.confirmScreens.get('totalDebitAmountCheckerCon'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(6, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/MultiBilling/BTN - SeeMoreRecord'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(4, FailureHandling.CONTINUE_ON_FAILURE)

TaxBillingTypeCheckerDet = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/Label - taxBillingType'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('TaxBillingTypeCheckerDet', TaxBillingTypeCheckerDet)

WebUI.verifyMatch(TaxBillingTypeCheckerDet, GlobalVariable.confirmScreens.get('TaxBillingTypeCheckerDet'), false, FailureHandling.CONTINUE_ON_FAILURE)

trfFormCheckerDet = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/Label - trfFrom'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('trfFormCheckerDet', trfFormCheckerDet)

WebUI.verifyMatch(trfFormCheckerDet, GlobalVariable.confirmScreens.get('trfFormCheckerDet'), false, FailureHandling.CONTINUE_ON_FAILURE)

insDateCheckerDet = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/Label - insDate'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('insDateCheckerDet', insDateCheckerDet)

WebUI.verifyMatch(insDateCheckerDet, GlobalVariable.confirmScreens.get('insDateCheckerDet'), false, FailureHandling.CONTINUE_ON_FAILURE)

totalRecordCheckerDet = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/Label - totalTransactionRecord'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('totalRecordCheckerDet', totalRecordCheckerDet)

WebUI.verifyMatch(totalRecordCheckerDet, GlobalVariable.confirmScreens.get('totalRecordCheckerDet'), false, FailureHandling.CONTINUE_ON_FAILURE)

totalAmountCheckerDet = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/Label - totalAmount'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('totalAmountCheckerDet', totalAmountCheckerDet)

WebUI.verifyMatch(totalAmountCheckerDet, GlobalVariable.confirmScreens.get('totalAmountCheckerDet'), false, FailureHandling.CONTINUE_ON_FAILURE)

paymentFeeCheckerDet = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/Label - serviceFee'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('paymentFeeCheckerDet', paymentFeeCheckerDet)

WebUI.verifyMatch(paymentFeeCheckerDet, GlobalVariable.confirmScreens.get('paymentFeeCheckerDet'), false, FailureHandling.CONTINUE_ON_FAILURE)

totalChargesCheckerDet = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/Label - totalCharges'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('totalChargesCheckerDet', totalChargesCheckerDet)

WebUI.verifyMatch(totalChargesCheckerDet, GlobalVariable.confirmScreens.get('totalChargesCheckerDet'), false, FailureHandling.CONTINUE_ON_FAILURE)

totalDebitAmountCheckerDet = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/Label - totalDebitAmount'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('totalDebitAmountCheckerDet', totalDebitAmountCheckerDet)

WebUI.verifyMatch(totalDebitAmountCheckerDet, GlobalVariable.confirmScreens.get('totalDebitAmountCheckerDet'), false, FailureHandling.CONTINUE_ON_FAILURE)

AllTablesCheckerDet = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/Tbody - AllAccountGetText - DetailTable'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('getTable', AllTablesCheckerDet)

WebUI.verifyMatch(AllTablesCheckerDet, GlobalVariable.confirmScreens.get('getTable'), false, FailureHandling.CONTINUE_ON_FAILURE)

TdNpwp = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/Td - npwp - detailTable(Generate)'), FailureHandling.CONTINUE_ON_FAILURE)

TdAkunCode = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/Td - AkunCode - detailTable(Generate)'), FailureHandling.CONTINUE_ON_FAILURE)

TdKodeJenisSetoran = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/Td - KodeJenisSetoran - detailTable(Generate)'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TdAmount = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/Td - Amount - detailTable(Generate)'), FailureHandling.CONTINUE_ON_FAILURE)

TdTaxPeriod = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/Td - TaxPeriod - detailTable(Generate)'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TdNpwp, GlobalVariable.confirmScreens.get('smNpwp'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TdAkunCode, GlobalVariable.confirmScreens.get('smAkunCode'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TdKodeJenisSetoran, GlobalVariable.confirmScreens.get('smKodeJenisSetoran'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TdAmount, GlobalVariable.confirmScreens.get('smAmount'), false, FailureHandling.CONTINUE_ON_FAILURE)

not_run: WebUI.verifyMatch(TdTaxPeriod, GlobalVariable.confirmScreens.get('smTaxPeriod'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(4, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/MultiBilling/BTN - Close - DetailRecord'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(3, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/BTN - btnSubmit'))

WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/responseCode'), '123456')

WebUI.scrollToElement(findTestObject('menu/trfManagement/bulkTransfer/BTN - btnSubmit'), 0)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/BTN - btnSubmit'))

WebUI.delay(2)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/BTN - submitEnding'))

WebUI.delay(19)

