import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(9, FailureHandling.CONTINUE_ON_FAILURE)

refNumberChecker = WebUI.getText(findTestObject('menu/report/transStatus/checker(multiBilling)/Label - refNumber'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('refNumberChecker', refNumberChecker)

WebUI.verifyMatch(refNumberChecker, GlobalVariable.confirmScreens.get('refNumberChecker'), false)

submitDateChecker = WebUI.getText(findTestObject('menu/report/transStatus/checker(multiBilling)/Label - submitDateTime'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('submitDateChecker', submitDateChecker)

WebUI.verifyMatch(submitDateChecker, GlobalVariable.confirmScreens.get('submitDateChecker'), false)

menuChecker = WebUI.getText(findTestObject('menu/report/transStatus/checker(multiBilling)/Label - Menu'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('menuChecker', menuChecker)

WebUI.verifyMatch(menuChecker, GlobalVariable.confirmScreens.get('menuChecker'), false)

productChecker = WebUI.getText(findTestObject('menu/report/transStatus/checker(multiBilling)/Label - Product'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('productChecker', productChecker)

WebUI.verifyMatch(productChecker, GlobalVariable.confirmScreens.get('productChecker'), false)

fileUploadChecker = WebUI.getText(findTestObject('menu/report/transStatus/checker(multiBilling)/Label - File Upload'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('fileUploadChecker', fileUploadChecker)

WebUI.verifyMatch(fileUploadChecker, GlobalVariable.confirmScreens.get('fileUploadChecker'), false)

fileDescriptionChecker = WebUI.getText(findTestObject('menu/report/transStatus/checker(multiBilling)/Label - File Description'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('fileDescriptionChecker', fileDescriptionChecker)

WebUI.verifyMatch(fileDescriptionChecker, GlobalVariable.confirmScreens.get('fileDescriptionChecker'), false)

trfFromChecker = WebUI.getText(findTestObject('menu/report/transStatus/checker(multiBilling)/Label - Transfer From'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('trfFromChecker', trfFromChecker)

WebUI.verifyMatch(trfFromChecker, GlobalVariable.confirmScreens.get('trfFromChecker'), false)

insModeChecker = WebUI.getText(findTestObject('menu/report/transStatus/checker(multiBilling)/Label - Instruction Mode'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(trfFromChecker, GlobalVariable.confirmScreens.get('trfFromChecker'), false)

expiredChecker = WebUI.getText(findTestObject('menu/report/transStatus/checker(multiBilling)/Label - Expired on'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(expiredChecker, GlobalVariable.confirmScreens.get('expiredChecker'), false)

ttrChecker = WebUI.getText(findTestObject('menu/report/transStatus/checker(multiBilling)/Label - Total Transaction Record'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('ttrChecker', ttrChecker)

WebUI.verifyMatch(ttrChecker, GlobalVariable.confirmScreens.get('ttrChecker'), false)

amountChecker = WebUI.getText(findTestObject('menu/report/transStatus/checker(multiBilling)/Label - Total Amount'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(amountChecker, GlobalVariable.confirmScreens.get('amountChecker'), false)

totalChargesChecker = WebUI.getText(findTestObject('menu/report/transStatus/checker(multiBilling)/Label - Total Charges'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(totalChargesChecker, GlobalVariable.confirmScreens.get('totalChargesChecker'), false)

totalDebitAmountChecker = WebUI.getText(findTestObject('menu/report/transStatus/checker(multiBilling)/Label - Total Debit Amount'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(totalDebitAmountChecker, GlobalVariable.confirmScreens.get('totalDebitAmountChecker'), false)

WebUI.delay(4, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/report/transStatus/checker(multiBilling)/BTN - SeeMoreRecord'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(4, FailureHandling.CONTINUE_ON_FAILURE)

TaxBillingTypeCheckerDet = WebUI.getText(findTestObject('menu/report/transStatus/checker(multiBilling)/detailRecord/Label - taxBillingType'), 
    FailureHandling.CONTINUE_ON_FAILURE)

trfFormCheckerDet = WebUI.getText(findTestObject('menu/report/transStatus/checker(multiBilling)/detailRecord/Label - trfFrom'), 
    FailureHandling.CONTINUE_ON_FAILURE)

insDateCheckerDet = WebUI.getText(findTestObject('menu/report/transStatus/checker(multiBilling)/detailRecord/Label - insDate'), 
    FailureHandling.CONTINUE_ON_FAILURE)

totalRecordCheckerDet = WebUI.getText(findTestObject('menu/report/transStatus/checker(multiBilling)/detailRecord/Label - totalTransactionRecord'), 
    FailureHandling.CONTINUE_ON_FAILURE)

totalAmountCheckerDet = WebUI.getText(findTestObject('menu/report/transStatus/checker(multiBilling)/detailRecord/Label - totalAmount'), 
    FailureHandling.CONTINUE_ON_FAILURE)

paymentFeeCheckerDet = WebUI.getText(findTestObject('menu/report/transStatus/checker(multiBilling)/detailRecord/Label - paymentFee'), 
    FailureHandling.CONTINUE_ON_FAILURE)

totalChargesCheckerDet = WebUI.getText(findTestObject('menu/report/transStatus/checker(multiBilling)/detailRecord/Label - totalCharges'), 
    FailureHandling.CONTINUE_ON_FAILURE)

totalDebitAmountCheckerDet = WebUI.getText(findTestObject('menu/report/transStatus/checker(multiBilling)/detailRecord/Label - totalDebitAmount'), 
    FailureHandling.CONTINUE_ON_FAILURE)

AllTablesCheckerDet = WebUI.getText(findTestObject('menu/report/transStatus/checker(multiBilling)/detailRecord/Tbody - AllAccountGetText - DetailTable'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TaxBillingTypeCheckerDet, GlobalVariable.confirmScreens.get('TaxBillingTypeCheckerDet'), false)

WebUI.verifyMatch(trfFormCheckerDet, GlobalVariable.confirmScreens.get('trfFormCheckerDet'), false)

WebUI.verifyMatch(insDateCheckerDet, GlobalVariable.confirmScreens.get('insDateCheckerDet'), false)

WebUI.verifyMatch(totalRecordCheckerDet, GlobalVariable.confirmScreens.get('totalRecordCheckerDet'), false)

WebUI.verifyMatch(totalAmountCheckerDet, GlobalVariable.confirmScreens.get('totalAmountCheckerDet'), false)

WebUI.verifyMatch(paymentFeeCheckerDet, GlobalVariable.confirmScreens.get('paymentFeeCheckerDet'), false)

WebUI.verifyMatch(totalChargesCheckerDet, GlobalVariable.confirmScreens.get('totalChargesCheckerDet'), false)

WebUI.verifyMatch(totalDebitAmountCheckerDet, GlobalVariable.confirmScreens.get('totalDebitAmountCheckerDet'), false)

WebUI.verifyMatch(AllTablesCheckerDet, GlobalVariable.confirmScreens.get('AllTablesCheckerDet'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(4, FailureHandling.CONTINUE_ON_FAILURE)

not_run: WebUI.waitForElementVisible(findTestObject('menu/MultiBilling/BTN - Close - DetailRecord(2)'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/MultiBilling/BTN - Close - DetailRecord'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(3, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/report/transStatus/transStatus_2/transStatusClick - noChropath'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(3, FailureHandling.CONTINUE_ON_FAILURE)

