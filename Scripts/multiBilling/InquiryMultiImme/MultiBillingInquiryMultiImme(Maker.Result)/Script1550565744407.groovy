import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(10, FailureHandling.CONTINUE_ON_FAILURE)

confirmScreenStatus = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - confirmScreenStatus'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(confirmScreenStatus, confirmScreenStatus, false, FailureHandling.CONTINUE_ON_FAILURE)

refNoChecker = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - refNo'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('refNum', refNoChecker)

WebUI.verifyMatch(refNoChecker, GlobalVariable.confirmScreens.get('refNum'), false, FailureHandling.CONTINUE_ON_FAILURE)

submitDateChecker = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - submitDateTime'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('submitDateChecker', submitDateChecker)

WebUI.verifyMatch(submitDateChecker, GlobalVariable.confirmScreens.get('submitDateChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

FileTypeCheckerCon = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - FileType'), FailureHandling.CONTINUE_ON_FAILURE)

FileTypeCheckerCon = FileTypeCheckerCon.replace(':  ', ': ')

WebUI.verifyMatch(FileTypeCheckerCon, GlobalVariable.confirmScreens.get('FileTypeChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

FileUploadCon = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - FileUpload'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('FileUploadCon', FileUploadCon)

WebUI.verifyMatch(FileUploadCon, GlobalVariable.confirmScreens.get('FileUploadCon'), false, FailureHandling.CONTINUE_ON_FAILURE)

FileDescriptionChecker = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - FileDescription'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(FileDescriptionChecker, GlobalVariable.confirmScreens.get('FileDescriptionChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

trfFormCheckerCon = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - trfFrom'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(trfFormCheckerCon, GlobalVariable.confirmScreens.get('trfFormCheckerCon'), false, FailureHandling.CONTINUE_ON_FAILURE)

insModeCheckerCon = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - insMode'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(insModeCheckerCon, GlobalVariable.confirmScreens.get('insModeCheckerCon'), false, FailureHandling.CONTINUE_ON_FAILURE)

expiredCheckerCon = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - expiredOn'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(expiredCheckerCon, GlobalVariable.confirmScreens.get('expiredCheckerCon'), false, FailureHandling.CONTINUE_ON_FAILURE)

totalTrxRecordCheckerCon = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - totalTransactionRecord'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(totalTrxRecordCheckerCon, GlobalVariable.confirmScreens.get('totalTrxRecordCheckerCon'), false, FailureHandling.CONTINUE_ON_FAILURE)

amountCheckerCon = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - totalAmount'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(amountCheckerCon, GlobalVariable.confirmScreens.get('amountCheckerCon'), false, FailureHandling.CONTINUE_ON_FAILURE)

totalChargesCheckerCon = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - totalCharges'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(totalChargesCheckerCon, GlobalVariable.confirmScreens.get('totalChargesCheckerCon'), false, FailureHandling.CONTINUE_ON_FAILURE)

totalDebitAmountCheckerCon = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - totalDebitAmount'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(totalDebitAmountCheckerCon, GlobalVariable.confirmScreens.get('totalDebitAmountCheckerCon'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(6, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/MultiBilling/BTN - SeeMoreRecord'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(4, FailureHandling.CONTINUE_ON_FAILURE)

TaxBillingTypeCheckerDet = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/Label - taxBillingType'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TaxBillingTypeCheckerDet, GlobalVariable.confirmScreens.get('TaxBillingTypeCheckerDet'), false, FailureHandling.CONTINUE_ON_FAILURE)

trfFormCheckerDet = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/Label - trfFrom'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(trfFormCheckerDet, GlobalVariable.confirmScreens.get('trfFormCheckerDet'), false, FailureHandling.CONTINUE_ON_FAILURE)

insDateCheckerDet = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/Label - insDate'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(insDateCheckerDet, GlobalVariable.confirmScreens.get('insDateCheckerDet'), false, FailureHandling.CONTINUE_ON_FAILURE)

totalRecordCheckerDet = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/Label - totalTransactionRecord'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(totalRecordCheckerDet, GlobalVariable.confirmScreens.get('totalRecordCheckerDet'), false, FailureHandling.CONTINUE_ON_FAILURE)

totalAmountCheckerDet = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/Label - totalAmount'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(totalAmountCheckerDet, GlobalVariable.confirmScreens.get('totalAmountCheckerDet'), false, FailureHandling.CONTINUE_ON_FAILURE)

paymentFeeCheckerDet = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/Label - paymentFee'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(paymentFeeCheckerDet, GlobalVariable.confirmScreens.get('paymentFeeCheckerDet'), false, FailureHandling.CONTINUE_ON_FAILURE)

totalChargesCheckerDet = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/Label - totalCharges'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(totalChargesCheckerDet, GlobalVariable.confirmScreens.get('totalChargesCheckerDet'), false, FailureHandling.CONTINUE_ON_FAILURE)

totalDebitAmountCheckerDet = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/Label - totalDebitAmount'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(totalDebitAmountCheckerDet, GlobalVariable.confirmScreens.get('totalDebitAmountCheckerDet'), false, FailureHandling.CONTINUE_ON_FAILURE)

AllTablesCheckerDet = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/Tbody - AllAccountGetText - DetailTable2'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(AllTablesCheckerDet, GlobalVariable.confirmScreens.get('AllTablesCheckerDet'), false, FailureHandling.CONTINUE_ON_FAILURE)

TdBillingId = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/Td - BillingId - detailTable - Copy'), FailureHandling.CONTINUE_ON_FAILURE)

TdNpwp = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/Td - npwp - detailTable - Copy'), FailureHandling.CONTINUE_ON_FAILURE)

TdAkunCode = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/Td - AkunCode - detailTable - Copy'), FailureHandling.CONTINUE_ON_FAILURE)

TdKodeJenisSetoran = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/Td - KodeJenisSetoran - detailTable - Copy'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TdAmount = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/Td - Amount - detailTable - Copy'), FailureHandling.CONTINUE_ON_FAILURE)

TdTaxPeriod = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/Td - TaxPeriod - detailTable - Copy'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TdBillingId, GlobalVariable.confirmScreens.get('smBillingId'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TdNpwp, GlobalVariable.confirmScreens.get('smNpwp'), false, FailureHandling.CONTINUE_ON_FAILURE)

not_run: WebUI.verifyMatch(TdAkunCode, GlobalVariable.confirmScreens.get('smAkunCode'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TdKodeJenisSetoran, GlobalVariable.confirmScreens.get('smKodeJenisSetoran'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TdAmount, GlobalVariable.confirmScreens.get('smAmount'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TdTaxPeriod, GlobalVariable.confirmScreens.get('smTaxPeriod'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(4, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/MultiBilling/BTN - Close - DetailRecord(2)'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(3, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/BTN - logoutClick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(20, FailureHandling.CONTINUE_ON_FAILURE)

