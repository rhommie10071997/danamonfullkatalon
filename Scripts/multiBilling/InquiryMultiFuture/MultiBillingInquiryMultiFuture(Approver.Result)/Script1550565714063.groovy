import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(15, FailureHandling.CONTINUE_ON_FAILURE)

Menu = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(MultiBilling)/Label - Menu'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(Menu, GlobalVariable.confirmScreens.get('Menu'), false)

Product = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(MultiBilling)/Label - Product'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(Product, GlobalVariable.confirmScreens.get('Product'), false)

TransactionReferenceNo = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(MultiBilling)/Label - transRefNumber'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TransactionReferenceNo, GlobalVariable.confirmScreens.get('TransactionReferenceNo'), false)

DocumentNo = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(MultiBilling)/Label - documentCode'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(DocumentNo, GlobalVariable.confirmScreens.get('DocumentNo'), false)

submitDate = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(MultiBilling)/Label - submitDate'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(submitDate, GlobalVariable.confirmScreens.get('submitDate'), false)

FileTypeChecker = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(MultiBilling)/Label - fileType'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(FileTypeChecker, GlobalVariable.confirmScreens.get('FileTypeChecker'), false)

FileUpload = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(MultiBilling)/Label - fileUpload'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(FileUpload, GlobalVariable.confirmScreens.get('FileUpload'), false)

FileDescriptionChecker = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(MultiBilling)/Label - fileDescrtiption'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(FileDescriptionChecker, GlobalVariable.confirmScreens.get('FileDescriptionChecker'), false)

trfFormChecker = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(MultiBilling)/Label - trfFrom'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(trfFormChecker, GlobalVariable.confirmScreens.get('trfFormChecker'), false)

insModeChecker = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(MultiBilling)/Label - insMode'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(insModeChecker, GlobalVariable.confirmScreens.get('insModeChecker'), false)

onChecker = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(MultiBilling)/Label - on'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(onChecker, GlobalVariable.confirmScreens.get('onChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

expiredChecker = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(MultiBilling)/Label - expiredOn'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(expiredChecker, GlobalVariable.confirmScreens.get('expiredChecker'), false)

totalTrxRecordChecker = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(MultiBilling)/Label - totalTransactionRecord'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(totalTrxRecordChecker, GlobalVariable.confirmScreens.get('totalTrxRecordChecker'), false)

amountChecker = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(MultiBilling)/Label - totalAmount'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(amountChecker, GlobalVariable.confirmScreens.get('amountChecker'), false)

totalChargesChecker = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(MultiBilling)/Label - totalCharges'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(totalChargesChecker, GlobalVariable.confirmScreens.get('totalChargesChecker'), false)

totalDebitAmountChecker = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(MultiBilling)/Label - totalDebitAmount'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(totalDebitAmountChecker, GlobalVariable.confirmScreens.get('totalDebitAmountChecker'), false)

WebUI.delay(6, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/MultiBilling/BTN - SeeMoreRecord'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(4, FailureHandling.CONTINUE_ON_FAILURE)

TaxBillingTypeCheckerDet = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(MultiBilling)/detailRecord/Label - taxBillingType'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TaxBillingTypeCheckerDet, GlobalVariable.confirmScreens.get('TaxBillingTypeCheckerDet'), false)

trfFormCheckerDet = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(MultiBilling)/detailRecord/Label - trfFrom'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(trfFormCheckerDet, GlobalVariable.confirmScreens.get('trfFormCheckerDet'), false)

insDateCheckerDet = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(MultiBilling)/detailRecord/Label - insDate'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(insDateCheckerDet, GlobalVariable.confirmScreens.get('insDateCheckerDet'), false)

totalRecordCheckerDet = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(MultiBilling)/detailRecord/Label - totalTransactionRecord'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(totalRecordCheckerDet, GlobalVariable.confirmScreens.get('totalRecordCheckerDet'), false)

totalAmountCheckerDet = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(MultiBilling)/detailRecord/Label - totalAmount'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(totalAmountCheckerDet, GlobalVariable.confirmScreens.get('totalAmountCheckerDet'), false)

paymentFeeCheckerDet = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(MultiBilling)/detailRecord/Label - paymentFee'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(paymentFeeCheckerDet, GlobalVariable.confirmScreens.get('paymentFeeCheckerDet'), false)

totalChargesCheckerDet = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(MultiBilling)/detailRecord/Label - totalCharges'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(totalChargesCheckerDet, GlobalVariable.confirmScreens.get('totalChargesCheckerDet'), false)

totalDebitAmountCheckerDet = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(MultiBilling)/detailRecord/Label - totalDebitAmount'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(totalDebitAmountCheckerDet, GlobalVariable.confirmScreens.get('totalDebitAmountCheckerDet'), false)

AllTablesCheckerDet = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(MultiBilling)/detailRecord/Tbody - AllAccountGetText - DetailTable'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(AllTablesCheckerDet, GlobalVariable.confirmScreens.get('AllTablesCheckerDet'), false)

WebUI.delay(4, FailureHandling.CONTINUE_ON_FAILURE)

not_run: WebUI.waitForElementVisible(findTestObject('menu/MultiBilling/BTN - Close - DetailRecord(2)'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/MultiBilling/BTN - Close - DetailRecord'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(3, FailureHandling.CONTINUE_ON_FAILURE)

