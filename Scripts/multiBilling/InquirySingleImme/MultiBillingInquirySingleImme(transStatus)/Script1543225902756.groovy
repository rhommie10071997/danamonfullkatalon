import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(9, FailureHandling.CONTINUE_ON_FAILURE)

refNumberChecker = WebUI.getText(findTestObject('menu/report/transStatus/checker(multiBilling)/Label - refNumber'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('refNumberChecker', refNumberChecker)

WebUI.verifyMatch(refNumberChecker, GlobalVariable.confirmScreens.get('refNumberChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

submitDateChecker = WebUI.getText(findTestObject('menu/report/transStatus/checker(multiBilling)/Label - submitDateTime'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('submitDateChecker', submitDateChecker)

WebUI.verifyMatch(submitDateChecker, GlobalVariable.confirmScreens.get('submitDateChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

menuChecker = WebUI.getText(findTestObject('menu/report/transStatus/checker(multiBilling)/Label - Menu'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('menuChecker', menuChecker)

WebUI.verifyMatch(menuChecker, GlobalVariable.confirmScreens.get('menuChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

productChecker = WebUI.getText(findTestObject('menu/report/transStatus/checker(multiBilling)/Label - Product'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('productChecker', productChecker)

WebUI.verifyMatch(productChecker, GlobalVariable.confirmScreens.get('productChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

fileUploadChecker = WebUI.getText(findTestObject('menu/report/transStatus/checker(multiBilling)/Label - File Upload'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('fileUploadChecker', fileUploadChecker)

WebUI.verifyMatch(fileUploadChecker, GlobalVariable.confirmScreens.get('fileUploadChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

fileDescriptionChecker = WebUI.getText(findTestObject('menu/report/transStatus/checker(multiBilling)/Label - File Description'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('fileDescriptionChecker', fileDescriptionChecker)

WebUI.verifyMatch(fileDescriptionChecker, GlobalVariable.confirmScreens.get('fileDescriptionChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

trfFromChecker = WebUI.getText(findTestObject('menu/report/transStatus/checker(multiBilling)/Label - Transfer From'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('trfFromChecker', trfFromChecker)

WebUI.verifyMatch(trfFromChecker, GlobalVariable.confirmScreens.get('trfFromChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

insModeChecker = WebUI.getText(findTestObject('menu/report/transStatus/checker(multiBilling)/Label - Instruction Mode'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('insModeChecker', insModeChecker)

WebUI.verifyMatch(insModeChecker, GlobalVariable.confirmScreens.get('insModeChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

expiredChecker = WebUI.getText(findTestObject('menu/report/transStatus/checker(multiBilling)/Label - Expired on'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('expiredChecker', expiredChecker)

WebUI.verifyMatch(expiredChecker, GlobalVariable.confirmScreens.get('expiredChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

ttrChecker = WebUI.getText(findTestObject('menu/report/transStatus/checker(multiBilling)/Label - Total Transaction Record'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('ttrChecker', ttrChecker)

WebUI.verifyMatch(ttrChecker, GlobalVariable.confirmScreens.get('ttrChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

amountChecker = WebUI.getText(findTestObject('menu/report/transStatus/checker(multiBilling)/Label - Total Amount'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('amountChecker', amountChecker)

WebUI.verifyMatch(amountChecker, GlobalVariable.confirmScreens.get('amountChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

totalChargesChecker = WebUI.getText(findTestObject('menu/report/transStatus/checker(multiBilling)/Label - Total Charges'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('totalChargesChecker', totalChargesChecker)

WebUI.verifyMatch(totalChargesChecker, GlobalVariable.confirmScreens.get('totalChargesChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

totalDebitAmountChecker = WebUI.getText(findTestObject('menu/report/transStatus/checker(multiBilling)/Label - Total Debit Amount'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('totalDebitAmountChecker', totalDebitAmountChecker)

WebUI.verifyMatch(totalDebitAmountChecker, GlobalVariable.confirmScreens.get('totalDebitAmountChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(4, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/report/transStatus/checker(multiBilling)/BTN - SeeMoreRecord'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(4, FailureHandling.CONTINUE_ON_FAILURE)

TaxBillingTypeCheckerDet = WebUI.getText(findTestObject('menu/report/transStatus/checker(multiBilling)/detailRecord/Label - taxBillingType'), 
    FailureHandling.CONTINUE_ON_FAILURE)

trfFormCheckerDet = WebUI.getText(findTestObject('menu/report/transStatus/checker(multiBilling)/detailRecord/Label - trfFrom'), 
    FailureHandling.CONTINUE_ON_FAILURE)

insDateCheckerDet = WebUI.getText(findTestObject('menu/report/transStatus/checker(multiBilling)/detailRecord/Label - insDate'), 
    FailureHandling.CONTINUE_ON_FAILURE)

totalRecordCheckerDet = WebUI.getText(findTestObject('menu/report/transStatus/checker(multiBilling)/detailRecord/Label - totalTransactionRecord'), 
    FailureHandling.CONTINUE_ON_FAILURE)

totalAmountCheckerDet = WebUI.getText(findTestObject('menu/report/transStatus/checker(multiBilling)/detailRecord/Label - totalAmount'), 
    FailureHandling.CONTINUE_ON_FAILURE)

paymentFeeCheckerDet = WebUI.getText(findTestObject('menu/report/transStatus/checker(multiBilling)/detailRecord/Label - paymentFee'), 
    FailureHandling.CONTINUE_ON_FAILURE)

totalChargesCheckerDet = WebUI.getText(findTestObject('menu/report/transStatus/checker(multiBilling)/detailRecord/Label - totalCharges'), 
    FailureHandling.CONTINUE_ON_FAILURE)

totalDebitAmountCheckerDet = WebUI.getText(findTestObject('menu/report/transStatus/checker(multiBilling)/detailRecord/Label - totalDebitAmount'), 
    FailureHandling.CONTINUE_ON_FAILURE)

AllTablesCheckerDet = WebUI.getText(findTestObject('menu/report/transStatus/checker(multiBilling)/detailRecord/Tbody - AllAccountGetText - DetailTable'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TaxBillingTypeCheckerDet, GlobalVariable.confirmScreens.get('TaxBillingTypeCheckerDet'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(trfFormCheckerDet, GlobalVariable.confirmScreens.get('trfFormCheckerDet'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(insDateCheckerDet, GlobalVariable.confirmScreens.get('insDateCheckerDet'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(totalRecordCheckerDet, GlobalVariable.confirmScreens.get('totalRecordCheckerDet'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(totalAmountCheckerDet, GlobalVariable.confirmScreens.get('totalAmountCheckerDet'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(paymentFeeCheckerDet, GlobalVariable.confirmScreens.get('paymentFeeCheckerDet'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(totalChargesCheckerDet, GlobalVariable.confirmScreens.get('totalChargesCheckerDet'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(totalDebitAmountCheckerDet, GlobalVariable.confirmScreens.get('totalDebitAmountCheckerDet'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(AllTablesCheckerDet, GlobalVariable.confirmScreens.get('AllTablesCheckerDet'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(4, FailureHandling.CONTINUE_ON_FAILURE)

not_run: WebUI.waitForElementVisible(findTestObject('menu/MultiBilling/BTN - Close - DetailRecord(2)'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/MultiBilling/BTN - Close - DetailRecord'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(3, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/report/transStatus/transStatus_2/transStatusClick - noChropath'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(3, FailureHandling.CONTINUE_ON_FAILURE)

