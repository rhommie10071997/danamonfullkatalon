import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

String similarFlag = 'On'

String FileDescription = 'MultiBillingInquiryMultiBray'

//Directory
String Directory = 'C:\\Users\\user\\Desktop\\Csv\\multi billing\\Multi Billing Inquiry DJP 001 - Future.csv'

WebUI.setText(findTestObject('homelogin/homelogin.corporateid'), 'PCMKILLUA', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('homelogin/homelogin.userid'), 'killua', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('homelogin/homelogin.password'), 'Password123', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('homelogin/homelogin.submit'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(10, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/BTN - clickMenu'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/MultiBilling/BTN - TaxPaymentClick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.scrollToElement(findTestObject('menu/MultiBilling/BTN - MultiBillingClick'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/MultiBilling/BTN - MultiBillingClick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/MultiBilling/DL - FileTemplateClick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/MultiBilling/BTN - Choices(1)'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('menu/MultiBilling/Value - ChooseFile'), Directory, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('menu/MultiBilling/Value - FileDescription'), FileDescription, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('menu/MultiBilling/CheckBox - Terms'), 0, FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/MultiBilling/CheckBox - Terms'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/MultiBilling/BTN - Continue'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(20, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/MultiBilling/BTN - BucketClick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/MultiBilling/BTN - TableClick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

FileUploadStatusChecker = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - FileUploadStatus'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('FileUploadStatusChecker', FileUploadStatusChecker)

WebUI.verifyMatch(FileUploadStatusChecker, GlobalVariable.confirmScreens.get('FileUploadStatusChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

FileTypeChecker = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - FileType'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('FileTypeChecker', FileTypeChecker)

WebUI.verifyMatch(FileTypeChecker, GlobalVariable.confirmScreens.get('FileTypeChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

FileTemplateChecker = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - FileTemplate'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('FileTemplateChecker', FileTemplateChecker)

WebUI.verifyMatch(FileTemplateChecker, GlobalVariable.confirmScreens.get('FileTemplateChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

TaxBillingTypeChecker = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - taxBillingType'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('TaxBillingTypeChecker', TaxBillingTypeChecker)

WebUI.verifyMatch(TaxBillingTypeChecker, GlobalVariable.confirmScreens.get('TaxBillingTypeChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

FileNameChecker = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - FileName'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('FileNameChecker', FileNameChecker)

WebUI.verifyMatch(FileNameChecker, GlobalVariable.confirmScreens.get('FileNameChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

FileDescriptionChecker = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - FileDescription'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('FileDescriptionChecker', FileDescriptionChecker)

WebUI.verifyMatch(FileDescriptionChecker, GlobalVariable.confirmScreens.get('FileDescriptionChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

totalRecordChecker = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - totalRecord'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('totalRecordChecker', totalRecordChecker)

WebUI.verifyMatch(totalRecordChecker, GlobalVariable.confirmScreens.get('totalRecordChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

TotalSuccessChecker = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - totalSuccess'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('TotalSuccessChecker', TotalSuccessChecker)

WebUI.verifyMatch(TotalSuccessChecker, GlobalVariable.confirmScreens.get('TotalSuccessChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

TotalFailedChecker = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - totalFailed'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('TotalFailedChecker', TotalFailedChecker)

WebUI.verifyMatch(TotalFailedChecker, GlobalVariable.confirmScreens.get('TotalFailedChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

TotalAmountChecker = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - totalAmount'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('TotalAmountChecker', TotalAmountChecker)

WebUI.verifyMatch(TotalAmountChecker, GlobalVariable.confirmScreens.get('TotalAmountChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/MultiBilling/BTN - Confirm'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(12, FailureHandling.CONTINUE_ON_FAILURE)

if (similarFlag.equals('On')) {
    WebUI.delay(6, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.scrollToElement(findTestObject('menu/MultiBilling/DetailRecord/Checkbox - ClickCheckboxTable'), 0, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.click(findTestObject('menu/MultiBilling/DetailRecord/Checkbox - ClickCheckboxTable'), FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    smBillingId = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/SimilarTransaction/Td - BillingId'), FailureHandling.CONTINUE_ON_FAILURE)

    GlobalVariable.confirmScreens.put('smBillingId', smBillingId)
	
	String smBillingId = GlobalVariable.confirmScreens.get('smBillingId')+''
	
	char smBillingIdChar = smBillingId.charAt(0)
	
	if(smBillingIdChar=="1" || smBillingIdChar=="2" || smBillingIdChar=="3"){
		GlobalVariable.confirmScreens.put('smBillingType','DJP')
		GlobalVariable.confirmScreens.put('smBillingIdFlag','0')
	}else if(smBillingIdChar=="4" || smBillingIdChar=="5" || smBillingIdChar=="6"){
		GlobalVariable.confirmScreens.put('smBillingType','DJBC')
		GlobalVariable.confirmScreens.put('smBillingIdFlag','1')
	}else{
		GlobalVariable.confirmScreens.put('smBillingType','DJA')
		GlobalVariable.confirmScreens.put('smBillingIdFlag','2')
	}
	
	if(GlobalVariable.confirmScreens.get('smBillingIdFlag').equals('0')){
	
    WebUI.verifyMatch(smBillingId, GlobalVariable.confirmScreens.get('smBillingId'), false)

    smTaxPayerName = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/SimilarTransaction/Td - TaxPayerName'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    GlobalVariable.confirmScreens.put('smTaxPayerName', smTaxPayerName)

    WebUI.verifyMatch(smTaxPayerName, GlobalVariable.confirmScreens.get('smTaxPayerName'), false)

    smTaxPayerAddress = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/SimilarTransaction/Td - TaxPayerAddress'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    GlobalVariable.confirmScreens.put('smTaxPayerAddress', smTaxPayerAddress)

    WebUI.verifyMatch(smTaxPayerAddress, GlobalVariable.confirmScreens.get('smTaxPayerAddress'), false)

    smAkunCode = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/SimilarTransaction/Td - AkunCode'), FailureHandling.CONTINUE_ON_FAILURE)

    GlobalVariable.confirmScreens.put('smAkunCode', smAkunCode)

    WebUI.verifyMatch(smAkunCode, GlobalVariable.confirmScreens.get('smAkunCode'), false)

    smKodeJenisSetoran = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/SimilarTransaction/Td - KodeJenisSetoran'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    GlobalVariable.confirmScreens.put('smKodeJenisSetoran', smKodeJenisSetoran)

    WebUI.verifyMatch(smKodeJenisSetoran, GlobalVariable.confirmScreens.get('smKodeJenisSetoran'), false)

    smTaxPeriod = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/SimilarTransaction/Td - TaxPeriod'), FailureHandling.CONTINUE_ON_FAILURE)

    GlobalVariable.confirmScreens.put('smTaxPeriod', smTaxPeriod)

    WebUI.verifyMatch(smTaxPeriod, GlobalVariable.confirmScreens.get('smTaxPeriod'), false)

 	smTaxPeriod_1 = ''+smTaxPeriod.substring(0, 2)
	smTaxPeriod_2 = ''+smTaxPeriod.substring(2, 4)
		
	smTaxPeriod_3 = ''+smTaxPeriod_1+smTaxPeriod_2
		
       if (smTaxPeriod_1.equals(smTaxPeriod_2)) {
           GlobalVariable.confirmScreens.put('smTaxPeriod', 'Monthly')
        } else if (smTaxPeriod_3.equals('0112')) {
            GlobalVariable.confirmScreens.put('smTaxPeriod', 'Annual')
        } else {
            GlobalVariable.confirmScreens.put('smTaxPeriod', 'Period')
        }
    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.click(findTestObject('menu/MultiBilling/DetailRecord/SimilarTransaction/Td - SimilarForClicking'), FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)
	
    smNpwp = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/SimilarTransaction/Td - Npwp(DetailRecord2)'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    GlobalVariable.confirmScreens.put('smNpwp', smNpwp)

    WebUI.verifyMatch(smNpwp, GlobalVariable.confirmScreens.get('smNpwp'), false)
	
	WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)
	
	WebUI.click(findTestObject('menu/MultiBilling/BTN - Close - DetailRecord'), FailureHandling.CONTINUE_ON_FAILURE)
	
	WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)
	
	}else{
		
		GlobalVariable.confirmScreens.put('smNpwp','')
		
		GlobalVariable.confirmScreens.put('smTaxPeriod', '')
		
		GlobalVariable.confirmScreens.put('smKodeJenisSetoran','')
		
		GlobalVariable.confirmScreens.put('smAkunCode','')
	}
    
	
	smAmount = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/SimilarTransaction/Td - Amount'), FailureHandling.CONTINUE_ON_FAILURE)
	
	GlobalVariable.confirmScreens.put('smAmount', smAmount)
	
	WebUI.verifyMatch(smAmount, GlobalVariable.confirmScreens.get('smAmount'), false)
	
	
   

    WebUI.click(findTestObject('menu/MultiBilling/BTN - Continue - DetailRecord'), FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.delay(4, FailureHandling.CONTINUE_ON_FAILURE)
} else {
    WebUI.delay(6, FailureHandling.CONTINUE_ON_FAILURE)
}

WebUI.delay(6, FailureHandling.CONTINUE_ON_FAILURE)

TaxBillingTypeCheckerCon = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - taxBillingType'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('TaxBillingTypeCheckerCon', TaxBillingTypeCheckerCon)

WebUI.verifyMatch(TaxBillingTypeCheckerCon, GlobalVariable.confirmScreens.get('TaxBillingTypeCheckerCon'), false, FailureHandling.CONTINUE_ON_FAILURE)

FileNameCheckerCon = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - FileName'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('FileNameCheckerCon', FileNameCheckerCon)

WebUI.verifyMatch(FileNameCheckerCon, GlobalVariable.confirmScreens.get('FileNameCheckerCon'), false, FailureHandling.CONTINUE_ON_FAILURE)

FileDescriptionCheckerCon = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - FileDescription'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('FileDescriptionCheckerCon', FileDescriptionCheckerCon)

WebUI.verifyMatch(FileDescriptionCheckerCon, GlobalVariable.confirmScreens.get('FileDescriptionCheckerCon'), false, FailureHandling.CONTINUE_ON_FAILURE)

totalRecordCheckerCon = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - totalRecord'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('totalRecordCheckerCon', totalRecordCheckerCon)

WebUI.verifyMatch(totalRecordCheckerCon, GlobalVariable.confirmScreens.get('totalRecordCheckerCon'), false, FailureHandling.CONTINUE_ON_FAILURE)

totalTransactionAmountCheckerCon = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - totalTransactionAmount'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('totalTransactionAmountCheckerCon', totalTransactionAmountCheckerCon)

WebUI.verifyMatch(totalTransactionAmountCheckerCon, GlobalVariable.confirmScreens.get('totalTransactionAmountCheckerCon'), 
    false, FailureHandling.CONTINUE_ON_FAILURE)

trfFormCheckerCon = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - trfFrom'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('trfFormCheckerCon', trfFormCheckerCon)

WebUI.verifyMatch(trfFormCheckerCon, GlobalVariable.confirmScreens.get('trfFormCheckerCon'), false, FailureHandling.CONTINUE_ON_FAILURE)

insModeCheckerCon = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - insMode'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('insModeCheckerCon', insModeCheckerCon)

WebUI.verifyMatch(insModeCheckerCon, GlobalVariable.confirmScreens.get('insModeCheckerCon'), false, FailureHandling.CONTINUE_ON_FAILURE)

expiredCheckerCon = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - expiredOn'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('expiredCheckerCon', expiredCheckerCon)

WebUI.verifyMatch(expiredCheckerCon, GlobalVariable.confirmScreens.get('expiredCheckerCon'), false, FailureHandling.CONTINUE_ON_FAILURE)

totalTrxRecordCheckerCon = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - totalTransactionRecord'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('totalTrxRecordCheckerCon', totalTrxRecordCheckerCon)

WebUI.verifyMatch(totalTrxRecordCheckerCon, GlobalVariable.confirmScreens.get('totalTrxRecordCheckerCon'), false, FailureHandling.CONTINUE_ON_FAILURE)

onCheckerCon = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - on'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('onCheckerCon', onCheckerCon)

WebUI.verifyMatch(onCheckerCon, GlobalVariable.confirmScreens.get('onCheckerCon'), false, FailureHandling.CONTINUE_ON_FAILURE)

amountCheckerCon = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - totalAmount'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('amountCheckerCon', amountCheckerCon)

WebUI.verifyMatch(amountCheckerCon, GlobalVariable.confirmScreens.get('amountCheckerCon'), false, FailureHandling.CONTINUE_ON_FAILURE)

totalChargesCheckerCon = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - totalCharges'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('totalChargesCheckerCon', totalChargesCheckerCon)

WebUI.verifyMatch(totalChargesCheckerCon, GlobalVariable.confirmScreens.get('totalChargesCheckerCon'), false, FailureHandling.CONTINUE_ON_FAILURE)

totalDebitAmountCheckerCon = WebUI.getText(findTestObject('menu/MultiBilling/Bucket/Label - totalDebitAmount'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('totalDebitAmountCheckerCon', totalDebitAmountCheckerCon)

WebUI.verifyMatch(totalDebitAmountCheckerCon, GlobalVariable.confirmScreens.get('totalDebitAmountCheckerCon'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(6, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/MultiBilling/BTN - SeeMoreRecord'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(4, FailureHandling.CONTINUE_ON_FAILURE)

TaxBillingTypeCheckerDet = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/Label - taxBillingType'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('TaxBillingTypeCheckerDet', TaxBillingTypeCheckerDet)

WebUI.verifyMatch(TaxBillingTypeCheckerDet, GlobalVariable.confirmScreens.get('TaxBillingTypeCheckerDet'), false, FailureHandling.CONTINUE_ON_FAILURE)

trfFormCheckerDet = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/Label - trfFrom'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('trfFormCheckerDet', trfFormCheckerDet)

WebUI.verifyMatch(trfFormCheckerDet, GlobalVariable.confirmScreens.get('trfFormCheckerDet'), false, FailureHandling.CONTINUE_ON_FAILURE)

insDateCheckerDet = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/Label - insDate'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('insDateCheckerDet', insDateCheckerDet)

WebUI.verifyMatch(insDateCheckerDet, GlobalVariable.confirmScreens.get('insDateCheckerDet'), false, FailureHandling.CONTINUE_ON_FAILURE)

totalRecordCheckerDet = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/Label - totalTransactionRecord'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('totalRecordCheckerDet', totalRecordCheckerDet)

WebUI.verifyMatch(totalRecordCheckerDet, GlobalVariable.confirmScreens.get('totalRecordCheckerDet'), false, FailureHandling.CONTINUE_ON_FAILURE)

totalAmountCheckerDet = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/Label - totalAmount'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('totalAmountCheckerDet', totalAmountCheckerDet)

WebUI.verifyMatch(totalAmountCheckerDet, GlobalVariable.confirmScreens.get('totalAmountCheckerDet'), false, FailureHandling.CONTINUE_ON_FAILURE)

paymentFeeCheckerDet = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/Label - paymentFee'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('paymentFeeCheckerDet', paymentFeeCheckerDet)

WebUI.verifyMatch(paymentFeeCheckerDet, GlobalVariable.confirmScreens.get('paymentFeeCheckerDet'), false, FailureHandling.CONTINUE_ON_FAILURE)

totalChargesCheckerDet = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/Label - totalCharges'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('totalChargesCheckerDet', totalChargesCheckerDet)

WebUI.verifyMatch(totalChargesCheckerDet, GlobalVariable.confirmScreens.get('totalChargesCheckerDet'), false, FailureHandling.CONTINUE_ON_FAILURE)

totalDebitAmountCheckerDet = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/Label - totalDebitAmount'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('totalDebitAmountCheckerDet', totalDebitAmountCheckerDet)

WebUI.verifyMatch(totalDebitAmountCheckerDet, GlobalVariable.confirmScreens.get('totalDebitAmountCheckerDet'), false, FailureHandling.CONTINUE_ON_FAILURE)

AllTablesCheckerDet = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/Tbody - AllAccountGetText - DetailTable'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('AllTablesCheckerDet', AllTablesCheckerDet)

WebUI.verifyMatch(AllTablesCheckerDet, GlobalVariable.confirmScreens.get('AllTablesCheckerDet'), false, FailureHandling.CONTINUE_ON_FAILURE)

TdBillingId = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/Td - BillingId - detailTable'), FailureHandling.CONTINUE_ON_FAILURE)

TdNpwp = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/Td - npwp - detailTable'), FailureHandling.CONTINUE_ON_FAILURE)

TdAkunCode = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/Td - AkunCode - detailTable'), FailureHandling.CONTINUE_ON_FAILURE)

TdKodeJenisSetoran = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/Td - KodeJenisSetoran - detailTable'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TdAmount = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/Td - Amount - detailTable'), FailureHandling.CONTINUE_ON_FAILURE)

TdTaxPeriod = WebUI.getText(findTestObject('menu/MultiBilling/DetailRecord/Td - TaxPeriod - detailTable'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TdBillingId, GlobalVariable.confirmScreens.get('smBillingId'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TdNpwp, GlobalVariable.confirmScreens.get('smNpwp'), false, FailureHandling.CONTINUE_ON_FAILURE)

not_run: WebUI.verifyMatch(TdAkunCode, GlobalVariable.confirmScreens.get('smAkunCode'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TdKodeJenisSetoran, GlobalVariable.confirmScreens.get('smKodeJenisSetoran'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TdAmount, GlobalVariable.confirmScreens.get('smAmount'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TdTaxPeriod, GlobalVariable.confirmScreens.get('smTaxPeriod'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(4, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/MultiBilling/BTN - Close - DetailRecord'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(3, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/BTN - btnSubmit'))

WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/responseCode'), '123456')

WebUI.scrollToElement(findTestObject('menu/trfManagement/bulkTransfer/BTN - btnSubmit'), 0)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/BTN - btnSubmit'))

WebUI.delay(2)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/BTN - submitEnding'))

WebUI.delay(19)

