import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.interactions.Actions as Actions
import java.util.Random as Random

Vector<String> subAccounts = new Vector<String>()

Random rand = new Random()


String subAccountDescription = 'testSubAccount'

String mainAccountDescription = 'testMainAccount'

subAccounts.add(GlobalVariable.confirmScreens.get('SubAccount'))

subAccounts.add(GlobalVariable.confirmScreens.get('SubAccount_2'))

int SubSweepPriority

int count = 0

int amountRandom

int min

if (GlobalVariable.confirmScreens.get('jumlahSub') == 1) {
    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.click(findTestObject('menu/LiquidityManagement/RangeBalance/BTN - plusClick'))

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.click(findTestObject('menu/LiquidityManagement/RangeBalance/TablesSubAccount/DL - subAccountClick'))

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.setText(findTestObject('menu/LiquidityManagement/RangeBalance/Textfield - textfieldMainAccountClick'), subAccounts.get(
            count))

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.click(findTestObject('menu/LiquidityManagement/RangeBalance/BTN - choices'), FailureHandling.STOP_ON_FAILURE)

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)
	
	SubSweepPriority = rand.nextInt(100) + 1

    WebUI.setText(findTestObject('menu/LiquidityManagement/RangeBalance/TablesSubAccount/Textfield - sweepPriorityTextfield'), 
        ''+SubSweepPriority)

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    amountRandom = (rand.nextInt(899999) + 100001)

    min = Integer.parseInt(GlobalVariable.confirmScreens.get('availableBalance1')) + amountRandom

    WebUI.setText(findTestObject('menu/LiquidityManagement/RangeBalance/TablesSubAccount/Textfield - minBalanceTextfield'), 
        ''+min)

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    min = (min * 2)

    WebUI.setText(findTestObject('menu/LiquidityManagement/RangeBalance/TablesSubAccount/Textfield - maxBalanceTextfield'), 
        ''+min)

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.setText(findTestObject('menu/LiquidityManagement/RangeBalance/TablesSubAccount/Textfield - subAccDescTextfield'), 
        subAccountDescription + amountRandom)

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.setText(findTestObject('menu/LiquidityManagement/RangeBalance/TablesSubAccount/Textfield - mainAccDescTextfield'), 
        mainAccountDescription + amountRandom)

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    count++
} else {
    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.click(findTestObject('menu/LiquidityManagement/RangeBalance/BTN - plusClick'))

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.click(findTestObject('menu/LiquidityManagement/RangeBalance/TablesSubAccount/DL - subAccountClick'))

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.setText(findTestObject('menu/LiquidityManagement/RangeBalance/Textfield - textfieldMainAccountClick'), subAccounts.get(
            count))

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.click(findTestObject('menu/LiquidityManagement/RangeBalance/BTN - choices'), FailureHandling.STOP_ON_FAILURE)

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

	SubSweepPriority = rand.nextInt(100) + 1
	
    WebUI.setText(findTestObject('menu/LiquidityManagement/RangeBalance/TablesSubAccount/Textfield - sweepPriorityTextfield'), 
        ''+SubSweepPriority)

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    amountRandom = (rand.nextInt(899999) + 100001)

    min = Integer.parseInt(GlobalVariable.confirmScreens.get('availableBalance1')) + amountRandom

    WebUI.setText(findTestObject('menu/LiquidityManagement/RangeBalance/TablesSubAccount/Textfield - minBalanceTextfield'), 
        ''+min)

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    min = (min * 2)

    WebUI.setText(findTestObject('menu/LiquidityManagement/RangeBalance/TablesSubAccount/Textfield - maxBalanceTextfield'), 
        ''+min)

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.setText(findTestObject('menu/LiquidityManagement/RangeBalance/TablesSubAccount/Textfield - subAccDescTextfield'), 
        subAccountDescription + amountRandom)

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.setText(findTestObject('menu/LiquidityManagement/RangeBalance/TablesSubAccount/Textfield - mainAccDescTextfield'), 
        mainAccountDescription + amountRandom)

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    count++

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.click(findTestObject('menu/LiquidityManagement/RangeBalance/BTN - plusClick'))

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.click(findTestObject('menu/LiquidityManagement/RangeBalance/TablesSubAccount/DL - subAccountClick - Copy'))

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.setText(findTestObject('menu/LiquidityManagement/RangeBalance/Textfield - textfieldMainAccountClick'), subAccounts.get(
            count))

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.click(findTestObject('menu/LiquidityManagement/RangeBalance/BTN - choices'), FailureHandling.STOP_ON_FAILURE)

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)
	
	SubSweepPriority = rand.nextInt(100) + 1

    WebUI.setText(findTestObject('menu/LiquidityManagement/RangeBalance/TablesSubAccount/Textfield - sweepPriorityTextfield - Copy'), 
        ''+SubSweepPriority)

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    amountRandom = (rand.nextInt(899999) + 100001)

    min = Integer.parseInt(GlobalVariable.confirmScreens.get('availableBalance2')) + amountRandom

    WebUI.setText(findTestObject('menu/LiquidityManagement/RangeBalance/TablesSubAccount/Textfield - minBalanceTextfield - Copy'), 
        ''+min)

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    min = (min * 2)

    WebUI.setText(findTestObject('menu/LiquidityManagement/RangeBalance/TablesSubAccount/Textfield - maxBalanceTextfield - Copy'), 
        ''+min)

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.setText(findTestObject('menu/LiquidityManagement/RangeBalance/TablesSubAccount/Textfield - subAccDescTextfield - Copy'), 
        subAccountDescription + amountRandom)

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.setText(findTestObject('menu/LiquidityManagement/RangeBalance/TablesSubAccount/Textfield - mainAccDescTextfield - Copy'), 
        mainAccountDescription + amountRandom)

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    count++
}

