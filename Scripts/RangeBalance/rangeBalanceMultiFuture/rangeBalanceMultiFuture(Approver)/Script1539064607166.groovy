import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(5)

menuChecker = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(rangeBalance)/Label - menuChecker'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('menuChecker', menuChecker)

WebUI.verifyMatch(menuChecker, GlobalVariable.confirmScreens.get('menuChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

productChecker = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(rangeBalance)/Label - productChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('productChecker', productChecker)

WebUI.verifyMatch(productChecker, GlobalVariable.confirmScreens.get('productChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

transRefNumberChecker = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(rangeBalance)/Label - transRefNumberChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(transRefNumberChecker, ': ' + GlobalVariable.confirmScreens.get('refNum'), false, FailureHandling.CONTINUE_ON_FAILURE)

documentTypeChecker = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(rangeBalance)/Label - documentCodeChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(documentTypeChecker, ': ' + GlobalVariable.confirmScreens.get('docNo'), false, FailureHandling.CONTINUE_ON_FAILURE)

submitDateChecker = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(rangeBalance)/Label - submitDateChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('submitDateChecker', submitDateChecker)

WebUI.verifyMatch(submitDateChecker, GlobalVariable.confirmScreens.get('submitDateChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

mainAccountChecker = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(rangeBalance)/Label - mainAccountChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

String[] mainAccountSplit = mainAccountChecker.split(' ')

WebUI.verifyMatch(mainAccountSplit[1], GlobalVariable.confirmScreens.get('mainAccountChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

mainSweepPriorityChecker = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(rangeBalance)/Label - setupSweepPriorityChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(mainSweepPriorityChecker, GlobalVariable.confirmScreens.get('mainSweepPriorityChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

autoReverseChecker = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(rangeBalance)/Label - autoReverseChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(autoReverseChecker, GlobalVariable.confirmScreens.get('autoReverseChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

tableChecker = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(cashDistribution)/Label - tables'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(tableChecker, GlobalVariable.confirmScreens.get('tableChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

cashConcerntrationChecker = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(cashDistribution)/Label - cashConcentrationChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(cashConcerntrationChecker, GlobalVariable.confirmScreens.get('cashConcerntrationChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

totalFeeChecker = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(cashDistribution)/Label - totalFeeChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(totalFeeChecker, GlobalVariable.confirmScreens.get('totalFeeChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

equilvalentToDebitAccountChecker = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(cashDistribution)/Label - equivalentToDebitAccountChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(equilvalentToDebitAccountChecker, GlobalVariable.confirmScreens.get('equilvalentToDebitAccountChecker'), 
    false, FailureHandling.CONTINUE_ON_FAILURE)

insModeChecker = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(cashDistribution)/Label - insModeChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(insModeChecker, GlobalVariable.confirmScreens.get('insModeChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

futureDateChecker = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(rangeBalance)/Label - futureDateChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(futureDateChecker, GlobalVariable.confirmScreens.get('futureDateChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

sessionChecker = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(rangeBalance)/Label - sessionChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(sessionChecker, GlobalVariable.confirmScreens.get('sessionChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

expiredChecker = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(cashDistribution)/Label - expiredChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(expiredChecker, GlobalVariable.confirmScreens.get('expiredChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/myTask/pendingTask/BTN - btnApprove'))

WebUI.delay(2)

not_run: WebUI.click(findTestObject('menu/myTask/pendingTask/Input - textfieldResponseCode'), FailureHandling.STOP_ON_FAILURE)

not_run: WebUI.setText(findTestObject('menu/myTask/pendingTask/Input - textfieldResponseCode'), '123456')

not_run: WebUI.delay(2)

WebUI.click(findTestObject('menu/myTask/pendingTask/BTN - btnApprove'))

WebUI.delay(2)

WebUI.click(findTestObject('menu/myTask/pendingTask/BTN - approveEnding'))

WebUI.delay(20)

