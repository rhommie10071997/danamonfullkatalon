import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(10, FailureHandling.CONTINUE_ON_FAILURE)

confirmScreenStatus = WebUI.getText(findTestObject('menu/LiquidityManagement/RangeBalance/confirmScreenStatus/confirmScreenStatus'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(confirmScreenStatus, 'Your transaction is waiting for approval', true, FailureHandling.CONTINUE_ON_FAILURE)

mainAccountChecker = WebUI.getText(findTestObject('menu/LiquidityManagement/RangeBalance/checker/Label - mainAccountChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

String[] mainAccountSplit = mainAccountChecker.split(' ')

WebUI.verifyMatch(mainAccountSplit[1], GlobalVariable.confirmScreens.get('mainAccountChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

mainSweepPriorityChecker = WebUI.getText(findTestObject('menu/LiquidityManagement/RangeBalance/checker/Label - setupSweepPriorityChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(mainSweepPriorityChecker, GlobalVariable.confirmScreens.get('mainSweepPriorityChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

autoReverseChecker = WebUI.getText(findTestObject('menu/LiquidityManagement/RangeBalance/checker/Label - autoReverseChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(autoReverseChecker, GlobalVariable.confirmScreens.get('autoReverseChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

tableChecker = WebUI.getText(findTestObject('menu/LiquidityManagement/RangeBalance/checker/Label - tables'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(tableChecker, GlobalVariable.confirmScreens.get('tableChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

cashConcerntrationChecker = WebUI.getText(findTestObject('menu/LiquidityManagement/RangeBalance/checker/Label - cashConcentrationChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(cashConcerntrationChecker, GlobalVariable.confirmScreens.get('cashConcerntrationChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

totalFeeChecker = WebUI.getText(findTestObject('menu/LiquidityManagement/RangeBalance/checker/Label - totalFeeChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(totalFeeChecker, GlobalVariable.confirmScreens.get('totalFeeChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

equilvalentToDebitAccountChecker = WebUI.getText(findTestObject('menu/LiquidityManagement/RangeBalance/checker/Label - equivalentToDebitAccountChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(equilvalentToDebitAccountChecker, GlobalVariable.confirmScreens.get('equilvalentToDebitAccountChecker'), 
    false, FailureHandling.CONTINUE_ON_FAILURE)

insModeChecker = WebUI.getText(findTestObject('menu/LiquidityManagement/RangeBalance/checker/Label - insModeChecker'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(insModeChecker, GlobalVariable.confirmScreens.get('insModeChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

expiredChecker = WebUI.getText(findTestObject('menu/LiquidityManagement/RangeBalance/checker/Label - expiredChecker'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(expiredChecker, GlobalVariable.confirmScreens.get('expiredChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

refNum = WebUI.getText(findTestObject('menu/LiquidityManagement/RangeBalance/checker/Label - refNumChecker'), FailureHandling.CONTINUE_ON_FAILURE)

String[] refNumSplit = refNum.split(' ')

GlobalVariable.confirmScreens.put('refNum', refNumSplit[2])

docNo = WebUI.getText(findTestObject('menu/LiquidityManagement/RangeBalance/checker/Label - DocChecker'), FailureHandling.CONTINUE_ON_FAILURE)

String[] docNoSplit = docNo.split(' ')

GlobalVariable.confirmScreens.put('docNo', docNoSplit[2])

submitDate = WebUI.getText(findTestObject('menu/LiquidityManagement/RangeBalance/checker/Label - submitDateChecker'), FailureHandling.CONTINUE_ON_FAILURE)

submitDate = submitDate.replace('Submitted Date ', '')

GlobalVariable.confirmScreens.put('submitDate', submitDate)

WebUI.delay(3, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/BTN - logoutClick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(10, FailureHandling.CONTINUE_ON_FAILURE)

