import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.setText(findTestObject('homelogin/homelogin.corporateid'), 'PCMKILLUA', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('homelogin/homelogin.userid'), 'killua', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('homelogin/homelogin.password'), 'Password123', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('homelogin/homelogin.submit'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(10, FailureHandling.CONTINUE_ON_FAILURE)

String MainAccount = '003571373608'

String SubAccount = '003571463466'

String SubAccount_2 = '003571403611'

String SettingSetupPriority = '100'

String autoReverse = 'No'

int jumlahSub = 2

if (jumlahSub > 2) {
    jumlahSub = 2
}

GlobalVariable.confirmScreens.put('jumlahSub', jumlahSub)

GlobalVariable.confirmScreens.put('SubAccount', SubAccount)

GlobalVariable.confirmScreens.put('SubAccount_2', SubAccount_2)

WebUI.click(findTestObject('menu/BTN - clickMenu'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/trfManagement/BTN - clickTrfManagement'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/BTN - clickBulkTrf'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.callTestCase(findTestCase('RangeBalance/rangeBalanceSingleFuture/getAvailableBalanced'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/BTN - clickMenu'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.scrollToElement(findTestObject('menu/LiquidityManagement/BTN - LiquidityManagementClick'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/LiquidityManagement/BTN - LiquidityManagementClick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/LiquidityManagement/RangeBalance/BTN - RangeBalanceCLick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(4, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/LiquidityManagement/RangeBalance/DL - mainAccountClick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('menu/LiquidityManagement/RangeBalance/Textfield - textfieldMainAccountClick'), MainAccount, 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/LiquidityManagement/RangeBalance/BTN - choices'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('menu/LiquidityManagement/RangeBalance/Textfield - sweepPriorityTextfield'), SettingSetupPriority, 
    FailureHandling.CONTINUE_ON_FAILURE)

if (autoReverse.equals('Yes')) {
    WebUI.click(findTestObject('menu/LiquidityManagement/RangeBalance/BTN - autoReverseClick'))
}

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.callTestCase(findTestCase('RangeBalance/rangeBalanceSingleFuture/InputSubAccount'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/LiquidityManagement/RangeBalance/specificDate/BTN - specificDateClick'))

WebUI.delay(2)

WebUI.click(findTestObject('menu/LiquidityManagement/RangeBalance/specificDate/BTN - futureDateClick'))

WebUI.click(findTestObject('menu/LiquidityManagement/RangeBalance/specificDate/tanggalClick/BTN - tanggalClick'))

WebUI.click(findTestObject('menu/LiquidityManagement/RangeBalance/specificDate/Session/DL - sessionClick'))

WebUI.click(findTestObject('menu/LiquidityManagement/RangeBalance/specificDate/Session/BTN - sessionChoice'))

WebUI.delay(2)

WebUI.click(findTestObject('menu/LiquidityManagement/RangeBalance/BTN - confirmClick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(6, FailureHandling.CONTINUE_ON_FAILURE)

mainAccountChecker = WebUI.getText(findTestObject('menu/LiquidityManagement/RangeBalance/checker/Label - mainAccountChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

String[] mainAccountSplit = mainAccountChecker.split(' ')

GlobalVariable.confirmScreens.put('mainAccountChecker', mainAccountSplit[1])

WebUI.verifyMatch(mainAccountSplit[1], GlobalVariable.confirmScreens.get('mainAccountChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

mainSweepPriorityChecker = WebUI.getText(findTestObject('menu/LiquidityManagement/RangeBalance/checker/Label - setupSweepPriorityChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('mainSweepPriorityChecker', mainSweepPriorityChecker)

WebUI.verifyMatch(mainSweepPriorityChecker, GlobalVariable.confirmScreens.get('mainSweepPriorityChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

autoReverseChecker = WebUI.getText(findTestObject('menu/LiquidityManagement/RangeBalance/checker/Label - autoReverseChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('autoReverseChecker', autoReverseChecker)

WebUI.verifyMatch(autoReverseChecker, GlobalVariable.confirmScreens.get('autoReverseChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

diagramChecker = WebUI.getText(findTestObject('menu/LiquidityManagement/RangeBalance/checker/Label - diagramsChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('diagramChecker', diagramChecker)

WebUI.verifyMatch(diagramChecker, GlobalVariable.confirmScreens.get('diagramChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

tableChecker = WebUI.getText(findTestObject('menu/LiquidityManagement/RangeBalance/checker/Label - tables'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('tableChecker', tableChecker)

WebUI.verifyMatch(tableChecker, GlobalVariable.confirmScreens.get('tableChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

cashConcerntrationChecker = WebUI.getText(findTestObject('menu/LiquidityManagement/RangeBalance/checker/Label - cashConcentrationChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('cashConcerntrationChecker', cashConcerntrationChecker)

WebUI.verifyMatch(cashConcerntrationChecker, GlobalVariable.confirmScreens.get('cashConcerntrationChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

totalFeeChecker = WebUI.getText(findTestObject('menu/LiquidityManagement/RangeBalance/checker/Label - totalFeeChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('totalFeeChecker', totalFeeChecker)

WebUI.verifyMatch(totalFeeChecker, GlobalVariable.confirmScreens.get('totalFeeChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

equilvalentToDebitAccountChecker = WebUI.getText(findTestObject('menu/LiquidityManagement/RangeBalance/checker/Label - equivalentToDebitAccountChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('equilvalentToDebitAccountChecker', equilvalentToDebitAccountChecker)

WebUI.verifyMatch(equilvalentToDebitAccountChecker, GlobalVariable.confirmScreens.get('equilvalentToDebitAccountChecker'), 
    false, FailureHandling.CONTINUE_ON_FAILURE)

insModeChecker = WebUI.getText(findTestObject('menu/LiquidityManagement/RangeBalance/checker/Label - insModeChecker'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('insModeChecker', insModeChecker)

WebUI.verifyMatch(insModeChecker, GlobalVariable.confirmScreens.get('insModeChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

futureDateChecker = WebUI.getText(findTestObject('menu/LiquidityManagement/RangeBalance/checker/Label - futureDateChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('futureDateChecker', futureDateChecker)

WebUI.verifyMatch(futureDateChecker, GlobalVariable.confirmScreens.get('futureDateChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

sessionChecker = WebUI.getText(findTestObject('menu/LiquidityManagement/RangeBalance/checker/Label - sessionChecker'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('sessionChecker', sessionChecker)

WebUI.verifyMatch(sessionChecker, GlobalVariable.confirmScreens.get('sessionChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

expiredChecker = WebUI.getText(findTestObject('menu/LiquidityManagement/RangeBalance/checker/Label - expiredChecker'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('expiredChecker', expiredChecker)

WebUI.verifyMatch(expiredChecker, GlobalVariable.confirmScreens.get('expiredChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/LiquidityManagement/RangeBalance/BTN - submitClick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/LiquidityManagement/RangeBalance/BTN - submitEndingClick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(10, FailureHandling.CONTINUE_ON_FAILURE)

