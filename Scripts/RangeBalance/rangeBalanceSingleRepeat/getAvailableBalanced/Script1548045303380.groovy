import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable


String[] availableBalanceForSubSplit 

if (GlobalVariable.confirmScreens.get('jumlahSub') == 1) {
	WebUI.delay(2)
	
	WebUI.waitForElementVisible(findTestObject('menu/trfManagement/bulkTransfer/DL - trfFormClick'), 0, FailureHandling.CONTINUE_ON_FAILURE)
	
	WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/DL - trfFormClick'), FailureHandling.CONTINUE_ON_FAILURE)
	
	WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Value - textfieldTrfForm'), GlobalVariable.confirmScreens.get(
	            'SubAccount'), FailureHandling.CONTINUE_ON_FAILURE)
	
	WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/trfFormChoice'), FailureHandling.CONTINUE_ON_FAILURE)
	
	WebUI.delay(4, FailureHandling.CONTINUE_ON_FAILURE)
		
	availableBalanceForSub = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/getAvailableBalance'))
	
	availableBalanceForSub = availableBalanceForSub.replace('IDR ', '')
	
	availableBalanceForSub = availableBalanceForSub.replace(',', '')
	
	availableBalanceForSub = availableBalanceForSub.replace('.', '. ')
	
	availableBalanceForSubSplit = availableBalanceForSub.split('. ')
	
	GlobalVariable.confirmScreens.put('availableBalance1',availableBalanceForSubSplit[0])
		
	WebUI.verifyMatch(availableBalanceForSubSplit[0], GlobalVariable.confirmScreens.get('availableBalance1'), false)
	
	WebUI.delay(4, FailureHandling.CONTINUE_ON_FAILURE)
	}
	else	{
	
		WebUI.delay(2)
	
		WebUI.waitForElementVisible(findTestObject('menu/trfManagement/bulkTransfer/DL - trfFormClick'), 0, FailureHandling.CONTINUE_ON_FAILURE)
	
		WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/DL - trfFormClick'), FailureHandling.CONTINUE_ON_FAILURE)
	
		WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Value - textfieldTrfForm'), GlobalVariable.confirmScreens.get(
				'SubAccount'), FailureHandling.CONTINUE_ON_FAILURE)
	
		WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/trfFormChoice'), FailureHandling.CONTINUE_ON_FAILURE)
	
		WebUI.delay(4, FailureHandling.CONTINUE_ON_FAILURE)
		
		availableBalanceForSub = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/getAvailableBalance'))
		
		availableBalanceForSub = availableBalanceForSub.replace('IDR ', '')
	
		availableBalanceForSub = availableBalanceForSub.replace(',', '')
	
		availableBalanceForSub = availableBalanceForSub.replace('.', '. ')
	
		availableBalanceForSubSplit = availableBalanceForSub.split('. ')
	
		GlobalVariable.confirmScreens.put('availableBalance1',availableBalanceForSubSplit[0])
	
		WebUI.verifyMatch(availableBalanceForSubSplit[0], GlobalVariable.confirmScreens.get('availableBalance1'), false)
	
		WebUI.delay(4, FailureHandling.CONTINUE_ON_FAILURE)
			
		WebUI.waitForElementVisible(findTestObject('menu/trfManagement/bulkTransfer/DL - trfFormClick'), 0, FailureHandling.CONTINUE_ON_FAILURE)
		
		WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/DL - trfFormClick'), FailureHandling.CONTINUE_ON_FAILURE)
		
		WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Value - textfieldTrfForm'), GlobalVariable.confirmScreens.get(
					'SubAccount_2'), FailureHandling.CONTINUE_ON_FAILURE)
		
		WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/trfFormChoice'), FailureHandling.CONTINUE_ON_FAILURE)
		
		WebUI.delay(4, FailureHandling.CONTINUE_ON_FAILURE)
		
		availableBalanceForSub = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/getAvailableBalance'))
		
		availableBalanceForSub = availableBalanceForSub.replace('IDR ', '')
		
		availableBalanceForSub = availableBalanceForSub.replace(',', '')
		
		availableBalanceForSub = availableBalanceForSub.replace('.', '. ')
		
		availableBalanceForSubSplit = availableBalanceForSub.split('. ')
		
		GlobalVariable.confirmScreens.put('availableBalance2',availableBalanceForSubSplit[0])
		
		WebUI.verifyMatch(availableBalanceForSubSplit[0], GlobalVariable.confirmScreens.get('availableBalance2'), false)
		
		
		WebUI.delay(4, FailureHandling.CONTINUE_ON_FAILURE)
	
	}

