import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.interactions.Actions as Actions
import java.util.Random as Random


//Transaction Status Reference Number 

refCodeChecker = WebUI.getText(findTestObject('menu/report/transStatus/checker(bulkTransfer)/Label - refNo3Checker'), FailureHandling.CONTINUE_ON_FAILURE)

String[] refCodeSplit = refCodeChecker.split(' ')

transStatusChecker = WebUI.getText(findTestObject('menu/report/transStatus/checker(bulkTransfer)/Label - transStatusChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

not_run: WebUI.getText(findTestObject('menu/report/transStatus/checker(bulkTransfer)/dateChecker'), FailureHandling.CONTINUE_ON_FAILURE)

menuChecker = WebUI.getText(findTestObject('menu/report/transStatus/checker(bulkTransfer)/Label - menuChecker'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('menuChecker', menuChecker)

versionNumberChecker = WebUI.getText(findTestObject('menu/report/transStatus/checker(bulkTransfer)/Label - versionNumber'), 
    FailureHandling.CONTINUE_ON_FAILURE)

String[] versionNumberSplit = versionNumberChecker.split(' ')

submitDateChecker = WebUI.getText(findTestObject('menu/report/transStatus/checker(bulkTransfer)/Label - submitDate'), FailureHandling.CONTINUE_ON_FAILURE)

String[] submitDateSplit = submitDateChecker.split(' ')

productChecker = WebUI.getText(findTestObject('menu/report/transStatus/checker(bulkTransfer)/Label - productChecker'), FailureHandling.CONTINUE_ON_FAILURE)

trfFromChecker = WebUI.getText(findTestObject('menu/report/transStatus/checker(bulkTransfer)/Label - trfFromChecker'), FailureHandling.CONTINUE_ON_FAILURE)

fromAccDescriptionChecker = WebUI.getText(findTestObject('menu/report/transStatus/checker(bulkTransfer)/Label - fromAccDescriptionChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

debitChargeChecker = WebUI.getText(findTestObject('menu/report/transStatus/checker(bulkTransfer)/Label - debitChargeChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

transTypeChecker = WebUI.getText(findTestObject('menu/report/transStatus/checker(bulkTransfer)/Label - transTypeChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

insModeChecker = WebUI.getText(findTestObject('menu/report/transStatus/checker(bulkTransfer)/Label - insModeChecker'), FailureHandling.CONTINUE_ON_FAILURE)

futureDateChecker = WebUI.getText(findTestObject('menu/report/transStatus/checker(bulkTransfer)/Label - futureDateChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

sessionChecker = WebUI.getText(findTestObject('menu/report/transStatus/checker(bulkTransfer)/Label - sessionChecker'), FailureHandling.CONTINUE_ON_FAILURE)

expiredChecker = WebUI.getText(findTestObject('menu/report/transStatus/checker(bulkTransfer)/Label - expiredChecker'), FailureHandling.CONTINUE_ON_FAILURE)

ttrChecker = WebUI.getText(findTestObject('menu/report/transStatus/checker(bulkTransfer)/Label - ttr'), FailureHandling.CONTINUE_ON_FAILURE)

String[] ttrSplit = ttrChecker.split(' ')

amountChecker = WebUI.getText(findTestObject('menu/report/transStatus/checker(bulkTransfer)/Label - amountChecker'), FailureHandling.CONTINUE_ON_FAILURE)

exchangeRateChecker = WebUI.getText(findTestObject('menu/report/transStatus/checker(bulkTransfer)/Label - exchangeRateChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

totalChargesChecker = WebUI.getText(findTestObject('menu/report/transStatus/checker(bulkTransfer)/Label - totalCharges'), 
    FailureHandling.CONTINUE_ON_FAILURE)

totalDebitAmountChecker = WebUI.getText(findTestObject('menu/report/transStatus/checker(bulkTransfer)/Label - totalDebitAmount'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('transStatusChecker', transStatusChecker)

WebUI.verifyMatch(transStatusChecker, GlobalVariable.confirmScreens.get('transStatusChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(refCodeSplit[1], GlobalVariable.confirmScreens.get('refNum'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(versionNumberSplit[1], GlobalVariable.confirmScreens.get('docNo'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(submitDateChecker, ': ' + GlobalVariable.confirmScreens.get('submitDate'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(menuChecker, GlobalVariable.confirmScreens.get('menuChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(productChecker, GlobalVariable.confirmScreens.get('productChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(trfFromChecker, GlobalVariable.confirmScreens.get('trfFromChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(fromAccDescriptionChecker, GlobalVariable.confirmScreens.get('accDescChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(debitChargeChecker, GlobalVariable.confirmScreens.get('debitChargeChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(transTypeChecker, GlobalVariable.confirmScreens.get('trfTypeChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(insModeChecker, GlobalVariable.confirmScreens.get('insModeChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(futureDateChecker, GlobalVariable.confirmScreens.get('futureDateChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(sessionChecker, GlobalVariable.confirmScreens.get('sessionChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

not_run: WebUI.verifyMatch(expiredChecker, GlobalVariable.confirmScreens.get('expiredChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(ttrSplit[1], GlobalVariable.confirmScreens.get('ttrChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(amountChecker, GlobalVariable.confirmScreens.get('amountChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(exchangeRateChecker, GlobalVariable.confirmScreens.get('exchangeRateChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(totalChargesChecker, GlobalVariable.confirmScreens.get('totalChargesChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(totalDebitAmountChecker, GlobalVariable.confirmScreens.get('totalDebitAmountChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/report/transStatus/recordList/BTN - recordListClick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

debitAccount_recordList = WebUI.getText(findTestObject('menu/report/transStatus/recordList/Label - debitAccountChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('debitAccount_recordList', debitAccount_recordList)

WebUI.verifyMatch(debitAccount_recordList, GlobalVariable.confirmScreens.get('debitAccount_recordList'), false, FailureHandling.CONTINUE_ON_FAILURE)

product_recordList = WebUI.getText(findTestObject('menu/report/transStatus/recordList/Label - productChecker'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('product_recordList', product_recordList)

WebUI.verifyMatch(product_recordList, GlobalVariable.confirmScreens.get('product_recordList'), false, FailureHandling.CONTINUE_ON_FAILURE)

insMode_recordList = WebUI.getText(findTestObject('menu/report/transStatus/recordList/Label - insModeChecker'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('insMode_recordList', insMode_recordList)

WebUI.verifyMatch(insMode_recordList, GlobalVariable.confirmScreens.get('insMode_recordList'), false, FailureHandling.CONTINUE_ON_FAILURE)

futureDate_recordList = WebUI.getText(findTestObject('menu/report/transStatus/recordList/Label - futureDateChecker'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('futureDate_recordList', futureDate_recordList)

WebUI.verifyMatch(futureDate_recordList, GlobalVariable.confirmScreens.get('futureDate_recordList'), false, FailureHandling.CONTINUE_ON_FAILURE)

session_recordList = WebUI.getText(findTestObject('menu/report/transStatus/recordList/Label - sessionChecker'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('session_recordList', session_recordList)

WebUI.verifyMatch(session_recordList, GlobalVariable.confirmScreens.get('session_recordList'), false, FailureHandling.CONTINUE_ON_FAILURE)

expired_recordList = WebUI.getText(findTestObject('menu/report/transStatus/recordList/Label - expiredChecker'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('expired_recordList', expired_recordList)

WebUI.verifyMatch(expired_recordList, GlobalVariable.confirmScreens.get('expired_recordList'), false, FailureHandling.CONTINUE_ON_FAILURE)

ttr_recordList = WebUI.getText(findTestObject('menu/report/transStatus/recordList/Label - ttrChecker'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('ttr_recordList', ttr_recordList)

WebUI.verifyMatch(ttr_recordList, GlobalVariable.confirmScreens.get('ttr_recordList'), false, FailureHandling.CONTINUE_ON_FAILURE)

amount_recordList = WebUI.getText(findTestObject('menu/report/transStatus/recordList/Label - amountChecker'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('amount_recordList', amount_recordList)

WebUI.verifyMatch(amount_recordList, GlobalVariable.confirmScreens.get('amount_recordList'), false, FailureHandling.CONTINUE_ON_FAILURE)

counterRate_recordList = WebUI.getText(findTestObject('menu/report/transStatus/recordList/Label - exchangeRateChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('counterRate_recordList', counterRate_recordList)

WebUI.verifyMatch(counterRate_recordList, GlobalVariable.confirmScreens.get('counterRate_recordList'), false, FailureHandling.CONTINUE_ON_FAILURE)

totalCharges_recordList = WebUI.getText(findTestObject('menu/report/transStatus/recordList/Label - totalChargesChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('totalCharges_recordList', totalCharges_recordList)

WebUI.verifyMatch(totalCharges_recordList, GlobalVariable.confirmScreens.get('totalCharges_recordList'), false, FailureHandling.CONTINUE_ON_FAILURE)

totalDebitAmount_recordList = WebUI.getText(findTestObject('menu/report/transStatus/recordList/Label - totalDebitAmountChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('totalDebitAmount_recordList', totalDebitAmount_recordList)

WebUI.verifyMatch(totalDebitAmount_recordList, GlobalVariable.confirmScreens.get('totalDebitAmount_recordList'), false, 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/report/transStatus/recordList/recordListClose'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(10, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/report/transStatus/transStatus_2/transStatusClick'), FailureHandling.CONTINUE_ON_FAILURE)

