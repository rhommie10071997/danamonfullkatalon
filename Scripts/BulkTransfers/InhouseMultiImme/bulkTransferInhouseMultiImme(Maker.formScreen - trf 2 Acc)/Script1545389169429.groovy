import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import java.text.DecimalFormat as DecimalFormat
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import java.text.NumberFormat as NumberFormat


Random rand = new Random()

int amountRandom

amountRandom = (rand.nextInt(999999 - 100000) + 10000)

String Product = 'In-House (Overbooking)'

//Flag Trf to , Own account = 1 , Beneficiary List = 2 , new Beneficiary = 3
String flagTrfTo = '1'

//Form Transaction
String Transfer_Notification = '123123123123'

String Reference_Number = '1231231234'

String Amount = amountRandom

String BeneCategory = 'pemerintah'

String Transaction_Relationship = 'Affiliated'

String POF = 'Ekspor barang'

String PurposeCode = 'Investasi Pemberian Kredit'

String Document_Description = 'Document Bray'

String Document_Type = 'Fotokopi Pemberitahuan Impor'

//--> Own Account
String Transfer_To = '000001825736'

GlobalVariable.confirmScreens.put('checkAccount', Transfer_To)

String Transfer_To_Description = 'testbraay2'

String Transfer_NotificationEmail = 'ssdfsfd@sadasd.com'

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/DL - productClick'), FailureHandling.CONTINUE_ON_FAILURE)

//--> Beneficiary List
//-->New Beneficiary
//
WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Value - trfToTextfield'), Product, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/BTN - productChoice'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('productChecker', Product)

GlobalVariable.confirmScreens.put('product_detailRecord1', Product)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

if (flagTrfTo.equals('1')) {
    WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/DL - trfToClick'), FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Value - trfToTextfield'), Transfer_To, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/BTN - trfToChoice'), FailureHandling.CONTINUE_ON_FAILURE)

    trfToEntry = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/DL - trfToClick'), FailureHandling.STOP_ON_FAILURE)

    trfToEntry = trfToEntry.replace(' - ', ' / ')

    trfToEntry = trfToEntry.replace('(', ' (')

    GlobalVariable.confirmScreens.put('accNumber_detailRecord1', trfToEntry)

    GlobalVariable.confirmScreens.put('trfTo_detailRecord1', ': Own account')
} else if (flagTrfTo.equals('2')) {
    WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/BTN - benefiaryListClick'), FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/DL - beneficiaryListClick'), FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Value - trfToTextfield'), Transfer_To, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/BTN - trfToChoice'), FailureHandling.CONTINUE_ON_FAILURE)

    trfToEntry = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/DL - beneficiaryListClick'), FailureHandling.STOP_ON_FAILURE)

    trfToEntry = trfToEntry.replace(' - ', ' / ')

    trfToEntry = trfToEntry.replace('(', ' (')

    GlobalVariable.confirmScreens.put('accNumber_detailRecord1', trfToEntry)

    GlobalVariable.confirmScreens.put('trfTo_detailRecord1', ': Beneficiary List')
} else {
    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/BTN - newBeneficiaryClick'), FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Textfield - AccountNumberCheck'), Transfer_To)

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/BTN - checkName'), FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.delay(13, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/Checkbox - saveToBeneList'), FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Textfield - aliasName'), 'BTAliasName' + Amount)

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    GlobalVariable.confirmScreens.put('trfTo_detailRecord1', ': New Beneficiary')
}

WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Value - troToDescription'), Transfer_To_Description, FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('toAccDescription_detailRecord1', Transfer_To_Description)

WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Value - sendTransferNotifocationEmail'), Transfer_NotificationEmail, 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('beneEmail_detailRecord1', Transfer_NotificationEmail)

WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Value - sendTransferNotifocation'), Transfer_Notification, 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('beneSms_detailRecord1', Transfer_Notification)

WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Value - beneficiaryReferenceNumber'), Reference_Number, FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('beneRefNo_detailRecord1', Reference_Number)

WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Value - amount'), '' + Amount, FailureHandling.CONTINUE_ON_FAILURE)

Locale local1 = new Locale('en', 'UK')

DecimalFormat amountFormat = ((NumberFormat.getNumberInstance(local1)) as DecimalFormat)

amountFormat.applyPattern('###,###,##0.00')

String amountResult = '' + amountFormat.format(new BigDecimal(Amount))

GlobalVariable.confirmScreens.put('amount_detailRecord1', amountResult)

WebUI.delay(4, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/DL - beneClick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(3, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Value - textfieldBene'), BeneCategory, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/Value - beneCategoryChoice'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/DL - relationshipClick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Value - textfieldBene'), Transaction_Relationship, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/BTN - relationshipChoice'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/DL - pofClick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Value - textfieldBene'), POF, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/BTN - pofChoice'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/DL - codeClick '), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Value - textfieldBene'), PurposeCode, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/BTN - codeChoice'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Value - lhbuDocumentTypeDescription'), Document_Description, 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('docDesc_detailRecord1', Document_Description)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/DL - documentTypeClick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Value - textfieldBene'), Document_Type, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/Value - documentTypeChoice'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

beneCategoryEntry = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/DL - beneClick'), FailureHandling.STOP_ON_FAILURE)

transRelationshipEntry = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/DL - relationshipClick'), FailureHandling.STOP_ON_FAILURE)

pofEntry = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/DL - pofClick'), FailureHandling.STOP_ON_FAILURE)

purposeCodeEntry = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/DL - codeClick '), FailureHandling.STOP_ON_FAILURE)

documentTypeEntry = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/DL - documentTypeClick'), FailureHandling.STOP_ON_FAILURE)

GlobalVariable.confirmScreens.put('beneCategory_detailRecord1', beneCategoryEntry)

GlobalVariable.confirmScreens.put('transRelationship_detailRecord1', transRelationshipEntry)

GlobalVariable.confirmScreens.put('PoT_detailRecord1', pofEntry)

GlobalVariable.confirmScreens.put('purpose__detailRecord1', purposeCodeEntry)

GlobalVariable.confirmScreens.put('docType_detailRecord1', documentTypeEntry)

WebUI.delay(3, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/BTN - addMoreTransaction'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(10, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/DL - productClick'), FailureHandling.CONTINUE_ON_FAILURE)

//--> Beneficiary List
//-->New Beneficiary
//
WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Value - trfToTextfield'), Product, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/BTN - productChoice'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('productChecker', Product)

GlobalVariable.confirmScreens.put('product_detailRecord2', Product)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

if (flagTrfTo.equals('1')) {
    WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/DL - trfToClick'), FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Value - trfToTextfield'), Transfer_To, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/BTN - trfToChoice'), FailureHandling.CONTINUE_ON_FAILURE)

    trfToEntry = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/DL - trfToClick'), FailureHandling.STOP_ON_FAILURE)

    trfToEntry = trfToEntry.replace(' - ', ' / ')

    trfToEntry = trfToEntry.replace('(', ' (')

    GlobalVariable.confirmScreens.put('accNumber_detailRecord2', trfToEntry)

    GlobalVariable.confirmScreens.put('trfTo_detailRecord2', ': Own account')
} else if (flagTrfTo.equals('2')) {
    WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/BTN - benefiaryListClick'), FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/DL - beneficiaryListClick'), FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Value - trfToTextfield'), Transfer_To, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/BTN - trfToChoice'), FailureHandling.CONTINUE_ON_FAILURE)

    trfToEntry = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/DL - beneficiaryListClick'), FailureHandling.STOP_ON_FAILURE)

    trfToEntry = trfToEntry.replace(' - ', ' / ')

    trfToEntry = trfToEntry.replace('(', ' (')

    GlobalVariable.confirmScreens.put('accNumber_detailRecord2', trfToEntry)

    GlobalVariable.confirmScreens.put('trfTo_detailRecord2', ': Beneficiary List')
} else {
    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/BTN - newBeneficiaryClick'), FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Textfield - AccountNumberCheck'), Transfer_To)

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/BTN - checkName'), FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.delay(13, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/Checkbox - saveToBeneList'), FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Textfield - aliasName'), 'BTAliasName' + Amount)

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    GlobalVariable.confirmScreens.put('trfTo_detailRecord2', ': New Beneficiary')
	
	
}

WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Value - troToDescription'), Transfer_To_Description, FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('toAccDescription_detailRecord2', Transfer_To_Description)

WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Value - sendTransferNotifocationEmail'), Transfer_NotificationEmail, 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('beneEmail_detailRecord2', Transfer_NotificationEmail)

WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Value - sendTransferNotifocation'), Transfer_Notification, 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('beneSms_detailRecord2', Transfer_Notification)

WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Value - beneficiaryReferenceNumber'), Reference_Number, FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('beneRefNo_detailRecord2', Reference_Number)

WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Value - amount'), '' + Amount, FailureHandling.CONTINUE_ON_FAILURE)

Locale local1_2 = new Locale('en', 'UK')

DecimalFormat amountFormat_2 = ((NumberFormat.getNumberInstance(local1_2)) as DecimalFormat)

amountFormat.applyPattern('###,###,##0.00')

String amountResult_2 = '' + amountFormat.format(new BigDecimal(Amount))

GlobalVariable.confirmScreens.put('amount_detailRecord2', amountResult_2)

WebUI.delay(4, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/DL - beneClick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(3, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Value - textfieldBene'), BeneCategory, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/Value - beneCategoryChoice'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/DL - relationshipClick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Value - textfieldBene'), Transaction_Relationship, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/BTN - relationshipChoice'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/DL - pofClick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Value - textfieldBene'), POF, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/BTN - pofChoice'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/DL - codeClick '), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Value - textfieldBene'), PurposeCode, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/BTN - codeChoice'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Value - lhbuDocumentTypeDescription'), Document_Description, 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('docDesc_detailRecord2', Document_Description)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/DL - documentTypeClick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Value - textfieldBene'), Document_Type, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/Value - documentTypeChoice'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

beneCategoryEntry = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/DL - beneClick'), FailureHandling.STOP_ON_FAILURE)

transRelationshipEntry = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/DL - relationshipClick'), FailureHandling.STOP_ON_FAILURE)

pofEntry = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/DL - pofClick'), FailureHandling.STOP_ON_FAILURE)

purposeCodeEntry = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/DL - codeClick '), FailureHandling.STOP_ON_FAILURE)

documentTypeEntry = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/DL - documentTypeClick'), FailureHandling.STOP_ON_FAILURE)

GlobalVariable.confirmScreens.put('beneCategory_detailRecord2', beneCategoryEntry)

GlobalVariable.confirmScreens.put('transRelationship_detailRecord2', transRelationshipEntry)

GlobalVariable.confirmScreens.put('PoT_detailRecord2', pofEntry)

GlobalVariable.confirmScreens.put('purpose__detailRecord2', purposeCodeEntry)

GlobalVariable.confirmScreens.put('docType_detailRecord2', documentTypeEntry)

