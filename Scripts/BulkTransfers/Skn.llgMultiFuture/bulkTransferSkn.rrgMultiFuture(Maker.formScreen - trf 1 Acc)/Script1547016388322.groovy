import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

Random rand = new Random()

int amountRandom

amountRandom = (rand.nextInt(999999 - 100000) + 10000)

String Product = 'SKN/LLG'

String Beneficiary_Type = 'Individual'

String Beneficiary_List = 'alias name1'

String Transfer_To_Description = 'testbraay2'

String Reference_Number = '1231231234'

String Amount = amountRandom

String BeneCategory = 'pemerintah'

String Transaction_Relationship = 'Affiliated'

String POF = 'Ekspor barang'

String PurposeCode = 'Investasi Pemberian Kredit'

String Document_Description = 'Document Bray'

String Document_Type = 'Fotokopi Pemberitahuan Impor'

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/DL - productClick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Value - trfToTextfield'), Product, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/BTN - productChoice'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/DL - beneficiaryTypeClick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Value - trfToTextfield'), Beneficiary_Type, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/BTN - trfToChoice'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/DL - beneficiaryListClick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Value - trfToTextfield'), Beneficiary_List, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/BTN - trfToChoice'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Value - troToDescription'), Transfer_To_Description, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Value - beneficiaryReferenceNumber'), Reference_Number, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Value - amount'), '' + Amount, FailureHandling.CONTINUE_ON_FAILURE)

not_run: WebUI.click(findTestObject('menu/BTN - clickMenu'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(4, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/DL - beneClick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(3, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Value - textfieldBene'), BeneCategory, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/Value - beneCategoryChoice'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/DL - relationshipClick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Value - textfieldBene'), Transaction_Relationship, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/BTN - relationshipChoice'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/DL - pofClick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Value - textfieldBene'), POF, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/BTN - pofChoice'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/DL - codeClick '), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Value - textfieldBene'), PurposeCode, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/BTN - codeChoice'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Value - lhbuDocumentTypeDescription'), Document_Description, 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/DL - documentTypeClick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Value - textfieldBene'), Document_Type, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/Value - documentTypeChoice'), FailureHandling.CONTINUE_ON_FAILURE)

