import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(10)

refCodeChecker2 = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/checker/refNo3Checker'))

String[] refCodeSplit2 = refCodeChecker2.split(' ')

String refCodeResult2 = refCodeSplit2[1]

GlobalVariable.refNo3 = refCodeResult2

WebUI.verifyMatch(GlobalVariable.refNo3, GlobalVariable.refNo2, false)

menuChecker2 = WebUI.getText(findTestObject('menu/report/transStatus/checker(bulkTransfer)/Label - menuChecker'), FailureHandling.STOP_ON_FAILURE)

productChecker2 = WebUI.getText(findTestObject('menu/report/transStatus/checker(bulkTransfer)/Label - productChecker'), FailureHandling.STOP_ON_FAILURE)

String[] productChecker2Split = productChecker2.split(' ')

String productChecker2result = productChecker2Split[1]

trfFromChecker2 = WebUI.getText(findTestObject('menu/report/transStatus/checker(bulkTransfer)/Label - trfFromChecker'), FailureHandling.STOP_ON_FAILURE)

String[] trfFromChecker2split = trfFromChecker2.split(' ')

String trfFromChecker2result = trfFromChecker2split[1]

accDescriptionChecker2 = WebUI.getText(findTestObject('menu/report/transStatus/checker(bulkTransfer)/Label - fromAccDescriptionChecker'), FailureHandling.STOP_ON_FAILURE)

String[] accDescriptionChecker2split = accDescriptionChecker2.split(' ')

String accDescriptionChecker2result = accDescriptionChecker2split[1]

debitChargeChecker2 = WebUI.getText(findTestObject('menu/report/transStatus/checker(bulkTransfer)/Label - debitChargeChecker'), FailureHandling.STOP_ON_FAILURE)

String[] debitChargeChecker2split = debitChargeChecker2.split(' ')

String debitChargeChecker2result = debitChargeChecker2split[1]

transTypeChecker2 = WebUI.getText(findTestObject('menu/report/transStatus/checker(bulkTransfer)/Label - transTypeChecker'), FailureHandling.STOP_ON_FAILURE)

String[] transTypeChecker2split = transTypeChecker2.split(' ')

String transTypeChecker2result = transTypeChecker2split[1]

insModeChecker2 = WebUI.getText(findTestObject('menu/report/transStatus/checker(bulkTransfer)/Label - insModeChecker'), FailureHandling.STOP_ON_FAILURE)

String[] insModeChecker2split = insModeChecker2.split(' ')

String insModeChecker2result = insModeChecker2split[1]

futureDateChecker2 = WebUI.getText(findTestObject('menu/report/transStatus/checker(bulkTransfer)/Label - futureDateChecker'), FailureHandling.STOP_ON_FAILURE)

String[] futureDateChecker2split = futureDateChecker2.split(' ')

String futureDateChecker2result = futureDateChecker2split[1]

sessionChecker2 = WebUI.getText(findTestObject('menu/report/transStatus/checker(bulkTransfer)/Label - sessionChecker'), FailureHandling.STOP_ON_FAILURE)

String[] sessionChecker2split = sessionChecker2.split(' ')

String sessionChecker2result = sessionChecker2split[1]

expiredChecker2 = WebUI.getText(findTestObject('menu/report/transStatus/checker(bulkTransfer)/expiredChecker - Inhouse(future)'), FailureHandling.STOP_ON_FAILURE)

String[] expiredChecker2split = expiredChecker2.split(' ')

String expiredChecker2result = ((((expiredChecker2split[1]) + ' ') + (expiredChecker2split[2])) + ' ') + (expiredChecker2split[
3])

amountChecker2 = WebUI.getText(findTestObject('menu/report/transStatus/checker(bulkTransfer)/amountChecker - Inhouse(future)'), FailureHandling.STOP_ON_FAILURE)

String[] amountChecker2split = amountChecker2.split(' ')

String amountChecker2result = amountChecker2split[1]

exchangeRateChecker2 = WebUI.getText(findTestObject('menu/report/transStatus/checker(bulkTransfer)/exchangeRateChecker(Future)'), FailureHandling.STOP_ON_FAILURE)

String[] exchangeRateChecker2split = exchangeRateChecker2.split(' ')

String exchangeRateChecker2result = exchangeRateChecker2split[1]

totalChargesChecker2 = WebUI.getText(findTestObject('menu/report/transStatus/checker(bulkTransfer)/Charges(Inter)/totalCharges(Sknllg.Future)'), 
    FailureHandling.STOP_ON_FAILURE)

String[] totalChargeschecker2split = totalChargesChecker2.split(' ')

String totalChargeschecker2result = totalChargeschecker2split[1]

totalDebitAmountChecker2 = WebUI.getText(findTestObject('menu/report/transStatus/checker(bulkTransfer)/totalDebitAmount - skn.llg(future)'), 
    FailureHandling.STOP_ON_FAILURE)

String[] totalDebitAmountchecker2split = totalDebitAmountChecker2.split(' ')

String totalDebitAmountchecker2result = totalDebitAmountchecker2split[1]

not_run: WebUI.verifyMatch(transStatusChecker2, ': Executed Successfully', true)

WebUI.verifyMatch(menuChecker2, ': Bulk Transfer', true)

WebUI.verifyMatch(productChecker2result, 'SKN/LLG', true)

WebUI.verifyMatch(trfFromChecker2result, '000021942610', true)

WebUI.verifyMatch(accDescriptionChecker2result, GlobalVariable.accDesc, true)

WebUI.verifyMatch(debitChargeChecker2result, GlobalVariable.debitCharge, true)

WebUI.verifyMatch(transTypeChecker2result, GlobalVariable.trfType, true)

WebUI.verifyMatch(insModeChecker2result, GlobalVariable.insMode, true)

not_run: WebUI.verifyMatch(expiredChecker2result, GlobalVariable.expired, true)

WebUI.verifyMatch(amountChecker2result, 'IDR' + GlobalVariable.amount, true)

WebUI.verifyMatch(totalDebitAmountchecker2result, 'IDR' + GlobalVariable.totalDebitAmount, true)

WebUI.delay(5)

WebUI.click(findTestObject('menu/report/transStatus/recordList/BTN - recordListClick'))

WebUI.delay(5)

debitAccount_recordList2 = WebUI.getText(findTestObject('menu/report/transStatus/recordList/Label - debitAccountChecker'))

String[] debitAccount_recordList2split = debitAccount_recordList2.split(' ')

String debitAccount_recordList2result = debitAccount_recordList2split[1]

product_recordList2 = WebUI.getText(findTestObject('menu/report/transStatus/recordList/Label - productChecker'))

String[] product_recordList2split = product_recordList2.split(' ')

String product_recordList2result = product_recordList2split[1]

insMode_recordList2 = WebUI.getText(findTestObject('menu/report/transStatus/recordList/Label - insModeChecker'))

String[] insMode_recordList2split = insMode_recordList2.split(' ')

String insMode_recordList2result = insMode_recordList2split[1]

expired_recordList2 = WebUI.getText(findTestObject('menu/report/transStatus/recordList/expiredChecker - Inhouse(Future)'))

String[] expired_recordList2split = expired_recordList2.split(' ')

String expired_recordList2result = ((((expired_recordList2split[1]) + ' ') + (expired_recordList2split[2])) + ' ') + (expired_recordList2split[
3])

ttr_recordList2 = WebUI.getText(findTestObject('menu/report/transStatus/recordList/ttrChecker - Inhouse(Future)'))

String[] ttr_recordList2split = ttr_recordList2.split(' ')

String ttr_recordList2result = ttr_recordList2split[1]

amount_recordList2 = WebUI.getText(findTestObject('menu/report/transStatus/recordList/amountChecker - Inhouse(future)'))

String[] amount_recordList2split = amount_recordList2.split(' ')

String amount_recordList2result = amount_recordList2split[1]

counterRate_recordList2 = WebUI.getText(findTestObject('menu/report/transStatus/recordList/counterRateChecker - Inhouse(future)'))

String[] counterRate_recordList2split = counterRate_recordList2.split(' ')

String counterRate_recordList2result = counterRate_recordList2split[1]

totalCharges_recordList2 = WebUI.getText(findTestObject('menu/report/transStatus/recordList/totalChargesChecker (skn.llg.future)'))

String[] totalCharges_recordList2split = totalCharges_recordList2.split(' ')

String totalCharges_recordList2result = totalCharges_recordList2split[1]

totalDebitAmount_recordList2 = WebUI.getText(findTestObject('menu/report/transStatus/recordList/totalDebitAmountChecker (skn.llg) - future'))

String[] totalDebitAmount_recordList2split = totalDebitAmount_recordList2.split(' ')

String totalDebitAmount_recordList2result = totalDebitAmount_recordList2split[1]

WebUI.verifyMatch(debitAccount_recordList2result, '000021942610', true)

WebUI.verifyMatch(product_recordList2result, 'SKN/LLG', true)

WebUI.verifyMatch(insMode_recordList2result, 'Spesific', true)

not_run: WebUI.verifyMatch(expired_recordList2result, GlobalVariable.expired, true)

WebUI.verifyMatch(ttr_recordList2result, '1', true)

WebUI.verifyMatch(amount_recordList2result, 'IDR' + GlobalVariable.amount, true)

WebUI.verifyMatch(counterRate_recordList2result, 'Counter', true)

WebUI.verifyMatch(totalCharges_recordList2result, 'IDR' + GlobalVariable.totalCharges, true)

WebUI.verifyMatch(totalDebitAmount_recordList2result, 'IDR' + GlobalVariable.totalDebitAmount, true)

WebUI.click(findTestObject('menu/report/transStatus/recordList/recordListClose'))

WebUI.delay(10)

WebUI.click(findTestObject('menu/BTN - logoutClick'))

WebUI.delay(50)

