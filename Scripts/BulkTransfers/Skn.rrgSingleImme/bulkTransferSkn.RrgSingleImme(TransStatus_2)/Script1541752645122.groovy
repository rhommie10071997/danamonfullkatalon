import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(7, FailureHandling.CONTINUE_ON_FAILURE)

refCodeChecker2 = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/checker/refNo3Checker'), FailureHandling.CONTINUE_ON_FAILURE)

String[] refCodeSplit2 = refCodeChecker2.split(' ')

String refCodeResult2 = refCodeSplit2[1]

GlobalVariable.refNo3 = refCodeResult2

WebUI.verifyMatch(GlobalVariable.refNo3, GlobalVariable.refNo2, false, FailureHandling.CONTINUE_ON_FAILURE)

menuChecker2 = WebUI.getText(findTestObject('menu/report/transStatus/checker(bulkTransfer)/Label - menuChecker'), FailureHandling.CONTINUE_ON_FAILURE)

productChecker2 = WebUI.getText(findTestObject('menu/report/transStatus/checker(bulkTransfer)/Label - productChecker'), FailureHandling.CONTINUE_ON_FAILURE)

String[] productChecker2Split = productChecker2.split(' ')

String productChecker2result = productChecker2Split[1]

trfFromChecker2 = WebUI.getText(findTestObject('menu/report/transStatus/checker(bulkTransfer)/Label - trfFromChecker'), FailureHandling.CONTINUE_ON_FAILURE)

String[] trfFromChecker2split = trfFromChecker2.split(' ')

String trfFromChecker2result = trfFromChecker2split[1]

accDescriptionChecker2 = WebUI.getText(findTestObject('menu/report/transStatus/checker(bulkTransfer)/Label - fromAccDescriptionChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

String[] accDescriptionChecker2split = accDescriptionChecker2.split(' ')

String accDescriptionChecker2result = accDescriptionChecker2split[1]

debitChargeChecker2 = WebUI.getText(findTestObject('menu/report/transStatus/checker(bulkTransfer)/Label - debitChargeChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

String[] debitChargeChecker2split = debitChargeChecker2.split(' ')

String debitChargeChecker2result = debitChargeChecker2split[1]

transTypeChecker2 = WebUI.getText(findTestObject('menu/report/transStatus/checker(bulkTransfer)/Label - transTypeChecker'), FailureHandling.CONTINUE_ON_FAILURE)

String[] transTypeChecker2split = transTypeChecker2.split(' ')

String transTypeChecker2result = transTypeChecker2split[1]

insModeChecker2 = WebUI.getText(findTestObject('menu/report/transStatus/checker(bulkTransfer)/Label - insModeChecker'), FailureHandling.CONTINUE_ON_FAILURE)

String[] insModeChecker2split = insModeChecker2.split(' ')

String insModeChecker2result = insModeChecker2split[1]

expiredChecker2 = WebUI.getText(findTestObject('menu/report/transStatus/checker(bulkTransfer)/Label - expiredChecker'), FailureHandling.CONTINUE_ON_FAILURE)

String[] expiredChecker2split = expiredChecker2.split(' ')

String expiredChecker2result = ((((expiredChecker2split[1]) + ' ') + (expiredChecker2split[2])) + ' ') + (expiredChecker2split[
3])

amountChecker2 = WebUI.getText(findTestObject('menu/report/transStatus/checker(bulkTransfer)/Label - amountChecker'), FailureHandling.CONTINUE_ON_FAILURE)

String[] amountChecker2split = amountChecker2.split(' ')

String amountChecker2result = amountChecker2split[1]

totalDebitAmountChecker2 = WebUI.getText(findTestObject('menu/report/transStatus/checker(bulkTransfer)/totalDebitAmount (skn.llg)'), 
    FailureHandling.CONTINUE_ON_FAILURE)

String[] totalDebitAmountchecker2split = totalDebitAmountChecker2.split(' ')

String totalDebitAmountchecker2result = totalDebitAmountchecker2split[1]

WebUI.verifyMatch(transStatusChecker2, ': Executed Successfully', true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(menuChecker2, ': Bulk Transfer', true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(productChecker2result, 'SKN/LLG', true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(trfFromChecker2result, '000021942610', true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(accDescriptionChecker2result, accDescResult, true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(debitChargeChecker2result, debitChargeResult, true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(transTypeChecker2result, trfTypeResult, true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(insModeChecker2result, insModeResult, true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(expiredChecker2result, GlobalVariable.expired, true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(amountChecker2result, 'IDR575,000.00', true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(totalDebitAmountchecker2result, 'IDR605,000.00', true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/report/transStatus/recordList/BTN - recordListClick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

debitAccount_recordList2 = WebUI.getText(findTestObject('menu/report/transStatus/recordList/Label - debitAccountChecker'), FailureHandling.CONTINUE_ON_FAILURE)

String[] debitAccount_recordList2split = debitAccount_recordList2.split(' ')

String debitAccount_recordList2result = debitAccount_recordList2split[1]

product_recordList2 = WebUI.getText(findTestObject('menu/report/transStatus/recordList/Label - productChecker'), FailureHandling.CONTINUE_ON_FAILURE)

String[] product_recordList2split = product_recordList2.split(' ')

String product_recordList2result = product_recordList2split[1]

insMode_recordList2 = WebUI.getText(findTestObject('menu/report/transStatus/recordList/Label - insModeChecker'), FailureHandling.CONTINUE_ON_FAILURE)

String[] insMode_recordList2split = insMode_recordList2.split(' ')

String insMode_recordList2result = insMode_recordList2split[1]

expired_recordList2 = WebUI.getText(findTestObject('menu/report/transStatus/recordList/Label - expiredChecker'), FailureHandling.CONTINUE_ON_FAILURE)

String[] expired_recordList2split = expired_recordList2.split(' ')

String expired_recordList2result = ((((expired_recordList2split[1]) + ' ') + (expired_recordList2split[2])) + ' ') + (expired_recordList2split[
3])

ttr_recordList2 = WebUI.getText(findTestObject('menu/report/transStatus/recordList/Label - ttrChecker'), FailureHandling.CONTINUE_ON_FAILURE)

String[] ttr_recordList2split = ttr_recordList2.split(' ')

String ttr_recordList2result = ttr_recordList2split[1]

amount_recordList2 = WebUI.getText(findTestObject('menu/report/transStatus/recordList/Label - amountChecker'), FailureHandling.CONTINUE_ON_FAILURE)

String[] amount_recordList2split = amount_recordList2.split(' ')

String amount_recordList2result = amount_recordList2split[1]

counterRate_recordList2 = WebUI.getText(findTestObject('menu/report/transStatus/recordList/Label - exchangeRateChecker'), FailureHandling.CONTINUE_ON_FAILURE)

String[] counterRate_recordList2split = counterRate_recordList2.split(' ')

String counterRate_recordList2result = counterRate_recordList2split[1]

totalCharges_recordList2 = WebUI.getText(findTestObject('menu/report/transStatus/recordList/totalChargesChecker (skn.llg)'), 
    FailureHandling.CONTINUE_ON_FAILURE)

String[] totalCharges_recordList2split = totalCharges_recordList2.split(' ')

String totalCharges_recordList2result = totalCharges_recordList2split[1]

totalDebitAmount_recordList2 = WebUI.getText(findTestObject('menu/report/transStatus/recordList/totalDebitAmountChecker (skn.llg)'), 
    FailureHandling.CONTINUE_ON_FAILURE)

String[] totalDebitAmount_recordList2split = totalDebitAmount_recordList2.split(' ')

String totalDebitAmount_recordList2result = totalDebitAmount_recordList2split[1]

WebUI.verifyMatch(debitAccount_recordList2result, '000021942610', true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(product_recordList2result, 'SKN/LLG', true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(insMode_recordList2result, 'Immediate', true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(expired_recordList2result, GlobalVariable.expired, true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(ttr_recordList2result, '1', true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(amount_recordList2result, 'IDR575,000.00', true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(counterRate_recordList2result, 'Counter', true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(totalCharges_recordList2result, 'IDR30,000.00', true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(totalDebitAmount_recordList2result, 'IDR605,000.00', true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/report/transStatus/recordList/recordListClose'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(10, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/report/transStatus/transStatus_2/transStatusClick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(10, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/BTN - logoutClick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(50, FailureHandling.CONTINUE_ON_FAILURE)

