import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.setText(findTestObject('homelogin/homelogin.corporateid'), 'aprismasm2')

WebUI.setText(findTestObject('homelogin/homelogin.userid'), 'mobile')

WebUI.setText(findTestObject('homelogin/homelogin.password'), 'Password123')

WebUI.click(findTestObject('homelogin/homelogin.submit'))

WebUI.waitForElementVisible(findTestObject('menu/BTN - clickMenu'), 0)

WebUI.delay(4)

WebUI.click(findTestObject('menu/BTN - clickMenu'))

WebUI.delay(2)

WebUI.click(findTestObject('menu/trfManagement/BTN - clickTrfManagement'))

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/BTN - clickBulkTrf'))

WebUI.waitForElementVisible(findTestObject('menu/trfManagement/bulkTransfer/DL - trfFormClick'), 0)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/DL - trfFormClick'))

WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Value - textfieldTrfForm'), '000008716771')

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/trfFormChoice(rtgs)'))

WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Value - textfieldAccDesc'), 'testings')

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/BTN - debitChargeClick'))

WebUI.scrollToElement(findTestObject('menu/trfManagement/bulkTransfer/Checkbox - clickTerm'), 0)

WebUI.delay(5)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/Checkbox - clickTerm'))

WebUI.delay(2)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/BTN - continueButton'))

WebUI.delay(10)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/product/productClick (rtgs)'))

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/productChoice(4)'))

WebUI.delay(6)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/DL - beneficiaryTypeClick'))

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/invidiual'))

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/DL - beneficiaryListClick'))

WebUI.delay(2)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/beneficiaryList/beneficiaryList(Hidas - 123) - skn.rrg'))

WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Value - troToDescription'), 'testbraay2')

WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Value - beneficiaryReferenceNumber'), '100797')

WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Value - amount'), '575000')

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/DL - beneClick'))

WebUI.delay(3)

WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Value - textfieldBene'), 'pemerintah')

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/Value - beneCategoryChoice'))

WebUI.delay(2)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/DL - relationshipClick'))

WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Value - textfieldBene'), 'Affiliated')

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/BTN - relationshipChoice'))

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/DL - pofClick'))

WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Value - textfieldBene'), 'Ekspor barang')

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/BTN - pofChoice'))

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/DL - codeClick '))

WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Value - textfieldBene'), 'Investasi Penyertaan Langsung')

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/BTN - codeChoice'))

WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Value - lhbuDocumentTypeDescription'), 'Document Bray')

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/DL - documentTypeClick'))

WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Value - textfieldBene'), 'Fotokopi Pemberitahuan Impor')

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/Value - documentTypeChoice'))

WebUI.scrollToElement(findTestObject('menu/trfManagement/bulkTransfer/BTN - confirm'), 0)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/BTN - confirm'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(7)

trfFormChecker = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/checker/Label - trfFormChecker'))

String[] trfFormSplit = trfFormChecker.split(' ')

String trfFormResult = trfFormSplit[1]

GlobalVariable.trfForm = trfFormResult

WebUI.verifyMatch(GlobalVariable.trfForm, '000008716771', true)

accDescChecker = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/checker/Label - accDescriptionChecker'))

String[] accDescSplit = accDescChecker.split(' ')

String accDescResult = accDescSplit[1]

GlobalVariable.accDesc = accDescResult

WebUI.verifyMatch(GlobalVariable.accDesc, 'testings', true)

debitChargeChecker = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/checker/Label - debitChargeChecker'))

String[] debitChargeSplit = debitChargeChecker.split(' ')

String debitChargeResult = debitChargeSplit[1]

GlobalVariable.debitCharge = debitChargeResult

WebUI.verifyMatch(GlobalVariable.debitCharge, 'Split', true)

insModeChecker = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/checker/Label - insModeChecker'))

String[] insModeSplit = insModeChecker.split(' ')

String insModeResult = insModeSplit[1]

GlobalVariable.insMode = insModeResult

WebUI.verifyMatch(GlobalVariable.insMode, 'Immediate', true)

trfTypeChecker = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/checker/Label - trfTypeChecker'))

String[] trfTypeSplit = trfTypeChecker.split(' ')

String trfTypeResult = trfTypeSplit[1]

GlobalVariable.trfType = trfTypeResult

WebUI.verifyMatch(GlobalVariable.trfType, 'Detail', true)

expiredChecker = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/checker/Label - expiredChecker'))

String[] expiredSplit = expiredChecker.split(' ')

String expiredResult = ((((expiredSplit[1]) + ' ') + (expiredSplit[2])) + ' ') + (expiredSplit[3])

GlobalVariable.expired = expiredResult

WebUI.verifyMatch(GlobalVariable.expired, GlobalVariable.expired, true)

productChecker = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/checker/Label - productChecker'))

String[] productSplit = productChecker.split(' ')

String productResult = productSplit[1]

GlobalVariable.product = productResult

WebUI.verifyMatch(GlobalVariable.product, 'SKN/LLG', true)

amountChecker = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/checker/Label - amountChecker'))

String[] amountSplit = amountChecker.split(' ')

String amountResult = amountSplit[1]

amountResult = amountResult.replace('IDR', '')

amountResult = amountResult.replace(',', '')

amountResult = amountResult.replace('.00', '')

GlobalVariable.amount = amountResult

WebUI.verifyMatch(GlobalVariable.amount, '575000', true)

WebUI.delay(5)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/Label - moreRecordsClick'))

WebUI.delay(7)

product_recordList = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/Td - product'))

beneAccName_recordList = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/Td - beneAccountName'))

beneBank_recordList = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/Td - beneBank'))

amount_recordList = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/Td - amount'))

exchangeRate_recordList = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/Td - exchangeRate'))

WebUI.verifyMatch(product_recordList, 'SKN/LLG', false)

not_run: WebUI.verifyMatch(beneAccName_recordList, 'Hildas', true)

WebUI.verifyMatch(beneBank_recordList, '0139926 - Permata Syariah', true)

WebUI.verifyMatch(amount_recordList, 'IDR 575,000.00', true)

WebUI.verifyMatch(exchangeRate_recordList, 'Counter Rate', true)

WebUI.delay(5)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/recordList/Td - beneAccountNoClick'))

WebUI.delay(10)

trfFrom_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Label - trfFrom'))

String[] trfFrom_detailRecordSplit = trfFrom_detailRecord.split(' ')

trfFrom_detailRecord = (trfFrom_detailRecordSplit[1])

accDescription_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Label - fromAccDescription'))

debitCharge_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Label - debitCharge'))

transType_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Label - transType'))

insMode_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Label - insMode'))

expired_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Label - expired'))

product_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/product - rtgs'))

trfTo_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/trfTo (rtgs)'))

beneType_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Label - beneType'))

beneList_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Label - beneList'))

toAccDescription_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/accDescription - (Inter) - To'))

beneRefNumber_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/beneRefNumber (Inter)'))

amount_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/amount (rtgs)'))

exchangeRate_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/exchangeRate (rtgs) '))

chargeInstruction_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/changeInstruction(rtgs)'))

totalCharge_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/totalCharge (rtgs)'))

totalDebitAmount_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/totalAmount (rtgs)'))

residentStatus_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/residentStatus (rtgs) '))

beneCategory_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Categories/beneCategory - rtgs'))

transRelationship_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Categories/transRelationship - rtgs'))

identicalStatus_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Categories/identicalStatus - rtgs'))

PoT_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Categories/PoT - (rtgs)'))

purposeCode_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Categories/purposeCode (rtgs)'))

docType_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Categories/docType - (rtgs)'))

docDesc_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Categories/docDesc - (RTGS)'))

WebUI.verifyMatch(trfFrom_detailRecord, GlobalVariable.trfForm, true)

WebUI.verifyMatch(accDescription_detailRecord, ': testings', true)

WebUI.verifyMatch(debitCharge_detailRecord, ': Split', true)

WebUI.verifyMatch(transType_detailRecord, ': Detail', true)

WebUI.verifyMatch(insMode_detailRecord, ': Immediate', true)

WebUI.verifyMatch(expired_detailRecord, expiredChecker, true)

WebUI.verifyMatch(product_detailRecord, ': SKN/LLG', true)

WebUI.verifyMatch(trfTo_detailRecord, ': Beneficiary List', true)

WebUI.verifyMatch(beneType_detailRecord, ': Individual', true)

not_run: WebUI.verifyMatch(beneList_detailRecord, ': 003400008464 - az', true)

WebUI.verifyMatch(toAccDescription_detailRecord, ': testbraay2', true)

WebUI.verifyMatch(beneRefNumber_detailRecord, ': 100797', true)

WebUI.verifyMatch(amount_detailRecord, ': IDR575,000.00', true)

WebUI.verifyMatch(exchangeRate_detailRecord, ': Counter Rate', true)

WebUI.verifyMatch(chargeInstruction_detailRecord, ': Remitter', true)

WebUI.verifyMatch(totalCharge_detailRecord, ': IDR   30,000.00', true)

WebUI.verifyMatch(totalDebitAmount_detailRecord, ': IDR   605,000.00', true)

WebUI.verifyMatch(residentStatus_detailRecord, ': Resident', true)

WebUI.verifyMatch(beneCategory_detailRecord, ': 3 - Pemerintah', true)

WebUI.verifyMatch(transRelationship_detailRecord, ': A - Affiliated', true)

WebUI.verifyMatch(identicalStatus_detailRecord, ': Identical', true)

WebUI.verifyMatch(PoT_detailRecord, ': 2011 - Ekspor barang', true)

WebUI.verifyMatch(purposeCode_detailRecord, ': 00 - 00 Investasi Penyertaan Langsung', true)

WebUI.verifyMatch(docType_detailRecord, ': 001 - 001 Fotokopi Pemberitahuan Impor Barang PIB', true)

WebUI.verifyMatch(docDesc_detailRecord, ': Document Bray', true)

WebUI.delay(5)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/BTN - detailClose'))

WebUI.delay(5)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/recordList/BTN - recordListClose'))

WebUI.delay(2)

WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/responseCode'), '123456')

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/BTN - btnSubmit'))

WebUI.delay(2)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/BTN - submitEnding'))

WebUI.delay(100)

refCode = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/checker/Label - refNumChecker'))

String[] refCodeSplit = refCode.split(' ')

String refCodeResult = refCodeSplit[2]

GlobalVariable.refNo2 = refCodeResult

WebUI.delay(10)

WebUI.click(findTestObject('menu/BTN - clickMenu'))

WebUI.delay(5)

WebUI.scrollToElement(findTestObject('menu/report/reportCLick'), 0)

WebUI.click(findTestObject('menu/report/reportCLick'))

WebUI.waitForElementVisible(findTestObject('menu/report/transStatus/Td - transStatusClick'), 0)

WebUI.click(findTestObject('menu/report/transStatus/Td - transStatusClick'))

WebUI.delay(7)

WebUI.click(findTestObject('menu/report/transStatus/rbTransactionReferenceNo/rbTransactionReferenceNo'))

WebUI.setText(findTestObject('menu/report/transStatus/referenceTransaction/refTextfield'), GlobalVariable.refNo2)

WebUI.click(findTestObject('menu/report/transStatus/btnShow/btnShow'))

WebUI.delay(7)

WebUI.click(findTestObject('menu/report/transStatus/referenceTransaction/refClick'))

WebUI.delay(7)

WebUI.verifyElementPresent(findTestObject('test'), 0)

CustomKeywords.'get.Screencapture.getEntirePage'('test.jpg')

refCodeChecker2 = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/checker/refNo3Checker'))

String[] refCodeSplit2 = refCodeChecker2.split(' ')

String refCodeResult2 = refCodeSplit2[1]

GlobalVariable.refNo3 = refCodeResult2

WebUI.verifyMatch(GlobalVariable.refNo3, GlobalVariable.refNo2, false)

menuChecker2 = WebUI.getText(findTestObject('menu/report/transStatus/checker(bulkTransfer)/Label - menuChecker'), FailureHandling.STOP_ON_FAILURE)

productChecker2 = WebUI.getText(findTestObject('menu/report/transStatus/checker(bulkTransfer)/Label - productChecker'), FailureHandling.STOP_ON_FAILURE)

String[] productChecker2Split = productChecker2.split(' ')

String productChecker2result = productChecker2Split[1]

trfFromChecker2 = WebUI.getText(findTestObject('menu/report/transStatus/checker(bulkTransfer)/Label - trfFromChecker'), FailureHandling.STOP_ON_FAILURE)

String[] trfFromChecker2split = trfFromChecker2.split(' ')

String trfFromChecker2result = trfFromChecker2split[1]

accDescriptionChecker2 = WebUI.getText(findTestObject('menu/report/transStatus/checker(bulkTransfer)/Label - fromAccDescriptionChecker'), FailureHandling.STOP_ON_FAILURE)

String[] accDescriptionChecker2split = accDescriptionChecker2.split(' ')

String accDescriptionChecker2result = accDescriptionChecker2split[1]

debitChargeChecker2 = WebUI.getText(findTestObject('menu/report/transStatus/checker(bulkTransfer)/Label - debitChargeChecker'), FailureHandling.STOP_ON_FAILURE)

String[] debitChargeChecker2split = debitChargeChecker2.split(' ')

String debitChargeChecker2result = debitChargeChecker2split[1]

transTypeChecker2 = WebUI.getText(findTestObject('menu/report/transStatus/checker(bulkTransfer)/Label - transTypeChecker'), FailureHandling.STOP_ON_FAILURE)

String[] transTypeChecker2split = transTypeChecker2.split(' ')

String transTypeChecker2result = transTypeChecker2split[1]

insModeChecker2 = WebUI.getText(findTestObject('menu/report/transStatus/checker(bulkTransfer)/Label - insModeChecker'), FailureHandling.STOP_ON_FAILURE)

String[] insModeChecker2split = insModeChecker2.split(' ')

String insModeChecker2result = insModeChecker2split[1]

expiredChecker2 = WebUI.getText(findTestObject('menu/report/transStatus/checker(bulkTransfer)/Label - expiredChecker'), FailureHandling.STOP_ON_FAILURE)

String[] expiredChecker2split = expiredChecker2.split(' ')

String expiredChecker2result = ((((expiredChecker2split[1]) + ' ') + (expiredChecker2split[2])) + ' ') + (expiredChecker2split[
3])

amountChecker2 = WebUI.getText(findTestObject('menu/report/transStatus/checker(bulkTransfer)/Label - amountChecker'), FailureHandling.STOP_ON_FAILURE)

String[] amountChecker2split = amountChecker2.split(' ')

String amountChecker2result = amountChecker2split[1]

totalDebitAmountChecker2 = WebUI.getText(findTestObject('menu/report/transStatus/checker(bulkTransfer)/totalDebitAmount (rtgs)'), FailureHandling.STOP_ON_FAILURE)

String[] totalDebitAmountchecker2split = totalDebitAmountChecker2.split(' ')

String totalDebitAmountchecker2result = totalDebitAmountchecker2split[1]

not_run: WebUI.verifyMatch(transStatusChecker2, ': Executed Successfully', true)

WebUI.verifyMatch(menuChecker2, ': Bulk Transfer', true)

WebUI.verifyMatch(productChecker2result, 'SKN/LLG', true)

WebUI.verifyMatch(trfFromChecker2result, '000008716771', true)

WebUI.verifyMatch(accDescriptionChecker2result, accDescResult, true)

WebUI.verifyMatch(debitChargeChecker2result, debitChargeResult, true)

WebUI.verifyMatch(transTypeChecker2result, trfTypeResult, true)

WebUI.verifyMatch(insModeChecker2result, insModeResult, true)

WebUI.verifyMatch(expiredChecker2result, GlobalVariable.expired, true)

WebUI.verifyMatch(amountChecker2result, 'IDR575,000.00', true)

WebUI.verifyMatch(totalDebitAmountchecker2result, 'IDR605,000.00', true)

WebUI.delay(5)

WebUI.click(findTestObject('menu/report/transStatus/recordList/BTN - recordListClick'))

WebUI.delay(5)

debitAccount_recordList2 = WebUI.getText(findTestObject('menu/report/transStatus/recordList/Label - debitAccountChecker'))

String[] debitAccount_recordList2split = debitAccount_recordList2.split(' ')

String debitAccount_recordList2result = debitAccount_recordList2split[1]

product_recordList2 = WebUI.getText(findTestObject('menu/report/transStatus/recordList/Label - productChecker'))

String[] product_recordList2split = product_recordList2.split(' ')

String product_recordList2result = product_recordList2split[1]

insMode_recordList2 = WebUI.getText(findTestObject('menu/report/transStatus/recordList/Label - insModeChecker'))

String[] insMode_recordList2split = insMode_recordList2.split(' ')

String insMode_recordList2result = insMode_recordList2split[1]

expired_recordList2 = WebUI.getText(findTestObject('menu/report/transStatus/recordList/Label - expiredChecker'))

String[] expired_recordList2split = expired_recordList2.split(' ')

String expired_recordList2result = ((((expired_recordList2split[1]) + ' ') + (expired_recordList2split[2])) + ' ') + (expired_recordList2split[
3])

ttr_recordList2 = WebUI.getText(findTestObject('menu/report/transStatus/recordList/Label - ttrChecker'))

String[] ttr_recordList2split = ttr_recordList2.split(' ')

String ttr_recordList2result = ttr_recordList2split[1]

amount_recordList2 = WebUI.getText(findTestObject('menu/report/transStatus/recordList/Label - amountChecker'))

String[] amount_recordList2split = amount_recordList2.split(' ')

String amount_recordList2result = amount_recordList2split[1]

counterRate_recordList2 = WebUI.getText(findTestObject('menu/report/transStatus/recordList/Label - exchangeRateChecker'))

String[] counterRate_recordList2split = counterRate_recordList2.split(' ')

String counterRate_recordList2result = counterRate_recordList2split[1]

totalCharges_recordList2 = WebUI.getText(findTestObject('menu/report/transStatus/recordList/totalChargesChecker (rtgs)'))

String[] totalCharges_recordList2split = totalCharges_recordList2.split(' ')

String totalCharges_recordList2result = totalCharges_recordList2split[1]

totalDebitAmount_recordList2 = WebUI.getText(findTestObject('menu/report/transStatus/recordList/totalDebitAmountChecker (rtgs)'))

String[] totalDebitAmount_recordList2split = totalDebitAmount_recordList2.split(' ')

String totalDebitAmount_recordList2result = totalDebitAmount_recordList2split[1]

WebUI.verifyMatch(debitAccount_recordList2result, '000008716771', true)

WebUI.verifyMatch(product_recordList2result, 'SKN/LLG', true)

WebUI.verifyMatch(insMode_recordList2result, 'Immediate', true)

WebUI.verifyMatch(expired_recordList2result, GlobalVariable.expired, true)

WebUI.verifyMatch(ttr_recordList2result, '1', true)

WebUI.verifyMatch(amount_recordList2result, 'IDR575,000.00', true)

WebUI.verifyMatch(counterRate_recordList2result, 'Counter', true)

WebUI.verifyMatch(totalCharges_recordList2result, 'IDR30,000.00', true)

WebUI.verifyMatch(totalDebitAmount_recordList2result, 'IDR605,000.00', true)

WebUI.click(findTestObject('menu/report/transStatus/recordList/recordListClose'))

WebUI.delay(100)

WebUI.click(findTestObject('menu/report/transStatus/transStatus_2/transStatusClick'))

WebUI.delay(15)

not_run: WebUI.verifyMatch(transStatusChecker2, ': Executed Successfully', true)

WebUI.verifyMatch(menuChecker2, ': Bulk Transfer', true)

WebUI.verifyMatch(productChecker2result, 'SKN/LLG', true)

WebUI.verifyMatch(trfFromChecker2result, '000008716771', true)

WebUI.verifyMatch(accDescriptionChecker2result, accDescResult, true)

WebUI.verifyMatch(debitChargeChecker2result, debitChargeResult, true)

WebUI.verifyMatch(transTypeChecker2result, trfTypeResult, true)

WebUI.verifyMatch(insModeChecker2result, insModeResult, true)

WebUI.verifyMatch(expiredChecker2result, expiredResult, true)

WebUI.verifyMatch(amountChecker2result, 'IDR575,000.00', true)

WebUI.verifyMatch(totalDebitAmountchecker2result, 'IDR605,000.00', true)

WebUI.delay(7)

WebUI.click(findTestObject('menu/report/transStatus/recordList/BTN - recordListClick'))

WebUI.delay(7)

WebUI.verifyMatch(debitAccount_recordList2result, '000008716771', true)

WebUI.verifyMatch(product_recordList2result, 'SKN/LLG', true)

WebUI.verifyMatch(insMode_recordList2result, 'Immediate', true)

WebUI.verifyMatch(expired_recordList2result, GlobalVariable.expired, true)

WebUI.verifyMatch(ttr_recordList2result, '1', true)

WebUI.verifyMatch(amount_recordList2result, 'IDR575,000.00', true)

WebUI.verifyMatch(counterRate_recordList2result, 'Counter', true)

WebUI.verifyMatch(totalCharges_recordList2result, 'IDR30,000.00', true)

WebUI.verifyMatch(totalDebitAmount_recordList2result, 'IDR605,000.00', true)

WebUI.delay(7)

WebUI.click(findTestObject('menu/report/transStatus/recordList/recordListClose'))

WebUI.delay(100)

WebUI.click(findTestObject('menu/BTN - logoutClick'))

WebUI.delay(50)

