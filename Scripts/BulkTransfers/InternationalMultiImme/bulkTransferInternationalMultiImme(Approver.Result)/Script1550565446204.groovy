import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.interactions.Actions as Actions
import java.util.Random as Random

//Approver Result Screen

menuChecker = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(bulkTransfer)/Label - menuChecker'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('menuChecker', menuChecker)

WebUI.verifyMatch(menuChecker, GlobalVariable.confirmScreens.get('menuChecker'), true, FailureHandling.CONTINUE_ON_FAILURE)

productChecker = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(bulkTransfer)/Label - productChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(productChecker, GlobalVariable.confirmScreens.get('productChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

transRefNumberChecker = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(bulkTransfer)/Label - transRefNumberChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(transRefNumberChecker, ': ' + GlobalVariable.confirmScreens.get('refNum'), true, FailureHandling.CONTINUE_ON_FAILURE)

docNoChecker = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(bulkTransfer)/Label - DocumentCode'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(docNoChecker, ': ' + GlobalVariable.confirmScreens.get('docNo'), true, FailureHandling.CONTINUE_ON_FAILURE)

submitDateChecker = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(bulkTransfer)/Label - submitDateChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(submitDateChecker, ': ' + GlobalVariable.confirmScreens.get('submitDate'), false, FailureHandling.CONTINUE_ON_FAILURE)

trfFromChecker = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(bulkTransfer)/Label - trfFormChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(trfFromChecker, GlobalVariable.confirmScreens.get('trfFromChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

accDescChecker = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(bulkTransfer)/Label - fromAccDescChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(accDescChecker, GlobalVariable.confirmScreens.get('accDescChecker'), true, FailureHandling.CONTINUE_ON_FAILURE)

debitChargeChecker = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(bulkTransfer)/Label - debitChargeChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(debitChargeChecker, GlobalVariable.confirmScreens.get('debitChargeChecker'), true, FailureHandling.CONTINUE_ON_FAILURE)

transTypeChecker = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(bulkTransfer)/Label - transTypeChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(transTypeChecker, GlobalVariable.confirmScreens.get('trfTypeChecker'), true, FailureHandling.CONTINUE_ON_FAILURE)

insModeChecker = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(bulkTransfer)/Label - insModeChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(insModeChecker, GlobalVariable.confirmScreens.get('insModeChecker'), true, FailureHandling.CONTINUE_ON_FAILURE)

expiredChecker = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(bulkTransfer)/Label - expiredChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(expiredChecker, GlobalVariable.confirmScreens.get('expiredChecker'), true, FailureHandling.CONTINUE_ON_FAILURE)

ttrChecker = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(bulkTransfer)/Label - ttr'), FailureHandling.CONTINUE_ON_FAILURE)

String[] ttrSplit = ttrChecker.split(' ')

WebUI.verifyMatch(ttrSplit[1], GlobalVariable.confirmScreens.get('ttrChecker'), true, FailureHandling.CONTINUE_ON_FAILURE)

amountChecker = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(bulkTransfer)/Label - amountChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(amountChecker, GlobalVariable.confirmScreens.get('amountChecker'), true, FailureHandling.CONTINUE_ON_FAILURE)

totalChargesChecker = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(bulkTransfer)/Label - totalChargesChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(totalChargesChecker, GlobalVariable.confirmScreens.get('totalChargesChecker'), true, FailureHandling.CONTINUE_ON_FAILURE)

totalDebitAmountChecker = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(bulkTransfer)/Label - totalDebitAmountChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(totalDebitAmountChecker, GlobalVariable.confirmScreens.get('totalDebitAmountChecker'), true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/myTask/pendingTask/entryScreen(bulkTransfer)/BTN - recordListClick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(3, FailureHandling.CONTINUE_ON_FAILURE)

allTables_recordList = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/Tbody - AllTables'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('allTables_recordList', allTables_recordList)

WebUI.verifyMatch(allTables_recordList, GlobalVariable.confirmScreens.get('allTables_recordList'), false, FailureHandling.CONTINUE_ON_FAILURE)

int count = 1

WebDriver driver = DriverFactory.getWebDriver()

Actions action = new Actions(driver)

while (count <= GlobalVariable.confirmScreens.get('Flag')) {
    driver.switchTo().frame('login')

    driver.switchTo().frame('seeRecordFrame')

    WebElement Table = driver.findElement(By.xpath(('//table[@id="globalTableTotalRcd"]/tbody/tr[' + count) + ']/td[3]/a'))

    action.click(Table).perform()

    WebUI.switchToDefaultContent(FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

    trfFrom_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Label - trfFrom'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    GlobalVariable.confirmScreens.put('trfFrom_detailRecord' + count, trfFrom_detailRecord)

    WebUI.verifyMatch(trfFrom_detailRecord, GlobalVariable.confirmScreens.get('trfFrom_detailRecord' + count), false, FailureHandling.CONTINUE_ON_FAILURE)

    accDescription_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Label - fromAccDescription'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    GlobalVariable.confirmScreens.put('accDescription_detailRecord' + count, accDescription_detailRecord)

    WebUI.verifyMatch(accDescription_detailRecord, GlobalVariable.confirmScreens.get('accDescription_detailRecord' + count), 
        false, FailureHandling.CONTINUE_ON_FAILURE)

    debitCharge_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Label - debitCharge'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    GlobalVariable.confirmScreens.put('debitCharge_detailRecord' + count, debitCharge_detailRecord)

    WebUI.verifyMatch(debitCharge_detailRecord, GlobalVariable.confirmScreens.get('debitCharge_detailRecord' + count), false, 
        FailureHandling.CONTINUE_ON_FAILURE)

    transType_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Label - transType'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    GlobalVariable.confirmScreens.put('transType_detailRecord' + count, transType_detailRecord)

    WebUI.verifyMatch(transType_detailRecord, GlobalVariable.confirmScreens.get('transType_detailRecord' + count), false, 
        FailureHandling.CONTINUE_ON_FAILURE)

    insMode_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Label - insMode'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    GlobalVariable.confirmScreens.put('insMode_detailRecord' + count, insMode_detailRecord)

    WebUI.verifyMatch(insMode_detailRecord, GlobalVariable.confirmScreens.get('insMode_detailRecord' + count), false, FailureHandling.CONTINUE_ON_FAILURE)

    expired_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Label - expired'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    GlobalVariable.confirmScreens.put('expired_detailRecord' + count, expired_detailRecord)

    WebUI.verifyMatch(expired_detailRecord, GlobalVariable.confirmScreens.get('expired_detailRecord' + count), false, FailureHandling.CONTINUE_ON_FAILURE)

    product_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Label - product'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    GlobalVariable.confirmScreens.put('product_detailRecord' + count, product_detailRecord)

    WebUI.verifyMatch(product_detailRecord, GlobalVariable.confirmScreens.get('product_detailRecord' + count), false, FailureHandling.CONTINUE_ON_FAILURE)

    trfTo_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Label - transferTo'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    GlobalVariable.confirmScreens.put('trfTo_detailRecord' + count, trfTo_detailRecord)

    WebUI.verifyMatch(trfTo_detailRecord, GlobalVariable.confirmScreens.get('trfTo_detailRecord' + count), false, FailureHandling.CONTINUE_ON_FAILURE)

    beneList_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Label - beneList'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    GlobalVariable.confirmScreens.put('beneList_detailRecord' + count, beneList_detailRecord)

    WebUI.verifyMatch(beneList_detailRecord, GlobalVariable.confirmScreens.get('beneList_detailRecord' + count), false, 
        FailureHandling.CONTINUE_ON_FAILURE)

    toAccDescription_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Label - toAccDescription'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    GlobalVariable.confirmScreens.put('toAccDescription_detailRecord' + count, toAccDescription_detailRecord)

    WebUI.verifyMatch(toAccDescription_detailRecord, GlobalVariable.confirmScreens.get('toAccDescription_detailRecord' + 
            count), false, FailureHandling.CONTINUE_ON_FAILURE)

    beneRefNo_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Label - beneRefNumber'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    GlobalVariable.confirmScreens.put('beneRefNo_detailRecord' + count, beneRefNo_detailRecord)

    WebUI.verifyMatch(beneRefNo_detailRecord, GlobalVariable.confirmScreens.get('beneRefNo_detailRecord' + count), false, 
        FailureHandling.CONTINUE_ON_FAILURE)

    remitCurrency_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Label - remitCurrency'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    GlobalVariable.confirmScreens.put('remitCurrency_detailRecord' + count, remitCurrency_detailRecord)

    WebUI.verifyMatch(remitCurrency_detailRecord, GlobalVariable.confirmScreens.get('remitCurrency_detailRecord' + count), 
        false, FailureHandling.CONTINUE_ON_FAILURE)

    amount_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Label - Amount'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    GlobalVariable.confirmScreens.put('amount_detailRecord' + count, amount_detailRecord)

    WebUI.verifyMatch(amount_detailRecord, GlobalVariable.confirmScreens.get('amount_detailRecord' + count), false, FailureHandling.CONTINUE_ON_FAILURE)

    exchangeRate_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Label - exchangeRate'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    GlobalVariable.confirmScreens.put('exchangeRate_detailRecord' + count, exchangeRate_detailRecord)

    WebUI.verifyMatch(exchangeRate_detailRecord, GlobalVariable.confirmScreens.get('exchangeRate_detailRecord' + count), 
        false, FailureHandling.CONTINUE_ON_FAILURE)

    not_run: changeInsruction_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Label - fullAmountCharge'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    not_run: GlobalVariable.confirmScreens.put('changeInsruction_detailRecord' + count, changeInsruction_detailRecord)

    not_run: WebUI.verifyMatch(changeInsruction_detailRecord, GlobalVariable.confirmScreens.get('changeInsruction_detailRecord' + 
            count), false, FailureHandling.CONTINUE_ON_FAILURE)

    bankFee_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Label - bankFee'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    GlobalVariable.confirmScreens.put('bankFee_detailRecord' + count, bankFee_detailRecord)

    WebUI.verifyMatch(bankFee_detailRecord, GlobalVariable.confirmScreens.get('bankFee_detailRecord' + count), false, FailureHandling.CONTINUE_ON_FAILURE)

    provision_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Label - Provision'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    GlobalVariable.confirmScreens.put('provision_detailRecord' + count, provision_detailRecord)

    WebUI.verifyMatch(provision_detailRecord, GlobalVariable.confirmScreens.get('provision_detailRecord' + count), false, 
        FailureHandling.CONTINUE_ON_FAILURE)

    minumunTransactionFee_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Label - minimumTransactionFee'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    GlobalVariable.confirmScreens.put('minumunTransactionFee_detailRecord' + count, minumunTransactionFee_detailRecord)

    WebUI.verifyMatch(minumunTransactionFee_detailRecord, GlobalVariable.confirmScreens.get('minumunTransactionFee_detailRecord' + 
            count), false, FailureHandling.CONTINUE_ON_FAILURE)

    inLieu_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Label - inLieu'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    GlobalVariable.confirmScreens.put('inLieu_detailRecord' + count, inLieu_detailRecord)

    WebUI.verifyMatch(inLieu_detailRecord, GlobalVariable.confirmScreens.get('inLieu_detailRecord' + count), false, FailureHandling.CONTINUE_ON_FAILURE)

    fullAmountCharge_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Label - fullAmountCharge'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    GlobalVariable.confirmScreens.put('fullAmountCharge_detailRecord' + count, fullAmountCharge_detailRecord)

    WebUI.verifyMatch(fullAmountCharge_detailRecord, GlobalVariable.confirmScreens.get('fullAmountCharge_detailRecord' + 
            count), false, FailureHandling.CONTINUE_ON_FAILURE)

    cableFee_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Label - cableFee'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    GlobalVariable.confirmScreens.put('cableFee_detailRecord' + count, cableFee_detailRecord)

    WebUI.verifyMatch(cableFee_detailRecord, GlobalVariable.confirmScreens.get('cableFee_detailRecord' + count), false, 
        FailureHandling.CONTINUE_ON_FAILURE)

    totalCharge_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Label - totalCharge'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    GlobalVariable.confirmScreens.put('totalCharge_detailRecord' + count, totalCharge_detailRecord)

    WebUI.verifyMatch(totalCharge_detailRecord, GlobalVariable.confirmScreens.get('totalCharge_detailRecord' + count), false, 
        FailureHandling.CONTINUE_ON_FAILURE)

    totalDebitAmount_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Label - totalDebitAmount(Inter)'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    GlobalVariable.confirmScreens.put('totalDebitAmount_detailRecord' + count, totalDebitAmount_detailRecord)

    WebUI.verifyMatch(totalDebitAmount_detailRecord, GlobalVariable.confirmScreens.get('totalDebitAmount_detailRecord' + 
            count), false, FailureHandling.CONTINUE_ON_FAILURE)

    beneCategory_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Categories/Label - beneCategory'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    GlobalVariable.confirmScreens.put('beneCategory_detailRecord' + count, beneCategory_detailRecord)

    WebUI.verifyMatch(beneCategory_detailRecord, GlobalVariable.confirmScreens.get('beneCategory_detailRecord' + count), 
        false, FailureHandling.CONTINUE_ON_FAILURE)

    transRelationship_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Categories/Label - transRelationship'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    GlobalVariable.confirmScreens.put('transRelationship_detailRecord' + count, transRelationship_detailRecord)

    WebUI.verifyMatch(transRelationship_detailRecord, GlobalVariable.confirmScreens.get('transRelationship_detailRecord' + 
            count), false, FailureHandling.CONTINUE_ON_FAILURE)

    identicalStatus_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Categories/Label - identicalStatus'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    GlobalVariable.confirmScreens.put('identicalStatus_detailRecord' + count, identicalStatus_detailRecord)

    WebUI.verifyMatch(identicalStatus_detailRecord, GlobalVariable.confirmScreens.get('identicalStatus_detailRecord' + count), 
        false, FailureHandling.CONTINUE_ON_FAILURE)

    PoT_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Categories/Label - PoT'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    GlobalVariable.confirmScreens.put('PoT_detailRecord' + count, PoT_detailRecord)

    WebUI.verifyMatch(PoT_detailRecord, GlobalVariable.confirmScreens.get('PoT_detailRecord' + count), false, FailureHandling.CONTINUE_ON_FAILURE)

    purpose__detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Categories/Label - purposeCode'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    GlobalVariable.confirmScreens.put('purpose__detailRecord' + count, purpose__detailRecord)

    WebUI.verifyMatch(purpose__detailRecord, GlobalVariable.confirmScreens.get('purpose__detailRecord' + count), false, 
        FailureHandling.CONTINUE_ON_FAILURE)

    docType_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Categories/Label - docType'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    GlobalVariable.confirmScreens.put('docType_detailRecord' + count, docType_detailRecord)

    WebUI.verifyMatch(docType_detailRecord, GlobalVariable.confirmScreens.get('docType_detailRecord' + count), false, FailureHandling.CONTINUE_ON_FAILURE)

    docDesc_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Categories/Label - docDesc'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    GlobalVariable.confirmScreens.put('docDesc_detailRecord' + count, docDesc_detailRecord)

    WebUI.verifyMatch(docDesc_detailRecord, GlobalVariable.confirmScreens.get('docDesc_detailRecord' + count), false, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/BTN - detailClose'), FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

    count++
}

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/recordList/BTN - recordListClose'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

