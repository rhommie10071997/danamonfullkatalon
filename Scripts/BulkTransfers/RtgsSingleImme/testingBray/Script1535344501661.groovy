import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.setText(findTestObject('homelogin/homelogin.corporateid'), 'aprismasm2')

WebUI.setText(findTestObject('homelogin/homelogin.userid'), 'mobile')

WebUI.setText(findTestObject('homelogin/homelogin.password'), 'Password123')

WebUI.click(findTestObject('homelogin/homelogin.submit'))

WebUI.waitForElementVisible(findTestObject('menu/BTN - clickMenu'), 0)

WebUI.delay(4)

WebUI.click(findTestObject('menu/BTN - clickMenu'))

WebUI.click(findTestObject('menu/BTN - clickMenu'))

WebUI.delay(5)

WebUI.scrollToElement(findTestObject('menu/report/reportCLick'), 0)

WebUI.click(findTestObject('menu/report/reportCLick'))

WebUI.waitForElementVisible(findTestObject('menu/report/transStatus/Td - transStatusClick'), 0)

WebUI.click(findTestObject('menu/report/transStatus/Td - transStatusClick'))

WebUI.delay(7)

WebUI.click(findTestObject('menu/report/transStatus/rbTransactionReferenceNo/rbTransactionReferenceNo'))

WebUI.setText(findTestObject('menu/report/transStatus/referenceTransaction/refTextfield'), '1805251957743926')

WebUI.click(findTestObject('menu/report/transStatus/btnShow/btnShow'))

WebUI.delay(7)

WebUI.click(findTestObject('menu/report/transStatus/referenceTransaction/refClick'))

WebUI.delay(7)

WebUI.verifyElementPresent(findTestObject('test'), 0)

CustomKeywords.'get.Screencapture.getEntirePage'('test.jpg')

refCodeChecker2 = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/checker/refNo3Checker'))

String[] refCodeSplit2 = refCodeChecker2.split(' ')

String refCodeResult2 = refCodeSplit2[1]

GlobalVariable.refNo3 = refCodeResult2

WebUI.verifyMatch(GlobalVariable.refNo3, GlobalVariable.refNo2, false)

transStatusChecker2 = WebUI.getText(findTestObject('menu/report/transStatus/checker(bulkTransfer)/Label - transStatusChecker'), FailureHandling.STOP_ON_FAILURE)

menuChecker2 = WebUI.getText(findTestObject('menu/report/transStatus/checker(bulkTransfer)/Label - menuChecker'), FailureHandling.STOP_ON_FAILURE)

productChecker2 = WebUI.getText(findTestObject('menu/report/transStatus/checker(bulkTransfer)/Label - productChecker'), FailureHandling.STOP_ON_FAILURE)

String[] productChecker2Split = productChecker2.split(' ')

String productChecker2result = productChecker2Split[1]

trfFromChecker2 = WebUI.getText(findTestObject('menu/report/transStatus/checker(bulkTransfer)/Label - trfFromChecker'), FailureHandling.STOP_ON_FAILURE)

String[] trfFromChecker2split = trfFromChecker2.split(' ')

String trfFromChecker2result = trfFromChecker2split[1]

accDescriptionChecker2 = WebUI.getText(findTestObject('menu/report/transStatus/checker(bulkTransfer)/Label - fromAccDescriptionChecker'), FailureHandling.STOP_ON_FAILURE)

String[] accDescriptionChecker2split = accDescriptionChecker2.split(' ')

String accDescriptionChecker2result = accDescriptionChecker2split[1]

debitChargeChecker2 = WebUI.getText(findTestObject('menu/report/transStatus/checker(bulkTransfer)/Label - debitChargeChecker'), FailureHandling.STOP_ON_FAILURE)

String[] debitChargeChecker2split = debitChargeChecker2.split(' ')

String debitChargeChecker2result = debitChargeChecker2split[1]

transTypeChecker2 = WebUI.getText(findTestObject('menu/report/transStatus/checker(bulkTransfer)/Label - transTypeChecker'), FailureHandling.STOP_ON_FAILURE)

String[] transTypeChecker2split = transTypeChecker2.split(' ')

String transTypeChecker2result = transTypeChecker2split[1]

insModeChecker2 = WebUI.getText(findTestObject('menu/report/transStatus/checker(bulkTransfer)/Label - insModeChecker'), FailureHandling.STOP_ON_FAILURE)

String[] insModeChecker2split = insModeChecker2.split(' ')

String insModeChecker2result = insModeChecker2split[1]

expiredChecker2 = WebUI.getText(findTestObject('menu/report/transStatus/checker(bulkTransfer)/Label - expiredChecker'), FailureHandling.STOP_ON_FAILURE)

String[] expiredChecker2split = expiredChecker2.split(' ')

String expiredChecker2result = ((((expiredChecker2split[1]) + ' ') + (expiredChecker2split[2])) + ' ') + (expiredChecker2split[
3])

amountChecker2 = WebUI.getText(findTestObject('menu/report/transStatus/checker(bulkTransfer)/Label - amountChecker'), FailureHandling.STOP_ON_FAILURE)

String[] amountChecker2split = amountChecker2.split(' ')

String amountChecker2result = amountChecker2split[1]

totalDebitAmountChecker2 = WebUI.getText(findTestObject('menu/report/transStatus/checker(bulkTransfer)/Label - totalDebitAmount'), FailureHandling.STOP_ON_FAILURE)

String[] totalDebitAmountchecker2split = totalDebitAmountChecker2.split(' ')

String totalDebitAmountchecker2result = totalDebitAmountchecker2split[1]

WebUI.verifyMatch(transStatusChecker2, ': Executed Successfully', true)

WebUI.verifyMatch(menuChecker2, ': Bulk Transfer', true)

WebUI.verifyMatch(productChecker2result, 'In-House', true)

WebUI.verifyMatch(trfFromChecker2result, '000001099266', true)

WebUI.verifyMatch(accDescriptionChecker2result, accDescResult, true)

WebUI.verifyMatch(debitChargeChecker2result, debitChargeResult, true)

WebUI.verifyMatch(transTypeChecker2result, trfTypeResult, true)

WebUI.verifyMatch(insModeChecker2result, insModeResult, true)

WebUI.verifyMatch(expiredChecker2result, GlobalVariable.expired, true)

WebUI.verifyMatch(amountChecker2result, 'IDR575,000.00', true)

WebUI.verifyMatch(totalDebitAmountchecker2result, 'IDR590,000.00', true)

WebUI.delay(5)

WebUI.click(findTestObject('menu/report/transStatus/recordList/BTN - recordListClick'))

WebUI.delay(5)

debitAccount_recordList2 = WebUI.getText(findTestObject('menu/report/transStatus/recordList/Label - debitAccountChecker'))

String[] debitAccount_recordList2split = debitAccount_recordList2.split(' ')

String debitAccount_recordList2result = debitAccount_recordList2split[1]

product_recordList2 = WebUI.getText(findTestObject('menu/report/transStatus/recordList/Label - productChecker'))

String[] product_recordList2split = product_recordList2.split(' ')

String product_recordList2result = product_recordList2split[1]

insMode_recordList2 = WebUI.getText(findTestObject('menu/report/transStatus/recordList/Label - insModeChecker'))

String[] insMode_recordList2split = insMode_recordList2.split(' ')

String insMode_recordList2result = insMode_recordList2split[1]

expired_recordList2 = WebUI.getText(findTestObject('menu/report/transStatus/recordList/Label - expiredChecker'))

String[] expired_recordList2split = expired_recordList2.split(' ')

String expired_recordList2result = ((((expired_recordList2split[1]) + ' ') + (expired_recordList2split[2])) + ' ') + (expired_recordList2split[
3])

ttr_recordList2 = WebUI.getText(findTestObject('menu/report/transStatus/recordList/Label - ttrChecker'))

String[] ttr_recordList2split = ttr_recordList2.split(' ')

String ttr_recordList2result = ttr_recordList2split[1]

amount_recordList2 = WebUI.getText(findTestObject('menu/report/transStatus/recordList/Label - amountChecker'))

String[] amount_recordList2split = amount_recordList2.split(' ')

String amount_recordList2result = amount_recordList2split[1]

counterRate_recordList2 = WebUI.getText(findTestObject('menu/report/transStatus/recordList/Label - exchangeRateChecker'))

String[] counterRate_recordList2split = counterRate_recordList2.split(' ')

String counterRate_recordList2result = counterRate_recordList2split[1]

totalCharges_recordList2 = WebUI.getText(findTestObject('menu/report/transStatus/recordList/totalChargesChecker'))

String[] totalCharges_recordList2split = totalCharges_recordList2.split(' ')

String totalCharges_recordList2result = totalCharges_recordList2split[1]

totalDebitAmount_recordList2 = WebUI.getText(findTestObject('menu/report/transStatus/recordList/Label - totalDebitAmountChecker'))

String[] totalDebitAmount_recordList2split = totalDebitAmount_recordList2.split(' ')

String totalDebitAmount_recordList2result = totalDebitAmount_recordList2split[1]

WebUI.verifyMatch(debitAccount_recordList2result, '000001099266', true)

WebUI.verifyMatch(product_recordList2result, 'In-House', true)

WebUI.verifyMatch(insMode_recordList2result, 'Immediate', true)

WebUI.verifyMatch(expired_recordList2result, GlobalVariable.expired, true)

WebUI.verifyMatch(ttr_recordList2result, '1', true)

WebUI.verifyMatch(amount_recordList2result, 'IDR575,000.00', true)

WebUI.verifyMatch(counterRate_recordList2result, 'Counter', true)

WebUI.verifyMatch(totalCharges_recordList2result, 'IDR15,000.00', true)

WebUI.verifyMatch(totalDebitAmount_recordList2result, 'IDR590,000.00', true)

WebUI.click(findTestObject('menu/report/transStatus/recordList/recordListClose'))

WebUI.delay(2)

WebUI.click(findTestObject('menu/report/transStatus/transStatus_2/transStatusClick'))

WebUI.delay(15)

WebUI.verifyMatch(transStatusChecker2, ': Executed Successfully', true)

WebUI.verifyMatch(menuChecker2, ': Bulk Transfer', true)

WebUI.verifyMatch(productChecker2result, 'In-House', true)

WebUI.verifyMatch(trfFromChecker2result, '000001099266', true)

WebUI.verifyMatch(accDescriptionChecker2result, accDescResult, true)

WebUI.verifyMatch(debitChargeChecker2result, debitChargeResult, true)

WebUI.verifyMatch(transTypeChecker2result, trfTypeResult, true)

WebUI.verifyMatch(insModeChecker2result, insModeResult, true)

WebUI.verifyMatch(expiredChecker2result, expiredResult, true)

WebUI.verifyMatch(amountChecker2result, 'IDR575,000.00', true)

WebUI.verifyMatch(totalDebitAmountchecker2result, 'IDR590,000.00', true)

WebUI.delay(7)

WebUI.click(findTestObject('menu/report/transStatus/recordList/BTN - recordListClick'))

WebUI.delay(7)

WebUI.verifyMatch(debitAccount_recordList2result, '000001099266', true)

WebUI.verifyMatch(product_recordList2result, 'In-House', true)

WebUI.verifyMatch(insMode_recordList2result, 'Immediate', true)

WebUI.verifyMatch(expired_recordList2result, GlobalVariable.expired, true)

WebUI.verifyMatch(ttr_recordList2result, '1', true)

WebUI.verifyMatch(amount_recordList2result, 'IDR575,000.00', true)

WebUI.verifyMatch(counterRate_recordList2result, 'Counter', true)

WebUI.verifyMatch(totalCharges_recordList2result, 'IDR15,000.00', true)

WebUI.verifyMatch(totalDebitAmount_recordList2result, 'IDR590,000.00', true)

WebUI.delay(7)

WebUI.click(findTestObject('menu/report/transStatus/recordList/recordListClose'))

WebUI.delay(5)

WebUI.click(findTestObject('menu/BTN - logoutClick'))

WebUI.delay(50)

