import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.interactions.Actions as Actions
import java.util.Random as Random

WebUI.delay(10, FailureHandling.CONTINUE_ON_FAILURE)

confirmScreenStatus = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/Label - confirmScreenStatus'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(confirmScreenStatus, 'Your transaction is waiting for approval', true, FailureHandling.CONTINUE_ON_FAILURE)

trfFromChecker = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/checker/Label - trfFormChecker'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(trfFromChecker, GlobalVariable.confirmScreens.get('trfFromChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

accDescChecker = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/checker/Label - accDescriptionChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(accDescChecker, GlobalVariable.confirmScreens.get('accDescChecker'), true, FailureHandling.CONTINUE_ON_FAILURE)

debitChargeChecker = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/checker/Label - debitChargeChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(debitChargeChecker, GlobalVariable.confirmScreens.get('debitChargeChecker'), true, FailureHandling.CONTINUE_ON_FAILURE)

trfTypeChecker = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/checker/Label - trfTypeChecker'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(trfTypeChecker, GlobalVariable.confirmScreens.get('trfTypeChecker'), true, FailureHandling.CONTINUE_ON_FAILURE)

insModeChecker = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/checker/Label - insModeChecker'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(insModeChecker, GlobalVariable.confirmScreens.get('insModeChecker'), true, FailureHandling.CONTINUE_ON_FAILURE)

futureDateChecker = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(bulkTransfer)/Label - futureDateChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(futureDateChecker, GlobalVariable.confirmScreens.get('futureDateChecker'), true, FailureHandling.CONTINUE_ON_FAILURE)

sessionChecker = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(bulkTransfer)/Label - sessionChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(sessionChecker, GlobalVariable.confirmScreens.get('sessionChecker'), true, FailureHandling.CONTINUE_ON_FAILURE)

expiredChecker = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/checker/Label - expiredChecker'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(expiredChecker, GlobalVariable.confirmScreens.get('expiredChecker'), true, FailureHandling.CONTINUE_ON_FAILURE)

productChecker = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/checker/Label - productChecker'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(productChecker, GlobalVariable.confirmScreens.get('productChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

amountChecker = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/checker/Label - amountChecker'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(amountChecker, GlobalVariable.confirmScreens.get('amountChecker'), true, FailureHandling.CONTINUE_ON_FAILURE)

ttrChecker = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/checker/Label - ttr'), FailureHandling.CONTINUE_ON_FAILURE)

String[] ttrSplit = ttrChecker.split(' ')

WebUI.verifyMatch(ttrSplit[1], GlobalVariable.confirmScreens.get('ttrChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

amountChecker = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/checker/Label - amountChecker'), FailureHandling.CONTINUE_ON_FAILURE)

exchangeRateChecker = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/checker/Label - exchangeRateChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

totalChargesChecker = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/checker/Label - totalChargesChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

totalDebitAmountChecker = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/checker/Label - totalDebitAmountChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(amountChecker, GlobalVariable.confirmScreens.get('amountChecker'), true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(exchangeRateChecker, GlobalVariable.confirmScreens.get('exchangeRateChecker'), true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(totalChargesChecker, GlobalVariable.confirmScreens.get('totalChargesChecker'), true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(totalDebitAmountChecker, GlobalVariable.confirmScreens.get('totalDebitAmountChecker'), true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/Label - moreRecordsClick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(7, FailureHandling.CONTINUE_ON_FAILURE)

allTables_recordList = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/Tbody - AllTables'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(allTables_recordList, GlobalVariable.confirmScreens.get('allTables_recordList'), false, FailureHandling.CONTINUE_ON_FAILURE)

int count = 1

WebDriver driver = DriverFactory.getWebDriver()

Actions action = new Actions(driver)

while (count <= GlobalVariable.confirmScreens.get('Flag')) {
    driver.switchTo().frame('login')

    driver.switchTo().frame('seeRecordFrame')

    WebElement Table = driver.findElement(By.xpath(('//table[@id="globalTableTotalRcd"]/tbody/tr[' + count) + ']/td[3]/a'))

    action.click(Table).perform()

    WebUI.switchToDefaultContent(FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

    trfFrom_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Label - trfFrom'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.verifyMatch(trfFrom_detailRecord, GlobalVariable.confirmScreens.get('trfFrom_detailRecord' + count), false, FailureHandling.CONTINUE_ON_FAILURE)

    accDescription_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Label - fromAccDescription'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.verifyMatch(accDescription_detailRecord, GlobalVariable.confirmScreens.get('accDescription_detailRecord' + count), 
        false, FailureHandling.CONTINUE_ON_FAILURE)

    debitCharge_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Label - debitCharge'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.verifyMatch(debitCharge_detailRecord, GlobalVariable.confirmScreens.get('debitCharge_detailRecord' + count), false, 
        FailureHandling.CONTINUE_ON_FAILURE)

    transType_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Label - transType'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.verifyMatch(transType_detailRecord, GlobalVariable.confirmScreens.get('transType_detailRecord' + count), false, 
        FailureHandling.CONTINUE_ON_FAILURE)

    insMode_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Label - insMode'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.verifyMatch(insMode_detailRecord, GlobalVariable.confirmScreens.get('insMode_detailRecord' + count), false, FailureHandling.CONTINUE_ON_FAILURE)

    futureDate_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Label - futureDateChecker'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.verifyMatch(futureDate_detailRecord, GlobalVariable.confirmScreens.get('futureDate_detailRecord' + count), false, 
        FailureHandling.CONTINUE_ON_FAILURE)

    session_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Label - sessionChecker'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.verifyMatch(session_detailRecord, GlobalVariable.confirmScreens.get('session_detailRecord' + count), false, FailureHandling.CONTINUE_ON_FAILURE)

    expired_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Label - expired'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.verifyMatch(expired_detailRecord, GlobalVariable.confirmScreens.get('expired_detailRecord' + count), false, FailureHandling.CONTINUE_ON_FAILURE)

    product_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Label - product'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.verifyMatch(product_detailRecord, GlobalVariable.confirmScreens.get('product_detailRecord' + count), false, FailureHandling.CONTINUE_ON_FAILURE)

    trfTo_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Label - transferTo'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.verifyMatch(trfTo_detailRecord, GlobalVariable.confirmScreens.get('trfTo_detailRecord' + count), false, FailureHandling.CONTINUE_ON_FAILURE)

    beneType_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Label - beneType'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.verifyMatch(beneType_detailRecord, GlobalVariable.confirmScreens.get('beneType_detailRecord' + count), false, 
        FailureHandling.CONTINUE_ON_FAILURE)

    beneList_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Label - beneList'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.verifyMatch(beneList_detailRecord, GlobalVariable.confirmScreens.get('beneList_detailRecord' + count), false, 
        FailureHandling.CONTINUE_ON_FAILURE)

    toAccDescription_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Label - toAccDescription'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.verifyMatch(toAccDescription_detailRecord, GlobalVariable.confirmScreens.get('toAccDescription_detailRecord' + 
            count), false, FailureHandling.CONTINUE_ON_FAILURE)

    beneRefNo_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Label - beneRefNumber'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.verifyMatch(beneRefNo_detailRecord, GlobalVariable.confirmScreens.get('beneRefNo_detailRecord' + count), false, 
        FailureHandling.CONTINUE_ON_FAILURE)

    amount_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Label - Amount'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.verifyMatch(amount_detailRecord, GlobalVariable.confirmScreens.get('amount_detailRecord' + count), false, FailureHandling.CONTINUE_ON_FAILURE)

    exchangeRate_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Label - exchangeRate'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.verifyMatch(exchangeRate_detailRecord, GlobalVariable.confirmScreens.get('exchangeRate_detailRecord' + count), 
        false, FailureHandling.CONTINUE_ON_FAILURE)

    changeInsruction_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Label - changeInstruction'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.verifyMatch(changeInsruction_detailRecord, GlobalVariable.confirmScreens.get('changeInsruction_detailRecord' + 
            count), false, FailureHandling.CONTINUE_ON_FAILURE)

    totalFee_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Label - totalFee'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.verifyMatch(totalFee_detailRecord, GlobalVariable.confirmScreens.get('totalFee_detailRecord' + count), false, 
        FailureHandling.CONTINUE_ON_FAILURE)

    totalCharge_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Label - totalCharge'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.verifyMatch(totalCharge_detailRecord, GlobalVariable.confirmScreens.get('totalCharge_detailRecord' + count), false, 
        FailureHandling.CONTINUE_ON_FAILURE)

    totalDebitAmount_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Label - totalDebitAmount'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.verifyMatch(totalDebitAmount_detailRecord, GlobalVariable.confirmScreens.get('totalDebitAmount_detailRecord' + 
            count), false, FailureHandling.CONTINUE_ON_FAILURE)

    beneCategory_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Categories/Label - beneCategory'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.verifyMatch(beneCategory_detailRecord, GlobalVariable.confirmScreens.get('beneCategory_detailRecord' + count), 
        false, FailureHandling.CONTINUE_ON_FAILURE)

    transRelationship_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Categories/Label - transRelationship'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.verifyMatch(transRelationship_detailRecord, GlobalVariable.confirmScreens.get('transRelationship_detailRecord' + 
            count), false, FailureHandling.CONTINUE_ON_FAILURE)

    identicalStatus_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Categories/Label - identicalStatus'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.verifyMatch(identicalStatus_detailRecord, GlobalVariable.confirmScreens.get('identicalStatus_detailRecord' + count), 
        false, FailureHandling.CONTINUE_ON_FAILURE)

    PoT_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Categories/Label - PoT'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.verifyMatch(PoT_detailRecord, GlobalVariable.confirmScreens.get('PoT_detailRecord' + count), false, FailureHandling.CONTINUE_ON_FAILURE)

    purpose__detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Categories/Label - purposeCode'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.verifyMatch(purpose__detailRecord, GlobalVariable.confirmScreens.get('purpose__detailRecord' + count), false, 
        FailureHandling.CONTINUE_ON_FAILURE)

    docType_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Categories/Label - docType'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.verifyMatch(docType_detailRecord, GlobalVariable.confirmScreens.get('docType_detailRecord' + count), false, FailureHandling.CONTINUE_ON_FAILURE)

    docDesc_detailRecord = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/Categories/Label - docDesc'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.verifyMatch(docDesc_detailRecord, GlobalVariable.confirmScreens.get('docDesc_detailRecord' + count), false, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/recordList/detailRecord/BTN - detailClose'), FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

    count++
}

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/recordList/BTN - recordListClose'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

refNum = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/checker/Label - refNumChecker'))

String[] refNumSplit = refNum.split(' ')

GlobalVariable.confirmScreens.put('refNum', refNumSplit[2])

docNo = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/checker/Label - DocChecker'))

String[] docNoSplit = docNo.split(' ')

GlobalVariable.confirmScreens.put('docNo', docNoSplit[2])

submitDate = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/checker/Label - submitDateChecker'))

submitDate = submitDate.replace('Submitted Date ', '')

GlobalVariable.confirmScreens.put('submitDate', submitDate)

WebUI.click(findTestObject('menu/BTN - logoutClick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(10, FailureHandling.CONTINUE_ON_FAILURE)

