import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import java.text.DecimalFormat as DecimalFormat
import java.text.NumberFormat as NumberFormat


Random rand = new Random()

//Flag Transfer To , 1 jika beneficiary list , 2 jika new beneficiary

String flagTrfTo = '1'

GlobalVariable.confirmScreens.put('flagTrfTo', flagTrfTo)

int amountRandom = rand.nextInt(89999)+10000

//Form New Beneficiary

int code = rand.next(89999)+10000

String aliasName = 'BTAliasName' + code

String BeneficiaryBank = 'ARTAJASA'

String AccountNumber = '6794445' + amountRandom

String AccountName = 'Bowo' + amountRandom

String Address_Field_1 = 'Jalan_1'

String Address_Field_2 = 'Jalan_2'

String Address_Field_3 = 'Jalan_3'

String BeneficiaryNotificationEmail = 'sadasdas@asdasd.com'

String BeneficiaryNotificationNumber = '123123123123'


String Product = 'SKN/LLG'

String Beneficiary_Type = 'Individual'

String Beneficiary_List = 'alias name1'

String Transfer_To_Description = 'testbraay2'

String Reference_Number = '1231231234'

String Amount = amountRandom

String BeneCategory = 'pemerintah'

String Transaction_Relationship = 'Affiliated'

String POF = 'Ekspor barang'

String PurposeCode = 'Investasi Pemberian Kredit'

String Document_Description = 'Document Bray'

String Document_Type = 'Fotokopi Pemberitahuan Impor'

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/DL - productClick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Value - trfToTextfield'), Product, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/BTN - productChoice'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(60, FailureHandling.CONTINUE_ON_FAILURE)

if(GlobalVariable.confirmScreens.get('flagTrfTo').equals('1')){
	WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/DL - beneficiaryTypeClick'), FailureHandling.CONTINUE_ON_FAILURE)
	
	WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)
	
	WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Value - trfToTextfield'), Beneficiary_Type, FailureHandling.CONTINUE_ON_FAILURE)
	
	WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)
	
	WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/BTN - trfToChoice'), FailureHandling.CONTINUE_ON_FAILURE)
	
	WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)
	
	WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/DL - beneficiaryListClick'), FailureHandling.CONTINUE_ON_FAILURE)
	
	WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)
	
	WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Value - trfToTextfield'), Beneficiary_List, FailureHandling.CONTINUE_ON_FAILURE)
	
	WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)
	
	WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/BTN - trfToChoice'), FailureHandling.CONTINUE_ON_FAILURE)
	
	beneListNumber1 = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/DL - beneficiaryListClick'), FailureHandling.STOP_ON_FAILURE)
	
	GlobalVariable.confirmScreens.put('beneList_detailRecord1', beneListNumber1.substring(0, beneListNumber1.indexOf(' ')))
	
	GlobalVariable.confirmScreens.put('trfTo_detailRecord1', ': Beneficiary List')
}else {
WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

	WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/BTN - newBeneficiaryClick - Copy'), FailureHandling.CONTINUE_ON_FAILURE)

	WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

	WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/Checkbox - saveToBeneList'), FailureHandling.CONTINUE_ON_FAILURE)

	WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

	WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Textfield - aliasName'), aliasName)

	WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

	WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/DL - beneficiaryBankClick'), FailureHandling.CONTINUE_ON_FAILURE)

	WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

	WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Value - trfToTextfield'), BeneficiaryBank)

	WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

	WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/BTN - trfToChoice'), FailureHandling.CONTINUE_ON_FAILURE)

	WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

	WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Textfield - accountNumber(newBeneficiary)'), AccountNumber)

	WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

	WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Textfield - accountName'), AccountName)

	WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

	WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Textfield - address'), Address_Field_1)

	WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

	WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Textfield - address2'), Address_Field_2)

	WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

	WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Textfield - address3'), Address_Field_3)

	WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

	WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Textfield - beneNotificationEmail'), BeneficiaryNotificationEmail)

	WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

	WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Textfield - beneNotificationNumber'), BeneficiaryNotificationNumber)

	WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

	GlobalVariable.confirmScreens.put('AliasName_detailRecord1', aliasName)

	GlobalVariable.confirmScreens.put('AccountNumber_detailRecord1', AccountNumber)

	GlobalVariable.confirmScreens.put('AccountName_detailRecord1', AccountName)

	GlobalVariable.confirmScreens.put('Address_Field_1_detailRecord1', Address_Field_1)

	GlobalVariable.confirmScreens.put('Address_Field_2_detailRecord1', Address_Field_2)

	GlobalVariable.confirmScreens.put('Address_Field_3_detailRecord1', Address_Field_3)

	GlobalVariable.confirmScreens.put('BeneficiaryNotificationEmail_detailRecord1', BeneficiaryNotificationEmail)

	GlobalVariable.confirmScreens.put('BeneficiaryNotificationNumber_detailRecord1', BeneficiaryNotificationNumber)
}

WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Value - troToDescription'), Transfer_To_Description, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Value - beneficiaryReferenceNumber'), Reference_Number, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Value - amount'), '' + Amount, FailureHandling.CONTINUE_ON_FAILURE)

Locale local1 = new Locale('en', 'UK')

DecimalFormat amountFormat = ((NumberFormat.getNumberInstance(local1)) as DecimalFormat)

amountFormat.applyPattern('###,###,##0.00')

String amountResult = '' + amountFormat.format(new BigDecimal(Amount))

not_run: WebUI.click(findTestObject('menu/BTN - clickMenu'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(4, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/DL - beneClick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(3, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Value - textfieldBene'), BeneCategory, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/Value - beneCategoryChoice'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/DL - relationshipClick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Value - textfieldBene'), Transaction_Relationship, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/BTN - relationshipChoice'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/DL - pofClick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Value - textfieldBene'), POF, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/BTN - pofChoice'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/DL - codeClick '), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Value - textfieldBene'), PurposeCode, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/BTN - codeChoice'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Value - lhbuDocumentTypeDescription'), Document_Description, 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/DL - documentTypeClick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('menu/trfManagement/bulkTransfer/Value - textfieldBene'), Document_Type, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/trfManagement/bulkTransfer/Value - documentTypeChoice'), FailureHandling.CONTINUE_ON_FAILURE)

BeneCategoryEntry = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/DL - beneClick'), FailureHandling.STOP_ON_FAILURE)

Transaction_RelationshipEntry = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/DL - relationshipClick'),
	FailureHandling.STOP_ON_FAILURE)

POFEntry = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/DL - pofClick'), FailureHandling.STOP_ON_FAILURE)

PurposeCodeEntry = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/DL - codeClick '), FailureHandling.STOP_ON_FAILURE)

Document_TypeEntry = WebUI.getText(findTestObject('menu/trfManagement/bulkTransfer/DL - documentTypeClick'), FailureHandling.STOP_ON_FAILURE)

GlobalVariable.confirmScreens.put('toAccDescription_detailRecord1', Transfer_To_Description)

GlobalVariable.confirmScreens.put('beneEmail_detailRecord1', BeneficiaryNotificationEmail)

GlobalVariable.confirmScreens.put('beneSms_detailRecord1', BeneficiaryNotificationNumber)

GlobalVariable.confirmScreens.put('beneRefNo_detailRecord1', Reference_Number)

GlobalVariable.confirmScreens.put('amount_detailRecord1', amountResult)

GlobalVariable.confirmScreens.put('docDesc_detailRecord1', Document_Description)

GlobalVariable.confirmScreens.put('beneCategory_detailRecord1', BeneCategoryEntry)

GlobalVariable.confirmScreens.put('transRelationship_detailRecord1', Transaction_RelationshipEntry)

GlobalVariable.confirmScreens.put('PoT_detailRecord1', POFEntry)

GlobalVariable.confirmScreens.put('purpose__detailRecord1', PurposeCodeEntry)

GlobalVariable.confirmScreens.put('docType_detailRecord1', Document_TypeEntry)