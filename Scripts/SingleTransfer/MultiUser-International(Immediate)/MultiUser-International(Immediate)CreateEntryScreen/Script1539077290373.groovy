import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import java.util.Locale as Locale
import java.text.DecimalFormat as DecimalFormat
import java.text.NumberFormat as NumberFormat
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW

///////////////////////////////////////////////////
fromAccountDescription = 'PangeranInhouseImmediate'

transferFrom = '003600088516'

toAccountDescription = 'money'

email = 'Pangeran@gmail.com'

sms = '911911911911'

inpBeneRefNum = '19283190219'

LHBUDocumentTypeDescription = 'aduhai'

aliasName = 'dapetdapet'

benefAccountNum = '123456789'

benefAccountName = 'dapitdapit'

add1 = 'Jl Kebon Sirih'

add2 = 'Jakarta Pusat'

///////////////////////////////////////////////////
WebUI.openBrowser('', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.navigateToUrl('https://10.194.8.106:39990/', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-CorpID'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-CorpID'), GlobalVariable.MultiUserLoginCorpregId, 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-UserID'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-UserID'), GlobalVariable.MultiUserLoginId, 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-UserPass'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-UserPass'), GlobalVariable.MultiUserPass, 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/B-Login'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/B-Menu'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/B-Menu'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TransferManagement'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TransferManagement'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/SingleTransfer'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/SingleTransfer'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInternational(Immediate)/Value-CreateEntryScreen/B-internationalForeignCurrencyTransafer'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(3, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccount'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), transferFrom, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/MainForm/V-Tfrom'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/MainForm/V-FAccDes'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/MainForm/V-FAccDes'), fromAccountDescription, 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInternational(Immediate)/Value-CreateEntryScreen/B-newBeneficiary'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('MultiUserInternational(Immediate)/Value-CreateEntryScreen/TF-aliasName'), aliasName, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInternational(Immediate)/Value-CreateEntryScreen/DL-beneficiaryBankCountry'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), 'ID', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), Keys.chord(Keys.ENTER), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInternational(Immediate)/Value-CreateEntryScreen/DL-nationalOrganizationDirectory'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInternational(Immediate)/Value-CreateEntryScreen/V-nationalOrganizationDirectory'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInternational(Immediate)/Value-CreateEntryScreen/B-beneficiaryBank'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), 'INDONESIA', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), Keys.chord(Keys.ENTER), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInternational(Immediate)/Value-CreateEntryScreen/DL-intermediaryBank'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), 'rere123', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), Keys.chord(Keys.ENTER), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('MultiUserInternational(Immediate)/Value-CreateEntryScreen/TF-beneficiaryAccountNumber'), benefAccountNum, 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('MultiUserInternational(Immediate)/Value-CreateEntryScreen/TF-beneficiaryAccountName'), benefAccountName, 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('MultiUserInternational(Immediate)/Value-CreateEntryScreen/TF-beneficiaryAddress1'), add1, 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('MultiUserInternational(Immediate)/Value-CreateEntryScreen/TF-beneficiaryAddress2'), add2, 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('MultiUserDomestic(Repeat)SKN/Value-EntryScreen/TF-email'), email, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('MultiUserDomestic(Repeat)SKN/Value-EntryScreen/TF-sms'), sms, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('MultiUserDomestic(Repeat)SKN/Value-EntryScreen/TF-toAccountDescription'), toAccountDescription, 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('MultiUserDomestic(Repeat)SKN/Value-EntryScreen/TF-beneficiaryReferenceNumber'), inpBeneRefNum, 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInternational(Immediate)/Value-CreateEntryScreen/DL-remittanceCurrency'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), 'JPY', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), Keys.chord(Keys.ENTER), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInternational(Immediate)/Value-CreateEntryScreen/DL-selectAmount'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), Keys.chord(Keys.ENTER), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-amount'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Random rand = new Random()

int angka

angka = rand.nextInt(999)

angka1 = ('3' + angka)

Locale local1 = new Locale('en', 'UK')

DecimalFormat amountInquiry = ((NumberFormat.getNumberInstance(local1)) as DecimalFormat)

amountInquiry.applyPattern('###,###,##0.00')

String amountInquiry1 = amountInquiry.format(new BigDecimal(angka1))

amountInquiry2 ='JPY   ' + amountInquiry1

GlobalVariable.UniversalVariable.put('Amount7',amountInquiry2)

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-amount'), angka1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('SingleUserInternational(Immediate)/DL-Borne by Remitter'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), 'borne by remitter', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), Keys.chord(Keys.ENTER), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInternational(Immediate)/Value-CreateEntryScreen/TF-beneficiaryCategory'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/MainForm/V-BenefCateg'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInternational(Immediate)/Value-CreateEntryScreen/TF-transactorRelationship'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/MainForm/V-TransactorRela'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInternational(Immediate)/Value-CreateEntryScreen/TF-purposeOfTransaction'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/MainForm/V-PuposeOfTransaction'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInternational(Immediate)/Value-CreateEntryScreen/DL-LHBUPurposeCode'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/MainForm/V-PuposeCode'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInternational(Immediate)/Value-CreateEntryScreen/DL-LHBUDocumentType'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/MainForm/V-DocType'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/MainForm/TFA-DocTypeDes'), LHBUDocumentTypeDescription, 
    FailureHandling.CONTINUE_ON_FAILURE)

transferFrom1 = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-EntryScreen(Extended)/V-transferFrom'), 
    FailureHandling.CONTINUE_ON_FAILURE)

transferFrom2 = transferFrom1.replace('-', '/')

transferFrom = transferFrom2.replace('(', ' (')

GlobalVariable.UniversalVariable.put('transferFrom', transferFrom)

GlobalVariable.UniversalVariable.put('fromAccountDescription', fromAccountDescription)

transferTo = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-EntryScreen(Extended)/V-transferTo'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('transferTo', transferTo)

benefBankCountry = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-EntryScreen(Extended)/V-benefBankCountry'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('benefBankCountry', benefBankCountry)

nationalOrganizationDirectory = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-EntryScreen(Extended)/V-nationalOrganizationDirectory'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('nationalOrganizationDirectory', nationalOrganizationDirectory)

benefBank = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-EntryScreen(Extended)/V-beneficiaryBank'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('benefBank', benefBank)

bankCity = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-EntryScreen(Extended)/V-bankCity'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('bankCity', bankCity)

intermediaryBank = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-EntryScreen(Extended)/V-intermedieryBank'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('intermediaryBank', intermediaryBank)

remitterCurrency = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-EntryScreen(Extended)/B-remitterCurrency'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('remitterCurrency', remitterCurrency)

amountCurrency = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-EntryScreen(Extended)/V-amount'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('amountCurrency', amountCurrency)

fullAmountCharge = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-EntryScreen(Extended)/V-fullAmountCharge'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('fullAmountCharge', fullAmountCharge)

debitCharge = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-EntryScreen(Extended)/V-debitCharge'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('debitCharge', debitCharge)

selectBenefciaryList = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-EntryScreen(Extended)/V-selectBeneficiaryList'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('selectBenefciaryList', selectBenefciaryList)

GlobalVariable.UniversalVariable.put('transferFrom', transferFrom)

GlobalVariable.UniversalVariable.put('sms', sms)

GlobalVariable.UniversalVariable.put('email', email)

GlobalVariable.UniversalVariable.put('toAccountDescription', toAccountDescription)

GlobalVariable.UniversalVariable.put('inputBeneficiaryReferenceNu', inpBeneRefNum)

GlobalVariable.UniversalVariable.put('aliasName', aliasName)

GlobalVariable.UniversalVariable.put('benefAccountNum', benefAccountNum)

GlobalVariable.UniversalVariable.put('benefAccountName', benefAccountName)

GlobalVariable.UniversalVariable.put('add1', add1)

GlobalVariable.UniversalVariable.put('add2', add2)

not_run: transferMethod = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-entry/V-transferMethod'), 
    FailureHandling.CONTINUE_ON_FAILURE)

not_run: GlobalVariable.UniversalVariable.put('transferMethod', transferMethod)

amount = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-EntryScreen/V-amount'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('amount', amount)

not_run: chargeInstruction = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-entry/V-chargeInstruction'), 
    FailureHandling.CONTINUE_ON_FAILURE)

not_run: GlobalVariable.UniversalVariable.put('chargeInstruction', chargeInstruction)

not_run: debitCharge = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-entry/V-debitCharge'), FailureHandling.CONTINUE_ON_FAILURE)

not_run: GlobalVariable.UniversalVariable.put('debitCharge', debitCharge)

beneficiaryCategory = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-EntryScreen(Extended)/V-benefCategory'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('beneficiaryCategory', beneficiaryCategory)

transactorRelationship = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-EntryScreen(Extended)/V-transactorRelationship'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('transactorRelationship', transactorRelationship)

identicalStatus = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-EntryScreen/V-identicalStatus'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('identicalStatus', identicalStatus)

purposeOfTransaction = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-EntryScreen(Extended)/V-purposeOfTransaction'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('purposeOfTransaction', purposeOfTransaction)

LHBUPurposeCode = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-EntryScreen(Extended)/V-LHBUPurposeCode'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('LHBUPurposeCode', LHBUPurposeCode)

LHBUDocumentType = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-EntryScreen(Extended)/V-LHBUDocumentType'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('LHBUDocumentType', LHBUDocumentType)

GlobalVariable.UniversalVariable.put('LHBUDocumentTypeDescription', LHBUDocumentTypeDescription)

instructionMode = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-EntryScreen/V-instructionMode'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('instructionMode', instructionMode)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/MainForm/B-Confim'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInternational(Immediate)/Value-CreateConfirmScreen/B-submitBox'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(10, FailureHandling.CONTINUE_ON_FAILURE)

