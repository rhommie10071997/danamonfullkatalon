import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

transFrom1 = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-ApproverConfrimScreen/V-transferFrom'), 
    FailureHandling.CONTINUE_ON_FAILURE)

transFrom = transFrom1.replace(': ', '')

 

WebUI.verifyMatch(transFrom, GlobalVariable.UniversalVariable.get('transferFrom'), false, FailureHandling.CONTINUE_ON_FAILURE)

fromAccDes = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-ApproverConfrimScreen/V-fromAccountDescription'), 
    FailureHandling.CONTINUE_ON_FAILURE)

FromAccDes = fromAccDes.replace(': ', '')

GlobalVariable.UniversalVariable.put('FromAccDes',FromAccDes)

WebUI.verifyMatch( FromAccDes, GlobalVariable.UniversalVariable.get('FromAccDes'), true, FailureHandling.CONTINUE_ON_FAILURE)

transTo = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-ApproverConfrimScreen/V-transferTo'), FailureHandling.CONTINUE_ON_FAILURE)

 TransferTo = transTo.replace(': ', '')

GlobalVariable.UniversalVariable.put('TransferTo',TransferTo)

WebUI.verifyMatch( TransferTo, GlobalVariable.UniversalVariable.get('TransferTo'), true, FailureHandling.CONTINUE_ON_FAILURE)

beneBankCon = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-CreateConfirmScreen/V-beneficiaryBankCountry'), 
    FailureHandling.CONTINUE_ON_FAILURE)

 BeneficiaryBankCountry = beneBankCon.replace(': ', '')

GlobalVariable.UniversalVariable.put('BeneficiaryBankCountry',BeneficiaryBankCountry)

WebUI.verifyMatch( BeneficiaryBankCountry, GlobalVariable.UniversalVariable.get('BeneficiaryBankCountry'), true, FailureHandling.CONTINUE_ON_FAILURE)

natOrgaDirectory = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-CreateConfirmScreen/V-nationalOrganizationDirectory'), 
    FailureHandling.CONTINUE_ON_FAILURE)

 NationalOrganizationDirectory = natOrgaDirectory.replace(': ', '')

GlobalVariable.UniversalVariable.put('NationalOrganizationDirectory',NationalOrganizationDirectory)

WebUI.verifyMatch(NationalOrganizationDirectory, GlobalVariable.UniversalVariable.get('NationalOrganizationDirectory'), true, FailureHandling.CONTINUE_ON_FAILURE)

beneBank = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-CreateConfirmScreen/V-beneficiaryBank'), 
    FailureHandling.CONTINUE_ON_FAILURE)

 BeneficiaryBank = beneBank.replace(': ', '')

GlobalVariable.UniversalVariable.put('BeneficiaryBank',BeneficiaryBank)

WebUI.verifyMatch(BeneficiaryBank, GlobalVariable.UniversalVariable.get('BeneficiaryBank'), true, FailureHandling.CONTINUE_ON_FAILURE)

bankCity = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-CreateConfirmScreen/V-bankCity'), FailureHandling.CONTINUE_ON_FAILURE)

 BankCity = bankCity.replace(': ', '')

GlobalVariable.UniversalVariable.put('BankCity',BankCity)

WebUI.verifyMatch( BankCity, GlobalVariable.UniversalVariable.get('BankCity'), true, FailureHandling.CONTINUE_ON_FAILURE)

interBank = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-CreateConfirmScreen/V-intermediaryBank'), 
    FailureHandling.CONTINUE_ON_FAILURE)

 IntermediaryBank = interBank.replace(': ', '')

GlobalVariable.UniversalVariable.put('IntermediaryBank',IntermediaryBank)

WebUI.verifyMatch( IntermediaryBank, GlobalVariable.UniversalVariable.get('IntermediaryBank'), true, FailureHandling.CONTINUE_ON_FAILURE)

accNumber = WebUI.getText(findTestObject('MultiUserDomestic(Repeat)SKN/Value-ConfirmScreen/V-accountNumber'), FailureHandling.CONTINUE_ON_FAILURE)

 accountNumber = accNumber.replace(': ', '')

 

WebUI.verifyMatch(accountNumber, GlobalVariable.UniversalVariable.get('benefAccountNum'), true, FailureHandling.CONTINUE_ON_FAILURE)

savToBeneList = WebUI.getText(findTestObject('MultiUserDomestic(Repeat)SKN/Value-ConfirmScreen/V-saveToBeneficiaryList'), 
    FailureHandling.CONTINUE_ON_FAILURE)

 saveToBeneficiaryList = savToBeneList.replace(': ', '')

GlobalVariable.UniversalVariable.put('saveToBeneficiaryList',saveToBeneficiaryList)

WebUI.verifyMatch( saveToBeneficiaryList, GlobalVariable.UniversalVariable.get('saveToBeneficiaryList'), true, FailureHandling.CONTINUE_ON_FAILURE)

accName = WebUI.getText(findTestObject('MultiUserDomestic(Repeat)SKN/Value-ConfirmScreen/V-accountName'), FailureHandling.CONTINUE_ON_FAILURE)

 AccName = accName.replace(': ', '')

 

WebUI.verifyMatch( AccName, GlobalVariable.UniversalVariable.get('benefAccountName'), true, FailureHandling.CONTINUE_ON_FAILURE)

alName = WebUI.getText(findTestObject('MultiUserDomestic(Repeat)SKN/Value-ConfirmScreen/V-aliasName'), FailureHandling.CONTINUE_ON_FAILURE)

 aliasName = alName.replace(': ', '')

WebUI.verifyMatch( aliasName, GlobalVariable.UniversalVariable.get('aliasName'), true, FailureHandling.CONTINUE_ON_FAILURE)

 

address1 = WebUI.getText(findTestObject('MultiUserDomestic(Repeat)SKN/Value-ConfirmScreen/V-address1'), FailureHandling.CONTINUE_ON_FAILURE)

 tempAddress1 = address1.replace(': ', '')

 

WebUI.verifyMatch( tempAddress1, GlobalVariable.UniversalVariable.get('add1'), true, FailureHandling.CONTINUE_ON_FAILURE)

address2 = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-CreateConfirmScreen/V-address2'), FailureHandling.CONTINUE_ON_FAILURE)

 tempAddress2 = address2.replace(': ', '')

 

WebUI.verifyMatch( tempAddress2, GlobalVariable.UniversalVariable.get('add2'), true, FailureHandling.CONTINUE_ON_FAILURE)

email = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-CreateConfirmScreen/V-email'), FailureHandling.CONTINUE_ON_FAILURE)

 ValueBankTypeEmail = email.replace('Email : ', '')

WebUI.verifyMatch( ValueBankTypeEmail, GlobalVariable.UniversalVariable.get('email'), true, FailureHandling.CONTINUE_ON_FAILURE)

sms = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-CreateConfirmScreen/V-sms'), FailureHandling.CONTINUE_ON_FAILURE)

ValueBankTypeSms = sms.replace('SMS   : ', '')

WebUI.verifyMatch( ValueBankTypeSms, GlobalVariable.UniversalVariable.get('sms'), true, FailureHandling.CONTINUE_ON_FAILURE)

benefRefNo = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-ApproverConfrimScreen/V-beneficiaryReffNumber'), 
    FailureHandling.CONTINUE_ON_FAILURE)

BenefRefNo = benefRefNo.replace(': ', '')
 
WebUI.verifyMatch( BenefRefNo, GlobalVariable.UniversalVariable.get('inputBeneficiaryReferenceNu'), true, FailureHandling.CONTINUE_ON_FAILURE)

toAccDes = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-ApproverConfrimScreen/V-toAccountDescription'), 
    FailureHandling.CONTINUE_ON_FAILURE)

ToAccDes = toAccDes.replace(': ', '')

WebUI.verifyMatch(ToAccDes, GlobalVariable.UniversalVariable.get('toAccountDescription'), true, FailureHandling.CONTINUE_ON_FAILURE)

remiCurrency = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-CreateConfirmScreen/V-remittanceCurrency'), 
    FailureHandling.CONTINUE_ON_FAILURE)

 RemittanceCurrency = remiCurrency.replace(': ', '')

GlobalVariable.UniversalVariable.put('RemittanceCurrency',RemittanceCurrency)

WebUI.verifyMatch( RemittanceCurrency, GlobalVariable.UniversalVariable.get('RemittanceCurrency'), true, FailureHandling.CONTINUE_ON_FAILURE)

amount = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-ApproverConfrimScreen/V-amount'), FailureHandling.CONTINUE_ON_FAILURE)

Amount = amount.replace(': ', '')

WebUI.verifyMatch(Amount, GlobalVariable.UniversalVariable.get('Amount7'), true, FailureHandling.CONTINUE_ON_FAILURE)

fuAmoCharge = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-CreateConfirmScreen/V-fullAmountCharge'), 
    FailureHandling.CONTINUE_ON_FAILURE)

FullAmountCharge = fuAmoCharge.replace(': ', '')

 

WebUI.verifyMatch( FullAmountCharge, GlobalVariable.UniversalVariable.get('fullAmountCharge'), true, FailureHandling.CONTINUE_ON_FAILURE)

debCharge = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-CreateConfirmScreen/V-debitCharge'), FailureHandling.CONTINUE_ON_FAILURE)

 DebitCharge = debCharge.replace(': ', '')

WebUI.verifyMatch( DebitCharge, GlobalVariable.UniversalVariable.get('debitCharge'), true, FailureHandling.CONTINUE_ON_FAILURE)

provision = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-CreateConfirmScreen/V-provision'), FailureHandling.CONTINUE_ON_FAILURE)

 Provision = provision.replace(': ', '')

GlobalVariable.UniversalVariable.put('Provision',Provision)

WebUI.verifyMatch( Provision, GlobalVariable.UniversalVariable.get('Provision'), true, FailureHandling.CONTINUE_ON_FAILURE)

inLieu = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-CreateConfirmScreen/V-inLieu'), FailureHandling.CONTINUE_ON_FAILURE)

 InLieu = inLieu.replace(': ', '')

GlobalVariable.UniversalVariable.put('InLieu',InLieu)

WebUI.verifyMatch( InLieu, GlobalVariable.UniversalVariable.get('InLieu'), true, FailureHandling.CONTINUE_ON_FAILURE)

fuAmoCharge2 = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-CreateConfirmScreen/V-fullAmountCharge1'), 
    FailureHandling.CONTINUE_ON_FAILURE)

FullAmountCharge2 = fuAmoCharge2.replace(': ', '')

GlobalVariable.UniversalVariable.put('FullAmountCharge2',FullAmountCharge2)

WebUI.verifyMatch(FullAmountCharge2, GlobalVariable.UniversalVariable.get('FullAmountCharge2'), true, FailureHandling.CONTINUE_ON_FAILURE)

cabFee = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-CreateConfirmScreen/V-cableFee'), FailureHandling.CONTINUE_ON_FAILURE)

 CableFee = cabFee.replace(': ', '')

GlobalVariable.UniversalVariable.put('CableFee',CableFee)

WebUI.verifyMatch( CableFee, GlobalVariable.UniversalVariable.get('CableFee'), true, FailureHandling.CONTINUE_ON_FAILURE)

MinTransFee = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-CreateConfirmScreen/V-minimunTransactionFee'), 
    FailureHandling.CONTINUE_ON_FAILURE)

 MinimumTransactionFee = MinTransFee.replace(': ', '')

GlobalVariable.UniversalVariable.put('MinimumTransactionFee',MinimumTransactionFee)

WebUI.verifyMatch( MinimumTransactionFee, GlobalVariable.UniversalVariable.get('MinimumTransactionFee'), true, FailureHandling.CONTINUE_ON_FAILURE)

corresBankFee = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-CreateConfirmScreen/V-correspondentBankFee'), 
    FailureHandling.CONTINUE_ON_FAILURE)

CorrespondentBankFee = corresBankFee.replace(': ', '')

GlobalVariable.UniversalVariable.put('CorrespondentBankFee',CorrespondentBankFee)

WebUI.verifyMatch( CorrespondentBankFee, GlobalVariable.UniversalVariable.get('CorrespondentBankFee'), true, FailureHandling.CONTINUE_ON_FAILURE)

totCharge = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-ApproverConfrimScreen/V-totalCharge'), 
    FailureHandling.CONTINUE_ON_FAILURE)

 TotalCharge = totCharge.replace(': ', '')

GlobalVariable.UniversalVariable.put('TotalCharge',TotalCharge)

WebUI.verifyMatch(TotalCharge, GlobalVariable.UniversalVariable.get('TotalCharge'), true, FailureHandling.CONTINUE_ON_FAILURE)

charToRemitter = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-CreateConfirmScreen/V-chargeToRemitter'), 
    FailureHandling.CONTINUE_ON_FAILURE)

 ChargeToRemitter = charToRemitter.replace(': ', '')

GlobalVariable.UniversalVariable.put('ChargeToRemitter',ChargeToRemitter)

WebUI.verifyMatch(ChargeToRemitter, GlobalVariable.UniversalVariable.get('ChargeToRemitter'), true, FailureHandling.CONTINUE_ON_FAILURE)

charToBeneficiary = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-CreateConfirmScreen/V-chargeToBeneficiary'), 
    FailureHandling.CONTINUE_ON_FAILURE)

 ChargeToBeneficiary = charToBeneficiary.replace(': ', '')

GlobalVariable.UniversalVariable.put('ChargeToBeneficiary',ChargeToBeneficiary)

WebUI.verifyMatch( ChargeToBeneficiary, GlobalVariable.UniversalVariable.get('ChargeToBeneficiary'), true, FailureHandling.CONTINUE_ON_FAILURE)

totDebAmount = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-ApproverConfrimScreen/V-totalDebit'), 
    FailureHandling.CONTINUE_ON_FAILURE)

 TotalDebet = totDebAmount.replace(': ', '')

GlobalVariable.UniversalVariable.put('TotalDebet',TotalDebet)

WebUI.verifyMatch( TotalDebet, GlobalVariable.UniversalVariable.get('TotalDebet'), true, FailureHandling.CONTINUE_ON_FAILURE)

benefResisStat = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-ApproverConfrimScreen/V-beneficiaryResistantStatus'), 
    FailureHandling.CONTINUE_ON_FAILURE)

BenefResidentStatus = benefResisStat.replace(': ', '')

GlobalVariable.UniversalVariable.put('BenefResidentStatus',BenefResidentStatus)

WebUI.verifyMatch(BenefResidentStatus, GlobalVariable.UniversalVariable.get('BenefResidentStatus'), true, FailureHandling.CONTINUE_ON_FAILURE)

benefCate = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-ApproverConfrimScreen/V-beneficiaryCategory'), 
    FailureHandling.CONTINUE_ON_FAILURE)

 BenefCate = benefCate.replace(': ', '')

 

WebUI.verifyMatch( BenefCate, GlobalVariable.UniversalVariable.get('beneficiaryCategory'), true, FailureHandling.CONTINUE_ON_FAILURE)

transRela = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-ApproverConfrimScreen/V-transactorRelationship'), 
    FailureHandling.CONTINUE_ON_FAILURE)

 TransRela = transRela.replace(': ', '')

 

WebUI.verifyMatch( TransRela, GlobalVariable.UniversalVariable.get('transactorRelationship'), true, FailureHandling.CONTINUE_ON_FAILURE)

idenStatus = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-ApproverConfrimScreen/V-identicalStatus'), 
    FailureHandling.CONTINUE_ON_FAILURE)

 IdenticalStatus = idenStatus.replace(': ', '')

 

WebUI.verifyMatch(IdenticalStatus, GlobalVariable.UniversalVariable.get('identicalStatus'), true, FailureHandling.CONTINUE_ON_FAILURE)

purpOfTrans = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverConfrimScreen/V-puposeOfTransaction'), 
    FailureHandling.CONTINUE_ON_FAILURE)

 PurpOfTrans = purpOfTrans.replace(': ', '')

 

WebUI.verifyMatch( PurpOfTrans, GlobalVariable.UniversalVariable.get('purposeOfTransaction'), true, FailureHandling.CONTINUE_ON_FAILURE)

purpCode = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-ApproverConfrimScreen/V-LHBUPurposeCode'), 
    FailureHandling.CONTINUE_ON_FAILURE)

 PurpCode = purpCode.replace(': ', '')
 

WebUI.verifyMatch( PurpCode, GlobalVariable.UniversalVariable.get('LHBUPurposeCode'), true, FailureHandling.CONTINUE_ON_FAILURE)

docType = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-ApproverConfrimScreen/V-LHBUDocumentType'), 
    FailureHandling.CONTINUE_ON_FAILURE)

 DocType = docType.replace(': ', '')

 

WebUI.verifyMatch(DocType, GlobalVariable.UniversalVariable.get('LHBUDocumentType'), true, FailureHandling.CONTINUE_ON_FAILURE)

docTypeDes = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-ApproverConfrimScreen/V-LHBUDocumentTypeDescription'), 
    FailureHandling.CONTINUE_ON_FAILURE)

 DocTypeDes = docTypeDes.replace(': ', '')
 

WebUI.verifyMatch( DocTypeDes, GlobalVariable.UniversalVariable.get('LHBUDocumentTypeDescription'), true, FailureHandling.CONTINUE_ON_FAILURE)

instrucMode = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-ApproverConfrimScreen/V-intructionMode'), 
    FailureHandling.CONTINUE_ON_FAILURE)

 InstructionMode = instrucMode.replace(': ', '')

GlobalVariable.UniversalVariable.put('InstructionMode',InstructionMode)

WebUI.verifyMatch( InstructionMode, GlobalVariable.UniversalVariable.get('InstructionMode'), true, FailureHandling.CONTINUE_ON_FAILURE)

expOn = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)RTGS/Value-ApproverConfrimScreen/V-expiredOn'), FailureHandling.CONTINUE_ON_FAILURE)

 ExpiredOn = expOn.replace(': ', '')

GlobalVariable.UniversalVariable.put('ExpiredOn',ExpiredOn)

WebUI.verifyMatch( ExpiredOn, GlobalVariable.UniversalVariable.get('ExpiredOn'), true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('SingleUserInhouse(immediate)/EntryScreen/B-Submit'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('SingleUserInhouse(immediate)/EntryScreen/B-Submit2'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

