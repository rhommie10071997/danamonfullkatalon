import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

transferFrom1 = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-ConfirmScreen/V-transferForm'), FailureHandling.CONTINUE_ON_FAILURE)

transferFrom2 = transferFrom1.replace(': ', '')

transferFrom = transferFrom2.replace('/', ' /')

WebUI.verifyMatch(transferFrom, GlobalVariable.UniversalVariable.get('transferFrom'), false, FailureHandling.CONTINUE_ON_FAILURE)

fromAccountDes1 = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-ConfirmScreen/V-fromAccountDescription'), 
    FailureHandling.CONTINUE_ON_FAILURE)

fromAccountDes = fromAccountDes1.replace(': ', '')

WebUI.verifyMatch(fromAccountDes, GlobalVariable.UniversalVariable.get('fromAccountDescription'), true, FailureHandling.CONTINUE_ON_FAILURE)

transferTo1 = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-ConfirmScreen/V-transferTo'), FailureHandling.CONTINUE_ON_FAILURE)

transferTo = transferTo1.replace(': ', '')

WebUI.verifyMatch(transferTo, GlobalVariable.UniversalVariable.get('transferTo'), true, FailureHandling.CONTINUE_ON_FAILURE)

accNum1 = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ConfirmScreen/V-beneficiary'), FailureHandling.CONTINUE_ON_FAILURE)

accNum = accNum1.replace(': ', '')

WebUI.verifyMatch(accNum, accNum, false, FailureHandling.CONTINUE_ON_FAILURE)

toAccDes1 = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-ConfirmScreen/V-toAccountDescription'), FailureHandling.CONTINUE_ON_FAILURE)

toAccDes = toAccDes1.replace(': ', '')

WebUI.verifyMatch(toAccDes, GlobalVariable.UniversalVariable.get('toAccountDescription'), true, FailureHandling.CONTINUE_ON_FAILURE)

inpBeneRefNum1 = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ConfirmScreen/V-beneficiaryRefferenceNumber'), 
    FailureHandling.CONTINUE_ON_FAILURE)

inpBeneRefNum = inpBeneRefNum1.replace(': ', '')

WebUI.verifyMatch(inpBeneRefNum, GlobalVariable.UniversalVariable.get('inputBeneficiaryReferenceNu'), true, FailureHandling.CONTINUE_ON_FAILURE)

transMeth = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ConfirmScreen/V-transferMethod'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.TransferMethod = transMeth.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.TransferMethod, GlobalVariable.V28, true, FailureHandling.CONTINUE_ON_FAILURE)

beneType = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ConfirmScreen/V-beneficiaryType'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.BenefType = beneType.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.BenefType, GlobalVariable.V29, true, FailureHandling.CONTINUE_ON_FAILURE)

amount1 = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-ConfirmScreen/V-amount'), FailureHandling.CONTINUE_ON_FAILURE)

amount = amount1.replace(': ', '')

GlobalVariable.Amount = amount

String verifyAmount1 = 'IDR ' + GlobalVariable.Amount7

verifyAmount = verifyAmount1.replace('IDR   ', 'IDR ')

GlobalVariable.UniversalVariable.put('Amount', verifyAmount)

WebUI.verifyMatch(verifyAmount, GlobalVariable.UniversalVariable.get('Amount'), true, FailureHandling.CONTINUE_ON_FAILURE)

exRate1 = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-ConfirmScreen/V-exchangeRate'), FailureHandling.CONTINUE_ON_FAILURE)

exRate = exRate1.replace(': ', '')

GlobalVariable.UniversalVariable.put('exchangeRate', exRate)

WebUI.verifyMatch(exRate, GlobalVariable.UniversalVariable.get('exchangeRate'), true, FailureHandling.CONTINUE_ON_FAILURE)

SKNFee = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ConfirmScreen/V-SKNFee'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.SKNFee = SKNFee.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.SKNFee, GlobalVariable.SKNFee, true, FailureHandling.CONTINUE_ON_FAILURE)

totCharge1 = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-ConfirmScreen/V-totalCharge'), FailureHandling.CONTINUE_ON_FAILURE)

totCharge = totCharge1.replace(': ', '')

GlobalVariable.UniversalVariable.put('totalCharge', totCharge)

WebUI.verifyMatch(totCharge, GlobalVariable.UniversalVariable.get('totalCharge'), true, FailureHandling.CONTINUE_ON_FAILURE)

totDebAmount1 = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-ConfirmScreen/V-totalDebetAmount'), FailureHandling.CONTINUE_ON_FAILURE)

totDebAmount = totDebAmount1.replace(': ', '')

GlobalVariable.UniversalVariable.put('totalDebitAmount', totDebAmount)

WebUI.verifyMatch(totDebAmount, GlobalVariable.UniversalVariable.get('totalDebitAmount'), true, FailureHandling.CONTINUE_ON_FAILURE)

chargeInst1 = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-ConfirmScreen/V-chargeInstruction'), FailureHandling.CONTINUE_ON_FAILURE)

chargeInst = chargeInst1.replace(': ', '')

WebUI.verifyMatch(chargeInst, GlobalVariable.UniversalVariable.get('chargeInstruction'), true, FailureHandling.CONTINUE_ON_FAILURE)

debitCharge1 = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-ConfirmScreen/V-debitCharge'), FailureHandling.CONTINUE_ON_FAILURE)

debitCharge = debitCharge1.replace(': ', '')

WebUI.verifyMatch(debitCharge, GlobalVariable.UniversalVariable.get('debitCharge'), true, FailureHandling.CONTINUE_ON_FAILURE)

beneficiaryCategory1 = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-ConfirmScreen/V-beneficiaryCategory'), 
    FailureHandling.CONTINUE_ON_FAILURE)

beneficiaryCategory = beneficiaryCategory1.replace(': ', '')

WebUI.verifyMatch(beneficiaryCategory, GlobalVariable.UniversalVariable.get('beneficiaryCategory'), true, FailureHandling.CONTINUE_ON_FAILURE)

transactorRelationship1 = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-ConfirmScreen/V-transactorRelationship'), 
    FailureHandling.CONTINUE_ON_FAILURE)

transactorRelationship = transactorRelationship1.replace(': ', '')

WebUI.verifyMatch(transactorRelationship, GlobalVariable.UniversalVariable.get('transactorRelationship'), true, FailureHandling.CONTINUE_ON_FAILURE)

identicalStatus1 = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-ConfirmScreen/V-identicalStatus'), FailureHandling.CONTINUE_ON_FAILURE)

identicalStatus = identicalStatus1.replace(': ', '')

WebUI.verifyMatch(identicalStatus, GlobalVariable.UniversalVariable.get('identicalStatus'), true, FailureHandling.CONTINUE_ON_FAILURE)

purposeOfTransaction1 = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-ConfirmScreen/V-purposeOfTransaction'), 
    FailureHandling.CONTINUE_ON_FAILURE)

purposeOfTransaction = purposeOfTransaction1.replace(': ', '')

WebUI.verifyMatch(purposeOfTransaction, GlobalVariable.UniversalVariable.get('purposeOfTransaction'), true, FailureHandling.CONTINUE_ON_FAILURE)

LHBUPurposeCode1 = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-ConfirmScreen/V-LHBUPurposeCode'), FailureHandling.CONTINUE_ON_FAILURE)

LHBUPurposeCode = LHBUPurposeCode1.replace(': ', '')

WebUI.verifyMatch(LHBUPurposeCode, GlobalVariable.UniversalVariable.get('LHBUPurposeCode'), true, FailureHandling.CONTINUE_ON_FAILURE)

LHBUDocumentType1 = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-ConfirmScreen/V-LHBUDocumentType'), 
    FailureHandling.CONTINUE_ON_FAILURE)

LHBUDocumentType = LHBUDocumentType1.replace(': ', '')

WebUI.verifyMatch(LHBUDocumentType, GlobalVariable.UniversalVariable.get('LHBUDocumentType'), true, FailureHandling.CONTINUE_ON_FAILURE)

LHBUDocumentTypreDescription1 = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-ConfirmScreen/V-LHBUDocumentTypreDescription'), 
    FailureHandling.CONTINUE_ON_FAILURE)

LHBUDocumentTypreDescription = LHBUDocumentTypreDescription1.replace(': ', '')

WebUI.verifyMatch(LHBUDocumentTypreDescription, GlobalVariable.UniversalVariable.get('LHBUDocumentTypeDescription'), true, 
    FailureHandling.CONTINUE_ON_FAILURE)

instructionMode1 = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-ConfirmScreen/V-instructionMode'), FailureHandling.CONTINUE_ON_FAILURE)

instructionMode = instructionMode1.replace(': ', '')

WebUI.verifyMatch(instructionMode, GlobalVariable.UniversalVariable.get('instructionMode'), true, FailureHandling.CONTINUE_ON_FAILURE)

expiredOn1 = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-ConfirmScreen/V-expiredOn'), FailureHandling.CONTINUE_ON_FAILURE)

expiredOn = expiredOn1.replace(': ', '')

GlobalVariable.UniversalVariable.put('expiredOn', expiredOn)

WebUI.verifyMatch(expiredOn, GlobalVariable.UniversalVariable.get('expiredOn'), true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2)

CustomKeywords.'get.Screencapture.getEntirePage'('')

WebUI.delay(2)

WebUI.scrollToElement(findTestObject('SingleUserInhouse(immediate)/Value-ConfirmScreen/V-totalCharge'), 0)

WebUI.delay(2)

CustomKeywords.'get.Screencapture.getEntirePage'('')

WebUI.delay(2)

WebUI.scrollToElement(findTestObject('SingleUserInhouse(immediate)/Value-ConfirmScreen/V-LHBUDocumentTypreDescription'), 
    0)

WebUI.delay(2)

CustomKeywords.'get.Screencapture.getEntirePage'('')

WebUI.delay(2)

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-responseCode'), '123456', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('SingleUserInhouse(immediate)/EntryScreen/B-Submit'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('SingleUserInhouse(immediate)/EntryScreen/B-Submit2'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

