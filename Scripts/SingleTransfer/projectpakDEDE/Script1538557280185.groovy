import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import java.util.Locale as Locale
import java.text.DecimalFormat as DecimalFormat
import java.text.NumberFormat as NumberFormat

WebUI.openBrowser('')

WebUI.navigateToUrl('https://10.194.8.106:39990/')

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-CorpID'), 0)

WebUI.setText(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-CorpID'), GlobalVariable.SingleUserLoginCorpregId)

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-UserID'), 0)

WebUI.setText(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-UserID'), GlobalVariable.SingleUserLoginId)

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-UserPass'), 0)

WebUI.setText(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-UserPass'), GlobalVariable.SingleUserPass)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/B-Login'))

WebUI.delay(5)

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-MenuSearch'), 0)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-MenuSearch'))

WebUI.setText(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-MenuSearch'), 'Single Transfer')

WebUI.delay(2)

WebUI.waitForElementVisible(findTestObject('SingleUserInhouse(immediate)/EntryScreen/Text-SingleTransfer'), 0)

WebUI.click(findTestObject('SingleUserInhouse(immediate)/EntryScreen/Text-SingleTransfer'))

WebUI.delay(5)

WebUI.click(findTestObject('SingleUSerDomestic(Immediate)/B-DomesticTransfer'))

WebUI.delay(5)

WebUI.click(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccount'))

WebUI.waitForElementVisible(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), 0)

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), GlobalVariable.userTransferFrom)

WebUI.delay(2)

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), Keys.chord(Keys.ENTER))

WebUI.waitForElementVisible(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-fromAcctDesc'), 0)

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-fromAcctDesc'), 'PangeranInhouseImmediate')

WebUI.waitForElementVisible(findTestObject('SingleUSerDomestic(Immediate)/Tf-Select Beneficiary List'), 0)

WebUI.click(findTestObject('SingleUSerDomestic(Immediate)/Tf-Select Beneficiary List'))

WebUI.waitForElementVisible(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), 0)

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), GlobalVariable.dodo)

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), Keys.chord(Keys.ENTER))

WebUI.waitForElementVisible(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-toAcctDesc'), 0)

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-toAcctDesc'), 'money')

WebUI.waitForElementVisible(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-beneficiaryReferenceNumber'), 0)

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-beneficiaryReferenceNumber'), '19283190219')

WebUI.waitForElementVisible(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-amount'), 0)

Random rand = new Random()

int angka

angka = rand.nextInt(999)

angka1 = ('399' + angka)

Locale local1 = new Locale('en', 'UK')

DecimalFormat amountInquiry = ((NumberFormat.getNumberInstance(local1)) as DecimalFormat)

amountInquiry.applyPattern('###,###,##0.00')

String amountInquiry1 = amountInquiry.format(new BigDecimal(angka1))

GlobalVariable.Amount7 = amountInquiry1

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-amount'), angka1)

WebUI.click(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-benefCat'))

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), '3 - Pemerintah')

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-transRela'))

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), 'A - Affiliated')

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-purpOfTrans'))

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), '2011 - Ekspor barang')

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('SingleUserInhouse(immediate)/EntryScreen/Tf-purpCode'))

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), '00 - 00 Investasi Penyertaan Langsung')

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-docType'))

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), '001 - 001 Fotokopi Pemberitahuan Impor Barang PIB ')

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-docTypeDes'))

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-docTypeDes'), 'aduhai')

WebUI.click(findTestObject('SingleUSerDomestic(Immediate)/Button-Confirm'))

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/MainForm/B-Confim'))

WebUI.delay(20)

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-responseCode'), '123456')

WebUI.click(findTestObject('SingleUserInhouse(immediate)/EntryScreen/B-Submit'))

WebUI.delay(2)

WebUI.click(findTestObject('SingleUserInhouse(immediate)/EntryScreen/B-Submit2'))

WebUI.delay(5)

WebUI.delay(60)

WebUI.click(findTestObject('SingleUserInhouse(immediate)/Value-SuccessScreen/Button-Done'))

WebUI.delay(15)

