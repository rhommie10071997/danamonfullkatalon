import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import java.util.Locale as Locale
import java.text.DecimalFormat as DecimalFormat
import java.text.NumberFormat as NumberFormat
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW

///////////////////////////////////////////////////
fromAccountDescription = 'PangeranInhouseImmediate'

transferFrom = '000002814846'

toAccountDescription = 'money'

email = 'Pangeran@gmail.com'

sms = '911911911911'

inpBeneRefNum = '19283190219'

LHBUDocumentTypeDescription = 'aduhai'

accNumber = '003400008464'

accName = 'dapit pangeran'

address1 = 'Jl Kebon Sirih'

address2 = 'Jakarta Pusat'

address3 = 'Deket Rel Kereta Sebelum Tugu Tani'

///////////////////////////////////////////////////
WebUI.openBrowser('', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.navigateToUrl('https://10.194.8.106:39990/', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-CorpID'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-CorpID'), GlobalVariable.MultiUserLoginCorpregId, 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-UserID'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-UserID'), GlobalVariable.MultiUserLoginId, 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-UserPass'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-UserPass'), GlobalVariable.MultiUserPass, 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/B-Login'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/B-Menu'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/B-Menu'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TransferManagement'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TransferManagement'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/SingleTransfer'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/SingleTransfer'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('SingleUSerDomestic(Immediate)/B-DomesticTransfer'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(3, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccount'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), transferFrom, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/MainForm/V-Tfrom'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/MainForm/V-FAccDes'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/MainForm/V-FAccDes'), fromAccountDescription, 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserDomestic(Repeat)SKN/Value-EntryScreen/B-newBeneficiary'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserDomestic(Repeat)SKN/Value-EntryScreen/TF-beneficiaryBank'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), GlobalVariable.newBeneficiaryBNI, 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), Keys.chord(Keys.ENTER), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('MultiUserDomestic(Repeat)SKN/Value-EntryScreen/TF-accountNumber'), accNumber, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('MultiUserDomestic(Repeat)SKN/Value-EntryScreen/TF-accountName'), accName, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('SingleUser(FutureDate)RTGS/TF-beneficiaryAddress1'), address1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('SingleUser(FutureDate)RTGS/TF-beneficiaryAddress2'), address2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('SingleUser(FutureDate)RTGS/TF-beneficiaryAddress3'), address3, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('MultiUserDomestic(Repeat)SKN/Value-EntryScreen/TF-email'), email, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('MultiUserDomestic(Repeat)SKN/Value-EntryScreen/TF-sms'), sms, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('MultiUserDomestic(Repeat)SKN/Value-EntryScreen/TF-toAccountDescription'), toAccountDescription, 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('MultiUserDomestic(Repeat)SKN/Value-EntryScreen/TF-beneficiaryReferenceNumber'), inpBeneRefNum, 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserDomestic(Repeat)SKN/Value-EntryScreen/TF-beneficiaryTpe'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), Keys.chord(Keys.ENTER), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-amount'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Random rand = new Random()

int angka

angka = rand.nextInt(999)

angka1 = ('399' + angka)

Locale local1 = new Locale('en', 'UK')

DecimalFormat amountInquiry = ((NumberFormat.getNumberInstance(local1)) as DecimalFormat)

amountInquiry.applyPattern('###,###,##0.00')

String amountInquiry1 = amountInquiry.format(new BigDecimal(angka1))

GlobalVariable.Amount7 = amountInquiry1

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-amount'), angka1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserDomestic(Repeat)SKN/Value-EntryScreen/B-resident'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserDomestic(Repeat)SKN/Value-EntryScreen/TF-beneficiaryCategory'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/MainForm/V-BenefCateg'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/MainForm/TF-TransactorRelation'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/MainForm/V-TransactorRela'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/MainForm/TF-PurposeOfTransaction'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/MainForm/V-PuposeOfTransaction'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/MainForm/TF-PurposeCode'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/MainForm/V-PuposeCode'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/MainForm/TF-DocType'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/MainForm/V-DocType'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/MainForm/TFA-DocTypeDes'), LHBUDocumentTypeDescription, 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInhouse(Repeat)/Value-CreateEntryScreen/B-repeat'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInhouse(Repeat)/Value-CreateEntryScreen/DL-searchSchedule'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), Keys.chord(Keys.ENTER), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInhouse(Repeat)/Value-CreateEntryScreen/DL-every'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), Keys.chord(Keys.ENTER), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInhouse(Repeat)/Value-CreateEntryScreen/DL-at'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), Keys.chord(Keys.ENTER), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInhouse(Repeat)/Value-CreateEntryScreen/TF-start'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInhouse(Repeat)/Value-CreateEntryScreen/V-start'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInhouse(Repeat)/Value-CreateEntryScreen/DL-end'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInhouse(Repeat)/Value-CreateEntryScreen/V-end'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInhouse(Repeat)/Value-CreateEntryScreen/DL-non-WorkingDayInstruction'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), Keys.chord(Keys.ENTER), 
    FailureHandling.CONTINUE_ON_FAILURE)

transferFrom1 = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-EntryScreen/V-transferFrom'), FailureHandling.CONTINUE_ON_FAILURE)

transferFrom2 = transferFrom1.replace('-', '/')

transferFrom = transferFrom2.replace('(', ' (')

GlobalVariable.UniversalVariable.put('transferFrom', transferFrom)

GlobalVariable.UniversalVariable.put('fromAccountDescription', fromAccountDescription)

transferTo = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-entry/V-transferTO'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('transferTo', transferTo)

beneficiaryBank = WebUI.getText(findTestObject('MultiUserDomestic(Repeat)SKN/Value-EntryScreen/V-extend/V-beneficiaryBank'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('beneficiaryBank', beneficiaryBank)

GlobalVariable.UniversalVariable.put('accNumber', accNumber)

GlobalVariable.UniversalVariable.put('accName', accName)

GlobalVariable.UniversalVariable.put('address1', address1)

GlobalVariable.UniversalVariable.put('address2', address2)

GlobalVariable.UniversalVariable.put('address3', address3)

selectBenefciaryList = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-entry/V-selectBeneficiaryList'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('selectAccount', selectBenefciaryList)

GlobalVariable.UniversalVariable.put('sms', sms)

GlobalVariable.UniversalVariable.put('email', email)

GlobalVariable.UniversalVariable.put('toAccountDescription', toAccountDescription)

GlobalVariable.UniversalVariable.put('inputBeneficiaryReferenceNu', inpBeneRefNum)

transferMethod = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-entry/V-transferMethod'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('transferMethod', transferMethod)

amount = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-EntryScreen/V-amount'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('amount', amount)

chargeInstruction = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-entry/V-chargeInstruction'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('chargeInstruction', chargeInstruction)

debitCharge = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-entry/V-debitCharge'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('debitCharge', debitCharge)

beneficiaryCategory = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-EntryScreen/V-beneficiaryCategory'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('beneficiaryCategory', beneficiaryCategory)

transactorRelationship = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-EntryScreen/V-transactorRelationship'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('transactorRelationship', transactorRelationship)

identicalStatus = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-EntryScreen/V-identicalStatus'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('identicalStatus', identicalStatus)

purposeOfTransaction = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-EntryScreen/V-purposeOfTransaction'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('purposeOfTransaction', purposeOfTransaction)

LHBUPurposeCode = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-EntryScreen/V-LHBUPurposeCode'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('LHBUPurposeCode', LHBUPurposeCode)

LHBUDocumentType = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-EntryScreen/V-LHBUDocumentType'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('LHBUDocumentType', LHBUDocumentType)

GlobalVariable.UniversalVariable.put('LHBUDocumentTypeDescription', LHBUDocumentTypeDescription)

instructionMode = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-EntryScreen/V-instructionMode'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('instructionMode', instructionMode)

on = WebUI.getText(findTestObject('MultiUserInhouse(Repeat)/Value-CreateEntryScreen/V-on'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('On', on)

every = WebUI.getText(findTestObject('MultiUserInhouse(Repeat)/Value-CreateEntryScreen/Extend/V-every'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('Every', every)

at1 = WebUI.getText(findTestObject('MultiUserInhouse(Repeat)/Value-CreateEntryScreen/DL-at'), FailureHandling.CONTINUE_ON_FAILURE)

String[] at2 = at1.split('- ')

at = (at2[1])

GlobalVariable.UniversalVariable.put('At', at)

nonWorkingDayInst = WebUI.getText(findTestObject('MultiUserInhouse(Repeat)/Value-CreateEntryScreen/Extend/V-next'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('NonWorkingDayInstruction', nonWorkingDayInst)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/MainForm/B-Confim'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(10, FailureHandling.CONTINUE_ON_FAILURE)

