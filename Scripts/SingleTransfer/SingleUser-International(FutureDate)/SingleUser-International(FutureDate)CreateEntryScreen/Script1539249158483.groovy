import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import java.util.Locale as Locale
import java.text.DecimalFormat as DecimalFormat
import java.text.NumberFormat as NumberFormat

WebUI.openBrowser('')

WebUI.navigateToUrl('https://10.194.8.106:39990/')

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-CorpID'), 0)

WebUI.setText(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-CorpID'), GlobalVariable.MultiUserLoginCorpregId)

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-UserID'), 0)

WebUI.setText(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-UserID'), GlobalVariable.MultiUserLoginId)

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-UserPass'), 0)

WebUI.setText(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-UserPass'), GlobalVariable.MultiUserPass)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/B-Login'))

WebUI.delay(5)

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/B-Menu'), 0)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/B-Menu'))

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TransferManagement'), 0)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TransferManagement'))

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/SingleTransfer'), 0)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/SingleTransfer'))

WebUI.delay(5)

WebUI.click(findTestObject('MultiUserInternational(Immediate)/Value-CreateEntryScreen/B-internationalForeignCurrencyTransafer'))

WebUI.delay(3)

WebUI.click(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccount'))

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), GlobalVariable.userYanuarJPY)

WebUI.delay(5)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/MainForm/V-Tfrom'))

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/MainForm/V-FAccDes'), 0)

WebUI.setText(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/MainForm/V-FAccDes'), 'PangeranInhouseImmediate')

WebUI.delay(5)

WebUI.click(findTestObject('MultiUserInternational(Immediate)/Value-CreateEntryScreen/TF-selectBeneficiary'))

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), '654654645456')

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('MultiUserDomestic(Repeat)SKN/Value-EntryScreen/TF-toAccountDescription'), 'money')

WebUI.setText(findTestObject('MultiUserDomestic(Repeat)SKN/Value-EntryScreen/TF-beneficiaryReferenceNumber'), '19283190219')

WebUI.click(findTestObject('MultiUserInternational(Immediate)/Value-CreateEntryScreen/DL-remittanceCurrency'))

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), 'JPY')

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('MultiUserInternational(Immediate)/Value-CreateEntryScreen/DL-selectAmount'))

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), Keys.chord(Keys.ENTER))

WebUI.waitForElementVisible(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-amount'), 0)

Random rand = new Random()

int angka

angka = rand.nextInt(999)

angka1 = ('3' + angka)

Locale local1 = new Locale('en', 'UK')

DecimalFormat amountInquiry = ((NumberFormat.getNumberInstance(local1)) as DecimalFormat)

amountInquiry.applyPattern('###,###,##0.00')

String amountInquiry1 = amountInquiry.format(new BigDecimal(angka1))

GlobalVariable.Amount7 = amountInquiry1

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-amount'), angka1)

WebUI.click(findTestObject('SingleUserInternational(Immediate)/DL-Borne by Remitter'))

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), 'borne by remitter')

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), Keys.chord(Keys.ENTER))

WebUI.delay(2)

WebUI.click(findTestObject('MultiUserInternational(Immediate)/Value-CreateEntryScreen/TF-beneficiaryCategory'))

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/MainForm/V-BenefCateg'))

WebUI.delay(1)

WebUI.click(findTestObject('MultiUserInternational(Immediate)/Value-CreateEntryScreen/TF-transactorRelationship'))

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/MainForm/V-TransactorRela'))

WebUI.click(findTestObject('MultiUserInternational(Immediate)/Value-CreateEntryScreen/TF-purposeOfTransaction'))

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/MainForm/V-PuposeOfTransaction'))

WebUI.click(findTestObject('MultiUserInternational(Immediate)/Value-CreateEntryScreen/DL-LHBUPurposeCode'))

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/MainForm/V-PuposeCode'))

WebUI.click(findTestObject('MultiUserInternational(Immediate)/Value-CreateEntryScreen/DL-LHBUDocumentType'))

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/MainForm/V-DocType'))

WebUI.setText(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/MainForm/TFA-DocTypeDes'), 'aduhai')

WebUI.click(findTestObject('MultiUserInhouse(FutureDate)/Value-CreateEntryScreen/B-specificDate'))

WebUI.click(findTestObject('MultiUserInhouse(FutureDate)/Value-CreateEntryScreen/TF-specificDate'))

WebUI.click(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-ConfirmScreen/v-nilaiSession'))

WebUI.delay(2)

WebUI.click(findTestObject('MultiUserInhouse(FutureDate)/Value-CreateEntryScreen/V-day'))

WebUI.click(findTestObject('MultiUserInhouse(FutureDate)/Value-CreateEntryScreen/DL-session'))

WebUI.click(findTestObject('MultiUserInternational(Immediate)/Value-CreateConfirmScreen/V-sessions'))

WebUI.click(findTestObject('MultiUserInhouse(FutureDate)/Value-CreateEntryScreen/TF-expiredFutureDate'))

WebUI.click(findTestObject('MultiUserInhouse(FutureDate)/Value-CreateEntryScreen/V-expiredFutureDate'))

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/MainForm/B-Confim'))

WebUI.click(findTestObject('MultiUserInternational(Immediate)/Value-CreateConfirmScreen/B-submitBox'))

WebUI.delay(10)

