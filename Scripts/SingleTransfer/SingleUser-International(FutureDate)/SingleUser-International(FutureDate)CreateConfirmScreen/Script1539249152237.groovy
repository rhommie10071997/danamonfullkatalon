import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

transFrom = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-ApproverConfrimScreen/V-transferFrom'))

GlobalVariable.SenderCode = transFrom.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.SenderCode, GlobalVariable.resultUserYanuarJPY, false)

fromAccDes = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-ApproverConfrimScreen/V-fromAccountDescription'))

GlobalVariable.FromAccDes = fromAccDes.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.FromAccDes, 'PangeranInhouseImmediate', true)

transTo = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-ApproverConfrimScreen/V-transferTo'))

GlobalVariable.TransferTo = transTo.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.TransferTo, 'Beneficiary List', true)

accNumber = WebUI.getText(findTestObject('MultiUserDomestic(Repeat)SKN/Value-ConfirmScreen/V-accountNumber'))

GlobalVariable.accountNumber = accNumber.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.accountNumber, GlobalVariable.accountNumber, true)

benefRefNo = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-ApproverConfrimScreen/V-beneficiaryReffNumber'))

GlobalVariable.BenefRefNo = benefRefNo.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.BenefRefNo, '19283190219', true)

toAccDes = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-ApproverConfrimScreen/V-toAccountDescription'))

GlobalVariable.ToAccDes = toAccDes.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.ToAccDes, 'money', true)

remiCurrency = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-CreateConfirmScreen/V-remittanceCurrency'))

GlobalVariable.RemittanceCurrency = remiCurrency.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.RemittanceCurrency, 'JPY', true)

amount = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-ApproverConfrimScreen/V-amount'))

GlobalVariable.Amount = amount.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.Amount, GlobalVariable.Amount, true)

fuAmoCharge = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-CreateConfirmScreen/V-fullAmountCharge'))

GlobalVariable.FullAmountCharge = fuAmoCharge.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.FullAmountCharge, 'Borne by Remitter', true)

debCharge = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-CreateConfirmScreen/V-debitCharge'))

GlobalVariable.DebitCharge = debCharge.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.DebitCharge, 'Combine', true)

provision = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-CreateConfirmScreen/V-provision'))

GlobalVariable.Provision = provision.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.Provision, 'IDR   10,000.00', true)

inLieu = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-CreateConfirmScreen/V-inLieu'))

GlobalVariable.InLieu = inLieu.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.InLieu, 'USD   5.00', true)

fuAmoCharge2 = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-CreateConfirmScreen/V-fullAmountCharge1'))

GlobalVariable.FullAmountCharge2 = fuAmoCharge2.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.FullAmountCharge2, 'USD   25.00', true)

cabFee = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-CreateConfirmScreen/V-cableFee'))

GlobalVariable.CableFee = cabFee.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.CableFee, 'IDR   10,000.00', true)

MinTransFee = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-CreateConfirmScreen/V-minimunTransactionFee'))

GlobalVariable.MinimumTransactionFee = MinTransFee.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.MinimumTransactionFee, 'IDR   10,000.00', true)

corresBankFee = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-CreateConfirmScreen/V-correspondentBankFee'))

GlobalVariable.CorrespondentBankFee = corresBankFee.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.CorrespondentBankFee, 'IDR   10,000.00', true)

totCharge = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-ApproverConfrimScreen/V-totalCharge'))

GlobalVariable.TotalCharge = totCharge.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.TotalCharge, GlobalVariable.TotalCharge, true)

charToRemitter = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-CreateConfirmScreen/V-chargeToRemitter'))

GlobalVariable.ChargeToRemitter = charToRemitter.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.ChargeToRemitter, GlobalVariable.ChargeToRemitter, true)

charToBeneficiary = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-CreateConfirmScreen/V-chargeToBeneficiary'))

GlobalVariable.ChargeToBeneficiary = charToBeneficiary.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.ChargeToBeneficiary, GlobalVariable.ChargeToBeneficiary, true)

totDebAmount = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-ApproverConfrimScreen/V-totalDebit'))

GlobalVariable.TotalDebet = totDebAmount.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.TotalDebet, GlobalVariable.TotalDebet, true)

benefResisStat = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-ApproverConfrimScreen/V-beneficiaryResistantStatus'))

GlobalVariable.BenefResidentStatus = benefResisStat.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.BenefResidentStatus, 'Resident', true)

benefCate = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-ApproverConfrimScreen/V-beneficiaryCategory'))

GlobalVariable.BenefCate = benefCate.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.BenefCate, '3 - Pemerintah', true)

transRela = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-ApproverConfrimScreen/V-transactorRelationship'))

GlobalVariable.TransRela = transRela.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.TransRela, 'A - Affiliated', true)

idenStatus = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-ApproverConfrimScreen/V-identicalStatus'))

GlobalVariable.IdenticalStatus = idenStatus.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.IdenticalStatus, 'Identical', true)

purpOfTrans = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverConfrimScreen/V-puposeOfTransaction'))

GlobalVariable.PurpOfTrans = purpOfTrans.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.PurpOfTrans, '2011 - Ekspor barang', true)

purpCode = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-ApproverConfrimScreen/V-LHBUPurposeCode'))

GlobalVariable.PurpCode = purpCode.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.PurpCode, '00 - 00 Investasi Penyertaan Langsung', true)

docType = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-ApproverConfrimScreen/V-LHBUDocumentType'))

GlobalVariable.DocType = docType.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.DocType, '001 - 001 Fotokopi Pemberitahuan Impor Barang PIB', true)

docTypeDes = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-ApproverConfrimScreen/V-LHBUDocumentTypeDescription'))

GlobalVariable.DocTypeDes = docTypeDes.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.DocTypeDes, 'aduhai', true)

instrucMode = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-ApproverConfrimScreen/V-intructionMode'))

GlobalVariable.InstructionMode = instrucMode.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.InstructionMode, 'Specific Date', true)

futDate = WebUI.getText(findTestObject('MultiUserInternational(FutureDate)/Value-CreateConfirm/V-On'))

GlobalVariable.FutureDate = futDate.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.FutureDate, GlobalVariable.FutureDate, true)

at = WebUI.getText(findTestObject('MultiUserInternational(FutureDate)/Value-CreateConfirm/V-SessionTime'))

GlobalVariable.At = at.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.At, GlobalVariable.At, true)

expMode = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-ApproverConfrimScreen/V-expiredOn'))

GlobalVariable.InstructionMode = expMode.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.ExpiredOn, GlobalVariable.ExpiredOn, true)

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-responseCode'), '123456')

WebUI.click(findTestObject('SingleUserInhouse(immediate)/EntryScreen/B-Submit'))

WebUI.delay(2)

WebUI.click(findTestObject('SingleUserInhouse(immediate)/EntryScreen/B-Submit2'))

WebUI.delay(5)

