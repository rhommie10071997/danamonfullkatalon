import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

Reciever = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/MainForm/Value-Confirm/V-ReffNo'), 
    FailureHandling.CONTINUE_ON_FAILURE)

Tampung = Reciever.replace('Reference No : ', '')

GlobalVariable.ReffNo = Tampung

transFrom = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-ApproverConfrimScreen/V-transferFrom'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.SenderCode = transFrom.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.SenderCode, GlobalVariable.resultUserYanuarJPY, false, FailureHandling.CONTINUE_ON_FAILURE)

fromAccDes = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-ApproverConfrimScreen/V-fromAccountDescription'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.FromAccDes = fromAccDes.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.FromAccDes, 'PangeranInhouseImmediate', true, FailureHandling.CONTINUE_ON_FAILURE)

transTo = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-ApproverConfrimScreen/V-transferTo'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.TransferTo = transTo.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.TransferTo, 'New Beneficiary', true, FailureHandling.CONTINUE_ON_FAILURE)

beneBankCon = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-CreateConfirmScreen/V-beneficiaryBankCountry'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.BeneficiaryBankCountry = beneBankCon.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.BeneficiaryBankCountry, 'ID - INDONESIA', true, FailureHandling.CONTINUE_ON_FAILURE)

natOrgaDirectory = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-CreateConfirmScreen/V-nationalOrganizationDirectory'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.NationalOrganizationDirectory = natOrgaDirectory.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.NationalOrganizationDirectory, 'SWIFT - SWIFT1', true, FailureHandling.CONTINUE_ON_FAILURE)

beneBank = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-CreateConfirmScreen/V-beneficiaryBank'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.BeneficiaryBank = beneBank.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.BeneficiaryBank, 'INDONESIA - BANKDANAMON', true, FailureHandling.CONTINUE_ON_FAILURE)

bankCity = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-CreateConfirmScreen/V-bankCity'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.BankCity = bankCity.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.BankCity, GlobalVariable.BankCity, true, FailureHandling.CONTINUE_ON_FAILURE)

interBank = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-CreateConfirmScreen/V-intermediaryBank'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.IntermediaryBank = interBank.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.IntermediaryBank, 'rere123 - hgju ju', true, FailureHandling.CONTINUE_ON_FAILURE)

accNumber = WebUI.getText(findTestObject('MultiUserDomestic(Repeat)SKN/Value-ConfirmScreen/V-accountNumber'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.accountNumber = accNumber.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.accountNumber, GlobalVariable.accountNumber, true, FailureHandling.CONTINUE_ON_FAILURE)

savToBeneList = WebUI.getText(findTestObject('MultiUserDomestic(Repeat)SKN/Value-ConfirmScreen/V-saveToBeneficiaryList'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.saveToBeneficiaryList = savToBeneList.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.saveToBeneficiaryList, GlobalVariable.saveToBeneficiaryList, true, FailureHandling.CONTINUE_ON_FAILURE)

accName = WebUI.getText(findTestObject('MultiUserDomestic(Repeat)SKN/Value-ConfirmScreen/V-accountName'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.AccName = accName.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.AccName, GlobalVariable.AccName, true, FailureHandling.CONTINUE_ON_FAILURE)

alName = WebUI.getText(findTestObject('MultiUserDomestic(Repeat)SKN/Value-ConfirmScreen/V-aliasName'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.aliasName = alName.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.aliasName, GlobalVariable.aliasName, true, FailureHandling.CONTINUE_ON_FAILURE)

address1 = WebUI.getText(findTestObject('MultiUserDomestic(Repeat)SKN/Value-ConfirmScreen/V-address1'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.tempAddress1 = address1.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.tempAddress1, GlobalVariable.address1, true, FailureHandling.CONTINUE_ON_FAILURE)

address2 = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-CreateConfirmScreen/V-address2'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.tempAddress2 = address2.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.tempAddress2, GlobalVariable.address2, true, FailureHandling.CONTINUE_ON_FAILURE)

email = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-CreateConfirmScreen/V-email'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.ValueBankTypeEmail = email.replace('Email : ', '')

WebUI.verifyMatch(GlobalVariable.ValueBankTypeEmail, GlobalVariable.v7, true, FailureHandling.CONTINUE_ON_FAILURE)

sms = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-CreateConfirmScreen/V-sms'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.ValueBankTypeSms = sms.replace('SMS   : ', '')

WebUI.verifyMatch(GlobalVariable.ValueBankTypeSms, GlobalVariable.v8, true, FailureHandling.CONTINUE_ON_FAILURE)

benefRefNo = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-ApproverConfrimScreen/V-beneficiaryReffNumber'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.BenefRefNo = benefRefNo.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.BenefRefNo, '19283190219', true, FailureHandling.CONTINUE_ON_FAILURE)

toAccDes = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-ApproverConfrimScreen/V-toAccountDescription'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.ToAccDes = toAccDes.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.ToAccDes, 'money', true, FailureHandling.CONTINUE_ON_FAILURE)

remiCurrency = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-CreateConfirmScreen/V-remittanceCurrency'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.RemittanceCurrency = remiCurrency.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.RemittanceCurrency, 'JPY', true, FailureHandling.CONTINUE_ON_FAILURE)

amount = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-ApproverConfrimScreen/V-amount'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Amount = amount.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.Amount, GlobalVariable.Amount, true, FailureHandling.CONTINUE_ON_FAILURE)

fuAmoCharge = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-CreateConfirmScreen/V-fullAmountCharge'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.FullAmountCharge = fuAmoCharge.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.FullAmountCharge, 'Borne by Remitter', true, FailureHandling.CONTINUE_ON_FAILURE)

debCharge = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-CreateConfirmScreen/V-debitCharge'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.DebitCharge = debCharge.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.DebitCharge, 'Combine', true, FailureHandling.CONTINUE_ON_FAILURE)

provision = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-CreateConfirmScreen/V-provision'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Provision = provision.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.Provision, 'IDR   10,000.00', true, FailureHandling.CONTINUE_ON_FAILURE)

inLieu = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-CreateConfirmScreen/V-inLieu'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.InLieu = inLieu.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.InLieu, 'USD   5.00', true, FailureHandling.CONTINUE_ON_FAILURE)

fuAmoCharge2 = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-CreateConfirmScreen/V-fullAmountCharge1'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.FullAmountCharge2 = fuAmoCharge2.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.FullAmountCharge2, 'USD   25.00', true, FailureHandling.CONTINUE_ON_FAILURE)

cabFee = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-CreateConfirmScreen/V-cableFee'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.CableFee = cabFee.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.CableFee, 'IDR   10,000.00', true, FailureHandling.CONTINUE_ON_FAILURE)

MinTransFee = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-CreateConfirmScreen/V-minimunTransactionFee'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.MinimumTransactionFee = MinTransFee.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.MinimumTransactionFee, 'IDR   10,000.00', true, FailureHandling.CONTINUE_ON_FAILURE)

corresBankFee = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-CreateConfirmScreen/V-correspondentBankFee'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.CorrespondentBankFee = corresBankFee.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.CorrespondentBankFee, 'IDR   10,000.00', true, FailureHandling.CONTINUE_ON_FAILURE)

totCharge = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-ApproverConfrimScreen/V-totalCharge'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.TotalCharge = totCharge.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.TotalCharge, GlobalVariable.TotalCharge, true, FailureHandling.CONTINUE_ON_FAILURE)

charToRemitter = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-CreateConfirmScreen/V-chargeToRemitter'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.ChargeToRemitter = charToRemitter.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.ChargeToRemitter, GlobalVariable.ChargeToRemitter, true, FailureHandling.CONTINUE_ON_FAILURE)

charToBeneficiary = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-CreateConfirmScreen/V-chargeToBeneficiary'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.ChargeToBeneficiary = charToBeneficiary.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.ChargeToBeneficiary, GlobalVariable.ChargeToBeneficiary, true, FailureHandling.CONTINUE_ON_FAILURE)

totDebAmount = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-ApproverConfrimScreen/V-totalDebit'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.TotalDebet = totDebAmount.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.TotalDebet, GlobalVariable.TotalDebet, true, FailureHandling.CONTINUE_ON_FAILURE)

benefResisStat = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-ApproverConfrimScreen/V-beneficiaryResistantStatus'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.BenefResidentStatus = benefResisStat.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.BenefResidentStatus, 'Resident', true, FailureHandling.CONTINUE_ON_FAILURE)

benefCate = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-ApproverConfrimScreen/V-beneficiaryCategory'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.BenefCate = benefCate.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.BenefCate, '3 - Pemerintah', true, FailureHandling.CONTINUE_ON_FAILURE)

transRela = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-ApproverConfrimScreen/V-transactorRelationship'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.TransRela = transRela.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.TransRela, 'A - Affiliated', true, FailureHandling.CONTINUE_ON_FAILURE)

idenStatus = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-ApproverConfrimScreen/V-identicalStatus'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.IdenticalStatus = idenStatus.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.IdenticalStatus, 'Identical', true, FailureHandling.CONTINUE_ON_FAILURE)

purpOfTrans = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverConfrimScreen/V-puposeOfTransaction'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.PurpOfTrans = purpOfTrans.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.PurpOfTrans, '2011 - Ekspor barang', true, FailureHandling.CONTINUE_ON_FAILURE)

purpCode = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-ApproverConfrimScreen/V-LHBUPurposeCode'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.PurpCode = purpCode.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.PurpCode, '00 - 00 Investasi Penyertaan Langsung', true, FailureHandling.CONTINUE_ON_FAILURE)

docType = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-ApproverConfrimScreen/V-LHBUDocumentType'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.DocType = docType.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.DocType, '001 - 001 Fotokopi Pemberitahuan Impor Barang PIB', true, FailureHandling.CONTINUE_ON_FAILURE)

docTypeDes = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-ApproverConfrimScreen/V-LHBUDocumentTypeDescription'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.DocTypeDes = docTypeDes.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.DocTypeDes, 'aduhai', true, FailureHandling.CONTINUE_ON_FAILURE)

instrucMode = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-ApproverConfrimScreen/V-intructionMode'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.InstructionMode = instrucMode.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.InstructionMode, 'Immediate', true, FailureHandling.CONTINUE_ON_FAILURE)

expOn = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)RTGS/Value-ApproverConfrimScreen/V-expiredOn'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.ExpiredOn = expOn.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.ExpiredOn, GlobalVariable.ExpiredOn, true, FailureHandling.CONTINUE_ON_FAILURE)

