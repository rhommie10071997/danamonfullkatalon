import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

menu = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-TransactionStatusReffNo/1/V-menu'))

GlobalVariable.Menu = menu.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.Menu, 'Single Transfer', true)

product = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-TransactionStatusReffNo/1/V-product'))

GlobalVariable.Product = product.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.Product, 'International Foreign Currency Transfer', false)

refNum = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-TransactionStatusReffNo/V-transactionRefferenceNumber'))

GlobalVariable.ApproverRefNo = refNum.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.ApproverRefNo, GlobalVariable.ReffNo, true)

date = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-TransactionStatusReffNo/1/V-date'))

GlobalVariable.Date = date.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.Date, GlobalVariable.Date, false)

transFrom = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-TransactionStatusReffNo/1/V-transferFrom'))

GlobalVariable.SenderCode = transFrom.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.SenderCode, GlobalVariable.resultUserYanuarJPY, false)

fromAccDes = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-TransactionStatusReffNo/1/V-fromAccountDescription'))

GlobalVariable.FromAccDes = fromAccDes.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.FromAccDes, 'PangeranInhouseImmediate', true)

transTo = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-TransactionStatusReffNo/1/V-transferTo'))

GlobalVariable.TransferTo = transTo.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.TransferTo, 'New Beneficiary', true)

beneBankCon = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-TransactionStatusReffNo/V-beneficiaryBankCountry'))

GlobalVariable.BeneficiaryBankCountry = beneBankCon.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.BeneficiaryBankCountry, 'ID - INDONESIA', true)

natOrgaDirectory = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-TransactionStatusReffNo/V-nationalOrganizationDirectory'))

GlobalVariable.NationalOrganizationDirectory = natOrgaDirectory.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.NationalOrganizationDirectory, 'SWIFT - SWIFT1', true)

beneBank = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-TransactionStatusReffNo/V-beneficiaryBank'))

GlobalVariable.BeneficiaryBank = beneBank.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.BeneficiaryBank, 'INDONESIA - BANKDANAMON', true)

bankCity = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-TransactionStatusReffNo/V-bankCity'))

GlobalVariable.BankCity = bankCity.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.BankCity, GlobalVariable.BankCity, true)

interBank = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-TransactionStatusReffNo/V-intermediaryBank'))

GlobalVariable.IntermediaryBank = interBank.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.IntermediaryBank, 'rere123 - hgju ju', true)

accNumber = WebUI.getText(findTestObject('SingleUserDometstic(Repeat)SKN/Value-TransactionStatusReffNo/V-accountNumber'))

GlobalVariable.accountNumber = accNumber.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.accountNumber, GlobalVariable.accountNumber, true)

savToBeneList = WebUI.getText(findTestObject('MultiUserDomestic(Repeat)SKN/Value-TransactionStatusReffNo/V-saveToBeneficiaryList'))

GlobalVariable.saveToBeneficiaryList = savToBeneList.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.saveToBeneficiaryList, GlobalVariable.saveToBeneficiaryList, true)

accName = WebUI.getText(findTestObject('MultiUserDomestic(Repeat)SKN/Value-TransactionStatusReffNo/V-accountName'))

GlobalVariable.AccName = accName.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.AccName, GlobalVariable.AccName, true)

alName = WebUI.getText(findTestObject('MultiUserDomestic(Repeat)SKN/Value-TransactionStatusReffNo/V-aliasName'))

GlobalVariable.aliasName = alName.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.aliasName, GlobalVariable.aliasName, true)

address1 = WebUI.getText(findTestObject('MultiUserDomestic(Repeat)SKN/Value-TransactionStatusReffNo/V-address1'))

GlobalVariable.tempAddress1 = address1.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.tempAddress1, GlobalVariable.address1, true)

address2 = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-TransactionStatusReffNo/V-address2'))

GlobalVariable.tempAddress2 = address2.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.tempAddress2, GlobalVariable.address2, true)

email = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-TransactionStatusReffNo/V-email'))

GlobalVariable.ValueBankTypeEmail = email.replace('Email : ', '')

WebUI.verifyMatch(GlobalVariable.ValueBankTypeEmail, GlobalVariable.v7, true)

sms = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-TransactionStatusReffNo/V-sms'))

GlobalVariable.ValueBankTypeSms = sms.replace('SMS   : ', '')

WebUI.verifyMatch(GlobalVariable.ValueBankTypeSms, GlobalVariable.v8, true)

benefRefNo = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-TransactionStatusReffNo/1/V-beneficiaryReffNumber'))

GlobalVariable.BenefRefNo = benefRefNo.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.BenefRefNo, '19283190219', true)

toAccDes = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-TransactionStatusReffNo/1/V-toAccountDescription'))

GlobalVariable.ToAccDes = toAccDes.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.ToAccDes, 'money', true)

remiCurrency = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-TransactionStatusReffNo/V-remittanceCurrency'))

GlobalVariable.RemittanceCurrency = remiCurrency.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.RemittanceCurrency, 'JPY', true)

amount = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-TransactionStatusReffNo/V-amount'))

GlobalVariable.Amount = amount.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.Amount, GlobalVariable.Amount, true)

fuAmoCharge = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-TransactionStatusReffNo/V-fullAmountCharge'))

GlobalVariable.FullAmountCharge = fuAmoCharge.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.FullAmountCharge, 'Borne by Remitter', true)

debCharge = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-TransactionStatusReffNo/V-debitCharge'))

GlobalVariable.DebitCharge = debCharge.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.DebitCharge, 'Combine', true)

provision = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-TransactionStatusReffNo/V-provision'))

GlobalVariable.Provision = provision.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.Provision, 'IDR   10,000.00', true)

inLieu = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-TransactionStatusReffNo/V-inLieu'))

GlobalVariable.InLieu = inLieu.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.InLieu, 'USD   5.00', true)

fuAmoCharge2 = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-TransactionStatusReffNo/V-fullAmountCharge1'))

GlobalVariable.FullAmountCharge2 = fuAmoCharge2.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.FullAmountCharge2, 'USD   25.00', true)

cabFee = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-TransactionStatusReffNo/V-cableFee'))

GlobalVariable.CableFee = cabFee.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.CableFee, 'IDR   10,000.00', true)

MinTransFee = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-TransactionStatusReffNo/V-minimunTransactionFee'))

GlobalVariable.MinimumTransactionFee = MinTransFee.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.MinimumTransactionFee, 'IDR   10,000.00', true)

corresBankFee = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-TransactionStatusReffNo/V-correspondentBankFee'))

GlobalVariable.CorrespondentBankFee = corresBankFee.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.CorrespondentBankFee, 'IDR   10,000.00', true)

totCharge = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-TransactionStatusReffNo/V-totalCharge'))

GlobalVariable.TotalCharge = totCharge.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.TotalCharge, GlobalVariable.TotalCharge, true)

charToRemitter = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-TransactionStatusReffNo/V-chargeToRemitter'))

GlobalVariable.ChargeToRemitter = charToRemitter.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.ChargeToRemitter, GlobalVariable.ChargeToRemitter, true)

charToBeneficiary = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-TransactionStatusReffNo/V-chargeToBeneficiary'))

GlobalVariable.ChargeToBeneficiary = charToBeneficiary.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.ChargeToBeneficiary, GlobalVariable.ChargeToBeneficiary, true)

totDebAmount = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-TransactionStatusReffNo/V-totalDebit'))

GlobalVariable.TotalDebet = totDebAmount.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.TotalDebet, GlobalVariable.TotalDebet, true)

benefResisStat = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-TransactionStatusReffNo/V-beneficiaryResistantStatus'))

GlobalVariable.BenefResidentStatus = benefResisStat.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.BenefResidentStatus, 'Resident', true)

benefCate = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-TransactionStatusReffNo/V-beneficiaryCategory'))

GlobalVariable.BenefCate = benefCate.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.BenefCate, '3 - Pemerintah', true)

transRela = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-TransactionStatusReffNo/1/V-transactorRelationship'))

GlobalVariable.TransRela = transRela.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.TransRela, 'A - Affiliated', true)

idenStatus = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-TransactionStatusReffNo/1/V-identicalStatus'))

GlobalVariable.IdenticalStatus = idenStatus.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.IdenticalStatus, 'Identical', true)

purpOfTrans = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-TransactionStatusReffNo/1/V-puposeOfTransaction'))

GlobalVariable.PurpOfTrans = purpOfTrans.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.PurpOfTrans, '2011 - Ekspor barang', true)

purpCode = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-TransactionStatusReffNo/1/V-LHBUPurposeCode'))

GlobalVariable.PurpCode = purpCode.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.PurpCode, '00 - 00 Investasi Penyertaan Langsung', true)

docType = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-TransactionStatusReffNo/1/V-LHBUDocumentType'))

GlobalVariable.DocType = docType.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.DocType, '001 - 001 Fotokopi Pemberitahuan Impor Barang PIB', true)

docTypeDes = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-TransactionStatusReffNo/1/V-LHBUDocumentTypeDescription'))

GlobalVariable.DocTypeDes = docTypeDes.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.DocTypeDes, 'aduhai', true)

instrucMode = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-TransactionStatusReffNo/1/V-intructionMode'))

GlobalVariable.InstructionMode = instrucMode.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.InstructionMode, 'Immediate', true)

expOn = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-TransactionStatusReffNo/1/V-expiredOn'))

GlobalVariable.ExpiredOn = expOn.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.ExpiredOn, GlobalVariable.ExpiredOn, true)

WebUI.delay(2)

