import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(10, FailureHandling.CONTINUE_ON_FAILURE)

menu1 = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverConfirmScreen/V-menu'), FailureHandling.CONTINUE_ON_FAILURE)

menu = menu1.replace(': ', '')

WebUI.verifyMatch(menu, GlobalVariable.UniversalVariable.get('menu'), true, FailureHandling.CONTINUE_ON_FAILURE)

product1 = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverConfirmScreen/v-product'), FailureHandling.CONTINUE_ON_FAILURE)

product = product1.replace(': ', '')

WebUI.verifyMatch(product, GlobalVariable.UniversalVariable.get('product'), false, FailureHandling.CONTINUE_ON_FAILURE)

refNum = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverConfirmScreen/v-transactionRefferenceNumber'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.ApproverRefNo = refNum.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.ApproverRefNo, GlobalVariable.ReffNo, true, FailureHandling.CONTINUE_ON_FAILURE)

docNum = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverConfirmScreen/V-documentCode'), FailureHandling.CONTINUE_ON_FAILURE)

documentNumber = docNum.replace(': ', '')

WebUI.verifyMatch(documentNumber, GlobalVariable.UniversalVariable.get('documentNumber'), true, FailureHandling.CONTINUE_ON_FAILURE)

date1 = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverConfirmScreen/V-date'), FailureHandling.CONTINUE_ON_FAILURE)

date = date1.replace(': ', '')

WebUI.verifyMatch(date, GlobalVariable.UniversalVariable.get('date'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

transferFrom1 = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-ConfirmScreen/V-transferForm'), FailureHandling.CONTINUE_ON_FAILURE)

transferFrom = transferFrom1.replace(': ', '')

WebUI.verifyMatch(transferFrom, GlobalVariable.UniversalVariable.get('transferFrom'), false, FailureHandling.CONTINUE_ON_FAILURE)

fromAccountDes1 = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-ConfirmScreen/V-fromAccountDescription'), 
    FailureHandling.CONTINUE_ON_FAILURE)

fromAccountDes = fromAccountDes1.replace(': ', '')

WebUI.verifyMatch(fromAccountDes, GlobalVariable.UniversalVariable.get('fromAccountDescription'), true, FailureHandling.CONTINUE_ON_FAILURE)

transferTo1 = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-ConfirmScreen/V-transferTo'), FailureHandling.CONTINUE_ON_FAILURE)

transferTo = transferTo1.replace(': ', '')

WebUI.verifyMatch(transferTo, GlobalVariable.UniversalVariable.get('transferTo'), true, FailureHandling.CONTINUE_ON_FAILURE)

accNum1 = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-ConfirmScreen/V-accountNumber'), FailureHandling.CONTINUE_ON_FAILURE)

accNum = accNum1.replace(': ', '')

WebUI.verifyMatch(accNum, accNum, true, FailureHandling.CONTINUE_ON_FAILURE)

toAccDes1 = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-ConfirmScreen/V-toAccountDescription'), FailureHandling.CONTINUE_ON_FAILURE)

toAccDes = toAccDes1.replace(': ', '')

WebUI.verifyMatch(toAccDes, GlobalVariable.UniversalVariable.get('toAccountDescription'), true, FailureHandling.CONTINUE_ON_FAILURE)

accName1 = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-ConfirmScreen/V-accountName'), FailureHandling.CONTINUE_ON_FAILURE)

accName = accName1.replace(': ', '')

WebUI.verifyMatch(accName, accName, false, FailureHandling.CONTINUE_ON_FAILURE)

email1 = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-ConfirmScreen/V-email'), FailureHandling.CONTINUE_ON_FAILURE)

email = email1.replace(': ', '')

WebUI.verifyMatch(email, GlobalVariable.UniversalVariable.get('email'), true, FailureHandling.CONTINUE_ON_FAILURE)

sms1 = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-ConfirmScreen/V-sms'), FailureHandling.CONTINUE_ON_FAILURE)

sms = sms1.replace(': ', '')

WebUI.verifyMatch(sms, GlobalVariable.UniversalVariable.get('sms'), true, FailureHandling.CONTINUE_ON_FAILURE)

inpBeneRefNum1 = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-ConfirmScreen/V-beneficiaryReferenceNumber'), 
    FailureHandling.CONTINUE_ON_FAILURE)

inpBeneRefNum = inpBeneRefNum1.replace(': ', '')

WebUI.verifyMatch(inpBeneRefNum, GlobalVariable.UniversalVariable.get('inputBeneficiaryReferenceNu'), true, FailureHandling.CONTINUE_ON_FAILURE)

amount1 = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-ConfirmScreen/V-amount'), FailureHandling.CONTINUE_ON_FAILURE)

amount = amount1.replace(': ', '')

GlobalVariable.Amount = amount

String verifyAmount = 'IDR ' + GlobalVariable.Amount7

GlobalVariable.Amount6 = verifyAmount

WebUI.verifyMatch(GlobalVariable.Amount, GlobalVariable.Amount6, true, FailureHandling.CONTINUE_ON_FAILURE)

exRate1 = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-ConfirmScreen/V-exchangeRate'), FailureHandling.CONTINUE_ON_FAILURE)

exRate = exRate1.replace(': ', '')

WebUI.verifyMatch(exRate, GlobalVariable.UniversalVariable.get('exchangeRate'), true, FailureHandling.CONTINUE_ON_FAILURE)

totCharge1 = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-ConfirmScreen/V-totalCharge'), FailureHandling.CONTINUE_ON_FAILURE)

totCharge = totCharge1.replace(': ', '')

WebUI.verifyMatch(totCharge, GlobalVariable.UniversalVariable.get('totalCharge'), true, FailureHandling.CONTINUE_ON_FAILURE)

totDebAmount1 = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-ConfirmScreen/V-totalDebetAmount'), FailureHandling.CONTINUE_ON_FAILURE)

totDebAmount = totDebAmount1.replace(': ', '')

WebUI.verifyMatch(totDebAmount, GlobalVariable.UniversalVariable.get('totalDebitAmount'), true, FailureHandling.CONTINUE_ON_FAILURE)

chargeInst1 = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-ConfirmScreen/V-chargeInstruction'), FailureHandling.CONTINUE_ON_FAILURE)

chargeInst = chargeInst1.replace(': ', '')

WebUI.verifyMatch(chargeInst, GlobalVariable.UniversalVariable.get('chargeInstruction'), true, FailureHandling.CONTINUE_ON_FAILURE)

debitCharge1 = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-ConfirmScreen/V-debitCharge'), FailureHandling.CONTINUE_ON_FAILURE)

debitCharge = debitCharge1.replace(': ', '')

WebUI.verifyMatch(debitCharge, GlobalVariable.UniversalVariable.get('debitCharge'), true, FailureHandling.CONTINUE_ON_FAILURE)

beneficiaryCategory1 = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-ConfirmScreen/V-beneficiaryCategory'), 
    FailureHandling.CONTINUE_ON_FAILURE)

beneficiaryCategory = beneficiaryCategory1.replace(': ', '')

WebUI.verifyMatch(beneficiaryCategory, GlobalVariable.UniversalVariable.get('beneficiaryCategory'), true, FailureHandling.CONTINUE_ON_FAILURE)

transactorRelationship1 = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-ConfirmScreen/V-transactorRelationship'), 
    FailureHandling.CONTINUE_ON_FAILURE)

transactorRelationship = transactorRelationship1.replace(': ', '')

WebUI.verifyMatch(transactorRelationship, GlobalVariable.UniversalVariable.get('transactorRelationship'), true, FailureHandling.CONTINUE_ON_FAILURE)

identicalStatus1 = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-ConfirmScreen/V-identicalStatus'), FailureHandling.CONTINUE_ON_FAILURE)

identicalStatus = identicalStatus1.replace(': ', '')

WebUI.verifyMatch(identicalStatus, GlobalVariable.UniversalVariable.get('identicalStatus'), true, FailureHandling.CONTINUE_ON_FAILURE)

purposeOfTransaction1 = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-ConfirmScreen/V-purposeOfTransaction'), 
    FailureHandling.CONTINUE_ON_FAILURE)

purposeOfTransaction = purposeOfTransaction1.replace(': ', '')

WebUI.verifyMatch(purposeOfTransaction, GlobalVariable.UniversalVariable.get('purposeOfTransaction'), true, FailureHandling.CONTINUE_ON_FAILURE)

LHBUPurposeCode1 = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-ConfirmScreen/V-LHBUPurposeCode'), FailureHandling.CONTINUE_ON_FAILURE)

LHBUPurposeCode = LHBUPurposeCode1.replace(': ', '')

WebUI.verifyMatch(LHBUPurposeCode, GlobalVariable.UniversalVariable.get('LHBUPurposeCode'), true, FailureHandling.CONTINUE_ON_FAILURE)

LHBUDocumentType1 = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-ConfirmScreen/V-LHBUDocumentType'), 
    FailureHandling.CONTINUE_ON_FAILURE)

LHBUDocumentType = LHBUDocumentType1.replace(': ', '')

WebUI.verifyMatch(LHBUDocumentType, GlobalVariable.UniversalVariable.get('LHBUDocumentType'), true, FailureHandling.CONTINUE_ON_FAILURE)

LHBUDocumentTypreDescription1 = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-ConfirmScreen/V-LHBUDocumentTypreDescription'), 
    FailureHandling.CONTINUE_ON_FAILURE)

LHBUDocumentTypreDescription = LHBUDocumentTypreDescription1.replace(': ', '')

WebUI.verifyMatch(LHBUDocumentTypreDescription, GlobalVariable.UniversalVariable.get('LHBUDocumentTypeDescription'), true, 
    FailureHandling.CONTINUE_ON_FAILURE)

instructionMode1 = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-ConfirmScreen/V-instructionMode'), FailureHandling.CONTINUE_ON_FAILURE)

instructionMode = instructionMode1.replace(': ', '')

WebUI.verifyMatch(instructionMode, GlobalVariable.UniversalVariable.get('instructionMode'), true, FailureHandling.CONTINUE_ON_FAILURE)

expiredOn1 = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-ConfirmScreen/V-expiredOn'), FailureHandling.CONTINUE_ON_FAILURE)

expiredOn = expiredOn1.replace(': ', '')

WebUI.verifyMatch(expiredOn, GlobalVariable.UniversalVariable.get('expiredOn'), true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(4, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

