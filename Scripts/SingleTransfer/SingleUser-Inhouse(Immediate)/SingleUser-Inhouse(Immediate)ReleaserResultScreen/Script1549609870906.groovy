import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(10, FailureHandling.CONTINUE_ON_FAILURE)

menu = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverResultScreen/V-menu'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Menu = menu.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.Menu, 'Single Transfer', true, FailureHandling.CONTINUE_ON_FAILURE)

product = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverResultScreen/V-product'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Product = product.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.Product, 'In-House (Overbooking)', false, FailureHandling.CONTINUE_ON_FAILURE)

refNum = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverResultScreen/V-referenceNumber'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.ApproverRefNo = refNum.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.ApproverRefNo, GlobalVariable.ReffNo, true, FailureHandling.CONTINUE_ON_FAILURE)

docNum = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverResultScreen/V-documentNumber'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.DocNo = docNum.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.DocNo, GlobalVariable.ReffNo, true, FailureHandling.CONTINUE_ON_FAILURE)

date = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverResultScreen/V-date'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Date = date.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.Date, GlobalVariable.Date, false, FailureHandling.CONTINUE_ON_FAILURE)

transFrom = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverResultScreen/V-transferFrom'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.SenderCode = transFrom.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.SenderCode, '000001825736 / ABADY (IDR)', false, FailureHandling.CONTINUE_ON_FAILURE)

fromAccDes = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverResultScreen/V-fromAccountDescription'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.FromAccDes = fromAccDes.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.FromAccDes, 'PangeranInhouseImmediate', true, FailureHandling.CONTINUE_ON_FAILURE)

transTo = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverResultScreen/V-transferToo'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.TransferTo = transTo.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.TransferTo, 'Own account', true, FailureHandling.CONTINUE_ON_FAILURE)

accNumber = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverResultScreen/V-accNumber'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.AccNo = accNumber.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.AccNo, 'money', true, FailureHandling.CONTINUE_ON_FAILURE)

toAccDes = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverResultScreen/V-toAccountDescription'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.ToAccDes = toAccDes.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.ToAccDes, 'money', true, FailureHandling.CONTINUE_ON_FAILURE)

beneficiary = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverResultScreen/V-beneficiaryList'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.AccNo = beneficiary.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.AccNo, '003533751909 / 2 SURYANTI NU (IDR)', false, FailureHandling.CONTINUE_ON_FAILURE)

email = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverResultScreen/V-email'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Email = email.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.Email, 'Pangeran@gmail.com', true, FailureHandling.CONTINUE_ON_FAILURE)

sms = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverResultScreen/V-sms'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Sms = sms.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.Sms, '911911911911', true, FailureHandling.CONTINUE_ON_FAILURE)

benefRefNo = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverResultScreen/V-beneficiaryReferenceNumber'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.BenefRefNo = benefRefNo.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.BenefRefNo, '19283190219', true, FailureHandling.CONTINUE_ON_FAILURE)

amount = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverResultScreen/V-amount'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Amount = amount.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.Amount, GlobalVariable.Amount, true, FailureHandling.CONTINUE_ON_FAILURE)

excRate = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverResultScreen/V-exchangeRate'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.ExchangeRate = excRate.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.ExchangeRate, 'Counter Rate', true, FailureHandling.CONTINUE_ON_FAILURE)

transFee = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverResultScreen/V-transferFee'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.TransFee = transFee.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.TransFee, GlobalVariable.TransFee, true, FailureHandling.CONTINUE_ON_FAILURE)

totCharge = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverResultScreen/V-totalCharge'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.TotalCharge = totCharge.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.TotalCharge, GlobalVariable.TotalCharge, true, FailureHandling.CONTINUE_ON_FAILURE)

totDebit = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverResultScreen/V-totalDebet'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.TotalDebet = totDebit.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.TotalDebet, GlobalVariable.TotalDebet, true, FailureHandling.CONTINUE_ON_FAILURE)

charInstruct = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverResultScreen/V-chargeInstruction'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.ChargeInstruction = charInstruct.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.ChargeInstruction, 'Remitter', true, FailureHandling.CONTINUE_ON_FAILURE)

debCharge = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverResultScreen/V-debetCharge'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.DebitCharge = debCharge.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.DebitCharge, 'Split', true, FailureHandling.CONTINUE_ON_FAILURE)

benefCate = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverResultScreen/V-beneficiaryCategory'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.BenefCate = benefCate.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.BenefCate, '3 - Pemerintah', true, FailureHandling.CONTINUE_ON_FAILURE)

transRela = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverResultScreen/V-transactorRelationship'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.TransRela = transRela.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.TransRela, 'A - Affiliated', true, FailureHandling.CONTINUE_ON_FAILURE)

idenStatus = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverResultScreen/V-identicalStatus'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.IdenticalStatus = idenStatus.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.IdenticalStatus, 'Identical', true, FailureHandling.CONTINUE_ON_FAILURE)

purpOfTrans = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverResultScreen/V-purposeOfTransaction'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.PurpOfTrans = purpOfTrans.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.PurpOfTrans, '2011 - Ekspor barang', true, FailureHandling.CONTINUE_ON_FAILURE)

purpCode = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverResultScreen/V-LHBUPurposeCode'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.PurpCode = purpCode.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.PurpCode, '00 - 00 Investasi Penyertaan Langsung', true, FailureHandling.CONTINUE_ON_FAILURE)

docType = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverResultScreen/V-LHBUDocumentType'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.DocType = docType.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.DocType, '001 - 001 Fotokopi Pemberitahuan Impor Barang PIB', true, FailureHandling.CONTINUE_ON_FAILURE)

docTypeDes = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverResultScreen/V-documentTypeDescription'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.DocTypeDes = docTypeDes.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.DocTypeDes, 'aduhai', true, FailureHandling.CONTINUE_ON_FAILURE)

instrucMode = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverResultScreen/V-instructionMode'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.InstructionMode = instrucMode.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.InstructionMode, 'Immediate', true, FailureHandling.CONTINUE_ON_FAILURE)

expOn = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverResultScreen/V-expiredOn'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.ExpiredOn = expOn.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.ExpiredOn, GlobalVariable.ExpiredOn, true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(15, FailureHandling.CONTINUE_ON_FAILURE)

