import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(3)

WebUI.setText(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-MenuSearch'), 'Account Statement')

WebUI.delay(1)

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/ExternalObject(immeduate)TransactionInquiry/T-AccountStatment'), 
    0)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/ExternalObject(immeduate)TransactionInquiry/T-AccountStatment'))

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/B-Menu'))

WebUI.delay(3)

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/ExternalObject(immeduate)TransactionInquiry/TF-PleaseSelectAccountCluster'), 
    0)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/ExternalObject(immeduate)TransactionInquiry/TF-PleaseSelectAccountCluster'))

WebUI.delay(1)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/ExternalObject(immeduate)TransactionInquiry/V-Tf-AccountCluster'))

WebUI.click(findTestObject('MultiUserDomestic(Immediate)SKN/Value-AccountCluster'))

WebUI.delay(1)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/ExternalObject(immeduate)TransactionInquiry/B-TransactionInquirySearch'))

WebUI.delay(1)

WebUI.waitForElementVisible(findTestObject('tf-acc-num-di-acc-list'), 0)

WebUI.delay(1)

WebUI.setText(findTestObject('tf-acc-num-di-acc-list'), '000001825736')

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/ExternalObject(immeduate)TransactionInquiry/B-SearchAccountLsit'))

WebUI.delay(1)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/ExternalObject(immeduate)TransactionInquiry/Cb-AccDiAccList'))

WebUI.delay(3)

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/ExternalObject(immeduate)TransactionInquiry/B-ShowDiAccList'), 
    0)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/ExternalObject(immeduate)TransactionInquiry/B-ShowDiAccList'))

WebUI.delay(5)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/ExternalObject(immeduate)TransactionInquiry/B-Display'))

WebUI.delay(30)

WebUI.callTestCase(findTestCase('SingleTransfer/TransactionInquiry2'), [('ExpectedValue') : GlobalVariable.Amount7, ('TableXpath') : '//table[@id="globalTableTarget0"]/tbody'
        , ('ColumnNumber') : 6, ('Click') : 'Button Next', ('StopCondition') : [('attribute') : 'class', ('value') : 'next btn grid-nextpage disabled']], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.delay(120)

