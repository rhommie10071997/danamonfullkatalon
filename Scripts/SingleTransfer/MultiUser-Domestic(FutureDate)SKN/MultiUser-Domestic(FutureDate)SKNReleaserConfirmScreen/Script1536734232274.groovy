import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-CorpID'), 0)

WebUI.setText(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-CorpID'), 'corpreg01')

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-UserID'), 0)

WebUI.setText(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-UserID'), 'releaser01')

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-UserPass'), 0)

WebUI.setText(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-UserPass'), 'Password123')

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/B-Login'))

WebUI.delay(15)

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/B-Menu'), 0)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/B-Menu'))

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/PendingTask/T-PendingTask'))

WebUI.delay(2)

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/PendingTask/T-DisplayOption'), 
    0)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/PendingTask/T-DisplayOption'))

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/PendingTask/C-RefNo'), 0)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/PendingTask/C-RefNo'))

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/PendingTask/TF-TransReffNo'))

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/PendingTask/TF-TransReffNo'), 
    0)

WebUI.setText(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/PendingTask/TF-TransReffNo'), GlobalVariable.ReffNo)

WebUI.delay(30)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/PendingTask/B-Show'))

WebUI.doubleClick(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/PendingTask/CB-DetailFormPendingTask'))

WebUI.delay(5)

menu = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverConfrimScreen/V-menu'))

String temp1 = menu.replace(': ', '')

GlobalVariable.Menu = temp1

WebUI.verifyMatch(GlobalVariable.Menu, 'Single Transfer', true)

product = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverConfrimScreen/V-product'))

String temp2 = product.replace(': ', '')

GlobalVariable.Product = temp2

WebUI.verifyMatch(GlobalVariable.Product, 'Domestic Transfer', false)

refNum = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverConfrimScreen/V-transactionReffNumber'))

String temp3 = refNum.replace(': ', '')

GlobalVariable.ApproverRefNo = temp3

WebUI.verifyMatch(GlobalVariable.ApproverRefNo, GlobalVariable.ReffNo, true)

docNum = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverConfrimScreen/V-documentNumber'))

String temp4 = docNum.replace(': ', '')

GlobalVariable.DocCode = temp4

WebUI.verifyMatch(GlobalVariable.DocCode, GlobalVariable.ReffNo, true)

date = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverConfrimScreen/V-date'))

String temp5 = date.replace(': ', '')

GlobalVariable.Date = temp5

WebUI.verifyMatch(GlobalVariable.Date, GlobalVariable.Date, false)

transFrom = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverConfrimScreen/V-transferFrom'))

String temp6 = transFrom.replace(': ', '')

GlobalVariable.SenderCode = temp6

WebUI.verifyMatch(GlobalVariable.SenderCode, '000001825736/ AANG SUBHAN (IDR)', false)

fromAccDes = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverConfrimScreen/V-fromAccountDescription'))

String temp7 = fromAccDes.replace(': ', '')

GlobalVariable.FromAccDes = temp7

WebUI.verifyMatch(GlobalVariable.FromAccDes, 'PangeranInhouseImmediate', true)

transTo = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverConfrimScreen/V-transferTo'))

String temp8 = transTo.replace(': ', '')

GlobalVariable.TransferTo = temp8

WebUI.verifyMatch(GlobalVariable.TransferTo, 'Beneficiary List', true)

beneficiary = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverConfrimScreen/V-beneficiary'))

String temp9 = beneficiary.replace(': ', '')

GlobalVariable.AccNo = temp9

WebUI.verifyMatch(GlobalVariable.AccNo, '3543654657 - Zuskie Azzahra (IDR)', false)

toAccDes = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverConfrimScreen/V-toAccountDescription'))

String temp10 = toAccDes.replace(': ', '')

GlobalVariable.ToAccDes = temp10

WebUI.verifyMatch(GlobalVariable.ToAccDes, 'money', true)

benefRefNo = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverConfrimScreen/V-beneficiaryReffNumber'))

String temp11 = benefRefNo.replace(': ', '')

GlobalVariable.BenefRefNo = temp11

WebUI.verifyMatch(GlobalVariable.BenefRefNo, '19283190219', true)

transMethod = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverConfrimScreen/V-transferMethod'))

String temp12 = transMethod.replace(': ', '')

GlobalVariable.TransferMethod = temp12

WebUI.verifyMatch(GlobalVariable.TransferMethod, 'SKN', true)

benefType = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverConfrimScreen/V-beneficiaryType'))

String temp13 = benefType.replace(': ', '')

GlobalVariable.BenefType = temp13

WebUI.verifyMatch(GlobalVariable.BenefType, '1', true)

amount = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverConfrimScreen/V-amount'))

String temp14 = amount.replace(': ', '')

GlobalVariable.Amount = temp14

WebUI.verifyMatch(GlobalVariable.Amount, GlobalVariable.Amount, true)

excRate = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverConfrimScreen/V-exchangeRate'))

String temp15 = excRate.replace(': ', '')

GlobalVariable.ExchangeRate = temp15

WebUI.verifyMatch(GlobalVariable.ExchangeRate, 'Counter Rate', true)

charInstruct = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverConfrimScreen/V-chargeInstuction'))

String temp16 = charInstruct.replace(': ', '')

GlobalVariable.ChargeInstruction = temp16

WebUI.verifyMatch(GlobalVariable.ChargeInstruction, 'Remitter', true)

debCharge = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverConfrimScreen/V-debitCharge'))

String temp17 = debCharge.replace(': ', '')

GlobalVariable.DebitCharge = temp17

WebUI.verifyMatch(GlobalVariable.DebitCharge, 'Split', true)

sknFee = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverConfrimScreen/V-SKNFee'))

String temp18 = sknFee.replace(': ', '')

GlobalVariable.SKNFee = temp18

WebUI.verifyMatch(GlobalVariable.SKNFee, GlobalVariable.SKNFee, true)

transFee = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverConfrimScreen/V-transferFee'))

String temp19 = transFee.replace(': ', '')

GlobalVariable.TransFee = temp19

WebUI.verifyMatch(GlobalVariable.TransFee, GlobalVariable.TransFee, true)

totCharge = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverConfrimScreen/V-totalCharge'))

String temp20 = totCharge.replace(': ', '')

GlobalVariable.TotalCharge = temp20

WebUI.verifyMatch(GlobalVariable.TotalCharge, GlobalVariable.TotalCharge, true)

totDebit = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverConfrimScreen/V-totalDebit'))

String temp21 = totDebit.replace(': ', '')

GlobalVariable.TotalDebet = temp21

WebUI.verifyMatch(GlobalVariable.TotalDebet, GlobalVariable.TotalDebet, true)

benefResisStat = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverConfrimScreen/V-beneficiaryResistantStatus'))

String temp22 = benefResisStat.replace(': ', '')

GlobalVariable.BenefResidentStatus = temp22

WebUI.verifyMatch(GlobalVariable.BenefResidentStatus, 'Resident', true)

benefCate = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverConfrimScreen/V-beneficiaryCategory'))

String temp23 = benefCate.replace(': ', '')

GlobalVariable.BenefCate = temp23

WebUI.verifyMatch(GlobalVariable.BenefCate, '3 - Pemerintah', true)

transRela = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverConfrimScreen/V-transactorRelationship'))

String temp24 = transRela.replace(': ', '')

GlobalVariable.TransRela = temp24

WebUI.verifyMatch(GlobalVariable.TransRela, 'A - Affiliated', true)

idenStatus = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverConfrimScreen/V-identicalStatus'))

String temp25 = idenStatus.replace(': ', '')

GlobalVariable.IdenticalStatus = temp25

WebUI.verifyMatch(GlobalVariable.IdenticalStatus, 'Identical', true)

purpOfTrans = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverConfrimScreen/V-puposeOfTransaction'))

String temp26 = purpOfTrans.replace(': ', '')

GlobalVariable.PurpOfTrans = temp26

WebUI.verifyMatch(GlobalVariable.PurpOfTrans, '2011 - Ekspor barang', true)

purpCode = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverConfrimScreen/V-LHBUPurposeCode'))

String temp27 = purpCode.replace(': ', '')

GlobalVariable.PurpCode = temp27

WebUI.verifyMatch(GlobalVariable.PurpCode, '00 - 00 Investasi Penyertaan Langsung', true)

docType = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverConfrimScreen/V-LHBUDocumentType'))

String temp28 = docType.replace(': ', '')

GlobalVariable.DocType = temp28

WebUI.verifyMatch(GlobalVariable.DocType, '001 - 001 Fotokopi Pemberitahuan Impor Barang PIB', true)

docTypeDes = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverConfrimScreen/V-LHBUDocumentTypeDescription'))

String temp29 = docTypeDes.replace(': ', '')

GlobalVariable.DocTypeDes = temp29

WebUI.verifyMatch(GlobalVariable.DocTypeDes, 'aduhai', true)

instrucMode = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverConfrimScreen/V-intructionMode'))

String temp30 = instrucMode.replace(': ', '')

GlobalVariable.InstructionMode = temp30

WebUI.verifyMatch(GlobalVariable.InstructionMode, 'Immediate', true)

expOn = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverConfrimScreen/V-expiredOn'))

String temp31 = expOn.replace(': ', '')

GlobalVariable.ExpiredOn = temp31

WebUI.verifyMatch(GlobalVariable.ExpiredOn, GlobalVariable.ExpiredOn, true)

WebUI.delay(2)

WebUI.click(findTestObject('B-Approve'))

WebUI.delay(5)

WebUI.waitForElementVisible(findTestObject('MultiUserDomestic(Immediate)SKN/tf-responseCode'), 0)

WebUI.click(findTestObject('MultiUserDomestic(Immediate)SKN/tf-responseCode'))

WebUI.setText(findTestObject('MultiUserDomestic(Immediate)SKN/tf-responseCode'), '123456')

WebUI.delay(2)

WebUI.click(findTestObject('B-Approve'))

WebUI.delay(2)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/PendingTask/B-FinalOKApprover'))

