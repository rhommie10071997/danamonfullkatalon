import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-CorpID'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-CorpID'), 'corpreg01', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-UserID'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-UserID'), 'releaser01', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-UserPass'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-UserPass'), 'Password123', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/B-Login'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(15, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/B-Menu'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/B-Menu'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/PendingTask/T-PendingTask'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(3, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/PendingTask/T-DisplayOption'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/PendingTask/T-DisplayOption'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/PendingTask/C-RefNo'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/PendingTask/C-RefNo'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/PendingTask/TF-TransReffNo'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/PendingTask/TF-TransReffNo'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/PendingTask/TF-TransReffNo'), GlobalVariable.ReffNo, 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(10, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/PendingTask/B-Show'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.doubleClick(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/PendingTask/CB-DetailFormPendingTask'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(20, FailureHandling.CONTINUE_ON_FAILURE)

menu = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverConfirmScreen/V-menu'), FailureHandling.CONTINUE_ON_FAILURE)

String temp1 = menu.replace(': ', '')

GlobalVariable.Menu = temp1

WebUI.verifyMatch(GlobalVariable.Menu, 'Single Transfer', true, FailureHandling.CONTINUE_ON_FAILURE)

product = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverConfirmScreen/v-product'), FailureHandling.CONTINUE_ON_FAILURE)

String temp2 = product.replace(': ', '')

GlobalVariable.Product = temp2

WebUI.verifyMatch(GlobalVariable.Product, 'In-House (Overbooking)', false, FailureHandling.CONTINUE_ON_FAILURE)

refNum = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverConfirmScreen/v-transactionRefferenceNumber'), 
    FailureHandling.CONTINUE_ON_FAILURE)

String temp3 = refNum.replace(': ', '')

GlobalVariable.ApproverRefNo = temp3

WebUI.verifyMatch(GlobalVariable.ApproverRefNo, GlobalVariable.ReffNo, true, FailureHandling.CONTINUE_ON_FAILURE)

docNum = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverConfirmScreen/V-documentCode'), FailureHandling.CONTINUE_ON_FAILURE)

String temp4 = docNum.replace(': ', '')

GlobalVariable.DocCode = temp4

WebUI.verifyMatch(GlobalVariable.DocCode, GlobalVariable.ReffNo, true, FailureHandling.CONTINUE_ON_FAILURE)

date = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverConfirmScreen/V-date'), FailureHandling.CONTINUE_ON_FAILURE)

String temp5 = date.replace(': ', '')

GlobalVariable.Date = temp5

WebUI.verifyMatch(GlobalVariable.Date, GlobalVariable.Date, false, FailureHandling.CONTINUE_ON_FAILURE)

transFrom = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverConfirmScreen/V-transferFrom'), FailureHandling.CONTINUE_ON_FAILURE)

String temp6 = transFrom.replace(': ', '')

GlobalVariable.SenderCode = temp6

WebUI.verifyMatch(GlobalVariable.SenderCode, '000001825736 / ABADY (IDR)', false, FailureHandling.CONTINUE_ON_FAILURE)

fromAccDes = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverConfirmScreen/V-fromAccountDescription'), 
    FailureHandling.CONTINUE_ON_FAILURE)

String temp7 = fromAccDes.replace(': ', '')

GlobalVariable.FromAccDes = temp7

WebUI.verifyMatch(GlobalVariable.FromAccDes, 'PangeranInhouseImmediate', true, FailureHandling.CONTINUE_ON_FAILURE)

transTo = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverConfirmScreen/V-transferTo'), FailureHandling.CONTINUE_ON_FAILURE)

String temp8 = transTo.replace(': ', '')

GlobalVariable.TransferTo = temp8

WebUI.verifyMatch(GlobalVariable.TransferTo, 'Own account', true, FailureHandling.CONTINUE_ON_FAILURE)

beneficiary = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverConfirmScreen/V-accountNumber'), FailureHandling.CONTINUE_ON_FAILURE)

String temp9 = beneficiary.replace(': ', '')

GlobalVariable.AccNo = temp9

WebUI.verifyMatch(GlobalVariable.AccNo, '003533751909', false, FailureHandling.CONTINUE_ON_FAILURE)

toAccDes = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverConfirmScreen/V-toAccountDescription'), 
    FailureHandling.CONTINUE_ON_FAILURE)

String temp10 = toAccDes.replace(': ', '')

GlobalVariable.ToAccDes = temp10

WebUI.verifyMatch(GlobalVariable.ToAccDes, 'money', true, FailureHandling.CONTINUE_ON_FAILURE)

benefRefNo = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverConfirmScreen/V-accountName'), FailureHandling.CONTINUE_ON_FAILURE)

String temp11 = benefRefNo.replace(': ', '')

GlobalVariable.BenefRefNo = temp11

WebUI.verifyMatch(GlobalVariable.BenefRefNo, '003533751909 / 2 SURYANTI NU (IDR)', false, FailureHandling.CONTINUE_ON_FAILURE)

email = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverConfirmScreen/V-email'), FailureHandling.CONTINUE_ON_FAILURE)

String temp12 = email.replace(': ', '')

GlobalVariable.Email = temp12

WebUI.verifyMatch(GlobalVariable.Email, 'Pangeran@gmail.com', true, FailureHandling.CONTINUE_ON_FAILURE)

sms = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverConfirmScreen/V-sms'), FailureHandling.CONTINUE_ON_FAILURE)

String temp13 = sms.replace(': ', '')

GlobalVariable.Sms = temp13

WebUI.verifyMatch(GlobalVariable.Sms, '911911911911', true, FailureHandling.CONTINUE_ON_FAILURE)

beneRefNo = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverConfirmScreen/V-beneficiaryRefferenceNumber'), 
    FailureHandling.CONTINUE_ON_FAILURE)

String temp14 = beneRefNo.replace(': ', '')

GlobalVariable.BenefRefNo = temp14

WebUI.verifyMatch(GlobalVariable.BenefRefNo, '19283190219', true, FailureHandling.CONTINUE_ON_FAILURE)

amount = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverConfirmScreen/V-amount'), FailureHandling.CONTINUE_ON_FAILURE)

String temp15 = amount.replace(': ', '')

GlobalVariable.Amount = temp15

WebUI.verifyMatch(GlobalVariable.Amount, GlobalVariable.Amount, true, FailureHandling.CONTINUE_ON_FAILURE)

excRate = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverConfirmScreen/V-exchangeRate'), FailureHandling.CONTINUE_ON_FAILURE)

String temp16 = excRate.replace(': ', '')

GlobalVariable.ExchangeRate = temp16

WebUI.verifyMatch(GlobalVariable.ExchangeRate, 'Counter Rate', true, FailureHandling.CONTINUE_ON_FAILURE)

transFee = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverConfirmScreen/V-transferFee'), FailureHandling.CONTINUE_ON_FAILURE)

String temp17 = transFee.replace(': ', '')

GlobalVariable.TransFee = temp17

WebUI.verifyMatch(GlobalVariable.TransFee, GlobalVariable.TransFee, true, FailureHandling.CONTINUE_ON_FAILURE)

totCharge = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverConfirmScreen/V-totalCharge'), FailureHandling.CONTINUE_ON_FAILURE)

String temp18 = totCharge.replace(': ', '')

GlobalVariable.TotalCharge = temp18

WebUI.verifyMatch(GlobalVariable.TotalCharge, GlobalVariable.TotalCharge, true, FailureHandling.CONTINUE_ON_FAILURE)

totDebit = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverConfirmScreen/V-totalDebet'), FailureHandling.CONTINUE_ON_FAILURE)

String temp19 = totDebit.replace(': ', '')

GlobalVariable.TotalDebet = temp19

WebUI.verifyMatch(GlobalVariable.TotalDebet, GlobalVariable.TotalDebet, true, FailureHandling.CONTINUE_ON_FAILURE)

charInstruct = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverConfirmScreen/V-chargeInstruction'), 
    FailureHandling.CONTINUE_ON_FAILURE)

String temp20 = charInstruct.replace(': ', '')

GlobalVariable.ChargeInstruction = temp20

WebUI.verifyMatch(GlobalVariable.ChargeInstruction, 'Remitter', true, FailureHandling.CONTINUE_ON_FAILURE)

debCharge = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverConfirmScreen/V-debitCharge'), FailureHandling.CONTINUE_ON_FAILURE)

String temp21 = debCharge.replace(': ', '')

GlobalVariable.DebitCharge = temp21

WebUI.verifyMatch(GlobalVariable.DebitCharge, 'Split', true, FailureHandling.CONTINUE_ON_FAILURE)

benefCate = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverConfirmScreen/V-beneficiaryCategory'), 
    FailureHandling.CONTINUE_ON_FAILURE)

String temp22 = benefCate.replace(': ', '')

GlobalVariable.BenefCate = temp22

WebUI.verifyMatch(GlobalVariable.BenefCate, '3 - Pemerintah', true, FailureHandling.CONTINUE_ON_FAILURE)

transRela = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverConfirmScreen/V-transactorRelationship'), 
    FailureHandling.CONTINUE_ON_FAILURE)

String temp23 = transRela.replace(': ', '')

GlobalVariable.TransRela = temp23

WebUI.verifyMatch(GlobalVariable.TransRela, 'A - Affiliated', true, FailureHandling.CONTINUE_ON_FAILURE)

idenStatus = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverConfirmScreen/V-identicalStatus'), 
    FailureHandling.CONTINUE_ON_FAILURE)

String temp24 = idenStatus.replace(': ', '')

GlobalVariable.IdenticalStatus = temp24

WebUI.verifyMatch(GlobalVariable.IdenticalStatus, 'Identical', true, FailureHandling.CONTINUE_ON_FAILURE)

purpOfTrans = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverConfirmScreen/V-purposeOfTransaction'), 
    FailureHandling.CONTINUE_ON_FAILURE)

String temp25 = purpOfTrans.replace(': ', '')

GlobalVariable.PurpOfTrans = temp25

WebUI.verifyMatch(GlobalVariable.PurpOfTrans, '2011 - Ekspor barang', true, FailureHandling.CONTINUE_ON_FAILURE)

purpCode = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverConfirmScreen/V-LHBUPurposeCode'), FailureHandling.CONTINUE_ON_FAILURE)

String temp26 = purpCode.replace(': ', '')

GlobalVariable.PurpCode = temp26

WebUI.verifyMatch(GlobalVariable.PurpCode, '00 - 00 Investasi Penyertaan Langsung', true, FailureHandling.CONTINUE_ON_FAILURE)

docType = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverConfirmScreen/V-LHBUDocumentType'), FailureHandling.CONTINUE_ON_FAILURE)

String temp27 = docType.replace(': ', '')

GlobalVariable.DocType = temp27

WebUI.verifyMatch(GlobalVariable.DocType, '001 - 001 Fotokopi Pemberitahuan Impor Barang PIB', true, FailureHandling.CONTINUE_ON_FAILURE)

docTypeDes = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverConfirmScreen/V-LHBUDocumentTypeDescription'), 
    FailureHandling.CONTINUE_ON_FAILURE)

String temp28 = docTypeDes.replace(': ', '')

GlobalVariable.DocTypeDes = temp28

WebUI.verifyMatch(GlobalVariable.DocTypeDes, 'aduhai', true, FailureHandling.CONTINUE_ON_FAILURE)

instrucMode = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverConfirmScreen/V-instructionMode'), 
    FailureHandling.CONTINUE_ON_FAILURE)

String temp29 = instrucMode.replace(': ', '')

GlobalVariable.InstructionMode = temp29

WebUI.verifyMatch(GlobalVariable.InstructionMode, 'Immediate', true, FailureHandling.CONTINUE_ON_FAILURE)

expOn = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverConfirmScreen/V-expiredOn'), FailureHandling.CONTINUE_ON_FAILURE)

String temp30 = expOn.replace(': ', '')

GlobalVariable.ExpiredOn = temp30

WebUI.verifyMatch(GlobalVariable.ExpiredOn, GlobalVariable.ExpiredOn, true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('B-Approve'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(7, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('MultiUserDomestic(Immediate)SKN/tf-responseCode'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserDomestic(Immediate)SKN/tf-responseCode'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('MultiUserDomestic(Immediate)SKN/tf-responseCode'), '123456', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(10, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('B-Approve'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/PendingTask/B-FinalOKApprover'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(20, FailureHandling.CONTINUE_ON_FAILURE)

