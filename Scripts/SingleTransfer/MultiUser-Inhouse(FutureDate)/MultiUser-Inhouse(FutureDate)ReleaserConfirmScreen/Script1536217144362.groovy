import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

transForm = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-ConfirmScreen/V-transferForm'))

GlobalVariable.SenderCode = transForm.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.SenderCode, GlobalVariable.userResultTransferFrom, false)

fromAccDes = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-ConfirmScreen/V-fromAccountDescription'))

GlobalVariable.FromAccDes = fromAccDes.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.FromAccDes, GlobalVariable.v2, true)

transTo = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-ConfirmScreen/V-transferTo'))

GlobalVariable.TransferTo = transTo.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.TransferTo, GlobalVariable.v3, true)

beneList = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-ConfirmScreen/V-accountNumber'))

GlobalVariable.AccNo = beneList.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.AccNo, GlobalVariable.userTransferFrom, true)

toAccDes = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-ConfirmScreen/V-toAccountDescription'))

GlobalVariable.ToAccDes = toAccDes.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.ToAccDes, GlobalVariable.v5, true)

accName = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-ConfirmScreen/V-accountName'))

GlobalVariable.AccName = accName.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.AccName, GlobalVariable.userResultBeneficiaryList, false)

email = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-ConfirmScreen/V-email'))

GlobalVariable.ValueBankTypeEmail = email.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.ValueBankTypeEmail, GlobalVariable.v7, true)

sms = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-ConfirmScreen/V-sms'))

GlobalVariable.ValueBankTypeSms = sms.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.ValueBankTypeSms, GlobalVariable.v8, true)

beneRefNo = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-ConfirmScreen/V-beneficiaryReferenceNumber'))

GlobalVariable.BenefRefNo = beneRefNo.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.BenefRefNo, GlobalVariable.v9, true)

amount = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-ConfirmScreen/V-amount'))

GlobalVariable.Amount = amount.replace(': ', '')

String verifyAmount = 'IDR ' + GlobalVariable.Amount7

GlobalVariable.Amount6 = verifyAmount

WebUI.verifyMatch(GlobalVariable.Amount, GlobalVariable.Amount6, true)

excRate = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-ConfirmScreen/V-exchangeRate'))

GlobalVariable.ExchangeRate = excRate.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.ExchangeRate, GlobalVariable.v11, true)

transFee = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-ConfirmScreen/V-transferFee'))

GlobalVariable.TransFee = transFee.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.TransFee, GlobalVariable.TransFee, true)

totCharge = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-ConfirmScreen/V-totalCharge'))

GlobalVariable.TotalCharge = totCharge.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.TotalCharge, GlobalVariable.TotalCharge, true)

totDebet = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-ConfirmScreen/V-totalDebetAmount'))

GlobalVariable.TotalDebet = totDebet.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.TotalDebet, GlobalVariable.TotalDebet, true)

charInstruction = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-ConfirmScreen/V-chargeInstruction'))

GlobalVariable.ChargeInstruction = charInstruction.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.ChargeInstruction, GlobalVariable.v15, true)

debCharge = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-ConfirmScreen/V-debitCharge'))

GlobalVariable.DebitCharge = debCharge.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.DebitCharge, GlobalVariable.v16, true)

beneCate = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-ConfirmScreen/V-beneficiaryCategory'))

GlobalVariable.BenefCate = beneCate.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.BenefCate, GlobalVariable.v17, true)

transRela = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-ConfirmScreen/V-transactorRelationship'))

GlobalVariable.TransRela = transRela.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.TransRela, GlobalVariable.v18, true)

idenStatus = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-ConfirmScreen/V-identicalStatus'))

GlobalVariable.IdenticalStatus = idenStatus.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.IdenticalStatus, GlobalVariable.v19, true)

purpOfTrans = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-ConfirmScreen/V-purposeOfTransaction'))

GlobalVariable.PurpOfTrans = purpOfTrans.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.PurpOfTrans, GlobalVariable.v20, true)

purpCode = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-ConfirmScreen/V-LHBUPurposeCode'))

GlobalVariable.PurpCode = purpCode.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.PurpCode, GlobalVariable.v21, true)

docType = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-ConfirmScreen/V-LHBUDocumentType'))

GlobalVariable.DocType = docType.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.DocType, GlobalVariable.v22, true)

docTypeDes = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-ConfirmScreen/V-LHBUDocumentTypreDescription'))

GlobalVariable.DocTypeDes = docTypeDes.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.DocTypeDes, GlobalVariable.v23, true)

insMode = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-ConfirmScreen/V-instructionMode'))

GlobalVariable.InstructionMode = insMode.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.InstructionMode, GlobalVariable.v24, true)

at = WebUI.getText(findTestObject('MultiUserInhouse(FutureDate)/Value-ApproverConfirmScreen/V-futureDate'))

GlobalVariable.At = at.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.At, GlobalVariable.At, true)

futDate = WebUI.getText(findTestObject('MultiUserInhouse(FutureDate)/Value-ApproverConfirmScreen/V-at'))

GlobalVariable.FutureDate = futDate.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.FutureDate, GlobalVariable.FutureDate, true)

expOn = WebUI.getText(findTestObject('MultiUserInhouse(FutureDate)/Value-ApproverConfirmScreen/V-expiredOn'))

GlobalVariable.ExpiredOn = expOn.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.ExpiredOn, GlobalVariable.ExpiredOn, true)

WebUI.delay(2)

WebUI.click(findTestObject('SingleUserInhouse(immediate)/EntryScreen/B-Submit'))

WebUI.delay(2)

WebUI.click(findTestObject('SingleUserInhouse(immediate)/EntryScreen/B-Submit2'))

WebUI.delay(5)

