import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import java.util.Locale as Locale
import java.text.DecimalFormat as DecimalFormat
import java.text.NumberFormat as NumberFormat

///////////////////////////////////////////////////
fromAccountDescription = 'PangeranInhouseImmediate'

transferFrom = '003600088516'

toAccountDescription = 'money'

email = 'Pangeran@gmail.com'

sms = '911911911911'

inpBeneRefNum = '19283190219'

LHBUDocumentTypeDescription = 'aduhai'

aliasName = 'dapetdapet'

benefAccountNum = '123456789'

benefAccountName = 'dapitdapit'

add1 = 'Jl Kebon Sirih'

add2 = 'Jakarta Pusat'

///////////////////////////////////////////////////
WebUI.openBrowser('')

WebUI.navigateToUrl('https://10.194.8.106:39990/')

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-CorpID'), 0)

WebUI.setText(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-CorpID'), GlobalVariable.MultiUserLoginCorpregId)

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-UserID'), 0)

WebUI.setText(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-UserID'), GlobalVariable.MultiUserLoginId)

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-UserPass'), 0)

WebUI.setText(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-UserPass'), GlobalVariable.MultiUserPass)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/B-Login'))

WebUI.delay(5)

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/B-Menu'), 0)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/B-Menu'))

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TransferManagement'), 0)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TransferManagement'))

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/SingleTransfer'), 0)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/SingleTransfer'))

WebUI.delay(5)

WebUI.click(findTestObject('MultiUserInternational(Immediate)/Value-CreateEntryScreen/B-internationalForeignCurrencyTransafer'))

WebUI.delay(3)

WebUI.click(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccount'))

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), transferFrom)

WebUI.delay(5)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/MainForm/V-Tfrom'))

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/MainForm/V-FAccDes'), 0)

WebUI.setText(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/MainForm/V-FAccDes'), fromAccountDescription)

WebUI.delay(5)

WebUI.click(findTestObject('MultiUserInternational(Immediate)/Value-CreateEntryScreen/B-newBeneficiary'))

WebUI.setText(findTestObject('MultiUserInternational(Immediate)/Value-CreateEntryScreen/TF-aliasName'), aliasName)

WebUI.click(findTestObject('MultiUserInternational(Immediate)/Value-CreateEntryScreen/DL-beneficiaryBankCountry'))

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), 'ID')

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('MultiUserInternational(Immediate)/Value-CreateEntryScreen/DL-nationalOrganizationDirectory'))

WebUI.click(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInternational(Immediate)/Value-CreateEntryScreen/V-nationalOrganizationDirectory'))

WebUI.click(findTestObject('MultiUserInternational(Immediate)/Value-CreateEntryScreen/B-beneficiaryBank'))

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), 'INDONESIA')

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('MultiUserInternational(Immediate)/Value-CreateEntryScreen/DL-intermediaryBank'))

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), 'rere123')

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('MultiUserInternational(Immediate)/Value-CreateEntryScreen/TF-beneficiaryAccountNumber'), benefAccountNum)

WebUI.setText(findTestObject('MultiUserInternational(Immediate)/Value-CreateEntryScreen/TF-beneficiaryAccountName'), benefAccountName)

WebUI.setText(findTestObject('MultiUserInternational(Immediate)/Value-CreateEntryScreen/TF-beneficiaryAddress1'),add1)

WebUI.setText(findTestObject('MultiUserInternational(Immediate)/Value-CreateEntryScreen/TF-beneficiaryAddress2'), add2)

WebUI.setText(findTestObject('MultiUserDomestic(Repeat)SKN/Value-EntryScreen/TF-email'), email)

WebUI.setText(findTestObject('MultiUserDomestic(Repeat)SKN/Value-EntryScreen/TF-sms'), sms)

WebUI.setText(findTestObject('MultiUserDomestic(Repeat)SKN/Value-EntryScreen/TF-toAccountDescription'), toAccountDescription)

WebUI.setText(findTestObject('MultiUserDomestic(Repeat)SKN/Value-EntryScreen/TF-beneficiaryReferenceNumber'), inpBeneRefNum)

WebUI.click(findTestObject('MultiUserInternational(Immediate)/Value-CreateEntryScreen/DL-remittanceCurrency'))

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), 'JPY')

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('MultiUserInternational(Immediate)/Value-CreateEntryScreen/DL-selectAmount'))

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), Keys.chord(Keys.ENTER))

WebUI.waitForElementVisible(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-amount'), 0)

Random rand = new Random()

int angka

angka = rand.nextInt(999)

angka1 = ('3' + angka)

Locale local1 = new Locale('en', 'UK')

DecimalFormat amountInquiry = ((NumberFormat.getNumberInstance(local1)) as DecimalFormat)

amountInquiry.applyPattern('###,###,##0.00')

String amountInquiry1 = amountInquiry.format(new BigDecimal(angka1))

amountInquiry2 = 'JPY   ' + amountInquiry1

GlobalVariable.UniversalVariable.put('Amount7',amountInquiry2)

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-amount'), angka1)

WebUI.click(findTestObject('SingleUserInternational(Immediate)/DL-Borne by Remitter'))

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), 'borne by remitter')

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), Keys.chord(Keys.ENTER))

WebUI.delay(2)

WebUI.click(findTestObject('MultiUserInternational(Immediate)/Value-CreateEntryScreen/TF-beneficiaryCategory'))

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/MainForm/V-BenefCateg'))

WebUI.delay(1)

WebUI.click(findTestObject('MultiUserInternational(Immediate)/Value-CreateEntryScreen/TF-transactorRelationship'))

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/MainForm/V-TransactorRela'))

WebUI.click(findTestObject('MultiUserInternational(Immediate)/Value-CreateEntryScreen/TF-purposeOfTransaction'))

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/MainForm/V-PuposeOfTransaction'))

WebUI.click(findTestObject('MultiUserInternational(Immediate)/Value-CreateEntryScreen/DL-LHBUPurposeCode'))

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/MainForm/V-PuposeCode'))

WebUI.click(findTestObject('MultiUserInternational(Immediate)/Value-CreateEntryScreen/DL-LHBUDocumentType'))

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/MainForm/V-DocType'))

WebUI.setText(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/MainForm/TFA-DocTypeDes'), LHBUDocumentTypeDescription)

WebUI.click(findTestObject('MultiUserInhouse(Repeat)/Value-CreateEntryScreen/B-repeat'))

WebUI.click(findTestObject('MultiUserInhouse(Repeat)/Value-CreateEntryScreen/DL-searchSchedule'))

WebUI.click(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'))

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('MultiUserInhouse(Repeat)/Value-CreateEntryScreen/DL-every'))

WebUI.click(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'))

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), Keys.chord(Keys.ENTER))

WebUI.delay(2)

WebUI.click(findTestObject('MultiUserInhouse(Repeat)/Value-CreateEntryScreen/DL-at'))

WebUI.click(findTestObject('MultiUserInternational(Immediate)/Value-CreateConfirmScreen/V-sessions'))

WebUI.click(findTestObject('MultiUserInhouse(Repeat)/Value-CreateEntryScreen/TF-start'))

WebUI.click(findTestObject('MultiUserInhouse(Repeat)/Value-CreateEntryScreen/V-start'))

WebUI.click(findTestObject('MultiUserInhouse(Repeat)/Value-CreateEntryScreen/DL-end'))

WebUI.click(findTestObject('MultiUserInhouse(Repeat)/Value-CreateEntryScreen/V-end'))

WebUI.click(findTestObject('MultiUserInhouse(Repeat)/Value-CreateEntryScreen/DL-non-WorkingDayInstruction'))

WebUI.click(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'))

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), Keys.chord(Keys.ENTER))

transferFrom1 = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-EntryScreen(Extended)/V-transferFrom'), 
    FailureHandling.CONTINUE_ON_FAILURE)

transferFrom2 = transferFrom1.replace('-', '/')

transferFrom = transferFrom2.replace('(', ' (')

GlobalVariable.UniversalVariable.put('transferFrom', transferFrom)

GlobalVariable.UniversalVariable.put('fromAccountDescription', fromAccountDescription)

transferTo = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-EntryScreen(Extended)/V-transferTo'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('transferTo', transferTo)

benefBankCountry = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-EntryScreen(Extended)/V-benefBankCountry'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('benefBankCountry', benefBankCountry)

nationalOrganizationDirectory = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-EntryScreen(Extended)/V-nationalOrganizationDirectory'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('nationalOrganizationDirectory', nationalOrganizationDirectory)

benefBank = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-EntryScreen(Extended)/V-beneficiaryBank'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('benefBank', benefBank)

bankCity = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-EntryScreen(Extended)/V-bankCity'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('bankCity', bankCity)

intermediaryBank = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-EntryScreen(Extended)/V-intermedieryBank'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('intermediaryBank', intermediaryBank)

remitterCurrency = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-EntryScreen(Extended)/B-remitterCurrency'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('remitterCurrency', remitterCurrency)

amountCurrency = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-EntryScreen(Extended)/V-amount'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('amountCurrency', amountCurrency)

fullAmountCharge = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-EntryScreen(Extended)/V-fullAmountCharge'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('fullAmountCharge', fullAmountCharge)

debitCharge = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-EntryScreen(Extended)/V-debitCharge'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('debitCharge', debitCharge)

selectBenefciaryList = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-EntryScreen(Extended)/V-selectBeneficiaryList'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('selectBenefciaryList', selectBenefciaryList)

GlobalVariable.UniversalVariable.put('transferFrom', transferFrom)

GlobalVariable.UniversalVariable.put('sms', sms)

GlobalVariable.UniversalVariable.put('email', email)

GlobalVariable.UniversalVariable.put('toAccountDescription', toAccountDescription)

GlobalVariable.UniversalVariable.put('inputBeneficiaryReferenceNu', inpBeneRefNum)

GlobalVariable.UniversalVariable.put('aliasName', aliasName)

GlobalVariable.UniversalVariable.put('benefAccountNum', benefAccountNum)

GlobalVariable.UniversalVariable.put('benefAccountName', benefAccountName)

GlobalVariable.UniversalVariable.put('add1', add1)

GlobalVariable.UniversalVariable.put('add2', add2)

not_run: transferMethod = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-entry/V-transferMethod'), 
    FailureHandling.CONTINUE_ON_FAILURE)

not_run: GlobalVariable.UniversalVariable.put('transferMethod', transferMethod)

amount = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-EntryScreen/V-amount'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('amount', amount)

not_run: chargeInstruction = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-entry/V-chargeInstruction'), 
    FailureHandling.CONTINUE_ON_FAILURE)

not_run: GlobalVariable.UniversalVariable.put('chargeInstruction', chargeInstruction)

not_run: debitCharge = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-entry/V-debitCharge'), FailureHandling.CONTINUE_ON_FAILURE)

not_run: GlobalVariable.UniversalVariable.put('debitCharge', debitCharge)

beneficiaryCategory = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-EntryScreen(Extended)/V-benefCategory'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('beneficiaryCategory', beneficiaryCategory)

transactorRelationship = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-EntryScreen(Extended)/V-transactorRelationship'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('transactorRelationship', transactorRelationship)

identicalStatus = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-EntryScreen/V-identicalStatus'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('identicalStatus', identicalStatus)

purposeOfTransaction = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-EntryScreen(Extended)/V-purposeOfTransaction'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('purposeOfTransaction', purposeOfTransaction)

LHBUPurposeCode = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-EntryScreen(Extended)/V-LHBUPurposeCode'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('LHBUPurposeCode', LHBUPurposeCode)

LHBUDocumentType = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-EntryScreen(Extended)/V-LHBUDocumentType'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('LHBUDocumentType', LHBUDocumentType)

GlobalVariable.UniversalVariable.put('LHBUDocumentTypeDescription', LHBUDocumentTypeDescription)

instructionMode = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-EntryScreen/V-instructionMode'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('instructionMode', instructionMode)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/MainForm/B-Confim'))

WebUI.click(findTestObject('MultiUserInternational(Immediate)/Value-CreateConfirmScreen/B-submitBox'))

WebUI.delay(10)

