import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

menu = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-TransactionStatusReffNo/1/V-menu'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Menu = menu.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.Menu, 'Single Transfer', true, FailureHandling.CONTINUE_ON_FAILURE)

product = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-TransactionStatusReffNo/1/V-product'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Product = product.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.Product, 'International Foreign Currency Transfer', false, FailureHandling.CONTINUE_ON_FAILURE)

refNum = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-TransactionStatusReffNo/V-transactionRefferenceNumber'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.ApproverRefNo = refNum.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.ApproverRefNo, GlobalVariable.ReffNo, true, FailureHandling.CONTINUE_ON_FAILURE)

date = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-TransactionStatusReffNo/1/V-date'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Date = date.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.Date, GlobalVariable.Date, false, FailureHandling.CONTINUE_ON_FAILURE)

transFrom1 = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-TransactionStatusReffNo/1/V-transferFrom'), 
    FailureHandling.CONTINUE_ON_FAILURE)

transFrom = transFrom1.replace(': ', '')

WebUI.verifyMatch(transFrom, GlobalVariable.UniversalVariable.get('transferFrom'), false, FailureHandling.CONTINUE_ON_FAILURE)

fromAccDes = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-TransactionStatusReffNo/1/V-fromAccountDescription'), 
    FailureHandling.CONTINUE_ON_FAILURE)

FromAccDes = fromAccDes.replace(': ', '')

WebUI.verifyMatch(FromAccDes, GlobalVariable.UniversalVariable.get('FromAccDes'), true, FailureHandling.CONTINUE_ON_FAILURE)

transTo = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-TransactionStatusReffNo/1/V-transferTo'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TransferTo = transTo.replace(': ', '')

WebUI.verifyMatch(TransferTo, GlobalVariable.UniversalVariable.get('TransferTo'), true, FailureHandling.CONTINUE_ON_FAILURE)

beneBankCon = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-TransactionStatusReffNo/V-beneficiaryBankCountry'), 
    FailureHandling.CONTINUE_ON_FAILURE)

BeneficiaryBankCountry = beneBankCon.replace(': ', '')

WebUI.verifyMatch(BeneficiaryBankCountry, GlobalVariable.UniversalVariable.get('BeneficiaryBankCountry'), true, FailureHandling.CONTINUE_ON_FAILURE)

natOrgaDirectory = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-TransactionStatusReffNo/V-nationalOrganizationDirectory'), 
    FailureHandling.CONTINUE_ON_FAILURE)

NationalOrganizationDirectory = natOrgaDirectory.replace(': ', '')

WebUI.verifyMatch(NationalOrganizationDirectory, GlobalVariable.UniversalVariable.get('NationalOrganizationDirectory'), 
    true, FailureHandling.CONTINUE_ON_FAILURE)

beneBank = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-TransactionStatusReffNo/V-beneficiaryBank'), 
    FailureHandling.CONTINUE_ON_FAILURE)

BeneficiaryBank = beneBank.replace(': ', '')

WebUI.verifyMatch(BeneficiaryBank, GlobalVariable.UniversalVariable.get('BeneficiaryBank'), true, FailureHandling.CONTINUE_ON_FAILURE)

bankCity = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-TransactionStatusReffNo/V-bankCity'), FailureHandling.CONTINUE_ON_FAILURE)

BankCity = bankCity.replace(': ', '')

WebUI.verifyMatch(BankCity, GlobalVariable.UniversalVariable.get('BankCity'), true, FailureHandling.CONTINUE_ON_FAILURE)

interBank = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-TransactionStatusReffNo/V-intermediaryBank'), 
    FailureHandling.CONTINUE_ON_FAILURE)

IntermediaryBank = interBank.replace(': ', '')

WebUI.verifyMatch(IntermediaryBank, GlobalVariable.UniversalVariable.get('IntermediaryBank'), true, FailureHandling.CONTINUE_ON_FAILURE)

accNumber = WebUI.getText(findTestObject('SingleUserDometstic(Repeat)SKN/Value-TransactionStatusReffNo/V-accountNumber'), 
    FailureHandling.CONTINUE_ON_FAILURE)

accountNumber = accNumber.replace(': ', '')

WebUI.verifyMatch(accountNumber, GlobalVariable.UniversalVariable.get('benefAccountNum'), true, FailureHandling.CONTINUE_ON_FAILURE)

savToBeneList = WebUI.getText(findTestObject('MultiUserDomestic(Repeat)SKN/Value-TransactionStatusReffNo/V-saveToBeneficiaryList'), 
    FailureHandling.CONTINUE_ON_FAILURE)

saveToBeneficiaryList = savToBeneList.replace(': ', '')

WebUI.verifyMatch(saveToBeneficiaryList, GlobalVariable.UniversalVariable.get('saveToBeneficiaryList'), true, FailureHandling.CONTINUE_ON_FAILURE)

accName = WebUI.getText(findTestObject('MultiUserDomestic(Repeat)SKN/Value-TransactionStatusReffNo/V-accountName'), FailureHandling.CONTINUE_ON_FAILURE)

AccName = accName.replace(': ', '')

WebUI.verifyMatch(AccName, GlobalVariable.UniversalVariable.get('benefAccountName'), true, FailureHandling.CONTINUE_ON_FAILURE)

alName = WebUI.getText(findTestObject('MultiUserDomestic(Repeat)SKN/Value-TransactionStatusReffNo/V-aliasName'), FailureHandling.CONTINUE_ON_FAILURE)

aliasName = alName.replace(': ', '')

WebUI.verifyMatch(aliasName, GlobalVariable.UniversalVariable.get('aliasName'), true, FailureHandling.CONTINUE_ON_FAILURE)

address1 = WebUI.getText(findTestObject('MultiUserDomestic(Repeat)SKN/Value-TransactionStatusReffNo/V-address1'), FailureHandling.CONTINUE_ON_FAILURE)

tempAddress1 = address1.replace(': ', '')

WebUI.verifyMatch(tempAddress1, GlobalVariable.UniversalVariable.get('add1'), true, FailureHandling.CONTINUE_ON_FAILURE)

address2 = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-TransactionStatusReffNo/V-address2'), FailureHandling.CONTINUE_ON_FAILURE)

tempAddress2 = address2.replace(': ', '')

WebUI.verifyMatch(tempAddress2, GlobalVariable.UniversalVariable.get('add2'), true, FailureHandling.CONTINUE_ON_FAILURE)

email = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-TransactionStatusReffNo/V-email'), FailureHandling.CONTINUE_ON_FAILURE)

ValueBankTypeEmail = email.replace('Email : ', '')

WebUI.verifyMatch(ValueBankTypeEmail, GlobalVariable.UniversalVariable.get('email'), true, FailureHandling.CONTINUE_ON_FAILURE)

sms = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-TransactionStatusReffNo/V-sms'), FailureHandling.CONTINUE_ON_FAILURE)

ValueBankTypeSms = sms.replace('SMS   : ', '')

WebUI.verifyMatch(ValueBankTypeSms, GlobalVariable.UniversalVariable.get('sms'), true, FailureHandling.CONTINUE_ON_FAILURE)

benefRefNo = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-TransactionStatusReffNo/1/V-beneficiaryReffNumber'), 
    FailureHandling.CONTINUE_ON_FAILURE)

BenefRefNo = benefRefNo.replace(': ', '')

WebUI.verifyMatch(BenefRefNo, GlobalVariable.UniversalVariable.get('inputBeneficiaryReferenceNu'), true, FailureHandling.CONTINUE_ON_FAILURE)

toAccDes = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-TransactionStatusReffNo/1/V-toAccountDescription'), 
    FailureHandling.CONTINUE_ON_FAILURE)

ToAccDes = toAccDes.replace(': ', '')

WebUI.verifyMatch(ToAccDes, GlobalVariable.UniversalVariable.get('toAccountDescription'), true, FailureHandling.CONTINUE_ON_FAILURE)

remiCurrency = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-TransactionStatusReffNo/V-remittanceCurrency'), 
    FailureHandling.CONTINUE_ON_FAILURE)

RemittanceCurrency = remiCurrency.replace(': ', '')

WebUI.verifyMatch(RemittanceCurrency, GlobalVariable.UniversalVariable.get('RemittanceCurrency'), true, FailureHandling.CONTINUE_ON_FAILURE)

amount = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-TransactionStatusReffNo/V-amount'), FailureHandling.CONTINUE_ON_FAILURE)

Amount = amount.replace(': ', '')

WebUI.verifyMatch(Amount, GlobalVariable.UniversalVariable.get('Amount7'), true, FailureHandling.CONTINUE_ON_FAILURE)

fuAmoCharge = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-TransactionStatusReffNo/V-fullAmountCharge'), 
    FailureHandling.CONTINUE_ON_FAILURE)

FullAmountCharge = fuAmoCharge.replace(': ', '')

WebUI.verifyMatch(FullAmountCharge, GlobalVariable.UniversalVariable.get('fullAmountCharge'), true, FailureHandling.CONTINUE_ON_FAILURE)

debCharge = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-TransactionStatusReffNo/V-debitCharge'), 
    FailureHandling.CONTINUE_ON_FAILURE)

DebitCharge = debCharge.replace(': ', '')

WebUI.verifyMatch(DebitCharge, GlobalVariable.UniversalVariable.get('debitCharge'), true, FailureHandling.CONTINUE_ON_FAILURE)

provision = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-TransactionStatusReffNo/V-provision'), 
    FailureHandling.CONTINUE_ON_FAILURE)

Provision = provision.replace(': ', '')

WebUI.verifyMatch(Provision, GlobalVariable.UniversalVariable.get('Provision'), true, FailureHandling.CONTINUE_ON_FAILURE)

inLieu = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-TransactionStatusReffNo/V-inLieu'), FailureHandling.CONTINUE_ON_FAILURE)

InLieu = inLieu.replace(': ', '')

WebUI.verifyMatch(InLieu, GlobalVariable.UniversalVariable.get('InLieu'), true, FailureHandling.CONTINUE_ON_FAILURE)

fuAmoCharge2 = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-TransactionStatusReffNo/V-fullAmountCharge1'), 
    FailureHandling.CONTINUE_ON_FAILURE)

FullAmountCharge2 = fuAmoCharge2.replace(': ', '')

WebUI.verifyMatch(FullAmountCharge2, GlobalVariable.UniversalVariable.get('FullAmountCharge2'), true, FailureHandling.CONTINUE_ON_FAILURE)

cabFee = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-TransactionStatusReffNo/V-cableFee'), FailureHandling.CONTINUE_ON_FAILURE)

CableFee = cabFee.replace(': ', '')

WebUI.verifyMatch(CableFee, GlobalVariable.UniversalVariable.get('CableFee'), true, FailureHandling.CONTINUE_ON_FAILURE)

MinTransFee = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-TransactionStatusReffNo/V-minimunTransactionFee'), 
    FailureHandling.CONTINUE_ON_FAILURE)

MinimumTransactionFee = MinTransFee.replace(': ', '')

WebUI.verifyMatch(MinimumTransactionFee, GlobalVariable.UniversalVariable.get('MinimumTransactionFee'), true, FailureHandling.CONTINUE_ON_FAILURE)

corresBankFee = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-TransactionStatusReffNo/V-correspondentBankFee'), 
    FailureHandling.CONTINUE_ON_FAILURE)

CorrespondentBankFee = corresBankFee.replace(': ', '')

WebUI.verifyMatch(CorrespondentBankFee, GlobalVariable.UniversalVariable.get('CorrespondentBankFee'), true, FailureHandling.CONTINUE_ON_FAILURE)

totCharge = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-TransactionStatusReffNo/V-totalCharge'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TotalCharge = totCharge.replace(': ', '')

WebUI.verifyMatch(TotalCharge, GlobalVariable.UniversalVariable.get('TotalCharge'), true, FailureHandling.CONTINUE_ON_FAILURE)

charToRemitter = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-TransactionStatusReffNo/V-chargeToRemitter'), 
    FailureHandling.CONTINUE_ON_FAILURE)

ChargeToRemitter = charToRemitter.replace(': ', '')

WebUI.verifyMatch(ChargeToRemitter, GlobalVariable.UniversalVariable.get('ChargeToRemitter'), true, FailureHandling.CONTINUE_ON_FAILURE)

charToBeneficiary = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-TransactionStatusReffNo/V-chargeToBeneficiary'), 
    FailureHandling.CONTINUE_ON_FAILURE)

ChargeToBeneficiary = charToBeneficiary.replace(': ', '')

WebUI.verifyMatch(ChargeToBeneficiary, GlobalVariable.UniversalVariable.get('ChargeToBeneficiary'), true, FailureHandling.CONTINUE_ON_FAILURE)

totDebAmount = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-TransactionStatusReffNo/V-totalDebit'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TotalDebet = totDebAmount.replace(': ', '')

WebUI.verifyMatch(TotalDebet, GlobalVariable.UniversalVariable.get('TotalDebet'), true, FailureHandling.CONTINUE_ON_FAILURE)

benefResisStat = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-TransactionStatusReffNo/V-beneficiaryResistantStatus'), 
    FailureHandling.CONTINUE_ON_FAILURE)

BenefResidentStatus = benefResisStat.replace(': ', '')

WebUI.verifyMatch(BenefResidentStatus, GlobalVariable.UniversalVariable.get('BenefResidentStatus'), true, FailureHandling.CONTINUE_ON_FAILURE)

benefCate = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-TransactionStatusReffNo/V-beneficiaryCategory'), 
    FailureHandling.CONTINUE_ON_FAILURE)

BenefCate = benefCate.replace(': ', '')

WebUI.verifyMatch(BenefCate, GlobalVariable.UniversalVariable.get('beneficiaryCategory'), true, FailureHandling.CONTINUE_ON_FAILURE)

transRela = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-TransactionStatusReffNo/1/V-transactorRelationship'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TransRela = transRela.replace(': ', '')

WebUI.verifyMatch(TransRela, GlobalVariable.UniversalVariable.get('transactorRelationship'), true, FailureHandling.CONTINUE_ON_FAILURE)

idenStatus = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-TransactionStatusReffNo/1/V-identicalStatus'), 
    FailureHandling.CONTINUE_ON_FAILURE)

IdenticalStatus = idenStatus.replace(': ', '')

WebUI.verifyMatch(IdenticalStatus, GlobalVariable.UniversalVariable.get('identicalStatus'), true, FailureHandling.CONTINUE_ON_FAILURE)

purpOfTrans = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-TransactionStatusReffNo/1/V-puposeOfTransaction'), 
    FailureHandling.CONTINUE_ON_FAILURE)

PurpOfTrans = purpOfTrans.replace(': ', '')

WebUI.verifyMatch(PurpOfTrans, GlobalVariable.UniversalVariable.get('purposeOfTransaction'), true, FailureHandling.CONTINUE_ON_FAILURE)

purpCode = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-TransactionStatusReffNo/1/V-LHBUPurposeCode'), 
    FailureHandling.CONTINUE_ON_FAILURE)

PurpCode = purpCode.replace(': ', '')

WebUI.verifyMatch(PurpCode, GlobalVariable.UniversalVariable.get('LHBUPurposeCode'), true, FailureHandling.CONTINUE_ON_FAILURE)

docType = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-TransactionStatusReffNo/1/V-LHBUDocumentType'), 
    FailureHandling.CONTINUE_ON_FAILURE)

DocType = docType.replace(': ', '')

WebUI.verifyMatch(DocType, GlobalVariable.UniversalVariable.get('LHBUDocumentType'), true, FailureHandling.CONTINUE_ON_FAILURE)

docTypeDes = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-TransactionStatusReffNo/1/V-LHBUDocumentTypeDescription'), 
    FailureHandling.CONTINUE_ON_FAILURE)

DocTypeDes = docTypeDes.replace(': ', '')

WebUI.verifyMatch(DocTypeDes, GlobalVariable.UniversalVariable.get('LHBUDocumentTypeDescription'), true, FailureHandling.CONTINUE_ON_FAILURE)

instrucMode = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-TransactionStatusReffNo/1/V-intructionMode'), 
    FailureHandling.CONTINUE_ON_FAILURE)

InstructionMode = instrucMode.replace(': ', '')

WebUI.verifyMatch(InstructionMode, GlobalVariable.UniversalVariable.get('InstructionMode'), true, FailureHandling.CONTINUE_ON_FAILURE)

on = WebUI.getText(findTestObject('MultiUserInternational(Repeat)/V-transactionStatusScreen/V-On'), FailureHandling.CONTINUE_ON_FAILURE)

On = on.replace(': ', '')

WebUI.verifyMatch(On, GlobalVariable.UniversalVariable.get('On'), true, FailureHandling.CONTINUE_ON_FAILURE)

every = WebUI.getText(findTestObject('MultiUserDomestic(Repeat)SKN/Value-TransactionStatusReffNo/V-every'), FailureHandling.CONTINUE_ON_FAILURE)

Every = every.replace(': ', '')

WebUI.verifyMatch(Every, GlobalVariable.UniversalVariable.get('Every'), true, FailureHandling.CONTINUE_ON_FAILURE)

at = WebUI.getText(findTestObject('MultiUserDomestic(Repeat)SKN/Value-TransactionStatusReffNo/V-at'), FailureHandling.CONTINUE_ON_FAILURE)

At = at.replace(': ', '')

WebUI.verifyMatch(At, GlobalVariable.UniversalVariable.get('At'), true, FailureHandling.CONTINUE_ON_FAILURE)

start = WebUI.getText(findTestObject('MultiUserDomestic(Repeat)SKN/Value-TransactionStatusReffNo/V-start'), FailureHandling.CONTINUE_ON_FAILURE)

Start = start.replace(': ', '')

WebUI.verifyMatch(Start, GlobalVariable.UniversalVariable.get('Start'), true, FailureHandling.CONTINUE_ON_FAILURE)

end = WebUI.getText(findTestObject('MultiUserDomestic(Repeat)SKN/Value-TransactionStatusReffNo/V-end'), FailureHandling.CONTINUE_ON_FAILURE)

End = end.replace(': ', '')

WebUI.verifyMatch(End, GlobalVariable.UniversalVariable.get('End'), true, FailureHandling.CONTINUE_ON_FAILURE)

insOnHoliday = WebUI.getText(findTestObject('MultiUserDomestic(Repeat)SKN/Value-TransactionStatusReffNo/V-nonWorkingDayInstruction'), 
    FailureHandling.CONTINUE_ON_FAILURE)

NonWorkingDayInstruction = insOnHoliday.replace(': ', '')

WebUI.verifyMatch(NonWorkingDayInstruction, GlobalVariable.UniversalVariable.get('NonWorkingDayInstruction'), true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

