import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

transStat = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-TransactionStatusReffNo/V-transactionStatus'))

GlobalVariable.TransactionStatus = transStat.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.TransactionStatus, GlobalVariable.TransactionStatus, true)

menu = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-TransactionStatusReffNo/V-menu'))

GlobalVariable.Menu = menu.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.Menu, 'Single Transfer', true)

product = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-TransactionStatusReffNo/V-product'))

GlobalVariable.Product = product.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.Product, 'RTGS', false)

refNum = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-TransactionStatusReffNo/V-transactionReffNumber'))

GlobalVariable.ApproverRefNo = refNum.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.ApproverRefNo, GlobalVariable.ReffNo, true)

verNum = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-TransactionStatusReffNo/V-versionNumber'))

GlobalVariable.VersionNo = verNum.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.VersionNo, GlobalVariable.ReffNo, true)

date = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-TransactionStatusReffNo/V-date'))

GlobalVariable.Date = date.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.Date, GlobalVariable.Date, false)

transferFrom1 = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-TransactionStatus/V-transferFrom'), 
    FailureHandling.CONTINUE_ON_FAILURE)

transferFrom2 = transferFrom1.replace(': ', '')

transferFrom = transferFrom2.replace('/', ' /')

WebUI.verifyMatch(transferFrom, GlobalVariable.UniversalVariable.get('transferFrom'), false, FailureHandling.CONTINUE_ON_FAILURE)

fromAccountDes1 = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-TransactionStatus/V-fromAccountDescription'), 
    FailureHandling.CONTINUE_ON_FAILURE)

fromAccountDes = fromAccountDes1.replace(': ', '')

WebUI.verifyMatch(fromAccountDes, GlobalVariable.UniversalVariable.get('fromAccountDescription'), true, FailureHandling.CONTINUE_ON_FAILURE)

transferTo1 = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-TransactionStatus/V-transferTo'), FailureHandling.CONTINUE_ON_FAILURE)

transferTo = transferTo1.replace(': ', '')

WebUI.verifyMatch(transferTo, GlobalVariable.UniversalVariable.get('transferTo'), true, FailureHandling.CONTINUE_ON_FAILURE)

savToBeneList = WebUI.getText(findTestObject('MultiUserDomestic(Repeat)SKN/Value-TransactionStatusReffNo/Value-Extend/V-saveToBeneficiaryList'), 
    FailureHandling.CONTINUE_ON_FAILURE)

saveToBeneficiaryList = savToBeneList.replace(': ', '')

WebUI.verifyMatch(saveToBeneficiaryList, GlobalVariable.UniversalVariable.get('saveToBeneficiaryList'), true, FailureHandling.CONTINUE_ON_FAILURE)

alName = WebUI.getText(findTestObject('MultiUserDomestic(Repeat)SKN/Value-TransactionStatusReffNo/Value-Extend/V-aliasName'), 
    FailureHandling.CONTINUE_ON_FAILURE)

aliasName = alName.replace(': ', '')

WebUI.verifyMatch(aliasName, GlobalVariable.UniversalVariable.get('aliasName'), true, FailureHandling.CONTINUE_ON_FAILURE)

beneficiary = WebUI.getText(findTestObject('MultiUserDomestic(Repeat)SKN/Value-TransactionStatusReffNo/Value-Extend/V-beneficiaryBank'), 
    FailureHandling.CONTINUE_ON_FAILURE)

accNo = beneficiary.replace(': ', '')

WebUI.verifyMatch(accNo, GlobalVariable.UniversalVariable.get('beneficiaryBank'), false, FailureHandling.CONTINUE_ON_FAILURE)

accName = WebUI.getText(findTestObject('MultiUserDomestic(Repeat)SKN/Value-TransactionStatusReffNo/Value-Extend/V-accountName'), 
    FailureHandling.CONTINUE_ON_FAILURE)

AccName = accName.replace(': ', '')

WebUI.verifyMatch(AccName, GlobalVariable.UniversalVariable.get('accName'), true, FailureHandling.CONTINUE_ON_FAILURE)

accNumber = WebUI.getText(findTestObject('MultiUserDomestic(Repeat)SKN/Value-TransactionStatusReffNo/Value-Extend/V-accountNumber'), 
    FailureHandling.CONTINUE_ON_FAILURE)

accountNumber = accNumber.replace(': ', '')

WebUI.verifyMatch(accountNumber, GlobalVariable.UniversalVariable.get('accNumber'), true, FailureHandling.CONTINUE_ON_FAILURE)

address1 = WebUI.getText(findTestObject('MultiUserDomestic(Repeat)SKN/Value-TransactionStatusReffNo/Value-Extend/V-address1'), 
    FailureHandling.CONTINUE_ON_FAILURE)

Address1 = address1.replace(': ', '')

WebUI.verifyMatch(Address1, GlobalVariable.UniversalVariable.get('address1'), true, FailureHandling.CONTINUE_ON_FAILURE)

address2 = WebUI.getText(findTestObject('MultiUserDomestic(Repeat)SKN/Value-transactionStatusDocNo/V-address2'), FailureHandling.CONTINUE_ON_FAILURE)

Address2 = address2.replace(': ', '')

WebUI.verifyMatch(Address2, GlobalVariable.UniversalVariable.get('address2'), true, FailureHandling.CONTINUE_ON_FAILURE)

address3 = WebUI.getText(findTestObject('MultiUserDomestic(Repeat)SKN/Value-transactionStatusDocNo/V-address3'), FailureHandling.CONTINUE_ON_FAILURE)

Address3 = address3.replace(': ', '')

WebUI.verifyMatch(Address3, GlobalVariable.UniversalVariable.get('address3'), true, FailureHandling.CONTINUE_ON_FAILURE)

toAccDes1 = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-TransactionStatus/V-toAccountDescription'), 
    FailureHandling.CONTINUE_ON_FAILURE)

toAccDes = toAccDes1.replace(': ', '')

WebUI.verifyMatch(toAccDes, GlobalVariable.UniversalVariable.get('toAccountDescription'), true, FailureHandling.CONTINUE_ON_FAILURE)

inpBeneRefNum1 = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-TransactionStatus/V-inputBeneficiaryReferenceNumber'), 
    FailureHandling.CONTINUE_ON_FAILURE)

inpBeneRefNum = inpBeneRefNum1.replace(': ', '')

WebUI.verifyMatch(inpBeneRefNum, GlobalVariable.UniversalVariable.get('inputBeneficiaryReferenceNu'), true, FailureHandling.CONTINUE_ON_FAILURE)

transMeth = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-TransactionStatus/V-transferMethod'), FailureHandling.CONTINUE_ON_FAILURE)

TransferMethod = transMeth.replace(': ', '')

WebUI.verifyMatch(TransferMethod, GlobalVariable.UniversalVariable.get('transferMethod'), true, FailureHandling.CONTINUE_ON_FAILURE)

amount1 = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-TransactionStatus/V-amount'), FailureHandling.CONTINUE_ON_FAILURE)

amount = amount1.replace(': ', '')

GlobalVariable.Amount = amount

String verifyAmount1 = 'IDR ' + GlobalVariable.Amount7

verifyAmount = verifyAmount1.replace('IDR   ', 'IDR ')

WebUI.verifyMatch(verifyAmount, GlobalVariable.UniversalVariable.get('Amount'), true, FailureHandling.CONTINUE_ON_FAILURE)

exRate1 = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-TransactionStatus/V-exchaneRate'), FailureHandling.CONTINUE_ON_FAILURE)

exRate = exRate1.replace(': ', '')

WebUI.verifyMatch(exRate, GlobalVariable.UniversalVariable.get('exchangeRate'), true, FailureHandling.CONTINUE_ON_FAILURE)

sknFee = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)RTGS/Value-TransactionStatusReffNo/V-rtgsFee'))

SKNFee = sknFee.replace(': ', '')

WebUI.verifyMatch(SKNFee, GlobalVariable.UniversalVariable.get('SKNFee'), true)

totCharge1 = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-TransactionStatus/V-totalCharge'), FailureHandling.CONTINUE_ON_FAILURE)

totCharge = totCharge1.replace(': ', '')

GlobalVariable.UniversalVariable.put('totalCharge', totCharge)

WebUI.verifyMatch(totCharge, GlobalVariable.UniversalVariable.get('totalCharge'), true, FailureHandling.CONTINUE_ON_FAILURE)

totDebAmount1 = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-TransactionStatus/V-totalDebitAmount'), 
    FailureHandling.CONTINUE_ON_FAILURE)

totDebAmount = totDebAmount1.replace(': ', '')

GlobalVariable.UniversalVariable.put('totalDebitAmount', totDebAmount)

WebUI.verifyMatch(totDebAmount, GlobalVariable.UniversalVariable.get('totalDebitAmount'), true, FailureHandling.CONTINUE_ON_FAILURE)

chargeInst1 = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-TransactionStatus/V-chargeIntruction'), 
    FailureHandling.CONTINUE_ON_FAILURE)

chargeInst = chargeInst1.replace(': ', '')

WebUI.verifyMatch(chargeInst, GlobalVariable.UniversalVariable.get('chargeInstruction'), true, FailureHandling.CONTINUE_ON_FAILURE)

debitCharge1 = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-TransactionStatus/V-debitCharge'), FailureHandling.CONTINUE_ON_FAILURE)

debitCharge = debitCharge1.replace(': ', '')

WebUI.verifyMatch(debitCharge, GlobalVariable.UniversalVariable.get('debitCharge'), true, FailureHandling.CONTINUE_ON_FAILURE)

beneficiaryCategory1 = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-TransactionStatus/V-beneficiaryCategory'), 
    FailureHandling.CONTINUE_ON_FAILURE)

beneficiaryCategory = beneficiaryCategory1.replace(': ', '')

WebUI.verifyMatch(beneficiaryCategory, GlobalVariable.UniversalVariable.get('beneficiaryCategory'), true, FailureHandling.CONTINUE_ON_FAILURE)

transactorRelationship1 = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-TransactionStatus/V-transactorRelationship'), 
    FailureHandling.CONTINUE_ON_FAILURE)

transactorRelationship = transactorRelationship1.replace(': ', '')

WebUI.verifyMatch(transactorRelationship, GlobalVariable.UniversalVariable.get('transactorRelationship'), true, FailureHandling.CONTINUE_ON_FAILURE)

identicalStatus1 = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-TransactionStatus/V-identicalStatus'), 
    FailureHandling.CONTINUE_ON_FAILURE)

identicalStatus = identicalStatus1.replace(': ', '')

WebUI.verifyMatch(identicalStatus, GlobalVariable.UniversalVariable.get('identicalStatus'), true, FailureHandling.CONTINUE_ON_FAILURE)

purposeOfTransaction1 = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-TransactionStatus/V-purposeOfTransaction'), 
    FailureHandling.CONTINUE_ON_FAILURE)

purposeOfTransaction = purposeOfTransaction1.replace(': ', '')

WebUI.verifyMatch(purposeOfTransaction, GlobalVariable.UniversalVariable.get('purposeOfTransaction'), true, FailureHandling.CONTINUE_ON_FAILURE)

LHBUPurposeCode1 = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-TransactionStatus/V-LHBUPurposeCode'), 
    FailureHandling.CONTINUE_ON_FAILURE)

LHBUPurposeCode = LHBUPurposeCode1.replace(': ', '')

WebUI.verifyMatch(LHBUPurposeCode, GlobalVariable.UniversalVariable.get('LHBUPurposeCode'), true, FailureHandling.CONTINUE_ON_FAILURE)

LHBUDocumentType1 = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-TransactionStatus/V-LHBUDocumentType'), 
    FailureHandling.CONTINUE_ON_FAILURE)

LHBUDocumentType = LHBUDocumentType1.replace(': ', '')

WebUI.verifyMatch(LHBUDocumentType, GlobalVariable.UniversalVariable.get('LHBUDocumentType'), true, FailureHandling.CONTINUE_ON_FAILURE)

LHBUDocumentTypreDescription1 = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-TransactionStatus/V-LHBUDocumentTypeDescription'), 
    FailureHandling.CONTINUE_ON_FAILURE)

LHBUDocumentTypreDescription = LHBUDocumentTypreDescription1.replace(': ', '')

WebUI.verifyMatch(LHBUDocumentTypreDescription, GlobalVariable.UniversalVariable.get('LHBUDocumentTypeDescription'), true, 
    FailureHandling.CONTINUE_ON_FAILURE)

instructionMode1 = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-TransactionStatus/V-instructionMode'), 
    FailureHandling.CONTINUE_ON_FAILURE)

instructionMode = instructionMode1.replace(': ', '')

WebUI.verifyMatch(instructionMode, GlobalVariable.UniversalVariable.get('instructionMode'), true, FailureHandling.CONTINUE_ON_FAILURE)

on = WebUI.getText(findTestObject('MultiUserInhouse(Repeat)/Value-TransactionStatusDocNo/V-on'))

GlobalVariable.On = on.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.On, GlobalVariable.On, true)

every = WebUI.getText(findTestObject('MultiUserInhouse(Repeat)/Value-TransactionStatusDocNo/V-every'))

GlobalVariable.Every = every.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.Every, GlobalVariable.Every, true)

at = WebUI.getText(findTestObject('MultiUserInhouse(Repeat)/Value-TransactionStatusDocNo/V-at'))

GlobalVariable.At = at.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.At, GlobalVariable.At, true)

start = WebUI.getText(findTestObject('MultiUserInhouse(Repeat)/Value-TransactionStatusDocNo/V-start'))

GlobalVariable.Start = start.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.Start, GlobalVariable.Start, true)

end = WebUI.getText(findTestObject('MultiUserInhouse(Repeat)/Value-TransactionStatusDocNo/V-end'))

GlobalVariable.End = end.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.End, GlobalVariable.End, true)

nonWorkDayInst = WebUI.getText(findTestObject('MultiUserInhouse(Repeat)/Value-TransactionStatusDocNo/V-nonWorkingDayInstruction'))

GlobalVariable.NonWorkingDayInstruction = nonWorkDayInst.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.NonWorkingDayInstruction, GlobalVariable.NonWorkingDayInstruction, true)

WebUI.delay(2)

CustomKeywords.'get.Screencapture.getEntirePage'('')

WebUI.delay(2)

WebUI.scrollToElement(findTestObject('MultiUserDomestic(Immediate)SKN/Value-TransactionStatus/V-totalDebitAmount'), 0)

WebUI.delay(2)

CustomKeywords.'get.Screencapture.getEntirePage'('')

WebUI.delay(2)

WebUI.scrollToElement(findTestObject('MultiUserDomestic(Immediate)SKN/Value-TransactionStatus/V-LHBUDocumentTypeDescription'), 
    0)

WebUI.delay(2)

CustomKeywords.'get.Screencapture.getEntirePage'('')

WebUI.delay(2)

WebUI.delay(2)

