import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

Reciever = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/MainForm/Value-Confirm/V-ReffNo'), 
    FailureHandling.CONTINUE_ON_FAILURE)

Tampung = Reciever.replace('Reference No : ', '')

GlobalVariable.ReffNo = Tampung

transFrom1 = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-ApproverConfrimScreen/V-transferFrom'), 
    FailureHandling.CONTINUE_ON_FAILURE)

transFrom = transFrom1.replace(': ', '')

WebUI.verifyMatch(transFrom, GlobalVariable.UniversalVariable.get('transferFrom'), false, FailureHandling.CONTINUE_ON_FAILURE)

fromAccDes = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-ApproverConfrimScreen/V-fromAccountDescription'), 
    FailureHandling.CONTINUE_ON_FAILURE)

FromAccDes = fromAccDes.replace(': ', '')

WebUI.verifyMatch(FromAccDes, GlobalVariable.UniversalVariable.get('FromAccDes'), true, FailureHandling.CONTINUE_ON_FAILURE)

transTo = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-ApproverConfrimScreen/V-transferTo'), FailureHandling.CONTINUE_ON_FAILURE)

TransferTo = transTo.replace(': ', '')

WebUI.verifyMatch(TransferTo, GlobalVariable.UniversalVariable.get('TransferTo'), true, FailureHandling.CONTINUE_ON_FAILURE)

accName = WebUI.getText(findTestObject('MultiUserInternational(FutureDate)/V-accountNum'), FailureHandling.CONTINUE_ON_FAILURE)

AccName = accName.replace(': ', '')

WebUI.verifyMatch(AccName, GlobalVariable.UniversalVariable.get('benefAccountName'), true, FailureHandling.CONTINUE_ON_FAILURE)

benefRefNo = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-ApproverConfrimScreen/V-beneficiaryReffNumber'), 
    FailureHandling.CONTINUE_ON_FAILURE)

BenefRefNo = benefRefNo.replace(': ', '')

WebUI.verifyMatch(BenefRefNo, GlobalVariable.UniversalVariable.get('inputBeneficiaryReferenceNu'), true, FailureHandling.CONTINUE_ON_FAILURE)

toAccDes = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-ApproverConfrimScreen/V-toAccountDescription'), 
    FailureHandling.CONTINUE_ON_FAILURE)

ToAccDes = toAccDes.replace(': ', '')

WebUI.verifyMatch(ToAccDes, GlobalVariable.UniversalVariable.get('toAccountDescription'), true, FailureHandling.CONTINUE_ON_FAILURE)

remiCurrency = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-CreateConfirmScreen/V-remittanceCurrency'), 
    FailureHandling.CONTINUE_ON_FAILURE)

RemittanceCurrency = remiCurrency.replace(': ', '')

WebUI.verifyMatch(RemittanceCurrency, GlobalVariable.UniversalVariable.get('RemittanceCurrency'), true, FailureHandling.CONTINUE_ON_FAILURE)

amount = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-ApproverConfrimScreen/V-amount'), FailureHandling.CONTINUE_ON_FAILURE)

Amount = amount.replace(': ', '')

WebUI.verifyMatch(Amount, GlobalVariable.UniversalVariable.get('Amount7'), true, FailureHandling.CONTINUE_ON_FAILURE)

fuAmoCharge = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-CreateConfirmScreen/V-fullAmountCharge'), 
    FailureHandling.CONTINUE_ON_FAILURE)

FullAmountCharge = fuAmoCharge.replace(': ', '')

WebUI.verifyMatch(FullAmountCharge, GlobalVariable.UniversalVariable.get('fullAmountCharge'), true, FailureHandling.CONTINUE_ON_FAILURE)

debCharge = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-CreateConfirmScreen/V-debitCharge'), FailureHandling.CONTINUE_ON_FAILURE)

DebitCharge = debCharge.replace(': ', '')

WebUI.verifyMatch(DebitCharge, GlobalVariable.UniversalVariable.get('debitCharge'), true, FailureHandling.CONTINUE_ON_FAILURE)

provision = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-CreateConfirmScreen/V-provision'), FailureHandling.CONTINUE_ON_FAILURE)

Provision = provision.replace(': ', '')

WebUI.verifyMatch(Provision, GlobalVariable.UniversalVariable.get('Provision'), true, FailureHandling.CONTINUE_ON_FAILURE)

inLieu = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-CreateConfirmScreen/V-inLieu'), FailureHandling.CONTINUE_ON_FAILURE)

InLieu = inLieu.replace(': ', '')

WebUI.verifyMatch(InLieu, GlobalVariable.UniversalVariable.get('InLieu'), true, FailureHandling.CONTINUE_ON_FAILURE)

fuAmoCharge2 = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-CreateConfirmScreen/V-fullAmountCharge1'), 
    FailureHandling.CONTINUE_ON_FAILURE)

FullAmountCharge2 = fuAmoCharge2.replace(': ', '')

WebUI.verifyMatch(FullAmountCharge2, GlobalVariable.UniversalVariable.get('FullAmountCharge2'), true, FailureHandling.CONTINUE_ON_FAILURE)

cabFee = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-CreateConfirmScreen/V-cableFee'), FailureHandling.CONTINUE_ON_FAILURE)

CableFee = cabFee.replace(': ', '')

WebUI.verifyMatch(CableFee, GlobalVariable.UniversalVariable.get('CableFee'), true, FailureHandling.CONTINUE_ON_FAILURE)

MinTransFee = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-CreateConfirmScreen/V-minimunTransactionFee'), 
    FailureHandling.CONTINUE_ON_FAILURE)

MinimumTransactionFee = MinTransFee.replace(': ', '')

WebUI.verifyMatch(MinimumTransactionFee, GlobalVariable.UniversalVariable.get('MinimumTransactionFee'), true, FailureHandling.CONTINUE_ON_FAILURE)

corresBankFee = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-CreateConfirmScreen/V-correspondentBankFee'), 
    FailureHandling.CONTINUE_ON_FAILURE)

CorrespondentBankFee = corresBankFee.replace(': ', '')

WebUI.verifyMatch(CorrespondentBankFee, GlobalVariable.UniversalVariable.get('CorrespondentBankFee'), true, FailureHandling.CONTINUE_ON_FAILURE)

totCharge = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-ApproverConfrimScreen/V-totalCharge'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TotalCharge = totCharge.replace(': ', '')

WebUI.verifyMatch(TotalCharge, GlobalVariable.UniversalVariable.get('TotalCharge'), true, FailureHandling.CONTINUE_ON_FAILURE)

charToRemitter = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-CreateConfirmScreen/V-chargeToRemitter'), 
    FailureHandling.CONTINUE_ON_FAILURE)

ChargeToRemitter = charToRemitter.replace(': ', '')

WebUI.verifyMatch(ChargeToRemitter, GlobalVariable.UniversalVariable.get('ChargeToRemitter'), true, FailureHandling.CONTINUE_ON_FAILURE)

charToBeneficiary = WebUI.getText(findTestObject('MultiUserInternational(Immediate)/Value-CreateConfirmScreen/V-chargeToBeneficiary'), 
    FailureHandling.CONTINUE_ON_FAILURE)

ChargeToBeneficiary = charToBeneficiary.replace(': ', '')

WebUI.verifyMatch(ChargeToBeneficiary, GlobalVariable.UniversalVariable.get('ChargeToBeneficiary'), true, FailureHandling.CONTINUE_ON_FAILURE)

totDebAmount = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-ApproverConfrimScreen/V-totalDebit'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TotalDebet = totDebAmount.replace(': ', '')

WebUI.verifyMatch(TotalDebet, GlobalVariable.UniversalVariable.get('TotalDebet'), true, FailureHandling.CONTINUE_ON_FAILURE)

benefResisStat = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-ApproverConfrimScreen/V-beneficiaryResistantStatus'), 
    FailureHandling.CONTINUE_ON_FAILURE)

BenefResidentStatus = benefResisStat.replace(': ', '')

WebUI.verifyMatch(BenefResidentStatus, GlobalVariable.UniversalVariable.get('BenefResidentStatus'), true, FailureHandling.CONTINUE_ON_FAILURE)

benefCate = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-ApproverConfrimScreen/V-beneficiaryCategory'), 
    FailureHandling.CONTINUE_ON_FAILURE)

BenefCate = benefCate.replace(': ', '')

WebUI.verifyMatch(BenefCate, GlobalVariable.UniversalVariable.get('beneficiaryCategory'), true, FailureHandling.CONTINUE_ON_FAILURE)

transRela = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-ApproverConfrimScreen/V-transactorRelationship'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TransRela = transRela.replace(': ', '')

WebUI.verifyMatch(TransRela, GlobalVariable.UniversalVariable.get('transactorRelationship'), true, FailureHandling.CONTINUE_ON_FAILURE)

idenStatus = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-ApproverConfrimScreen/V-identicalStatus'), 
    FailureHandling.CONTINUE_ON_FAILURE)

IdenticalStatus = idenStatus.replace(': ', '')

WebUI.verifyMatch(IdenticalStatus, GlobalVariable.UniversalVariable.get('identicalStatus'), true, FailureHandling.CONTINUE_ON_FAILURE)

purpOfTrans = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverConfrimScreen/V-puposeOfTransaction'), 
    FailureHandling.CONTINUE_ON_FAILURE)

PurpOfTrans = purpOfTrans.replace(': ', '')

WebUI.verifyMatch(PurpOfTrans, GlobalVariable.UniversalVariable.get('purposeOfTransaction'), true, FailureHandling.CONTINUE_ON_FAILURE)

purpCode = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-ApproverConfrimScreen/V-LHBUPurposeCode'), 
    FailureHandling.CONTINUE_ON_FAILURE)

PurpCode = purpCode.replace(': ', '')

WebUI.verifyMatch(PurpCode, GlobalVariable.UniversalVariable.get('LHBUPurposeCode'), true, FailureHandling.CONTINUE_ON_FAILURE)

docType = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-ApproverConfrimScreen/V-LHBUDocumentType'), 
    FailureHandling.CONTINUE_ON_FAILURE)

DocType = docType.replace(': ', '')

WebUI.verifyMatch(DocType, GlobalVariable.UniversalVariable.get('LHBUDocumentType'), true, FailureHandling.CONTINUE_ON_FAILURE)

docTypeDes = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-ApproverConfrimScreen/V-LHBUDocumentTypeDescription'), 
    FailureHandling.CONTINUE_ON_FAILURE)

DocTypeDes = docTypeDes.replace(': ', '')

WebUI.verifyMatch(DocTypeDes, GlobalVariable.UniversalVariable.get('LHBUDocumentTypeDescription'), true, FailureHandling.CONTINUE_ON_FAILURE)

instrucMode = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-ApproverConfrimScreen/V-intructionMode'), 
    FailureHandling.CONTINUE_ON_FAILURE)

InstructionMode = instrucMode.replace(': ', '')

WebUI.verifyMatch(InstructionMode, GlobalVariable.UniversalVariable.get('InstructionMode'), true, FailureHandling.CONTINUE_ON_FAILURE)

futDate = WebUI.getText(findTestObject('MultiUserInternational(FutureDate)/Value-CreateConfirm/V-On'), FailureHandling.CONTINUE_ON_FAILURE)

FutureDate = futDate.replace(': ', '')

WebUI.verifyMatch(FutureDate, GlobalVariable.UniversalVariable.get('FutureDate'), true, FailureHandling.CONTINUE_ON_FAILURE)

at = WebUI.getText(findTestObject('MultiUserInternational(FutureDate)/Value-CreateConfirm/V-SessionTime'), FailureHandling.CONTINUE_ON_FAILURE)

At = at.replace(': ', '')

WebUI.verifyMatch(At, GlobalVariable.UniversalVariable.get('At'), true, FailureHandling.CONTINUE_ON_FAILURE)

expMode = WebUI.getText(findTestObject('MultiUserDomestic(FutureDate)SKN/Value-ApproverConfrimScreen/V-expiredOn'), FailureHandling.CONTINUE_ON_FAILURE)

expMode = expMode.replace(': ', '')

WebUI.verifyMatch(expMode, GlobalVariable.UniversalVariable.get('expMode'), true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Domestic(FutureDate)/B-Logout'), FailureHandling.CONTINUE_ON_FAILURE)

