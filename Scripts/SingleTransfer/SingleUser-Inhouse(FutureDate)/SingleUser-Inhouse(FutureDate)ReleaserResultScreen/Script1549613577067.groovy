import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(10)

menu = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverResultScreen/V-menu'))

GlobalVariable.Menu = menu.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.Menu, 'Single Transfer', true)

product = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverResultScreen/V-product'))

GlobalVariable.Product = product.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.Product, 'In-House (Overbooking)', false)

refNum = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverResultScreen/V-referenceNumber'))

GlobalVariable.ApproverRefNo = refNum.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.ApproverRefNo, GlobalVariable.ReffNo, true)

docNum = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverResultScreen/V-documentNumber'))

GlobalVariable.DocNo = docNum.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.DocNo, GlobalVariable.ReffNo, true)

date = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverResultScreen/V-date'))

GlobalVariable.Date = date.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.Date, GlobalVariable.Date, false)

transFrom = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverResultScreen/V-transferFrom'))

GlobalVariable.SenderCode = transFrom.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.SenderCode, GlobalVariable.userResultTransferFrom, false)

fromAccDes = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverResultScreen/V-fromAccountDescription'))

GlobalVariable.FromAccDes = fromAccDes.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.FromAccDes, 'PangeranInhouseImmediate', true)

transTo = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverResultScreen/V-transferToo'))

GlobalVariable.TransferTo = transTo.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.TransferTo, 'Own account', true)

accNumber = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverResultScreen/V-accNumber'))

GlobalVariable.AccNo = accNumber.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.AccNo, GlobalVariable.userBeneficiaryList, true)

toAccDes = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverResultScreen/V-toAccountDescription'))

GlobalVariable.ToAccDes = toAccDes.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.ToAccDes, 'money', true)

beneficiary = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverResultScreen/V-beneficiaryList'))

GlobalVariable.AccNo = beneficiary.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.AccNo, GlobalVariable.userResultBeneficiaryList, false)

email = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverResultScreen/V-email'))

GlobalVariable.ValueBankTypeEmail = email.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.ValueBankTypeEmail, 'Pangeran@gmail.com', true)

sms = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverResultScreen/V-sms'))

GlobalVariable.ValueBankTypeSms = sms.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.ValueBankTypeSms, '911911911911', true)

benefRefNo = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverResultScreen/V-beneficiaryReferenceNumber'))

GlobalVariable.BenefRefNo = benefRefNo.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.BenefRefNo, '19283190219', true)

amount = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverResultScreen/V-amount'))

GlobalVariable.Amount = amount.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.Amount, GlobalVariable.Amount, true)

excRate = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverResultScreen/V-exchangeRate'))

GlobalVariable.ExchangeRate = excRate.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.ExchangeRate, 'Counter Rate', true)

transFee = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverResultScreen/V-transferFee'))

GlobalVariable.TransFee = transFee.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.TransFee, GlobalVariable.TransFee, true)

totCharge = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverResultScreen/V-totalCharge'))

GlobalVariable.TotalCharge = totCharge.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.TotalCharge, GlobalVariable.TotalCharge, true)

totDebit = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverResultScreen/V-totalDebet'))

GlobalVariable.TotalDebet = totDebit.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.TotalDebet, GlobalVariable.TotalDebet, true)

charInstruct = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverResultScreen/V-chargeInstruction'))

GlobalVariable.ChargeInstruction = charInstruct.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.ChargeInstruction, 'Remitter', true)

debCharge = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverResultScreen/V-debetCharge'))

GlobalVariable.DebitCharge = debCharge.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.DebitCharge, 'Split', true)

benefCate = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverResultScreen/V-beneficiaryCategory'))

GlobalVariable.BenefCate = benefCate.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.BenefCate, '3 - Pemerintah', true)

transRela = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverResultScreen/V-transactorRelationship'))

GlobalVariable.TransRela = transRela.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.TransRela, 'A - Affiliated', true)

idenStatus = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverResultScreen/V-identicalStatus'))

GlobalVariable.IdenticalStatus = idenStatus.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.IdenticalStatus, 'Identical', true)

purpOfTrans = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverResultScreen/V-purposeOfTransaction'))

GlobalVariable.PurpOfTrans = purpOfTrans.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.PurpOfTrans, '2011 - Ekspor barang', true)

purpCode = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverResultScreen/V-LHBUPurposeCode'))

GlobalVariable.PurpCode = purpCode.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.PurpCode, '00 - 00 Investasi Penyertaan Langsung', true)

docType = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverResultScreen/V-LHBUDocumentType'))

GlobalVariable.DocType = docType.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.DocType, '001 - 001 Fotokopi Pemberitahuan Impor Barang PIB', true)

docTypeDes = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverResultScreen/V-documentTypeDescription'))

GlobalVariable.DocTypeDes = docTypeDes.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.DocTypeDes, 'aduhai', true)

instrucMode = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-ApproverResultScreen/V-instructionMode'))

GlobalVariable.InstructionMode = instrucMode.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.InstructionMode, 'Immediate', true)

at = WebUI.getText(findTestObject('MultiUserInhouse(FutureDate)/Value-ApproverResultScreen/V-futureDate'))

GlobalVariable.At = at.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.At, GlobalVariable.At, true)

futDate = WebUI.getText(findTestObject('MultiUserInhouse(FutureDate)/Value-ApproverResultScreen/V-at'))

GlobalVariable.FutureDate = futDate.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.FutureDate, GlobalVariable.FutureDate, true)

expOn = WebUI.getText(findTestObject('MultiUserInhouse(FutureDate)/Value-ApproverResultScreen/V-expiredOn'))

GlobalVariable.ExpiredOn = expOn.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.ExpiredOn, GlobalVariable.ExpiredOn, true)

GlobalVariable.ExpiredOn = expOn.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.ExpiredOn, GlobalVariable.ExpiredOn, true)

WebUI.delay(15)

