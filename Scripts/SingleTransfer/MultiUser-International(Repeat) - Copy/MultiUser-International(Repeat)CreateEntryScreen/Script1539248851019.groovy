import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import java.util.Locale as Locale
import java.text.DecimalFormat as DecimalFormat
import java.text.NumberFormat as NumberFormat

WebUI.openBrowser('')

WebUI.navigateToUrl('https://10.194.8.106:39990/')

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-CorpID'), 0)

WebUI.setText(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-CorpID'), GlobalVariable.MultiUserLoginCorpregId)

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-UserID'), 0)

WebUI.setText(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-UserID'), GlobalVariable.MultiUserLoginId)

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-UserPass'), 0)

WebUI.setText(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-UserPass'), GlobalVariable.MultiUserPass)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/B-Login'))

WebUI.delay(5)

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/B-Menu'), 0)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/B-Menu'))

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TransferManagement'), 0)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TransferManagement'))

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/SingleTransfer'), 0)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/SingleTransfer'))

WebUI.delay(5)

WebUI.click(findTestObject('MultiUserInternational(Immediate)/Value-CreateEntryScreen/B-internationalForeignCurrencyTransafer'))

WebUI.delay(3)

WebUI.click(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccount'))

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), GlobalVariable.userYanuarJPY)

WebUI.delay(5)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/MainForm/V-Tfrom'))

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/MainForm/V-FAccDes'), 0)

WebUI.setText(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/MainForm/V-FAccDes'), 'PangeranInhouseImmediate')

WebUI.delay(5)

WebUI.click(findTestObject('MultiUserInternational(Immediate)/Value-CreateEntryScreen/B-newBeneficiary'))

WebUI.setText(findTestObject('MultiUserInternational(Immediate)/Value-CreateEntryScreen/TF-aliasName'), 'dapetdapet')

WebUI.click(findTestObject('MultiUserInternational(Immediate)/Value-CreateEntryScreen/DL-beneficiaryBankCountry'))

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), 'ID')

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('MultiUserInternational(Immediate)/Value-CreateEntryScreen/DL-nationalOrganizationDirectory'))

WebUI.click(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInternational(Immediate)/Value-CreateEntryScreen/V-nationalOrganizationDirectory'))

WebUI.click(findTestObject('MultiUserInternational(Immediate)/Value-CreateEntryScreen/B-beneficiaryBank'))

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), 'INDONESIA')

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('MultiUserInternational(Immediate)/Value-CreateEntryScreen/DL-intermediaryBank'))

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), 'rere123')

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('MultiUserInternational(Immediate)/Value-CreateEntryScreen/TF-beneficiaryAccountNumber'), '123456789')

WebUI.setText(findTestObject('MultiUserInternational(Immediate)/Value-CreateEntryScreen/TF-beneficiaryAccountName'), 'dapitdapit')

WebUI.setText(findTestObject('MultiUserInternational(Immediate)/Value-CreateEntryScreen/TF-beneficiaryAddress1'), GlobalVariable.address1)

WebUI.setText(findTestObject('MultiUserInternational(Immediate)/Value-CreateEntryScreen/TF-beneficiaryAddress2'), GlobalVariable.address2)

WebUI.setText(findTestObject('MultiUserDomestic(Repeat)SKN/Value-EntryScreen/TF-email'), 'Pangeran@gmail.com')

WebUI.setText(findTestObject('MultiUserDomestic(Repeat)SKN/Value-EntryScreen/TF-sms'), '911911911911')

WebUI.setText(findTestObject('MultiUserDomestic(Repeat)SKN/Value-EntryScreen/TF-toAccountDescription'), 'money')

WebUI.setText(findTestObject('MultiUserDomestic(Repeat)SKN/Value-EntryScreen/TF-beneficiaryReferenceNumber'), '19283190219')

WebUI.click(findTestObject('MultiUserInternational(Immediate)/Value-CreateEntryScreen/DL-remittanceCurrency'))

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), 'JPY')

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('MultiUserInternational(Immediate)/Value-CreateEntryScreen/DL-selectAmount'))

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), Keys.chord(Keys.ENTER))

WebUI.waitForElementVisible(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-amount'), 0)

Random rand = new Random()

int angka

angka = rand.nextInt(999)

angka1 = ('3' + angka)

Locale local1 = new Locale('en', 'UK')

DecimalFormat amountInquiry = ((NumberFormat.getNumberInstance(local1)) as DecimalFormat)

amountInquiry.applyPattern('###,###,##0.00')

String amountInquiry1 = amountInquiry.format(new BigDecimal(angka1))

GlobalVariable.Amount7 = amountInquiry1

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-amount'), angka1)

WebUI.click(findTestObject('SingleUserInternational(Immediate)/DL-Borne by Remitter'))

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), 'borne by remitter')

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), Keys.chord(Keys.ENTER))

WebUI.delay(2)

WebUI.click(findTestObject('MultiUserInternational(Immediate)/Value-CreateEntryScreen/TF-beneficiaryCategory'))

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/MainForm/V-BenefCateg'))

WebUI.delay(1)

WebUI.click(findTestObject('MultiUserInternational(Immediate)/Value-CreateEntryScreen/TF-transactorRelationship'))

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/MainForm/V-TransactorRela'))

WebUI.click(findTestObject('MultiUserInternational(Immediate)/Value-CreateEntryScreen/TF-purposeOfTransaction'))

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/MainForm/V-PuposeOfTransaction'))

WebUI.click(findTestObject('MultiUserInternational(Immediate)/Value-CreateEntryScreen/DL-LHBUPurposeCode'))

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/MainForm/V-PuposeCode'))

WebUI.click(findTestObject('MultiUserInternational(Immediate)/Value-CreateEntryScreen/DL-LHBUDocumentType'))

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/MainForm/V-DocType'))

WebUI.setText(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/MainForm/TFA-DocTypeDes'), 'aduhai')

WebUI.click(findTestObject('MultiUserInhouse(Repeat)/Value-CreateEntryScreen/B-repeat'))

WebUI.click(findTestObject('MultiUserInhouse(Repeat)/Value-CreateEntryScreen/DL-searchSchedule'))

WebUI.click(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'))

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('MultiUserInhouse(Repeat)/Value-CreateEntryScreen/DL-every'))

WebUI.click(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'))

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), Keys.chord(Keys.ENTER))

WebUI.delay(2)

WebUI.click(findTestObject('MultiUserInhouse(Repeat)/Value-CreateEntryScreen/DL-at'))

WebUI.click(findTestObject('MultiUserInternational(Immediate)/Value-CreateConfirmScreen/V-sessions'))

WebUI.click(findTestObject('MultiUserInhouse(Repeat)/Value-CreateEntryScreen/TF-start'))

WebUI.click(findTestObject('MultiUserInhouse(Repeat)/Value-CreateEntryScreen/V-start'))

WebUI.click(findTestObject('MultiUserInhouse(Repeat)/Value-CreateEntryScreen/DL-end'))

WebUI.click(findTestObject('MultiUserInhouse(Repeat)/Value-CreateEntryScreen/V-end'))

WebUI.click(findTestObject('MultiUserInhouse(Repeat)/Value-CreateEntryScreen/DL-non-WorkingDayInstruction'))

WebUI.click(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'))

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/MainForm/B-Confim'))

WebUI.click(findTestObject('MultiUserInternational(Immediate)/Value-CreateConfirmScreen/B-submitBox'))

WebUI.delay(10)

