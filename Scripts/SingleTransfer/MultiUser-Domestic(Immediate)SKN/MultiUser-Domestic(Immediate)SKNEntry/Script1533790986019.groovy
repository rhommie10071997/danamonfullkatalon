import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import java.util.Locale as Locale
import java.text.DecimalFormat as DecimalFormat
import java.text.NumberFormat as NumberFormat
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW

///////////////////////////////////////////////////
fromAccountDescription = 'PangeranInhouseImmediate'

transferFrom = '000021942610'

toAccountDescription = 'money'

email = 'Pangeran@gmail.com'

sms = '911911911911'

inpBeneRefNum = '19283190219'

LHBUDocumentTypeDescription = 'aduhai'

///////////////////////////////////////////////////
WebUI.openBrowser('', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.navigateToUrl('https://10.194.8.106:39990/', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-CorpID'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-CorpID'), GlobalVariable.MultiUserLoginCorpregId, 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-UserID'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-UserID'), GlobalVariable.MultiUserLoginId, 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-UserPass'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-UserPass'), GlobalVariable.MultiUserPass, 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2)

CustomKeywords.'get.Screencapture.getEntirePage'('')

WebUI.delay(2)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/B-Login'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(15, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-MenuSearch'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-MenuSearch'), 'Single Transfer', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(3, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('SingleUserInhouse(immediate)/EntryScreen/Text-SingleTransfer'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('SingleUserInhouse(immediate)/EntryScreen/Text-SingleTransfer'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('SingleUSerDomestic(Immediate)/B-DomesticTransfer'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccount'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), transferFrom, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), Keys.chord(Keys.ENTER), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-fromAcctDesc'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-fromAcctDesc'), fromAccountDescription, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('SingleUSerDomestic(Immediate)/Tf-Select Beneficiary List'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('SingleUSerDomestic(Immediate)/Tf-Select Beneficiary List'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), '08080808', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), Keys.chord(Keys.ENTER), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-toAcctDesc'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-toAcctDesc'), toAccountDescription, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-beneficiaryReferenceNumber'), 0, 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-beneficiaryReferenceNumber'), inpBeneRefNum, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-amount'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Random rand = new Random()

int angka

angka = rand.nextInt(999)

angka1 = ('399' + angka)

Locale local1 = new Locale('en', 'UK')

DecimalFormat amountInquiry = ((NumberFormat.getNumberInstance(local1)) as DecimalFormat)

amountInquiry.applyPattern('###,###,##0.00')

String amountInquiry1 = amountInquiry.format(new BigDecimal(angka1))

GlobalVariable.Amount7 = amountInquiry1

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-amount'), angka1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-benefCat'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), '3 - Pemerintah', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), Keys.chord(Keys.ENTER), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-transRela'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), 'A - Affiliated', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), Keys.chord(Keys.ENTER), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-purpOfTrans'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), '2011 - Ekspor barang', 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), Keys.chord(Keys.ENTER), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('SingleUserInhouse(immediate)/EntryScreen/Tf-purpCode'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), '00 - 00 Investasi Penyertaan Langsung', 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), Keys.chord(Keys.ENTER), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-docType'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), '001 - 001 Fotokopi Pemberitahuan Impor Barang PIB ', 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), Keys.chord(Keys.ENTER), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-docTypeDes'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-docTypeDes'), LHBUDocumentTypeDescription, FailureHandling.CONTINUE_ON_FAILURE)

transferFrom1 = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-EntryScreen/V-transferFrom'), FailureHandling.CONTINUE_ON_FAILURE)

transferFrom2 = transferFrom1.replace('-', '/')

transferFrom = transferFrom2.replace('(', ' (')

GlobalVariable.UniversalVariable.put('transferFrom', transferFrom)

GlobalVariable.UniversalVariable.put('fromAccountDescription', fromAccountDescription)

transferTo = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-entry/V-transferTO'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('transferTo', transferTo)

selectBenefciaryList = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-entry/V-selectBeneficiaryList'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('selectAccount', selectBenefciaryList)

GlobalVariable.UniversalVariable.put('sms', sms)

GlobalVariable.UniversalVariable.put('email', email)

GlobalVariable.UniversalVariable.put('toAccountDescription', toAccountDescription)

GlobalVariable.UniversalVariable.put('inputBeneficiaryReferenceNu', inpBeneRefNum)

transferMethod = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-entry/V-transferMethod'))

GlobalVariable.UniversalVariable.put('transferMethod', transferMethod)

amount = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-EntryScreen/V-amount'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('amount', amount)

chargeInstruction = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-entry/V-chargeInstruction'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('chargeInstruction', chargeInstruction)

debitCharge = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-entry/V-debitCharge'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('debitCharge', debitCharge)

beneficiaryCategory = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-EntryScreen/V-beneficiaryCategory'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('beneficiaryCategory', beneficiaryCategory)

transactorRelationship = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-EntryScreen/V-transactorRelationship'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('transactorRelationship', transactorRelationship)

identicalStatus = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-EntryScreen/V-identicalStatus'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('identicalStatus', identicalStatus)

purposeOfTransaction = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-EntryScreen/V-purposeOfTransaction'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('purposeOfTransaction', purposeOfTransaction)

LHBUPurposeCode = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-EntryScreen/V-LHBUPurposeCode'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('LHBUPurposeCode', LHBUPurposeCode)

LHBUDocumentType = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-EntryScreen/V-LHBUDocumentType'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('LHBUDocumentType', LHBUDocumentType)

GlobalVariable.UniversalVariable.put('LHBUDocumentTypeDescription', LHBUDocumentTypeDescription)

instructionMode = WebUI.getText(findTestObject('MultiUserInhouse(Immediate)/Value-EntryScreen/V-instructionMode'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('instructionMode', instructionMode)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/MainForm/B-Confim'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/MainForm/B-Confim'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(10, FailureHandling.CONTINUE_ON_FAILURE)

