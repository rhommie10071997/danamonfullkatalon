import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

a = WebUI.getText(findTestObject('SingleUSerDomestic(Immediate)/Value-ConfirmScreen/1'))

String Temp1 = a.replace(': ', '')

GlobalVariable.SenderCode = Temp1

WebUI.verifyMatch(GlobalVariable.SenderCode, GlobalVariable.SenderCode, false)

b = WebUI.getText(findTestObject('SingleUSerDomestic(Immediate)/Value-ConfirmScreen/2'))

String Temp2 = b.replace(': ', '')

GlobalVariable.FromAccDes = Temp2

WebUI.verifyMatch(GlobalVariable.FromAccDes, GlobalVariable.v2, true)

c = WebUI.getText(findTestObject('SingleUSerDomestic(Immediate)/Value-ConfirmScreen/3'))

String Temp3 = c.replace(': ', '')

String Flag3 = Temp3

GlobalVariable.TransferTo = Flag3

WebUI.verifyMatch(GlobalVariable.TransferTo, GlobalVariable.V26, true)

d = WebUI.getText(findTestObject('SingleUSerDomestic(Immediate)/Value-ConfirmScreen/4'))

String Temp4 = d.replace(': ', '')

String Flag4 = Temp4

GlobalVariable.AccNo = Flag4

WebUI.verifyMatch(GlobalVariable.AccNo, GlobalVariable.V27, false)

e = WebUI.getText(findTestObject('SingleUSerDomestic(Immediate)/Value-ConfirmScreen/5'))

String Temp5 = e.replace(': ', '')

String Flag5 = Temp5

GlobalVariable.ToAccDes = Flag5

WebUI.verifyMatch(GlobalVariable.ToAccDes, GlobalVariable.v5, true)

f = WebUI.getText(findTestObject('SingleUSerDomestic(Immediate)/Value-ConfirmScreen/6'))

String Temp6 = f.replace(': ', '')

String Flag6 = Temp6

GlobalVariable.BenefRefNo = Flag6

WebUI.verifyMatch(GlobalVariable.BenefRefNo, GlobalVariable.v9, false)

g = WebUI.getText(findTestObject('SingleUSerDomestic(Immediate)/Value-ConfirmScreen/7'))

String Temp7 = g.replace(': ', '')

String Flag7 = Temp7

GlobalVariable.TransferMethod = Flag7

WebUI.verifyMatch(GlobalVariable.TransferMethod, GlobalVariable.V28, true)

h = WebUI.getText(findTestObject('SingleUSerDomestic(Immediate)/Value-ConfirmScreen/8'))

String Temp8 = h.replace(': ', '')

String Flag8 = Temp8

GlobalVariable.BenefType = Flag8

WebUI.verifyMatch(GlobalVariable.BenefType, GlobalVariable.V29, true)

j = WebUI.getText(findTestObject('SingleUSerDomestic(Immediate)/Value-ConfirmScreen/9'))

String Temp10 = j.replace(': ', '')

String Flag10 = Temp10

GlobalVariable.Amount = Flag10

String verifyAmount = 'IDR ' + GlobalVariable.Amount7

GlobalVariable.Amount6 = verifyAmount

WebUI.verifyMatch(GlobalVariable.Amount, GlobalVariable.Amount, true)

k = WebUI.getText(findTestObject('SingleUSerDomestic(Immediate)/Value-ConfirmScreen/10'))

String Temp11 = k.replace(': ', '')

String Flag11 = Temp11

GlobalVariable.ExchangeRate = Flag11

WebUI.verifyMatch(GlobalVariable.ExchangeRate, GlobalVariable.v11, true)

l = WebUI.getText(findTestObject('SingleUSerDomestic(Immediate)/Value-ConfirmScreen/11'))

String Temp12 = l.replace(': ', '')

String Flag12 = Temp12

GlobalVariable.TransFee = Flag12

WebUI.verifyMatch(GlobalVariable.TransFee, GlobalVariable.v15, true)

i = WebUI.getText(findTestObject('SingleUSerDomestic(Immediate)/Value-ConfirmScreen/12'))

String Temp9 = i.replace(': ', '')

String Flag9 = Temp9

GlobalVariable.SKNFee = Flag9

WebUI.verifyMatch(GlobalVariable.SKNFee, GlobalVariable.v16, true)

x = WebUI.getText(findTestObject('SingleUSerDomestic(Immediate)/Value-ConfirmScreen/13'))

String Temp26 = x.replace(': ', '')

String Flag26 = Temp26

GlobalVariable.BenefResidentStatus = Flag26

WebUI.verifyMatch(GlobalVariable.BenefResidentStatus, GlobalVariable.BenefResidentStatus, true)

m = WebUI.getText(findTestObject('SingleUSerDomestic(Immediate)/Value-ConfirmScreen/14'))

String Temp13 = m.replace(': ', '')

String Flag13 = Temp13

GlobalVariable.TotalCharge = Flag13

WebUI.verifyMatch(GlobalVariable.TotalCharge, GlobalVariable.TotalCharge, true)

n = WebUI.getText(findTestObject('SingleUSerDomestic(Immediate)/Value-ConfirmScreen/15'))

String Temp14 = n.replace(': ', '')

String Flag14 = Temp14

GlobalVariable.TotalDebet = Flag14

WebUI.verifyMatch(GlobalVariable.TotalDebet, GlobalVariable.TotalDebet, true)

o = WebUI.getText(findTestObject('SingleUSerDomestic(Immediate)/Value-ConfirmScreen/16'))

String Temp15 = o.replace(': ', '')

String Flag15 = Temp15

GlobalVariable.ChargeInstruction = Flag15

WebUI.verifyMatch(GlobalVariable.ChargeInstruction, GlobalVariable.ChargeInstruction, true)

p = WebUI.getText(findTestObject('SingleUSerDomestic(Immediate)/Value-ConfirmScreen/17'))

String Temp16 = p.replace(': ', '')

String Flag16 = Temp16

GlobalVariable.DebitCharge = Flag16

WebUI.verifyMatch(GlobalVariable.DebitCharge, GlobalVariable.DebitCharge, true)

q = WebUI.getText(findTestObject('SingleUSerDomestic(Immediate)/Value-ConfirmScreen/18'))

String Temp17 = q.replace(': ', '')

String Flag17 = Temp17

GlobalVariable.BenefCate = Flag17

WebUI.verifyMatch(GlobalVariable.BenefCate, GlobalVariable.v18, true)

r = WebUI.getText(findTestObject('SingleUSerDomestic(Immediate)/Value-ConfirmScreen/19'))

String Temp18 = r.replace(': ', '')

String Flag18 = Temp18

GlobalVariable.TransRela = Flag18

WebUI.verifyMatch(GlobalVariable.TransRela, GlobalVariable.v19, true)

s = WebUI.getText(findTestObject('SingleUSerDomestic(Immediate)/Value-ConfirmScreen/20'))

String Temp19 = s.replace(': ', '')

String Flag19 = Temp19

GlobalVariable.IdenticalStatus = Flag19

WebUI.verifyMatch(GlobalVariable.IdenticalStatus, GlobalVariable.v20, true)

t = WebUI.getText(findTestObject('SingleUSerDomestic(Immediate)/Value-ConfirmScreen/21'))

String Temp20 = t.replace(': ', '')

String Flag20 = Temp20

GlobalVariable.PurpOfTrans = Flag20

WebUI.verifyMatch(GlobalVariable.PurpOfTrans, GlobalVariable.v21, true)

t = WebUI.getText(findTestObject('SingleUSerDomestic(Immediate)/Value-ConfirmScreen/22'))

String Temp21 = t.replace(': ', '')

String Flag21 = Temp21

GlobalVariable.PurpCode = Flag21

WebUI.verifyMatch(GlobalVariable.PurpCode, GlobalVariable.v22, true)

t = WebUI.getText(findTestObject('SingleUSerDomestic(Immediate)/Value-ConfirmScreen/23'))

String Temp22 = t.replace(': ', '')

String Flag22 = Temp22

GlobalVariable.DocType = Flag22

WebUI.verifyMatch(GlobalVariable.DocType, GlobalVariable.v23, true)

u = WebUI.getText(findTestObject('SingleUSerDomestic(Immediate)/Value-ConfirmScreen/24'))

String Temp23 = u.replace(': ', '')

String Flag23 = Temp23

GlobalVariable.DocTypeDes = Flag23

WebUI.verifyMatch(GlobalVariable.DocTypeDes, GlobalVariable.v24, true)

v = WebUI.getText(findTestObject('SingleUSerDomestic(Immediate)/Value-ConfirmScreen/25'))

String Temp24 = v.replace(': ', '')

String Flag24 = Temp24

GlobalVariable.InstructionMode = Flag24

WebUI.verifyMatch(GlobalVariable.InstructionMode, GlobalVariable.InstructionMode, true)

w = WebUI.getText(findTestObject('SingleUSerDomestic(Immediate)/Value-ConfirmScreen/26'))

String Temp25 = w.replace(': ', '')

String Flag25 = Temp25

GlobalVariable.InstructionMode = Flag25

WebUI.verifyMatch(GlobalVariable.InstructionMode, GlobalVariable.InstructionMode, true)

