import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-CorpID'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-CorpID'), 'corpreg01', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-UserID'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-UserID'), 'releaser01', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-UserPass'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-UserPass'), 'Password123', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/B-Login'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(15, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/B-Menu'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/B-Menu'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/PendingTask/T-PendingTask'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/PendingTask/T-DisplayOption'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/PendingTask/T-DisplayOption'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/PendingTask/C-RefNo'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/PendingTask/C-RefNo'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/PendingTask/TF-TransReffNo'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/PendingTask/TF-TransReffNo'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/PendingTask/TF-TransReffNo'), GlobalVariable.ReffNo, 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(30, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/PendingTask/B-Show'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.doubleClick(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/PendingTask/CB-DetailFormPendingTask'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

menu = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverConfrimScreen/V-menu'), FailureHandling.CONTINUE_ON_FAILURE)

String temp1 = menu.replace(': ', '')

GlobalVariable.Menu = temp1

WebUI.verifyMatch(GlobalVariable.Menu, 'Single Transfer', true, FailureHandling.CONTINUE_ON_FAILURE)

product = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverConfrimScreen/V-product'), FailureHandling.CONTINUE_ON_FAILURE)

String temp2 = product.replace(': ', '')

GlobalVariable.Product = temp2

WebUI.verifyMatch(GlobalVariable.Product, 'Domestic Transfer', false, FailureHandling.CONTINUE_ON_FAILURE)

refNum = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverConfrimScreen/V-transactionReffNumber'), 
    FailureHandling.CONTINUE_ON_FAILURE)

String temp3 = refNum.replace(': ', '')

GlobalVariable.ApproverRefNo = temp3

WebUI.verifyMatch(GlobalVariable.ApproverRefNo, GlobalVariable.ReffNo, true, FailureHandling.CONTINUE_ON_FAILURE)

docNum = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverConfrimScreen/V-documentNumber'), FailureHandling.CONTINUE_ON_FAILURE)

String temp4 = docNum.replace(': ', '')

GlobalVariable.DocCode = temp4

WebUI.verifyMatch(GlobalVariable.DocCode, GlobalVariable.ReffNo, true, FailureHandling.CONTINUE_ON_FAILURE)

date = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverConfrimScreen/V-date'), FailureHandling.CONTINUE_ON_FAILURE)

String temp5 = date.replace(': ', '')

GlobalVariable.Date = temp5

WebUI.verifyMatch(GlobalVariable.Date, GlobalVariable.Date, false, FailureHandling.CONTINUE_ON_FAILURE)

transFrom = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverConfrimScreen/V-transferFrom'), 
    FailureHandling.CONTINUE_ON_FAILURE)

String temp6 = transFrom.replace(': ', '')

GlobalVariable.SenderCode = temp6

WebUI.verifyMatch(GlobalVariable.SenderCode, '000001825736/ AANG SUBHAN (IDR)', false, FailureHandling.CONTINUE_ON_FAILURE)

fromAccDes = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverConfrimScreen/V-fromAccountDescription'), 
    FailureHandling.CONTINUE_ON_FAILURE)

String temp7 = fromAccDes.replace(': ', '')

GlobalVariable.FromAccDes = temp7

WebUI.verifyMatch(GlobalVariable.FromAccDes, 'PangeranInhouseImmediate', true, FailureHandling.CONTINUE_ON_FAILURE)

transTo = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverConfrimScreen/V-transferTo'), FailureHandling.CONTINUE_ON_FAILURE)

String temp8 = transTo.replace(': ', '')

GlobalVariable.TransferTo = temp8

WebUI.verifyMatch(GlobalVariable.TransferTo, 'Beneficiary List', true, FailureHandling.CONTINUE_ON_FAILURE)

beneficiary = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverConfrimScreen/V-beneficiary'), 
    FailureHandling.CONTINUE_ON_FAILURE)

String temp9 = beneficiary.replace(': ', '')

GlobalVariable.AccNo = temp9

WebUI.verifyMatch(GlobalVariable.AccNo, '3543654657 - Zuskie Azzahra (IDR)', false, FailureHandling.CONTINUE_ON_FAILURE)

toAccDes = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverConfrimScreen/V-toAccountDescription'), 
    FailureHandling.CONTINUE_ON_FAILURE)

String temp10 = toAccDes.replace(': ', '')

GlobalVariable.ToAccDes = temp10

WebUI.verifyMatch(GlobalVariable.ToAccDes, 'money', true, FailureHandling.CONTINUE_ON_FAILURE)

benefRefNo = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverConfrimScreen/V-beneficiaryReffNumber'), 
    FailureHandling.CONTINUE_ON_FAILURE)

String temp11 = benefRefNo.replace(': ', '')

GlobalVariable.BenefRefNo = temp11

WebUI.verifyMatch(GlobalVariable.BenefRefNo, '19283190219', true, FailureHandling.CONTINUE_ON_FAILURE)

transMethod = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverConfrimScreen/V-transferMethod'), 
    FailureHandling.CONTINUE_ON_FAILURE)

String temp12 = transMethod.replace(': ', '')

GlobalVariable.TransferMethod = temp12

WebUI.verifyMatch(GlobalVariable.TransferMethod, 'SKN', true, FailureHandling.CONTINUE_ON_FAILURE)

benefType = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverConfrimScreen/V-beneficiaryType'), 
    FailureHandling.CONTINUE_ON_FAILURE)

String temp13 = benefType.replace(': ', '')

GlobalVariable.BenefType = temp13

WebUI.verifyMatch(GlobalVariable.BenefType, '1', true, FailureHandling.CONTINUE_ON_FAILURE)

amount = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverConfrimScreen/V-amount'), FailureHandling.CONTINUE_ON_FAILURE)

String temp14 = amount.replace(': ', '')

GlobalVariable.Amount = temp14

WebUI.verifyMatch(GlobalVariable.Amount, GlobalVariable.Amount, true, FailureHandling.CONTINUE_ON_FAILURE)

excRate = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverConfrimScreen/V-exchangeRate'), FailureHandling.CONTINUE_ON_FAILURE)

String temp15 = excRate.replace(': ', '')

GlobalVariable.ExchangeRate = temp15

WebUI.verifyMatch(GlobalVariable.ExchangeRate, 'Counter Rate', true, FailureHandling.CONTINUE_ON_FAILURE)

charInstruct = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverConfrimScreen/V-chargeInstuction'), 
    FailureHandling.CONTINUE_ON_FAILURE)

String temp16 = charInstruct.replace(': ', '')

GlobalVariable.ChargeInstruction = temp16

WebUI.verifyMatch(GlobalVariable.ChargeInstruction, 'Remitter', true, FailureHandling.CONTINUE_ON_FAILURE)

debCharge = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverConfrimScreen/V-debitCharge'), FailureHandling.CONTINUE_ON_FAILURE)

String temp17 = debCharge.replace(': ', '')

GlobalVariable.DebitCharge = temp17

WebUI.verifyMatch(GlobalVariable.DebitCharge, 'Split', true, FailureHandling.CONTINUE_ON_FAILURE)

sknFee = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverConfrimScreen/V-SKNFee'), FailureHandling.CONTINUE_ON_FAILURE)

String temp18 = sknFee.replace(': ', '')

GlobalVariable.SKNFee = temp18

WebUI.verifyMatch(GlobalVariable.SKNFee, GlobalVariable.SKNFee, true, FailureHandling.CONTINUE_ON_FAILURE)

transFee = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverConfrimScreen/V-transferFee'), FailureHandling.CONTINUE_ON_FAILURE)

String temp19 = transFee.replace(': ', '')

GlobalVariable.TransFee = temp19

WebUI.verifyMatch(GlobalVariable.TransFee, GlobalVariable.TransFee, true, FailureHandling.CONTINUE_ON_FAILURE)

totCharge = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverConfrimScreen/V-totalCharge'), FailureHandling.CONTINUE_ON_FAILURE)

String temp20 = totCharge.replace(': ', '')

GlobalVariable.TotalCharge = temp20

WebUI.verifyMatch(GlobalVariable.TotalCharge, GlobalVariable.TotalCharge, true, FailureHandling.CONTINUE_ON_FAILURE)

totDebit = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverConfrimScreen/V-totalDebit'), FailureHandling.CONTINUE_ON_FAILURE)

String temp21 = totDebit.replace(': ', '')

GlobalVariable.TotalDebet = temp21

WebUI.verifyMatch(GlobalVariable.TotalDebet, GlobalVariable.TotalDebet, true, FailureHandling.CONTINUE_ON_FAILURE)

benefResisStat = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverConfrimScreen/V-beneficiaryResistantStatus'), 
    FailureHandling.CONTINUE_ON_FAILURE)

String temp22 = benefResisStat.replace(': ', '')

GlobalVariable.BenefResidentStatus = temp22

WebUI.verifyMatch(GlobalVariable.BenefResidentStatus, 'Resident', true, FailureHandling.CONTINUE_ON_FAILURE)

benefCate = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverConfrimScreen/V-beneficiaryCategory'), 
    FailureHandling.CONTINUE_ON_FAILURE)

String temp23 = benefCate.replace(': ', '')

GlobalVariable.BenefCate = temp23

WebUI.verifyMatch(GlobalVariable.BenefCate, '3 - Pemerintah', true, FailureHandling.CONTINUE_ON_FAILURE)

transRela = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverConfrimScreen/V-transactorRelationship'), 
    FailureHandling.CONTINUE_ON_FAILURE)

String temp24 = transRela.replace(': ', '')

GlobalVariable.TransRela = temp24

WebUI.verifyMatch(GlobalVariable.TransRela, 'A - Affiliated', true, FailureHandling.CONTINUE_ON_FAILURE)

idenStatus = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverConfrimScreen/V-identicalStatus'), 
    FailureHandling.CONTINUE_ON_FAILURE)

String temp25 = idenStatus.replace(': ', '')

GlobalVariable.IdenticalStatus = temp25

WebUI.verifyMatch(GlobalVariable.IdenticalStatus, 'Identical', true, FailureHandling.CONTINUE_ON_FAILURE)

purpOfTrans = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverConfrimScreen/V-puposeOfTransaction'), 
    FailureHandling.CONTINUE_ON_FAILURE)

String temp26 = purpOfTrans.replace(': ', '')

GlobalVariable.PurpOfTrans = temp26

WebUI.verifyMatch(GlobalVariable.PurpOfTrans, '2011 - Ekspor barang', true, FailureHandling.CONTINUE_ON_FAILURE)

purpCode = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverConfrimScreen/V-LHBUPurposeCode'), 
    FailureHandling.CONTINUE_ON_FAILURE)

String temp27 = purpCode.replace(': ', '')

GlobalVariable.PurpCode = temp27

WebUI.verifyMatch(GlobalVariable.PurpCode, '00 - 00 Investasi Penyertaan Langsung', true, FailureHandling.CONTINUE_ON_FAILURE)

docType = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverConfrimScreen/V-LHBUDocumentType'), 
    FailureHandling.CONTINUE_ON_FAILURE)

String temp28 = docType.replace(': ', '')

GlobalVariable.DocType = temp28

WebUI.verifyMatch(GlobalVariable.DocType, '001 - 001 Fotokopi Pemberitahuan Impor Barang PIB', true, FailureHandling.CONTINUE_ON_FAILURE)

docTypeDes = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverConfrimScreen/V-LHBUDocumentTypeDescription'), 
    FailureHandling.CONTINUE_ON_FAILURE)

String temp29 = docTypeDes.replace(': ', '')

GlobalVariable.DocTypeDes = temp29

WebUI.verifyMatch(GlobalVariable.DocTypeDes, 'aduhai', true, FailureHandling.CONTINUE_ON_FAILURE)

instrucMode = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverConfrimScreen/V-intructionMode'), 
    FailureHandling.CONTINUE_ON_FAILURE)

String temp30 = instrucMode.replace(': ', '')

GlobalVariable.InstructionMode = temp30

WebUI.verifyMatch(GlobalVariable.InstructionMode, 'Immediate', true, FailureHandling.CONTINUE_ON_FAILURE)

expOn = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverConfrimScreen/V-expiredOn'), FailureHandling.CONTINUE_ON_FAILURE)

String temp31 = expOn.replace(': ', '')

GlobalVariable.ExpiredOn = temp31

WebUI.verifyMatch(GlobalVariable.ExpiredOn, GlobalVariable.ExpiredOn, true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('B-Approve'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('MultiUserDomestic(Immediate)SKN/tf-responseCode'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserDomestic(Immediate)SKN/tf-responseCode'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('MultiUserDomestic(Immediate)SKN/tf-responseCode'), '123456', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('B-Approve'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/PendingTask/B-FinalOKApprover'), FailureHandling.CONTINUE_ON_FAILURE)

