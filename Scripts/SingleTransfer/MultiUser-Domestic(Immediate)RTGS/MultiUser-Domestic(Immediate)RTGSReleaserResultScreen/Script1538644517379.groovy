import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

menu = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverResultScreen/1'))

String temp1 = menu.replace(': ', '')

GlobalVariable.Menu = temp1

WebUI.verifyMatch(GlobalVariable.Menu, 'Single Transfer', true)

product = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverResultScreen/2'))

String temp2 = product.replace(': ', '')

GlobalVariable.Product = temp2

WebUI.verifyMatch(GlobalVariable.Product, 'Domestic Transfer', false)

refNum = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverResultScreen/3'))

String temp3 = refNum.replace(': ', '')

GlobalVariable.ApproverRefNo = temp3

WebUI.verifyMatch(GlobalVariable.ApproverRefNo, GlobalVariable.ReffNo, true)

transFrom = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverResultScreen/4'))

String temp4 = transFrom.replace(': ', '')

GlobalVariable.SenderCode = temp4

WebUI.verifyMatch(GlobalVariable.SenderCode, '000001825736/ AANG SUBHAN (IDR)', false)

fromAccDes = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverResultScreen/5'))

String temp5 = fromAccDes.replace(': ', '')

GlobalVariable.FromAccDes = temp5

WebUI.verifyMatch(GlobalVariable.FromAccDes, 'PangeranInhouseImmediate', true)

transTo = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverResultScreen/6'))

String temp6 = transTo.replace(': ', '')

GlobalVariable.TransferTo = temp6

WebUI.verifyMatch(GlobalVariable.TransferTo, 'Beneficiary List', true)

beneficiary = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverResultScreen/7'))

String temp7 = beneficiary.replace(': ', '')

GlobalVariable.AccNo = temp7

WebUI.verifyMatch(GlobalVariable.AccNo, '3543654657 - Zuskie Azzahra (IDR)', false)

toAccDes = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverResultScreen/8'))

String temp8 = toAccDes.replace(': ', '')

GlobalVariable.ToAccDes = temp8

WebUI.verifyMatch(GlobalVariable.ToAccDes, 'money', true)

benefRefNo = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverResultScreen/9'))

String temp9 = benefRefNo.replace(': ', '')

GlobalVariable.BenefRefNo = temp9

WebUI.verifyMatch(GlobalVariable.BenefRefNo, '19283190219', true)

transMethod = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverResultScreen/10'))

String temp10 = transMethod.replace(': ', '')

GlobalVariable.TransferMethod = temp10

WebUI.verifyMatch(GlobalVariable.TransferMethod, 'SKN', true)

benefType = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverResultScreen/11'))

String temp11 = benefType.replace(': ', '')

GlobalVariable.BenefType = temp11

WebUI.verifyMatch(GlobalVariable.BenefType, '1', true)

amount = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverResultScreen/12'))

String temp12 = amount.replace(': ', '')

GlobalVariable.Amount = temp12

WebUI.verifyMatch(GlobalVariable.Amount, GlobalVariable.Amount, true)

excRate = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverResultScreen/13'))

String temp13 = excRate.replace(': ', '')

GlobalVariable.ExchangeRate = temp13

WebUI.verifyMatch(GlobalVariable.ExchangeRate, 'Counter Rate', true)

charInstruct = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverResultScreen/14'))

String temp14 = charInstruct.replace(': ', '')

GlobalVariable.ChargeInstruction = temp14

WebUI.verifyMatch(GlobalVariable.ChargeInstruction, 'Remitter', true)

debCharge = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverResultScreen/15'))

String temp15 = debCharge.replace(': ', '')

GlobalVariable.DebitCharge = temp15

WebUI.verifyMatch(GlobalVariable.DebitCharge, 'Split', true)

sknFee = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverResultScreen/16'))

String temp16 = sknFee.replace(': ', '')

GlobalVariable.SKNFee = temp16

WebUI.verifyMatch(GlobalVariable.SKNFee, GlobalVariable.SKNFee, true)

transFee = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverResultScreen/17'))

String temp17 = transFee.replace(': ', '')

GlobalVariable.TransFee = temp17

WebUI.verifyMatch(GlobalVariable.TransFee, GlobalVariable.TransFee, true)

totCharge = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverResultScreen/18'))

String temp18 = totCharge.replace(': ', '')

GlobalVariable.TotalCharge = temp18

WebUI.verifyMatch(GlobalVariable.TotalCharge, GlobalVariable.TotalCharge, true)

totDebit = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverResultScreen/19'))

String temp19 = totDebit.replace(': ', '')

GlobalVariable.TotalDebet = temp19

WebUI.verifyMatch(GlobalVariable.TotalDebet, GlobalVariable.TotalDebet, true)

benefResisStat = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverResultScreen/20'))

String temp20 = benefResisStat.replace(': ', '')

GlobalVariable.BenefResidentStatus = temp20

WebUI.verifyMatch(GlobalVariable.BenefResidentStatus, 'Resident', true)

benefCate = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverResultScreen/21'))

String temp21 = benefCate.replace(': ', '')

GlobalVariable.BenefCate = temp21

WebUI.verifyMatch(GlobalVariable.BenefCate, '3 - Pemerintah', true)

transRela = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverResultScreen/22'))

String temp22 = transRela.replace(': ', '')

GlobalVariable.TransRela = temp22

WebUI.verifyMatch(GlobalVariable.TransRela, 'A - Affiliated', true)

idenStatus = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverResultScreen/23'))

String temp23 = idenStatus.replace(': ', '')

GlobalVariable.IdenticalStatus = temp23

WebUI.verifyMatch(GlobalVariable.IdenticalStatus, 'Identical', true)

purpOfTrans = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverResultScreen/24'))

String temp24 = purpOfTrans.replace(': ', '')

GlobalVariable.PurpOfTrans = temp24

WebUI.verifyMatch(GlobalVariable.PurpOfTrans, '2011 - Ekspor barang', true)

purpCode = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverResultScreen/25'))

String temp25 = purpCode.replace(': ', '')

GlobalVariable.PurpCode = temp25

WebUI.verifyMatch(GlobalVariable.PurpCode, '00 - 00 Investasi Penyertaan Langsung', true)

docType = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverResultScreen/26'))

String temp26 = docType.replace(': ', '')

GlobalVariable.DocType = temp26

WebUI.verifyMatch(GlobalVariable.DocType, '001 - 001 Fotokopi Pemberitahuan Impor Barang PIB', true)

docTypeDes = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverResultScreen/27'))

String temp27 = docTypeDes.replace(': ', '')

GlobalVariable.DocTypeDes = temp27

WebUI.verifyMatch(GlobalVariable.DocTypeDes, 'aduhai', true)

instrucMode = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverResultScreen/28'))

String temp28 = instrucMode.replace(': ', '')

GlobalVariable.InstructionMode = temp28

WebUI.verifyMatch(GlobalVariable.InstructionMode, 'Immediate', true)

expOn = WebUI.getText(findTestObject('MultiUserDomestic(Immediate)SKN/Value-ApproverResultScreen/29'))

String temp29 = expOn.replace(': ', '')

GlobalVariable.ExpiredOn = temp29

WebUI.verifyMatch(GlobalVariable.ExpiredOn, GlobalVariable.ExpiredOn, true)

WebUI.delay(60)

