import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

reffNo = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-TransactionStatus(ReffNo)/V-referenceNumber'))

GlobalVariable.ReffNo = reffNo.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.ReffNo, GlobalVariable.ReffNo, true)

date = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-TransactionStatus(ReffNo)/V-date'))

GlobalVariable.Date = date.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.Date, GlobalVariable.Date, false)

menu = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-TransactionStatus(ReffNo)/V-menu'))

GlobalVariable.Menu = menu.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.Menu, GlobalVariable.Menu, true)

product = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-TransactionStatus(ReffNo)/V-product'))

GlobalVariable.Product = product.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.Product, GlobalVariable.Product, false)

transFrom = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-TransactionStatus(ReffNo)/V-transferFrom'))

GlobalVariable.SenderCode = transFrom.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.SenderCode, GlobalVariable.SenderCode, false)

fromAccDes = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-TransactionStatus(ReffNo)/V-fromAccountDescription'))

GlobalVariable.FromAccDes = fromAccDes.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.FromAccDes, GlobalVariable.v2, true)

transTo = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-TransactionStatus(ReffNo)/V-transferTo'))

GlobalVariable.TransferTo = transTo.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.TransferTo, 'New Beneficiary', true)

accName = WebUI.getText(findTestObject('MultiUserInhouse(Repeat)/Value-TransactionStatusReffNo/V-accountName'))

GlobalVariable.AccName = accName.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.AccName, GlobalVariable.userResultBeneficiaryList2, false)

saveToBeneList = WebUI.getText(findTestObject('MultiUserInhouse(Repeat)/Value-TransactionStatusReffNo/V-saveToBeneficiaryList'))

GlobalVariable.saveToBeneficiaryList = saveToBeneList

WebUI.verifyMatch(GlobalVariable.saveToBeneficiaryList, GlobalVariable.saveToBeneficiaryList, true)

alName = WebUI.getText(findTestObject('MultiUserInhouse(Repeat)/Value-TransactionStatusReffNo/V-aliasName'))

GlobalVariable.aliasName = alName.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.aliasName, GlobalVariable.aliasName, true)

toAccDes = WebUI.getText(findTestObject('MultiUserInhouse(Repeat)/Value-TransactionStatusReffNo/V-toAccountDescription'))

GlobalVariable.ToAccDes = toAccDes.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.ToAccDes, GlobalVariable.v5, true)

email = WebUI.getText(findTestObject('MultiUserInhouse(Repeat)/Value-TransactionStatusReffNo/V-Email'))

GlobalVariable.ValueBankTypeEmail = email.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.ValueBankTypeEmail, GlobalVariable.v7, true)

sms = WebUI.getText(findTestObject('MultiUserInhouse(Repeat)/Value-TransactionStatusReffNo/V-sms'))

GlobalVariable.ValueBankTypeSms = sms.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.ValueBankTypeSms, GlobalVariable.v8, true)

beneRefNo = WebUI.getText(findTestObject('MultiUserInhouse(Repeat)/Value-TransactionStatusReffNo/V-beneficiaryReferenceNumber'))

GlobalVariable.BenefRefNo = beneRefNo.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.BenefRefNo, GlobalVariable.v9, true)

amount = WebUI.getText(findTestObject('MultiUserInhouse(Repeat)/Value-TransactionStatusReffNo/V-amount'))

GlobalVariable.Amount = amount.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.Amount, GlobalVariable.Amount, true)

excRate = WebUI.getText(findTestObject('MultiUserInhouse(Repeat)/Value-TransactionStatusReffNo/V-exchangeRate'))

GlobalVariable.ExchangeRate = excRate.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.ExchangeRate, GlobalVariable.v11, true)

totDebet = WebUI.getText(findTestObject('MultiUserInhouse(Repeat)/Value-TransactionStatusReffNo/V-totalDebetAmount'))

GlobalVariable.TotalDebet = totDebet.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.TotalDebet, GlobalVariable.TotalDebet, true)

charIns = WebUI.getText(findTestObject('MultiUserInhouse(Repeat)/Value-TransactionStatusReffNo/V-chargeInstruction'))

GlobalVariable.ChargeInstruction = charIns.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.ChargeInstruction, GlobalVariable.v15, true)

debChar = WebUI.getText(findTestObject('MultiUserInhouse(Repeat)/Value-TransactionStatusReffNo/V-debitCharge'))

GlobalVariable.DebitCharge = debChar.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.DebitCharge, GlobalVariable.v16, true)

beneCat = WebUI.getText(findTestObject('MultiUserInhouse(Repeat)/Value-TransactionStatusReffNo/V-beneficiaryCategory'))

GlobalVariable.BenefCate = beneCat.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.BenefCate, GlobalVariable.v17, true)

tranRela = WebUI.getText(findTestObject('MultiUserInhouse(Repeat)/Value-TransactionStatusReffNo/V-transactorRelationship'))

GlobalVariable.TransRela = tranRela.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.TransRela, GlobalVariable.v18, true)

idenStat = WebUI.getText(findTestObject('MultiUserInhouse(Repeat)/Value-TransactionStatusReffNo/V-identicalStatus'))

GlobalVariable.IdenticalStatus = idenStat.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.IdenticalStatus, GlobalVariable.v19, true)

purpOfTrans = WebUI.getText(findTestObject('MultiUserInhouse(Repeat)/Value-TransactionStatusReffNo/V-purposeOfTransaction'))

GlobalVariable.PurpOfTrans = purpOfTrans.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.PurpOfTrans, GlobalVariable.v20, true)

purpCode = WebUI.getText(findTestObject('MultiUserInhouse(Repeat)/Value-TransactionStatusReffNo/V-LHBUPurposeCode'))

GlobalVariable.PurpCode = purpCode.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.PurpCode, GlobalVariable.v21, true)

docType = WebUI.getText(findTestObject('MultiUserInhouse(Repeat)/Value-TransactionStatusReffNo/V-LHBUDocumentType'))

GlobalVariable.DocType = docType.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.DocType, GlobalVariable.v22, true)

docTypeDes = WebUI.getText(findTestObject('MultiUserInhouse(Repeat)/Value-TransactionStatusReffNo/V-LHBUDocumentTypeDescription'))

GlobalVariable.DocTypeDes = docTypeDes.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.DocTypeDes, GlobalVariable.v23, true)

insMode = WebUI.getText(findTestObject('MultiUserInhouse(Repeat)/Value-TransactionStatusReffNo/V-instructionMode'))

GlobalVariable.InstructionMode = insMode.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.InstructionMode, 'Repeat', true)

on = WebUI.getText(findTestObject('MultiUserInhouse(Repeat)/Value-TransactionStatusReffNo/V-on'))

GlobalVariable.On = on.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.On, GlobalVariable.On, true)

every = WebUI.getText(findTestObject('MultiUserInhouse(Repeat)/Value-TransactionStatusReffNo/V-every'))

GlobalVariable.Every = every.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.Every, GlobalVariable.Every, true)

at = WebUI.getText(findTestObject('MultiUserInhouse(Repeat)/Value-TransactionStatusReffNo/V-at'))

GlobalVariable.At = at.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.At, GlobalVariable.At, true)

start = WebUI.getText(findTestObject('MultiUserInhouse(Repeat)/Value-TransactionStatusReffNo/V-start'))

GlobalVariable.Start = start.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.Start, GlobalVariable.Start, true)

end = WebUI.getText(findTestObject('MultiUserInhouse(Repeat)/Value-TransactionStatusReffNo/V-end'))

GlobalVariable.End = end.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.End, GlobalVariable.End, true)

nonWorkDayInst = WebUI.getText(findTestObject('MultiUserInhouse(Repeat)/Value-TransactionStatusReffNo/V-nonWorkingDayInstruction'))

GlobalVariable.NonWorkingDayInstruction = nonWorkDayInst.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.NonWorkingDayInstruction, GlobalVariable.NonWorkingDayInstruction, true)

WebUI.delay(2)

CustomKeywords.'get.Screencapture.getEntirePage'('')

WebUI.delay(2)

WebUI.scrollToElement(findTestObject('SingleUserInhouse(immediate)/Value-TransactionStatus(ReffNo)/V-totalCharge'), 0)

WebUI.delay(2)

CustomKeywords.'get.Screencapture.getEntirePage'('')

WebUI.delay(2)

WebUI.scrollToElement(findTestObject('SingleUserInhouse(immediate)/Value-TransactionStatus(ReffNo)/V-LHBUDocumentTypeDescription'), 
    0)

WebUI.delay(2)

CustomKeywords.'get.Screencapture.getEntirePage'('')

WebUI.delay(2)

WebUI.delay(15)

