import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

transStat = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-TransactionStatus(DocNo)/V-transactionStatus'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.TransactionStatus = transStat.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.TransactionStatus, GlobalVariable.TransactionStatus, true, FailureHandling.CONTINUE_ON_FAILURE)

reffNo = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-TransactionStatus(DocNo)/V-refferenceNumber'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.ReffNo = reffNo.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.ReffNo, GlobalVariable.ReffNo, true, FailureHandling.CONTINUE_ON_FAILURE)

verNo = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-TransactionStatus(DocNo)/V-versionNumber'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.VersionNo = verNo.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.VersionNo, GlobalVariable.VersionNo, true, FailureHandling.CONTINUE_ON_FAILURE)

date = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-TransactionStatus(DocNo)/V-date'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Date = date.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.Date, GlobalVariable.Date, false, FailureHandling.CONTINUE_ON_FAILURE)

menu = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-TransactionStatus(DocNo)/V-menu'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Menu = menu.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.Menu, GlobalVariable.Menu, true, FailureHandling.CONTINUE_ON_FAILURE)

product = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-TransactionStatus(DocNo)/V-product'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Product = product.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.Product, GlobalVariable.Product, false, FailureHandling.CONTINUE_ON_FAILURE)

sendCode = WebUI.getText(findTestObject('SingleUserInhouse(immediate)/Value-TransactionStatus(DocNo)/V-transferFrom'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.SenderCode = sendCode.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.SenderCode, GlobalVariable.userResultTransferFrom, false, FailureHandling.CONTINUE_ON_FAILURE)

fromAccDes = WebUI.getText(findTestObject('MultiUserInhouse(Repeat)/Value-TransactionStatusDocNo/V-fromAccountDescription'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.FromAccDes = fromAccDes.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.FromAccDes, GlobalVariable.v2, true, FailureHandling.CONTINUE_ON_FAILURE)

transTo = WebUI.getText(findTestObject('MultiUserInhouse(Repeat)/Value-TransactionStatusDocNo/V-transferTo'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.TransferTo = transTo.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.TransferTo, 'New Beneficiary', true, FailureHandling.CONTINUE_ON_FAILURE)

accName = WebUI.getText(findTestObject('MultiUserInhouse(Repeat)/Value-TransactionStatusDocNo/V-accountName'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.AccName = accName.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.AccName, GlobalVariable.userResultBeneficiaryList2, false, FailureHandling.CONTINUE_ON_FAILURE)

saveToBeneList = WebUI.getText(findTestObject('MultiUserInhouse(Repeat)/Value-TransactionStatusDocNo/V-saveToBeneficiaryList'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.saveToBeneficiaryList = saveToBeneList

WebUI.verifyMatch(GlobalVariable.saveToBeneficiaryList, GlobalVariable.saveToBeneficiaryList, true, FailureHandling.CONTINUE_ON_FAILURE)

alName = WebUI.getText(findTestObject('MultiUserInhouse(Repeat)/Value-TransactionStatusDocNo/V-aliasName'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.aliasName = alName.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.aliasName, GlobalVariable.aliasName, true, FailureHandling.CONTINUE_ON_FAILURE)

toAccDes = WebUI.getText(findTestObject('MultiUserInhouse(Repeat)/Value-TransactionStatusDocNo/V-toAccountDescription'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.ToAccDes = toAccDes.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.ToAccDes, GlobalVariable.v5, true, FailureHandling.CONTINUE_ON_FAILURE)

email = WebUI.getText(findTestObject('MultiUserInhouse(Repeat)/Value-TransactionStatusDocNo/V-Email'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.ValueBankTypeEmail = email.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.ValueBankTypeEmail, GlobalVariable.v7, true, FailureHandling.CONTINUE_ON_FAILURE)

sms = WebUI.getText(findTestObject('MultiUserInhouse(Repeat)/Value-TransactionStatusDocNo/V-sms'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.ValueBankTypeSms = sms.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.ValueBankTypeSms, GlobalVariable.v8, true, FailureHandling.CONTINUE_ON_FAILURE)

beneRefNo = WebUI.getText(findTestObject('MultiUserInhouse(Repeat)/Value-TransactionStatusDocNo/V-beneficiaryReferenceNumber'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.BenefRefNo = beneRefNo.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.BenefRefNo, GlobalVariable.v9, true, FailureHandling.CONTINUE_ON_FAILURE)

amount = WebUI.getText(findTestObject('MultiUserInhouse(Repeat)/Value-TransactionStatusDocNo/V-amount'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Amount = amount.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.Amount, GlobalVariable.Amount, true, FailureHandling.CONTINUE_ON_FAILURE)

excRate = WebUI.getText(findTestObject('MultiUserInhouse(Repeat)/Value-TransactionStatusDocNo/V-exchangeRate'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.ExchangeRate = excRate.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.ExchangeRate, GlobalVariable.v11, true, FailureHandling.CONTINUE_ON_FAILURE)

totDebet = WebUI.getText(findTestObject('MultiUserInhouse(Repeat)/Value-TransactionStatusDocNo/V-totalDebetAmount'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.TotalDebet = totDebet.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.TotalDebet, GlobalVariable.TotalDebet, true, FailureHandling.CONTINUE_ON_FAILURE)

charIns = WebUI.getText(findTestObject('MultiUserInhouse(Repeat)/Value-TransactionStatusDocNo/V-chargeInstruction'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.ChargeInstruction = charIns.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.ChargeInstruction, GlobalVariable.v15, true, FailureHandling.CONTINUE_ON_FAILURE)

debChar = WebUI.getText(findTestObject('MultiUserInhouse(Repeat)/Value-TransactionStatusDocNo/V-debitCharge'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.DebitCharge = debChar.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.DebitCharge, GlobalVariable.DebitCharge, true, FailureHandling.CONTINUE_ON_FAILURE)

beneCat = WebUI.getText(findTestObject('MultiUserInhouse(Repeat)/Value-TransactionStatusDocNo/V-beneficiaryCategory'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.BenefCate = beneCat.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.BenefCate, GlobalVariable.v17, true, FailureHandling.CONTINUE_ON_FAILURE)

tranRela = WebUI.getText(findTestObject('MultiUserInhouse(Repeat)/Value-TransactionStatusDocNo/V-transactorRelationship'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.TransRela = tranRela.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.TransRela, GlobalVariable.v18, true, FailureHandling.CONTINUE_ON_FAILURE)

idenStat = WebUI.getText(findTestObject('MultiUserInhouse(Repeat)/Value-TransactionStatusDocNo/V-identicalStatus'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.IdenticalStatus = idenStat.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.IdenticalStatus, GlobalVariable.v19, true, FailureHandling.CONTINUE_ON_FAILURE)

purpOfTrans = WebUI.getText(findTestObject('MultiUserInhouse(Repeat)/Value-TransactionStatusDocNo/V-purposeOfTransaction'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.PurpOfTrans = purpOfTrans.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.PurpOfTrans, GlobalVariable.v20, true, FailureHandling.CONTINUE_ON_FAILURE)

purpCode = WebUI.getText(findTestObject('MultiUserInhouse(Repeat)/Value-TransactionStatusDocNo/V-LHBUPurposeCode'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.PurpCode = purpCode.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.PurpCode, GlobalVariable.v21, true, FailureHandling.CONTINUE_ON_FAILURE)

docType = WebUI.getText(findTestObject('MultiUserInhouse(Repeat)/Value-TransactionStatusDocNo/V-LHBUDocumentType'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.DocType = docType.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.DocType, GlobalVariable.v22, true, FailureHandling.CONTINUE_ON_FAILURE)

docTypeDes = WebUI.getText(findTestObject('MultiUserInhouse(Repeat)/Value-TransactionStatusDocNo/V-LHBUDocumentTypeDescription'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.DocTypeDes = docTypeDes.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.DocTypeDes, GlobalVariable.v23, true, FailureHandling.CONTINUE_ON_FAILURE)

insMode = WebUI.getText(findTestObject('MultiUserInhouse(Repeat)/Value-TransactionStatusDocNo/V-instructionMode'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.InstructionMode = insMode.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.InstructionMode, 'Repeat', true, FailureHandling.CONTINUE_ON_FAILURE)

on = WebUI.getText(findTestObject('MultiUserInhouse(Repeat)/Value-TransactionStatusDocNo/V-on'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.On = on.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.On, GlobalVariable.On, true, FailureHandling.CONTINUE_ON_FAILURE)

every = WebUI.getText(findTestObject('MultiUserInhouse(Repeat)/Value-TransactionStatusDocNo/V-every'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Every = every.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.Every, GlobalVariable.Every, true, FailureHandling.CONTINUE_ON_FAILURE)

at = WebUI.getText(findTestObject('MultiUserInhouse(Repeat)/Value-TransactionStatusDocNo/V-at'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.At = at.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.At, GlobalVariable.At, true, FailureHandling.CONTINUE_ON_FAILURE)

start = WebUI.getText(findTestObject('MultiUserInhouse(Repeat)/Value-TransactionStatusDocNo/V-start'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Start = start.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.Start, GlobalVariable.Start, true, FailureHandling.CONTINUE_ON_FAILURE)

end = WebUI.getText(findTestObject('MultiUserInhouse(Repeat)/Value-TransactionStatusDocNo/V-end'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.End = end.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.End, GlobalVariable.End, true, FailureHandling.CONTINUE_ON_FAILURE)

nonWorkDayInst = WebUI.getText(findTestObject('MultiUserInhouse(Repeat)/Value-TransactionStatusDocNo/V-nonWorkingDayInstruction'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.NonWorkingDayInstruction = nonWorkDayInst.replace(': ', '')

WebUI.verifyMatch(GlobalVariable.NonWorkingDayInstruction, GlobalVariable.NonWorkingDayInstruction, true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

CustomKeywords.'get.Screencapture.getEntirePage'('')

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.scrollToElement(findTestObject('SingleUserInhouse(immediate)/Value-TransactionStatus(ReffNo)/V-totalCharge'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

CustomKeywords.'get.Screencapture.getEntirePage'('')

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.scrollToElement(findTestObject('SingleUserInhouse(immediate)/Value-TransactionStatus(ReffNo)/V-LHBUDocumentTypeDescription'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

CustomKeywords.'get.Screencapture.getEntirePage'('')

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Domestic(FutureDate)/B-Logout'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(15, FailureHandling.CONTINUE_ON_FAILURE)

