import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Bulk Upload/Bucket Confirm/Label - Bucket Confirm'), 0, FailureHandling.CONTINUE_ON_FAILURE)

LabelBucketDetail = WebUI.getText(findTestObject('Bulk Upload/Bucket Confirm/Label - Bucket Confirm'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('LabelBucketDetail', LabelBucketDetail)

WebUI.verifyMatch(LabelBucketDetail, GlobalVariable.UniversalVariable.get('LabelBucketDetail'), true, FailureHandling.CONTINUE_ON_FAILURE)

FileType = WebUI.getText(findTestObject('Bulk Upload/BucketDetail/Label - File Type'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('FileType', FileType)

WebUI.verifyMatch(FileType, GlobalVariable.UniversalVariable.get('FileType'), true, FailureHandling.CONTINUE_ON_FAILURE)

FileTemplate = WebUI.getText(findTestObject('Bulk Upload/BucketDetail/Label - File Template'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('FileTemplate', FileTemplate)

WebUI.verifyMatch(FileTemplate, GlobalVariable.UniversalVariable.get('FileTemplate'), true, FailureHandling.CONTINUE_ON_FAILURE)

FileUpload = WebUI.getText(findTestObject('Bulk Upload/BucketDetail/Label - File Upload'), FailureHandling.CONTINUE_ON_FAILURE)

String[] flag = FileUpload.split('RTGS.')

String Extention = flag[1]

GlobalVariable.UniversalVariable.put('Extention', Extention)

GlobalVariable.UniversalVariable.put('FileUpload', FileUpload)

WebUI.verifyMatch(FileUpload, GlobalVariable.UniversalVariable.get('FileUpload'), true, FailureHandling.CONTINUE_ON_FAILURE)

FileDescription = WebUI.getText(findTestObject('Bulk Upload/BucketDetail/Label - File Description'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('FileDescription', FileDescription)

WebUI.verifyMatch(FileDescription, GlobalVariable.UniversalVariable.get('FileDescription'), true, FailureHandling.CONTINUE_ON_FAILURE)

TransactionType = WebUI.getText(findTestObject('Bulk Upload/BucketDetail/Label - Transaction Type'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('TransactionType', TransactionType)

WebUI.verifyMatch(TransactionType, GlobalVariable.UniversalVariable.get('TransactionType'), true, FailureHandling.CONTINUE_ON_FAILURE)

TotalRecordInFile = WebUI.getText(findTestObject('Bulk Upload/BucketDetail/Label - Total Record In File'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('TotalRecordInFile', TotalRecordInFile)

WebUI.verifyMatch(TotalRecordInFile, GlobalVariable.UniversalVariable.get('TotalRecordInFile'), true, FailureHandling.CONTINUE_ON_FAILURE)

TotalTransactionRecord = WebUI.getText(findTestObject('Bulk Upload/BucketDetail/Label - Total Transaction Record'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('TotalTransactionRecord', TotalTransactionRecord)

WebUI.verifyMatch(TotalTransactionRecord, GlobalVariable.UniversalVariable.get('TotalTransactionRecord'), true, FailureHandling.CONTINUE_ON_FAILURE)

TotalUploadAmountInIDR = WebUI.getText(findTestObject('Bulk Upload/BucketDetail/Label - Total Upload Amount in USD'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('TotalUploadAmountInIDR', TotalUploadAmountInIDR)

WebUI.verifyMatch(TotalUploadAmountInIDR, GlobalVariable.UniversalVariable.get('TotalUploadAmountInIDR'), true, FailureHandling.CONTINUE_ON_FAILURE)

DebitAccount = WebUI.getText(findTestObject('Bulk Upload/Bucket Confirm/Label - Debit Account'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('DebitAccount', DebitAccount)

WebUI.verifyMatch(DebitAccount, GlobalVariable.UniversalVariable.get('DebitAccount'), false, FailureHandling.CONTINUE_ON_FAILURE)

Product = WebUI.getText(findTestObject('Bulk Upload/Bucket Confirm/Label - Product'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('Product', Product)

WebUI.verifyMatch(Product, GlobalVariable.UniversalVariable.get('Product'), false, FailureHandling.CONTINUE_ON_FAILURE)

TotalTransactionRecord2 = WebUI.getText(findTestObject('Bulk Upload/Bucket Confirm/Label - Total Transaction Record - 2'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('TotalTransactionRecord2', TotalTransactionRecord2)

WebUI.verifyMatch(TotalTransactionRecord2, GlobalVariable.UniversalVariable.get('TotalTransactionRecord2'), true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Bulk Upload/Bucket Confirm/BTN - Total Transaction Record - See Detail Record'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

PopUpListRecord = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - List Record'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('PopUpListRecord', PopUpListRecord)

WebUI.verifyMatch(PopUpListRecord, GlobalVariable.UniversalVariable.get('PopUpListRecord'), true, FailureHandling.CONTINUE_ON_FAILURE)

PopUpProduct = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Product'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('PopUpProduct', PopUpProduct)

WebUI.verifyMatch(PopUpProduct, GlobalVariable.UniversalVariable.get('PopUpProduct'), false, FailureHandling.CONTINUE_ON_FAILURE)

PopUpDebitAccount = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Debit Account'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('PopUpDebitAccount', PopUpDebitAccount)

WebUI.verifyMatch(PopUpDebitAccount, GlobalVariable.UniversalVariable.get('PopUpDebitAccount'), false, FailureHandling.CONTINUE_ON_FAILURE)

PopUpTotalTransactionRecord = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Total Transaction Record'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('PopUpTotalTransactionRecord', PopUpTotalTransactionRecord)

WebUI.verifyMatch(PopUpTotalTransactionRecord, GlobalVariable.UniversalVariable.get('PopUpTotalTransactionRecord'), true, 
    FailureHandling.CONTINUE_ON_FAILURE)

SKNFeeDetails = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - RTGS Fee(Details)'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('SKNFeeDetails', SKNFeeDetails)

WebUI.verifyMatch(SKNFeeDetails, GlobalVariable.UniversalVariable.get('SKNFeeDetails'), false, FailureHandling.CONTINUE_ON_FAILURE)

TransferFeeDetails = WebUI.getText(findTestObject('Bulk Upload/List Record/V-transferFee(details)'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('TransferFeeDetails', TransferFeeDetails)

WebUI.verifyMatch(TransferFeeDetails, GlobalVariable.UniversalVariable.get('TransferFeeDetails'), false, FailureHandling.CONTINUE_ON_FAILURE)

TotalDebitAmount = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Total Debit Amount'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('TotalDebitAmount', TotalDebitAmount)

WebUI.verifyMatch(TotalDebitAmount, GlobalVariable.UniversalVariable.get('TotalDebitAmount'), false, FailureHandling.CONTINUE_ON_FAILURE)

PopUpDeatilTableInfo = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Detail Table Info'), FailureHandling.CONTINUE_ON_FAILURE)

String[] PopUpDeatilTableInfo1 = PopUpDeatilTableInfo.split(' ')

PopUpDeatilTableInfo2 = (PopUpDeatilTableInfo1[5])

GlobalVariable.UniversalVariable.put('PopUpDeatilTableInfo2', PopUpDeatilTableInfo2)

WebUI.verifyMatch(PopUpDeatilTableInfo2, GlobalVariable.UniversalVariable.get('PopUpDeatilTableInfo2'), true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.getAttribute(findTestObject('Bulk Upload/List Record/BTN - Next Page - Kosong'), 'class', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.callTestCase(findTestCase('Payroll/Get Text - Table'), [('btnnext') : findTestObject('Bulk Upload/List Record/BTN - Next Page')
        , ('btnnextattribute') : findTestObject('Bulk Upload/List Record/BTN - Next Page - Kosong'), ('ColumnCreditAccountNumber') : 2
        , ('ColumnAmount') : 1], FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Bulk Upload/List Record/BTN - Close'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Bulk Upload/Bucket Confirm/BTN - Total Transaction Record - See Detail Record'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

SKNFee = WebUI.getText(findTestObject('Bulk Upload/Bucket Confirm/Label - RTGS Fee'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('SKNFee', SKNFee)

WebUI.verifyMatch(SKNFee, GlobalVariable.UniversalVariable.get('SKNFee'), false, FailureHandling.CONTINUE_ON_FAILURE)

TransferFee = WebUI.getText(findTestObject('Bulk Upload/List Record/V-transferFee'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('TransferFee', TransferFee)

WebUI.verifyMatch(TransferFee, GlobalVariable.UniversalVariable.get('TransferFee'), false, FailureHandling.CONTINUE_ON_FAILURE)

TotalCharges = WebUI.getText(findTestObject('Bulk Upload/Bucket Confirm/Label - Total Charges'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('TotalCharges', TotalCharges)

WebUI.verifyMatch(TotalCharges, GlobalVariable.UniversalVariable.get('TotalCharges'), false, FailureHandling.CONTINUE_ON_FAILURE)

TotalDebitAmount = WebUI.getText(findTestObject('Bulk Upload/Bucket Confirm/Label - Total Debit Amount'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('TotalDebitAmount', TotalDebitAmount)

WebUI.verifyMatch(TotalDebitAmount, GlobalVariable.UniversalVariable.get('TotalDebitAmount'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Bulk Upload/Bucket Confirm/BTN - Submit'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('B-Ok'), FailureHandling.CONTINUE_ON_FAILURE)

