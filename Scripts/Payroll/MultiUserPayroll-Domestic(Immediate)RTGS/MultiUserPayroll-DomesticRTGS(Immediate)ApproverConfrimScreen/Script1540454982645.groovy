import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

Menu = WebUI.getText(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-ApproverConfrimScreen/V-menu '), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(Menu, 'Payroll', true, FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.menu123 = Menu

Product = WebUI.getText(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-ApproverConfrimScreen/V-product'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(Product, 'Payroll RTGS', false, FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.product123 = Product

TransactionReferenceNo = WebUI.getText(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-ApproverConfrimScreen/V-transactionReffnum'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TransactionReferenceNo, GlobalVariable.ReffNo, true, FailureHandling.CONTINUE_ON_FAILURE)

DocumentNo = WebUI.getText(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-ApproverConfrimScreen/V-transactionDocumentNum'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(DocumentNo, GlobalVariable.ReffNo, true, FailureHandling.CONTINUE_ON_FAILURE)

FileType = WebUI.getText(findTestObject('Bulk Upload/BucketDetail/Label - File Type'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(FileType, GlobalVariable.UniversalVariable.get('FileType'), true, FailureHandling.CONTINUE_ON_FAILURE)

FileUpload = WebUI.getText(findTestObject('Bulk Upload/BucketDetail/Label - File Upload'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(FileUpload, GlobalVariable.UniversalVariable.get('FileUpload'), true, FailureHandling.CONTINUE_ON_FAILURE)

FileDescription = WebUI.getText(findTestObject('Bulk Upload/BucketDetail/Label - File Description'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(FileDescription, GlobalVariable.UniversalVariable.get('FileDescription'), true, FailureHandling.CONTINUE_ON_FAILURE)

DebitAccount1 = WebUI.getText(findTestObject('Bulk Upload/Bucket Confirm/Label - Debit Account'), FailureHandling.CONTINUE_ON_FAILURE)

DebitAccount2 = DebitAccount1.replace('( ', '(  ')

DebitAccount = DebitAccount2.replace(' )', '  )')

WebUI.verifyMatch(DebitAccount, GlobalVariable.UniversalVariable.get('DebitAccount'), false, FailureHandling.CONTINUE_ON_FAILURE)

Product = WebUI.getText(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-ApproverConfrimScreen/V-product2'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(Product, GlobalVariable.UniversalVariable.get('Product'), false, FailureHandling.CONTINUE_ON_FAILURE)

TotalTransactionRecord = WebUI.getText(findTestObject('Bulk Upload/BucketDetail/Label - Total Transaction Record'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TotalTransactionRecord, GlobalVariable.UniversalVariable.get('TotalTransactionRecord'), true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Bulk Upload/Bucket Confirm/BTN - Total Transaction Record - See Detail Record'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

PopUpListRecord = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - List Record'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(PopUpListRecord, GlobalVariable.UniversalVariable.get('PopUpListRecord'), true, FailureHandling.CONTINUE_ON_FAILURE)

PopUpProduct = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Product'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(PopUpProduct, GlobalVariable.UniversalVariable.get('PopUpProduct'), false, FailureHandling.CONTINUE_ON_FAILURE)

PopUpDebitAccount = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Debit Account'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(PopUpDebitAccount, GlobalVariable.UniversalVariable.get('PopUpDebitAccount'), false, FailureHandling.CONTINUE_ON_FAILURE)

PopUpTotalTransactionRecord = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Total Transaction Record'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(PopUpTotalTransactionRecord, GlobalVariable.UniversalVariable.get('PopUpTotalTransactionRecord'), true, 
    FailureHandling.CONTINUE_ON_FAILURE)

TotalTransactionAmountinIDR1 = WebUI.getText(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-ApproverConfrimScreen/V-totalDebitAmount'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TotalTransactionAmountinIDR = TotalTransactionAmountinIDR1.replace(' ', '  ')

WebUI.verifyMatch(TotalTransactionAmountinIDR, GlobalVariable.UniversalVariable.get('TotalDebitAmount'), false, FailureHandling.CONTINUE_ON_FAILURE)

SKNFeeDetails = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - RTGS Fee(Details)'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(SKNFeeDetails, GlobalVariable.UniversalVariable.get('SKNFeeDetails'), false, FailureHandling.CONTINUE_ON_FAILURE)

TransferFeeDetails = WebUI.getText(findTestObject('Bulk Upload/List Record/V-transferFee(details)'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TransferFeeDetails, GlobalVariable.UniversalVariable.get('TransferFeeDetails'), false, FailureHandling.CONTINUE_ON_FAILURE)

TotalCharges1 = WebUI.getText(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-ApproverConfrimScreen/V-totalCharge'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TotalCharges = TotalCharges1.replace(' ', '  ')

WebUI.verifyMatch(TotalCharges, GlobalVariable.UniversalVariable.get('TotalCharges'), false, FailureHandling.CONTINUE_ON_FAILURE)

TotalDebitAmount1 = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Total Debit Amount'), FailureHandling.CONTINUE_ON_FAILURE)

TotalDebitAmount = TotalDebitAmount1.replace(' ', '  ')

WebUI.verifyMatch(TotalDebitAmount, GlobalVariable.UniversalVariable.get('TotalDebitAmount'), false, FailureHandling.CONTINUE_ON_FAILURE)

PopUpDeatilTableInfo = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Detail Table Info'), FailureHandling.CONTINUE_ON_FAILURE)

String[] PopUpDeatilTableInfo1 = PopUpDeatilTableInfo.split(' ')

PopUpDeatilTableInfo2 = (PopUpDeatilTableInfo1[5])

WebUI.verifyMatch(PopUpDeatilTableInfo2, GlobalVariable.UniversalVariable.get('PopUpDeatilTableInfo2'), true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.callTestCase(findTestCase('Payroll/LoopingForVerifyApproverConfrim'), [('bntnext') : findTestObject('Bulk Upload/List Record/BTN - Next Page')
        , ('btnnextattribute') : findTestObject('Bulk Upload/List Record/BTN - Next Page - Kosong'), ('CreditAccountNo') : 2
        , ('CreditAccountName') : 1], FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Bulk Upload/List Record/BTN - Close'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

TotalTransactionAmountinIDR11 = WebUI.getText(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-ApproverConfrimScreen/V-totalDebitAmount'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TotalTransactionAmountinIDR = TotalTransactionAmountinIDR11.replace(' ', '  ')

WebUI.verifyMatch(TotalTransactionAmountinIDR, GlobalVariable.UniversalVariable.get('TotalDebitAmount'), false, FailureHandling.CONTINUE_ON_FAILURE)

SKNFee1 = WebUI.getText(findTestObject('Bulk Upload/Bucket Confirm/Label - RTGS Fee'), FailureHandling.CONTINUE_ON_FAILURE)

SKNFee = SKNFee1.replace('   ', '  ')

WebUI.verifyMatch(SKNFee, GlobalVariable.UniversalVariable.get('SKNFee'), false, FailureHandling.CONTINUE_ON_FAILURE)

TransferFee1 = WebUI.getText(findTestObject('Bulk Upload/List Record/V-transferFee'), FailureHandling.CONTINUE_ON_FAILURE)

TransferFee = TransferFee1.replace('   ', '  ')

WebUI.verifyMatch(TransferFee, GlobalVariable.UniversalVariable.get('TransferFee'), false, FailureHandling.CONTINUE_ON_FAILURE)

TotalCharges1 = WebUI.getText(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-ApproverConfrimScreen/V-totalCharge'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TotalCharges = TotalCharges1.replace(' ', '  ')

WebUI.verifyMatch(TotalCharges, GlobalVariable.UniversalVariable.get('TotalCharges'), false, FailureHandling.CONTINUE_ON_FAILURE)

TotalDebitAmount11 = WebUI.getText(findTestObject('Bulk Upload/Bucket Confirm/Label - Total Debit Amount'), FailureHandling.CONTINUE_ON_FAILURE)

TotalDebitAmount = TotalDebitAmount11.replace('   ', '  ')

WebUI.verifyMatch(TotalDebitAmount, GlobalVariable.UniversalVariable.get('TotalDebitAmount'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('B-Approve'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-ApproverConfrimScreen/TF-response'), '123456', 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('B-Approve'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(4, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-ApproverConfrimScreen/V-okAfterApprove'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(10, FailureHandling.CONTINUE_ON_FAILURE)

