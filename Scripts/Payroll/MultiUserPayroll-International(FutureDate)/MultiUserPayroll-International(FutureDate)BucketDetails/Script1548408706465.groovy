import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Bulk Upload/BucketDetail/Label - BucketDetail'), 0, FailureHandling.CONTINUE_ON_FAILURE)

LabelBucketDetail = WebUI.getText(findTestObject('Bulk Upload/BucketDetail/Label - BucketDetail'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('LabelBucketDetail',LabelBucketDetail)

WebUI.verifyMatch(LabelBucketDetail, GlobalVariable.UniversalVariable.get('LabelBucketDetail'), true, FailureHandling.CONTINUE_ON_FAILURE)
 
FileUploadStatus = WebUI.getText(findTestObject('Bulk Upload/BucketDetail/Label - File Upload Status - Comp'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('FileUploadStatus',FileUploadStatus)

WebUI.verifyMatch(FileUploadStatus, GlobalVariable.UniversalVariable.get('FileUploadStatus'), true, FailureHandling.CONTINUE_ON_FAILURE)

FileType = WebUI.getText(findTestObject('Bulk Upload/BucketDetail/Label - File Type'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('FileType',FileType)

WebUI.verifyMatch(FileType, GlobalVariable.UniversalVariable.get('FileType'), true, FailureHandling.CONTINUE_ON_FAILURE)

FileTemplate = WebUI.getText(findTestObject('Bulk Upload/BucketDetail/Label - File Template'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('FileTemplate',FileTemplate)

WebUI.verifyMatch(FileTemplate, GlobalVariable.UniversalVariable.get('FileTemplate'), true, FailureHandling.CONTINUE_ON_FAILURE)

FileUpload = WebUI.getText(findTestObject('Bulk Upload/BucketDetail/Label - File Upload'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('FileUpload',FileUpload)

WebUI.verifyMatch(FileUpload, GlobalVariable.UniversalVariable.get('FileUpload'), true, FailureHandling.CONTINUE_ON_FAILURE)

FileDescription = WebUI.getText(findTestObject('Bulk Upload/BucketDetail/Label - File Description'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('FileDescription',FileDescription)

WebUI.verifyMatch(FileDescription, GlobalVariable.UniversalVariable.get('FileDescription'), true, FailureHandling.CONTINUE_ON_FAILURE)

TransactionType = WebUI.getText(findTestObject('Bulk Upload/BucketDetail/Label - Transaction Type'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('TransactionType',TransactionType)

WebUI.verifyMatch(TransactionType, GlobalVariable.UniversalVariable.get('TransactionType'), true, FailureHandling.CONTINUE_ON_FAILURE)

TotalRecordInFile = WebUI.getText(findTestObject('Bulk Upload/BucketDetail/Label - Total Record In File'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('TotalRecordInFile',TotalRecordInFile)

WebUI.verifyMatch(TotalRecordInFile, GlobalVariable.UniversalVariable.get('TotalRecordInFile'), true, FailureHandling.CONTINUE_ON_FAILURE)

TotalTransactionRecord = WebUI.getText(findTestObject('Bulk Upload/BucketDetail/Label - Total Transaction Record'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('TotalTransactionRecord',TotalTransactionRecord)

WebUI.verifyMatch(TotalTransactionRecord, GlobalVariable.UniversalVariable.get('TotalTransactionRecord'), true, FailureHandling.CONTINUE_ON_FAILURE)

TotalSucces = WebUI.getText(findTestObject('Bulk Upload/BucketDetail/Label - Total Succes'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('TotalSucces',TotalSucces)

WebUI.verifyMatch(TotalSucces, GlobalVariable.UniversalVariable.get('TotalSucces'), true, FailureHandling.CONTINUE_ON_FAILURE)

TotalFailed = WebUI.getText(findTestObject('Bulk Upload/BucketDetail/Label - Total Failed'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('TotalFailed',TotalFailed)

WebUI.verifyMatch(TotalFailed, GlobalVariable.UniversalVariable.get('TotalFailed'), true, FailureHandling.CONTINUE_ON_FAILURE)

TotalUploadAmountInIDR = WebUI.getText(findTestObject('Bulk Upload/BucketDetail/Label - Total Upload Amount in USD'), FailureHandling.CONTINUE_ON_FAILURE)

totalAmount = TotalUploadAmountInIDR.replace('IDR   ', 'IDR ')

GlobalVariable.UniversalVariable.put('totalAmount',totalAmount)

WebUI.verifyMatch(totalAmount, GlobalVariable.UniversalVariable.get('totalAmount'), true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Bulk Upload/BucketDetail/BTN - Confirm'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

