import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW

transStat = WebUI.getText(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-TransactionStatusDocNo/V-transactionStatus'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(transStat, 'Pending Execute', true, FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.TransactionStatus = transStat

TransactionReferenceNo = WebUI.getText(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-TransactionStatus/V-transactionReferenceNo'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TransactionReferenceNo, GlobalVariable.ReffNo, true, FailureHandling.CONTINUE_ON_FAILURE)

verNum = WebUI.getText(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-TransactionStatusDocNo/V-versionNumber'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(verNum, GlobalVariable.ReffNo, true, FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.VersionNo = verNum

Datee = WebUI.getText(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-TransactionStatus/V-submitDateTime'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Date = Datee

WebUI.verifyMatch(Datee, Datee, false, FailureHandling.CONTINUE_ON_FAILURE)

Menu = WebUI.getText(findTestObject('SingleUserDometstic(Repeat)SKN/Value-TransactionStatusReffNo/V-menu'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(Menu, 'Menu', true, FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.menu123 = Menu

Product = WebUI.getText(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-TransactionStatus/V-product'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(Product, GlobalVariable.UniversalVariable.get('Product'), false, FailureHandling.CONTINUE_ON_FAILURE)

InstructionDate = WebUI.getText(findTestObject('Payroll/SingleUserDomestic(Future)SKN/V-transactionStatus/V-instructionDate'), 
    FailureHandling.CONTINUE_ON_FAILURE)

not_run: WebUI.verifyMatch(InstructionDate, GlobalVariable.UniversalVariable.get('InstructionDate'), false, FailureHandling.CONTINUE_ON_FAILURE)

FileType = WebUI.getText(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-TransactionStatus/Label - File Type'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(FileType, GlobalVariable.UniversalVariable.get('FileType'), true, FailureHandling.CONTINUE_ON_FAILURE)

FileFormat = WebUI.getText(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-TransactionStatus/V-FileFormat'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('FileFormat', FileFormat)

WebUI.verifyMatch(FileFormat, GlobalVariable.UniversalVariable.get('FileFormat'), true, FailureHandling.CONTINUE_ON_FAILURE)

FileUpload1 = WebUI.getText(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-TransactionStatus/Label - File Upload'), 
    FailureHandling.CONTINUE_ON_FAILURE)

FileUpload = FileUpload1.concat('.CSV')

WebUI.verifyMatch(FileUpload, GlobalVariable.UniversalVariable.get('FileUpload'), true, FailureHandling.CONTINUE_ON_FAILURE)

FileDescription = WebUI.getText(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-TransactionStatus/Label - File Description'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(FileDescription, GlobalVariable.UniversalVariable.get('FileDescription'), true, FailureHandling.CONTINUE_ON_FAILURE)

DebitAccount1 = WebUI.getText(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-TransactionStatus/Label - Debit Account'), 
    FailureHandling.CONTINUE_ON_FAILURE)

DebitAccount = DebitAccount1.replace('(USD)', '(  USD  )')

WebUI.verifyMatch(DebitAccount, GlobalVariable.UniversalVariable.get('DebitAccount'), false, FailureHandling.CONTINUE_ON_FAILURE)

Product = WebUI.getText(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-TransactionStatus/V-product2'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(Product, GlobalVariable.UniversalVariable.get('Product'), false, FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.product123 = Product

TotalTransactionRecord = WebUI.getText(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-TransactionStatus/Label - Total Transaction Record'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TotalTransactionRecord, GlobalVariable.UniversalVariable.get('TotalTransactionRecord'), true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-TransactionStatus/BTN - Total Transaction Record - See Detail Record'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

PopUpProduct = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Product'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(PopUpProduct, GlobalVariable.UniversalVariable.get('Product'), false, FailureHandling.CONTINUE_ON_FAILURE)

InstructionDate = WebUI.getText(findTestObject('Payroll/SingleUserDomestic(Future)SKN/V-confrimScreen/V-instructionDate(Details)'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(InstructionDate, GlobalVariable.UniversalVariable.get('InstructionDate'), false, FailureHandling.CONTINUE_ON_FAILURE)

PopUpDebitAccount1 = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Debit Account'), FailureHandling.CONTINUE_ON_FAILURE)

PopUpDebitAccount = PopUpDebitAccount1.replace('(USD)', '(  USD  )')

WebUI.verifyMatch(PopUpDebitAccount, GlobalVariable.UniversalVariable.get('DebitAccount'), false, FailureHandling.CONTINUE_ON_FAILURE)

PopUpTotalTransactionRecord = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Total Transaction Record'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(PopUpTotalTransactionRecord, GlobalVariable.UniversalVariable.get('TotalTransactionRecord'), true, FailureHandling.CONTINUE_ON_FAILURE)

TotalTransactionAmountinIDR = WebUI.getText(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-ApproverConfrimScreen/V-totalDebitAmount'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TotalTransactionAmountinIDR, GlobalVariable.UniversalVariable.get('TotalDebitAmount'), false, FailureHandling.CONTINUE_ON_FAILURE)

TotalCharges = WebUI.getText(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-ApproverConfrimScreen/V-totalCharge'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TotalCharges, GlobalVariable.UniversalVariable.get('totalCharge'), false, FailureHandling.CONTINUE_ON_FAILURE)

mimTransactionFee1 = WebUI.getText(findTestObject('Payroll/singleUserInternational(Immediate)/V-ConfrimScreen(Detail)/V-minimumTransactionFee'), 
    FailureHandling.CONTINUE_ON_FAILURE)

mimTransactionFee = mimTransactionFee1.replace('/ Record', ' / record')

WebUI.verifyMatch(mimTransactionFee, GlobalVariable.UniversalVariable.get('mimTransactionFee'), false, FailureHandling.CONTINUE_ON_FAILURE)

fullAmountCharge1 = WebUI.getText(findTestObject('Payroll/singleUserInternational(Immediate)/V-ConfrimScreen(Detail)/V-fullAmountCharge'), 
    FailureHandling.CONTINUE_ON_FAILURE)

fullAmountCharge = fullAmountCharge1.replace('/ Record', ' / record')

WebUI.verifyMatch(fullAmountCharge, GlobalVariable.UniversalVariable.get('fullAmountCharge'), false, FailureHandling.CONTINUE_ON_FAILURE)

provision1 = WebUI.getText(findTestObject('Payroll/singleUserInternational(Immediate)/V-ConfrimScreen(Detail)/V-provision'), 
    FailureHandling.CONTINUE_ON_FAILURE)

provision = provision1.replace('/ Record', ' / record')

WebUI.verifyMatch(provision, GlobalVariable.UniversalVariable.get('provision'), false, FailureHandling.CONTINUE_ON_FAILURE)

correspondentBankFee1 = WebUI.getText(findTestObject('Payroll/singleUserInternational(Immediate)/V-ConfrimScreen(Detail)/V-correspondentBankFee'), 
    FailureHandling.CONTINUE_ON_FAILURE)

correspondentBankFee = correspondentBankFee1.replace('/ Record', ' / record')

WebUI.verifyMatch(correspondentBankFee, GlobalVariable.UniversalVariable.get('correspondentBankFee'), false, FailureHandling.CONTINUE_ON_FAILURE)

cableFee1 = WebUI.getText(findTestObject('Payroll/singleUserInternational(Immediate)/V-ConfrimScreen(Detail)/V-cableFee'), 
    FailureHandling.CONTINUE_ON_FAILURE)

cableFee = cableFee1.replace('/ Record', ' / record')

WebUI.verifyMatch(cableFee, GlobalVariable.UniversalVariable.get('cableFee'), false, FailureHandling.CONTINUE_ON_FAILURE)

TotalDebitAmount = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Total Debit Amount'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TotalDebitAmount, GlobalVariable.UniversalVariable.get('TotalDebitAmount'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Bulk Upload/List Record/BTN - Close'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-TransactionStatus/BTN - Total Transaction Record - See Detail Record'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

TotalTransactionAmountinIDR = WebUI.getText(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-ApproverConfrimScreen/V-totalDebitAmount'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TotalTransactionAmountinIDR, GlobalVariable.UniversalVariable.get('TotalDebitAmount'), false, FailureHandling.CONTINUE_ON_FAILURE)

mimTransactionFee = WebUI.getText(findTestObject('Payroll/singleUserInternational(Immediate)/V-TransactionStatus/V-minimumTransactionFee'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(mimTransactionFee, GlobalVariable.UniversalVariable.get('mimTransactionFee'), false, FailureHandling.CONTINUE_ON_FAILURE)

fullAmountCharge = WebUI.getText(findTestObject('Payroll/singleUserInternational(Immediate)/V-TransactionStatus/V-fullAmountCharge'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(fullAmountCharge, GlobalVariable.UniversalVariable.get('fullAmountCharge'), false, FailureHandling.CONTINUE_ON_FAILURE)

provision = WebUI.getText(findTestObject('Payroll/singleUserInternational(Immediate)/V-TransactionStatus/V-provision'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(provision, GlobalVariable.UniversalVariable.get('provision'), false, FailureHandling.CONTINUE_ON_FAILURE)

correspondentBankFee = WebUI.getText(findTestObject('Payroll/singleUserInternational(Immediate)/V-TransactionStatus/V-correspondentBankFee'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(correspondentBankFee, GlobalVariable.UniversalVariable.get('correspondentBankFee'), false, FailureHandling.CONTINUE_ON_FAILURE)

cableFee = WebUI.getText(findTestObject('Payroll/singleUserInternational(Immediate)/V-TransactionStatus/V-cableFee'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(cableFee, GlobalVariable.UniversalVariable.get('cableFee'), false, FailureHandling.CONTINUE_ON_FAILURE)

TotalCharges = WebUI.getText(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-TransactionStatus/V-totalCharge'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TotalCharges, GlobalVariable.UniversalVariable.get('totalCharge'), false, FailureHandling.CONTINUE_ON_FAILURE)

TotalDebitAmount = WebUI.getText(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-TransactionStatus/V-totalDebitAmount'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TotalDebitAmount, GlobalVariable.UniversalVariable.get('TotalDebitAmount'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

