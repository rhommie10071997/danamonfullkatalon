import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

SuccessMessage = WebUI.getText(findTestObject('Bulk Upload/Create Result/Label - Success Message'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(SuccessMessage, 'This transaction has been successfully released', true, FailureHandling.CONTINUE_ON_FAILURE)

ReferenceNo = WebUI.getText(findTestObject('Bulk Upload/Create Result/Label - Reference No'), FailureHandling.CONTINUE_ON_FAILURE)

String[] ReferenceNo1 = ReferenceNo.split(' ')

RefNo2 = (ReferenceNo1[3])

GlobalVariable.ReffNo = RefNo2

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

FileType = WebUI.getText(findTestObject('Bulk Upload/BucketDetail/Label - File Type'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(FileType, GlobalVariable.UniversalVariable.get('FileType'), true, FailureHandling.CONTINUE_ON_FAILURE)

FileUpload = WebUI.getText(findTestObject('Bulk Upload/BucketDetail/Label - File Upload'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(FileUpload, GlobalVariable.UniversalVariable.get('FileUpload'), true, FailureHandling.CONTINUE_ON_FAILURE)

FileDescription = WebUI.getText(findTestObject('Bulk Upload/BucketDetail/Label - File Description'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(FileDescription, GlobalVariable.UniversalVariable.get('FileDescription'), true, FailureHandling.CONTINUE_ON_FAILURE)

TransactionType = WebUI.getText(findTestObject('Bulk Upload/BucketDetail/Label - Transaction Type'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TransactionType, GlobalVariable.UniversalVariable.get('TransactionType'), true, FailureHandling.CONTINUE_ON_FAILURE)

DebitAccount1 = WebUI.getText(findTestObject('Bulk Upload/Bucket Confirm/Label - Debit Account'), FailureHandling.CONTINUE_ON_FAILURE)

DebitAccount = DebitAccount1.replace('  IDR  ', ' IDR ')

WebUI.verifyMatch(DebitAccount, GlobalVariable.UniversalVariable.get('DebitAccount'), false, FailureHandling.CONTINUE_ON_FAILURE)

Product = WebUI.getText(findTestObject('Bulk Upload/Bucket Confirm/Label - Product'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(Product, GlobalVariable.UniversalVariable.get('Product'), false, FailureHandling.CONTINUE_ON_FAILURE)

TotalTransactionRecord2 = WebUI.getText(findTestObject('Bulk Upload/Bucket Confirm/Label - Total Transaction Record - 2'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TotalTransactionRecord2, GlobalVariable.UniversalVariable.get('TotalTransactionRecord2'), true, FailureHandling.CONTINUE_ON_FAILURE)

SKNFee1 = WebUI.getText(findTestObject('Payroll/SingleUserDomestic(Immediate)SKN/Value-confrimScreen/V-SKNFee'), FailureHandling.CONTINUE_ON_FAILURE)

SKNFee = SKNFee1.replace('record', 'Record')

WebUI.verifyMatch(SKNFee, GlobalVariable.UniversalVariable.get('SKNFee'), false, FailureHandling.CONTINUE_ON_FAILURE)

TransferFee1 = WebUI.getText(findTestObject('MenuPayroll/MultiUserDomesticSKN(Immediate)/Value-CreateResultScreen/V-transferFee'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TransferFee = TransferFee1.replace('record', 'Record')

WebUI.verifyMatch(TransferFee, GlobalVariable.UniversalVariable.get('TransferFee'), false, FailureHandling.CONTINUE_ON_FAILURE)

totalCharge = WebUI.getText(findTestObject('Payroll/SingleUserDomestic(Immediate)SKN/Value-confrimScreen/V-totalCharge'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(totalCharge, GlobalVariable.UniversalVariable.get('totalCharge'), false, FailureHandling.CONTINUE_ON_FAILURE)

TotalDebitAmount = WebUI.getText(findTestObject('Bulk Upload/Bucket Confirm/Label - Total Debit Amount'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TotalDebitAmount, GlobalVariable.UniversalVariable.get('TotalDebitAmount'), false, FailureHandling.CONTINUE_ON_FAILURE)

chargeToRemitter = WebUI.getText(findTestObject('Payroll/SingleUserDomestic(Immediate)SKN/Value-confrimScreen/V-chargeToRemitter'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(chargeToRemitter, GlobalVariable.UniversalVariable.get('chargeToRemitter'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Bulk Upload/Bucket Confirm/BTN - Total Transaction Record - See Detail Record'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

PopUpListRecord = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - List Record'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(PopUpListRecord, GlobalVariable.UniversalVariable.get('PopUpListRecord'), true, FailureHandling.CONTINUE_ON_FAILURE)

PopUpProduct = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Product'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(PopUpProduct, GlobalVariable.UniversalVariable.get('Product'), false, FailureHandling.CONTINUE_ON_FAILURE)

PopUpDebitAccount = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Debit Account'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(PopUpDebitAccount, GlobalVariable.UniversalVariable.get('DebitAccount'), false, FailureHandling.CONTINUE_ON_FAILURE)

PopUpTotalTransactionRecord = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Total Transaction Record'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(PopUpTotalTransactionRecord, GlobalVariable.UniversalVariable.get('TotalTransactionRecord2'), true, FailureHandling.CONTINUE_ON_FAILURE)

PopUpSKNFee1 = WebUI.getText(findTestObject('Payroll/SingleUserDomestic(Immediate)SKN/Value-confrimScreen(Details)/V-SKNFee'), 
    FailureHandling.CONTINUE_ON_FAILURE)

PopUpSKNFee = PopUpSKNFee1.replace('/', ' /')

WebUI.verifyMatch(PopUpSKNFee, GlobalVariable.UniversalVariable.get('SKNFee'), false, FailureHandling.CONTINUE_ON_FAILURE)

PopUpTransferFee1 = WebUI.getText(findTestObject('MenuPayroll/MultiUserDomesticSKN(Immediate)/Value-CreateResultScreen/V-transferFeeForLooping'), 
    FailureHandling.CONTINUE_ON_FAILURE)

PopUpTransferFee = PopUpTransferFee1.replace('/', ' /')

WebUI.verifyMatch(PopUpTransferFee, GlobalVariable.UniversalVariable.get('TransferFee'), false, FailureHandling.CONTINUE_ON_FAILURE)

PopUptotalCharge = WebUI.getText(findTestObject('Payroll/SingleUserDomestic(Immediate)SKN/Value-confrimScreen(Details)/V-totalCharge'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(PopUptotalCharge, GlobalVariable.UniversalVariable.get('totalCharge'), false, FailureHandling.CONTINUE_ON_FAILURE)

PopUpTotalDebitAmount = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Total Debit Amount'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(PopUpTotalDebitAmount, GlobalVariable.UniversalVariable.get('TotalDebitAmount'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.callTestCase(findTestCase('Payroll/Get Text - Table'), [('btnnext') : findTestObject('Bulk Upload/List Record/BTN - Next Page')
        , ('btnnextattribute') : findTestObject('Bulk Upload/List Record/BTN - Next Page - Kosong'), ('ColumnCreditAccountNumber') : 2
        , ('ColumnAmount') : 5], FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Bulk Upload/List Record/BTN - Close'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/PendingTask/B-Done'))

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

