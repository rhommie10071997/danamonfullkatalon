import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW

TransactionReferenceNo = WebUI.getText(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-TransactionStatus/V-transactionReferenceNo'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TransactionReferenceNo, GlobalVariable.ReffNo, true, FailureHandling.CONTINUE_ON_FAILURE)

Datee = WebUI.getText(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-TransactionStatus/V-submitDateTime'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Date = Datee

WebUI.verifyMatch(Datee, GlobalVariable.Date, true, FailureHandling.CONTINUE_ON_FAILURE)

Menu = WebUI.getText(findTestObject('SingleUserDometstic(Repeat)SKN/Value-TransactionStatusReffNo/V-menu'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(Menu, 'Menu', true, FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.menu123 = Menu

Product = WebUI.getText(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-TransactionStatus/V-product'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(Product, 'Payroll Inhouse', false, FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.product123 = Product

FileType = WebUI.getText(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-TransactionStatus/Label - File Type'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(FileType, 'Non Encrypted', true, FailureHandling.CONTINUE_ON_FAILURE)

FileUpload = WebUI.getText(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-TransactionStatus/V-FileFormat'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(FileUpload, 'CSV', true, FailureHandling.CONTINUE_ON_FAILURE)

FileDescription = WebUI.getText(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-TransactionStatus/Label - File Description'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(FileDescription, 'mamayoukeo', true, FailureHandling.CONTINUE_ON_FAILURE)

DebitAccount = WebUI.getText(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-TransactionStatus/Label - Debit Account'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(DebitAccount, '000001099811 - DORAEM (IDR)', false, FailureHandling.CONTINUE_ON_FAILURE)

Product = WebUI.getText(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-TransactionStatus/V-product2'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(Product, 'Payroll Inhouse', false, FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.product123 = Product

TotalTransactionRecord = WebUI.getText(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-TransactionStatus/Label - Total Transaction Record'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TotalTransactionRecord, '2', true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-TransactionStatus/BTN - Total Transaction Record - See Detail Record'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

PopUpListRecord = WebUI.getText(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-TransactionStatus/Label - Search'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(PopUpListRecord, 'Search', true, FailureHandling.CONTINUE_ON_FAILURE)

PopUpProduct = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Product'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(PopUpProduct, 'Payroll Inhouse', false, FailureHandling.CONTINUE_ON_FAILURE)

PopUpDebitAccount = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Debit Account'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(PopUpDebitAccount, '000001099811 - DORAEM (IDR)', false, FailureHandling.CONTINUE_ON_FAILURE)

PopUpTotalTransactionRecord = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Total Transaction Record'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(PopUpTotalTransactionRecord, '2', true, FailureHandling.CONTINUE_ON_FAILURE)

TotalTransactionAmountinIDR = WebUI.getText(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-ApproverConfrimScreen/V-totalDebitAmount'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TotalTransactionAmountinIDR, 'IDR 310,000.00', false, FailureHandling.CONTINUE_ON_FAILURE)

TransferFee = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Transfer Fee'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TransferFee, 'IDR 0.00 / record', false, FailureHandling.CONTINUE_ON_FAILURE)

TotalCharges = WebUI.getText(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-ApproverConfrimScreen/V-totalCharge'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TotalCharges, 'IDR 0.00', false, FailureHandling.CONTINUE_ON_FAILURE)

TotalDebitAmount = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Total Debit Amount'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TotalDebitAmount, 'IDR 310,000.00', false, FailureHandling.CONTINUE_ON_FAILURE)

PopUpDeatilTableInfo = WebUI.getText(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-TransactionStatus/Label - Detail Table Info'), 
    FailureHandling.CONTINUE_ON_FAILURE)

String[] PopUpDeatilTableInfo1 = PopUpDeatilTableInfo.split(' ')

PopUpDeatilTableInfo2 = (PopUpDeatilTableInfo1[5])

WebUI.verifyMatch(PopUpDeatilTableInfo2, '2', true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.callTestCase(findTestCase('Payroll/SingleUserPayroll-Inhouse(Immediate)/LoopingForVerifyTransactionStatus2'), [('btnnext') : findTestObject(
            'Bulk Upload/List Record/BTN - Next Page'), ('btnnextattribute') : findTestObject('Bulk Upload/List Record/BTN - Next Page - Kosong')
        , ('CreditAccountNo') : 2, ('CreditAccountName') : 3], FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Bulk Upload/List Record/BTN - Close'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-TransactionStatus/BTN - Total Transaction Record - See Detail Record'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

TotalTransactionAmountinIDR = WebUI.getText(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-ApproverConfrimScreen/V-totalDebitAmount'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TotalTransactionAmountinIDR, 'IDR 310,000.00', false, FailureHandling.CONTINUE_ON_FAILURE)

PayrollFeee = WebUI.getText(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-TransactionStatus/V-payrollFee'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(PayrollFeee, 'IDR 0.00 / record', false, FailureHandling.CONTINUE_ON_FAILURE)

TotalCharges = WebUI.getText(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-TransactionStatus/V-totalCharge'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TotalCharges, 'IDR 0.00', false, FailureHandling.CONTINUE_ON_FAILURE)

TotalDebitAmount = WebUI.getText(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-TransactionStatus/V-totalDebitAmount'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TotalDebitAmount, 'IDR 310,000.00', false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

