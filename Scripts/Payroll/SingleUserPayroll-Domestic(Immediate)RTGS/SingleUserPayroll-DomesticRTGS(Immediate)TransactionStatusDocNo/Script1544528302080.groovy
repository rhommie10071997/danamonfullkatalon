import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW

transStat = WebUI.getText(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-TransactionStatusDocNo/V-transactionStatus'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(transStat, 'In Progress', true, FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.TransactionStatus = transStat

TransactionReferenceNo = WebUI.getText(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-TransactionStatus/V-transactionReferenceNo'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TransactionReferenceNo, GlobalVariable.ReffNo, true, FailureHandling.CONTINUE_ON_FAILURE)

verNum = WebUI.getText(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-TransactionStatusDocNo/V-versionNumber'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(verNum, GlobalVariable.ReffNo, true, FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.VersionNo = verNum

Datee = WebUI.getText(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-TransactionStatus/V-submitDateTime'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Date = Datee

WebUI.verifyMatch(Datee, Datee, false, FailureHandling.CONTINUE_ON_FAILURE)

Menu = WebUI.getText(findTestObject('SingleUserDometstic(Repeat)SKN/Value-TransactionStatusReffNo/V-menu'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(Menu, 'Menu', true, FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.menu123 = Menu

Product = WebUI.getText(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-TransactionStatus/V-product'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(Product, GlobalVariable.UniversalVariable.get('Product'), false, FailureHandling.CONTINUE_ON_FAILURE)

FileType = WebUI.getText(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-TransactionStatus/Label - File Type'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(FileType, GlobalVariable.UniversalVariable.get('FileType'), true, FailureHandling.CONTINUE_ON_FAILURE)

FileFormat = WebUI.getText(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-TransactionStatus/V-FileFormat'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('FileFormat', FileFormat)

WebUI.verifyMatch(FileFormat, GlobalVariable.UniversalVariable.get('FileFormat'), true, FailureHandling.CONTINUE_ON_FAILURE)

FileUpload1 = WebUI.getText(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-TransactionStatus/Label - File Upload'), 
    FailureHandling.CONTINUE_ON_FAILURE)

FileUpload = FileUpload1.concat(".CSV")

WebUI.verifyMatch(FileUpload, GlobalVariable.UniversalVariable.get('FileUpload'), true, FailureHandling.CONTINUE_ON_FAILURE)

FileDescription = WebUI.getText(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-TransactionStatus/Label - File Description'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(FileDescription, GlobalVariable.UniversalVariable.get('FileDescription'), true, FailureHandling.CONTINUE_ON_FAILURE)

DebitAccount1 = WebUI.getText(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-TransactionStatus/Label - Debit Account'), 
    FailureHandling.CONTINUE_ON_FAILURE)

DebitAccount = DebitAccount1.replace('(IDR)', '( IDR )')

WebUI.verifyMatch(DebitAccount, GlobalVariable.UniversalVariable.get('DebitAccount'), false, FailureHandling.CONTINUE_ON_FAILURE)

Product = WebUI.getText(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-TransactionStatus/V-product2'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(Product, GlobalVariable.UniversalVariable.get('Product'), false, FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.product123 = Product

TotalTransactionRecord = WebUI.getText(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-TransactionStatus/Label - Total Transaction Record'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TotalTransactionRecord, GlobalVariable.UniversalVariable.get('TotalTransactionRecord'), true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-TransactionStatus/BTN - Total Transaction Record - See Detail Record'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

PopUpProduct = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Product'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(PopUpProduct, GlobalVariable.UniversalVariable.get('Product'), false, FailureHandling.CONTINUE_ON_FAILURE)

PopUpDebitAccount1 = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Debit Account'), FailureHandling.CONTINUE_ON_FAILURE)

PopUpDebitAccount = PopUpDebitAccount1.replace('(IDR)', '( IDR )')

WebUI.verifyMatch(PopUpDebitAccount, GlobalVariable.UniversalVariable.get('DebitAccount'), false, FailureHandling.CONTINUE_ON_FAILURE)

PopUpTotalTransactionRecord = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Total Transaction Record'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(PopUpTotalTransactionRecord, GlobalVariable.UniversalVariable.get('TotalTransactionRecord'), true, FailureHandling.CONTINUE_ON_FAILURE)

TotalTransactionAmountinIDR = WebUI.getText(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-ApproverConfrimScreen/V-totalDebitAmount'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TotalTransactionAmountinIDR, GlobalVariable.UniversalVariable.get('TotalDebitAmount'), false, FailureHandling.CONTINUE_ON_FAILURE)

TransferFee1 = WebUI.getText(findTestObject('MenuPayroll/MultiUserDomesticSKN(Immediate)/Value-CreateResultScreen/V-transferFeeForLooping'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TransferFee = TransferFee1.replace('record', 'Record')

WebUI.verifyMatch(TransferFee, GlobalVariable.UniversalVariable.get('TransferFee'), false, FailureHandling.CONTINUE_ON_FAILURE)

detailsSKNFee1 = WebUI.getText(findTestObject('Payroll/SingleUserDomestic(Immediate)RTGS/V-confrimScreen/V-RTGSFee(Details)'))

detailsSKNFee = detailsSKNFee1.replace('record', 'Record')

WebUI.verifyMatch(detailsSKNFee, GlobalVariable.UniversalVariable.get('SKNFee'), false)

TotalCharges = WebUI.getText(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-ApproverConfrimScreen/V-totalCharge'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TotalCharges, GlobalVariable.UniversalVariable.get('totalCharge'), false, FailureHandling.CONTINUE_ON_FAILURE)

TotalDebitAmount = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Total Debit Amount'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TotalDebitAmount, GlobalVariable.UniversalVariable.get('TotalDebitAmount'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Bulk Upload/List Record/BTN - Close'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-TransactionStatus/BTN - Total Transaction Record - See Detail Record'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

TotalTransactionAmountinIDR = WebUI.getText(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-ApproverConfrimScreen/V-totalDebitAmount'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TotalTransactionAmountinIDR, GlobalVariable.UniversalVariable.get('TotalDebitAmount'), false, FailureHandling.CONTINUE_ON_FAILURE)

TransferFee2 = WebUI.getText(findTestObject('MenuPayroll/MultiUserDomesticSKN(Immediate)/Value-TransactionStatus/V-transferFee'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TransferFee3 = TransferFee2.replace('record', 'Record')

WebUI.verifyMatch(TransferFee3, GlobalVariable.UniversalVariable.get('TransferFee'), false, FailureHandling.CONTINUE_ON_FAILURE)

SKNFee5 = WebUI.getText(findTestObject('Payroll/SingleUserDomestic(Immediate)RTGS/V-transactionStatus/V-RTGSFee'))

SKNFee4 = SKNFee5.replace('record', 'Record')

WebUI.verifyMatch(SKNFee4, GlobalVariable.UniversalVariable.get('SKNFee'), false)

TotalCharges = WebUI.getText(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-TransactionStatus/V-totalCharge'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TotalCharges, GlobalVariable.UniversalVariable.get('totalCharge'), false, FailureHandling.CONTINUE_ON_FAILURE)

TotalDebitAmount = WebUI.getText(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-TransactionStatus/V-totalDebitAmount'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TotalDebitAmount, GlobalVariable.UniversalVariable.get('TotalDebitAmount'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

