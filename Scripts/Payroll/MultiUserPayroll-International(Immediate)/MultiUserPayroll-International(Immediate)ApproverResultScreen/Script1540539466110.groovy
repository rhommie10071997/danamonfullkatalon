import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

Menu = WebUI.getText(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-ApproverConfrimScreen/V-menu '), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(Menu, 'Payroll', true, FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.menu123 = Menu

Product = WebUI.getText(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-ApproverConfrimScreen/V-product'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(Product, 'Payroll International', false, FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.product123 = Product

TransactionReferenceNo = WebUI.getText(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-ApproverConfrimScreen/V-transactionReffnum'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TransactionReferenceNo, GlobalVariable.ReffNo, true, FailureHandling.CONTINUE_ON_FAILURE)

DocumentNo = WebUI.getText(findTestObject('MenuPayroll/MultiUserInhouse(Immediate)/Value-ApproverConfrimScreen/V-transactionDocumentNum'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(DocumentNo, GlobalVariable.ReffNo, true, FailureHandling.CONTINUE_ON_FAILURE)

FileType = WebUI.getText(findTestObject('Bulk Upload/BucketDetail/Label - File Type'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(FileType, GlobalVariable.UniversalVariable.get('FileType'), true, FailureHandling.CONTINUE_ON_FAILURE)

FileUpload = WebUI.getText(findTestObject('Bulk Upload/BucketDetail/Label - File Upload'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(FileUpload, GlobalVariable.UniversalVariable.get('FileUpload'), true, FailureHandling.CONTINUE_ON_FAILURE)

FileDescription = WebUI.getText(findTestObject('Bulk Upload/BucketDetail/Label - File Description'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(FileDescription, GlobalVariable.UniversalVariable.get('FileDescription'), true, FailureHandling.CONTINUE_ON_FAILURE)

DebitAccount1 = WebUI.getText(findTestObject('Bulk Upload/Bucket Confirm/Label - Debit Account'), FailureHandling.CONTINUE_ON_FAILURE)

DebitAccount = DebitAccount1.replace(' USD ','  USD  ')

WebUI.verifyMatch(DebitAccount, GlobalVariable.UniversalVariable.get('DebitAccount'), false, FailureHandling.CONTINUE_ON_FAILURE)

Product = WebUI.getText(findTestObject('Bulk Upload/Bucket Confirm/Label - Product'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(Product, GlobalVariable.UniversalVariable.get('Product'), false, FailureHandling.CONTINUE_ON_FAILURE)

TotalTransactionRecord2 = WebUI.getText(findTestObject('Bulk Upload/Bucket Confirm/Label - Total Transaction Record - 2'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TotalTransactionRecord2, GlobalVariable.UniversalVariable.get('TotalTransactionRecord2'), true, FailureHandling.CONTINUE_ON_FAILURE)

mimTransactionFee2 = WebUI.getText(findTestObject('Payroll/singleUserInternational(Immediate)/V-confrimScreen/V-minimumTransactionFee'), 
    FailureHandling.CONTINUE_ON_FAILURE)

mimTransactionFee = mimTransactionFee2.replace('IDR ', 'IDR')

WebUI.verifyMatch(mimTransactionFee, GlobalVariable.UniversalVariable.get('mimTransactionFee'), false, FailureHandling.CONTINUE_ON_FAILURE)

fullAmountCharge2 = WebUI.getText(findTestObject('Payroll/singleUserInternational(Immediate)/V-confrimScreen/V-fullAmountCharge'), 
    FailureHandling.CONTINUE_ON_FAILURE)

fullAmountCharge = fullAmountCharge2.replace('IDR ', 'IDR')

WebUI.verifyMatch(fullAmountCharge, GlobalVariable.UniversalVariable.get('fullAmountCharge'), false, FailureHandling.CONTINUE_ON_FAILURE)

provision2 = WebUI.getText(findTestObject('Payroll/singleUserInternational(Immediate)/V-confrimScreen/V-provision'), FailureHandling.CONTINUE_ON_FAILURE)

provision = provision2.replace('IDR ', 'IDR')

WebUI.verifyMatch(provision, GlobalVariable.UniversalVariable.get('provision'), false, FailureHandling.CONTINUE_ON_FAILURE)

correspondentBankFee2 = WebUI.getText(findTestObject('Payroll/singleUserInternational(Immediate)/V-confrimScreen/V-correspondentBankFee'), 
    FailureHandling.CONTINUE_ON_FAILURE)

correspondentBankFee = correspondentBankFee2.replace('IDR ', 'IDR')

WebUI.verifyMatch(correspondentBankFee, GlobalVariable.UniversalVariable.get('correspondentBankFee'), false, FailureHandling.CONTINUE_ON_FAILURE)

cableFee2 = WebUI.getText(findTestObject('Payroll/singleUserInternational(Immediate)/V-confrimScreen/V-cableFee'), FailureHandling.CONTINUE_ON_FAILURE)

cableFee = cableFee2.replace('IDR ', 'IDR')

WebUI.verifyMatch(cableFee, GlobalVariable.UniversalVariable.get('cableFee'), false, FailureHandling.CONTINUE_ON_FAILURE)

totalCharge1 = WebUI.getText(findTestObject('Payroll/SingleUserDomestic(Immediate)SKN/Value-confrimScreen/V-totalCharge'), 
    FailureHandling.CONTINUE_ON_FAILURE)

totalCharge = totalCharge1.replace('USD ', 'USD')

WebUI.verifyMatch(totalCharge, GlobalVariable.UniversalVariable.get('totalCharge'), false, FailureHandling.CONTINUE_ON_FAILURE)

TotalDebitAmount1 = WebUI.getText(findTestObject('Bulk Upload/Bucket Confirm/Label - Total Debit Amount'), FailureHandling.CONTINUE_ON_FAILURE)
TotalDebitAmount = TotalDebitAmount1.replace('USD ', 'USD')
WebUI.verifyMatch(TotalDebitAmount, GlobalVariable.UniversalVariable.get('TotalDebitAmount'), false, FailureHandling.CONTINUE_ON_FAILURE)

chargeToRemitter1 = WebUI.getText(findTestObject('Payroll/SingleUserDomestic(Immediate)SKN/Value-confrimScreen/V-chargeToRemitter'), 
    FailureHandling.CONTINUE_ON_FAILURE)

String [] chargeToRemitter2  = chargeToRemitter1.split('=')

chargeToRemitter3 = chargeToRemitter2 [1]
chargeToRemitter = chargeToRemitter3.replace(' USD   ','   USD  ')
String [] FlagRemitter1 = GlobalVariable.UniversalVariable.get('chargeToRemitter').split('=')
FlagRemitter = FlagRemitter1[1]
WebUI.verifyMatch(chargeToRemitter, FlagRemitter, false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Bulk Upload/Bucket Confirm/BTN - Total Transaction Record - See Detail Record'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

PopUpListRecord = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - List Record'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(PopUpListRecord, GlobalVariable.UniversalVariable.get('PopUpListRecord'), true, FailureHandling.CONTINUE_ON_FAILURE)

PopUpProduct = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Product'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(PopUpProduct, GlobalVariable.UniversalVariable.get('Product'), false, FailureHandling.CONTINUE_ON_FAILURE)

PopUpDebitAccount3 = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Debit Account'), FailureHandling.CONTINUE_ON_FAILURE)

PopUpDebitAccount = PopUpDebitAccount3.replace(' USD ','  USD  ')

WebUI.verifyMatch(PopUpDebitAccount, GlobalVariable.UniversalVariable.get('DebitAccount'), false, FailureHandling.CONTINUE_ON_FAILURE)

PopUpTotalTransactionRecord = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Total Transaction Record'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(PopUpTotalTransactionRecord, GlobalVariable.UniversalVariable.get('TotalTransactionRecord2'), true, FailureHandling.CONTINUE_ON_FAILURE)

mimTransactionFee1 = WebUI.getText(findTestObject('Payroll/singleUserInternational(Immediate)/V-ConfrimScreen(Detail)/V-minimumTransactionFee'), 
    FailureHandling.CONTINUE_ON_FAILURE)

mimTransactionFee = mimTransactionFee1.replace('/ Record', ' / record')

WebUI.verifyMatch(mimTransactionFee, GlobalVariable.UniversalVariable.get('mimTransactionFee'), false, FailureHandling.CONTINUE_ON_FAILURE)

fullAmountCharge1 = WebUI.getText(findTestObject('Payroll/singleUserInternational(Immediate)/V-ConfrimScreen(Detail)/V-fullAmountCharge'), 
    FailureHandling.CONTINUE_ON_FAILURE)

fullAmountCharge = fullAmountCharge1.replace('/ Record', ' / record')

WebUI.verifyMatch(fullAmountCharge, GlobalVariable.UniversalVariable.get('fullAmountCharge'), false, FailureHandling.CONTINUE_ON_FAILURE)

provision1 = WebUI.getText(findTestObject('Payroll/singleUserInternational(Immediate)/V-ConfrimScreen(Detail)/V-provision'), 
    FailureHandling.CONTINUE_ON_FAILURE)

provision = provision1.replace('/ Record', ' / record')

WebUI.verifyMatch(provision, GlobalVariable.UniversalVariable.get('provision'), false, FailureHandling.CONTINUE_ON_FAILURE)

correspondentBankFee1 = WebUI.getText(findTestObject('Payroll/singleUserInternational(Immediate)/V-ConfrimScreen(Detail)/V-correspondentBankFee'), 
    FailureHandling.CONTINUE_ON_FAILURE)

correspondentBankFee = correspondentBankFee1.replace('/ Record', ' / record')

WebUI.verifyMatch(correspondentBankFee, GlobalVariable.UniversalVariable.get('correspondentBankFee'), false, FailureHandling.CONTINUE_ON_FAILURE)

cableFee1 = WebUI.getText(findTestObject('Payroll/singleUserInternational(Immediate)/V-ConfrimScreen(Detail)/V-cableFee'), 
    FailureHandling.CONTINUE_ON_FAILURE)

cableFee = cableFee1.replace('/ Record', ' / record')

WebUI.verifyMatch(cableFee, GlobalVariable.UniversalVariable.get('cableFee'), false, FailureHandling.CONTINUE_ON_FAILURE)

PopUptotalCharge = WebUI.getText(findTestObject('Payroll/SingleUserDomestic(Immediate)SKN/Value-confrimScreen(Details)/V-totalCharge'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(PopUptotalCharge, GlobalVariable.UniversalVariable.get('totalCharge'), false, FailureHandling.CONTINUE_ON_FAILURE)

PopUpTotalDebitAmount = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Total Debit Amount'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(PopUpTotalDebitAmount, GlobalVariable.UniversalVariable.get('TotalDebitAmount'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.callTestCase(findTestCase('Payroll/Get Text - Table'), [('btnnext') : findTestObject('Bulk Upload/List Record/BTN - Next Page')
        , ('btnnextattribute') : findTestObject('Bulk Upload/List Record/BTN - Next Page - Kosong'), ('ColumnCreditAccountNumber') : 2
        , ('ColumnAmount') : 5], FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Bulk Upload/List Record/BTN - Close'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

