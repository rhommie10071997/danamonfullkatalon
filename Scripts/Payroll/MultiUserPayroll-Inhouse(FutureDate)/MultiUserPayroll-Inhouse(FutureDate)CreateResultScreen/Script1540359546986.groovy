import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

instDate = WebUI.getText(findTestObject('MenuPayroll/MultuUserInhouse(FutureDate)/Value-ApproverConfrimScreen/Value-instructionDateAndSession'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(instDate, GlobalVariable.UniversalVariable.get('instDate'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

SuccessMessage = WebUI.getText(findTestObject('Bulk Upload/Create Result/Label - Success Message'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(SuccessMessage, 'Your transaction is waiting for approval', true, FailureHandling.CONTINUE_ON_FAILURE)

ReferenceNo = WebUI.getText(findTestObject('Bulk Upload/Create Result/Label - Reference No'), FailureHandling.CONTINUE_ON_FAILURE)

String[] ReferenceNo1 = ReferenceNo.split(' ')

RefNo2 = (ReferenceNo1[2])

GlobalVariable.ReffNo = RefNo2

FileType = WebUI.getText(findTestObject('Bulk Upload/BucketDetail/Label - File Type'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(FileType, GlobalVariable.UniversalVariable.get('FileType'), true, FailureHandling.CONTINUE_ON_FAILURE)

FileTemplate = WebUI.getText(findTestObject('Bulk Upload/Create Result/V-FileFormat'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(FileTemplate, GlobalVariable.UniversalVariable.get('Extention'), true, FailureHandling.CONTINUE_ON_FAILURE)

FileUpload = WebUI.getText(findTestObject('Bulk Upload/BucketDetail/Label - File Upload'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(FileUpload, GlobalVariable.UniversalVariable.get('FileUpload'), true, FailureHandling.CONTINUE_ON_FAILURE)

FileDescription = WebUI.getText(findTestObject('Bulk Upload/BucketDetail/Label - File Description'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(FileDescription, GlobalVariable.UniversalVariable.get('FileDescription'), true, FailureHandling.CONTINUE_ON_FAILURE)

TransactionType = WebUI.getText(findTestObject('Bulk Upload/BucketDetail/Label - Transaction Type'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TransactionType, GlobalVariable.UniversalVariable.get('TransactionType'), true, FailureHandling.CONTINUE_ON_FAILURE)

DebitAccount = WebUI.getText(findTestObject('Bulk Upload/Bucket Confirm/Label - Debit Account'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(DebitAccount, GlobalVariable.UniversalVariable.get('DebitAccount'), false, FailureHandling.CONTINUE_ON_FAILURE)

Product = WebUI.getText(findTestObject('Bulk Upload/Bucket Confirm/Label - Product'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(Product, GlobalVariable.UniversalVariable.get('Product'), false, FailureHandling.CONTINUE_ON_FAILURE)

TotalTransactionRecord = WebUI.getText(findTestObject('Bulk Upload/BucketDetail/Label - Total Transaction Record'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TotalTransactionRecord, GlobalVariable.UniversalVariable.get('TotalTransactionRecord'), true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Bulk Upload/Bucket Confirm/BTN - Total Transaction Record - See Detail Record'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

PopUpListRecord = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - List Record'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(PopUpListRecord, GlobalVariable.UniversalVariable.get('PopUpListRecord'), true, FailureHandling.CONTINUE_ON_FAILURE)

PopUpProduct = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Product'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(PopUpProduct, GlobalVariable.UniversalVariable.get('PopUpProduct'), false, FailureHandling.CONTINUE_ON_FAILURE)

PopUpDebitAccount = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Debit Account'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(PopUpDebitAccount, GlobalVariable.UniversalVariable.get('PopUpDebitAccount'), false, FailureHandling.CONTINUE_ON_FAILURE)

instDate = WebUI.getText(findTestObject('MenuPayroll/MultuUserInhouse(FutureDate)/Value-ApproverConfrimScreen/Value-instructionDateAndSessionDetailRecord'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(instDate, GlobalVariable.UniversalVariable.get('instDate'), false, FailureHandling.CONTINUE_ON_FAILURE)

PopUpTotalTransactionRecord = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Total Transaction Record'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(PopUpTotalTransactionRecord, GlobalVariable.UniversalVariable.get('PopUpTotalTransactionRecord'), true, 
    FailureHandling.CONTINUE_ON_FAILURE)

TransferFee1 = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Transfer Fee'), FailureHandling.CONTINUE_ON_FAILURE)

TransferFee2 = TransferFee1.replace(' ', '  ')

TransferFee = (TransferFee2 + ' / record')

WebUI.verifyMatch(TransferFee, GlobalVariable.UniversalVariable.get('TransferFee'), false, FailureHandling.CONTINUE_ON_FAILURE)

TotalDebitAmount1 = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Total Debit Amount'), FailureHandling.CONTINUE_ON_FAILURE)

TotalDebitAmount = TotalDebitAmount1.replace(' ', '  ')

WebUI.verifyMatch(TotalDebitAmount, GlobalVariable.UniversalVariable.get('TotalDebitAmount'), false, FailureHandling.CONTINUE_ON_FAILURE)

PopUpDeatilTableInfo = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Detail Table Info'), FailureHandling.CONTINUE_ON_FAILURE)

String[] PopUpDeatilTableInfo1 = PopUpDeatilTableInfo.split(' ')

PopUpDeatilTableInfo2 = (PopUpDeatilTableInfo1[5])

WebUI.verifyMatch(PopUpDeatilTableInfo2, GlobalVariable.UniversalVariable.get('PopUpDeatilTableInfo2'), true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.callTestCase(findTestCase('Payroll/LoppingForVerifyCreateResult'), [('btnnext') : findTestObject('Bulk Upload/List Record/BTN - Next Page')
        , ('btnnextattribute') : findTestObject('Bulk Upload/List Record/BTN - Next Page - Kosong'), ('CreaditAccountNo') : 2
        , ('CreaditAccountName') : 1], FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Bulk Upload/List Record/BTN - Close'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Bulk Upload/Bucket Confirm/BTN - Total Transaction Record - See Detail Record'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

TransferFee = WebUI.getText(findTestObject('Bulk Upload/Bucket Confirm/Label - Transfer Fee'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TransferFee, GlobalVariable.UniversalVariable.get('TransferFee'), false, FailureHandling.CONTINUE_ON_FAILURE)

TotalDebitAmount = WebUI.getText(findTestObject('Bulk Upload/Bucket Confirm/Label - Total Debit Amount'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TotalDebitAmount, GlobalVariable.UniversalVariable.get('TotalDebitAmount'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('SingleUserInhouse(immediate)/Value-TransactionStatus(ReffNo)/Value-SuccessScreen/Button-Done'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Domestic(FutureDate)/B-Logout'), FailureHandling.CONTINUE_ON_FAILURE)

