import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.click(findTestObject('menu/BTN - clickMenu'))

WebUI.delay(5)

WebUI.click(findTestObject('menu/accInformation/DL - accInformationClick'))

WebUI.delay(2)

WebUI.click(findTestObject('menu/accInformation/accStatement/BTN - accStatementClick'))

WebUI.delay(2)

WebUI.click(findTestObject('menu/accInformation/accStatement/DisplayOption/DL - accClusterClick'))

WebUI.delay(2)

WebUI.click(findTestObject('menu/accInformation/accStatement/DisplayOption/Value - accClusterChoice (Ungrouped)'))

WebUI.click(findTestObject('menu/accInformation/accStatement/DisplayOption/BTN - searchClick'))

WebUI.delay(3)

String Account = GlobalVariable.confirmScreens.get('trfFromChecker')
String[] AccountSplit = Account.split(' ')
//String Account = '000002018547'

//WebUI.setText(findTestObject('menu/accInformation/accStatement/DisplayOption/Search/Textfield - textfieldAccNo'), AccountSplit[
//    1])
WebUI.setText(findTestObject('menu/accInformation/accStatement/DisplayOption/Search/Textfield - textfieldAccNo'), AccountSplit[1])

WebUI.click(findTestObject('menu/accInformation/accStatement/DisplayOption/Search/BTN - searchAccountClick'))

WebUI.delay(3)

WebUI.click(findTestObject('menu/accInformation/accStatement/DisplayOption/Search/Checkbox - checkboxClick'))

WebUI.delay(2)

WebUI.click(findTestObject('menu/accInformation/accStatement/DisplayOption/Search/BTN - showClick'))

WebUI.delay(2)

WebUI.click(findTestObject('menu/accInformation/accStatement/displayClick'))

WebUI.delay(20)

String inquiryCheckerForRight = GlobalVariable.confirmScreens.get('amountChecker')

WebUI.callTestCase(findTestCase('TransactionInquiry/InquiryChecker'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.switchToDefaultContent()

WebUI.delay(10)

