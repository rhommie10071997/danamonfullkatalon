import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import internal.GlobalVariable as GlobalVariable

WebDriver driver = DriverFactory.getWebDriver()

WebUI.scrollToElement(findTestObject('menu/accInformation/accStatement/Value - TotalPage'), 0)

totalPageStr = WebUI.getText(findTestObject('menu/accInformation/accStatement/Value - TotalPage'))

int totalPage = Integer.parseInt(totalPageStr)



if (totalPage > 13) {
	
	totalPage = (totalPage - 6)
	
    WebUI.setText(findTestObject('menu/accInformation/accStatement/Textfield - totalPageText'), '' + totalPage)

    WebUI.delay(7)
}

driver.switchTo().frame('login')

driver.switchTo().frame('mainFrame')

String flag = 'false'

String SKNFee = ''

WebUI.delay(3)

'Bakal Nge loop kalau flagnya 0'
while (flag.equals('false')) {
    'Bakal Nge loop sebanyak totalpage di dalam table'
    for (int i = 0; i < 10; i++) {
        'Letak Table Inquiry'
        WebElement Table = driver.findElement(By.xpath('//table[@id="globalTableTarget0"]/tbody'))

        'Cari seberapa bnyak Rows yang ada di table'
        List<WebElement> Rows = Table.findElements(By.tagName('tr'))

        'Bakal Nge loop seberapa banyak Rows yang di dapat'
        table: for (int j = 0; j < Rows.size(); j++) {
            'Cari seberapa banyak Column yang ada di table'
            List<WebElement> Cols = Rows.get(j).findElements(By.tagName('td'))

            String AccountDescription = '' + Cols.get(4).getText()
			
            String AccountDescriptionChecker = '' + GlobalVariable.confirmScreens.get('accDescChecker')

            AccountDescriptionChecker = AccountDescriptionChecker.replace(': ', '')
			
			//String AccountDescriptionChecker = 'BTInhouse1342382'
			
			//WebUI.verifyMatch(AccountDescription, AccountDescriptionChecker, false, FailureHandling.CONTINUE_ON_FAILURE)
			
            'Pengecheckan AccountDescription sama atau tidak'
            if (AccountDescription.equals(AccountDescriptionChecker)) {
                'Verify AccountDescription dimana sudah di temukan'
                WebUI.verifyMatch(AccountDescription, AccountDescriptionChecker, false, FailureHandling.CONTINUE_ON_FAILURE)
				
				flag = 'true'
				
				j++
				
				if (j > 10) {
					'Ganti Next Page'
					WebUI.click(findTestObject('menu/accInformation/accStatement/BTN - Button Next'))

					'Rows yang paling pertama'
					j = 1
				}
				
				SKNFee = GlobalVariable.confirmScreens.get('totalChargesChecker')
				
				//SKNFee = '0.00'
				
				String FeeFlag = GlobalVariable.confirmScreens.get('FeeFlag')
				
				if(FeeFlag.equals('True')) {
				
				String[] SKNFee1 = SKNFee.split(': ')
				
				String SKNFee2 = SKNFee1[1]
				
				SKNFee2 = SKNFee2.replace('IDR','')
									'------------------------------------------------'
				
									'Get Charge di table'
				List<WebElement> ColsBaru = Rows.get(j).findElements(By.tagName('td'))
				
				String GetSKN = '' + ColsBaru.get(5).getText()
				
									'Menghilangkan Currency'
				GetSKN = GetSKN.substring(4)
				
									'Verify Match'
				WebUI.verifyMatch(GetSKN, SKNFee2, false, FailureHandling.CONTINUE_ON_FAILURE)
				}
				
				break
				
				
				/*
                'Jika transaksi ada charge maka nembak di bawa rows yang sudah di temukan'
                if (GlobalVariable.confirmScreens.get('Charge') == 'Yes') {
                    j++

                    'Jika Rows telaknya ada di urutan ke 10, maka Charge ada di page sebalah di Rows pertama'
                    if (j > 10) {
                        'Ganti Next Page'
                        WebUI.click(findTestObject('menu/accInformation/accStatement/BTN - Button Next'))

                        'Rows yang paling pertama'
                        j = 1
                    }
                    
                    'Pemindahan Format SKNFee'
                    SKNFee = GlobalVariable.confirmScreens.get('totalChargesChecker')

                    SKNFee = SKNFee.replace('       ', '')

                    String[] SKNFee1 = SKNFee.split('   ')

                    String SKNFee2 = (SKNFee1[1]) + '.00'

                    '------------------------------------------------'

                    'Get Charge di table'
                    List<WebElement> ColsBaru = Rows.get(j).findElements(By.tagName('td'))

                    String GetSKN = '' + ColsBaru.get(5).getText()

                    'Menghilangkan Currency'
                    GetSKN = GetSKN.substring(4)

                    'Verify Match'
                    WebUI.verifyMatch(GetSKN, SKNFee2, false, FailureHandling.CONTINUE_ON_FAILURE)

                    'Kalau sama maka flag akan berutepba menjadi True dan Loop bakal keluar'
                    flag = 'true'
                }*/
               
				
                'Kalau sama maka flag akan beruba menjadi True dan Loop bakal keluar'
            }
        }
        
        if (flag.equals('false')) {
            'Frame yang di gunakan BTN ini kosong - BTN Next'
            WebUI.click(findTestObject('menu/accInformation/accStatement/BTN - Button Next'), FailureHandling.CONTINUE_ON_FAILURE)
        }
    }
}

WebUI.switchToDefaultContent(FailureHandling.CONTINUE_ON_FAILURE)

