import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW

WebUI.openBrowser('https://10.194.8.106:39990/')

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-CorpID'), 0)

WebUI.setText(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-CorpID'), GlobalVariable.MultiUserLoginCorpregId)

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-UserID'), 0)

WebUI.setText(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-UserID'), GlobalVariable.userIDforApprover3)

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-UserPass'), 0)

WebUI.setText(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-UserPass'), GlobalVariable.MultiUserPass)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/B-Login'))

WebUI.delay(15)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-MenuSearch'))

WebUI.setText(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/TF-MenuSearch'), 'Pending Task')

WebUI.delay(2)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/B-Menu'))

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/PendingTask/T-PendingTask'))

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/B-Menu'))

WebUI.delay(2)

WebUI.waitForElementVisible(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/PendingTask/T-DisplayOption'), 
    0)

WebUI.click(findTestObject('MultiUserInhouse(Immediate)/Inhouse(Immediate)/PendingTask/T-DisplayOption'))

WebUI.click(findTestObject('BeneficiaryListAdd/BeneficiaryListApproverEntryScreen/T-advancedSearch'))

WebUI.delay(2)

WebUI.click(findTestObject('BeneficiaryListAdd/BeneficiaryListApproverEntryScreen/RB-dateRange'))

WebUI.click(findTestObject('BeneficiaryListAdd/BeneficiaryListApproverEntryScreen/DL-menuSearch'))

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), 'Beneficiary List')

WebUI.setText(findTestObject('SingleUserInhouse(immediate)/EntryScreen/TF-SelectAccountValue'), Keys.chord(Keys.ENTER))

WebUI.delay(2)

WebUI.click(findTestObject('BeneficiaryListAdd/BeneficiaryListApproverEntryScreen/B-ApproverShow'))

