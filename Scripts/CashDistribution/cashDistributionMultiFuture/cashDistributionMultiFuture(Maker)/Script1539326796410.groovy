import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable


WebUI.setText(findTestObject('homelogin/homelogin.corporateid'), 'PCMSILK001', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('homelogin/homelogin.userid'), 'silk02', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('homelogin/homelogin.password'), 'Password123', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('homelogin/homelogin.submit'), FailureHandling.CONTINUE_ON_FAILURE)

String SettingSetupPriority = '100'

String MainAccount = '000001105535'

String AmountType = 'Fixed'

//Entry Screen Cash Distribution(Form Sub Tabel)
//*Note = Maximal jumlah sub = 2
int jumlahSub = 2

if (jumlahSub > 2) {
    jumlahSub = 2
}

int subSweepPriority = 100

if (subSweepPriority > 100) {
    subSweepPriority = 100
}

String subAccountDescription = 'TestforSub'

String mainAccountDescription = 'TestforMain'

GlobalVariable.confirmScreens.put('jumlahSub', jumlahSub)

GlobalVariable.confirmScreens.put('subSweepPriority', subSweepPriority)

GlobalVariable.confirmScreens.put('mainAccDesc', mainAccountDescription)

GlobalVariable.confirmScreens.put('subAccDesc', subAccountDescription)

WebUI.delay(10, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/BTN - clickMenu'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.scrollToElement(findTestObject('menu/LiquidityManagement/BTN - LiquidityManagementClick'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/LiquidityManagement/BTN - LiquidityManagementClick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/LiquidityManagement/CashDistribution/BTN - CashDistributionClick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(9, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/LiquidityManagement/CashDistribution/DL - mainAccountClick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('menu/LiquidityManagement/CashDistribution/Textfield - textfieldMainAccountClick'), MainAccount, 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/LiquidityManagement/CashDistribution/BTN - choicesAll(PalingAtas)'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('menu/LiquidityManagement/CashDistribution/Textfield - sweepPriorityTextfield'), SettingSetupPriority, 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

if (AmountType.equals('Fixed')) {
    WebUI.click(findTestObject('menu/LiquidityManagement/CashDistribution/BTN - fixedButton'))
} else if (AmountType.equals('Percentage')) {
    WebUI.click(findTestObject('menu/LiquidityManagement/CashDistribution/BTN - percentageButton'))
} else {
    WebUI.click(findTestObject('menu/LiquidityManagement/CashDistribution/BTN - fixedButton'))
}

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.callTestCase(findTestCase('CashDistribution/cashDistributionMultiFuture/InputSubAccount'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/LiquidityManagement/CashDistribution/specificDate/Label - specificDateClick'))

WebUI.delay(2)

WebUI.click(findTestObject('menu/LiquidityManagement/CashDistribution/specificDate/Label - futureDateClick'))

WebUI.click(findTestObject('menu/LiquidityManagement/CashDistribution/specificDate/tanggalClick/BTN - tanggalClick'))

WebUI.click(findTestObject('menu/LiquidityManagement/CashDistribution/specificDate/Session/DL - sessionClick'))

WebUI.click(findTestObject('menu/LiquidityManagement/CashDistribution/specificDate/Session/BTN - sessionChoice(Jam4)'))

WebUI.delay(2)

WebUI.click(findTestObject('menu/LiquidityManagement/RangeBalance/BTN - confirmClick'))

WebUI.delay(5)

mainAccountChecker = WebUI.getText(findTestObject('menu/LiquidityManagement/CashDistribution/checker/Label - mainAccountChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

String[] mainAccountSplit = mainAccountChecker.split(' ')

GlobalVariable.confirmScreens.put('mainAccountChecker', mainAccountSplit[1])

WebUI.verifyMatch(mainAccountSplit[1], GlobalVariable.confirmScreens.get('mainAccountChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

mainSweepPriorityChecker = WebUI.getText(findTestObject('menu/LiquidityManagement/CashDistribution/checker/Label - setupSweepPriorityChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('mainSweepPriorityChecker', mainSweepPriorityChecker)

WebUI.verifyMatch(mainSweepPriorityChecker, GlobalVariable.confirmScreens.get('mainSweepPriorityChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

amountTypeChecker = WebUI.getText(findTestObject('menu/LiquidityManagement/CashDistribution/checker/Label - amountTypeChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('amountTypeChecker', amountTypeChecker)

WebUI.verifyMatch(amountTypeChecker, GlobalVariable.confirmScreens.get('amountTypeChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

diagramChecker = WebUI.getText(findTestObject('menu/LiquidityManagement/CashDistribution/checker/Label - diagrams'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('diagramChecker', diagramChecker)

WebUI.verifyMatch(amountTypeChecker, GlobalVariable.confirmScreens.get('amountTypeChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

tableChecker = WebUI.getText(findTestObject('menu/LiquidityManagement/CashDistribution/checker/Label - tables'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('tableChecker', tableChecker)

WebUI.verifyMatch(tableChecker, GlobalVariable.confirmScreens.get('tableChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

cashConcerntrationChecker = WebUI.getText(findTestObject('menu/LiquidityManagement/CashDistribution/checker/Label - cashConcentrationChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('cashConcerntrationChecker', cashConcerntrationChecker)

WebUI.verifyMatch(cashConcerntrationChecker, GlobalVariable.confirmScreens.get('cashConcerntrationChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

totalFeeChecker = WebUI.getText(findTestObject('menu/LiquidityManagement/CashDistribution/checker/Label - totalFeeChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('totalFeeChecker', totalFeeChecker)

WebUI.verifyMatch(totalFeeChecker, GlobalVariable.confirmScreens.get('totalFeeChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

equilvalentToDebitAccountChecker = WebUI.getText(findTestObject('menu/LiquidityManagement/CashDistribution/checker/Label - equivalentToDebitAccountChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('equilvalentToDebitAccountChecker', equilvalentToDebitAccountChecker)

WebUI.verifyMatch(equilvalentToDebitAccountChecker, GlobalVariable.confirmScreens.get('equilvalentToDebitAccountChecker'), 
    false, FailureHandling.CONTINUE_ON_FAILURE)

insModeChecker = WebUI.getText(findTestObject('menu/LiquidityManagement/CashDistribution/checker/Label - insModeChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('insModeChecker', insModeChecker)

WebUI.verifyMatch(insModeChecker, GlobalVariable.confirmScreens.get('insModeChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

futureDateChecker = WebUI.getText(findTestObject('menu/LiquidityManagement/CashDistribution/checker/Label - futureDateChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('futureDateChecker', futureDateChecker)

WebUI.verifyMatch(futureDateChecker, GlobalVariable.confirmScreens.get('futureDateChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

sessionChecker = WebUI.getText(findTestObject('menu/LiquidityManagement/CashDistribution/checker/Label - sessionChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('sessionChecker', sessionChecker)

WebUI.verifyMatch(sessionChecker, GlobalVariable.confirmScreens.get('sessionChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

expiredChecker = WebUI.getText(findTestObject('menu/LiquidityManagement/CashDistribution/checker/Label - expiredChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('expiredChecker', expiredChecker)

WebUI.verifyMatch(expiredChecker, GlobalVariable.confirmScreens.get('expiredChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/LiquidityManagement/RangeBalance/BTN - submitClick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/LiquidityManagement/RangeBalance/BTN - submitEndingClick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(10, FailureHandling.CONTINUE_ON_FAILURE)

