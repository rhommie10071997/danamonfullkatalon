import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.interactions.Actions as Actions
import java.util.Random as Random

Vector<String> subAccounts = new Vector<String>()

subAccounts.add('003571403611')

subAccounts.add('003571463466')

subAccounts.add('000001468362')

Random rand = new Random()

int count = 0

int amountRandom

if (GlobalVariable.confirmScreens.get('jumlahSub') == 1) {
    WebUI.scrollToElement(findTestObject('menu/LiquidityManagement/CashDistribution/BTN - fixedButton'), 0)

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.click(findTestObject('menu/LiquidityManagement/CashDistribution/BTN - plusClick'), FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.click(findTestObject('menu/LiquidityManagement/CashDistribution/DL - subAccountClick'), FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.setText(findTestObject('menu/LiquidityManagement/CashDistribution/Textfield - textfieldMainAccountClick'), subAccounts.get(
            count), FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.click(findTestObject('menu/LiquidityManagement/CashDistribution/BTN - choicesAll(PalingAtas)'), FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.delay(4, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.setText(findTestObject('menu/LiquidityManagement/CashDistribution/Textfield - subSweepPriorityTextfield'), '' + 
        GlobalVariable.confirmScreens.get('subSweepPriority'), FailureHandling.CONTINUE_ON_FAILURE)

    amountRandom = rand.nextInt(899999) + 100000

    WebUI.setText(findTestObject('menu/LiquidityManagement/CashDistribution/Textfield - amountTextfield'), '' + amountRandom, 
        FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.scrollToElement(findTestObject('menu/LiquidityManagement/CashDistribution/Textfield - subAccDescTextfield'), 0, 
        FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.setText(findTestObject('menu/LiquidityManagement/CashDistribution/Textfield - subAccDescTextfield'), (('' + count) + 
        GlobalVariable.confirmScreens.get('subAccDesc')) + amountRandom, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.scrollToElement(findTestObject('menu/LiquidityManagement/CashDistribution/Textfield - mainAccDescTextfield'), 
        0, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.setText(findTestObject('menu/LiquidityManagement/CashDistribution/Textfield - mainAccDescTextfield'), (('' + count) + 
        GlobalVariable.confirmScreens.get('mainAccDesc')) + amountRandom, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    count++
} else {
    WebUI.scrollToElement(findTestObject('menu/LiquidityManagement/CashDistribution/BTN - fixedButton'), 0)

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.click(findTestObject('menu/LiquidityManagement/CashDistribution/BTN - plusClick'), FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.click(findTestObject('menu/LiquidityManagement/CashDistribution/DL - subAccountClick'), FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.setText(findTestObject('menu/LiquidityManagement/CashDistribution/Textfield - textfieldMainAccountClick'), subAccounts.get(
            count), FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.click(findTestObject('menu/LiquidityManagement/CashDistribution/BTN - choicesAll(PalingAtas)'), FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.delay(4, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.setText(findTestObject('menu/LiquidityManagement/CashDistribution/Textfield - subSweepPriorityTextfield'), '' + 
        GlobalVariable.confirmScreens.get('subSweepPriority'), FailureHandling.CONTINUE_ON_FAILURE)

    amountRandom = rand.nextInt(899999) + 100000

    WebUI.setText(findTestObject('menu/LiquidityManagement/CashDistribution/Textfield - amountTextfield'), '' + amountRandom, 
        FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.scrollToElement(findTestObject('menu/LiquidityManagement/CashDistribution/Textfield - subAccDescTextfield'), 0, 
        FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.setText(findTestObject('menu/LiquidityManagement/CashDistribution/Textfield - subAccDescTextfield'), (('' + count) + 
        GlobalVariable.confirmScreens.get('subAccDesc')) + amountRandom, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.scrollToElement(findTestObject('menu/LiquidityManagement/CashDistribution/Textfield - mainAccDescTextfield'), 
        0, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.setText(findTestObject('menu/LiquidityManagement/CashDistribution/Textfield - mainAccDescTextfield'), (('' + count) + 
        GlobalVariable.confirmScreens.get('mainAccDesc')) + amountRandom, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    count++

    WebUI.scrollToElement(findTestObject('menu/LiquidityManagement/CashDistribution/BTN - fixedButton'), 0)

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.click(findTestObject('menu/LiquidityManagement/CashDistribution/BTN - plusClick'), FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.click(findTestObject('menu/LiquidityManagement/CashDistribution/DL - subAccountClick(2)'), FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.setText(findTestObject('menu/LiquidityManagement/CashDistribution/Textfield - textfieldMainAccountClick'), subAccounts.get(
            count), FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.click(findTestObject('menu/LiquidityManagement/CashDistribution/BTN - choicesAll(PalingAtas)'), FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.delay(4, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.setText(findTestObject('menu/LiquidityManagement/CashDistribution/Textfield - subSweepPriorityTextfield(2)'), 
        '' + GlobalVariable.confirmScreens.get('subSweepPriority'), FailureHandling.CONTINUE_ON_FAILURE)

    amountRandom = rand.nextInt(899999) + 100000

    WebUI.setText(findTestObject('menu/LiquidityManagement/CashDistribution/Textfield - amountTextfield(2)'), '' + amountRandom, 
        FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.scrollToElement(findTestObject('menu/LiquidityManagement/CashDistribution/Textfield - subAccDescTextfield(2)'), 
        0, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.setText(findTestObject('menu/LiquidityManagement/CashDistribution/Textfield - subAccDescTextfield(2)'), (('' + 
        count) + GlobalVariable.confirmScreens.get('subAccDesc')) + amountRandom, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.scrollToElement(findTestObject('menu/LiquidityManagement/CashDistribution/Textfield - mainAccDescTextfield(2)'), 
        0, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.setText(findTestObject('menu/LiquidityManagement/CashDistribution/Textfield - mainAccDescTextfield(2)'), (('' + 
        count) + GlobalVariable.confirmScreens.get('mainAccDesc')) + amountRandom, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)
}

