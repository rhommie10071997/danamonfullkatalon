import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

FileTemplate1 = WebUI.getText(findTestObject('Bulk Upload/BucketDetail/Label - File Template'), FailureHandling.CONTINUE_ON_FAILURE)

FileTemplate = FileTemplate1.replace(': ', '')

GlobalVariable.UniversalVariable.put('FileTemplate', FileTemplate)

WebUI.verifyMatch(FileTemplate, GlobalVariable.UniversalVariable.get('FileTemplate'), true, FailureHandling.CONTINUE_ON_FAILURE)

FileDescription1 = WebUI.getText(findTestObject('Bulk Upload/BucketDetail/Label - File Description'), FailureHandling.CONTINUE_ON_FAILURE)

FileDescription = FileDescription1.replace(': ', '')

GlobalVariable.UniversalVariable.put('FileDescription', FileDescription)

WebUI.verifyMatch(FileDescription, GlobalVariable.UniversalVariable.get('FileDescription'), true, FailureHandling.CONTINUE_ON_FAILURE)

TotalRecordInFile1 = WebUI.getText(findTestObject('BeneficiaryListUpload/ConfrimScreen/V-totalRecord'), FailureHandling.CONTINUE_ON_FAILURE)

TotalRecordInFile = TotalRecordInFile1.replace(': ', '')

GlobalVariable.UniversalVariable.put('TotalRecordInFile', TotalRecordInFile)

WebUI.verifyMatch(TotalRecordInFile, GlobalVariable.UniversalVariable.get('TotalRecordInFile'), true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('BeneficiaryListUpload/ConfrimScreen/sortingAscendingAccountNo'), FailureHandling.CONTINUE_ON_FAILURE)

myBankLineNumber = WebUI.getText(findTestObject('BeneficiaryListUpload/ConfrimScreen/Value/V-myBankLineNumber'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('myBankLineNumber', myBankLineNumber)

WebUI.verifyMatch(myBankLineNumber, GlobalVariable.UniversalVariable.get('myBankLineNumber'), true, FailureHandling.CONTINUE_ON_FAILURE)

myBankAliasName = WebUI.getText(findTestObject('BeneficiaryListUpload/ConfrimScreen/Value/V-myBankAliasName'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('myBankAliasName', myBankAliasName)

WebUI.verifyMatch(myBankAliasName, GlobalVariable.UniversalVariable.get('myBankAliasName'), true, FailureHandling.CONTINUE_ON_FAILURE)

myBankEmailNotification = WebUI.getText(findTestObject('BeneficiaryListUpload/ConfrimScreen/Value/V-myBankEmailNotification'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('myBankEmailNotification', myBankEmailNotification)

WebUI.verifyMatch(myBankEmailNotification, GlobalVariable.UniversalVariable.get('myBankEmailNotification'), false, FailureHandling.CONTINUE_ON_FAILURE)

myBankSMSNotification = WebUI.getText(findTestObject('BeneficiaryListUpload/ConfrimScreen/Value/V-myBankSMSNotification'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('myBankSMSNotification', myBankSMSNotification)

WebUI.verifyMatch(myBankSMSNotification, GlobalVariable.UniversalVariable.get('myBankSMSNotification'), true, FailureHandling.CONTINUE_ON_FAILURE)

myBankBankType = WebUI.getText(findTestObject('BeneficiaryListUpload/ConfrimScreen/Value/V-myBankBankType'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('myBankBankType', myBankBankType)

WebUI.verifyMatch(myBankBankType, GlobalVariable.UniversalVariable.get('myBankBankType'), true, FailureHandling.CONTINUE_ON_FAILURE)

myBankAccountNumber = WebUI.getText(findTestObject('BeneficiaryListUpload/ConfrimScreen/Value/V-myBankAccountNumber'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('myBankAccountNumber', myBankAccountNumber)

WebUI.verifyMatch(myBankAccountNumber, GlobalVariable.UniversalVariable.get('myBankAccountNumber'), true, FailureHandling.CONTINUE_ON_FAILURE)

myBankAccountName = WebUI.getText(findTestObject('BeneficiaryListUpload/ConfrimScreen/Value/V-myBankAccountName'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('myBankAccountName', myBankAccountName)

WebUI.verifyMatch(myBankAccountName, GlobalVariable.UniversalVariable.get('myBankAccountName'), true, FailureHandling.CONTINUE_ON_FAILURE)

myBankCurrency = WebUI.getText(findTestObject('BeneficiaryListUpload/ConfrimScreen/Value/V-myBankCurrency'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('myBankCurrency', myBankCurrency)

WebUI.verifyMatch(myBankCurrency, GlobalVariable.UniversalVariable.get('myBankCurrency'), true, FailureHandling.CONTINUE_ON_FAILURE)

