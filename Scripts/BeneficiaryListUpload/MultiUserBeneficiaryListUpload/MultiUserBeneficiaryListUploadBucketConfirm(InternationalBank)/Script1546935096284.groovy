import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(2)

WebUI.click(findTestObject('BeneficiaryListUpload/ConfrimScreen/Value/V-InternationalBankAccountNumber'), FailureHandling.CONTINUE_ON_FAILURE)

detailsInternationalBankAliasName1 = WebUI.getText(findTestObject('BeneficiaryListUpload/ConfrimScreen/Value/V-detailsInternationalBankAliasName'), 
    FailureHandling.CONTINUE_ON_FAILURE)

detailsInternationalBankAliasName = detailsInternationalBankAliasName1.replace(': ', '')

GlobalVariable.UniversalVariable.put('detailsInternationalBankAliasName', detailsInternationalBankAliasName)

WebUI.verifyMatch(detailsInternationalBankAliasName, GlobalVariable.UniversalVariable.get('detailsInternationalBankAliasName'), 
    true, FailureHandling.CONTINUE_ON_FAILURE)

detailsInternationalBankAccountNumber1 = WebUI.getText(findTestObject('BeneficiaryListUpload/ConfrimScreen/Value/V-detailsInternationalBankAccountNumber'), 
    FailureHandling.CONTINUE_ON_FAILURE)

detailsInternationalBankAccountNumber = detailsInternationalBankAccountNumber1.replace(': ', '')

GlobalVariable.UniversalVariable.put('detailsInternationalBankAccountNumber', detailsInternationalBankAccountNumber)

WebUI.verifyMatch(detailsInternationalBankAccountNumber, GlobalVariable.UniversalVariable.get('detailsInternationalBankAccountNumber'), 
    true, FailureHandling.CONTINUE_ON_FAILURE)

detailsInternationalBankAccountName1 = WebUI.getText(findTestObject('BeneficiaryListUpload/ConfrimScreen/Value/V-detailsInternationalBankAccountName'), 
    FailureHandling.CONTINUE_ON_FAILURE)

detailsInternationalBankAccountName = detailsInternationalBankAccountName1.replace(': ', '')

GlobalVariable.UniversalVariable.put('detailsInternationalBankAccountName', detailsInternationalBankAccountName)

WebUI.verifyMatch(detailsInternationalBankAccountName, GlobalVariable.UniversalVariable.get('detailsInternationalBankAccountName'), 
    true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2)

WebUI.click(findTestObject('BeneficiaryListUpload/ConfrimScreen/B-close'), FailureHandling.CONTINUE_ON_FAILURE)

internationalBankLineNumber = WebUI.getText(findTestObject('BeneficiaryListUpload/ConfrimScreen/Value/V-internationalBankLineNumber'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('internationalBankLineNumber', internationalBankLineNumber)

WebUI.verifyMatch(internationalBankLineNumber, GlobalVariable.UniversalVariable.get('internationalBankLineNumber'), true, 
    FailureHandling.CONTINUE_ON_FAILURE)

internationalBankAliasName = WebUI.getText(findTestObject('BeneficiaryListUpload/ConfrimScreen/Value/V-internationalBankAliasName'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('internationalBankAliasName', internationalBankAliasName)

WebUI.verifyMatch(internationalBankAliasName, GlobalVariable.UniversalVariable.get('internationalBankAliasName'), true, 
    FailureHandling.CONTINUE_ON_FAILURE)

internationalBankEmailNotification = WebUI.getText(findTestObject('BeneficiaryListUpload/ConfrimScreen/Value/V-internationalBankEmailNotification'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('internationalBankEmailNotification', internationalBankEmailNotification)

WebUI.verifyMatch(internationalBankEmailNotification, GlobalVariable.UniversalVariable.get('internationalBankEmailNotification'), 
    false, FailureHandling.CONTINUE_ON_FAILURE)

internationalBankSMSNotification = WebUI.getText(findTestObject('BeneficiaryListUpload/ConfrimScreen/Value/V-internationalBankSMSNotification'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('internationalBankSMSNotification', internationalBankSMSNotification)

WebUI.verifyMatch(internationalBankSMSNotification, GlobalVariable.UniversalVariable.get('internationalBankSMSNotification'), 
    true, FailureHandling.CONTINUE_ON_FAILURE)

internationalBankBankType = WebUI.getText(findTestObject('BeneficiaryListUpload/ConfrimScreen/Value/V-internationalBankBankType'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('internationalBankBankType', internationalBankBankType)

WebUI.verifyMatch(internationalBankBankType, GlobalVariable.UniversalVariable.get('internationalBankBankType'), true, FailureHandling.CONTINUE_ON_FAILURE)

internationalBankAccountNumber = WebUI.getText(findTestObject('BeneficiaryListUpload/ConfrimScreen/Value/V-internationalBankAccountNumber'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('internationalBankAccountNumber', internationalBankAccountNumber)

WebUI.verifyMatch(internationalBankAccountNumber, GlobalVariable.UniversalVariable.get('internationalBankAccountNumber'), 
    true, FailureHandling.CONTINUE_ON_FAILURE)

internationalBankAccountName = WebUI.getText(findTestObject('BeneficiaryListUpload/ConfrimScreen/Value/V-internationalBankAccountName'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('internationalBankAccountName', internationalBankAccountName)

WebUI.verifyMatch(internationalBankAccountName, GlobalVariable.UniversalVariable.get('internationalBankAccountName'), true, 
    FailureHandling.CONTINUE_ON_FAILURE)

internationalBankBankBank = WebUI.getText(findTestObject('BeneficiaryListUpload/ConfrimScreen/Value/V-internationalBankBank'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('internationalBankBankBank', internationalBankBankBank)

WebUI.verifyMatch(internationalBankBankBank, GlobalVariable.UniversalVariable.get('internationalBankBankBank'), true, FailureHandling.CONTINUE_ON_FAILURE)

internationalBankCountry = WebUI.getText(findTestObject('BeneficiaryListUpload/ConfrimScreen/Value/V-InternationalBankCountry'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('internationalBankAccountName', internationalBankAccountName)

WebUI.verifyMatch(internationalBankAccountName, GlobalVariable.UniversalVariable.get('internationalBankAccountName'), true, 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Bulk Upload/Bucket Confirm/BTN - Submit'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('B-Ok'))

