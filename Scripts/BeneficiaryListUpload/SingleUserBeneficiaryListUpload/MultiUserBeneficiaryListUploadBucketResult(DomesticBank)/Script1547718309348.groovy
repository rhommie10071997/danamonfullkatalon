import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.click(findTestObject('BeneficiaryListUpload/ConfrimScreen/Value/V-DomesticBankAccountNumber'), FailureHandling.CONTINUE_ON_FAILURE)

detailsDomesticBankAliasName1 = WebUI.getText(findTestObject('BeneficiaryListUpload/ConfrimScreen/Value/V-detailsDomesticBankAliasName'), 
    FailureHandling.CONTINUE_ON_FAILURE)

detailsDomesticBankAliasName = detailsDomesticBankAliasName1.replace(': ','')

WebUI.verifyMatch(detailsDomesticBankAliasName, GlobalVariable.UniversalVariable.get('detailsDomesticBankAliasName'), true, 
    FailureHandling.CONTINUE_ON_FAILURE)

detailsDomesticBankAccountNumber1 = WebUI.getText(findTestObject('BeneficiaryListUpload/ConfrimScreen/Value/V-detailsDomesticBankAccountNumber'), 
    FailureHandling.CONTINUE_ON_FAILURE)

detailsDomesticBankAccountNumber =detailsDomesticBankAccountNumber1.replace(': ','')

WebUI.verifyMatch(detailsDomesticBankAccountNumber, GlobalVariable.UniversalVariable.get('detailsDomesticBankAccountNumber'), 
    true, FailureHandling.CONTINUE_ON_FAILURE)

detailsDomesticBankAccountName1 = WebUI.getText(findTestObject('BeneficiaryListUpload/ConfrimScreen/Value/V-detailsDomesticBankAccountName'), 
    FailureHandling.CONTINUE_ON_FAILURE)

detailsDomesticBankAccountName = detailsDomesticBankAccountName1.replace(': ','')

WebUI.verifyMatch(detailsDomesticBankAccountName, GlobalVariable.UniversalVariable.get('detailsDomesticBankAccountName'), 
    true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('BeneficiaryListUpload/ConfrimScreen/B-close'), FailureHandling.CONTINUE_ON_FAILURE)

domesticBankLineNumber = WebUI.getText(findTestObject('BeneficiaryListUpload/ConfrimScreen/Value/V-DomesticBankLineNumber'), 
    FailureHandling.CONTINUE_ON_FAILURE)


WebUI.verifyMatch(domesticBankLineNumber, GlobalVariable.UniversalVariable.get('domesticBankLineNumber'), true, FailureHandling.CONTINUE_ON_FAILURE)

domesticBankAliasName = WebUI.getText(findTestObject('BeneficiaryListUpload/ConfrimScreen/Value/V-DomesticBankAliasName'), 
    FailureHandling.CONTINUE_ON_FAILURE)


WebUI.verifyMatch(domesticBankAliasName, GlobalVariable.UniversalVariable.get('domesticBankAliasName'), true, FailureHandling.CONTINUE_ON_FAILURE)

domesticBankEmailNotification = WebUI.getText(findTestObject('BeneficiaryListUpload/ConfrimScreen/Value/V-DomesticBankEmailNotification '), 
    FailureHandling.CONTINUE_ON_FAILURE)


WebUI.verifyMatch(domesticBankEmailNotification, GlobalVariable.UniversalVariable.get('domesticBankEmailNotification'), 
    false, FailureHandling.CONTINUE_ON_FAILURE)

domesticBankSMSNotification = WebUI.getText(findTestObject('BeneficiaryListUpload/ConfrimScreen/Value/V-DomesticBankSMSNotification'), 
    FailureHandling.CONTINUE_ON_FAILURE)


WebUI.verifyMatch(domesticBankSMSNotification, GlobalVariable.UniversalVariable.get('domesticBankSMSNotification'), true, 
    FailureHandling.CONTINUE_ON_FAILURE)

domesticBankBankType = WebUI.getText(findTestObject('BeneficiaryListUpload/ConfrimScreen/Value/V-DomesticBankBankType'), 
    FailureHandling.CONTINUE_ON_FAILURE)


WebUI.verifyMatch(domesticBankBankType, GlobalVariable.UniversalVariable.get('domesticBankBankType'), true, FailureHandling.CONTINUE_ON_FAILURE)

domesticBankAccountNumber = WebUI.getText(findTestObject('BeneficiaryListUpload/ConfrimScreen/Value/V-DomesticBankAccountNumber'), 
    FailureHandling.CONTINUE_ON_FAILURE)


WebUI.verifyMatch(domesticBankAccountNumber, GlobalVariable.UniversalVariable.get('domesticBankAccountNumber'), true, FailureHandling.CONTINUE_ON_FAILURE)

domesticBankAccountName = WebUI.getText(findTestObject('BeneficiaryListUpload/ConfrimScreen/Value/V-DomesticBankAccountName'), 
    FailureHandling.CONTINUE_ON_FAILURE)


WebUI.verifyMatch(domesticBankAccountName, GlobalVariable.UniversalVariable.get('domesticBankAccountName'), true, FailureHandling.CONTINUE_ON_FAILURE)

domesticBankBankBank = WebUI.getText(findTestObject('BeneficiaryListUpload/ConfrimScreen/Value/V-DomesticBankBank'), FailureHandling.CONTINUE_ON_FAILURE)


WebUI.verifyMatch(domesticBankBankBank, GlobalVariable.UniversalVariable.get('domesticBankBankBank'), true, FailureHandling.CONTINUE_ON_FAILURE)

