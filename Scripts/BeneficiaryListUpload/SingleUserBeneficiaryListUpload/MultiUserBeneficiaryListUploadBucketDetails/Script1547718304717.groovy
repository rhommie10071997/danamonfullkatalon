import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

FileUploadStatus1 = WebUI.getText(findTestObject('Bulk Upload/BucketDetail/Label - File Upload Status - Comp'), FailureHandling.CONTINUE_ON_FAILURE)

FileUploadStatus = FileUploadStatus1.replace(': ', '')

GlobalVariable.UniversalVariable.put('FileUploadStatus',FileUploadStatus)

WebUI.verifyMatch(FileUploadStatus, GlobalVariable.UniversalVariable.get('FileUploadStatus'), true, FailureHandling.CONTINUE_ON_FAILURE)

userGroup1 = WebUI.getText(findTestObject('BeneficiaryListUpload/ConfrimScreen/V-userGroup'), FailureHandling.CONTINUE_ON_FAILURE)

userGroup = userGroup1.replace(': ', '')

GlobalVariable.UniversalVariable.put('userGroup',userGroup)

WebUI.verifyMatch(userGroup, GlobalVariable.UniversalVariable.get('userGroup'), false, FailureHandling.CONTINUE_ON_FAILURE)

fileFormat1 = WebUI.getText(findTestObject('BeneficiaryListUpload/ConfrimScreen/V-fileFormat'), FailureHandling.CONTINUE_ON_FAILURE)

fileFormat = fileFormat1.replace(': ', '')

GlobalVariable.UniversalVariable.put('fileFormat',fileFormat)

WebUI.verifyMatch(fileFormat, GlobalVariable.UniversalVariable.get('fileFormat'), false, FailureHandling.CONTINUE_ON_FAILURE)

fileName1 = WebUI.getText(findTestObject('BeneficiaryListUpload/ConfrimScreen/V-fileName'), FailureHandling.CONTINUE_ON_FAILURE)

fileName = fileName1.replace(': ', '')

GlobalVariable.UniversalVariable.put('fileName',fileName)

WebUI.verifyMatch(fileName, GlobalVariable.UniversalVariable.get('fileName'), false, FailureHandling.CONTINUE_ON_FAILURE)

TotalRecordInFile = WebUI.getText(findTestObject('Bulk Upload/BucketDetail/Label - Total Record In File'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('TotalRecordInFile',TotalRecordInFile)

WebUI.verifyMatch(TotalRecordInFile, GlobalVariable.UniversalVariable.get('TotalRecordInFile'), false, FailureHandling.CONTINUE_ON_FAILURE)

TotalSucces = WebUI.getText(findTestObject('Bulk Upload/BucketDetail/Label - Total Succes'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('TotalSucces',TotalSucces)

WebUI.verifyMatch(TotalSucces, GlobalVariable.UniversalVariable.get('TotalSucces'), false, FailureHandling.CONTINUE_ON_FAILURE)


TotalFailed = WebUI.getText(findTestObject('Bulk Upload/BucketDetail/Label - Total Failed'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.UniversalVariable.put('TotalFailed',TotalFailed)

WebUI.verifyMatch(TotalFailed, GlobalVariable.UniversalVariable.get('TotalFailed'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Bulk Upload/BucketDetail/BTN - Confirm'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

