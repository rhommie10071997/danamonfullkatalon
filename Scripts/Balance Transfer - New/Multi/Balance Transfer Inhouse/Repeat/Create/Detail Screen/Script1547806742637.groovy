import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import java.text.DecimalFormat as DecimalFormat
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW

WebUI.waitForElementVisible(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Confirm Screen/Label - From Account'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

FromAccountCreateConfirm = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Confirm Screen/Label - From Account'), 
    FailureHandling.CONTINUE_ON_FAILURE)

FromAccountCreateConfirm = FromAccountCreateConfirm.replace(': ', '')

WebUI.verifyMatch(FromAccountCreateConfirm, GlobalVariable.Map123.get('FromAccount123'), false, FailureHandling.CONTINUE_ON_FAILURE)

FAD = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Confirm Screen/Label - From Account Description'), 
    FailureHandling.CONTINUE_ON_FAILURE)

FromAccountDescriptionCreateConfirm = FAD.replace(': ', '')

WebUI.verifyMatch(FromAccountDescriptionCreateConfirm, GlobalVariable.Map123.get('FromAccountDescription123'), true, FailureHandling.CONTINUE_ON_FAILURE)

TT = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Confirm Screen/Label - Transfer To'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TransferToCreateConfirm = TT.replace(': ', '')

WebUI.verifyMatch(TransferToCreateConfirm, GlobalVariable.Map123.get('TransferTo123'), true, FailureHandling.CONTINUE_ON_FAILURE)

AccountNumberDetailScreen = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Confirm Screen/Label - Account Number'), 
    FailureHandling.CONTINUE_ON_FAILURE)

AccountNumberDetailScreen = AccountNumberDetailScreen.replace(': ', '')

WebUI.verifyMatch(AccountNumberDetailScreen, GlobalVariable.Map123.get('AccountNumber123'), true, FailureHandling.CONTINUE_ON_FAILURE)

if (GlobalVariable.Map123.get('TransferToFlag123') == 0) {
    AccNameConfirmScreen = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Confirm Screen/Label - Account Name'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    AccNameConfirmScreen = AccNameConfirmScreen.replace(': ', '')

    WebUI.verifyMatch(AccNameConfirmScreen, GlobalVariable.Map123.get('AccountName123'), false, FailureHandling.CONTINUE_ON_FAILURE)

    BenefNotifEmailCreateConfirm = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Confirm Screen/Label - Beneficiary notification Phone'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.verifyMatch(BenefNotifEmailCreateConfirm, GlobalVariable.Map123.get('email123'), true, FailureHandling.CONTINUE_ON_FAILURE)

    BenefNotifPhoneCreateConfirm = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Confirm Screen/Label - Beneficiary notification Email'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.verifyMatch(BenefNotifPhoneCreateConfirm, GlobalVariable.Map123.get('phone123'), true, FailureHandling.CONTINUE_ON_FAILURE)
} else if (GlobalVariable.Map123.get('TransferToFlag123') == 1) {
    AccNameConfirmScreen = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Confirm Screen/Label - Account Name'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    AccNameConfirmScreen = AccNameConfirmScreen.replace(': ', '')

    WebUI.verifyMatch(AccNameConfirmScreen, GlobalVariable.Map123.get('AccountName123'), false, FailureHandling.CONTINUE_ON_FAILURE)
} else {
    SaveToBenefList = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Confirm Screen/Label - Save to Beneficiary List'))

    WebUI.verifyMatch(SaveToBenefList, GlobalVariable.Map123.get('SaveToBenefList123'), false)

    AliasName = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Confirm Screen/Label - Alias Name'))

    AliasName = AliasName.replace(': ', '')

    AliasName = AliasName.replace(':', '')

    WebUI.verifyMatch(AliasName, GlobalVariable.Map123.get('AliasName123'), false, FailureHandling.CONTINUE_ON_FAILURE)

    BenefNotifEmailCreateConfirm = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Confirm Screen/Label - Beneficiary notification Phone'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.verifyMatch(BenefNotifEmailCreateConfirm, GlobalVariable.Map123.get('email123'), true, FailureHandling.CONTINUE_ON_FAILURE)

    BenefNotifPhoneCreateConfirm = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Confirm Screen/Label - Beneficiary notification Email'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.verifyMatch(BenefNotifPhoneCreateConfirm, GlobalVariable.Map123.get('phone123'), true, FailureHandling.CONTINUE_ON_FAILURE)
}

ToAccDescCreateConfirm = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Confirm Screen/Label - To Account Description'), 
    FailureHandling.CONTINUE_ON_FAILURE)

ToAccDescCreateConfirm = ToAccDescCreateConfirm.replace(': ', '')

WebUI.verifyMatch(ToAccDescCreateConfirm, GlobalVariable.Map123.get('FromAccountDescription123'), true, FailureHandling.CONTINUE_ON_FAILURE)

BenefRefNoCreateConfirm = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Confirm Screen/Label - Beneficiary Reference Number'), 
    FailureHandling.CONTINUE_ON_FAILURE)

BenefRefNoCreateConfirm = BenefRefNoCreateConfirm.replace(': ', '')

WebUI.verifyMatch(BenefRefNoCreateConfirm, GlobalVariable.Map123.get('BenefRefNum123'), true, FailureHandling.CONTINUE_ON_FAILURE)

RetainAmountCreateConfirm = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Confirm Screen/Label - Retain Amount'), 
    FailureHandling.CONTINUE_ON_FAILURE)

RetainAmountCreateConfirm = RetainAmountCreateConfirm.replace(': ', '')

WebUI.verifyMatch(RetainAmountCreateConfirm, GlobalVariable.Map123.get('retainamount123'), true, FailureHandling.CONTINUE_ON_FAILURE)

ER12 = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Confirm Screen/Label - Exchange Rate'), 
    FailureHandling.CONTINUE_ON_FAILURE)

ExhangeRateCreateConfirm = ER12.replace(': ', '')

WebUI.verifyMatch(ExhangeRateCreateConfirm, GlobalVariable.Map123.get('ExchangeRate123'), true, FailureHandling.CONTINUE_ON_FAILURE)

TC12 = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Confirm Screen/Label - Total Charge'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TotalChargeCreateConfirm = TC12.replace(': ', '')

GlobalVariable.Map123.put('TotalCharge123', TotalChargeCreateConfirm)

TDA12 = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Confirm Screen/Label - Total Debet Amount'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TotalDebetAmountCreateConfirm = TDA12.replace(': ', '')

GlobalVariable.Map123.put('TotalDebetAmount123', TotalDebetAmountCreateConfirm)

CI12 = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Confirm Screen/Label - Charge Instruction'), 
    FailureHandling.CONTINUE_ON_FAILURE)

ChargeInstructionCreateConfirm = CI12.replace(': ', '')

WebUI.verifyMatch(ChargeInstructionCreateConfirm, GlobalVariable.Map123.get('ChargeInstruction123'), true, FailureHandling.CONTINUE_ON_FAILURE)

DC12 = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Confirm Screen/Label - Debit Charge'), 
    FailureHandling.CONTINUE_ON_FAILURE)

DebitChargeCreateConfirm = DC12.replace(': ', '')

WebUI.verifyMatch(DebitChargeCreateConfirm, GlobalVariable.Map123.get('DebitCharge123'), true, FailureHandling.CONTINUE_ON_FAILURE)

LHBUPC = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Confirm Screen/Label - LHBU Purpose Code'), 
    FailureHandling.CONTINUE_ON_FAILURE)

LHBUPCCreateConfirm = LHBUPC.replace(': ', '')

WebUI.verifyMatch(LHBUPCCreateConfirm, GlobalVariable.Map123.get('LHBUPurposeCode123'), true, FailureHandling.CONTINUE_ON_FAILURE)

LHBUDT = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Confirm Screen/Label - LHBU Document Type'), 
    FailureHandling.CONTINUE_ON_FAILURE)

LHBUDTCreateConfirm = LHBUDT.replace(': ', '')

WebUI.verifyMatch(LHBUDTCreateConfirm, GlobalVariable.Map123.get('LHBUDocumentType123'), true, FailureHandling.CONTINUE_ON_FAILURE)

LHBUDTD = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Confirm Screen/Label - LHBU Document Type Description'), 
    FailureHandling.CONTINUE_ON_FAILURE)

LHBUDTDCreateConfirm = LHBUDTD.replace(': ', '')

WebUI.verifyMatch(LHBUDTDCreateConfirm, GlobalVariable.Map123.get('LHBUDocumentTypeDescription123'), true, FailureHandling.CONTINUE_ON_FAILURE)

IM12 = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Confirm Screen/Label - Instruction Mode'), 
    FailureHandling.CONTINUE_ON_FAILURE)

InstructionModeCreateConfirm = IM12.replace(': ', '')

WebUI.verifyMatch(InstructionModeCreateConfirm, GlobalVariable.Map123.get('InstructionMode123'), false, FailureHandling.CONTINUE_ON_FAILURE)

RepeatOn = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Confirm Screen/Label - Repeat - On'), 
    FailureHandling.CONTINUE_ON_FAILURE)

RepeatOn = RepeatOn.replace(': ', '')

WebUI.verifyMatch(RepeatOn, GlobalVariable.Map123.get('RepeatOn'), false, FailureHandling.CONTINUE_ON_FAILURE)

RepeatEvery = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Confirm Screen/Label - Repeat - Every'), 
    FailureHandling.CONTINUE_ON_FAILURE)

RepeatEvery = RepeatEvery.replace(': ', '')

WebUI.verifyMatch(RepeatEvery, GlobalVariable.Map123.get('RepeatEvery'), false, FailureHandling.CONTINUE_ON_FAILURE)

RepeatAt = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Confirm Screen/Label - Repeat - At'), 
    FailureHandling.CONTINUE_ON_FAILURE)

RepeatAt = RepeatAt.replace(': ', '')

WebUI.verifyMatch(RepeatAt, GlobalVariable.Map123.get('RepeatAt'), false, FailureHandling.CONTINUE_ON_FAILURE)

RepeatStart = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Confirm Screen/Label - Repeat - Start'), 
    FailureHandling.CONTINUE_ON_FAILURE)

RepeatStart = RepeatStart.replace(': ', '')

GlobalVariable.Map123.put('RepeatStart',RepeatStart)

RepeatEnd = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Confirm Screen/Label - Repeat - End'), 
    FailureHandling.CONTINUE_ON_FAILURE)

RepeatEnd = RepeatEnd.replace(': ', '')

GlobalVariable.Map123.put('RepeatEnd',RepeatEnd)

InstructionOnHoliday = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Confirm Screen/Label - Repeat - Instruction on Holiday'), 
    FailureHandling.CONTINUE_ON_FAILURE)

InstructionOnHoliday = InstructionOnHoliday.replace(': ', '')

WebUI.verifyMatch(InstructionOnHoliday, GlobalVariable.Map123.get('NonWorkingDayInstruction'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.scrollToElement(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Confirm Screen/BTN - Sumbit'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Confirm Screen/BTN - Sumbit'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(3, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Confirm Screen/BTN - Pop Up - Submit'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Confirm Screen/BTN - Pop Up - Submit'), 
    FailureHandling.CONTINUE_ON_FAILURE)

