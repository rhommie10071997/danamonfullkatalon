import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.remote.server.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Menu/TextField - Search'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Menu/TextField - Search'), ' ', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Menu/TextField - Search'), '', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Menu/BTN - Menu Object'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Menu/TextField - Search'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Menu/TextField - Search'), ' ', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Menu/TextField - Search'), 'Account Statement', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Menu/Label - Account Statement (By Search)'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Menu/BTN - Menu Object'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Menu/BTN - Menu Object'), FailureHandling.CONTINUE_ON_FAILURE)

/*
WebUI.click(findTestObject('Account Statement/DL - Account Cluster'))

WebUI.waitForElementVisible(findTestObject('Account Statement/DL - Account Cluster - Ungrouped'), 0)

WebUI.click(findTestObject('Account Statement/DL - Account Cluster - Ungrouped'))

*/
WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Account Statement/BTN - Search'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Account Statement/BTN - Search'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Account Statement/TF - Account Number'), 0, FailureHandling.CONTINUE_ON_FAILURE)

'Debit------------------------------------------------------------------------------------------'

'Split Account Number - Name  ------------------------------------------------------------------'
Account = GlobalVariable.Map123.get('FromAccount123')

String[] AccountSplit = Account.split('/ ')

AccountNumber = (AccountSplit[0])

'-----------------------------------------------------------------------------------------------'
WebUI.setText(findTestObject('Account Statement/TF - Account Number'), AccountNumber, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Account Statement/BTN - Search (1)'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Account Statement/BTN - Search (1)'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

TableInfo = WebUI.getText(findTestObject('Account Statement/Label - Table Info Showing'))

WebUI.verifyMatch(TableInfo, 'Showing 1 to 1 of 1 entries', false)

WebUI.click(findTestObject('Account Statement/Checkbox - Checkbox'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Account Statement/BTN - Show'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Account Statement/BTN - Show'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Account Statement/BTN - Display'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Account Statement/BTN - Display'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(15, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Account Statement/Transaction Inquiry/Label - Sub Label - Transaction Inquiry (Specific)'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

test = WebUI.getText(findTestObject('Account Statement/Transaction Inquiry/test'), FailureHandling.CONTINUE_ON_FAILURE)

'Get Text Total Page dan Set Text Search Page -----------------------------------------------------'
TotalPag = WebUI.getText(findTestObject('Account Statement/Transaction Inquiry/Label - Total Page'), FailureHandling.CONTINUE_ON_FAILURE)

int TotalPage = Integer.parseInt(TotalPag)

int PageSearch

'Total Page dikurangi 10 agar gak kelamaan saat mencari Amount dan Description yang benar'
PageSearch = (TotalPage - 10)

'Jika Total Page di bawah 0 atau Minus( -3 )'
if (PageSearch < 1) {
    PageSearch = 1
}

String PSearch = '' + PageSearch

'Set Text pada Text Field Search Page'
WebUI.setText(findTestObject('Account Statement/Transaction Inquiry/TextField - Search Page'), PSearch, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

'Verify Amount , Description------------------------------------------------------------------------'

'Mengubah Default Frame menjadi MainFrame untuks semua object'
WebDriver driver = DriverFactory.getWebDriver()

driver.switchTo().frame('login')

driver.switchTo().frame('mainFrame')

flag = 0

'Bakal Nge loop kalau flagnya 0'
while (flag == 0) {
    'Bakal Nge loop sebanyak totalpage di dalam table'
    for (int i = 0; i < 10; i++) {
        'Letak Table Inquiry'
        WebElement Table = driver.findElement(By.xpath('//table[@id="globalTableTarget0"]/tbody'))

        'Cari seberapa bnyak Rows yang ada di table'
        List<WebElement> Rows = Table.findElements(By.tagName('tr'))

        'Bakal Nge loop seberapa banyak Rows yang di dapat'
        table: for (int j = 0; j < Rows.size(); j++) {
            'Cari seberapa banyak Column yang ada di table'
            List<WebElement> Cols = Rows.get(j).findElements(By.tagName('td'))

            String Amount = '' + Cols.get(5).getText()

            Amount = Amount.substring(4)

            'Pengecheckan From Account Description dan Amount sama atau tidak'
            if ((Cols.get(4).getText() == GlobalVariable.Map123.get('FromAccountDescription123')) && (Amount == GlobalVariable.Map123.get(
                'amountinquiry'))) {
                'Verify Amount dan From Account Description dimana sudah di temukan'
                WebUI.verifyMatch(Cols.get(4).getText(), GlobalVariable.Map123.get('FromAccountDescription123'), false)

                WebUI.verifyMatch(Amount, GlobalVariable.Map123.get('amountinquiry'), false)

                'Kalau sama maka flag akan beruba menjadi True dan Loop bakal keluar'
                flag = 1

                break
            }
        }
    }
    
    'Frame yang di gunakan BTN ini kosong'
    WebUI.click(findTestObject('Account Statement/Transaction Inquiry/BTN - Button Next Page'))
}

WebUI.switchToDefaultContent()
'Validasi untuk pengecheckan Credit jika Transfer To nya Own Account'
if (GlobalVariable.Map123.get('TransferTo123') == 'Own account') {
    WebUI.callTestCase(findTestCase('Transaction Inquiry - Credit'), [:], FailureHandling.STOP_ON_FAILURE)
}

