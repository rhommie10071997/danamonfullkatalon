import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(5)

SubTitle = WebUI.getText(findTestObject('Pending Task/Balance Transfer/Inhouse/Detail/Label - Sub Title'))

WebUI.verifyMatch(SubTitle, 'Document Number Detail', true)

WebUI.delay(5)

WebUI.waitForElementVisible(findTestObject('Transaction Status/Balance Transfer - Inhouse/Document Number/Label - Transaction Status'), 
    0)

TransactionStatusTsDocNo = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - Inhouse/Document Number/Label - Transaction Status'))

TransactionStatusTsDocNo = TransactionStatusTsDocNo.replace(': ', '')

WebUI.verifyMatch(TransactionStatusTsDocNo, 'Pending Execute', true)

VersionTsDocNo = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - Inhouse/Document Number/Label - Version'))

VersionTsDocNo = VersionTsDocNo.replace(': ', '')

WebUI.verifyMatch(VersionTsDocNo, GlobalVariable.Map123.get('RefNo'), true)

TranRefNumDetailApprove = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - Inhouse/New - Ref No Detail/Label - Transaction Reference Number'))

TranRefNumDetailApprove = TranRefNumDetailApprove.replace(': ', '')

WebUI.verifyMatch(TranRefNumDetailApprove, GlobalVariable.Map123.get('RefNo'), true)

SubDateTime = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - Inhouse/New - Ref No Detail/Label - Submit Date Time'))

SubDateTime = SubDateTime.replace(': ', '')

String[] datetimesplit = SubDateTime.split(' - ')

String DateDetailApprove = datetimesplit[0]

String TimeDA = datetimesplit[1]

TimeDetailApprove = TimeDA.substring(0, TimeDA.length() - 7)

WebUI.verifyMatch(DateDetailApprove, GlobalVariable.Map123.get('Date123'), true)

WebUI.waitForElementVisible(findTestObject('Transaction Status/Balance Transfer - Inhouse/New - Ref No Detail/Label - Menu'), 
    0)

MenuDetailApprove = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - Inhouse/New - Ref No Detail/Label - Menu'))

MenuDetailApprove = MenuDetailApprove.replace(': ', '')

WebUI.verifyMatch(MenuDetailApprove, GlobalVariable.Map123.get('menu123'), true)

ProductDetailApprov = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - Inhouse/New - Ref No Detail/Label - Product'))

ProductDetailApprov = ProductDetailApprov.replace(': ', '')

WebUI.verifyMatch(ProductDetailApprov, GlobalVariable.Map123.get('product123'), false)

WebUI.waitForElementVisible(findTestObject('Transaction Status/Balance Transfer - Inhouse/New - Ref No Detail/Label - From Account'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

FromAccountCreateConfirm = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - Inhouse/New - Ref No Detail/Label - From Account'), 
    FailureHandling.CONTINUE_ON_FAILURE)

FromAccountCreateConfirm = FromAccountCreateConfirm.replace(': ', '')

WebUI.verifyMatch(FromAccountCreateConfirm, GlobalVariable.Map123.get('FromAccount123'), false, FailureHandling.CONTINUE_ON_FAILURE)

FAD = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - Inhouse/New - Ref No Detail/Label - From Account Description'), 
    FailureHandling.CONTINUE_ON_FAILURE)

FromAccountDescriptionCreateConfirm = FAD.replace(': ', '')

WebUI.verifyMatch(FromAccountDescriptionCreateConfirm, GlobalVariable.Map123.get('FromAccountDescription123'), true, FailureHandling.CONTINUE_ON_FAILURE)

TT = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - Inhouse/New - Ref No Detail/Label - Transfer To'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TransferToCreateConfirm = TT.replace(': ', '')

WebUI.verifyMatch(TransferToCreateConfirm, GlobalVariable.Map123.get('TransferTo123'), true, FailureHandling.CONTINUE_ON_FAILURE)

AccountNumberDetailScreen = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - Inhouse/New - Ref No Detail/Label - Account Number'), 
    FailureHandling.CONTINUE_ON_FAILURE)

AccountNumberDetailScreen = AccountNumberDetailScreen.replace(': ', '')

WebUI.verifyMatch(AccountNumberDetailScreen, GlobalVariable.Map123.get('AccountNumber123'), true, FailureHandling.CONTINUE_ON_FAILURE)

if (GlobalVariable.Map123.get('TransferToFlag123') == 0) {
    AccNameConfirmScreen = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - Inhouse/New - Ref No Detail/Label - Account Name'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    AccNameConfirmScreen = AccNameConfirmScreen.replace(': ', '')

    WebUI.verifyMatch(AccNameConfirmScreen, GlobalVariable.Map123.get('AccountName123'), false, FailureHandling.CONTINUE_ON_FAILURE)

    BenefNotifEmailCreateConfirm = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - Inhouse/New - Ref No Detail/Label - Beneficiary notification Phone'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.verifyMatch(BenefNotifEmailCreateConfirm, GlobalVariable.Map123.get('email123'), true, FailureHandling.CONTINUE_ON_FAILURE)

    BenefNotifPhoneCreateConfirm = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - Inhouse/New - Ref No Detail/Label - Beneficiary notification Email'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.verifyMatch(BenefNotifPhoneCreateConfirm, GlobalVariable.Map123.get('phone123'), true, FailureHandling.CONTINUE_ON_FAILURE)
} else if (GlobalVariable.Map123.get('TransferToFlag123') == 1) {
    AccNameConfirmScreen = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - Inhouse/New - Ref No Detail/Label - Account Name'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    AccNameConfirmScreen = AccNameConfirmScreen.replace(': ', '')

    WebUI.verifyMatch(AccNameConfirmScreen, GlobalVariable.Map123.get('AccountName123'), false, FailureHandling.CONTINUE_ON_FAILURE)
} else {
    SaveToBenefList = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - Inhouse/New - Ref No Detail/Label - Save to Beneficiary List'))

    WebUI.verifyMatch(SaveToBenefList, GlobalVariable.Map123.get('SaveToBenefList123'), false)

    AliasName = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - Inhouse/New - Ref No Detail/Label - Alias Name'))

    AliasName = AliasName.replace(': ', '')

    AliasName = AliasName.replace(':', '')

    WebUI.verifyMatch(AliasName, GlobalVariable.Map123.get('AliasName123'), false, FailureHandling.CONTINUE_ON_FAILURE)

    BenefNotifEmailCreateConfirm = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - Inhouse/New - Ref No Detail/Label - Beneficiary notification Phone'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.verifyMatch(BenefNotifEmailCreateConfirm, GlobalVariable.Map123.get('email123'), true, FailureHandling.CONTINUE_ON_FAILURE)

    BenefNotifPhoneCreateConfirm = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - Inhouse/New - Ref No Detail/Label - Beneficiary notification Email'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.verifyMatch(BenefNotifPhoneCreateConfirm, GlobalVariable.Map123.get('phone123'), true, FailureHandling.CONTINUE_ON_FAILURE)
}

ToAccDescCreateConfirm = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - Inhouse/New - Ref No Detail/Label - To Account Description'), 
    FailureHandling.CONTINUE_ON_FAILURE)

ToAccDescCreateConfirm = ToAccDescCreateConfirm.replace(': ', '')

WebUI.verifyMatch(ToAccDescCreateConfirm, GlobalVariable.Map123.get('FromAccountDescription123'), true, FailureHandling.CONTINUE_ON_FAILURE)

BenefRefNoCreateConfirm = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - Inhouse/New - Ref No Detail/Label - Beneficiary Reference Number'), 
    FailureHandling.CONTINUE_ON_FAILURE)

BenefRefNoCreateConfirm = BenefRefNoCreateConfirm.replace(': ', '')

WebUI.verifyMatch(BenefRefNoCreateConfirm, GlobalVariable.Map123.get('BenefRefNum123'), true, FailureHandling.CONTINUE_ON_FAILURE)

RetainAmountCreateConfirm = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - Inhouse/New - Ref No Detail/Label - Retain Amount'), 
    FailureHandling.CONTINUE_ON_FAILURE)

RetainAmountCreateConfirm = RetainAmountCreateConfirm.replace(': ', '')

WebUI.verifyMatch(RetainAmountCreateConfirm, GlobalVariable.Map123.get('retainamount123'), true, FailureHandling.CONTINUE_ON_FAILURE)

ER12 = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - Inhouse/New - Ref No Detail/Label - Exchange Rate'), 
    FailureHandling.CONTINUE_ON_FAILURE)

ExhangeRateCreateConfirm = ER12.replace(': ', '')

WebUI.verifyMatch(ExhangeRateCreateConfirm, GlobalVariable.Map123.get('ExchangeRate123'), true, FailureHandling.CONTINUE_ON_FAILURE)

TC12 = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - Inhouse/New - Ref No Detail/Label - Total Charge'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TotalChargeCreateConfirm = TC12.replace(': ', '')

WebUI.verifyMatch(TotalChargeCreateConfirm, GlobalVariable.Map123.get('TotalCharge123'), true)

TDA12 = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - Inhouse/New - Ref No Detail/Label - Total Debet Amount'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TotalDebetAmountCreateConfirm = TDA12.replace(': ', '')

WebUI.verifyMatch(TotalDebetAmountCreateConfirm, GlobalVariable.Map123.get('TotalDebetAmount123'), true)

CI12 = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - Inhouse/New - Ref No Detail/Label - Charge Instruction'), 
    FailureHandling.CONTINUE_ON_FAILURE)

ChargeInstructionCreateConfirm = CI12.replace(': ', '')

WebUI.verifyMatch(ChargeInstructionCreateConfirm, GlobalVariable.Map123.get('ChargeInstruction123'), true, FailureHandling.CONTINUE_ON_FAILURE)

DC12 = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - Inhouse/New - Ref No Detail/Label - Debit Charge'), 
    FailureHandling.CONTINUE_ON_FAILURE)

DebitChargeCreateConfirm = DC12.replace(': ', '')

WebUI.verifyMatch(DebitChargeCreateConfirm, GlobalVariable.Map123.get('DebitCharge123'), true, FailureHandling.CONTINUE_ON_FAILURE)

LHBUPC = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - Inhouse/New - Ref No Detail/Label - LHBU Purpose Code'), 
    FailureHandling.CONTINUE_ON_FAILURE)

LHBUPCCreateConfirm = LHBUPC.replace(': ', '')

WebUI.verifyMatch(LHBUPCCreateConfirm, GlobalVariable.Map123.get('LHBUPurposeCode123'), true, FailureHandling.CONTINUE_ON_FAILURE)

LHBUDT = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - Inhouse/New - Ref No Detail/Label - LHBU Document Type'), 
    FailureHandling.CONTINUE_ON_FAILURE)

LHBUDTCreateConfirm = LHBUDT.replace(': ', '')

WebUI.verifyMatch(LHBUDTCreateConfirm, GlobalVariable.Map123.get('LHBUDocumentType123'), true, FailureHandling.CONTINUE_ON_FAILURE)

LHBUDTD = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - Inhouse/New - Ref No Detail/Label - LHBU Document Type Description'), 
    FailureHandling.CONTINUE_ON_FAILURE)

LHBUDTDCreateConfirm = LHBUDTD.replace(': ', '')

WebUI.verifyMatch(LHBUDTDCreateConfirm, GlobalVariable.Map123.get('LHBUDocumentTypeDescription123'), true, FailureHandling.CONTINUE_ON_FAILURE)

IM12 = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - Inhouse/New - Ref No Detail/Label - Instruction Mode'), 
    FailureHandling.CONTINUE_ON_FAILURE)

InstructionModeCreateConfirm = IM12.replace(': ', '')

WebUI.verifyMatch(InstructionModeCreateConfirm, GlobalVariable.Map123.get('InstructionMode123'), false)

FutureDate = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - Inhouse/New - Ref No Detail/Label - Specific Date - Future Date'))

FutureDate = FutureDate.replace(': ', '')

WebUI.verifyMatch(FutureDate, GlobalVariable.Map123.get('FutureDate'), false)

SpecificAt = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - Inhouse/New - Ref No Detail/Label - Specific Date - At'))

SpecificAt = SpecificAt.replace(': ', '')

WebUI.verifyMatch(SpecificAt, GlobalVariable.Map123.get('SpecificAt'), false)

ExpOn = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - Inhouse/New - Ref No Detail/Label - Expired On'), 
    FailureHandling.CONTINUE_ON_FAILURE)

ExpOnCreateConfirm = ExpOn.replace(': ', '')

WebUI.verifyMatch(ExpOnCreateConfirm, GlobalVariable.Map123.get('ExpiredOn123'), false)

WebUI.delay(2)

WebUI.click(findTestObject('5 - Log Out btn'))

