import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

SubTitle = WebUI.getText(findTestObject('Pending Task/Balance Transfer/Inhouse/Detail/Label - Sub Title'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(SubTitle, 'Reference Number Detail', true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Menu'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Menu = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Menu'), FailureHandling.CONTINUE_ON_FAILURE)

Menu = Menu.replace('    : ', '')

WebUI.verifyMatch(Menu, GlobalVariable.Map123.get('Menu'), true, FailureHandling.CONTINUE_ON_FAILURE)

Product = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Product'), FailureHandling.CONTINUE_ON_FAILURE)

Product = Product.replace('    : ', '')

WebUI.verifyMatch(Product, GlobalVariable.Map123.get('Product'), true, FailureHandling.CONTINUE_ON_FAILURE)

TransactionReferenceNo = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Transaction Reference No'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TransactionReferenceNo = TransactionReferenceNo.replace('    : ', '')

WebUI.verifyMatch(TransactionReferenceNo, GlobalVariable.Map123.get('RefNo'), false, FailureHandling.CONTINUE_ON_FAILURE)

DocumentCode = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Document Code'), 
    FailureHandling.CONTINUE_ON_FAILURE)

DocumentCode = DocumentCode.replace('    : ', '')

WebUI.verifyMatch(DocumentCode, GlobalVariable.Map123.get('RefNo'), false, FailureHandling.CONTINUE_ON_FAILURE)

FromAccount = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - From Account'), 
    FailureHandling.CONTINUE_ON_FAILURE)

FromAccount = FromAccount.replace(': ', '')

WebUI.verifyMatch(FromAccount, GlobalVariable.Map123.get('FromAccount'), false, FailureHandling.CONTINUE_ON_FAILURE)

FromAccountDescription = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - From Account Description'), 
    FailureHandling.CONTINUE_ON_FAILURE)

FromAccountDescription = FromAccountDescription.replace(': ', '')

WebUI.verifyMatch(FromAccountDescription, GlobalVariable.Map123.get('FromAccountDescription'), false, FailureHandling.CONTINUE_ON_FAILURE)

TransferTo = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Transfer To'), FailureHandling.CONTINUE_ON_FAILURE)

TransferTo = TransferTo.replace(': ', '')

WebUI.verifyMatch(TransferTo, GlobalVariable.Map123.get('TransferTo'), false, FailureHandling.CONTINUE_ON_FAILURE)

if (GlobalVariable.Map123.get('TransferTo') == 'Beneficiary List') {
    BeneficiaryList = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Beneficiary List'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    BeneficiaryList = BeneficiaryList.replace(': ', '')

    WebUI.verifyMatch(BeneficiaryList, GlobalVariable.Map123.get('BeneficiaryList'), false, FailureHandling.CONTINUE_ON_FAILURE)

    BeneficiaryBank = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Beneficiary Bank'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    BeneficiaryBank = BeneficiaryBank.replace(': ', '')

    BeneficiaryBank = BeneficiaryBank.replace(':', '')

    WebUI.verifyMatch(BeneficiaryBank, GlobalVariable.Map123.get('BeneficiaryBank'), false, FailureHandling.CONTINUE_ON_FAILURE)

    BankBranch = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Bank Branch'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    BankBranch = BankBranch.replace(': ', '')

    BankBranch = BankBranch.replace(':', '')

    WebUI.verifyMatch(BankBranch, GlobalVariable.Map123.get('BankBranch'), false, FailureHandling.CONTINUE_ON_FAILURE)

    BankCity = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Bank City'), FailureHandling.CONTINUE_ON_FAILURE)

    BankCity = BankCity.replace(': ', '')

    BankCity = BankCity.replace(':', '')

    WebUI.verifyMatch(BankCity, GlobalVariable.Map123.get('BankCity'), false, FailureHandling.CONTINUE_ON_FAILURE)

    AccountNumber = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Account Number'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    AccountNumber = AccountNumber.replace(': ', '')

    AccountNumber = AccountNumber.replace(':', '')

    WebUI.verifyMatch(AccountNumber, GlobalVariable.Map123.get('AccountNumber'), false, FailureHandling.CONTINUE_ON_FAILURE)

    AccountName = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Account Name'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    AccountName = AccountName.replace(': ', '')

    AccountName = AccountName.replace(':', '')

    WebUI.verifyMatch(AccountName, GlobalVariable.Map123.get('AccountName'), false, FailureHandling.CONTINUE_ON_FAILURE)

    Address1 = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Address 1'), FailureHandling.CONTINUE_ON_FAILURE)

    Address1 = Address1.replace(': ', '')

    Address1 = Address1.replace(':', '')

    WebUI.verifyMatch(Address1, GlobalVariable.Map123.get('Address1'), false, FailureHandling.CONTINUE_ON_FAILURE)

    Address2 = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Address 2'), FailureHandling.CONTINUE_ON_FAILURE)

    Address2 = Address2.replace(': ', '')

    Address2 = Address2.replace(':', '')

    WebUI.verifyMatch(Address2, GlobalVariable.Map123.get('Address2'), false, FailureHandling.CONTINUE_ON_FAILURE)

    Address3 = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Address 3'), FailureHandling.CONTINUE_ON_FAILURE)

    Address3 = Address3.replace(': ', '')

    Address3 = Address3.replace(':', '')

    WebUI.verifyMatch(Address3, GlobalVariable.Map123.get('Address3'), false, FailureHandling.CONTINUE_ON_FAILURE)

    Email = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Email'), FailureHandling.CONTINUE_ON_FAILURE)

    Email = Email.replace(': ', '')

    Email = Email.replace(':', '')

    WebUI.verifyMatch(Email, GlobalVariable.Map123.get('Email'), false, FailureHandling.CONTINUE_ON_FAILURE)

    Phone = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Phone'), FailureHandling.CONTINUE_ON_FAILURE)

    Phone = Phone.replace(': ', '')

    Phone = Phone.replace(':', '')

    WebUI.verifyMatch(Phone, GlobalVariable.Map123.get('Phone'), false, FailureHandling.CONTINUE_ON_FAILURE)
} else {
    SavetoBeneficiaryList = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Save to Beneficiary List'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    SavetoBeneficiaryList = SavetoBeneficiaryList.replace(': ', '')

    SavetoBeneficiaryList = SavetoBeneficiaryList.replace(':', '')

    WebUI.verifyMatch(SavetoBeneficiaryList, GlobalVariable.Map123.get('SavetoBeneficiaryList'), false, FailureHandling.CONTINUE_ON_FAILURE)

    AliasName = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Alias Name'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    AliasName = AliasName.replace(': ', '')

    AliasName = AliasName.replace(':', '')

    WebUI.verifyMatch(AliasName, GlobalVariable.Map123.get('AliasName'), false, FailureHandling.CONTINUE_ON_FAILURE)

    BeneficiaryBank = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Beneficiary Bank'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    BeneficiaryBank = BeneficiaryBank.replace(': ', '')

    BeneficiaryBank = BeneficiaryBank.replace(':', '')

    WebUI.verifyMatch(BeneficiaryBank, GlobalVariable.Map123.get('BeneficiaryBank'), false, FailureHandling.CONTINUE_ON_FAILURE)

    BankBranch = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Bank Branch'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    BankBranch = BankBranch.replace(': ', '')

    BankBranch = BankBranch.replace(':', '')

    WebUI.verifyMatch(BankBranch, GlobalVariable.Map123.get('BankBranch'), false, FailureHandling.CONTINUE_ON_FAILURE)

    BankCity = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Bank City'), FailureHandling.CONTINUE_ON_FAILURE)

    BankCity = BankCity.replace(': ', '')

    BankCity = BankCity.replace(':', '')

    WebUI.verifyMatch(BankCity, GlobalVariable.Map123.get('BankCity'), false, FailureHandling.CONTINUE_ON_FAILURE)

    AccountNumber = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Account Number'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    AccountNumber = AccountNumber.replace(': ', '')

    AccountNumber = AccountNumber.replace(':', '')

    WebUI.verifyMatch(AccountNumber, GlobalVariable.Map123.get('AccountNumber'), false, FailureHandling.CONTINUE_ON_FAILURE)

    AccountName = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Account Name'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    AccountName = AccountName.replace(': ', '')

    AccountName = AccountName.replace(':', '')

    WebUI.verifyMatch(AccountName, GlobalVariable.Map123.get('AccountName'), false, FailureHandling.CONTINUE_ON_FAILURE)

    Address1 = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Address 1'), FailureHandling.CONTINUE_ON_FAILURE)

    Address1 = Address1.replace(': ', '')

    Address1 = Address1.replace(':', '')

    WebUI.verifyMatch(Address1, GlobalVariable.Map123.get('Address1'), false, FailureHandling.CONTINUE_ON_FAILURE)

    Address2 = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Address 2'), FailureHandling.CONTINUE_ON_FAILURE)

    Address2 = Address2.replace(': ', '')

    Address2 = Address2.replace(':', '')

    WebUI.verifyMatch(Address2, GlobalVariable.Map123.get('Address2'), false, FailureHandling.CONTINUE_ON_FAILURE)

    Address3 = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Address 3'), FailureHandling.CONTINUE_ON_FAILURE)

    Address3 = Address3.replace(': ', '')

    Address3 = Address3.replace(':', '')

    WebUI.verifyMatch(Address3, GlobalVariable.Map123.get('Address3'), false, FailureHandling.CONTINUE_ON_FAILURE)

    Email = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Email'), FailureHandling.CONTINUE_ON_FAILURE)

    Email = Email.replace(': ', '')

    Email = Email.replace(':', '')

    WebUI.verifyMatch(Email, GlobalVariable.Map123.get('Email'), false, FailureHandling.CONTINUE_ON_FAILURE)

    Phone = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Phone'), FailureHandling.CONTINUE_ON_FAILURE)

    Phone = Phone.replace(': ', '')

    Phone = Phone.replace(':', '')

    WebUI.verifyMatch(Phone, GlobalVariable.Map123.get('Phone'), false, FailureHandling.CONTINUE_ON_FAILURE)
}

ToAccountDescription = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - To Account Description'), 
    FailureHandling.CONTINUE_ON_FAILURE)

ToAccountDescription = ToAccountDescription.replace(': ', '')

ToAccountDescription = ToAccountDescription.replace(':', '')

WebUI.verifyMatch(ToAccountDescription, GlobalVariable.Map123.get('ToAccountDescription'), false, FailureHandling.CONTINUE_ON_FAILURE)

BeneficiaryReferenceNo = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Beneficiary Reference No'), 
    FailureHandling.CONTINUE_ON_FAILURE)

BeneficiaryReferenceNo = BeneficiaryReferenceNo.replace(': ', '')

BeneficiaryReferenceNo = BeneficiaryReferenceNo.replace(':', '')

WebUI.verifyMatch(BeneficiaryReferenceNo, GlobalVariable.Map123.get('BeneficiaryReferenceNo'), false, FailureHandling.CONTINUE_ON_FAILURE)

TransferMethod = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Transfer Method'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TransferMethod = TransferMethod.replace(': ', '')

TransferMethod = TransferMethod.replace(':', '')

WebUI.verifyMatch(TransferMethod, GlobalVariable.Map123.get('TransferMethod'), false, FailureHandling.CONTINUE_ON_FAILURE)

RetainedAmount = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Retained Amount'), 
    FailureHandling.CONTINUE_ON_FAILURE)

RetainedAmount = RetainedAmount.replace(': ', '')

RetainedAmount = RetainedAmount.replace(':', '')

WebUI.verifyMatch(RetainedAmount, GlobalVariable.Map123.get('RetainAmount'), false, FailureHandling.CONTINUE_ON_FAILURE)

RemittanceCurrency = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Remittance Currency'), 
    FailureHandling.CONTINUE_ON_FAILURE)

RemittanceCurrency = RemittanceCurrency.replace(': ', '')

RemittanceCurrency = RemittanceCurrency.replace(':', '')

WebUI.verifyMatch(RemittanceCurrency, GlobalVariable.Map123.get('Currency'), false, FailureHandling.CONTINUE_ON_FAILURE)

ExchangeRate = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Exchange Rate'), 
    FailureHandling.CONTINUE_ON_FAILURE)

ExchangeRate = ExchangeRate.replace(': ', '')

ExchangeRate = ExchangeRate.replace(':', '')

WebUI.verifyMatch(ExchangeRate, GlobalVariable.Map123.get('ExchangeRate'), false, FailureHandling.CONTINUE_ON_FAILURE)

RTGSFee = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - RTGS Fee'), FailureHandling.CONTINUE_ON_FAILURE)

RTGSFee = RTGSFee.replace(': ', '')

RTGSFee = RTGSFee.replace(':', '')

WebUI.verifyMatch(RTGSFee, GlobalVariable.Map123.get('RTGSFee'), false, FailureHandling.CONTINUE_ON_FAILURE)

TotalDebitAmount = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Total Debit Amount'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TotalDebitAmount = TotalDebitAmount.replace(': ', '')

TotalDebitAmount = TotalDebitAmount.replace(':', '')

WebUI.verifyMatch(TotalDebitAmount, GlobalVariable.Map123.get('TotalDebitAmount'), false, FailureHandling.CONTINUE_ON_FAILURE)

ChargeInstruction = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Charge Instruction'), 
    FailureHandling.CONTINUE_ON_FAILURE)

ChargeInstruction = ChargeInstruction.replace(': ', '')

ChargeInstruction = ChargeInstruction.replace(':', '')

WebUI.verifyMatch(ChargeInstruction, GlobalVariable.Map123.get('ChargeInstruction'), false, FailureHandling.CONTINUE_ON_FAILURE)

DebitCharge = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Debit Charge'), 
    FailureHandling.CONTINUE_ON_FAILURE)

DebitCharge = DebitCharge.replace(': ', '')

DebitCharge = DebitCharge.replace(':', '')

WebUI.verifyMatch(DebitCharge, GlobalVariable.Map123.get('DebitCharge'), false, FailureHandling.CONTINUE_ON_FAILURE)

LHBUPurposeCode = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - LHBU Purpose Code'), 
    FailureHandling.CONTINUE_ON_FAILURE)

LHBUPurposeCode = LHBUPurposeCode.replace(': ', '')

LHBUPurposeCode = LHBUPurposeCode.replace(':', '')

WebUI.verifyMatch(LHBUPurposeCode, GlobalVariable.Map123.get('LHBUPurposeCode'), false, FailureHandling.CONTINUE_ON_FAILURE)

LHBUDocumentType = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - LHBU Document Type'), 
    FailureHandling.CONTINUE_ON_FAILURE)

LHBUDocumentType = LHBUDocumentType.replace(': ', '')

LHBUDocumentType = LHBUDocumentType.replace(':', '')

WebUI.verifyMatch(LHBUDocumentType, GlobalVariable.Map123.get('LHBUDocumentType'), false, FailureHandling.CONTINUE_ON_FAILURE)

LHBUDocumentTypeDescription = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - LHBU Document Type Description'), 
    FailureHandling.CONTINUE_ON_FAILURE)

LHBUDocumentTypeDescription = LHBUDocumentTypeDescription.replace(': ', '')

LHBUDocumentTypeDescription = LHBUDocumentTypeDescription.replace(':', '')

WebUI.verifyMatch(LHBUDocumentTypeDescription, GlobalVariable.Map123.get('LHBUDocumentTypeDescription'), false, FailureHandling.CONTINUE_ON_FAILURE)

InstructionMode = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Instuction Mode'), 
    FailureHandling.CONTINUE_ON_FAILURE)

InstructionMode = InstructionMode.replace(': ', '')

InstructionMode = InstructionMode.replace(':', '')

WebUI.verifyMatch(InstructionMode, GlobalVariable.Map123.get('InstructionMode'), false, FailureHandling.CONTINUE_ON_FAILURE)

RepeatOn = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Repeat - On'), FailureHandling.CONTINUE_ON_FAILURE)

RepeatOn = RepeatOn.replace(': ', '')

WebUI.verifyMatch(RepeatOn, GlobalVariable.Map123.get('RepeatOn'), false, FailureHandling.CONTINUE_ON_FAILURE)

RepeatEvery = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Repeat - Every'), 
    FailureHandling.CONTINUE_ON_FAILURE)

RepeatEvery = RepeatEvery.replace(': ', '')

WebUI.verifyMatch(RepeatEvery, GlobalVariable.Map123.get('RepeatEvery'), false, FailureHandling.CONTINUE_ON_FAILURE)

RepeatAt = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Repeat - At'), FailureHandling.CONTINUE_ON_FAILURE)

RepeatAt = RepeatAt.replace(': ', '')

WebUI.verifyMatch(RepeatAt, GlobalVariable.Map123.get('RepeatAt'), false, FailureHandling.CONTINUE_ON_FAILURE)

RepeatStart = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Repeat - Start'), 
    FailureHandling.CONTINUE_ON_FAILURE)

RepeatStart = RepeatStart.replace(': ', '')

WebUI.verifyMatch(RepeatStart, GlobalVariable.Map123.get('RepeatStart'), false, FailureHandling.CONTINUE_ON_FAILURE)

RepeatEnd = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Repeat - End'), FailureHandling.CONTINUE_ON_FAILURE)

RepeatEnd = RepeatEnd.replace(': ', '')

WebUI.verifyMatch(RepeatEnd, GlobalVariable.Map123.get('RepeatEnd'), false, FailureHandling.CONTINUE_ON_FAILURE)

InstructionOnHoliday = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Repeat - Instruction on Holiday'), 
    FailureHandling.CONTINUE_ON_FAILURE)

InstructionOnHoliday = InstructionOnHoliday.replace(': ', '')

WebUI.verifyMatch(InstructionOnHoliday, GlobalVariable.Map123.get('NonWorkingDayInstruction'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.scrollToElement(findTestObject('Transaction Status/Balance Transfer - Inhouse/New - Ref No Detail/BTN - Document Number'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Transaction Status/Balance Transfer - Inhouse/New - Ref No Detail/BTN - Document Number'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

