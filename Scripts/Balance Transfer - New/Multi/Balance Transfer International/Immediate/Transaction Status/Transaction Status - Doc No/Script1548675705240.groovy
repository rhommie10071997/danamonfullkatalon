import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

SubTitle = WebUI.getText(findTestObject('Pending Task/Balance Transfer/Inhouse/Detail/Label - Sub Title'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(SubTitle, 'Document Number Detail', true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - Transaction Status'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

TransactionStatusTsDocNo = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - Transaction Status'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TransactionStatusTsDocNo = TransactionStatusTsDocNo.replace(': ', '')

WebUI.verifyMatch(TransactionStatusTsDocNo, 'Executed Successfully', true, FailureHandling.CONTINUE_ON_FAILURE)

Menu = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - Menu'), FailureHandling.CONTINUE_ON_FAILURE)

Menu = Menu.replace(': ', '')

WebUI.verifyMatch(Menu, GlobalVariable.Map123.get('Menu'), false, FailureHandling.CONTINUE_ON_FAILURE)

Product = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - Product'), 
    FailureHandling.CONTINUE_ON_FAILURE)

Product = Product.replace(': ', '')

WebUI.verifyMatch(Product, GlobalVariable.Map123.get('Product'), false, FailureHandling.CONTINUE_ON_FAILURE)

TransactionReferenceNumber = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - Transaction Reference Number'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TransactionReferenceNumber = TransactionReferenceNumber.replace(': ', '')

WebUI.verifyMatch(TransactionReferenceNumber, GlobalVariable.Map123.get('RefNo'), false, FailureHandling.CONTINUE_ON_FAILURE)

DocumentCode = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - Document Code'), 
    FailureHandling.CONTINUE_ON_FAILURE)

DocumentCode = DocumentCode.replace(': ', '')

WebUI.verifyMatch(DocumentCode, GlobalVariable.Map123.get('RefNo'), false, FailureHandling.CONTINUE_ON_FAILURE)

TransferFrom = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - Transfer From'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TransferFrom = TransferFrom.replace(': ', '')

WebUI.verifyMatch(TransferFrom, GlobalVariable.Map123.get('TransferFrom'), false, FailureHandling.CONTINUE_ON_FAILURE)

FromAccountDescription = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - From Account Description'), 
    FailureHandling.CONTINUE_ON_FAILURE)

FromAccountDescription = FromAccountDescription.replace(': ', '')

WebUI.verifyMatch(FromAccountDescription, GlobalVariable.Map123.get('FromAccountDescription'), false, FailureHandling.CONTINUE_ON_FAILURE)

TransferTo = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - Transfer To'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TransferTo = TransferTo.replace(': ', '')

WebUI.verifyMatch(TransferTo, GlobalVariable.Map123.get('TransferTo'), false, FailureHandling.CONTINUE_ON_FAILURE)

BeneficiaryBank = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - Beneficiary Bank'), 
    FailureHandling.CONTINUE_ON_FAILURE)

BeneficiaryBank = BeneficiaryBank.replace(': ', '')

WebUI.verifyMatch(BeneficiaryBank, GlobalVariable.Map123.get('BeneficiaryBank'), false, FailureHandling.CONTINUE_ON_FAILURE)

RetainedAmount = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - Retained Amount'), 
    FailureHandling.CONTINUE_ON_FAILURE)

RetainedAmount = RetainedAmount.replace(': ', '')

WebUI.verifyMatch(RetainedAmount, GlobalVariable.Map123.get('RetainedAmount'), false, FailureHandling.CONTINUE_ON_FAILURE)

RemittanceCurrency = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - Remittance Currency'), 
    FailureHandling.CONTINUE_ON_FAILURE)

RemittanceCurrency = RemittanceCurrency.replace(': ', '')

WebUI.verifyMatch(RemittanceCurrency, GlobalVariable.Map123.get('RemittanceCurrency'), false, FailureHandling.CONTINUE_ON_FAILURE)

ExchangeRate = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - Exchange Rate'), 
    FailureHandling.CONTINUE_ON_FAILURE)

ExchangeRate = ExchangeRate.replace(': ', '')

WebUI.verifyMatch(ExchangeRate, GlobalVariable.Map123.get('ExchangeRate'), false, FailureHandling.CONTINUE_ON_FAILURE)

InLieu = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - In Lieu'), 
    FailureHandling.CONTINUE_ON_FAILURE)

InLieu = InLieu.replace(': ', '')

WebUI.verifyMatch(InLieu, GlobalVariable.Map123.get('InLieu'), false, FailureHandling.CONTINUE_ON_FAILURE)

if (InLieu.substring(0, 3) == 'IDR') {
    InLieuEquivalent = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - In Lieu Equivalent'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    InLieuEquivalent = InLieuEquivalent.replace(': ', '')

    WebUI.verifyMatch(InLieuEquivalent, GlobalVariable.Map123.get('InLieuEquivalent'), false, FailureHandling.CONTINUE_ON_FAILURE)
}

Provision = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - Provision'), 
    FailureHandling.CONTINUE_ON_FAILURE)

Provision = Provision.replace(': ', '')

WebUI.verifyMatch(Provision, GlobalVariable.Map123.get('Provision'), false, FailureHandling.CONTINUE_ON_FAILURE)

if (Provision.substring(0, 3) == 'IDR') {
    ProvisionEquivalent = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - Provision Equivalent'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    ProvisionEquivalent = ProvisionEquivalent.replace(': ', '')

    WebUI.verifyMatch(ProvisionEquivalent, GlobalVariable.Map123.get('ProvisionEquivalent'), false, FailureHandling.CONTINUE_ON_FAILURE)
}

CorrespondentBankFee = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - Correspondent Bank Fee'), 
    FailureHandling.CONTINUE_ON_FAILURE)

CorrespondentBankFee = CorrespondentBankFee.replace(': ', '')

WebUI.verifyMatch(CorrespondentBankFee, GlobalVariable.Map123.get('CorrespondentBankFee'), false, FailureHandling.CONTINUE_ON_FAILURE)

if (CorrespondentBankFee.substring(0, 3) == 'IDR') {
    CorrespondentBankFeeEquivalent = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - Correspondent Bank Fee Equivalent'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    CorrespondentBankFeeEquivalent = CorrespondentBankFee.replace(': ', '')

    WebUI.verifyMatch(CorrespondentBankFeeEquivalent, GlobalVariable.Map123.get('CorrespondentBankFeeEquivalent'), false, 
        FailureHandling.CONTINUE_ON_FAILURE)
}

MinimumTransactionFee = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - Minimum Transaction Fee'), 
    FailureHandling.CONTINUE_ON_FAILURE)

MinimumTransactionFee = MinimumTransactionFee.replace(': ', '')

WebUI.verifyMatch(MinimumTransactionFee, GlobalVariable.Map123.get('MinimumTransactionFee'), false, FailureHandling.CONTINUE_ON_FAILURE)

if (MinimumTransactionFee.substring(0, 3) == 'IDR') {
    MinimumTransactionFeeEquivalent = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - Minimum Transaction Fee Equivalent'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    MinimumTransactionFeeEquivalent = MinimumTransactionFeeEquivalent.replace(': ', '')

    WebUI.verifyMatch(MinimumTransactionFeeEquivalent, GlobalVariable.Map123.get('MinimumTransactionFeeEquivalent'), false, 
        FailureHandling.CONTINUE_ON_FAILURE)
}

FullAmountCharge = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - Full Amount Charge'), 
    FailureHandling.CONTINUE_ON_FAILURE)

FullAmountCharge = FullAmountCharge.replace(': ', '')

WebUI.verifyMatch(FullAmountCharge, GlobalVariable.Map123.get('FullAmountCharge'), false, FailureHandling.CONTINUE_ON_FAILURE)

CableFee = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - Cable Fee'), 
    FailureHandling.CONTINUE_ON_FAILURE)

CableFee = CableFee.replace(': ', '')

WebUI.verifyMatch(CableFee, GlobalVariable.Map123.get('CableFee'), false, FailureHandling.CONTINUE_ON_FAILURE)

if (CableFee.substring(0, 3) == 'IDR') {
    CableFeeEquivalent = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - Cable Fee Equivalent'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    CableFeeEquivalent = CableFeeEquivalent.replace(': ', '')

    WebUI.verifyMatch(CableFeeEquivalent, GlobalVariable.Map123.get('CableFeeEquivalent'), false, FailureHandling.CONTINUE_ON_FAILURE)
}

TotalCharge = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - Total Charge'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TotalCharge = TotalCharge.replace(': ', '')

WebUI.verifyMatch(TotalCharge, GlobalVariable.Map123.get('TotalCharge'), false, FailureHandling.CONTINUE_ON_FAILURE)

ChargetoRemitter = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - Charge to Remitter'), 
    FailureHandling.CONTINUE_ON_FAILURE)

ChargetoRemitter = ChargetoRemitter.replace(': ', '')

WebUI.verifyMatch(ChargetoRemitter, GlobalVariable.Map123.get('ChargetoRemitter'), false, FailureHandling.CONTINUE_ON_FAILURE)

ChargetoBeneficiary = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - Charge to Beneficiary'), 
    FailureHandling.CONTINUE_ON_FAILURE)

ChargetoBeneficiary = ChargetoBeneficiary.replace(': ', '')

WebUI.verifyMatch(ChargetoBeneficiary, GlobalVariable.Map123.get('ChargetoBeneficiary'), false, FailureHandling.CONTINUE_ON_FAILURE)

TotalDebitAmount = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - Total Debit Amount'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TotalDebitAmount = TotalDebitAmount.replace(': ', '')

WebUI.verifyMatch(TotalDebitAmount, GlobalVariable.Map123.get('TotalDebitAmount'), false, FailureHandling.CONTINUE_ON_FAILURE)

ChargeInstruction = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - Charge Instruction'), 
    FailureHandling.CONTINUE_ON_FAILURE)

ChargeInstruction = ChargeInstruction.replace(': ', '')

ChargeInstruction = ChargeInstruction.replace(':', '')

WebUI.verifyMatch(ChargeInstruction, GlobalVariable.Map123.get('ChargeInstruction'), false, FailureHandling.CONTINUE_ON_FAILURE)

DebitCharge = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - Debit Charge'), 
    FailureHandling.CONTINUE_ON_FAILURE)

DebitCharge = DebitCharge.replace(': ', '')

DebitCharge = DebitCharge.replace(':', '')

WebUI.verifyMatch(DebitCharge, GlobalVariable.Map123.get('DebitCharge'), false, FailureHandling.CONTINUE_ON_FAILURE)

ToAccountDescription = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - To Account Description'), 
    FailureHandling.CONTINUE_ON_FAILURE)

ToAccountDescription = ToAccountDescription.replace(': ', '')

ToAccountDescription = ToAccountDescription.replace(':', '')

WebUI.verifyMatch(ToAccountDescription, GlobalVariable.Map123.get('ToAccountDescription'), false, FailureHandling.CONTINUE_ON_FAILURE)

BeneficiaryReferenceNo = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - Beneficiary Reference No'), 
    FailureHandling.CONTINUE_ON_FAILURE)

BeneficiaryReferenceNo = BeneficiaryReferenceNo.replace(': ', '')

BeneficiaryReferenceNo = BeneficiaryReferenceNo.replace(':', '')

WebUI.verifyMatch(BeneficiaryReferenceNo, GlobalVariable.Map123.get('BeneficiaryReferenceNo'), false, FailureHandling.CONTINUE_ON_FAILURE)

BeneficiaryResidentStatus = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - Beneficiary Resident Status'), 
    FailureHandling.CONTINUE_ON_FAILURE)

BeneficiaryResidentStatus = BeneficiaryResidentStatus.replace(': ', '')

BeneficiaryResidentStatus = BeneficiaryResidentStatus.replace(':', '')

WebUI.verifyMatch(BeneficiaryResidentStatus, GlobalVariable.Map123.get('BeneficiaryResidentStatus'), false, FailureHandling.CONTINUE_ON_FAILURE)

BeneficiaryCategory = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - Beneficiary Category'), 
    FailureHandling.CONTINUE_ON_FAILURE)

BeneficiaryCategory = BeneficiaryCategory.replace(': ', '')

BeneficiaryCategory = BeneficiaryCategory.replace(':', '')

WebUI.verifyMatch(BeneficiaryCategory, GlobalVariable.Map123.get('BeneficiaryCategory'), false, FailureHandling.CONTINUE_ON_FAILURE)

TransactorRelationship = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - Transactor Relationship'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TransactorRelationship = TransactorRelationship.replace(': ', '')

TransactorRelationship = TransactorRelationship.replace(':', '')

WebUI.verifyMatch(TransactorRelationship, GlobalVariable.Map123.get('TransactorRelationship'), false, FailureHandling.CONTINUE_ON_FAILURE)

IdenticalStatus = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - Identical Status'), 
    FailureHandling.CONTINUE_ON_FAILURE)

IdenticalStatus = IdenticalStatus.replace(': ', '')

IdenticalStatus = IdenticalStatus.replace(':', '')

WebUI.verifyMatch(IdenticalStatus, GlobalVariable.Map123.get('IdenticalStatus'), false, FailureHandling.CONTINUE_ON_FAILURE)

PurposeOfTransaction = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - Purpose Of Transaction'), 
    FailureHandling.CONTINUE_ON_FAILURE)

PurposeOfTransaction = PurposeOfTransaction.replace(': ', '')

PurposeOfTransaction = PurposeOfTransaction.replace(':', '')

WebUI.verifyMatch(PurposeOfTransaction, GlobalVariable.Map123.get('PurposeOfTransaction'), false, FailureHandling.CONTINUE_ON_FAILURE)

LHBUPurposeCode = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - LHBU Purpose Code'), 
    FailureHandling.CONTINUE_ON_FAILURE)

LHBUPurposeCode = LHBUPurposeCode.replace(': ', '')

LHBUPurposeCode = LHBUPurposeCode.replace(':', '')

WebUI.verifyMatch(LHBUPurposeCode, GlobalVariable.Map123.get('LHBUPurposeCode'), false, FailureHandling.CONTINUE_ON_FAILURE)

LHBUDocumentType = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - LHBU Document Type'), 
    FailureHandling.CONTINUE_ON_FAILURE)

LHBUDocumentType = LHBUDocumentType.replace(': ', '')

LHBUDocumentType = LHBUDocumentType.replace(':', '')

WebUI.verifyMatch(LHBUDocumentType, GlobalVariable.Map123.get('LHBUDocumentType'), false, FailureHandling.CONTINUE_ON_FAILURE)

LHBUDocumentType = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - LHBU Document Type'), 
    FailureHandling.CONTINUE_ON_FAILURE)

LHBUDocumentType = LHBUDocumentType.replace(': ', '')

LHBUDocumentType = LHBUDocumentType.replace(':', '')

WebUI.verifyMatch(LHBUDocumentType, GlobalVariable.Map123.get('LHBUDocumentType'), false, FailureHandling.CONTINUE_ON_FAILURE)

LHBUDocumentTypeDescription = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - LHBU Document Type Description'), 
    FailureHandling.CONTINUE_ON_FAILURE)

LHBUDocumentTypeDescription = LHBUDocumentTypeDescription.replace(': ', '')

LHBUDocumentTypeDescription = LHBUDocumentTypeDescription.replace(':', '')

WebUI.verifyMatch(LHBUDocumentTypeDescription, GlobalVariable.Map123.get('LHBUDocumentTypeDescription'), false, FailureHandling.CONTINUE_ON_FAILURE)

InstructionMode = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - Instruction Mode'), 
    FailureHandling.CONTINUE_ON_FAILURE)

InstructionMode = InstructionMode.replace(': ', '')

InstructionMode = InstructionMode.replace(':', '')

WebUI.verifyMatch(InstructionMode, GlobalVariable.Map123.get('InstructionMode'), false, FailureHandling.CONTINUE_ON_FAILURE)

Expiredon = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - Expired on'), 
    FailureHandling.CONTINUE_ON_FAILURE)

Expiredon = Expiredon.replace(': ', '')

Expiredon = Expiredon.replace(':', '')

WebUI.verifyMatch(Expiredon, GlobalVariable.Map123.get('Expiredon'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('5 - Log Out btn'))

