import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.waitForElementVisible(findTestObject('Balance Transfer/Balance Transfer - International New/Confirm/Label - Sub Label'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

SubLabel = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - International New/Confirm/Label - Sub Label'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(SubLabel, 'Confirm', false, FailureHandling.CONTINUE_ON_FAILURE)

TransferFrom = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - International New/Confirm/Label - Transfer From'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TransferFrom = TransferFrom.replace(': ', '')

WebUI.verifyMatch(TransferFrom, GlobalVariable.Map123.get('TransferFrom'), false, FailureHandling.CONTINUE_ON_FAILURE)

FromAccountDescription = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - International New/Confirm/Label - From Account Description'), 
    FailureHandling.CONTINUE_ON_FAILURE)

FromAccountDescription = FromAccountDescription.replace(': ', '')

WebUI.verifyMatch(FromAccountDescription, GlobalVariable.Map123.get('FromAccountDescription'), false, FailureHandling.CONTINUE_ON_FAILURE)

TransferTo = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - International New/Confirm/Label - Transfer To'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TransferTo = TransferTo.replace(': ', '')

if (GlobalVariable.Map123.get('TransferToFlag123') == 0) {
    WebUI.verifyMatch(TransferTo, GlobalVariable.Map123.get('BeneficiaryList'), false)

    GlobalVariable.Map123.put('TransferTo', TransferTo)

    BeneficiaryBank = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - International New/Confirm/Label - Beneficiary Bank'))

    BeneficiaryBank = BeneficiaryBank.replace(': ', '')

    GlobalVariable.Map123.put('BeneficiaryBank', BeneficiaryBank)
} else {
    WebUI.verifyMatch(TransferTo, (GlobalVariable.Map123.get('AccountNumber') + ' - ') + GlobalVariable.Map123.get('AccountName'), 
        false)

    GlobalVariable.Map123.put('TransferTo', TransferTo)

    BeneficiaryBank = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - International New/Confirm/Label - Beneficiary Bank'))

    BeneficiaryBank = BeneficiaryBank.replace(': ', '')

    WebUI.verifyMatch(BeneficiaryBank, GlobalVariable.Map123.get('BeneficiaryBank'), false)
}

RetainedAmount = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - International New/Confirm/Label - Retained Amount'), 
    FailureHandling.CONTINUE_ON_FAILURE)

RetainedAmount = RetainedAmount.replace(': ', '')

WebUI.verifyMatch(RetainedAmount, GlobalVariable.Map123.get('RetainedAmount'), false, FailureHandling.CONTINUE_ON_FAILURE)

RemittanceCurrency = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - International New/Confirm/Label - Remittance Currency'), 
    FailureHandling.CONTINUE_ON_FAILURE)

RemittanceCurrency = RemittanceCurrency.replace(': ', '')

WebUI.verifyMatch(RemittanceCurrency, GlobalVariable.Map123.get('RemittanceCurrency'), false, FailureHandling.CONTINUE_ON_FAILURE)

ExchangeRate = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - International New/Confirm/Label - Exchange Rate'), 
    FailureHandling.CONTINUE_ON_FAILURE)

ExchangeRate = ExchangeRate.replace(': ', '')

GlobalVariable.Map123.put('ExchangeRate', 'Counter Rate')

WebUI.verifyMatch(ExchangeRate, GlobalVariable.Map123.get('ExchangeRate'), false, FailureHandling.CONTINUE_ON_FAILURE)

InLieu = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - International New/Confirm/Label - In Lieu'), 
    FailureHandling.CONTINUE_ON_FAILURE)

InLieu = InLieu.replace(': ', '')

GlobalVariable.Map123.put('InLieu', InLieu)

if (InLieu.substring(0, 3) == 'IDR') {
    InLieuEquivalent = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - International New/Confirm/Label - In Lieu Equivalent'))

    InLieuEquivalent = InLieuEquivalent.replace(': ', '')

    GlobalVariable.Map123.put('InLieuEquivalent', InLieuEquivalent)
}

Provision = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - International New/Confirm/Label - Provision'), 
    FailureHandling.CONTINUE_ON_FAILURE)

Provision = Provision.replace(': ', '')

GlobalVariable.Map123.put('Provision', Provision)

if (Provision.substring(0, 3) == 'IDR') {
    ProvisionEquivalent = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - International New/Confirm/Label - Provision Equivalent'))

    ProvisionEquivalent = ProvisionEquivalent.replace(': ', '')

    GlobalVariable.Map123.put('ProvisionEquivalent', ProvisionEquivalent)
}

CorrespondentBankFee = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - International New/Confirm/Label - Correspondent Bank Fee'), 
    FailureHandling.CONTINUE_ON_FAILURE)

CorrespondentBankFee = CorrespondentBankFee.replace(': ', '')

GlobalVariable.Map123.put('CorrespondentBankFee', CorrespondentBankFee)

if (CorrespondentBankFee.substring(0, 3) == 'IDR') {
    CorrespondentBankFeeEquivalent = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - International New/Confirm/Label - Correspondent Bank Fee Equivalent'))

    CorrespondentBankFeeEquivalent = CorrespondentBankFee.replace(': ', '')

    GlobalVariable.Map123.put('CorrespondentBankFeeEquivalent', CorrespondentBankFeeEquivalent)
}

MinimumTransactionFee = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - International New/Confirm/Label - Minimum Transaction Fee'), 
    FailureHandling.CONTINUE_ON_FAILURE)

MinimumTransactionFee = MinimumTransactionFee.replace(': ', '')

GlobalVariable.Map123.put('MinimumTransactionFee', MinimumTransactionFee)

if (MinimumTransactionFee.substring(0, 3) == 'IDR') {
    MinimumTransactionFeeEquivalent = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - International New/Confirm/Label - Minimum Transaction Fee Equivalent'))

    MinimumTransactionFeeEquivalent = MinimumTransactionFeeEquivalent.replace(': ', '')

    GlobalVariable.Map123.put('MinimumTransactionFeeEquivalent', MinimumTransactionFeeEquivalent)
}

FullAmountCharge = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - International New/Confirm/Label - Full Amount Charge'), 
    FailureHandling.CONTINUE_ON_FAILURE)

FullAmountCharge = FullAmountCharge.replace(': ', '')

GlobalVariable.Map123.put('FullAmountCharge', FullAmountCharge)

CableFee = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - International New/Confirm/Label - Cable Fee'), 
    FailureHandling.CONTINUE_ON_FAILURE)

CableFee = CableFee.replace(': ', '')

GlobalVariable.Map123.put('CableFee', CableFee)

if (CableFee.substring(0, 3) == 'IDR') {
    CableFeeEquivalent = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - International New/Confirm/Label - Cable Fee Equivalent'))

    CableFeeEquivalent = CableFeeEquivalent.replace(': ', '')

    GlobalVariable.Map123.put('CableFeeEquivalent', CableFeeEquivalent)
}

TotalCharge = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - International New/Confirm/Label - Total Charge'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TotalCharge = TotalCharge.replace(': ', '')

GlobalVariable.Map123.put('TotalCharge', TotalCharge)

ChargetoRemitter = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - International New/Confirm/Label - Charge to Remitter'), 
    FailureHandling.CONTINUE_ON_FAILURE)

ChargetoRemitter = ChargetoRemitter.replace(': ', '')

GlobalVariable.Map123.put('ChargetoRemitter', ChargetoRemitter)

ChargetoBeneficiary = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - International New/Confirm/Label - Charge to Beneficiary'), 
    FailureHandling.CONTINUE_ON_FAILURE)

ChargetoBeneficiary = ChargetoBeneficiary.replace(': ', '')

GlobalVariable.Map123.put('ChargetoBeneficiary', ChargetoBeneficiary)

TotalDebitAmount = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - International New/Confirm/Label - Total Debit Amount'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TotalDebitAmount = TotalDebitAmount.replace(': ', '')

GlobalVariable.Map123.put('TotalDebitAmount', TotalDebitAmount)

ChargeInstruction = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - International New/Confirm/Label - Charge Instruction'), 
    FailureHandling.CONTINUE_ON_FAILURE)

ChargeInstruction = ChargeInstruction.replace(': ', '')

ChargeInstruction = ChargeInstruction.replace(':', '')

WebUI.verifyMatch(ChargeInstruction, 'Remitter', false, FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Map123.put('ChargeInstruction', ChargeInstruction)

DebitCharge = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - International New/Confirm/Label - Debit Charge'), 
    FailureHandling.CONTINUE_ON_FAILURE)

DebitCharge = DebitCharge.replace(': ', '')

DebitCharge = DebitCharge.replace(':', '')

WebUI.verifyMatch(DebitCharge, 'Combine', false, FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Map123.put('DebitCharge', DebitCharge)

ToAccountDescription = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - International New/Confirm/Label - To Account Description'), 
    FailureHandling.CONTINUE_ON_FAILURE)

ToAccountDescription = ToAccountDescription.replace(': ', '')

ToAccountDescription = ToAccountDescription.replace(':', '')

WebUI.verifyMatch(ToAccountDescription, GlobalVariable.Map123.get('ToAccountDescription'), false, FailureHandling.CONTINUE_ON_FAILURE)

BeneficiaryReferenceNo = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - International New/Confirm/Label - Beneficiary Reference No'), 
    FailureHandling.CONTINUE_ON_FAILURE)

BeneficiaryReferenceNo = BeneficiaryReferenceNo.replace(': ', '')

BeneficiaryReferenceNo = BeneficiaryReferenceNo.replace(':', '')

WebUI.verifyMatch(BeneficiaryReferenceNo, GlobalVariable.Map123.get('BeneficiaryReferenceNo'), false, FailureHandling.CONTINUE_ON_FAILURE)

BeneficiaryResidentStatus = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - International New/Confirm/Label - Beneficiary Resident Status'), 
    FailureHandling.CONTINUE_ON_FAILURE)

BeneficiaryResidentStatus = BeneficiaryResidentStatus.replace(': ', '')

BeneficiaryResidentStatus = BeneficiaryResidentStatus.replace(':', '')

WebUI.verifyMatch(BeneficiaryResidentStatus, 'Resident', false, FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Map123.put('BeneficiaryResidentStatus', BeneficiaryResidentStatus)

BeneficiaryCategory = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - International New/Confirm/Label - Beneficiary Category'), 
    FailureHandling.CONTINUE_ON_FAILURE)

BeneficiaryCategory = BeneficiaryCategory.replace(': ', '')

BeneficiaryCategory = BeneficiaryCategory.replace(':', '')

WebUI.verifyMatch(BeneficiaryCategory, GlobalVariable.Map123.get('BeneficiaryCategory'), false, FailureHandling.CONTINUE_ON_FAILURE)

TransactorRelationship = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - International New/Confirm/Label - Transactor Relationship'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TransactorRelationship = TransactorRelationship.replace(': ', '')

TransactorRelationship = TransactorRelationship.replace(':', '')

WebUI.verifyMatch(TransactorRelationship, GlobalVariable.Map123.get('TransactorRelationship'), false, FailureHandling.CONTINUE_ON_FAILURE)

IdenticalStatus = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - International New/Confirm/Label - Identical Status'), 
    FailureHandling.CONTINUE_ON_FAILURE)

IdenticalStatus = IdenticalStatus.replace(': ', '')

IdenticalStatus = IdenticalStatus.replace(':', '')

WebUI.verifyMatch(IdenticalStatus, 'Identical', false, FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Map123.put('IdenticalStatus', IdenticalStatus)

PurposeOfTransaction = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - International New/Confirm/Label - Purpose Of Transaction'), 
    FailureHandling.CONTINUE_ON_FAILURE)

PurposeOfTransaction = PurposeOfTransaction.replace(': ', '')

PurposeOfTransaction = PurposeOfTransaction.replace(':', '')

WebUI.verifyMatch(PurposeOfTransaction, GlobalVariable.Map123.get('PurposeOfTransaction'), false, FailureHandling.CONTINUE_ON_FAILURE)

LHBUPurposeCode = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - International New/Confirm/Label - LHBU Purpose Code'), 
    FailureHandling.CONTINUE_ON_FAILURE)

LHBUPurposeCode = LHBUPurposeCode.replace(': ', '')

LHBUPurposeCode = LHBUPurposeCode.replace(':', '')

WebUI.verifyMatch(LHBUPurposeCode, GlobalVariable.Map123.get('LHBUPurposeCode'), false, FailureHandling.CONTINUE_ON_FAILURE)

LHBUDocumentType = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - International New/Confirm/Label - LHBU Document Type'), 
    FailureHandling.CONTINUE_ON_FAILURE)

LHBUDocumentType = LHBUDocumentType.replace(': ', '')

LHBUDocumentType = LHBUDocumentType.replace(':', '')

WebUI.verifyMatch(LHBUDocumentType, GlobalVariable.Map123.get('LHBUDocumentType'), false, FailureHandling.CONTINUE_ON_FAILURE)

LHBUDocumentType = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - International New/Confirm/Label - LHBU Document Type'), 
    FailureHandling.CONTINUE_ON_FAILURE)

LHBUDocumentType = LHBUDocumentType.replace(': ', '')

LHBUDocumentType = LHBUDocumentType.replace(':', '')

WebUI.verifyMatch(LHBUDocumentType, GlobalVariable.Map123.get('LHBUDocumentType'), false, FailureHandling.CONTINUE_ON_FAILURE)

LHBUDocumentTypeDescription = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - International New/Confirm/Label - LHBU Document Type Description'), 
    FailureHandling.CONTINUE_ON_FAILURE)

LHBUDocumentTypeDescription = LHBUDocumentTypeDescription.replace(': ', '')

LHBUDocumentTypeDescription = LHBUDocumentTypeDescription.replace(':', '')

WebUI.verifyMatch(LHBUDocumentTypeDescription, GlobalVariable.Map123.get('LHBUDocumentTypeDescription'), false, FailureHandling.CONTINUE_ON_FAILURE)

InstructionMode = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - International New/Confirm/Label - Instruction Mode'), 
    FailureHandling.CONTINUE_ON_FAILURE)

InstructionMode = InstructionMode.replace(': ', '')

InstructionMode = InstructionMode.replace(':', '')

GlobalVariable.Map123.put('InstructionMode', InstructionMode)

WebUI.verifyMatch(InstructionMode, GlobalVariable.Map123.get('InstructionMode'), false, FailureHandling.CONTINUE_ON_FAILURE)

FutureDate = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - International New/Confirm/Label - Specific Date - On'), 
    FailureHandling.CONTINUE_ON_FAILURE)

FutureDate = FutureDate.replace(': ', '')

GlobalVariable.Map123.put('FutureDate', FutureDate)

SpecificAt = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - International New/Confirm/Label - Specific Date - Session Time'), 
    FailureHandling.CONTINUE_ON_FAILURE)

SpecificAt = SpecificAt.replace(': ', '')

WebUI.verifyMatch(SpecificAt, GlobalVariable.Map123.get('SpecificAt'), false, FailureHandling.CONTINUE_ON_FAILURE)

Expiredon = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - International New/Confirm/Label - Expired on'), 
    FailureHandling.CONTINUE_ON_FAILURE)

Expiredon = Expiredon.replace(': ', '')

Expiredon = Expiredon.replace(':', '')

GlobalVariable.Map123.put('Expiredon', Expiredon)

WebUI.waitForElementVisible(findTestObject('Pending Task/Balance Transfer/Inhouse/Detail/TextField - Response Code'), 0, 
    FailureHandling.CONTINUE_ON_FAILURE)

String abc = '123456'

WebUI.setText(findTestObject('Pending Task/Balance Transfer/Inhouse/Detail/TextField - Response Code'), abc, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Balance Transfer/Balance Transfer - International New/Confirm/BTN - Submit'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Balance Transfer/Balance Transfer - International New/Confirm/BTN - Submit (Pop Up)'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Balance Transfer/Balance Transfer - International New/Confirm/BTN - Submit (Pop Up)'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

