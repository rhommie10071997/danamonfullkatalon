import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(5)

SubTitle = WebUI.getText(findTestObject('Pending Task/Balance Transfer/Inhouse/Detail/Label - Sub Title'))

WebUI.verifyMatch(SubTitle, 'Reference Number Detail', true)

Menu = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - Menu'), FailureHandling.STOP_ON_FAILURE)

Menu = Menu.replace(': ', '')

WebUI.verifyMatch(Menu, "Balance Transfer", false)

Product = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - Product'), 
    FailureHandling.STOP_ON_FAILURE)

Product = Product.replace(': ', '')

WebUI.verifyMatch(Product, GlobalVariable.Map123.get('Product'), false)

TransactionReferenceNumber = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - Transaction Reference Number'), 
    FailureHandling.STOP_ON_FAILURE)

TransactionReferenceNumber = TransactionReferenceNumber.replace(': ', '')

WebUI.verifyMatch(TransactionReferenceNumber, GlobalVariable.Map123.get('RefNo'), false)

DocumentCode = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - Document Code'), 
    FailureHandling.STOP_ON_FAILURE)

DocumentCode = DocumentCode.replace(': ', '')

WebUI.verifyMatch(DocumentCode, GlobalVariable.Map123.get('RefNo'), false)

TransferFrom = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - Transfer From'))

TransferFrom = TransferFrom.replace(': ', '')

WebUI.verifyMatch(TransferFrom, GlobalVariable.Map123.get('TransferFrom'), false)

FromAccountDescription = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - From Account Description'))

FromAccountDescription = FromAccountDescription.replace(': ', '')

WebUI.verifyMatch(FromAccountDescription, GlobalVariable.Map123.get('FromAccountDescription'), false)

TransferTo = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - Transfer To'))

TransferTo = TransferTo.replace(': ', '')

WebUI.verifyMatch(TransferTo, GlobalVariable.Map123.get('TransferTo'), false)

BeneficiaryBank = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - Beneficiary Bank'))

BeneficiaryBank = BeneficiaryBank.replace(': ', '')

WebUI.verifyMatch(BeneficiaryBank, GlobalVariable.Map123.get('BeneficiaryBank'), false)

RetainedAmount = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - Retained Amount'))

RetainedAmount = RetainedAmount.replace(': ', '')

WebUI.verifyMatch(RetainedAmount, GlobalVariable.Map123.get('RetainedAmount'), false)

RemittanceCurrency = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - Remittance Currency'))

RemittanceCurrency = RemittanceCurrency.replace(': ', '')

WebUI.verifyMatch(RemittanceCurrency, GlobalVariable.Map123.get('RemittanceCurrency'), false)

ExchangeRate = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - Exchange Rate'))

ExchangeRate = ExchangeRate.replace(': ', '')

WebUI.verifyMatch(ExchangeRate, GlobalVariable.Map123.get('ExchangeRate'), false)

InLieu = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - In Lieu'))

InLieu = InLieu.replace(': ', '')

WebUI.verifyMatch(InLieu, GlobalVariable.Map123.get('InLieu'), false)

if (InLieu.substring(0, 3) == 'IDR') {
    InLieuEquivalent = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - In Lieu Equivalent'))

    InLieuEquivalent = InLieuEquivalent.replace(': ', '')

    WebUI.verifyMatch(InLieuEquivalent, GlobalVariable.Map123.get('InLieuEquivalent'), false)
}

Provision = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - Provision'))

Provision = Provision.replace(': ', '')

WebUI.verifyMatch(Provision, GlobalVariable.Map123.get('Provision'), false)

if (Provision.substring(0, 3) == 'IDR') {
    ProvisionEquivalent = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - Provision Equivalent'))

    ProvisionEquivalent = ProvisionEquivalent.replace(': ', '')

    WebUI.verifyMatch(ProvisionEquivalent, GlobalVariable.Map123.get('ProvisionEquivalent'), false)
}

CorrespondentBankFee = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - Correspondent Bank Fee'))

CorrespondentBankFee = CorrespondentBankFee.replace(': ', '')

WebUI.verifyMatch(CorrespondentBankFee, GlobalVariable.Map123.get('CorrespondentBankFee'), false)

if (CorrespondentBankFee.substring(0, 3) == 'IDR') {
    CorrespondentBankFeeEquivalent = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - Correspondent Bank Fee Equivalent'))

    CorrespondentBankFeeEquivalent = CorrespondentBankFee.replace(': ', '')

    WebUI.verifyMatch(CorrespondentBankFeeEquivalent, GlobalVariable.Map123.get('CorrespondentBankFeeEquivalent'), false)
}

MinimumTransactionFee = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - Minimum Transaction Fee'))

MinimumTransactionFee = MinimumTransactionFee.replace(': ', '')

WebUI.verifyMatch(MinimumTransactionFee, GlobalVariable.Map123.get('MinimumTransactionFee'), false)

if (MinimumTransactionFee.substring(0, 3) == 'IDR') {
    MinimumTransactionFeeEquivalent = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - Minimum Transaction Fee Equivalent'))

    MinimumTransactionFeeEquivalent = MinimumTransactionFeeEquivalent.replace(': ', '')

    WebUI.verifyMatch(MinimumTransactionFeeEquivalent, GlobalVariable.Map123.get('MinimumTransactionFeeEquivalent'), false)
}

FullAmountCharge = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - Full Amount Charge'))

FullAmountCharge = FullAmountCharge.replace(': ', '')

WebUI.verifyMatch(FullAmountCharge, GlobalVariable.Map123.get('FullAmountCharge'), false)

CableFee = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - Cable Fee'))

CableFee = CableFee.replace(': ', '')

WebUI.verifyMatch(CableFee, GlobalVariable.Map123.get('CableFee'), false)

if (CableFee.substring(0, 3) == 'IDR') {
    CableFeeEquivalent = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - Cable Fee Equivalent'))

    CableFeeEquivalent = CableFeeEquivalent.replace(': ', '')

    WebUI.verifyMatch(CableFeeEquivalent, GlobalVariable.Map123.get('CableFeeEquivalent'), false)
}

TotalCharge = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - Total Charge'))

TotalCharge = TotalCharge.replace(': ', '')

WebUI.verifyMatch(TotalCharge, GlobalVariable.Map123.get('TotalCharge'), false)

ChargetoRemitter = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - Charge to Remitter'))

ChargetoRemitter = ChargetoRemitter.replace(': ', '')

WebUI.verifyMatch(ChargetoRemitter, GlobalVariable.Map123.get('ChargetoRemitter'), false)

ChargetoBeneficiary = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - Charge to Beneficiary'))

ChargetoBeneficiary = ChargetoBeneficiary.replace(': ', '')

WebUI.verifyMatch(ChargetoBeneficiary, GlobalVariable.Map123.get('ChargetoBeneficiary'), false)

TotalDebitAmount = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - Total Debit Amount'))

TotalDebitAmount = TotalDebitAmount.replace(': ', '')

WebUI.verifyMatch(TotalDebitAmount, GlobalVariable.Map123.get('TotalDebitAmount'), false)

ChargeInstruction = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - Charge Instruction'))

ChargeInstruction = ChargeInstruction.replace(': ', '')

ChargeInstruction = ChargeInstruction.replace(':', '')

WebUI.verifyMatch(ChargeInstruction, GlobalVariable.Map123.get('ChargeInstruction'), false)

DebitCharge = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - Debit Charge'))

DebitCharge = DebitCharge.replace(': ', '')

DebitCharge = DebitCharge.replace(':', '')

WebUI.verifyMatch(DebitCharge, GlobalVariable.Map123.get('DebitCharge'), false)

ToAccountDescription = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - To Account Description'))

ToAccountDescription = ToAccountDescription.replace(': ', '')

ToAccountDescription = ToAccountDescription.replace(':', '')

WebUI.verifyMatch(ToAccountDescription, GlobalVariable.Map123.get('ToAccountDescription'), false)

BeneficiaryReferenceNo = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - Beneficiary Reference No'))

BeneficiaryReferenceNo = BeneficiaryReferenceNo.replace(': ', '')

BeneficiaryReferenceNo = BeneficiaryReferenceNo.replace(':', '')

WebUI.verifyMatch(BeneficiaryReferenceNo, GlobalVariable.Map123.get('BeneficiaryReferenceNo'), false)

BeneficiaryResidentStatus = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - Beneficiary Resident Status'))

BeneficiaryResidentStatus = BeneficiaryResidentStatus.replace(': ', '')

BeneficiaryResidentStatus = BeneficiaryResidentStatus.replace(':', '')

WebUI.verifyMatch(BeneficiaryResidentStatus, GlobalVariable.Map123.get('BeneficiaryResidentStatus'), false)

BeneficiaryCategory = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - Beneficiary Category'))

BeneficiaryCategory = BeneficiaryCategory.replace(': ', '')

BeneficiaryCategory = BeneficiaryCategory.replace(':', '')

WebUI.verifyMatch(BeneficiaryCategory, GlobalVariable.Map123.get('BeneficiaryCategory'), false)

TransactorRelationship = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - Transactor Relationship'))

TransactorRelationship = TransactorRelationship.replace(': ', '')

TransactorRelationship = TransactorRelationship.replace(':', '')

WebUI.verifyMatch(TransactorRelationship, GlobalVariable.Map123.get('TransactorRelationship'), false)

IdenticalStatus = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - Identical Status'))

IdenticalStatus = IdenticalStatus.replace(': ', '')

IdenticalStatus = IdenticalStatus.replace(':', '')

WebUI.verifyMatch(IdenticalStatus, GlobalVariable.Map123.get('IdenticalStatus'), false)

PurposeOfTransaction = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - Purpose Of Transaction'))

PurposeOfTransaction = PurposeOfTransaction.replace(': ', '')

PurposeOfTransaction = PurposeOfTransaction.replace(':', '')

WebUI.verifyMatch(PurposeOfTransaction, GlobalVariable.Map123.get('PurposeOfTransaction'), false)

LHBUPurposeCode = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - LHBU Purpose Code'))

LHBUPurposeCode = LHBUPurposeCode.replace(': ', '')

LHBUPurposeCode = LHBUPurposeCode.replace(':', '')

WebUI.verifyMatch(LHBUPurposeCode, GlobalVariable.Map123.get('LHBUPurposeCode'), false)

LHBUDocumentType = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - LHBU Document Type'))

LHBUDocumentType = LHBUDocumentType.replace(': ', '')

LHBUDocumentType = LHBUDocumentType.replace(':', '')

WebUI.verifyMatch(LHBUDocumentType, GlobalVariable.Map123.get('LHBUDocumentType'), false)

LHBUDocumentType = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - LHBU Document Type'))

LHBUDocumentType = LHBUDocumentType.replace(': ', '')

LHBUDocumentType = LHBUDocumentType.replace(':', '')

WebUI.verifyMatch(LHBUDocumentType, GlobalVariable.Map123.get('LHBUDocumentType'), false)

LHBUDocumentTypeDescription = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - LHBU Document Type Description'))

LHBUDocumentTypeDescription = LHBUDocumentTypeDescription.replace(': ', '')

LHBUDocumentTypeDescription = LHBUDocumentTypeDescription.replace(':', '')

WebUI.verifyMatch(LHBUDocumentTypeDescription, GlobalVariable.Map123.get('LHBUDocumentTypeDescription'), false)

InstructionMode = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - International/New Ref No/Label - Instruction Mode'))

InstructionMode = InstructionMode.replace(': ', '')

InstructionMode = InstructionMode.replace(':', '')

WebUI.verifyMatch(InstructionMode, GlobalVariable.Map123.get('InstructionMode'), false)

RepeatOn = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - Inhouse/New - Ref No Detail/Label - Repeat - On'), 
    FailureHandling.CONTINUE_ON_FAILURE)

RepeatOn = RepeatOn.replace(': ', '')

RepeatOn = RepeatOn.replace(':', '')

WebUI.verifyMatch(RepeatOn, GlobalVariable.Map123.get('RepeatOn'), false, FailureHandling.CONTINUE_ON_FAILURE)

RepeatEvery = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - Inhouse/New - Ref No Detail/Label - Repeat - Every'), 
    FailureHandling.CONTINUE_ON_FAILURE)

RepeatEvery = RepeatEvery.replace(': ', '')

RepeatEvery = RepeatEvery.replace(':', '')

WebUI.verifyMatch(RepeatEvery, GlobalVariable.Map123.get('RepeatEvery'), false, FailureHandling.CONTINUE_ON_FAILURE)

RepeatAt = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - Inhouse/New - Ref No Detail/Label - Repeat - At'), 
    FailureHandling.CONTINUE_ON_FAILURE)

RepeatAt = RepeatAt.replace(': ', '')

RepeatAt = RepeatAt.replace(':', '')

WebUI.verifyMatch(RepeatAt, GlobalVariable.Map123.get('RepeatAt'), false, FailureHandling.CONTINUE_ON_FAILURE)

RepeatStart = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - Inhouse/New - Ref No Detail/Label - Repeat - Start'), 
    FailureHandling.CONTINUE_ON_FAILURE)

RepeatStart = RepeatStart.replace(': ', '')

RepeatStart = RepeatStart.replace(':', '')

WebUI.verifyMatch(RepeatStart, GlobalVariable.Map123.get('RepeatStart'), false, FailureHandling.CONTINUE_ON_FAILURE)

RepeatEnd = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - Inhouse/New - Ref No Detail/Label - Repeat - End'), 
    FailureHandling.CONTINUE_ON_FAILURE)

RepeatEnd = RepeatEnd.replace(': ', '')

RepeatEnd = RepeatEnd.replace(':', '')

WebUI.verifyMatch(RepeatEnd, GlobalVariable.Map123.get('RepeatEnd'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.scrollToElement(findTestObject('Transaction Status/Balance Transfer - Inhouse/New - Ref No Detail/BTN - Document Number'), 
    0)

WebUI.click(findTestObject('Transaction Status/Balance Transfer - Inhouse/New - Ref No Detail/BTN - Document Number'))

WebUI.delay(5)

