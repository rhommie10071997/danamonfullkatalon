import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import java.text.DecimalFormat as DecimalFormat
import java.text.NumberFormat as NumberFormat
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

Random rand = new Random()

'Di Balance Transfer International di sini hanya bisa transaksi USD to USD'

'Form Create    --------------------------------------------------------------------------------------------------------------------------------------'

'From Account Bisa nama atau account number dia'
FromAccount = '000001176908'

'From Account Description trakhirnya di pasang angka random biar unik'
FromAccountDescription = 'FromAccDesc Ardo Inter Lets gow'

'Amount Angka Depan'
AmountDepan = '25'

'Amount Angka Belakang dimana jumlah nya akan di random'
AmountRandomBelakang = 9999

'Transfer To Flag 0 = Beneficiary List, Flag 1 = New Beneficiary'
TransferToFlag123 = 0

'-------------  Beneficiary List -------------------------------------------'

'Beneficiary List bisa nomor dan nama'
BeneficiaryList = 'Neville LB'

'-------------New Beneficiary List -------------------------------------------'

'Save to Beneficiary List Yes & No'
SavetoBeneficiaryList = 'Yes'

'Alias Name'
AliasName = 'AliasName Ardo Kuy'

'National Organization Directory'
NationalOrganizationDirectory = 'Chips'

'Beneficiary Bank'
BeneficiaryBank = 'bank anza'

'Intermediary Bank'
IntermediaryBank = 'Bank Anza'

'Account Number'
AccountNumber = '12341234'

'Account Name'
AccountName = 'TestArdo'

'Beneficiary Address 1 2 3'
BeneficiaryAddress1 = 'Jakarta Barat'

BeneficiaryAddress2 = 'Tanjung Duren'

BeneficiaryAddress3 = 'No.98 JalanAsdasdasd'

'Beneficiary notification Email dan Phone'
BeneEmail = 'test123123@gmail.com'

BenePhone = '085395959512'

'To Account Description di sini saya sama-in kayak from account description'
ToAccountDescription = FromAccountDescription

'Beneficiary Reference No'
BeneficiaryReferenceNo = '123412341234'

'Remittance Currency'
RemittanceCurrency = 'USD'

'Beneficiary Category'
BeneficiaryCategory = '3 - Pemerintah'

'Transactor Relationship'
TransactorRelationship = 'A - Affiliated'

'PurposeOfTransaction'
PurposeOfTransaction = 'Ekspor Barang'

'LHBU Purpose Code'
LHBUPurposeCode = '00- 00'

'LHBUDocumentType'
LHBUDocumentType = '001 - 001'

'LHBU Document Type Description'
LHBUDocumentTypeDescription = 'LHBU Document Type Descriptin Ardo Lets Gow'

'Charge Yes or No'
Charge = 'Yes'

'----------------------------------------------------------------------------------------------------------------------------------------------------------'
GlobalVariable.Map123.put('SavetoBeneficiaryList', SavetoBeneficiaryList)

GlobalVariable.Map123.put('Charge Instruction', 'Remitter')

GlobalVariable.Map123.put('Debit Charge', 'Combine')

GlobalVariable.Map123.put('Beneficiary Resident Status', 'Resident')

GlobalVariable.Map123.put('Identical Status', 'Identical')

'----------------------------------------------------------------------------------------------------------------------------------------------------------'
WebUI.openBrowser(GlobalVariable.url, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.maximizeWindow()

WebUI.waitForAngularLoad(0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('1 - Frame/1 - iframe login'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Log in/corpid'), 'pcmkillua', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Log in/userid'), 'killua', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Log in/password'), 'Password123', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Log in/login'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(3, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementPresent(findTestObject('Menu/BTN - Menu Object'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Menu/BTN - Menu Object'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementClickable(findTestObject('Menu/BTN - Menu Object'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Menu/BTN - Menu Object'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Menu/BTN - Liquidity Management'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Menu/BTN - Liquidity Management'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Menu/BTN - Balance Transfer'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Menu/BTN - Balance Transfer'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Balance Transfer/Balance Transfer - International New/Create/BTN - International Foreign Currency Transfer'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Balance Transfer/Balance Transfer - International New/Create/BTN - International Foreign Currency Transfer'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Balance Transfer/Balance Transfer - International New/Create/DropList - From Account'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Balance Transfer/Balance Transfer - International New/Create/DropList - From Account'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Balance Transfer/Balance Transfer - International New/Create/TextField - Input DropList'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Balance Transfer/Balance Transfer - International New/Create/TextField - Input DropList'), 
    FromAccount, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Balance Transfer/Balance Transfer - International New/Create/DropList - Select Account - Highlight'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Balance Transfer/Balance Transfer - International New/Create/DropList - Select Account - Highlight'), 
    FailureHandling.CONTINUE_ON_FAILURE)

FromAccount = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - International New/Create/DropList - From Account'), 
    FailureHandling.CONTINUE_ON_FAILURE)

FromAccount = FromAccount.replace('(', ' (')

GlobalVariable.Map123.put('TransferFrom', FromAccount)

WebUI.delay(7, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Balance Transfer/Balance Transfer - International New/Create/Label - Nett Balance'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

'Split Amount , buat di isi ----------------------------------------------------------------------------------------------------------------'
NettBalance = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - International New/Create/Label - Nett Balance'), 
    FailureHandling.CONTINUE_ON_FAILURE)

String[] NettBalanceSplit = NettBalance.split(' ')

String Currency = NettBalanceSplit[0]

String[] AmountRep1 = NettBalanceSplit[1].split('\\.')

String Amount11 = AmountRep1[0]

String AmountRep2 = Amount11.replaceAll('\\,', '')

int angka

angka = rand.nextInt(AmountRandomBelakang)

angka1 = (AmountDepan + angka)

Locale local1 = new Locale('en', 'UK')

DecimalFormat amountinquiry1 = ((NumberFormat.getNumberInstance(local1)) as DecimalFormat)

amountinquiry1.applyPattern('###,###,##0.00')

String amountinquiry12 = amountinquiry1.format(new BigDecimal(angka1))

GlobalVariable.Map123.put('AmountInquiry', amountinquiry12)

println(amountinquiry12)

Long angka2 = Long.parseLong(angka1)

Long retainamountmines = Long.parseLong(AmountRep2)

Long retainamount = retainamountmines - angka2

String retainamount1 = retainamount

DecimalFormat retainamountformat = ((NumberFormat.getNumberInstance(local1)) as DecimalFormat)

retainamountformat.applyPattern('###,###,##0.00')

String retainamountformat2 = Currency + retainamountformat.format(new BigDecimal(retainamount1))

GlobalVariable.Map123.put('RetainedAmount', retainamountformat2)

'------------------------------------------------------------------------------------------------------------------------------------'
Random rand123 = new Random()

int angka123

angka123 = rand123.nextInt(999)

fromaccountdes = (FromAccountDescription + angka123)

WebUI.setText(findTestObject('Balance Transfer/Balance Transfer - International New/Create/TextField - From Account Description'), 
    FromAccountDescription, FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Map123.put('FromAccountDescription', FromAccountDescription)

'Transfer To --------------------------------------------------------------------------------------------------------------------------'
if (GlobalVariable.Map123.get('TransferToFlag123') == 0) {
    WebUI.click(findTestObject('Balance Transfer/Balance Transfer - International New/Create/BTN - Transfer To - Beneficiary List'))

    TransferTo2 = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - International New/Create/BTN - Transfer To - Beneficiary List'))

    GlobalVariable.Map123.put('TransferTo', TransferTo2)

    WebUI.click(findTestObject('Balance Transfer/Balance Transfer - International New/Create/DropList - Beneficiary List'))

    WebUI.delay(1)

    WebUI.waitForElementVisible(findTestObject('Balance Transfer/Balance Transfer - International New/Create/TextField - Input DropList'), 
        0)

    WebUI.setText(findTestObject('Balance Transfer/Balance Transfer - International New/Create/TextField - Input DropList'), 
        BeneficiaryList)

    WebUI.delay(2)

    WebUI.waitForElementVisible(findTestObject('Balance Transfer/Balance Transfer - International New/Create/DropList - Select Account - Highlight'), 
        0)

    WebUI.click(findTestObject('Balance Transfer/Balance Transfer - International New/Create/DropList - Select Account - Highlight'))

    BeneficiaryList = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - International New/Create/DropList - Beneficiary List'))

    GlobalVariable.Map123.put('BeneficiaryList', BeneficiaryList)
} else {
    WebUI.click(findTestObject('Balance Transfer/Balance Transfer - International New/Create/BTN - Transfer To - New Beneficiary'))

    TransferTo2 = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - International New/Create/BTN - Transfer To - New Beneficiary'))

    if (GlobalVariable.Map123.get('FlagSavetoBeneficiaryList') == 'Yes') {
        WebUI.click(findTestObject('Balance Transfer/Balance Transfer - International New/Create/CheckBox - Save to Beneficiary List'))

        WebUI.delay(1)

        Random AngkaRandomAliasName123 = new Random()

        int AngkaRandomAliasName

        AngkaRandomAliasName = AngkaRandomAliasName123.nextInt(999)

        String AliasName1 = AliasName + AngkaRandomAliasName

        WebUI.setText(findTestObject('Balance Transfer/Balance Transfer - International New/Create/TextField - Alias Name'), 
            AliasName1)

        GlobalVariable.Map123.put('AliasName', AliasName1)
    }
    
    WebUI.click(findTestObject('Balance Transfer/Balance Transfer - International New/Create/DropList - National Organizational Directory'))

    WebUI.delay(1)

    WebUI.waitForElementVisible(findTestObject('Balance Transfer/Balance Transfer - International New/Create/TextField - Input DropList'), 
        0)

    WebUI.setText(findTestObject('Balance Transfer/Balance Transfer - International New/Create/TextField - Input DropList'), 
        NationalOrganizationDirectory)

    WebUI.delay(2)

    WebUI.waitForElementVisible(findTestObject('Balance Transfer/Balance Transfer - International New/Create/DropList - Select Account - Highlight'), 
        0)

    WebUI.click(findTestObject('Balance Transfer/Balance Transfer - International New/Create/DropList - Select Account - Highlight'))

    NationalOrganizationDirectory = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - International New/Create/DropList - National Organizational Directory'))

    GlobalVariable.Map123.put('NationalOrganizationDirectory', NationalOrganizationDirectory)

    WebUI.click(findTestObject('Balance Transfer/Balance Transfer - International New/Create/DropList - Beneficiary Bank'))

    WebUI.delay(1)

    WebUI.waitForElementVisible(findTestObject('Balance Transfer/Balance Transfer - International New/Create/TextField - Input DropList'), 
        0)

    WebUI.setText(findTestObject('Balance Transfer/Balance Transfer - International New/Create/TextField - Input DropList'), 
        BeneficiaryBank)

    WebUI.delay(2)

    WebUI.waitForElementVisible(findTestObject('Balance Transfer/Balance Transfer - International New/Create/DropList - Select Account - Highlight'), 
        0)

    WebUI.click(findTestObject('Balance Transfer/Balance Transfer - International New/Create/DropList - Select Account - Highlight'))

    BeneficiaryBank = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - International New/Create/DropList - Beneficiary Bank'))

    GlobalVariable.Map123.put('BeneficiaryBank', BeneficiaryBank)

    BankBranch = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - International New/Create/TextField - Bank Branch'))

    GlobalVariable.Map123.put('BankBranch', BankBranch)

    BankCity = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - International New/Create/TextField - Bank City'))

    GlobalVariable.Map123.put('BankCity', BankCity)

    BankCountry = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - International New/Create/TextField - Bank Country'))

    GlobalVariable.Map123.put('BankCountry', BankCountry)

    WebUI.click(findTestObject('Balance Transfer/Balance Transfer - International New/Create/DropList - Intermediary Bank'))

    WebUI.delay(1)

    WebUI.waitForElementVisible(findTestObject('Balance Transfer/Balance Transfer - International New/Create/TextField - Input DropList'), 
        0)

    WebUI.setText(findTestObject('Balance Transfer/Balance Transfer - International New/Create/TextField - Input DropList'), 
        IntermediaryBank)

    WebUI.delay(2)

    WebUI.waitForElementVisible(findTestObject('Balance Transfer/Balance Transfer - International New/Create/DropList - Select Account - Highlight'), 
        0)

    WebUI.click(findTestObject('Balance Transfer/Balance Transfer - International New/Create/DropList - Select Account - Highlight'))

    IntermediaryBank = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - International New/Create/DropList - Intermediary Bank'))

    GlobalVariable.Map123.put('IntermediaryBank', IntermediaryBank)

    WebUI.setText(findTestObject('Balance Transfer/Balance Transfer - International New/Create/TextField - Account Number'), 
        AccountNumber)

    GlobalVariable.Map123.put('AccountNumber', AccountNumber)

    WebUI.setText(findTestObject('Balance Transfer/Balance Transfer - International New/Create/TextField - Account Name'), 
        AccountName)

    GlobalVariable.Map123.put('AccountName', AccountName)

    WebUI.setText(findTestObject('Balance Transfer/Balance Transfer - International New/Create/TextField - Address 1'), 
        BeneficiaryAddress1)

    GlobalVariable.Map123.put('BeneficiaryAddress1', BeneficiaryAddress1)

    WebUI.setText(findTestObject('Balance Transfer/Balance Transfer - International New/Create/TextField - Address 2'), 
        BeneficiaryAddress2)

    GlobalVariable.Map123.put('BeneficiaryAddress2', BeneficiaryAddress2)

    WebUI.setText(findTestObject('Balance Transfer/Balance Transfer - International New/Create/TextField - Address 3'), 
        BeneficiaryAddress3)

    GlobalVariable.Map123.put('BeneficiaryAddress3', BeneficiaryAddress3)

    WebUI.setText(findTestObject('Balance Transfer/Balance Transfer - International New/Create/TextField - Email'), BeneEmail)

    GlobalVariable.Map123.put('BeneEmail', BeneEmail)

    WebUI.setText(findTestObject('Balance Transfer/Balance Transfer - International New/Create/TextField - Phone'), BenePhone)

    GlobalVariable.Map123.put('BenePhone', BenePhone)
}

WebUI.setText(findTestObject('Balance Transfer/Balance Transfer - International New/Create/TextField - To Account Description'), 
    ToAccountDescription, FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Map123.put('ToAccountDescription', ToAccountDescription)

WebUI.setText(findTestObject('Balance Transfer/Balance Transfer - International New/Create/TextField - Beneficiary Reference No'), 
    BeneficiaryReferenceNo, FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Map123.put('BeneficiaryReferenceNo', BeneficiaryReferenceNo)

WebUI.setText(findTestObject('Balance Transfer/Balance Transfer - International New/Create/TextField - Retained Amount'), 
    retainamount1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Balance Transfer/Balance Transfer - International New/Create/DropList - Remittance Currency'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Balance Transfer/Balance Transfer - International New/Create/TextField - Input DropList'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Balance Transfer/Balance Transfer - International New/Create/TextField - Input DropList'), 
    RemittanceCurrency, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Balance Transfer/Balance Transfer - International New/Create/DropList - Select Account - Highlight'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Balance Transfer/Balance Transfer - International New/Create/DropList - Select Account - Highlight'), 
    FailureHandling.CONTINUE_ON_FAILURE)

RemittanceCurrency = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - International New/Create/DropList - Remittance Currency'), 
    FailureHandling.CONTINUE_ON_FAILURE)

String[] RemittanceCurrency1 = RemittanceCurrency.split(' - ')

GlobalVariable.Map123.put('RemittanceCurrency', RemittanceCurrency1[0])

WebUI.click(findTestObject('Balance Transfer/Balance Transfer - International New/Create/DropList - Beneficiary Category'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Balance Transfer/Balance Transfer - International New/Create/TextField - Input DropList'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Balance Transfer/Balance Transfer - International New/Create/TextField - Input DropList'), 
    BeneficiaryCategory, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Balance Transfer/Balance Transfer - International New/Create/DropList - Select Account - Highlight'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Balance Transfer/Balance Transfer - International New/Create/DropList - Select Account - Highlight'), 
    FailureHandling.CONTINUE_ON_FAILURE)

BeneficiaryCategory = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - International New/Create/DropList - Beneficiary Category'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Map123.put('BeneficiaryCategory', BeneficiaryCategory)

WebUI.click(findTestObject('Balance Transfer/Balance Transfer - International New/Create/DropList - Transactor Relationship'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Balance Transfer/Balance Transfer - International New/Create/TextField - Input DropList'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Balance Transfer/Balance Transfer - International New/Create/TextField - Input DropList'), 
    TransactorRelationship, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Balance Transfer/Balance Transfer - International New/Create/DropList - Select Account - Highlight'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Balance Transfer/Balance Transfer - International New/Create/DropList - Select Account - Highlight'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TransactorRelationship = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - International New/Create/DropList - Transactor Relationship'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Map123.put('TransactorRelationship', TransactorRelationship)

WebUI.click(findTestObject('Balance Transfer/Balance Transfer - International New/Create/DropList - Purpose Of Transaction'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Balance Transfer/Balance Transfer - International New/Create/TextField - Input DropList'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Balance Transfer/Balance Transfer - International New/Create/TextField - Input DropList'), 
    PurposeOfTransaction, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Balance Transfer/Balance Transfer - International New/Create/DropList - Select Account - Highlight'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Balance Transfer/Balance Transfer - International New/Create/DropList - Select Account - Highlight'), 
    FailureHandling.CONTINUE_ON_FAILURE)

PurposeOfTransaction = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - International New/Create/DropList - Purpose Of Transaction'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Map123.put('PurposeOfTransaction', PurposeOfTransaction)

WebUI.click(findTestObject('Balance Transfer/Balance Transfer - International New/Create/DropList - LHBU Purpose Code'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Balance Transfer/Balance Transfer - International New/Create/TextField - Input DropList'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Balance Transfer/Balance Transfer - International New/Create/TextField - Input DropList'), 
    LHBUPurposeCode, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Balance Transfer/Balance Transfer - International New/Create/DropList - Select Account - Highlight'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Balance Transfer/Balance Transfer - International New/Create/DropList - Select Account - Highlight'), 
    FailureHandling.CONTINUE_ON_FAILURE)

LHBUPurposeCode = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - International New/Create/DropList - LHBU Purpose Code'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Map123.put('LHBUPurposeCode', LHBUPurposeCode)

WebUI.click(findTestObject('Balance Transfer/Balance Transfer - International New/Create/DropList - LHBU Document Type'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Balance Transfer/Balance Transfer - International New/Create/TextField - Input DropList'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Balance Transfer/Balance Transfer - International New/Create/TextField - Input DropList'), 
    LHBUDocumentType, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Balance Transfer/Balance Transfer - International New/Create/DropList - Select Account - Highlight'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Balance Transfer/Balance Transfer - International New/Create/DropList - Select Account - Highlight'), 
    FailureHandling.CONTINUE_ON_FAILURE)

LHBUDocumentType = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - International New/Create/DropList - LHBU Document Type'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Map123.put('LHBUDocumentType', LHBUDocumentType)

WebUI.setText(findTestObject('Balance Transfer/Balance Transfer - International New/Create/TextField - LHBU Document Type Description'), 
    LHBUDocumentTypeDescription, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Map123.put('LHBUDocumentTypeDescription', LHBUDocumentTypeDescription)

WebUI.click(findTestObject('Balance Transfer/Balance Transfer - International New/Create/BTN - Instruction Mode - Immediate'), 
    FailureHandling.CONTINUE_ON_FAILURE)

IntructionMode = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - International New/Create/BTN - Instruction Mode - Immediate'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Map123.put('IntructionMode', IntructionMode)

WebUI.click(findTestObject('Balance Transfer/Balance Transfer - Domestic/Entry/BTN - Confirm'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

