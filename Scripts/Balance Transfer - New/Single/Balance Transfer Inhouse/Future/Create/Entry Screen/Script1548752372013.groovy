import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import java.text.DecimalFormat as DecimalFormat
import java.text.NumberFormat as NumberFormat
import org.junit.After as After
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

Random rand = new Random()

'Form Edit-------------------------------------------------------------------------------------------------------------------------------------------------'

'Transfer From (Account nya), Bisa Account Number atau Account Name nya'
String TransferFromAcc = 'Achmad'

'From Account Description nya'
String FromAccDes = 'Test Balance Transf Inhouse Future Ardo'

'Transfer To (0 = Own Account, 1 = Beneficiary List , 2 = New Beneficiary List)'
TransferToFlag123 = 0

'Random Transfer To nya mau own acc, benef list atau new beneflist .Kalau mau pilih manual di command("//") aja dlu " GlobalVariable.TransferToFlag123 = rand.nextInt(2) "'

//GlobalVariable.TransferToFlag123 = rand.nextInt(2)
'Transfer To nya "Own Account" & "New Beneficiary" itu Account Numbernya'
String TransferTo = '000001468362'

'Transfer To nya "Beneficiary List" Nama Beneficiary List nya'
String TransferToBeneList = 'minang'

'Save to Beneficiary List (Flag 0 = Save , Flag 1 = No)'
SaveToBenefListFlag123 = 1

//GlobalVariable.SaveToBenefListFlag123 = rand.nextInt(1)
'Alias Name'
AliasName = 'TestArdoBenef'

'Amount yang ada di Script sini di pisah jadi 2 Amount Depan dan Amount Random Belakang agar Amount Jadi Unique'

'Nanti kalau di isi AmountDepan = 25 dan Amount Belakang 9999 hasil amount nya berupa 25X.XXX'

'Amount Depan nya '
AmountDepan = '25'

'Amount Belakang itu random'
AmountRandomBelakang = 9999

'Email dan Phone nya'
Email = 'caw.seller1@uat-danamon.co.id'

Phone = '08531231231'

'Beneficiary Refrence Number nya'
BeneficiaryRefrenceNumber = '41234512'

'LHBUDocumentTypeDescription nya'
LHBUDocTypeDesc = 'test ardo LHBUDocTypeDesc lets go kuy'

'----------------------------------------------------------------------------------------------------------------------------------------------------------'
GlobalVariable.Map123.put('TransferToFlag123', TransferToFlag123)

GlobalVariable.Map123.put('SaveToBenefListFlag123', SaveToBenefListFlag123)

GlobalVariable.Map123.put('LHBUDocumentTypeDescription123', LHBUDocTypeDesc)

GlobalVariable.Map123.put('ExchangeRate123', 'Counter Rate')

GlobalVariable.Map123.put('ChargeInstruction123', 'Remitter')

GlobalVariable.Map123.put('DebitCharge123', 'Split')

GlobalVariable.Map123.put('InstructionMode123', 'Immediate')

'----------------------------------------------------------------------------------------------------------------------------------------------------------'
WebUI.openBrowser(GlobalVariable.url, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.maximizeWindow()

WebUI.waitForAngularLoad(0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('1 - Frame/1 - iframe login'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Log in/corpid'), 'pcmkillua', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Log in/userid'), 'killua', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Log in/password'), 'Password123', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Log in/login'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(3, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementPresent(findTestObject('Menu/BTN - Menu Object'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Menu/BTN - Menu Object'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementClickable(findTestObject('Menu/BTN - Menu Object'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Menu/BTN - Menu Object'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Menu/BTN - Liquidity Management'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Menu/BTN - Liquidity Management'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Menu/BTN - Balance Transfer'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Menu/BTN - Balance Transfer'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/DropList - Transfer From'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/DropList - Transfer From'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/DropList - Transfer From'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/Text Field - Set Text'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/Text Field - Set Text'), 
    TransferFromAcc, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/DropList - Select Account - Highlight'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/DropList - Select Account - Highlight'), 
    FailureHandling.CONTINUE_ON_FAILURE)

FromAccount = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/DropList - Transfer From'), 
    FailureHandling.CONTINUE_ON_FAILURE)

FromAccount = FromAccount.replace(' - ', '/ ')

FromAccount = FromAccount.replace('(IDR)', ' (IDR)')

GlobalVariable.Map123.put('FromAccount123', FromAccount)

WebUI.delay(10, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/Label - Nett Balance'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

netbalance = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/Label - Nett Balance'), 
    FailureHandling.CONTINUE_ON_FAILURE)

String[] splitidr = netbalance.split(' ')

GlobalVariable.Map123.put('Currency', splitidr[0])

String splitidr2 = splitidr[1]

String[] ilangtitik = splitidr2.split('\\.')

String c = ilangtitik[0].replaceAll('\\,', '')

int angka

angka = rand.nextInt(AmountRandomBelakang)

angka1 = (AmountDepan + angka)

Locale local1 = new Locale('en', 'UK')

DecimalFormat amountinquiry1 = ((NumberFormat.getNumberInstance(local1)) as DecimalFormat)

amountinquiry1.applyPattern('###,###,##0.00')

String amountinquiry12 = amountinquiry1.format(new BigDecimal(angka1))

GlobalVariable.Map123.put('amountinquiry', amountinquiry12)

println(amountinquiry12)

Long angka2 = Long.parseLong(angka1)

Long retainamountmines = Long.parseLong(c)

Long retainamount = retainamountmines - angka2

String retainamount1 = retainamount

DecimalFormat retainamountformat = ((NumberFormat.getNumberInstance(local1)) as DecimalFormat)

retainamountformat.applyPattern('###,###,##0.00')

String retainamountformat2 = (splitidr[0]) + retainamountformat.format(new BigDecimal(retainamount1))

GlobalVariable.Map123.put('retainamount123', retainamountformat2)

Random rand123 = new Random()

int angka123

angka123 = rand123.nextInt(999)

fromaccountdes = (FromAccDes + angka123)

GlobalVariable.Map123.put('FromAccountDescription123', fromaccountdes)

WebUI.setText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/TextField - From Account Description'), 
    fromaccountdes, FailureHandling.CONTINUE_ON_FAILURE)

//Transfer To pecah 3 (Own Account , Beneficiary List ,& New Beneficiary)
if (GlobalVariable.Map123.get('TransferToFlag123') == 0) {
    WebUI.click(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/BTN - Transfer To - Own Account'))

    TransferTo2 = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/BTN - Transfer To - Own Account'))

    GlobalVariable.Map123.put('TransferTo123', TransferTo2)

    WebUI.waitForElementVisible(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/DropList - Transfer To - Own Account'), 
        0)

    WebUI.click(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/DropList - Transfer To - Own Account'))

    WebUI.waitForElementVisible(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/Text Field - Set Text'), 
        0)

    WebUI.setText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/Text Field - Set Text'), 
        TransferTo)

    WebUI.delay(2)

    WebUI.waitForElementVisible(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/DropList - Select Account - Highlight'), 
        0)

    WebUI.click(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/DropList - Select Account - Highlight'))

    AccountName = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/DropList - Transfer To - Own Account'))

    AccountName = AccountName.replace(' - ', '/ ')

    AccountName = AccountName.replace('(IDR)', ' (IDR)')

    GlobalVariable.Map123.put('AccountName123', AccountName)

    String[] AccountName1 = AccountName.split('/ ')

    GlobalVariable.Map123.put('AccountNumber123', AccountName1[0])

    WebUI.setText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/TextField - Email'), 
        Email)

    GlobalVariable.Map123.put('email123', Email)

    WebUI.setText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/TextField - Phone'), 
        Phone)

    GlobalVariable.Map123.put('phone123', Phone)
} else if (GlobalVariable.Map123.get('TransferToFlag123') == 1) {
    WebUI.click(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/BTN - Transfer To - Beneficiary List'))

    TransferTo2 = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/BTN - Transfer To - Beneficiary List'))

    GlobalVariable.Map123.put('TransferTo123', TransferTo2)

    WebUI.click(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/DropList - Transfer To - Beneficiary List'))

    WebUI.waitForElementVisible(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/Text Field - Set Text'), 
        0)

    WebUI.setText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/Text Field - Set Text'), 
        TransferToBeneList)

    WebUI.delay(2)

    WebUI.waitForElementVisible(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/DropList - Select Account - Highlight'), 
        0)

    WebUI.click(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/DropList - Select Account - Highlight'))

    AccountName = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/DropList - Transfer To - Beneficiary List'))

    AccountName = AccountName.replace(' - ', '/ ')

    GlobalVariable.Map123.put('AccountName123', AccountName)

    String[] AccountName1 = AccountName.split('/ ')

    GlobalVariable.Map123.put('AccountNumber123', AccountName1[0])
} else {
    WebUI.click(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/BTN - Transfer To - New Beneficiary'))

    TransferTo2 = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/BTN - Transfer To - New Beneficiary'))

    GlobalVariable.Map123.put('TransferTo123', TransferTo2)

    WebUI.delay(2)

    WebUI.setText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/TextField - Transfer To - New Beneficary - Account Number'), 
        TransferTo)

    GlobalVariable.Map123.put('AccountNumber123', TransferTo)

    WebUI.delay(1)

    WebUI.click(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/BTN - Transfer To - New Beneficiary - Check Name'))

    GlobalVariable.Map123.put('SaveToBenefList123', 'No')

    GlobalVariable.Map123.put('AliasName123', '')

    if (GlobalVariable.Map123.get('SaveToBenefListFlag123') == 0) {
        WebUI.click(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/CheckBox - Save To Beneficiary List'))

        GlobalVariable.Map123.put('SaveToBenefList123', 'Yes')

        AliasNo = rand.nextInt(9999)

        String AliasName = AliasName + AliasNo

        GlobalVariable.Map123.put('AliasName123', AliasName)

        WebUI.setText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/TextField - Alias Name'), 
            AliasName)
    }
    
    WebUI.setText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/TextField - Email'), 
        Email)

    GlobalVariable.Map123.put('email123', Email)

    WebUI.setText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/TextField - Phone'), 
        Phone)

    GlobalVariable.Map123.put('phone123', Phone)
}

WebUI.setText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/TextField - To Account Description'), 
    fromaccountdes, FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Map123.put('BenefRefNum123', BeneficiaryRefrenceNumber)

WebUI.setText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/TextField - Beneficiary Reference Number'), 
    BeneficiaryRefrenceNumber, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/TextField - Retain Amount'), 
    retainamount1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/DropList - LHBU Purpose Code'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/Text Field - Set Text'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/Text Field - Set Text'), 
    '00 - 00', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/DropList - Select Account - Highlight'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/DropList - Select Account - Highlight'), 
    FailureHandling.CONTINUE_ON_FAILURE)

LHBUPurposeCode = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/DropList - LHBU Purpose Code'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Map123.put('LHBUPurposeCode123', LHBUPurposeCode)

WebUI.click(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/DropList - LHBU Document Type'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/Text Field - Set Text'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/Text Field - Set Text'), 
    '001 - 001', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/DropList - Select Account - Highlight'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/DropList - Select Account - Highlight'), 
    FailureHandling.CONTINUE_ON_FAILURE)

LHBUDocumentType = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/DropList - LHBU Document Type'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Map123.put('LHBUDocumentType123', LHBUDocumentType)

WebUI.setText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/TextField - LHBU Document Type Description'), 
    LHBUDocTypeDesc, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/BTN - Specific Date'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/DropList - Specific Date - Future Date'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/DropList - Specific Date - Future Date'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/DropList - Specific Date - Future Date - Date'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/DropList - Specific Date - Future Date - Date'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/DropList - Specific Date - At'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/Text Field - Set Text'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/Text Field - Set Text'), 
    '07', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/DropList - Select Account - Highlight'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/DropList - Select Account - Highlight'), 
    FailureHandling.CONTINUE_ON_FAILURE)

SpecificAt = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/DropList - Specific Date - At'), 
    FailureHandling.CONTINUE_ON_FAILURE)

String[] SpecificA1 = SpecificAt.split(' - ')

GlobalVariable.Map123.put('SpecificAt', SpecificA1[1])

InstructionModeActive = WebUI.getText(findTestObject('Cash Pooling/New Entry/Label - Instruction Mode'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Map123.put('InstructionMode123', InstructionModeActive)

WebUI.click(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/BTN - Confirm'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

