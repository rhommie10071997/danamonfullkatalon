import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.waitForElementVisible(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Result/Label - Reference No'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

ReferenceNumber = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Result/Label - Reference No'), 
    FailureHandling.CONTINUE_ON_FAILURE)

String[]ReferenceNumber1 = ReferenceNumber.split(' : ')

GlobalVariable.Map123.put('RefNo', ReferenceNumber1[1])

Waitingforapproval = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Result/Label - Your Transaction Status is waiting for approval'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(Waitingforapproval, 'This transaction has been successfully released', true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Confirm Screen/Label - From Account'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

FromAccountCreateConfirm = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Confirm Screen/Label - From Account'), 
    FailureHandling.CONTINUE_ON_FAILURE)

FromAccountCreateConfirm = FromAccountCreateConfirm.replace(': ', '')

WebUI.verifyMatch(FromAccountCreateConfirm, GlobalVariable.Map123.get('FromAccount123'), false, FailureHandling.CONTINUE_ON_FAILURE)

FAD = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Confirm Screen/Label - From Account Description'), 
    FailureHandling.CONTINUE_ON_FAILURE)

FromAccountDescriptionCreateConfirm = FAD.replace(': ', '')

WebUI.verifyMatch(FromAccountDescriptionCreateConfirm, GlobalVariable.Map123.get('FromAccountDescription123'), true, FailureHandling.CONTINUE_ON_FAILURE)

TT = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Confirm Screen/Label - Transfer To'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TransferToCreateConfirm = TT.replace(': ', '')

WebUI.verifyMatch(TransferToCreateConfirm, GlobalVariable.Map123.get('TransferTo123'), true, FailureHandling.CONTINUE_ON_FAILURE)

AccountNumberDetailScreen = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Confirm Screen/Label - Account Number'), 
    FailureHandling.CONTINUE_ON_FAILURE)

AccountNumberDetailScreen = AccountNumberDetailScreen.replace(': ', '')

WebUI.verifyMatch(AccountNumberDetailScreen, GlobalVariable.Map123.get('AccountNumber123'), true, FailureHandling.CONTINUE_ON_FAILURE)

if (GlobalVariable.Map123.get('TransferToFlag123') == 0) {
    AccNameConfirmScreen = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Confirm Screen/Label - Account Name'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    AccNameConfirmScreen = AccNameConfirmScreen.replace(': ', '')

    WebUI.verifyMatch(AccNameConfirmScreen, GlobalVariable.Map123.get('AccountName123'), false, FailureHandling.CONTINUE_ON_FAILURE)

    BenefNotifEmailCreateConfirm = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Confirm Screen/Label - Beneficiary notification Phone'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.verifyMatch(BenefNotifEmailCreateConfirm, GlobalVariable.Map123.get('email123'), true, FailureHandling.CONTINUE_ON_FAILURE)

    BenefNotifPhoneCreateConfirm = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Confirm Screen/Label - Beneficiary notification Email'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.verifyMatch(BenefNotifPhoneCreateConfirm, GlobalVariable.Map123.get('phone123'), true, FailureHandling.CONTINUE_ON_FAILURE)
} else if (GlobalVariable.Map123.get('TransferToFlag123') == 1) {
    AccNameConfirmScreen = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Confirm Screen/Label - Account Name'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    AccNameConfirmScreen = AccNameConfirmScreen.replace(': ', '')

    WebUI.verifyMatch(AccNameConfirmScreen, GlobalVariable.Map123.get('AccountName123'), false, FailureHandling.CONTINUE_ON_FAILURE)
} else {
    SaveToBenefList = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Confirm Screen/Label - Save to Beneficiary List'))

    WebUI.verifyMatch(SaveToBenefList, GlobalVariable.Map123.get('SaveToBenefList123'), false)

    AliasName = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Confirm Screen/Label - Alias Name'))

    AliasName = AliasName.replace(': ', '')

    AliasName = AliasName.replace(':', '')

    WebUI.verifyMatch(AliasName, GlobalVariable.Map123.get('AliasName123'), false, FailureHandling.CONTINUE_ON_FAILURE)

    BenefNotifEmailCreateConfirm = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Confirm Screen/Label - Beneficiary notification Phone'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.verifyMatch(BenefNotifEmailCreateConfirm, GlobalVariable.Map123.get('email123'), true, FailureHandling.CONTINUE_ON_FAILURE)

    BenefNotifPhoneCreateConfirm = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Confirm Screen/Label - Beneficiary notification Email'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.verifyMatch(BenefNotifPhoneCreateConfirm, GlobalVariable.Map123.get('phone123'), true, FailureHandling.CONTINUE_ON_FAILURE)
}

ToAccDescCreateConfirm = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Confirm Screen/Label - To Account Description'), 
    FailureHandling.CONTINUE_ON_FAILURE)

ToAccDescCreateConfirm = ToAccDescCreateConfirm.replace(': ', '')

WebUI.verifyMatch(ToAccDescCreateConfirm, GlobalVariable.Map123.get('FromAccountDescription123'), true, FailureHandling.CONTINUE_ON_FAILURE)

BenefRefNoCreateConfirm = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Confirm Screen/Label - Beneficiary Reference Number'), 
    FailureHandling.CONTINUE_ON_FAILURE)

BenefRefNoCreateConfirm = BenefRefNoCreateConfirm.replace(': ', '')

WebUI.verifyMatch(BenefRefNoCreateConfirm, GlobalVariable.Map123.get('BenefRefNum123'), true, FailureHandling.CONTINUE_ON_FAILURE)

RetainAmountCreateConfirm = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Confirm Screen/Label - Retain Amount'), 
    FailureHandling.CONTINUE_ON_FAILURE)

RetainAmountCreateConfirm = RetainAmountCreateConfirm.replace(': ', '')

WebUI.verifyMatch(RetainAmountCreateConfirm, GlobalVariable.Map123.get('retainamount123'), true, FailureHandling.CONTINUE_ON_FAILURE)

ER12 = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Confirm Screen/Label - Exchange Rate'), 
    FailureHandling.CONTINUE_ON_FAILURE)

ExhangeRateCreateConfirm = ER12.replace(': ', '')

WebUI.verifyMatch(ExhangeRateCreateConfirm, GlobalVariable.Map123.get('ExchangeRate123'), true, FailureHandling.CONTINUE_ON_FAILURE)

TC12 = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Confirm Screen/Label - Total Charge'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TotalChargeCreateConfirm = TC12.replace(': ', '')

WebUI.verifyMatch(TotalChargeCreateConfirm, GlobalVariable.Map123.get('TotalCharge123'), true, FailureHandling.CONTINUE_ON_FAILURE)

TDA12 = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Confirm Screen/Label - Total Debet Amount'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TotalDebetAmountCreateConfirm = TDA12.replace(': ', '')

WebUI.verifyMatch(TotalDebetAmountCreateConfirm, GlobalVariable.Map123.get('TotalDebetAmount123'), true, FailureHandling.CONTINUE_ON_FAILURE)

CI12 = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Confirm Screen/Label - Charge Instruction'), 
    FailureHandling.CONTINUE_ON_FAILURE)

ChargeInstructionCreateConfirm = CI12.replace(': ', '')

WebUI.verifyMatch(ChargeInstructionCreateConfirm, GlobalVariable.Map123.get('ChargeInstruction123'), true, FailureHandling.CONTINUE_ON_FAILURE)

DC12 = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Confirm Screen/Label - Debit Charge'), 
    FailureHandling.CONTINUE_ON_FAILURE)

DebitChargeCreateConfirm = DC12.replace(': ', '')

WebUI.verifyMatch(DebitChargeCreateConfirm, GlobalVariable.Map123.get('DebitCharge123'), true, FailureHandling.CONTINUE_ON_FAILURE)

LHBUPC = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Confirm Screen/Label - LHBU Purpose Code'), 
    FailureHandling.CONTINUE_ON_FAILURE)

LHBUPCCreateConfirm = LHBUPC.replace(': ', '')

WebUI.verifyMatch(LHBUPCCreateConfirm, GlobalVariable.Map123.get('LHBUPurposeCode123'), true, FailureHandling.CONTINUE_ON_FAILURE)

LHBUDT = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Confirm Screen/Label - LHBU Document Type'), 
    FailureHandling.CONTINUE_ON_FAILURE)

LHBUDTCreateConfirm = LHBUDT.replace(': ', '')

WebUI.verifyMatch(LHBUDTCreateConfirm, GlobalVariable.Map123.get('LHBUDocumentType123'), true, FailureHandling.CONTINUE_ON_FAILURE)

LHBUDTD = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Confirm Screen/Label - LHBU Document Type Description'), 
    FailureHandling.CONTINUE_ON_FAILURE)

LHBUDTDCreateConfirm = LHBUDTD.replace(': ', '')

WebUI.verifyMatch(LHBUDTDCreateConfirm, GlobalVariable.Map123.get('LHBUDocumentTypeDescription123'), true, FailureHandling.CONTINUE_ON_FAILURE)

IM12 = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Confirm Screen/Label - Instruction Mode'), 
    FailureHandling.CONTINUE_ON_FAILURE)

InstructionModeCreateConfirm = IM12.replace(': ', '')

WebUI.verifyMatch(InstructionModeCreateConfirm, GlobalVariable.Map123.get('InstructionMode123'), false, FailureHandling.CONTINUE_ON_FAILURE)

FutureDate = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Confirm Screen/Label - Specific Date - Future Date'), 
    FailureHandling.CONTINUE_ON_FAILURE)

FutureDate = FutureDate.replace(': ', '')

WebUI.verifyMatch(FutureDate, GlobalVariable.Map123.get('FutureDate'), false, FailureHandling.CONTINUE_ON_FAILURE)

SpecificAt = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Confirm Screen/Label - Specific Date - At'), 
    FailureHandling.CONTINUE_ON_FAILURE)

SpecificAt = SpecificAt.replace(': ', '')

WebUI.verifyMatch(SpecificAt, GlobalVariable.Map123.get('SpecificAt'), false, FailureHandling.CONTINUE_ON_FAILURE)

ExpOn = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Confirm Screen/Label - Expired On'), 
    FailureHandling.CONTINUE_ON_FAILURE)

ExpOnCreateConfirm = ExpOn.replace(': ', '')

WebUI.verifyMatch(ExpOnCreateConfirm, GlobalVariable.Map123.get('ExpiredOn123'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.scrollToElement(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Result/BTN - Done'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Result/BTN - Done'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(15, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/DropList - Transfer To - Own Account'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

