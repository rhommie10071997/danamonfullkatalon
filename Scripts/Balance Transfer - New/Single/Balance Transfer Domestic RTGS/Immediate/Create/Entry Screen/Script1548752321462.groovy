import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import java.text.DecimalFormat as DecimalFormat
import java.text.NumberFormat as NumberFormat
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

Random rand = new Random()

'From Edit      --------------------------------------------------------------------------------------------------------------------------------------------'

'From Account bisa di isi Account Number atau Nama dia'
FromAccount = 'achmad'

'Amount Angka Depan'
AmountDepan = '100000'

'Amount Angka Belakang dimana jumlah nya akan di random'
AmountRandomBelakang = 999

'From Account Description + 3 angka random di belakang'
FromAccountDescription = 'FromAccDes Ardo Balance RTGS'

'Transfer To Flag 0 = Beneficiary List, Flag 1 = New Beneficiary'
TransferToFlag123 = 0

'Beneficiary List bisa nomor dan nama'
BeneficiaryList = 'minang'

'-------------New Beneficiary List -------------------------------------------'

'Save to Beneficiary List Flag 0 = Yes , Flag 1 = No'
SavetoBeneficiaryList = 0

'Alias Name'
AliasName = 'TestAliasNameArdo'

'Beneficiary Bank'
BeneficiaryBank = 'Permata'

'Account Number'
AccountNumber = '12341234'

'Account Name'
AccountName = 'TestArdo'

'Beneficiary Address 1 2 3'
BeneficiaryAddress1 = 'Jakarta Barat'

BeneficiaryAddress2 = 'Tanjung Duren'

BeneficiaryAddress3 = 'No.98 JalanAsdasdasd'

'Beneficiary notification Email dan Phone'
BeneEmail = 'test123123@gmail.com'

BenePhone = '085395959512'

'------------------------------------------------------------------------------'

'To Account Description = FromAccountDescription'

'BeneficiaryReferenceNo'
BeneficiaryReferenceNo = '1234123412'

'LHBU Purpose Code'
LHBUPurposeCode = '00 - 00'

'LHBUDocumentType'
LHBUDocumentType = '001 - 001'

'LHBU Document Type Description'
LHBUDocumentTypeDescription = 'LHBU Document Type Descriptin Ardo Lets Gow'

'Charge nya ada atau tidak : Yes or No'
Charge = 'Yes'

'----------------------------------------------------------------------------------------------------------------------------------------------------------'
GlobalVariable.Map123.put('TransferToFlag123', TransferToFlag123)

GlobalVariable.Map123.put('ChargeInstruction', 'Remitter')

GlobalVariable.Map123.put('DebitCharge', 'Split')

GlobalVariable.Map123.put('Charge', Charge)

'----------------------------------------------------------------------------------------------------------------------------------------------------------'
WebUI.openBrowser(GlobalVariable.url, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.maximizeWindow()

WebUI.waitForAngularLoad(0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('1 - Frame/1 - iframe login'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Log in/corpid'), 'pcmkillua', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Log in/userid'), 'killua', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Log in/password'), 'Password123', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Log in/login'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(3, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementPresent(findTestObject('Menu/BTN - Menu Object'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Menu/BTN - Menu Object'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementClickable(findTestObject('Menu/BTN - Menu Object'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Menu/BTN - Menu Object'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Menu/BTN - Liquidity Management'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Menu/BTN - Liquidity Management'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Menu/BTN - Balance Transfer'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Menu/BTN - Balance Transfer'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Balance Transfer/Balance Transfer - Domestic/Entry/BTN - Domestic Transfer'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Balance Transfer/Balance Transfer - Domestic/Entry/BTN - Domestic Transfer'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Balance Transfer/Balance Transfer - Domestic/Entry/DropList - From Account'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Balance Transfer/Balance Transfer - Domestic/Entry/TextField - Input DropList'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Balance Transfer/Balance Transfer - Domestic/Entry/TextField - Input DropList'), FromAccount, 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Balance Transfer/Balance Transfer - Domestic/Entry/DropList - Select Account - Highlight'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Balance Transfer/Balance Transfer - Domestic/Entry/DropList - Select Account - Highlight'), 
    FailureHandling.CONTINUE_ON_FAILURE)

FromAccount = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - Domestic/Entry/DropList - From Account'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Map123.put('FromAccount', FromAccount)

WebUI.delay(10, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/Label - Nett Balance'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

'Split Amount , buat di isi ----------------------------------------------------------------------------------------------------------------'
Netbalance = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/Label - Nett Balance'), 
    FailureHandling.CONTINUE_ON_FAILURE)

String[] SplitAmount = Netbalance.split(' ')

GlobalVariable.Map123.put('Currency', SplitAmount[0])

String NetAmount = SplitAmount[1]

String[] ilangtitik = NetAmount.split('\\.')

String c = ilangtitik[0].replaceAll('\\,', '')

int angka

angka = rand.nextInt(AmountRandomBelakang)

angka1 = (AmountDepan + angka)

Locale local1 = new Locale('en', 'UK')

DecimalFormat amountinquiry1 = ((NumberFormat.getNumberInstance(local1)) as DecimalFormat)

amountinquiry1.applyPattern('###,###,##0.00')

String amountinquiry12 = amountinquiry1.format(new BigDecimal(angka1))

GlobalVariable.Map123.put('AmountInquiry', amountinquiry12)

println(amountinquiry12)

Long angka2 = Long.parseLong(angka1)

Long retainamountmines = Long.parseLong(c)

Long retainamount = retainamountmines - angka2

String retainamount1 = retainamount

DecimalFormat retainamountformat = ((NumberFormat.getNumberInstance(local1)) as DecimalFormat)

retainamountformat.applyPattern('###,###,##0.00')

String retainamountformat2 = ((SplitAmount[0]) + ' ') + retainamountformat.format(new BigDecimal(retainamount1))

GlobalVariable.Map123.put('RetainAmount', retainamountformat2)

'------------------------------------------------------------------------------------------------------------------------------------'
Random rand123 = new Random()

int angka123

angka123 = rand123.nextInt(999)

fromaccountdes = (FromAccountDescription + angka123)

WebUI.setText(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Entry/TextField - From Account Description'), 
    fromaccountdes, FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Map123.put('FromAccountDescription', fromaccountdes)

'Transfer To --------------------------------------------------------------------------------------------------------------------------'
if (GlobalVariable.Map123.get('TransferToFlag123') == 0) {
    WebUI.click(findTestObject('Balance Transfer/Balance Transfer - Domestic/Entry/BTN - Transfer To - Beneficiary List'))

    TransferTo2 = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - Domestic/Entry/BTN - Transfer To - Beneficiary List'))

    GlobalVariable.Map123.put('TransferTo', TransferTo2)

    WebUI.click(findTestObject('Balance Transfer/Balance Transfer - Domestic/Entry/DropList - Beneficiary List'))

    WebUI.delay(1)

    WebUI.waitForElementVisible(findTestObject('Balance Transfer/Balance Transfer - Domestic/Entry/TextField - Input DropList'), 
        0)

    WebUI.setText(findTestObject('Balance Transfer/Balance Transfer - Domestic/Entry/TextField - Input DropList'), BeneficiaryList)

    WebUI.delay(2)

    WebUI.waitForElementVisible(findTestObject('Balance Transfer/Balance Transfer - Domestic/Entry/DropList - Select Account - Highlight'), 
        0)

    WebUI.click(findTestObject('Balance Transfer/Balance Transfer - Domestic/Entry/DropList - Select Account - Highlight'))

    BeneficiaryList = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - Domestic/Entry/DropList - Beneficiary List'))

    GlobalVariable.Map123.put('BeneficiaryList', BeneficiaryList)
} else {
    WebUI.click(findTestObject('Balance Transfer/Balance Transfer - Domestic/Entry/BTN - Transfer To - New Beneficiary'))

    TransferTo2 = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - Domestic/Entry/BTN - Transfer To - New Beneficiary'))

    GlobalVariable.Map123.put('TransferTo', TransferTo2)

    GlobalVariable.Map123.put('SavetoBeneficiaryList', 'No')

    if (SavetoBeneficiaryList == 0) {
        WebUI.click(findTestObject('Balance Transfer/Balance Transfer - Domestic/Entry/CheckBox - Save to Beneficiary List'))

        GlobalVariable.Map123.put('SavetoBeneficiaryList', 'Yes')

        String AliasNam = AliasName + angka123

        WebUI.setText(findTestObject('Balance Transfer/Balance Transfer - Domestic/Entry/TextField - Alias Name'), AliasNam)

        GlobalVariable.Map123.put('AliasName', AliasNam)
    }
    
    WebUI.click(findTestObject('Balance Transfer/Balance Transfer - Domestic/Entry/DropList - Beneficiary Bank'))

    WebUI.delay(1)

    WebUI.waitForElementVisible(findTestObject('Balance Transfer/Balance Transfer - Domestic/Entry/TextField - Input DropList'), 
        0)

    WebUI.setText(findTestObject('Balance Transfer/Balance Transfer - Domestic/Entry/TextField - Input DropList'), BeneficiaryBank)

    WebUI.delay(2)

    WebUI.waitForElementVisible(findTestObject('Balance Transfer/Balance Transfer - Domestic/Entry/DropList - Select Account - Highlight'), 
        0)

    WebUI.click(findTestObject('Balance Transfer/Balance Transfer - Domestic/Entry/DropList - Select Account - Highlight'))

    BeneficiaryBank = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - Domestic/Entry/DropList - Beneficiary Bank'))

    GlobalVariable.Map123.put('BeneficiaryBank', BeneficiaryBank)

    WebUI.setText(findTestObject('Balance Transfer/Balance Transfer - Domestic/Entry/TextField - Account Number'), AccountNumber)

    GlobalVariable.Map123.put('AccountNumber', AccountNumber)

    WebUI.setText(findTestObject('Balance Transfer/Balance Transfer - Domestic/Entry/TextField - Account Name'), AccountName)

    GlobalVariable.Map123.put('AccountName', AccountName)

    WebUI.setText(findTestObject('Balance Transfer/Balance Transfer - Domestic/Entry/TextField - Beneficiary Address 1'), 
        BeneficiaryAddress1)

    GlobalVariable.Map123.put('Address1', BeneficiaryAddress1)

    WebUI.setText(findTestObject('Balance Transfer/Balance Transfer - Domestic/Entry/TextField - Beneficiary Address 2'), 
        BeneficiaryAddress2)

    GlobalVariable.Map123.put('Address2', BeneficiaryAddress2)

    WebUI.setText(findTestObject('Balance Transfer/Balance Transfer - Domestic/Entry/TextField - Beneficiary Address 3'), 
        BeneficiaryAddress3)

    GlobalVariable.Map123.put('Address3', BeneficiaryAddress3)

    WebUI.setText(findTestObject('Balance Transfer/Balance Transfer - Domestic/Entry/TextField - Email'), BeneEmail)

    GlobalVariable.Map123.put('Email', BeneEmail)

    WebUI.setText(findTestObject('Balance Transfer/Balance Transfer - Domestic/Entry/TextField - Phone'), BenePhone)

    GlobalVariable.Map123.put('Phone', BenePhone)
}

WebUI.setText(findTestObject('Balance Transfer/Balance Transfer - Domestic/Entry/TextField - To Account Description'), fromaccountdes, 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Map123.put('ToAccountDescription', fromaccountdes)

WebUI.setText(findTestObject('Balance Transfer/Balance Transfer - Domestic/Entry/TextField - Beneficiary Reference No'), 
    BeneficiaryReferenceNo, FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Map123.put('BeneficiaryReferenceNo', BeneficiaryReferenceNo)

WebUI.click(findTestObject('Balance Transfer/Balance Transfer - Domestic/Entry/BTN - Transfer Method - RTGS'), FailureHandling.CONTINUE_ON_FAILURE)

TransferMethod = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - Domestic/Entry/BTN - Transfer Method - RTGS'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Map123.put('TransferMethod', TransferMethod)

WebUI.setText(findTestObject('Balance Transfer/Balance Transfer - Domestic/Entry/TextField - Retained Amount'), retainamount1, 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Balance Transfer/Balance Transfer - Domestic/Entry/DropList - LHBU Purpose Code'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Balance Transfer/Balance Transfer - Domestic/Entry/TextField - Input DropList'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Balance Transfer/Balance Transfer - Domestic/Entry/TextField - Input DropList'), LHBUPurposeCode, 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Balance Transfer/Balance Transfer - Domestic/Entry/DropList - Select Account - Highlight'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Balance Transfer/Balance Transfer - Domestic/Entry/DropList - Select Account - Highlight'), 
    FailureHandling.CONTINUE_ON_FAILURE)

LHBUPurposeCode = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - Domestic/Entry/DropList - LHBU Purpose Code'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Map123.put('LHBUPurposeCode', LHBUPurposeCode)

WebUI.click(findTestObject('Balance Transfer/Balance Transfer - Domestic/Entry/DropList - LHBU Document Type'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Balance Transfer/Balance Transfer - Domestic/Entry/TextField - Input DropList'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Balance Transfer/Balance Transfer - Domestic/Entry/TextField - Input DropList'), LHBUDocumentType, 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Balance Transfer/Balance Transfer - Domestic/Entry/DropList - Select Account - Highlight'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Balance Transfer/Balance Transfer - Domestic/Entry/DropList - Select Account - Highlight'), 
    FailureHandling.CONTINUE_ON_FAILURE)

LHBUDocumentType = WebUI.getText(findTestObject('Balance Transfer/Balance Transfer - Domestic/Entry/DropList - LHBU Document Type'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Map123.put('LHBUDocumentType', LHBUDocumentType)

WebUI.setText(findTestObject('Balance Transfer/Balance Transfer - Domestic/Entry/TextField - LHBU Document Type Description'), 
    LHBUDocumentTypeDescription, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Map123.put('LHBUDocumentTypeDescription', LHBUDocumentTypeDescription)

GlobalVariable.Map123.put('InstructionMode', 'Immediate')

WebUI.click(findTestObject('Balance Transfer/Balance Transfer - Domestic/Entry/BTN - Confirm'), FailureHandling.CONTINUE_ON_FAILURE)

