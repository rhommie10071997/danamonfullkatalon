import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

SubTitle = WebUI.getText(findTestObject('Pending Task/Balance Transfer/Inhouse/Detail/Label - Sub Title'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(SubTitle, 'Reference Number Detail', true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Menu'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Menu = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Menu'), FailureHandling.CONTINUE_ON_FAILURE)

Menu = Menu.replace('    : ', '')

WebUI.verifyMatch(Menu, 'Balance Transfer', true, FailureHandling.CONTINUE_ON_FAILURE)

Product = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Product'), FailureHandling.CONTINUE_ON_FAILURE)

Product = Product.replace('    : ', '')

WebUI.verifyMatch(Product, 'RTGS', true, FailureHandling.CONTINUE_ON_FAILURE)

TransactionReferenceNo = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Transaction Reference No'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TransactionReferenceNo = TransactionReferenceNo.replace('    : ', '')

WebUI.verifyMatch(TransactionReferenceNo, GlobalVariable.Map123.get('RefNo'), false, FailureHandling.CONTINUE_ON_FAILURE)

DocumentCode = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Document Code'), 
    FailureHandling.CONTINUE_ON_FAILURE)

DocumentCode = DocumentCode.replace('    : ', '')

WebUI.verifyMatch(DocumentCode, GlobalVariable.Map123.get('RefNo'), false, FailureHandling.CONTINUE_ON_FAILURE)

FromAccount = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - From Account'), 
    FailureHandling.CONTINUE_ON_FAILURE)

FromAccount = FromAccount.replace(': ', '')

WebUI.verifyMatch(FromAccount, GlobalVariable.Map123.get('FromAccount'), false, FailureHandling.CONTINUE_ON_FAILURE)

FromAccountDescription = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - From Account Description'), 
    FailureHandling.CONTINUE_ON_FAILURE)

FromAccountDescription = FromAccountDescription.replace(': ', '')

WebUI.verifyMatch(FromAccountDescription, GlobalVariable.Map123.get('FromAccountDescription'), false, FailureHandling.CONTINUE_ON_FAILURE)

TransferTo = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Transfer To'), FailureHandling.CONTINUE_ON_FAILURE)

TransferTo = TransferTo.replace(': ', '')

WebUI.verifyMatch(TransferTo, GlobalVariable.Map123.get('TransferTo'), false, FailureHandling.CONTINUE_ON_FAILURE)

if (GlobalVariable.Map123.get('TransferTo') == 'Beneficiary List') {
    BeneficiaryList = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Beneficiary List'))

    BeneficiaryList = BeneficiaryList.replace(': ', '')

    WebUI.verifyMatch(BeneficiaryList, GlobalVariable.Map123.get('BeneficiaryList'), false)

    BeneficiaryBank = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Beneficiary Bank'))

    BeneficiaryBank = BeneficiaryBank.replace(': ', '')

    BeneficiaryBank = BeneficiaryBank.replace(':', '')

    WebUI.verifyMatch(BeneficiaryBank, GlobalVariable.Map123.get('BeneficiaryBank'), false)

    BankBranch = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Bank Branch'))

    BankBranch = BankBranch.replace(': ', '')

    BankBranch = BankBranch.replace(':', '')

    WebUI.verifyMatch(BankBranch, GlobalVariable.Map123.get('BankBranch'), false)

    BankCity = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Bank City'))

    BankCity = BankCity.replace(': ', '')

    BankCity = BankCity.replace(':', '')

    WebUI.verifyMatch(BankCity, GlobalVariable.Map123.get('BankCity'), false)

    AccountNumber = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Account Number'))

    AccountNumber = AccountNumber.replace(': ', '')

    AccountNumber = AccountNumber.replace(':', '')

    WebUI.verifyMatch(AccountNumber, GlobalVariable.Map123.get('AccountNumber'), false)

    AccountName = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Account Name'))

    AccountName = AccountName.replace(': ', '')

    AccountName = AccountName.replace(':', '')

    WebUI.verifyMatch(AccountName, GlobalVariable.Map123.get('AccountName'), false)

    Address1 = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Address 1'))

    Address1 = Address1.replace(': ', '')

    Address1 = Address1.replace(':', '')

    WebUI.verifyMatch(Address1, GlobalVariable.Map123.get('Address1'), false)

    Address2 = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Address 2'))

    Address2 = Address2.replace(': ', '')

    Address2 = Address2.replace(':', '')

    WebUI.verifyMatch(Address2, GlobalVariable.Map123.get('Address2'), false)

    Address3 = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Address 3'))

    Address3 = Address3.replace(': ', '')

    Address3 = Address3.replace(':', '')

    WebUI.verifyMatch(Address3, GlobalVariable.Map123.get('Address3'), false)

    Email = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Email'))

    Email = Email.replace(': ', '')

    Email = Email.replace(':', '')

    WebUI.verifyMatch(Email, GlobalVariable.Map123.get('Email'), false)

    Phone = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Phone'))

    Phone = Phone.replace(': ', '')

    Phone = Phone.replace(':', '')

    WebUI.verifyMatch(Phone, GlobalVariable.Map123.get('Phone'), false)
} else {
    SavetoBeneficiaryList = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Save to Beneficiary List'))

    SavetoBeneficiaryList = SavetoBeneficiaryList.replace(': ', '')

    SavetoBeneficiaryList = SavetoBeneficiaryList.replace(':', '')

    WebUI.verifyMatch(SavetoBeneficiaryList, GlobalVariable.Map123.get('SavetoBeneficiaryList'), false)

    AliasName = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Alias Name'))

    AliasName = AliasName.replace(': ', '')

    AliasName = AliasName.replace(':', '')

    WebUI.verifyMatch(AliasName, GlobalVariable.Map123.get('AliasName'), false)

    BeneficiaryBank = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Beneficiary Bank'))

    BeneficiaryBank = BeneficiaryBank.replace(': ', '')

    BeneficiaryBank = BeneficiaryBank.replace(':', '')

    WebUI.verifyMatch(BeneficiaryBank, GlobalVariable.Map123.get('BeneficiaryBank'), false)

    BankBranch = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Bank Branch'))

    BankBranch = BankBranch.replace(': ', '')

    BankBranch = BankBranch.replace(':', '')

    WebUI.verifyMatch(BankBranch, GlobalVariable.Map123.get('BankBranch'), false)

    BankCity = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Bank City'))

    BankCity = BankCity.replace(': ', '')

    BankCity = BankCity.replace(':', '')

    WebUI.verifyMatch(BankCity, GlobalVariable.Map123.get('BankCity'), false)

    AccountNumber = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Account Number'))

    AccountNumber = AccountNumber.replace(': ', '')

    AccountNumber = AccountNumber.replace(':', '')

    WebUI.verifyMatch(AccountNumber, GlobalVariable.Map123.get('AccountNumber'), false)

    AccountName = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Account Name'))

    AccountName = AccountName.replace(': ', '')

    AccountName = AccountName.replace(':', '')

    WebUI.verifyMatch(AccountName, GlobalVariable.Map123.get('AccountName'), false)

    Address1 = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Address 1'))

    Address1 = Address1.replace(': ', '')

    Address1 = Address1.replace(':', '')

    WebUI.verifyMatch(Address1, GlobalVariable.Map123.get('Address1'), false)

    Address2 = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Address 2'))

    Address2 = Address2.replace(': ', '')

    Address2 = Address2.replace(':', '')

    WebUI.verifyMatch(Address2, GlobalVariable.Map123.get('Address2'), false)

    Address3 = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Address 3'))

    Address3 = Address3.replace(': ', '')

    Address3 = Address3.replace(':', '')

    WebUI.verifyMatch(Address3, GlobalVariable.Map123.get('Address3'), false)

    Email = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Email'))

    Email = Email.replace(': ', '')

    Email = Email.replace(':', '')

    WebUI.verifyMatch(Email, GlobalVariable.Map123.get('Email'), false)

    Phone = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Phone'))

    Phone = Phone.replace(': ', '')

    Phone = Phone.replace(':', '')

    WebUI.verifyMatch(Phone, GlobalVariable.Map123.get('Phone'), false)
}

ToAccountDescription = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - To Account Description'), 
    FailureHandling.CONTINUE_ON_FAILURE)

ToAccountDescription = ToAccountDescription.replace(': ', '')

ToAccountDescription = ToAccountDescription.replace(':', '')

WebUI.verifyMatch(ToAccountDescription, GlobalVariable.Map123.get('ToAccountDescription'), false, FailureHandling.CONTINUE_ON_FAILURE)

BeneficiaryReferenceNo = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Beneficiary Reference No'), 
    FailureHandling.CONTINUE_ON_FAILURE)

BeneficiaryReferenceNo = BeneficiaryReferenceNo.replace(': ', '')

BeneficiaryReferenceNo = BeneficiaryReferenceNo.replace(':', '')

WebUI.verifyMatch(BeneficiaryReferenceNo, GlobalVariable.Map123.get('BeneficiaryReferenceNo'), false, FailureHandling.CONTINUE_ON_FAILURE)

TransferMethod = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Transfer Method'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TransferMethod = TransferMethod.replace(': ', '')

TransferMethod = TransferMethod.replace(':', '')

WebUI.verifyMatch(TransferMethod, GlobalVariable.Map123.get('TransferMethod'), false, FailureHandling.CONTINUE_ON_FAILURE)

RetainedAmount = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Retained Amount'), 
    FailureHandling.CONTINUE_ON_FAILURE)

RetainedAmount = RetainedAmount.replace(': ', '')

RetainedAmount = RetainedAmount.replace(':', '')

WebUI.verifyMatch(RetainedAmount, GlobalVariable.Map123.get('RetainAmount'), false, FailureHandling.CONTINUE_ON_FAILURE)

RemittanceCurrency = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Remittance Currency'), 
    FailureHandling.CONTINUE_ON_FAILURE)

RemittanceCurrency = RemittanceCurrency.replace(': ', '')

RemittanceCurrency = RemittanceCurrency.replace(':', '')

WebUI.verifyMatch(RemittanceCurrency, GlobalVariable.Map123.get('Currency'), false, FailureHandling.CONTINUE_ON_FAILURE)

ExchangeRate = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Exchange Rate'), 
    FailureHandling.CONTINUE_ON_FAILURE)

ExchangeRate = ExchangeRate.replace(': ', '')

ExchangeRate = ExchangeRate.replace(':', '')

WebUI.verifyMatch(ExchangeRate, GlobalVariable.Map123.get('ExchangeRate'), false, FailureHandling.CONTINUE_ON_FAILURE)

RTGSFee = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - RTGS Fee'), FailureHandling.CONTINUE_ON_FAILURE)

RTGSFee = RTGSFee.replace(': ', '')

RTGSFee = RTGSFee.replace(':', '')

WebUI.verifyMatch(RTGSFee, GlobalVariable.Map123.get('RTGSFee'), false, FailureHandling.CONTINUE_ON_FAILURE)

TotalDebitAmount = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Total Debit Amount'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TotalDebitAmount = TotalDebitAmount.replace(': ', '')

TotalDebitAmount = TotalDebitAmount.replace(':', '')

WebUI.verifyMatch(TotalDebitAmount, GlobalVariable.Map123.get('TotalDebitAmount'), false, FailureHandling.CONTINUE_ON_FAILURE)

ChargeInstruction = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Charge Instruction'), 
    FailureHandling.CONTINUE_ON_FAILURE)

ChargeInstruction = ChargeInstruction.replace(': ', '')

ChargeInstruction = ChargeInstruction.replace(':', '')

WebUI.verifyMatch(ChargeInstruction, GlobalVariable.Map123.get('ChargeInstruction'), false, FailureHandling.CONTINUE_ON_FAILURE)

DebitCharge = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Debit Charge'), 
    FailureHandling.CONTINUE_ON_FAILURE)

DebitCharge = DebitCharge.replace(': ', '')

DebitCharge = DebitCharge.replace(':', '')

WebUI.verifyMatch(DebitCharge, GlobalVariable.Map123.get('DebitCharge'), false, FailureHandling.CONTINUE_ON_FAILURE)

LHBUPurposeCode = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - LHBU Purpose Code'), 
    FailureHandling.CONTINUE_ON_FAILURE)

LHBUPurposeCode = LHBUPurposeCode.replace(': ', '')

LHBUPurposeCode = LHBUPurposeCode.replace(':', '')

WebUI.verifyMatch(LHBUPurposeCode, GlobalVariable.Map123.get('LHBUPurposeCode'), false, FailureHandling.CONTINUE_ON_FAILURE)

LHBUDocumentType = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - LHBU Document Type'), 
    FailureHandling.CONTINUE_ON_FAILURE)

LHBUDocumentType = LHBUDocumentType.replace(': ', '')

LHBUDocumentType = LHBUDocumentType.replace(':', '')

WebUI.verifyMatch(LHBUDocumentType, GlobalVariable.Map123.get('LHBUDocumentType'), false, FailureHandling.CONTINUE_ON_FAILURE)

LHBUDocumentTypeDescription = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - LHBU Document Type Description'), 
    FailureHandling.CONTINUE_ON_FAILURE)

LHBUDocumentTypeDescription = LHBUDocumentTypeDescription.replace(': ', '')

LHBUDocumentTypeDescription = LHBUDocumentTypeDescription.replace(':', '')

WebUI.verifyMatch(LHBUDocumentTypeDescription, GlobalVariable.Map123.get('LHBUDocumentTypeDescription'), false, FailureHandling.CONTINUE_ON_FAILURE)

InstructionMode = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Instuction Mode'), 
    FailureHandling.CONTINUE_ON_FAILURE)

InstructionMode = InstructionMode.replace(': ', '')

InstructionMode = InstructionMode.replace(':', '')

WebUI.verifyMatch(InstructionMode, GlobalVariable.Map123.get('InstructionMode'), false, FailureHandling.CONTINUE_ON_FAILURE)

Expiredon = WebUI.getText(findTestObject('Transaction Status/Balance Transfer - SKN/New Ref No/Label - Expired on'), FailureHandling.CONTINUE_ON_FAILURE)

Expiredon = Expiredon.replace(': ', '')

Expiredon = Expiredon.replace(':', '')

WebUI.verifyMatch(Expiredon, GlobalVariable.Map123.get('Expiredon'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.scrollToElement(findTestObject('Transaction Status/Balance Transfer - Inhouse/New - Ref No Detail/BTN - Document Number'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Transaction Status/Balance Transfer - Inhouse/New - Ref No Detail/BTN - Document Number'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

