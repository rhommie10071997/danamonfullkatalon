import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import internal.GlobalVariable as GlobalVariable

/*
String Sub1Acc = GlobalVariable.Sub1Account222
String[] Sub1Acc1 = Sub1Acc.split(' - ')
String Sub1Acc11 = Sub1Acc1[0]
*/

String Sub1Acc = GlobalVariable.Map123.get("Sub1Account")
Sub1Acc = Sub1Acc.replace('(',' (')
GlobalVariable.Map123.put("Sub1Account",Sub1Acc)

String Sub1Amm = GlobalVariable.Map123.get("Sub1Amount")
String Sub1Ammo = Sub1Amm.substring(0, 3)+" "+Sub1Amm.substring(3);




GlobalVariable.TTDataTables222 = new ArrayList()
int TRow = GlobalVariable.Map123.get("AccountRand")

VerifyRow = new ArrayList()


WebDriver driver = DriverFactory.getWebDriver()
driver.switchTo().frame("login");
driver.switchTo().frame("mainFrame");

WebElement Table = driver.findElement(By.xpath('//table[@id="globalTableTarget"]/tbody'))
List<WebElement> Rows = Table.findElements(By.tagName('tr'))

	
List<WebElement> Cols1 = Rows.get(0).findElements(By.tagName('td'))

	SubAccount1=''+Cols1.get(0).getText()
	RetainAmount1=''+Cols1.get(1).getText()
	SubAccountDescription1=''+Cols1.get(2).getText()
	MainAccountDescription1=''+Cols1.get(3).getText()
	

	VerifyRow.add(GlobalVariable.Map123.get("Sub1Account")+"---"+Sub1Ammo+"---"+GlobalVariable.Map123.get("Description")+"---"+GlobalVariable.Map123.get("Description"))	
	GlobalVariable.TTDataTables222.add(SubAccount1+"---"+RetainAmount1+"---"+SubAccountDescription1+"---"+MainAccountDescription1)

	Collections.sort(VerifyRow)
	Collections.sort(GlobalVariable.TTDataTables222)
	
	
int trow = GlobalVariable.Map123.get("AccountRand")
		for (i=0;i<trow;i++) {
			WebUI.verifyMatch(VerifyRow[i], GlobalVariable.TTDataTables222[i], false)
		}
	
	WebUI.switchToDefaultContent()

