import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.waitForElementVisible(findTestObject('Cash Pooling/Confirm Screen/Label - Main Account - Account'), 0, FailureHandling.CONTINUE_ON_FAILURE)

LabelConfirm = WebUI.getText(findTestObject('Cash Pooling/Confirm Screen/Label - Sub Title - Confirm'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(LabelConfirm, 'Confirm', false, FailureHandling.CONTINUE_ON_FAILURE)

MainAccount = WebUI.getText(findTestObject('Cash Pooling/Confirm Screen/Label - Main Account - Account'), FailureHandling.CONTINUE_ON_FAILURE)

String MainAccount = MainAccount.replace(': ', '')

WebUI.verifyMatch(MainAccount, GlobalVariable.MainAccount222, false, FailureHandling.CONTINUE_ON_FAILURE)

SetupSweepPriority = WebUI.getText(findTestObject('Cash Pooling/Confirm Screen/Label - Setup Sweep Priority'), FailureHandling.CONTINUE_ON_FAILURE)

String SetupSweepPriority = SetupSweepPriority.replace(': ', '')

WebUI.verifyMatch(SetupSweepPriority, GlobalVariable.SetupSweepPriority222, false, FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.SetupSweepPriority222 = SetupSweepPriority

AutoReverse = WebUI.getText(findTestObject('Cash Pooling/Confirm Screen/Label - Auto Reverse'), FailureHandling.CONTINUE_ON_FAILURE)

String AutoReverse = AutoReverse.replace(': ', '')

if (GlobalVariable.RandomAutoReverse222 == '1') {
    WebUI.verifyMatch(AutoReverse, 'Yes', false)

    GlobalVariable.RandomAutoReverse222 = AutoReverse
} else {
    WebUI.verifyMatch(AutoReverse, 'No', false)

    GlobalVariable.RandomAutoReverse222 = AutoReverse
}

AmountType = WebUI.getText(findTestObject('Cash Pooling/Confirm Screen/Label - Amount Type'), FailureHandling.CONTINUE_ON_FAILURE)

String AmountType = AmountType.replace(': ', '')

WebUI.verifyMatch(AmountType, 'Fixed', false, FailureHandling.CONTINUE_ON_FAILURE)

DiagramMainAccount = WebUI.getText(findTestObject('Cash Pooling/Confirm Screen/Label - Diagram - Main Account'), FailureHandling.CONTINUE_ON_FAILURE)

String DiagramMainAcc = DiagramMainAccount.substring(13)

WebUI.verifyMatch(DiagramMainAcc, GlobalVariable.MainAccount222, false, FailureHandling.CONTINUE_ON_FAILURE)

DiagramSubAccount1 = WebUI.getText(findTestObject('Cash Pooling/Confirm Screen/Label - Diagram - Sub Account 1'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(DiagramSubAccount1, GlobalVariable.Sub1Account222, false, FailureHandling.CONTINUE_ON_FAILURE)

if (GlobalVariable.AccountRand222 == 2) {
    DiagramSubAccount2 = WebUI.getText(findTestObject('Cash Pooling/Confirm Screen/Label - Diagram - Sub Account 2'))

    WebUI.verifyMatch(DiagramSubAccount2, GlobalVariable.Sub2Account222, false)
}

if (GlobalVariable.AccountRand222 == 1) {
    WebUI.callTestCase(findTestCase('Cash Pooling/Table Loop/Get Text 1'), [('SubAccount') : 0, ('RetainAmount') : 1, ('SubAccountDescription') : 2
            , ('MainAccountDescription') : 3], FailureHandling.STOP_ON_FAILURE)
} else {
    WebUI.callTestCase(findTestCase('Cash Pooling/Table Loop/Get Text 2'), [('SubAccount') : 0, ('RetainAmount') : 1, ('SubAccountDescription') : 2
            , ('MainAccountDescription') : 3], FailureHandling.STOP_ON_FAILURE)
}

WebUI.delay(3, FailureHandling.CONTINUE_ON_FAILURE)

DataTableInfo = WebUI.getText(findTestObject('Cash Pooling/New Entry/Label - Data Table Info'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(DataTableInfo, GlobalVariable.DataTablesDetails222, false, FailureHandling.CONTINUE_ON_FAILURE)

CashConcentrationFee = WebUI.getText(findTestObject('Cash Pooling/Confirm Screen/Label - Cash Concentration Fee'))

String CashConcentrationFee = CashConcentrationFee.replace(': ', '')

GlobalVariable.CashConcentrationFee222 = CashConcentrationFee

TotalFee = WebUI.getText(findTestObject('Cash Pooling/Confirm Screen/Label - Total Fee'))

String TotalFee = TotalFee.replace(': ', '')

GlobalVariable.TotalFee222 = TotalFee

InstructionMode = WebUI.getText(findTestObject('Cash Pooling/Confirm Screen/Label - Instruction Mode'))

String InstructionMode = InstructionMode.replace(': ', '')

WebUI.verifyMatch(InstructionMode, GlobalVariable.InstructionMode123, false, FailureHandling.CONTINUE_ON_FAILURE)

RepeatOn = WebUI.getText(findTestObject('Cash Pooling/Confirm Screen/Label - Repeat - On'))

String RepeatOn = RepeatOn.replace(': ', '')

WebUI.verifyMatch(RepeatOn, GlobalVariable.OnRepeatDate123, false)

RepeatEvery = WebUI.getText(findTestObject('Cash Pooling/Confirm Screen/Label - Repeat - Every'))

String RepeatEvery = RepeatEvery.replace(': ', '')

WebUI.verifyMatch(RepeatEvery, GlobalVariable.EveryRepeatDate123, false)

RepeatAt = WebUI.getText(findTestObject('Cash Pooling/Confirm Screen/Label - Repeat - At'))

String RepeatAt = RepeatAt.replace(': ', '')

WebUI.verifyMatch(RepeatAt, GlobalVariable.AtRepeatDate123, false)

RepeatStart = WebUI.getText(findTestObject('Cash Pooling/Confirm Screen/Label - Repeat - Start'))

String RepeatStart = RepeatStart.replace(': ', '')

GlobalVariable.StartRepeatDate123 = RepeatStart

RepeatEnd = WebUI.getText(findTestObject('Cash Pooling/Confirm Screen/Label - Repeat - End'))

String RepeatEnd = RepeatEnd.replace(': ', '')

GlobalVariable.EndRepeatDate123 = RepeatEnd

RepeatNonWorkingDayInstruction = WebUI.getText(findTestObject('Cash Pooling/Confirm Screen/Label - Repeat - Non-Working Day Instruction'))

String RepeatNonWorkingDayInstruction = RepeatNonWorkingDayInstruction.replace(': ', '')

WebUI.verifyMatch(RepeatNonWorkingDayInstruction, GlobalVariable.NonWorkingDayInstructionRepeatDate123, false)

WebUI.click(findTestObject('Single Billing/Generate Billing ID/Confirm/BTN - Submit'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Pending Task/Balance Transfer/Inhouse/Detail/TextField - Response Code'), 0, FailureHandling.CONTINUE_ON_FAILURE)

String abc = '123456'

WebUI.setText(findTestObject('Pending Task/Balance Transfer/Inhouse/Detail/TextField - Response Code'), abc, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Single Billing/Generate Billing ID/Confirm/BTN - Submit'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Single Billing/Generate Billing ID/Confirm/BTN - Pop Up OK'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Single Billing/Generate Billing ID/Confirm/BTN - Pop Up OK'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

