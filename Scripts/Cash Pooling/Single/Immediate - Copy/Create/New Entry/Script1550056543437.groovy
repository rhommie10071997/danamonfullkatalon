import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.junit.After as After
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint

'------------------------------------------------------------------------------------------------------------'
MainAccount = '000094611548'

SubAccount1 = '000021942610'

SubAccount2 = '000001115344'

SubAccount1Amount = '250'

SubAccount2Amount = '250'

Charge = 'Yes'

'------------------------------------------------------------------------------------------------------------'
GlobalVariable.Map123.put('Charge', Charge)

GlobalVariable.Map123.put('SubAccount1Amount', SubAccount1Amount)

GlobalVariable.Map123.put('SubAccount2Amount', SubAccount2Amount)

'------------------------------------------------------------------------------------------------------------'
GlobalVariable.Map123.put('MainAccount', MainAccount)

GlobalVariable.Map123.put('Sub1Account', SubAccount1)

GlobalVariable.Map123.put('Sub2Account', SubAccount2)

Random random = new Random()

int Account = random.nextInt(2) + 1

GlobalVariable.Map123.put('AccountRand', Account)

int SetupSweepPrio = random.nextInt(999)

GlobalVariable.Map123.put('SetupSweepPriority', '' + SetupSweepPrio)

'------------------------------------------------------------------------------------------------------------'
WebUI.openBrowser(GlobalVariable.url, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('1 - Frame/1 - iframe login'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Log in/corpid'), 'pcmkillua', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Log in/userid'), 'killua', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Log in/password'), 'Password123', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Log in/login'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(3, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.callTestCase(findTestCase('Cash Pooling/Check Nett Balance/Check Nett Balance'), [:], FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Menu/BTN - Menu Object'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Menu/TextField - Search'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Menu/TextField - Search'), ' ', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Menu/TextField - Search'), 'Cash Pooling', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Menu/Label - Cash Pooling (By Search)'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Menu/Label - Cash Pooling (By Search)'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Cash Pooling/New Entry/Label - Title Cash Pooling'), 0, FailureHandling.CONTINUE_ON_FAILURE)

TitleCashPooling = WebUI.getText(findTestObject('Cash Pooling/New Entry/Label - Title Cash Pooling'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TitleCashPooling, 'Cash Pooling', false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Cash Pooling/New Entry/Drop List - Main Account - Select Account'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Cash Pooling/New Entry/Text Field - Set Text'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Cash Pooling/New Entry/Text Field - Set Text'), GlobalVariable.Map123.get('MainAccount'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Cash Pooling/New Entry/Drop List - Highlight or 1'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Cash Pooling/New Entry/Drop List - Highlight or 1'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

MainAccount = WebUI.getText(findTestObject('Cash Pooling/New Entry/Drop List - Main Account - Select Account'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Map123.put('MainAccount', MainAccount)

WebUI.setText(findTestObject('Cash Pooling/New Entry/Text Field - Setup Sweep Priority'), GlobalVariable.Map123.get('SetupSweepPriority'), 
    FailureHandling.CONTINUE_ON_FAILURE)

int RandomAutoRevers = random.nextInt(2) + 1

String RandomAutoReverse = RandomAutoRevers

GlobalVariable.Map123.put('RandomAutoReverse', RandomAutoReverse)

if (GlobalVariable.Map123.get('RandomAutoReverse') == '1') {
    WebUI.click(findTestObject('Cash Pooling/New Entry/BTN - Auto Reverse'))
}

WebUI.click(findTestObject('Cash Pooling/New Entry/BTN - Amount Type - Fixed'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Cash Pooling/New Entry/BTN - Plus'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Cash Pooling/New Entry/Drop List - Sub Account Row 1'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Cash Pooling/New Entry/Drop List - Sub Account Row 1'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Cash Pooling/New Entry/Text Field - Set Text'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Cash Pooling/New Entry/Text Field - Set Text'), GlobalVariable.Map123.get('Sub1Account'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Cash Pooling/New Entry/Drop List - Highlight or 1'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Cash Pooling/New Entry/Drop List - Highlight or 1'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

SubAccountRow1 = WebUI.getText(findTestObject('Cash Pooling/New Entry/Drop List - Sub Account Row 1'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Map123.put('Sub1Account', SubAccountRow1)

WebUI.setText(findTestObject('Cash Pooling/New Entry/Text Field - Retain Amount 1'), GlobalVariable.Map123.get('Sub1AmountOutFormat'), 
    FailureHandling.CONTINUE_ON_FAILURE)

Random SubAccRand = new Random()

int SubAccount = SubAccRand.nextInt(99)

GlobalVariable.Map123.put('Description', 'SubAccDesc Ardo ' + SubAccount)

WebUI.setText(findTestObject('Cash Pooling/New Entry/Text Field - Sub Account Description 1'), GlobalVariable.Map123.get(
        'Description'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Cash Pooling/New Entry/Text Field - Main Account Description 1'), GlobalVariable.Map123.get(
        'Description'), FailureHandling.CONTINUE_ON_FAILURE)

if (GlobalVariable.Map123.get('AccountRand') == 2) {
    WebUI.click(findTestObject('Cash Pooling/New Entry/BTN - Plus'))

    WebUI.waitForElementVisible(findTestObject('Cash Pooling/New Entry/Drop List - Sub Account Row 2'), 0)

    WebUI.click(findTestObject('Cash Pooling/New Entry/Drop List - Sub Account Row 2'))

    WebUI.waitForElementVisible(findTestObject('Cash Pooling/New Entry/Text Field - Set Text'), 0)

    WebUI.setText(findTestObject('Cash Pooling/New Entry/Text Field - Set Text'), GlobalVariable.Map123.get('Sub2Account'))

    WebUI.waitForElementVisible(findTestObject('Cash Pooling/New Entry/Drop List - Highlight or 1'), 0)

    WebUI.click(findTestObject('Cash Pooling/New Entry/Drop List - Highlight or 1'))

    WebUI.delay(1)

    SubAccountRow2 = WebUI.getText(findTestObject('Cash Pooling/New Entry/Drop List - Sub Account Row 2'))

    GlobalVariable.Map123.put('Sub2Account', SubAccountRow2)

    WebUI.setText(findTestObject('Cash Pooling/New Entry/Text Field - Retain Amount 2'), GlobalVariable.Map123.get('Sub2AmountOutFormat'))

    WebUI.setText(findTestObject('Cash Pooling/New Entry/Text Field - Sub Account Description 2'), GlobalVariable.Map123.get(
            'Description'))

    WebUI.setText(findTestObject('Cash Pooling/New Entry/Text Field - Main Account Description 2'), GlobalVariable.Map123.get(
            'Description'))
}

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

DataTableInfo = WebUI.getText(findTestObject('Cash Pooling/New Entry/Label - Data Table Info'), FailureHandling.CONTINUE_ON_FAILURE)

if (GlobalVariable.Map123.get('AccountRand') == 1) {
    WebUI.verifyMatch(DataTableInfo, 'Showing 1 to 1 of 1 entries', false)
} else {
    WebUI.verifyMatch(DataTableInfo, 'Showing 1 to 2 of 2 entries', false)
}

GlobalVariable.Map123.put('DataTablesDetails', DataTableInfo)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

InstructionModeActive = WebUI.getText(findTestObject('Cash Pooling/New Entry/Label - Instruction Mode'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Map123.put('InstructionMode', InstructionModeActive)

WebUI.click(findTestObject('Cash Pooling/New Entry/Checkbox - Terms and Condition'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Cash Pooling/New Entry/BTN - Confirm'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

