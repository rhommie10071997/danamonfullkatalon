import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.junit.After as After
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint

GlobalVariable.MainAccount222 = '000002814846'

GlobalVariable.Sub1Account222 = '003571373608'

GlobalVariable.Sub2Account222 = '000001115344'

Random random = new Random()

int Account = random.nextInt(2) + 1

GlobalVariable.AccountRand222 = Account

int SetupSweepPrio = random.nextInt(999)

GlobalVariable.SetupSweepPriority222 = ('' + SetupSweepPrio)

WebUI.openBrowser(GlobalVariable.url, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('1 - Frame/1 - iframe login'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Log in/corpid'), 'pcmkillua', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Log in/userid'), 'killua', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Log in/password'), 'Password123', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Log in/login'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(3, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.callTestCase(findTestCase('Cash Pooling/Check Nett Balance/Check Nett Balance'), [:], FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Menu/BTN - Menu Object'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Menu/TextField - Search'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Menu/TextField - Search'), ' ', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Menu/TextField - Search'), 'Cash Pooling', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Menu/Label - Cash Pooling (By Search)'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Menu/Label - Cash Pooling (By Search)'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Cash Pooling/New Entry/Label - Title Cash Pooling'), 0, FailureHandling.CONTINUE_ON_FAILURE)

TitleCashPooling = WebUI.getText(findTestObject('Cash Pooling/New Entry/Label - Title Cash Pooling'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TitleCashPooling, 'Cash Pooling', false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Cash Pooling/New Entry/Drop List - Main Account - Select Account'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Cash Pooling/New Entry/Text Field - Set Text'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Cash Pooling/New Entry/Text Field - Set Text'), GlobalVariable.MainAccount222, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Cash Pooling/New Entry/Drop List - Highlight or 1'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Cash Pooling/New Entry/Drop List - Highlight or 1'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

MainAccount = WebUI.getText(findTestObject('Cash Pooling/New Entry/Drop List - Main Account - Select Account'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.MainAccount222 = MainAccount

WebUI.setText(findTestObject('Cash Pooling/New Entry/Text Field - Setup Sweep Priority'), GlobalVariable.SetupSweepPriority222, 
    FailureHandling.CONTINUE_ON_FAILURE)

int RandomAutoRevers = random.nextInt(2) + 1

String RandomAutoReverse = RandomAutoRevers

GlobalVariable.RandomAutoReverse222 = RandomAutoReverse

if (GlobalVariable.RandomAutoReverse222 == '1') {
    WebUI.click(findTestObject('Cash Pooling/New Entry/BTN - Auto Reverse'))
}

WebUI.click(findTestObject('Cash Pooling/New Entry/BTN - Amount Type - Fixed'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Cash Pooling/New Entry/BTN - Plus'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Cash Pooling/New Entry/Drop List - Sub Account Row 1'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Cash Pooling/New Entry/Drop List - Sub Account Row 1'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Cash Pooling/New Entry/Text Field - Set Text'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Cash Pooling/New Entry/Text Field - Set Text'), GlobalVariable.Sub1Account222, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Cash Pooling/New Entry/Drop List - Highlight or 1'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Cash Pooling/New Entry/Drop List - Highlight or 1'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

SubAccountRow1 = WebUI.getText(findTestObject('Cash Pooling/New Entry/Drop List - Sub Account Row 1'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Sub1Account222 = SubAccountRow1

WebUI.setText(findTestObject('Cash Pooling/New Entry/Text Field - Retain Amount 1'), GlobalVariable.Sub1AmountOutFormat222, 
    FailureHandling.CONTINUE_ON_FAILURE)

Random SubAccRand = new Random()

int SubAccount = SubAccRand.nextInt(999)

GlobalVariable.Description222 = ('SubAccDesc Ardo ' + SubAccount)

WebUI.setText(findTestObject('Cash Pooling/New Entry/Text Field - Sub Account Description 1'), GlobalVariable.Description222, 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Cash Pooling/New Entry/Text Field - Main Account Description 1'), GlobalVariable.Description222, 
    FailureHandling.CONTINUE_ON_FAILURE)

if (GlobalVariable.AccountRand222 == 2) {
    WebUI.click(findTestObject('Cash Pooling/New Entry/BTN - Plus'))

    WebUI.waitForElementVisible(findTestObject('Cash Pooling/New Entry/Drop List - Sub Account Row 2'), 0)

    WebUI.click(findTestObject('Cash Pooling/New Entry/Drop List - Sub Account Row 2'))

    WebUI.waitForElementVisible(findTestObject('Cash Pooling/New Entry/Text Field - Set Text'), 0)

    WebUI.setText(findTestObject('Cash Pooling/New Entry/Text Field - Set Text'), GlobalVariable.Sub2Account222)

    WebUI.waitForElementVisible(findTestObject('Cash Pooling/New Entry/Drop List - Highlight or 1'), 0)

    WebUI.click(findTestObject('Cash Pooling/New Entry/Drop List - Highlight or 1'))

    WebUI.delay(1)

    SubAccountRow2 = WebUI.getText(findTestObject('Cash Pooling/New Entry/Drop List - Sub Account Row 2'))

    GlobalVariable.Sub2Account222 = SubAccountRow2

    WebUI.setText(findTestObject('Cash Pooling/New Entry/Text Field - Retain Amount 2'), GlobalVariable.Sub2AmountOutFormat222)

    WebUI.setText(findTestObject('Cash Pooling/New Entry/Text Field - Sub Account Description 2'), GlobalVariable.Description222)

    WebUI.setText(findTestObject('Cash Pooling/New Entry/Text Field - Main Account Description 2'), GlobalVariable.Description222)
}

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

DataTableInfo = WebUI.getText(findTestObject('Cash Pooling/New Entry/Label - Data Table Info'), FailureHandling.CONTINUE_ON_FAILURE)

if (GlobalVariable.AccountRand222 == 1) {
    WebUI.verifyMatch(DataTableInfo, 'Showing 1 to 1 of 1 entries', false)
} else {
    WebUI.verifyMatch(DataTableInfo, 'Showing 1 to 2 of 2 entries', false)
}

GlobalVariable.DataTablesDetails222 = DataTableInfo

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Cash Pooling/New Entry/BTN - Specific Date'))

WebUI.delay(1)

WebUI.waitForElementVisible(findTestObject('Cash Pooling/New Entry/Drop List - Future Date'), 0)

WebUI.click(findTestObject('Cash Pooling/New Entry/Drop List - Future Date'))

WebUI.waitForElementVisible(findTestObject('Cash Pooling/New Entry/Drop List - Future Date - Date'), 0)

WebUI.click(findTestObject('Cash Pooling/New Entry/Drop List - Future Date - Date'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(1)

WebUI.click(findTestObject('Cash Pooling/New Entry/Drop List - Session Time'))

WebUI.waitForElementVisible(findTestObject('Cash Pooling/New Entry/Text Field - Set Text'), 0)

WebUI.setText(findTestObject('Cash Pooling/New Entry/Text Field - Set Text'), '07')

WebUI.waitForElementVisible(findTestObject('Cash Pooling/New Entry/Drop List - Highlight or 1'), 0)

WebUI.click(findTestObject('Cash Pooling/New Entry/Drop List - Highlight or 1'))

WebUI.getText(findTestObject('Cash Pooling/New Entry/Drop List - Session Time'))

InstructionModeActive = WebUI.getText(findTestObject('Cash Pooling/New Entry/Label - Instruction Mode'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.InstructionMode123 = InstructionModeActive

WebUI.click(findTestObject('Cash Pooling/New Entry/Checkbox - Terms and Condition'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Cash Pooling/New Entry/BTN - Confirm'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

