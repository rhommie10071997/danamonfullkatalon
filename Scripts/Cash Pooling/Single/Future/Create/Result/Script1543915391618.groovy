import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(10)

LabelConfirm = WebUI.getText(findTestObject('Cash Pooling/Confirm Screen/Label - Sub Title - Confirm'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(LabelConfirm, 'Result', false, FailureHandling.CONTINUE_ON_FAILURE)

MessageSuccess = WebUI.getText(findTestObject('Cash Pooling/Result Screen/Label - Message Success'))

WebUI.verifyMatch(MessageSuccess, 'This transaction has been successfully released', false)

ReferenceNo = WebUI.getText(findTestObject('Cash Pooling/Result Screen/Label - Reference Number'))

String[] ReferenceNo1 = ReferenceNo.split(' ')

String ReferenceNo2 = ReferenceNo1[2]

GlobalVariable.RefNo = ReferenceNo2

MainAccount = WebUI.getText(findTestObject('Cash Pooling/Confirm Screen/Label - Main Account - Account'), FailureHandling.CONTINUE_ON_FAILURE)

String MainAccount = MainAccount.replace(': ', '')

String MainAccount1 = MainAccount.replace(' (', '(')

WebUI.verifyMatch(MainAccount1, GlobalVariable.MainAccount222, false, FailureHandling.CONTINUE_ON_FAILURE)

SetupSweepPriority = WebUI.getText(findTestObject('Cash Pooling/Confirm Screen/Label - Setup Sweep Priority'), FailureHandling.CONTINUE_ON_FAILURE)

String SetupSweepPriority = SetupSweepPriority.replace(': ', '')

WebUI.verifyMatch(SetupSweepPriority, GlobalVariable.SetupSweepPriority222, false, FailureHandling.CONTINUE_ON_FAILURE)

AutoReverse = WebUI.getText(findTestObject('Cash Pooling/Confirm Screen/Label - Auto Reverse'), FailureHandling.CONTINUE_ON_FAILURE)

String AutoReverse = AutoReverse.replace(': ', '')

WebUI.verifyMatch(AutoReverse, GlobalVariable.RandomAutoReverse222, false, FailureHandling.CONTINUE_ON_FAILURE)

AmountType = WebUI.getText(findTestObject('Cash Pooling/Confirm Screen/Label - Amount Type'), FailureHandling.CONTINUE_ON_FAILURE)

String AmountType = AmountType.replace(': ', '')

WebUI.verifyMatch(AmountType, 'Fixed', false, FailureHandling.CONTINUE_ON_FAILURE)

if (GlobalVariable.AccountRand222 == 1) {
    WebUI.callTestCase(findTestCase('Cash Pooling/Table Loop/Verify Table 1'), [:], FailureHandling.STOP_ON_FAILURE)
} else {
    WebUI.callTestCase(findTestCase('Cash Pooling/Table Loop/Verify Table 2'), [:], FailureHandling.STOP_ON_FAILURE)
}

WebUI.delay(3, FailureHandling.CONTINUE_ON_FAILURE)

DataTableInfo = WebUI.getText(findTestObject('Cash Pooling/New Entry/Label - Data Table Info'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(DataTableInfo, GlobalVariable.DataTablesDetails222, false, FailureHandling.CONTINUE_ON_FAILURE)

CashConcentrationFee = WebUI.getText(findTestObject('Cash Pooling/Confirm Screen/Label - Cash Concentration Fee'))

String CashConcentrationFee = CashConcentrationFee.replace(': ', '')

WebUI.verifyMatch(CashConcentrationFee, GlobalVariable.CashConcentrationFee222, false)

TotalFee = WebUI.getText(findTestObject('Cash Pooling/Confirm Screen/Label - Total Fee'))

String TotalFee = TotalFee.replace(': ', '')

WebUI.verifyMatch(TotalFee, GlobalVariable.TotalFee222, false)

InstructionMode = WebUI.getText(findTestObject('Cash Pooling/Confirm Screen/Label - Instruction Mode'))

String InstructionMode = InstructionMode.replace(': ', '')

WebUI.verifyMatch(InstructionMode, GlobalVariable.InstructionMode123, false, FailureHandling.CONTINUE_ON_FAILURE)

FutureDate = WebUI.getText(findTestObject('Cash Pooling/Confirm Screen/Label - Future Date'))

String FutureDate = FutureDate.replace(': ', '')

WebUI.verifyMatch(FutureDate, GlobalVariable.FutureDate123, false)

atSession = WebUI.getText(findTestObject('Cash Pooling/Confirm Screen/Label - at Session'))

String atSession = atSession.replace(': ', '')

WebUI.verifyMatch(atSession, GlobalVariable.SpecificDateAt123, false)

ExpiredOn = WebUI.getText(findTestObject('Cash Pooling/Confirm Screen/Label - Expired on'))

String ExpiredOn = ExpiredOn.replace(': ', '')

WebUI.verifyMatch(ExpiredOn, GlobalVariable.ExpiredOn123, false)

WebUI.click(findTestObject('Cash Pooling/Result Screen/BTN - Done'))

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Menu/TextField - Search'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Menu/TextField - Search'), ' ', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Menu/TextField - Search'), '', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

