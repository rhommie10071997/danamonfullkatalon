import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

Pendingtask = WebUI.getText(findTestObject('Pending Task/Cash Pooling/Detail Screen/Label - Sub Pending Task'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(Pendingtask, 'Pending Task', false, FailureHandling.CONTINUE_ON_FAILURE)

Menu = WebUI.getText(findTestObject('Pending Task/Cash Pooling/Detail Screen/Label - Menu'))

String Menu = Menu.replace(': ', '')

WebUI.verifyMatch(Menu, 'Cash Pooling', false, FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Map123.put('Menu', Menu)

Product = WebUI.getText(findTestObject('Pending Task/Cash Pooling/Detail Screen/Label - Product'))

String Product = Product.replace(': ', '')

WebUI.verifyMatch(Product, 'Cash Concentration', false, FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Map123.put('Product', Product)

TransactionRefNo = WebUI.getText(findTestObject('Pending Task/Cash Pooling/Detail Screen/Label - Transaction Ref No'))

String TransactionRefNo1 = TransactionRefNo.replace(': ', '')

WebUI.verifyMatch(TransactionRefNo1, GlobalVariable.Map123.get('RefNo'), false)

DocumentCode = WebUI.getText(findTestObject('Pending Task/Cash Pooling/Detail Screen/Label - Document Code'))

String DocumentCode = DocumentCode.replace(': ', '')

WebUI.verifyMatch(DocumentCode, GlobalVariable.Map123.get('RefNo'), false)

MainAccount = WebUI.getText(findTestObject('Cash Pooling/Confirm Screen/Label - Main Account - Account'), FailureHandling.CONTINUE_ON_FAILURE)

String MainAccount = MainAccount.replace(': ', '')

String MainAccount1 = MainAccount.replace(' (', '(')

WebUI.verifyMatch(MainAccount1, GlobalVariable.Map123.get('MainAccount'), false, FailureHandling.CONTINUE_ON_FAILURE)

SetupSweepPriority = WebUI.getText(findTestObject('Cash Pooling/Confirm Screen/Label - Setup Sweep Priority'), FailureHandling.CONTINUE_ON_FAILURE)

String SetupSweepPriority = SetupSweepPriority.replace(': ', '')

WebUI.verifyMatch(SetupSweepPriority, GlobalVariable.Map123.get('SetupSweepPriority'), false, FailureHandling.CONTINUE_ON_FAILURE)

AutoReverse = WebUI.getText(findTestObject('Cash Pooling/Confirm Screen/Label - Auto Reverse'), FailureHandling.CONTINUE_ON_FAILURE)

String AutoReverse = AutoReverse.replace(': ', '')

WebUI.verifyMatch(AutoReverse, GlobalVariable.Map123.get('RandomAutoReverse'), false, FailureHandling.CONTINUE_ON_FAILURE)

AmountType = WebUI.getText(findTestObject('Cash Pooling/Confirm Screen/Label - Amount Type'), FailureHandling.CONTINUE_ON_FAILURE)

String AmountType = AmountType.replace(': ', '')

WebUI.verifyMatch(AmountType, 'Fixed', false, FailureHandling.CONTINUE_ON_FAILURE)

if (GlobalVariable.Map123.get('AccountRand') == 1) {
    WebUI.callTestCase(findTestCase('Cash Pooling/Table Loop/Verify Table 1'), [:], FailureHandling.STOP_ON_FAILURE)
} else {
    WebUI.callTestCase(findTestCase('Cash Pooling/Table Loop/Verify Table 2'), [:], FailureHandling.STOP_ON_FAILURE)
}

WebUI.delay(3, FailureHandling.CONTINUE_ON_FAILURE)

DataTableInfo = WebUI.getText(findTestObject('Cash Pooling/New Entry/Label - Data Table Info'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(DataTableInfo, GlobalVariable.Map123.get('DataTablesDetails'), false, FailureHandling.CONTINUE_ON_FAILURE)

CashConcentrationFee = WebUI.getText(findTestObject('Cash Pooling/Confirm Screen/Label - Cash Concentration Fee'))

String CashConcentrationFee = CashConcentrationFee.replace(': ', '')

WebUI.verifyMatch(CashConcentrationFee, GlobalVariable.Map123.get('CashConcentrationFee'), false)

TotalFee = WebUI.getText(findTestObject('Cash Pooling/Confirm Screen/Label - Total Fee'))

String TotalFee = TotalFee.replace(': ', '')

WebUI.verifyMatch(TotalFee, GlobalVariable.Map123.get('TotalFee'), false)

InstructionMode = WebUI.getText(findTestObject('Cash Pooling/Confirm Screen/Label - Instruction Mode'))

String InstructionMode = InstructionMode.replace(': ', '')

WebUI.verifyMatch(InstructionMode, GlobalVariable.Map123.get('InstructionMode'), false, FailureHandling.CONTINUE_ON_FAILURE)

FutureDate = WebUI.getText(findTestObject('Cash Pooling/Confirm Screen/Label - Future Date'))

String FutureDate = FutureDate.replace(': ', '')

WebUI.verifyMatch(FutureDate, GlobalVariable.Map123.get('FutureDate'), false)

atSession = WebUI.getText(findTestObject('Cash Pooling/Confirm Screen/Label - at Session'))

String atSession = atSession.replace(': ', '')

WebUI.verifyMatch(atSession, GlobalVariable.Map123.get('atSession'), false)

WebUI.click(findTestObject('Pending Task/Cash Pooling/Detail Screen/BTN - Approve'))

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.focus(findTestObject('Pending Task/Balance Transfer/Inhouse/Detail/TextField - Response Code'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Pending Task/Balance Transfer/Inhouse/Detail/TextField - Response Code'), 0, 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Pending Task/Balance Transfer/Inhouse/Detail/TextField - Response Code'), 0, 
    FailureHandling.CONTINUE_ON_FAILURE)

String abc = '123456'

WebUI.setText(findTestObject('Pending Task/Balance Transfer/Inhouse/Detail/TextField - Response Code'), abc, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Pending Task/Balance Transfer/Inhouse/Detail/BTN - Approve'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(4, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Pending Task/Balance Transfer/Inhouse/Detail/BTN - Ok (Pop Up)'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Pending Task/Balance Transfer/Inhouse/Detail/BTN - Ok (Pop Up)'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

