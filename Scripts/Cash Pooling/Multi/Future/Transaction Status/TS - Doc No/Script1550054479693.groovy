import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(10, FailureHandling.CONTINUE_ON_FAILURE)

LabelConfirm = WebUI.getText(findTestObject('Cash Pooling/Confirm Screen/Label - Sub Title - Confirm'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(LabelConfirm, 'Document Number Detail', false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Transaction Status/Cash Pooling/Doc No/Label - Transaction Status'), 0, FailureHandling.CONTINUE_ON_FAILURE)

TransactionStatus = WebUI.getText(findTestObject('Transaction Status/Cash Pooling/Doc No/Label - Transaction Status'), FailureHandling.CONTINUE_ON_FAILURE)

String TransactionStatus = TransactionStatus.replace(': ', '')

WebUI.verifyMatch(TransactionStatus, 'Pending Execute', false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(10, FailureHandling.CONTINUE_ON_FAILURE)

Product = WebUI.getText(findTestObject('Transaction Status/Cash Pooling/Ref No/Label - Product'), FailureHandling.CONTINUE_ON_FAILURE)

String Product = Product.replace(': ', '')

WebUI.verifyMatch(Product, GlobalVariable.Map123.get('Product'), false, FailureHandling.CONTINUE_ON_FAILURE)

TransactionRefNo = WebUI.getText(findTestObject('Transaction Status/Cash Pooling/Ref No/Label - Transaction Ref No'), FailureHandling.CONTINUE_ON_FAILURE)

String TransactionRefNo1 = TransactionRefNo.replace(': ', '')

WebUI.verifyMatch(TransactionRefNo1, GlobalVariable.Map123.get('RefNo'), false, FailureHandling.CONTINUE_ON_FAILURE)

DocumentCode = WebUI.getText(findTestObject('Transaction Status/Cash Pooling/Ref No/Label - Document Code'), FailureHandling.CONTINUE_ON_FAILURE)

String DocumentCode = DocumentCode.replace(': ', '')

WebUI.verifyMatch(DocumentCode, GlobalVariable.Map123.get('RefNo'), false, FailureHandling.CONTINUE_ON_FAILURE)

MainAccount = WebUI.getText(findTestObject('Transaction Status/Cash Pooling/Ref No/Label - Main Account - Account'), FailureHandling.CONTINUE_ON_FAILURE)

String MainAccount = MainAccount.replace(': ', '')

String MainAccount1 = MainAccount.replace(' (', '(')

WebUI.verifyMatch(MainAccount1, GlobalVariable.Map123.get('MainAccount'), false, FailureHandling.CONTINUE_ON_FAILURE)

SetupSweepPriority = WebUI.getText(findTestObject('Transaction Status/Cash Pooling/Ref No/Label - Setup Sweep Priority'), 
    FailureHandling.CONTINUE_ON_FAILURE)

String SetupSweepPriority = SetupSweepPriority.replace(': ', '')

WebUI.verifyMatch(SetupSweepPriority, GlobalVariable.Map123.get('SetupSweepPriority'), false, FailureHandling.CONTINUE_ON_FAILURE)

AutoReverse = WebUI.getText(findTestObject('Transaction Status/Cash Pooling/Ref No/Label - Auto Reverse'), FailureHandling.CONTINUE_ON_FAILURE)

String AutoReverse = AutoReverse.replace(': ', '')

WebUI.verifyMatch(AutoReverse, GlobalVariable.Map123.get('RandomAutoReverse'), false, FailureHandling.CONTINUE_ON_FAILURE)

AmountType = WebUI.getText(findTestObject('Transaction Status/Cash Pooling/Ref No/Label - Amount Type'), FailureHandling.CONTINUE_ON_FAILURE)

String AmountType = AmountType.replace(': ', '')

WebUI.verifyMatch(AmountType, 'Fixed', false, FailureHandling.CONTINUE_ON_FAILURE)

if (GlobalVariable.Map123.get('AccountRand') == 1) {
    WebUI.callTestCase(findTestCase('Cash Pooling/Table Loop/Verify Table 1 - Transaction Status'), [:], FailureHandling.STOP_ON_FAILURE)
} else {
    WebUI.callTestCase(findTestCase('Cash Pooling/Table Loop/Verify Table 2 - Transaction Status'), [:], FailureHandling.STOP_ON_FAILURE)
}

WebUI.delay(3, FailureHandling.CONTINUE_ON_FAILURE)

DataTableInfo = WebUI.getText(findTestObject('Transaction Status/Cash Pooling/Ref No/Label - Data Table Info'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(DataTableInfo, GlobalVariable.Map123.get('DataTablesDetails'), false, FailureHandling.CONTINUE_ON_FAILURE)

CashConcentrationFee = WebUI.getText(findTestObject('Transaction Status/Cash Pooling/Ref No/Label - Cash Concentration Fee'), 
    FailureHandling.CONTINUE_ON_FAILURE)

String CashConcentrationFee = CashConcentrationFee.replace(': ', '')

WebUI.verifyMatch(CashConcentrationFee, GlobalVariable.Map123.get('CashConcentrationFee'), false, FailureHandling.CONTINUE_ON_FAILURE)

TotalFee = WebUI.getText(findTestObject('Transaction Status/Cash Pooling/Ref No/Label - Total Fee'), FailureHandling.CONTINUE_ON_FAILURE)

String TotalFee = TotalFee.replace(': ', '')

WebUI.verifyMatch(TotalFee, GlobalVariable.Map123.get('TotalFee'), false, FailureHandling.CONTINUE_ON_FAILURE)

InstructionMode = WebUI.getText(findTestObject('Transaction Status/Cash Pooling/Ref No/Label - Instruction Mode'), FailureHandling.CONTINUE_ON_FAILURE)

String InstructionMode = InstructionMode.replace(': ', '')

WebUI.verifyMatch(InstructionMode, GlobalVariable.Map123.get('InstructionMode'), false, FailureHandling.CONTINUE_ON_FAILURE)

FutureDate = WebUI.getText(findTestObject('Transaction Status/Cash Pooling/Ref No/Label - Future Date'))

String FutureDate = FutureDate.replace(': ', '')

WebUI.verifyMatch(FutureDate, GlobalVariable.Map123.get('FutureDate'), false)

atSession = WebUI.getText(findTestObject('Transaction Status/Cash Pooling/Ref No/Label - at Session'))

String atSession = atSession.replace(': ', '')

WebUI.verifyMatch(atSession, GlobalVariable.Map123.get('atSession'), false)

ExpiredOn = WebUI.getText(findTestObject('Transaction Status/Cash Pooling/Ref No/Label - Expired on'), FailureHandling.CONTINUE_ON_FAILURE)

String ExpiredOn = ExpiredOn.replace(': ', '')

WebUI.verifyMatch(ExpiredOn, GlobalVariable.Map123.get('ExpiredOn'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(10, FailureHandling.CONTINUE_ON_FAILURE)

