import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.waitForElementVisible(findTestObject('Cash Pooling/Confirm Screen/Label - Main Account - Account'), 0, FailureHandling.CONTINUE_ON_FAILURE)

LabelConfirm = WebUI.getText(findTestObject('Cash Pooling/Confirm Screen/Label - Sub Title - Confirm'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(LabelConfirm, 'Confirm', false, FailureHandling.CONTINUE_ON_FAILURE)

MainAccount = WebUI.getText(findTestObject('Cash Pooling/Confirm Screen/Label - Main Account - Account'), FailureHandling.CONTINUE_ON_FAILURE)

String MainAccount = MainAccount.replace(': ', '')

WebUI.verifyMatch(MainAccount, GlobalVariable.Map123.get('MainAccount'), false, FailureHandling.CONTINUE_ON_FAILURE)

SetupSweepPriority = WebUI.getText(findTestObject('Cash Pooling/Confirm Screen/Label - Setup Sweep Priority'), FailureHandling.CONTINUE_ON_FAILURE)

String SetupSweepPriority = SetupSweepPriority.replace(': ', '')

WebUI.verifyMatch(SetupSweepPriority, GlobalVariable.Map123.get('SetupSweepPriority'), false, FailureHandling.CONTINUE_ON_FAILURE)

//GlobalVariable.Map123.put("SetupSweepPriority", SetupSweepPriority)
AutoReverse = WebUI.getText(findTestObject('Cash Pooling/Confirm Screen/Label - Auto Reverse'), FailureHandling.CONTINUE_ON_FAILURE)

String AutoReverse = AutoReverse.replace(': ', '')

if (GlobalVariable.Map123.get('RandomAutoReverse') == '1') {
    WebUI.verifyMatch(AutoReverse, 'Yes', false)
} else {
    WebUI.verifyMatch(AutoReverse, 'No', false)
}

GlobalVariable.Map123.put('RandomAutoReverse', AutoReverse)

AmountType = WebUI.getText(findTestObject('Cash Pooling/Confirm Screen/Label - Amount Type'), FailureHandling.CONTINUE_ON_FAILURE)

String AmountType = AmountType.replace(': ', '')

WebUI.verifyMatch(AmountType, 'Fixed', false, FailureHandling.CONTINUE_ON_FAILURE)

DiagramMainAccount = WebUI.getText(findTestObject('Cash Pooling/Confirm Screen/Label - Diagram - Main Account'), FailureHandling.CONTINUE_ON_FAILURE)

String DiagramMainAcc = DiagramMainAccount.substring(13)

WebUI.verifyMatch(DiagramMainAcc, GlobalVariable.Map123.get('MainAccount'), false, FailureHandling.CONTINUE_ON_FAILURE)

DiagramSubAccount1 = WebUI.getText(findTestObject('Cash Pooling/Confirm Screen/Label - Diagram - Sub Account 1'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(DiagramSubAccount1, GlobalVariable.Map123.get('Sub1Account'), false, FailureHandling.CONTINUE_ON_FAILURE)

if (GlobalVariable.Map123.get('AccountRand') == 2) {
    DiagramSubAccount2 = WebUI.getText(findTestObject('Cash Pooling/Confirm Screen/Label - Diagram - Sub Account 2'))

    WebUI.verifyMatch(DiagramSubAccount2, GlobalVariable.Map123.get('Sub2Account'), false)
}

if (GlobalVariable.Map123.get('AccountRand') == 1) {
    WebUI.callTestCase(findTestCase('Cash Pooling/Table Loop/Get Text 1'), [('SubAccount') : 0, ('RetainAmount') : 1, ('SubAccountDescription') : 2
            , ('MainAccountDescription') : 3], FailureHandling.STOP_ON_FAILURE)
} else {
    WebUI.callTestCase(findTestCase('Cash Pooling/Table Loop/Get Text 2'), [('SubAccount') : 0, ('RetainAmount') : 1, ('SubAccountDescription') : 2
            , ('MainAccountDescription') : 3], FailureHandling.STOP_ON_FAILURE)
}

WebUI.delay(3, FailureHandling.CONTINUE_ON_FAILURE)

DataTableInfo = WebUI.getText(findTestObject('Cash Pooling/New Entry/Label - Data Table Info'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(DataTableInfo, GlobalVariable.Map123.get('DataTablesDetails'), false, FailureHandling.CONTINUE_ON_FAILURE)

CashConcentrationFee = WebUI.getText(findTestObject('Cash Pooling/Confirm Screen/Label - Cash Concentration Fee'))

String CashConcentrationFee = CashConcentrationFee.replace(': ', '')

GlobalVariable.Map123.put('CashConcentrationFee', CashConcentrationFee)

TotalFee = WebUI.getText(findTestObject('Cash Pooling/Confirm Screen/Label - Total Fee'))

String TotalFee = TotalFee.replace(': ', '')

GlobalVariable.Map123.put('TotalFee', TotalFee)

InstructionMode = WebUI.getText(findTestObject('Cash Pooling/Confirm Screen/Label - Instruction Mode'))

String InstructionMode = InstructionMode.replace(': ', '')

WebUI.verifyMatch(InstructionMode, GlobalVariable.Map123.get('InstructionMode'), false, FailureHandling.CONTINUE_ON_FAILURE)

FutureDate = WebUI.getText(findTestObject('Cash Pooling/Confirm Screen/Label - Future Date'))

String FutureDate = FutureDate.replace(': ', '')

GlobalVariable.Map123.put('FutureDate', FutureDate)

atSession = WebUI.getText(findTestObject('Cash Pooling/Confirm Screen/Label - at Session'))

String atSession = atSession.replace(': ', '')

GlobalVariable.Map123.put('atSession', atSession)

ExpiredOn = WebUI.getText(findTestObject('Cash Pooling/Confirm Screen/Label - Expired on'))

String ExpiredOn = ExpiredOn.replace(': ', '')

GlobalVariable.Map123.put('ExpiredOn', ExpiredOn)

WebUI.click(findTestObject('Cash Pooling/Confirm Screen/BTN - Submit'))

WebUI.delay(2)

WebUI.waitForElementVisible(findTestObject('Cash Pooling/Confirm Screen/BTN - Pop Up Submit'), 0)

WebUI.click(findTestObject('Cash Pooling/Confirm Screen/BTN - Pop Up Submit'))

