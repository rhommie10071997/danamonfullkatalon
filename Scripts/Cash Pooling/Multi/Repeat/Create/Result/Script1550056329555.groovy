import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(5)

LabelConfirm = WebUI.getText(findTestObject('Cash Pooling/Confirm Screen/Label - Sub Title - Confirm'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(LabelConfirm, 'Result', false, FailureHandling.CONTINUE_ON_FAILURE)

MessageSuccess = WebUI.getText(findTestObject('Cash Pooling/Result Screen/Label - Message Success'))

WebUI.verifyMatch(MessageSuccess, 'Your transaction is waiting for approval', false)

ReferenceNo = WebUI.getText(findTestObject('Cash Pooling/Result Screen/Label - Reference Number'))

String[] ReferenceNo1 = ReferenceNo.split(' ')

String ReferenceNo2 = ReferenceNo1[2]

GlobalVariable.Map123.put('RefNo', ReferenceNo2)

MainAccount = WebUI.getText(findTestObject('Cash Pooling/Confirm Screen/Label - Main Account - Account'), FailureHandling.CONTINUE_ON_FAILURE)

String MainAccount = MainAccount.replace(': ', '')

String MainAccount1 = MainAccount.replace(' (', '(')

WebUI.verifyMatch(MainAccount1, GlobalVariable.Map123.get('MainAccount'), false, FailureHandling.CONTINUE_ON_FAILURE)

SetupSweepPriority = WebUI.getText(findTestObject('Cash Pooling/Confirm Screen/Label - Setup Sweep Priority'), FailureHandling.CONTINUE_ON_FAILURE)

String SetupSweepPriority = SetupSweepPriority.replace(': ', '')

WebUI.verifyMatch(SetupSweepPriority, GlobalVariable.Map123.get('SetupSweepPriority'), false, FailureHandling.CONTINUE_ON_FAILURE)

AutoReverse = WebUI.getText(findTestObject('Cash Pooling/Confirm Screen/Label - Auto Reverse'), FailureHandling.CONTINUE_ON_FAILURE)

String AutoReverse = AutoReverse.replace(': ', '')

WebUI.verifyMatch(AutoReverse, GlobalVariable.Map123.get('RandomAutoReverse'), false, FailureHandling.CONTINUE_ON_FAILURE)

AmountType = WebUI.getText(findTestObject('Cash Pooling/Confirm Screen/Label - Amount Type'), FailureHandling.CONTINUE_ON_FAILURE)

String AmountType = AmountType.replace(': ', '')

WebUI.verifyMatch(AmountType, 'Fixed', false, FailureHandling.CONTINUE_ON_FAILURE)

if (GlobalVariable.Map123.get('AccountRand') == 1) {
    WebUI.callTestCase(findTestCase('Cash Pooling/Table Loop/Verify Table 1'), [:], FailureHandling.STOP_ON_FAILURE)
} else {
    WebUI.callTestCase(findTestCase('Cash Pooling/Table Loop/Verify Table 2'), [:], FailureHandling.STOP_ON_FAILURE)
}

WebUI.delay(3, FailureHandling.CONTINUE_ON_FAILURE)

DataTableInfo = WebUI.getText(findTestObject('Cash Pooling/New Entry/Label - Data Table Info'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(DataTableInfo, GlobalVariable.Map123.get('DataTablesDetails'), false, FailureHandling.CONTINUE_ON_FAILURE)

CashConcentrationFee = WebUI.getText(findTestObject('Cash Pooling/Confirm Screen/Label - Cash Concentration Fee'))

String CashConcentrationFee = CashConcentrationFee.replace(': ', '')

WebUI.verifyMatch(CashConcentrationFee, GlobalVariable.Map123.get('CashConcentrationFee'), false)

TotalFee = WebUI.getText(findTestObject('Cash Pooling/Confirm Screen/Label - Total Fee'))

String TotalFee = TotalFee.replace(': ', '')

WebUI.verifyMatch(TotalFee, GlobalVariable.Map123.get('TotalFee'), false)

InstructionMode = WebUI.getText(findTestObject('Cash Pooling/Confirm Screen/Label - Instruction Mode'))

String InstructionMode = InstructionMode.replace(': ', '')

WebUI.verifyMatch(InstructionMode, GlobalVariable.Map123.get('InstructionMode'), false, FailureHandling.CONTINUE_ON_FAILURE)

RepeatOn = WebUI.getText(findTestObject('Cash Pooling/Confirm Screen/Label - Repeat - On'))

String RepeatOn = RepeatOn.replace(': ', '')

WebUI.verifyMatch(RepeatOn, GlobalVariable.Map123.get('RepeatOn'), false)

RepeatEvery = WebUI.getText(findTestObject('Cash Pooling/Confirm Screen/Label - Repeat - Every'))

String RepeatEvery = RepeatEvery.replace(': ', '')

WebUI.verifyMatch(RepeatEvery, GlobalVariable.Map123.get('RepeatEvery'), false)

RepeatAt = WebUI.getText(findTestObject('Cash Pooling/Confirm Screen/Label - Repeat - At'))

String RepeatAt = RepeatAt.replace(': ', '')

WebUI.verifyMatch(RepeatAt, GlobalVariable.Map123.get('RepeatAt'), false)

RepeatStart = WebUI.getText(findTestObject('Cash Pooling/Confirm Screen/Label - Repeat - Start'))

String RepeatStart = RepeatStart.replace(': ', '')

WebUI.verifyMatch(RepeatStart, GlobalVariable.Map123.get('RepeatStart'), false)

RepeatEnd = WebUI.getText(findTestObject('Cash Pooling/Confirm Screen/Label - Repeat - End'))

String RepeatEnd = RepeatEnd.replace(': ', '')

WebUI.verifyMatch(RepeatEnd, GlobalVariable.Map123.get('RepeatEnd'), false)

RepeatNonWorkingDayInstruction = WebUI.getText(findTestObject('Cash Pooling/Confirm Screen/Label - Repeat - Non-Working Day Instruction'))

String RepeatNonWorkingDayInstruction = RepeatNonWorkingDayInstruction.replace(': ', '')

WebUI.verifyMatch(RepeatNonWorkingDayInstruction, GlobalVariable.Map123.get('NonWorkingDay'), false)

WebUI.click(findTestObject('Cash Pooling/Result Screen/BTN - Done'))

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('5 - Log Out btn'), FailureHandling.CONTINUE_ON_FAILURE)

