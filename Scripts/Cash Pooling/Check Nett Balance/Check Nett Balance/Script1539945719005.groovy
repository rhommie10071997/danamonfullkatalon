import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.google.api.client.util.ExponentialBackOff as ExponentialBackOff
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.sun.org.apache.bcel.internal.generic.TABLESWITCH as TABLESWITCH
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import java.math.BigDecimal as BigDecimal
import java.math.BigDecimal.LongOverflow as LongOverflow
import java.text.DecimalFormat as DecimalFormat
import java.text.NumberFormat as NumberFormat
import java.text.SimpleDateFormat as SimpleDateFormat
import org.junit.After as After
import java.util.Calendar as Calendar
import java.util.Scanner as Scanner
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW

WebUI.waitForElementVisible(findTestObject('Menu/BTN - Menu Object'), 0)

WebUI.delay(2)

WebUI.setText(findTestObject('Menu/TextField - Search'), 'Single Transfer')

WebUI.waitForElementVisible(findTestObject('Menu/Label - Single Transfer (By Search)'), 0)

WebUI.click(findTestObject('Menu/Label - Single Transfer (By Search)'))

WebUI.delay(5)

WebUI.waitForElementVisible(findTestObject('Cash Pooling/Check Nett Balance/Label - Title - Create Single Transfer'), 0)

TitleCreateSingleTransfer = WebUI.getText(findTestObject('Cash Pooling/Check Nett Balance/Label - Title - Create Single Transfer'))

WebUI.verifyMatch(TitleCreateSingleTransfer, 'Create Single Transfer', false)

WebUI.click(findTestObject('Cash Pooling/Check Nett Balance/Drop List - Transfer Form - Select Account'))

WebUI.waitForElementVisible(findTestObject('Cash Pooling/New Entry/Text Field - Set Text'), 0)

WebUI.setText(findTestObject('Cash Pooling/New Entry/Text Field - Set Text'), GlobalVariable.Map123.get("Sub1Account"))

WebUI.waitForElementVisible(findTestObject('Cash Pooling/New Entry/Drop List - Highlight or 1'), 0)

WebUI.click(findTestObject('Cash Pooling/New Entry/Drop List - Highlight or 1'))

WebUI.waitForElementVisible(findTestObject('Cash Pooling/Check Nett Balance/Label - Nett Balance'), 0)

WebUI.delay(15)

NettBalance = WebUI.getText(findTestObject('Cash Pooling/Check Nett Balance/Label - Nett Balance'))

String NettBalance1 = NettBalance

String[] nett1 = NettBalance1.split('\\.')

String nett2 = nett1[0]

String nett3 = nett2.replaceAll('\\,', '')

String[] nett4 = nett3.split(' ')

String Currency1 = nett4[0]

String nett11 = nett4[1]

Random rand = new Random()

int angka

angka = rand.nextInt(999)

String angka1 = GlobalVariable.Map123.get('SubAccount1Amount') + angka

BigInteger Amm1 = new BigInteger(nett11)

BigInteger Amm2 = new BigInteger(angka1)

BigInteger Amm3 = Amm1 - Amm2

String RetainAmount = '' + Amm3

GlobalVariable.Map123.put("Sub1AmountOutFormat", RetainAmount)

Locale local1 = new Locale('en', 'UK')

DecimalFormat retainamountformat = ((NumberFormat.getNumberInstance(local1)) as DecimalFormat)

retainamountformat.applyPattern('###,###,##0.00')

String retainamountformat1 = retainamountformat.format(new BigDecimal(RetainAmount))

GlobalVariable.Map123.put("AmountInquirySub1",retainamountformat1)

GlobalVariable.Map123.put("Sub1Amount",(Currency1 + retainamountformat1))

if (GlobalVariable.Map123.get("AccountRand") == 2) {
    WebUI.callTestCase(findTestCase('Cash Pooling/Check Nett Balance/Check Nett Balance2'), [:], FailureHandling.STOP_ON_FAILURE)
}

