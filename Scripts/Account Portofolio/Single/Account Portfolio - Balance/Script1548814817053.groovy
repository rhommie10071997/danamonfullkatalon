import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.waitForElementVisible(findTestObject('Account Portfolio/Balance/Drop List - Account Cluster'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Account Portfolio/Balance/Drop List - Account Cluster'))

WebUI.waitForElementVisible(findTestObject('Cash Pooling/New Entry/Text Field - Set Text'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Cash Pooling/New Entry/Text Field - Set Text'), GlobalVariable.Map123.get("Group"), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Cash Pooling/New Entry/Drop List - Highlight or 1'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Cash Pooling/New Entry/Drop List - Highlight or 1'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Account Portfolio/Balance/BTN - Search - Account'))

WebUI.waitForElementVisible(findTestObject('Account Statement/TF - Account Number'), 0)

WebUI.setText(findTestObject('Account Statement/TF - Account Number'), GlobalVariable.Map123.get("Account"))

WebUI.delay(2)

WebUI.waitForElementVisible(findTestObject('Account Statement/BTN - Search (1)'), 0)

WebUI.click(findTestObject('Account Statement/BTN - Search (1)'))

WebUI.waitForElementVisible(findTestObject('Account Statement/Label - Table Info Showing'), 300)

WebUI.click(findTestObject('Account Statement/Checkbox - Checkbox'))

WebUI.delay(1)

WebUI.waitForElementVisible(findTestObject('Account Statement/BTN - Show'), 0)

WebUI.click(findTestObject('Account Statement/BTN - Show'))

WebUI.delay(3)

WebUI.click(findTestObject('Account Portfolio/Balance/DropList - Format File'))


if (GlobalVariable.Map123.get("FlagFormatFile") == 0) {
    WebUI.click(findTestObject('Account Portfolio/Balance/DropList - Format File - Excel'))
} else {
    WebUI.click(findTestObject('Account Portfolio/Balance/DropList- Format File - PDF'))
}

WebUI.setText(findTestObject('Account Portfolio/Balance/TextField - Report Name'), GlobalVariable.Map123.get("FileDescription"))

WebUI.click(findTestObject('Account Portfolio/Balance/BTN - Download'))

WebUI.delay(3)

TextSuccess = WebUI.getText(findTestObject('Account Portfolio/Balance/Label - Text Success'))

WebUI.verifyMatch(TextSuccess, 'Please check on Download Report menu.', false)

WebUI.click(findTestObject('Menu/TextField - Search'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Menu/TextField - Search'), ' ', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Menu/TextField - Search'), '', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Menu/TextField - Search'), 'Download Report', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2)

WebUI.click(findTestObject('Menu/Label - Download Report (By Search)'))

