import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint

import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable


Random rand = new Random()



"-----------------------------------------------------------------------------------------------------------------"

"Group yang mau di check"
Group = 'UNGROUPED'

"Account yang mau di check"
Account = "000001115344"

"Format File nya mau apa PDF, Excel. Flag 0 =  Excel dan Flag 1 = PDF"
FlagFormatFile = 0
//int randFormatFile = rand.nextInt(2) + 1


"Nama File Descriptionnya"
NamaFile = "AccountPortArdo"

int randFileName = rand.nextInt(99999) + 1



"-----------------------------------------------------------------------------------------------------------------"

GlobalVariable.Map123.put("Account",Account)

GlobalVariable.Map123.put("Group",Group)

GlobalVariable.Map123.put("FlagFormatFile",FlagFormatFile)

GlobalVariable.Map123.put("FileDescription",NamaFile +" "+randFileName )



"-----------------------------------------------------------------------------------------------------------------"


WebUI.openBrowser(GlobalVariable.url, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('1 - Frame/1 - iframe login'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Log in/corpid'), 'pcm000100', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Log in/userid'), 'hermione', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Log in/password'), 'Password123', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Log in/login'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(3, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Menu/BTN - Menu Object'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Menu/TextField - Search'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Menu/TextField - Search'), ' ', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Menu/TextField - Search'), 'Account Portfolio', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Menu/Label - Account Portfolio (By Search)'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Menu/Label - Account Portfolio (By Search)'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(10, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Account Portfolio/Summary/Label - Sub Title'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Title = WebUI.getText(findTestObject('Account Portfolio/Summary/Label - Title'))

WebUI.verifyMatch(Title, 'Account Portfolio', false)

SubTitle = WebUI.getText(findTestObject('Account Portfolio/Summary/Label - Sub Title'))

WebUI.verifyMatch(SubTitle, 'Summary', false)

DetailsTable = WebUI.getText(findTestObject('Account Portfolio/Summary/Label - Details Table'))

WebUI.verifyMatch(DetailsTable, 'Showing 1 to 3 of 3 entries', false)

WebUI.verifyElementPresent(findTestObject('Account Portfolio/Summary/Label - Table'), 0)

WebUI.verifyElementVisible(findTestObject('Account Portfolio/Summary/Label - Table'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Account Portfolio/Summary/BTN - Balance'))

