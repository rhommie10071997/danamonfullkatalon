import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(30)

WebUI.waitForElementVisible(findTestObject('Download Report/Label - Title'), 0)

Title = WebUI.getText(findTestObject('Download Report/Label - Title'), FailureHandling.STOP_ON_FAILURE)

WebUI.verifyMatch(Title, 'Download Report', false)

WebUI.setText(findTestObject('Download Report/TextField - File Name'), GlobalVariable.Map123.get('FileDescription'))

WebUI.click(findTestObject('Download Report/BTN - Search'))

WebUI.delay(2)

WebUI.waitForElementVisible(findTestObject('Download Report/Label - File Name TD2'), 0)

String FileNameTd2 = WebUI.getText(findTestObject('Download Report/Label - File Name TD2'))

if (FileNameTd2.contains(GlobalVariable.Map123.get('FileDescription'))) {
    WebUI.click(findTestObject('Download Report/BTN - Download'))
} else {
    println('File Not Found')

    println('File Not Found')

    println('File Not Found')

    println('File Not Found')

    WebUI.delay(15)
}

WebUI.delay(5)

