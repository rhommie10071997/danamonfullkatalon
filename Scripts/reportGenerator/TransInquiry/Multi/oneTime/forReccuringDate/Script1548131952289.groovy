import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

//Instruction on list = Daily = 1 , Weekly = 2, Monthly = 3
int Instruction = 1

GlobalVariable.confirmScreens.put('Instruction', Instruction)

WebUI.click(findTestObject('menu/download/reportGenerator/scheduling/Date/DL - onClick(reccuring)'))

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

if (Instruction == 1) {
    WebUI.click(findTestObject('menu/download/reportGenerator/Value - accChoice(1)'))

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.click(findTestObject('menu/download/reportGenerator/DL - everyClick'))

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.click(findTestObject('menu/download/reportGenerator/Value - accChoice(1)'))
} else if (Instruction == 2) {
    WebUI.click(findTestObject('menu/download/reportGenerator/Value - menuChoice(2)'))

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.click(findTestObject('menu/download/reportGenerator/DL - dateInterval'))

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.click(findTestObject('menu/download/reportGenerator/Value - accChoice(1)'))
} else {
    WebUI.click(findTestObject('menu/download/reportGenerator/Value - menuChoice(3)'))

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.click(findTestObject('menu/download/reportGenerator/DL - dailyInterval'))

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.click(findTestObject('menu/download/reportGenerator/Value - accChoice(1)'))
}

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/download/reportGenerator/DL - atClick'))

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/download/reportGenerator/Value - menuChoice(4)'))

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/download/reportGenerator/scheduling/Date/BTN - endClick(reccuring)'))

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/download/reportGenerator/scheduling/Date/DL - TanggalClick'))

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

