import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

status = WebUI.getText(findTestObject('menu/download/reportGenerator/checker/Label - ConfirmScreenStatus'))

WebUI.verifyMatch(status, status, false)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

menuChecker = WebUI.getText(findTestObject('menu/download/reportGenerator/checker/Label - MenuChecker'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('menuChecker', menuChecker)

WebUI.verifyMatch(menuChecker, GlobalVariable.confirmScreens.get('menuChecker'), false)

accClusterChecker = WebUI.getText(findTestObject('menu/download/reportGenerator/checker/Label - AccountClusterChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('accClusterChecker', accClusterChecker)

WebUI.verifyMatch(accClusterChecker, GlobalVariable.confirmScreens.get('accClusterChecker'), false)

AccountChecker = WebUI.getText(findTestObject('menu/download/reportGenerator/checker/Label - AccountChecker'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('AccountChecker', AccountChecker)

WebUI.verifyMatch(AccountChecker, GlobalVariable.confirmScreens.get('AccountChecker'), false)

dateRangeChecker = WebUI.getText(findTestObject('menu/download/reportGenerator/checker/Label - DateRangeChecker'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('dateRangeChecker', dateRangeChecker)

WebUI.verifyMatch(dateRangeChecker, GlobalVariable.confirmScreens.get('dateRangeChecker'), false)

FileFormatChecker = WebUI.getText(findTestObject('menu/download/reportGenerator/checker/Label - FileFormatChecker'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('FileFormatChecker', FileFormatChecker)

WebUI.verifyMatch(FileFormatChecker, GlobalVariable.confirmScreens.get('FileFormatChecker'), false)

encryptionTypeChecker = WebUI.getText(findTestObject('menu/download/reportGenerator/checker/Label - EncryptionTypeChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('encryptionTypeChecker', encryptionTypeChecker)

WebUI.verifyMatch(encryptionTypeChecker, GlobalVariable.confirmScreens.get('encryptionTypeChecker'), false)

consilatedFormatChecker = WebUI.getText(findTestObject('menu/download/reportGenerator/checker/Label - ConsilatedFormatChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('consilatedFormatChecker', consilatedFormatChecker)

WebUI.verifyMatch(consilatedFormatChecker, GlobalVariable.confirmScreens.get('consilatedFormatChecker'), false)

archivedFlagChecker = WebUI.getText(findTestObject('menu/download/reportGenerator/checker/Label - ArchivedFlagChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('archivedFlagChecker', archivedFlagChecker)

WebUI.verifyMatch(archivedFlagChecker, GlobalVariable.confirmScreens.get('archivedFlagChecker'), false)

reportIdChecker = WebUI.getText(findTestObject('menu/download/reportGenerator/checker/Label - ReportIDChecker'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('reportIdChecker', reportIdChecker)

WebUI.verifyMatch(reportIdChecker, GlobalVariable.confirmScreens.get('reportIdChecker'), false)

scheduleChecker = WebUI.getText(findTestObject('menu/download/reportGenerator/checker/Label - ScheduleChecker'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('scheduleChecker', scheduleChecker)

WebUI.verifyMatch(scheduleChecker, GlobalVariable.confirmScreens.get('scheduleChecker'), false)

if (GlobalVariable.confirmScreens.get('Flag') == 1) {
    DateChecker = WebUI.getText(findTestObject('menu/download/reportGenerator/checker/Label - DateChecker - Portofolio'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    GlobalVariable.confirmScreens.put('DateChecker', DateChecker)

    WebUI.verifyMatch(DateChecker, GlobalVariable.confirmScreens.get('DateChecker'), false)

    atChecker = WebUI.getText(findTestObject('menu/download/reportGenerator/checker/Label - AtChecker'), FailureHandling.CONTINUE_ON_FAILURE)

    GlobalVariable.confirmScreens.put('atChecker', atChecker)

    WebUI.verifyMatch(atChecker, GlobalVariable.confirmScreens.get('atChecker'), false)
} else {
    onChecker = WebUI.getText(findTestObject('menu/download/reportGenerator/checker/Label - OnChecker'), FailureHandling.CONTINUE_ON_FAILURE)

    GlobalVariable.confirmScreens.put('onChecker', onChecker)

    WebUI.verifyMatch(onChecker, GlobalVariable.confirmScreens.get('onChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

    everyChecker = WebUI.getText(findTestObject('menu/download/reportGenerator/checker/Label - EveryChecker'), FailureHandling.CONTINUE_ON_FAILURE)

    GlobalVariable.confirmScreens.put('everyChecker', everyChecker)

    WebUI.verifyMatch(everyChecker, GlobalVariable.confirmScreens.get('everyChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

    atChecker = WebUI.getText(findTestObject('menu/download/reportGenerator/checker/Label - AtChecker'), FailureHandling.CONTINUE_ON_FAILURE)

    GlobalVariable.confirmScreens.put('atChecker', atChecker)

    WebUI.verifyMatch(atChecker, GlobalVariable.confirmScreens.get('atChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

    endChecker = WebUI.getText(findTestObject('menu/download/reportGenerator/checker/Label - EndChecker'), FailureHandling.CONTINUE_ON_FAILURE)

    GlobalVariable.confirmScreens.put('endChecker', endChecker)

    WebUI.verifyMatch(endChecker, GlobalVariable.confirmScreens.get('endChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)
}

mediaDeliveryChecker = WebUI.getText(findTestObject('menu/download/reportGenerator/checker/Label - MediaDeliveyChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('mediaDeliveryChecker', mediaDeliveryChecker)

WebUI.verifyMatch(mediaDeliveryChecker, GlobalVariable.confirmScreens.get('mediaDeliveryChecker'), false)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/BTN - logoutClick'), FailureHandling.CONTINUE_ON_FAILURE)

