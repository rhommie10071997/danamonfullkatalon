import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('homelogin/homelogin.corporateid'), 'PCMSILK001', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('homelogin/homelogin.userid'), 'Silk04', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('homelogin/homelogin.password'), 'Password123', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('homelogin/homelogin.submit'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(3, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('menu/BTN - clickMenu'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/BTN - clickMenu'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/myTask/BTN - myTaskClick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/myTask/pendingTask/BTN - pendingTaskClick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/myTask/pendingTask/displayOption/displayOptionClick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/myTask/pendingTask/DL - MenuClick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('menu/myTask/pendingTask/Input - TextfieldMenu'), 'report generator', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/myTask/pendingTask/Value - MenuChoice'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/myTask/pendingTask/BTN - fromDateClick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/myTask/pendingTask/BTN - fromDateTanggalClick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/myTask/pendingTask/BTN - toDateClick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/myTask/pendingTask/BTN - fromDateTanggalClick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('menu/myTask/pendingTask/BTN - btnShow'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/myTask/pendingTask/BTN - btnShow'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(4, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.callTestCase(findTestCase('reportGenerator/accountStatement/Multi/oneTime/reportGeneratorAccStatementOneTime(Approver.Finding)'), 
    [:], FailureHandling.STOP_ON_FAILURE)

menuChecker = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(reportGenerator)/Label - Menu'), FailureHandling.CONTINUE_ON_FAILURE)

accClusterChecker = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(reportGenerator)/Label - Account Cluster'), 
    FailureHandling.CONTINUE_ON_FAILURE)

AccountChecker = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(reportGenerator)/Label - Account'), FailureHandling.CONTINUE_ON_FAILURE)

FileFormatChecker = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(reportGenerator)/Label - FileFormat'), 
    FailureHandling.CONTINUE_ON_FAILURE)

encryptionTypeChecker = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(reportGenerator)/Label - EncryptionType'), 
    FailureHandling.CONTINUE_ON_FAILURE)

consilatedFormatChecker = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(reportGenerator)/Label - Consilated Format'), 
    FailureHandling.CONTINUE_ON_FAILURE)

archivedFlagChecker = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(reportGenerator)/Label - Archive Flag'), 
    FailureHandling.CONTINUE_ON_FAILURE)

reportIdChecker = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(reportGenerator)/Label - Report ID'), 
    FailureHandling.CONTINUE_ON_FAILURE)

scheduleChecker = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(reportGenerator)/Label - Schedule'), 
    FailureHandling.CONTINUE_ON_FAILURE)

if (GlobalVariable.confirmScreens.get('Flag') == 1) {
    DateChecker = WebUI.getText(findTestObject('menu/download/reportGenerator/checker/Label - DateChecker - Portofolio'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.verifyMatch(DateChecker, GlobalVariable.confirmScreens.get('DateChecker'), false)

    atChecker = WebUI.getText(findTestObject('menu/download/reportGenerator/checker/Label - AtChecker'), FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.verifyMatch(atChecker, GlobalVariable.confirmScreens.get('atChecker'), false)
} else {
    onChecker = WebUI.getText(findTestObject('menu/download/reportGenerator/checker/Label - OnChecker'), FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.verifyMatch(onChecker, GlobalVariable.confirmScreens.get('onChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

    everyChecker = WebUI.getText(findTestObject('menu/download/reportGenerator/checker/Label - EveryChecker'), FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.verifyMatch(everyChecker, GlobalVariable.confirmScreens.get('everyChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

    atChecker = WebUI.getText(findTestObject('menu/download/reportGenerator/checker/Label - AtChecker'), FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.verifyMatch(atChecker, GlobalVariable.confirmScreens.get('atChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

    endChecker = WebUI.getText(findTestObject('menu/download/reportGenerator/checker/Label - EndChecker'), FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.verifyMatch(endChecker, GlobalVariable.confirmScreens.get('endChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)
}

mediaDeliveryChecker = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(reportGenerator)/Label - MediaDelivery'), 
    FailureHandling.CONTINUE_ON_FAILURE)

dateRangeChecker = WebUI.getText(findTestObject('menu/download/reportGenerator/checker/Label - DateRangeChecker'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(dateRangeChecker, GlobalVariable.confirmScreens.get('dateRangeChecker'), false)

WebUI.verifyMatch(menuChecker, GlobalVariable.confirmScreens.get('menuChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(accClusterChecker, GlobalVariable.confirmScreens.get('accClusterChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(AccountChecker, GlobalVariable.confirmScreens.get('AccountChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(FileFormatChecker, GlobalVariable.confirmScreens.get('FileFormatChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(encryptionTypeChecker, GlobalVariable.confirmScreens.get('encryptionTypeChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(consilatedFormatChecker, GlobalVariable.confirmScreens.get('consilatedFormatChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(archivedFlagChecker, GlobalVariable.confirmScreens.get('archivedFlagChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(reportIdChecker, GlobalVariable.confirmScreens.get('reportIdChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(scheduleChecker, GlobalVariable.confirmScreens.get('scheduleChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(mediaDeliveryChecker, GlobalVariable.confirmScreens.get('mediaDeliveryChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/myTask/pendingTask/BTN - btnApprove'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/myTask/pendingTask/Input - textfieldResponseCode'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('menu/myTask/pendingTask/Input - textfieldResponseCode'), '123456', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/myTask/pendingTask/BTN - btnApprove'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/myTask/pendingTask/BTN - approveEnding'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(10, FailureHandling.CONTINUE_ON_FAILURE)

