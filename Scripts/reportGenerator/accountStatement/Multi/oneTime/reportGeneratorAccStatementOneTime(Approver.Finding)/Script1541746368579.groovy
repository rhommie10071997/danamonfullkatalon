import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.interactions.Actions as Actions
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory

int flag = 0

int count = 1

WebDriver driver = DriverFactory.getWebDriver()



Actions action = new Actions(driver)

while (flag == 0) {
    ///tr['+count+']/td[2]
	WebUI.delay(5)
	
	driver.switchTo().frame('login')
	
	driver.switchTo().frame('mainFrame')
	
    WebElement Table = driver.findElement(By.xpath(('//table[@id="globalTable"]/tbody/tr[' + count) + ']/td[2]'))

    action.doubleClick(Table).perform()
	
	WebUI.switchToDefaultContent()
	
    reportID = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(reportGenerator)/Label - Report ID'))

    if (reportID.equals(GlobalVariable.confirmScreens.get('reportIdChecker'))) {
        break
    }
    
    WebUI.click(findTestObject('menu/myTask/pendingTask/BTN - Back'))

    count++

    WebUI.delay(5)
	
	WebUI.click(findTestObject('menu/myTask/pendingTask/RB - InsDate'), FailureHandling.CONTINUE_ON_FAILURE)
	
	WebUI.delay(2)
   
	WebUI.click(findTestObject('menu/myTask/pendingTask/DL - MenuClick'))

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.setText(findTestObject('menu/myTask/pendingTask/Input - TextfieldMenu'), 'report generator')

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.click(findTestObject('menu/myTask/pendingTask/Value - MenuChoice'))

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.waitForElementVisible(findTestObject('menu/myTask/pendingTask/BTN - btnShow'), 0, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.click(findTestObject('menu/myTask/pendingTask/BTN - btnShow'), FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.delay(10, FailureHandling.CONTINUE_ON_FAILURE)

   
}