import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

String NoAccount = '000001105535'

//Flag , oneTime = 1 & reccuring = 2
int Flag = 2

// Month , Random mode 1 s/d 3 
Random randMonth = new Random()

int monthRandom = randMonth.nextInt(3) + 1

WebUI.setText(findTestObject('homelogin/homelogin.corporateid'), 'PCMSILK001', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('homelogin/homelogin.userid'), 'silk02', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('homelogin/homelogin.password'), 'Password123', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('homelogin/homelogin.submit'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(10, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('menu/BTN - clickMenu'), 0)

WebUI.click(findTestObject('menu/BTN - clickMenu'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/download/BTN - DownloadClick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/download/reportGenerator/BTN - reportGenerator'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(15, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('menu/download/reportGenerator/BTN - setupClick'), 0)

WebUI.click(findTestObject('menu/download/reportGenerator/BTN - setupClick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(3, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/download/reportGenerator/DL - menuClick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/download/reportGenerator/Value - menuChoice(2)'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(3, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/download/reportGenerator/DL - accClusterClick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/download/reportGenerator/Value - menuChoice(1)'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/download/reportGenerator/BTN - searchClick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(4, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('menu/download/reportGenerator/Input - AccountNumberTextfield(2)'), NoAccount, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/download/reportGenerator/BTN - searchClick - iframeAccount'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/download/reportGenerator/BTN - searchClick - iframeAccount'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/download/reportGenerator/CheckBox - checkboxClick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/download/reportGenerator/BTN - showClick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/download/reportGenerator/DL - monthClick'))

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

if (monthRandom == 1) {
    WebUI.click(findTestObject('menu/download/reportGenerator/Value - menuChoice(1)'))
} else if (monthRandom == 2) {
    WebUI.click(findTestObject('menu/download/reportGenerator/Value - menuChoice(2)'))
} else {
    WebUI.click(findTestObject('menu/download/reportGenerator/Value - menuChoice(3)'))
}

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/download/reportGenerator/DL - formatFileClick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/download/reportGenerator/Value - menuChoice(1)'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

Random rand = new Random()

String str = ''

for (int i = 0; i < 2; i++) {
    ctr = ((rand.nextInt((57 - 48) + 1) + 48) as char)

    str += ctr

    ctr = ((rand.nextInt((122 - 97) + 1) + 97) as char)

    str += ctr

    ctr = ((rand.nextInt((122 - 97) + 1) + 97) as char)

    str += ctr
}

WebUI.setText(findTestObject('menu/download/reportGenerator/Input - reportIdClick'), 'RG_AccStatement' + str, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('Flag', Flag)

if (GlobalVariable.confirmScreens.get('Flag') == 1) {
    WebUI.click(findTestObject('menu/download/reportGenerator/Rb - oneTimeClick'), FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.click(findTestObject('menu/download/reportGenerator/scheduling/Date/BTN - dateClick(oneTime)'), FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.click(findTestObject('menu/download/reportGenerator/scheduling/Date/DL - monthClick'), FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.click(findTestObject('menu/download/reportGenerator/scheduling/Date/Value - monthChoice'), FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.click(findTestObject('menu/download/reportGenerator/scheduling/Date/DL - TanggalClick'), FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.click(findTestObject('menu/download/reportGenerator/scheduling/Date/DL - sessionClick(oneTime)'), FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.click(findTestObject('menu/download/reportGenerator/scheduling/Date/Value - atChoice'), FailureHandling.CONTINUE_ON_FAILURE)
} else {
    WebUI.click(findTestObject('menu/download/reportGenerator/Rb - recurringClick'), FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.callTestCase(findTestCase('reportGenerator/accountPortofolio/Multi/oneTime/forReccuringDate'), [:], FailureHandling.STOP_ON_FAILURE)
}

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/download/reportGenerator/BTN - confirmClick'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

menuChecker = WebUI.getText(findTestObject('menu/download/reportGenerator/checker/Label - MenuChecker'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('menuChecker', menuChecker)

WebUI.verifyMatch(menuChecker, GlobalVariable.confirmScreens.get('menuChecker'), false)

accClusterChecker = WebUI.getText(findTestObject('menu/download/reportGenerator/checker/Label - AccountClusterChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('accClusterChecker', accClusterChecker)

WebUI.verifyMatch(accClusterChecker, GlobalVariable.confirmScreens.get('accClusterChecker'), false)

AccountChecker = WebUI.getText(findTestObject('menu/download/reportGenerator/checker/Label - AccountChecker'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('AccountChecker', AccountChecker)

WebUI.verifyMatch(AccountChecker, GlobalVariable.confirmScreens.get('AccountChecker'), false)

dateRangeChecker = WebUI.getText(findTestObject('menu/download/reportGenerator/checker/Label - DateRangeChecker'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('dateRangeChecker', dateRangeChecker)

WebUI.verifyMatch(dateRangeChecker, GlobalVariable.confirmScreens.get('dateRangeChecker'), false)

FileFormatChecker = WebUI.getText(findTestObject('menu/download/reportGenerator/checker/Label - FileFormatChecker'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('FileFormatChecker', FileFormatChecker)

WebUI.verifyMatch(FileFormatChecker, GlobalVariable.confirmScreens.get('FileFormatChecker'), false)

encryptionTypeChecker = WebUI.getText(findTestObject('menu/download/reportGenerator/checker/Label - EncryptionTypeChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('encryptionTypeChecker', encryptionTypeChecker)

WebUI.verifyMatch(encryptionTypeChecker, GlobalVariable.confirmScreens.get('encryptionTypeChecker'), false)

consilatedFormatChecker = WebUI.getText(findTestObject('menu/download/reportGenerator/checker/Label - ConsilatedFormatChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('consilatedFormatChecker', consilatedFormatChecker)

WebUI.verifyMatch(consilatedFormatChecker, GlobalVariable.confirmScreens.get('consilatedFormatChecker'), false)

archivedFlagChecker = WebUI.getText(findTestObject('menu/download/reportGenerator/checker/Label - ArchivedFlagChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('archivedFlagChecker', archivedFlagChecker)

WebUI.verifyMatch(archivedFlagChecker, GlobalVariable.confirmScreens.get('archivedFlagChecker'), false)

reportIdChecker = WebUI.getText(findTestObject('menu/download/reportGenerator/checker/Label - ReportIDChecker'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('reportIdChecker', reportIdChecker)

WebUI.verifyMatch(reportIdChecker, GlobalVariable.confirmScreens.get('reportIdChecker'), false)

scheduleChecker = WebUI.getText(findTestObject('menu/download/reportGenerator/checker/Label - ScheduleChecker'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('scheduleChecker', scheduleChecker)

WebUI.verifyMatch(scheduleChecker, GlobalVariable.confirmScreens.get('scheduleChecker'), false)

if (GlobalVariable.confirmScreens.get('Flag') == 1) {
    DateChecker = WebUI.getText(findTestObject('menu/download/reportGenerator/checker/Label - DateChecker - Portofolio'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    GlobalVariable.confirmScreens.put('DateChecker', DateChecker)

    WebUI.verifyMatch(DateChecker, GlobalVariable.confirmScreens.get('DateChecker'), false)

    atChecker = WebUI.getText(findTestObject('menu/download/reportGenerator/checker/Label - AtChecker'), FailureHandling.CONTINUE_ON_FAILURE)

    GlobalVariable.confirmScreens.put('atChecker', atChecker)

    WebUI.verifyMatch(atChecker, GlobalVariable.confirmScreens.get('atChecker'), false)
} else {
    onChecker = WebUI.getText(findTestObject('menu/download/reportGenerator/checker/Label - OnChecker'), FailureHandling.CONTINUE_ON_FAILURE)

    GlobalVariable.confirmScreens.put('onChecker', onChecker)

    WebUI.verifyMatch(onChecker, GlobalVariable.confirmScreens.get('onChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

    everyChecker = WebUI.getText(findTestObject('menu/download/reportGenerator/checker/Label - EveryChecker'), FailureHandling.CONTINUE_ON_FAILURE)

    GlobalVariable.confirmScreens.put('everyChecker', everyChecker)

    WebUI.verifyMatch(everyChecker, GlobalVariable.confirmScreens.get('everyChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

    atChecker = WebUI.getText(findTestObject('menu/download/reportGenerator/checker/Label - AtChecker'), FailureHandling.CONTINUE_ON_FAILURE)

    GlobalVariable.confirmScreens.put('atChecker', atChecker)

    WebUI.verifyMatch(atChecker, GlobalVariable.confirmScreens.get('atChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

    endChecker = WebUI.getText(findTestObject('menu/download/reportGenerator/checker/Label - EndChecker'), FailureHandling.CONTINUE_ON_FAILURE)

    GlobalVariable.confirmScreens.put('endChecker', endChecker)

    WebUI.verifyMatch(endChecker, GlobalVariable.confirmScreens.get('endChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)
}

mediaDeliveryChecker = WebUI.getText(findTestObject('menu/download/reportGenerator/checker/Label - MediaDeliveyChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.confirmScreens.put('mediaDeliveryChecker', mediaDeliveryChecker)

WebUI.verifyMatch(mediaDeliveryChecker, GlobalVariable.confirmScreens.get('mediaDeliveryChecker'), false)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/download/reportGenerator/BTN - Submit'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/download/reportGenerator/BTN - SubmitEnding'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(15, FailureHandling.CONTINUE_ON_FAILURE)

