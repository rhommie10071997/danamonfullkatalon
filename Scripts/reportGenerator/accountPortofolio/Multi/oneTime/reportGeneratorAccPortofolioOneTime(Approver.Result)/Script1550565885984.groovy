import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

confirmScreenStatus = WebUI.getText(findTestObject('menu/myTask/pendingTask/entryScreen(reportGenerator)/Label - ConfirmScreenStatus'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(confirmScreenStatus, 'This transaction has been successfully released', true, FailureHandling.CONTINUE_ON_FAILURE)

menuChecker = WebUI.getText(findTestObject('menu/download/reportGenerator/checker/Label - MenuChecker'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(menuChecker, GlobalVariable.confirmScreens.get('menuChecker'), false)

accClusterChecker = WebUI.getText(findTestObject('menu/download/reportGenerator/checker/Label - AccountClusterChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(accClusterChecker, GlobalVariable.confirmScreens.get('accClusterChecker'), false)

AccountChecker = WebUI.getText(findTestObject('menu/download/reportGenerator/checker/Label - AccountChecker'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(AccountChecker, GlobalVariable.confirmScreens.get('AccountChecker'), false)

accountTypeChecker = WebUI.getText(findTestObject('menu/download/reportGenerator/checker/Label - AccountTypeChecker'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(accountTypeChecker, GlobalVariable.confirmScreens.get('accountTypeChecker'), false)

groupByChecker = WebUI.getText(findTestObject('menu/download/reportGenerator/checker/Label - GroupBy'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(groupByChecker, GlobalVariable.confirmScreens.get('groupByChecker'), false)

displayCurrencyChecker = WebUI.getText(findTestObject('menu/download/reportGenerator/checker/Label - DisplayCurrency'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(displayCurrencyChecker, GlobalVariable.confirmScreens.get('displayCurrencyChecker'), false)

FileFormatChecker = WebUI.getText(findTestObject('menu/download/reportGenerator/checker/Label - FileFormatChecker'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(FileFormatChecker, GlobalVariable.confirmScreens.get('FileFormatChecker'), false)

encryptionTypeChecker = WebUI.getText(findTestObject('menu/download/reportGenerator/checker/Label - EncryptionTypeChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(encryptionTypeChecker, GlobalVariable.confirmScreens.get('encryptionTypeChecker'), false)

consilatedFormatChecker = WebUI.getText(findTestObject('menu/download/reportGenerator/checker/Label - ConsilatedFormatChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(consilatedFormatChecker, GlobalVariable.confirmScreens.get('consilatedFormatChecker'), false)

archivedFlagChecker = WebUI.getText(findTestObject('menu/download/reportGenerator/checker/Label - ArchivedFlagChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(archivedFlagChecker, GlobalVariable.confirmScreens.get('archivedFlagChecker'), false)

reportIdChecker = WebUI.getText(findTestObject('menu/download/reportGenerator/checker/Label - ReportIDChecker'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(reportIdChecker, GlobalVariable.confirmScreens.get('reportIdChecker'), false)

scheduleChecker = WebUI.getText(findTestObject('menu/download/reportGenerator/checker/Label - ScheduleChecker'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(scheduleChecker, GlobalVariable.confirmScreens.get('scheduleChecker'), false)

if (GlobalVariable.confirmScreens.get('Flag') == 1) {
    DateChecker = WebUI.getText(findTestObject('menu/download/reportGenerator/checker/Label - DateChecker - Portofolio'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.verifyMatch(DateChecker, GlobalVariable.confirmScreens.get('DateChecker'), false)

    atChecker = WebUI.getText(findTestObject('menu/download/reportGenerator/checker/Label - AtChecker'), FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.verifyMatch(atChecker, GlobalVariable.confirmScreens.get('atChecker'), false)
} else {
    onChecker = WebUI.getText(findTestObject('menu/download/reportGenerator/checker/Label - OnChecker'), FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.verifyMatch(onChecker, GlobalVariable.confirmScreens.get('onChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

    everyChecker = WebUI.getText(findTestObject('menu/download/reportGenerator/checker/Label - EveryChecker'), FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.verifyMatch(everyChecker, GlobalVariable.confirmScreens.get('everyChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

    atChecker = WebUI.getText(findTestObject('menu/download/reportGenerator/checker/Label - AtChecker'), FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.verifyMatch(atChecker, GlobalVariable.confirmScreens.get('atChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)

    endChecker = WebUI.getText(findTestObject('menu/download/reportGenerator/checker/Label - EndChecker'), FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.verifyMatch(endChecker, GlobalVariable.confirmScreens.get('endChecker'), false, FailureHandling.CONTINUE_ON_FAILURE)
}

mediaDeliveryChecker = WebUI.getText(findTestObject('menu/download/reportGenerator/checker/Label - MediaDeliveyChecker'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(mediaDeliveryChecker, GlobalVariable.confirmScreens.get('mediaDeliveryChecker'), false)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('menu/BTN - logoutClick'), FailureHandling.CONTINUE_ON_FAILURE)

