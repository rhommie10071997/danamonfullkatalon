import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(2)

WebUI.waitForElementVisible(findTestObject('Log in/corpid'), 0)

WebUI.setText(findTestObject('Log in/corpid'), 'pcm000100')

WebUI.setText(findTestObject('Log in/userid'), 'snapes')

WebUI.setText(findTestObject('Log in/password'), 'Password123')

WebUI.click(findTestObject('Log in/login'))

WebUI.delay(15)

WebUI.waitForElementVisible(findTestObject('Menu/BTN - Menu Object'), 0)

WebUI.click(findTestObject('Menu/BTN - Menu Object'))

WebUI.delay(1)

WebUI.waitForElementVisible(findTestObject('Menu/BTN - My Task'), 0)

WebUI.click(findTestObject('Menu/BTN - My Task'))

WebUI.delay(1)

WebUI.waitForElementVisible(findTestObject('Menu/BTN - Pending Task'), 0)

WebUI.click(findTestObject('Menu/BTN - Pending Task'))

WebUI.delay(5)

WebUI.waitForElementVisible(findTestObject('Pending Task/Entry/Dispay Option btn'), 0)

WebUI.delay(2)

WebUI.click(findTestObject('Pending Task/Entry/Dispay Option btn'))

WebUI.delay(1)

WebUI.waitForElementVisible(findTestObject('Pending Task/Entry/Transaction Reference No radio button'), 0)

WebUI.click(findTestObject('Pending Task/Entry/Transaction Reference No radio button'))

WebUI.setText(findTestObject('Pending Task/Entry/Transaction Reference No field'), GlobalVariable.RefNo)

WebUI.waitForElementVisible(findTestObject('Pending Task/Entry/show'), 0)

WebUI.click(findTestObject('Pending Task/Entry/show'))

WebUI.delay(2)

WebUI.waitForElementVisible(findTestObject('Pending Task/Entry/show - transaction'), 0)

WebUI.doubleClick(findTestObject('Pending Task/Entry/show - transaction'))

WebUI.delay(5)

