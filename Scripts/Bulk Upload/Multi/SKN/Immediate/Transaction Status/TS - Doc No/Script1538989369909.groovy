import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Transaction Status/Bulk Upload/Inhouse/TS - Ref No/Label - Transaction Reference No'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

DocNumberDetail = WebUI.getText(findTestObject('Transaction Status/Bulk Upload/Inhouse/TS - Ref No/Label - Reference Number Detail'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(DocNumberDetail, 'Document Number Detail', false, FailureHandling.CONTINUE_ON_FAILURE)

TransactionStatus = WebUI.getText(findTestObject('Transaction Status/Bulk Upload/Inhouse/TS - Doc No/Label - Transaction Status'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TransactionStatus, 'In Progress', false, FailureHandling.CONTINUE_ON_FAILURE)

Menu = WebUI.getText(findTestObject('Transaction Status/Bulk Upload/Inhouse/TS - Ref No/Label - Menu'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(Menu, GlobalVariable.menu123, true, FailureHandling.CONTINUE_ON_FAILURE)

Product = WebUI.getText(findTestObject('Transaction Status/Bulk Upload/Inhouse/TS - Ref No/Label - Product'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(Product, GlobalVariable.product123, false, FailureHandling.CONTINUE_ON_FAILURE)

FileUpload = WebUI.getText(findTestObject('Transaction Status/Bulk Upload/Inhouse/TS - Ref No/Label - File Upload'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(FileUpload, GlobalVariable.FileUploadName123, true, FailureHandling.CONTINUE_ON_FAILURE)

FileDescription = WebUI.getText(findTestObject('Transaction Status/Bulk Upload/Inhouse/TS - Ref No/Label - File Description'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(FileDescription, GlobalVariable.FileDescription123, true, FailureHandling.CONTINUE_ON_FAILURE)

DebitAccount = WebUI.getText(findTestObject('Transaction Status/Bulk Upload/Inhouse/TS - Ref No/Label - Debit Account'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(DebitAccount, GlobalVariable.FromAccount123, false, FailureHandling.CONTINUE_ON_FAILURE)

TotalTransactionRecord = WebUI.getText(findTestObject('Transaction Status/Bulk Upload/Inhouse/TS - Ref No/Label - Total Transaction Record'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TotalTransactionRecord, GlobalVariable.TotalTransactionRecord123, true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Transaction Status/Bulk Upload/Inhouse/TS - Ref No/BTN - Total Transaction Record - See Detail Record'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2)

PopUpListRecord = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - List Record'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(PopUpListRecord, 'List Record', true, FailureHandling.CONTINUE_ON_FAILURE)

PopUpProduct = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Product'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(PopUpProduct, GlobalVariable.product123, false, FailureHandling.CONTINUE_ON_FAILURE)

PopUpDebitAccount = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Debit Account'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(PopUpDebitAccount, GlobalVariable.FromAccount123, false, FailureHandling.CONTINUE_ON_FAILURE)

PopUpTotalTransactionRecord = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Total Transaction Record'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(PopUpTotalTransactionRecord, GlobalVariable.TotalTransactionRecord123, true, FailureHandling.CONTINUE_ON_FAILURE)

TotalTransactionAmountinIDR = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Total Transaction AmountinIDR'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TotalTransactionAmountinIDR, GlobalVariable.TotalTransactionAmountinIDR123, false, FailureHandling.CONTINUE_ON_FAILURE)

TransferFee = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Transfer Fee'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TransferFee, GlobalVariable.TransferFee123, false, FailureHandling.CONTINUE_ON_FAILURE)

TotalDebitAmount = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Total Debit Amount'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TotalDebitAmount, GlobalVariable.TotalDebetAmount123, false, FailureHandling.CONTINUE_ON_FAILURE)

PopUpDeatilTableInfo = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Detail Table Info'), FailureHandling.CONTINUE_ON_FAILURE)

String[] PopUpDeatilTableInfo1 = PopUpDeatilTableInfo.split(' ')

PopUpDeatilTableInfo2 = (PopUpDeatilTableInfo1[5])

WebUI.verifyMatch(PopUpDeatilTableInfo2, GlobalVariable.TotalTransactionRecord123, true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.callTestCase(findTestCase('Bulk Upload/Detail Loop/Verify Transaction Status'), [('TableXpath') : '//table[@id="detailTable"]/tbody'
        , ('ColumnCreditAccountNumber') : 4, ('NextButton') : findTestObject('Bulk Upload/List Record/BTN - Next Page'), ('StopCondition') : [
            ('value') : 'next btn grid-nextpage disabled', ('attribute') : 'class'], ('btnnextattribute') : findTestObject(
            'Bulk Upload/List Record/BTN - Next Page - Kosong'), ('ColumnAmount') : 3], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Bulk Upload/List Record/BTN - Close'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

TotalTransactionAmountinIDR = WebUI.getText(findTestObject('Transaction Status/Bulk Upload/Inhouse/TS - Ref No/Label - Total Transaction AmountinIDR'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TotalTransactionAmountinIDR, GlobalVariable.TotalTransactionAmountinIDR123, false, FailureHandling.CONTINUE_ON_FAILURE)

TransferFee = WebUI.getText(findTestObject('Transaction Status/Bulk Upload/Inhouse/TS - Ref No/Label - Transfer Fee'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TransferFee, GlobalVariable.TransferFee123, false, FailureHandling.CONTINUE_ON_FAILURE)

SKNFee = WebUI.getText(findTestObject('Transaction Status/Bulk Upload/Inhouse/TS - Ref No/Label - SKN Fee'))

WebUI.verifyMatch(SKNFee, GlobalVariable.SKNFee123, false, FailureHandling.CONTINUE_ON_FAILURE)

TotalCharges = WebUI.getText(findTestObject('Transaction Status/Bulk Upload/Inhouse/TS - Ref No/Label - Total Charges'))

WebUI.verifyMatch(TotalCharges, GlobalVariable.TotalCharge123, false, FailureHandling.CONTINUE_ON_FAILURE)

ChargeToRemitter = WebUI.getText(findTestObject('Transaction Status/Bulk Upload/Inhouse/TS - Ref No/Label - Charge to Remitter'))

WebUI.verifyMatch(ChargeToRemitter, GlobalVariable.ChargetoRemitter123, false, FailureHandling.CONTINUE_ON_FAILURE)

TotalDebitAmount = WebUI.getText(findTestObject('Transaction Status/Bulk Upload/Inhouse/TS - Ref No/Label - Total Debit Amount'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TotalDebitAmount, GlobalVariable.TotalDebetAmount123, false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(3)

CustomKeywords.'get.Screencapture.getEntirePage'('')

WebUI.delay(3)

WebUI.scrollToElement(findTestObject('Bulk Upload/Bucket Confirm/Label - Instruction Date'), 0)

WebUI.delay(2)

CustomKeywords.'get.Screencapture.getEntirePage'('')

WebUI.delay(2)

WebUI.delay(10, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('5 - Log Out btn'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

