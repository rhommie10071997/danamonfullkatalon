import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import internal.GlobalVariable as GlobalVariable

WebDriver driver = DriverFactory.getWebDriver()
driver.switchTo().frame("login");
driver.switchTo().frame("mainFrame");
def isExist = false

'Find a matching text in a table and performing action'
while (!(WebUI.verifyElementAttributeValue(NextButton, StopCondition['attribute'], StopCondition['value'], 2, FailureHandling.OPTIONAL))) {
	
	
	'To locate table'
	WebElement Table = driver.findElement(By.xpath(TableXpath))

	'To locate rows of table it will Capture all the rows available in the table '
	List<WebElement> Rows = Table.findElements(By.tagName('tr'))

	'Loop will execute for all the rows of the table'
	table: for (int i = 0; i < Rows.size(); i++) {
		'To locate columns(cells) of that specific row'
		List<WebElement> Cols = Rows.get(i).findElements(By.tagName('td'))

		'Verifying the expected text in the each cell'
		if (Cols.get(ColumnNumber).getText().equalsIgnoreCase(ExpectedValue)) {
			'To locate anchor in the expected value matched row to perform action'
			isExist = true

			break
		}
	}
	
	if (isExist) {
		break
	}
	
	WebUI.click(NextButton)
}
driver.switchTo().defaultContent();
WebUI.verifyEqual(isExist, true)



