import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Bulk Upload/Create Result/Label - Result'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Result = WebUI.getText(findTestObject('Bulk Upload/Create Result/Label - Result'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(Result, 'Result', true, FailureHandling.CONTINUE_ON_FAILURE)

SuccessMessage = WebUI.getText(findTestObject('Bulk Upload/Create Result/Label - Success Message'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(SuccessMessage, 'Your transaction is waiting for approval', true, FailureHandling.CONTINUE_ON_FAILURE)

ReferenceNo = WebUI.getText(findTestObject('Bulk Upload/Create Result/Label - Reference No'), FailureHandling.CONTINUE_ON_FAILURE)

String[] ReferenceNo1 = ReferenceNo.split(' ')

RefNo2 = (ReferenceNo1[2])

GlobalVariable.RefNo = RefNo2

FileType = WebUI.getText(findTestObject('Bulk Upload/BucketDetail/Label - File Type'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(FileType, GlobalVariable.FileType123, true, FailureHandling.CONTINUE_ON_FAILURE)

FileTemplate = WebUI.getText(findTestObject('Bulk Upload/BucketDetail/Label - File Template'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(FileTemplate, GlobalVariable.FileTemplate123, true, FailureHandling.CONTINUE_ON_FAILURE)

FileUpload = WebUI.getText(findTestObject('Bulk Upload/BucketDetail/Label - File Upload'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(FileUpload, GlobalVariable.FileUploadName123, true, FailureHandling.CONTINUE_ON_FAILURE)

FileDescription = WebUI.getText(findTestObject('Bulk Upload/BucketDetail/Label - File Description'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(FileDescription, GlobalVariable.FileDescription123, true, FailureHandling.CONTINUE_ON_FAILURE)

TransactionType = WebUI.getText(findTestObject('Bulk Upload/BucketDetail/Label - Transaction Type'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TransactionType, 'Detail', true, FailureHandling.CONTINUE_ON_FAILURE)

DebitAccount = WebUI.getText(findTestObject('Bulk Upload/Bucket Confirm/Label - Debit Account'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(DebitAccount, '000094611548 - MUHAMMAD AMARUDIN (IDR)', false, FailureHandling.CONTINUE_ON_FAILURE)

Product = WebUI.getText(findTestObject('Bulk Upload/Bucket Confirm/Label - Product'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(Product, 'RTGS', false, FailureHandling.CONTINUE_ON_FAILURE)

TotalTransactionRecord = WebUI.getText(findTestObject('Bulk Upload/BucketDetail/Label - Total Transaction Record'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TotalTransactionRecord, GlobalVariable.TotalTransactionRecord123, true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Bulk Upload/Bucket Confirm/BTN - Total Transaction Record - See Detail Record'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

PopUpListRecord = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - List Record'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(PopUpListRecord, 'List Record', true, FailureHandling.CONTINUE_ON_FAILURE)

PopUpProduct = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Product'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(PopUpProduct, GlobalVariable.product123, false, FailureHandling.CONTINUE_ON_FAILURE)

PopUpDebitAccount = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Debit Account'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(PopUpDebitAccount, GlobalVariable.FromAccount123, false, FailureHandling.CONTINUE_ON_FAILURE)

PopUpTotalTransactionRecord = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Total Transaction Record'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(PopUpTotalTransactionRecord, GlobalVariable.TotalTransactionRecord123, true, FailureHandling.CONTINUE_ON_FAILURE)

TotalTransactionAmountinIDR = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Total Transaction AmountinIDR'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TotalTransactionAmountinIDR, GlobalVariable.TotalTransactionAmountinIDR123, false, FailureHandling.CONTINUE_ON_FAILURE)

TransferFee = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Transfer Fee'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TransferFee, GlobalVariable.TransferFee123, false, FailureHandling.CONTINUE_ON_FAILURE)

SKNFee = WebUI.getText(findTestObject('Bulk Upload/Bucket Confirm/Label - RTGS Fee'))

WebUI.verifyMatch(SKNFee, GlobalVariable.SKNFee123, false)

ChargeToRemitter = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Charge to Remitter'))

WebUI.verifyMatch(ChargeToRemitter, GlobalVariable.ChargetoRemitter123, false)

TotlCharges = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Total Charges'))

WebUI.verifyMatch(TotlCharges, GlobalVariable.TotalCharge123, false)

TotalDebitAmount = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Total Debit Amount'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TotalDebitAmount, GlobalVariable.TotalDebetAmount123, false, FailureHandling.CONTINUE_ON_FAILURE)

PopUpDeatilTableInfo = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Detail Table Info'), FailureHandling.CONTINUE_ON_FAILURE)

String[] PopUpDeatilTableInfo1 = PopUpDeatilTableInfo.split(' ')

PopUpDeatilTableInfo2 = (PopUpDeatilTableInfo1[5])

WebUI.verifyMatch(PopUpDeatilTableInfo2, GlobalVariable.TotalTransactionRecord123, true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.callTestCase(findTestCase('Bulk Upload/Detail Loop/Verify Table'), [('btnnext') : findTestObject('Bulk Upload/List Record/BTN - Next Page')
        , ('btnnextattribute') : findTestObject('Bulk Upload/List Record/BTN - Next Page - Kosong'), ('ColumnCreditAccountNumber') : 2
        , ('ColumnAmount') : 5], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Bulk Upload/List Record/BTN - Close'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Bulk Upload/Bucket Confirm/BTN - Total Transaction Record - See Detail Record'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

TotalTransactionAmountinIDR = WebUI.getText(findTestObject('Bulk Upload/Bucket Confirm/Label - Total Transaction AmountinIDR'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TotalTransactionAmountinIDR, GlobalVariable.TotalTransactionAmountinIDR123, false, FailureHandling.CONTINUE_ON_FAILURE)

TransferFee = WebUI.getText(findTestObject('Bulk Upload/Bucket Confirm/Label - Transfer Fee'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TransferFee, GlobalVariable.TransferFee123, false, FailureHandling.CONTINUE_ON_FAILURE)

SKNFee = WebUI.getText(findTestObject('Bulk Upload/Bucket Confirm/Label - RTGS Fee'))

WebUI.verifyMatch(SKNFee, GlobalVariable.SKNFee123, false, FailureHandling.CONTINUE_ON_FAILURE)

TotalCharges = WebUI.getText(findTestObject('Bulk Upload/Bucket Confirm/Label - Total Charges'))

WebUI.verifyMatch(TotalCharges, GlobalVariable.TotalCharge123, false, FailureHandling.CONTINUE_ON_FAILURE)

ChargeToRemitter = WebUI.getText(findTestObject('Bulk Upload/Bucket Confirm/Label - Charge to Remitter'))

WebUI.verifyMatch(ChargeToRemitter, GlobalVariable.ChargetoRemitter123, false, FailureHandling.CONTINUE_ON_FAILURE)

TotalDebitAmount = WebUI.getText(findTestObject('Bulk Upload/Bucket Confirm/Label - Total Debit Amount'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TotalDebitAmount, GlobalVariable.TotalDebetAmount123, false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(3)

CustomKeywords.'get.Screencapture.getEntirePage'('')

WebUI.delay(3)

WebUI.scrollToElement(findTestObject('Bulk Upload/Bucket Confirm/Label - Product'), 0)

WebUI.delay(2)

CustomKeywords.'get.Screencapture.getEntirePage'('')

WebUI.delay(2)

WebUI.click(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Create Result/BTN - Done'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('5 - Log Out btn'), FailureHandling.CONTINUE_ON_FAILURE)

