import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW

'---------------------------------------------------------------------------------------------------------------'

'Tempat Filenya di mana'
PathFile = 'C:\\Users\\USER\\Desktop\\Bulk Upload - Upload File\\Single User\\bulkUploadnewinhouseSingle.csv'

String[] PathFileSplit = PathFile.split('\\\\')

GlobalVariable.confirmScreens.put('FileUploadName', PathFileSplit[6])

'File Descriptionya'
FileDesc = 'FileDescArdo'

'---------------------------------------------------------------------------------------------------------------'
WebUI.openBrowser(GlobalVariable.url3, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.maximizeWindow()

WebUI.waitForElementVisible(findTestObject('1 - Frame/1 - iframe login'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Log in/corpid'), 'pcmkillua', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Log in/userid'), 'killua', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Log in/password'), 'Password123', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(4)

CustomKeywords.'get.Screencapture.getEntirePage'('')

WebUI.delay(4)

WebUI.click(findTestObject('Log in/login'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(3, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementPresent(findTestObject('Menu/BTN - Menu Object'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Menu/BTN - Menu Object'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementClickable(findTestObject('Menu/BTN - Menu Object'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Menu/BTN - Menu Object'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Menu/BTN - Transfer Management'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Menu/BTN - Transfer Management'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Menu/BTN - Bulk Upload'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Menu/BTN - Bulk Upload'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Bulk Upload/New Entry/Label - New Entry'), 0, FailureHandling.CONTINUE_ON_FAILURE)

NewEntry = WebUI.getText(findTestObject('Bulk Upload/New Entry/Label - New Entry'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(NewEntry, 'New Entry', true, FailureHandling.CONTINUE_ON_FAILURE)

AccessibleProduct = WebUI.getText(findTestObject('Bulk Upload/New Entry/Label - Accessible Product'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(AccessibleProduct, 'Online, International Foreign Currency Transfer, SKN/LLG, In-House (Overbooking), RTGS', 
    false, FailureHandling.CONTINUE_ON_FAILURE)

FileType = WebUI.getText(findTestObject('Bulk Upload/New Entry/Label - File Type'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.FileType123 = FileType

WebUI.verifyMatch(FileType, 'Non Encrypted', true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Bulk Upload/New Entry/DL - File Template - Select File Template'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Bulk Upload/New Entry/TF - Set Text Drop List'), 'mapping_bulk_upload_latest', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Bulk Upload/New Entry/DL - File Template - Select File Template - 1'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

FileTemplate = WebUI.getText(findTestObject('Bulk Upload/New Entry/DL - File Template - Select File Template'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.FileTemplate123 = FileTemplate

WebUI.setText(findTestObject('Bulk Upload/New Entry/BTN - File Upload - Choose File'), PathFile, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

Random rand = new Random()

int RandFileDesc

RandFileDesc = rand.nextInt(99)

RandFileDesc1 = (FileDesc + RandFileDesc)

GlobalVariable.FileDescription123 = RandFileDesc1

WebUI.setText(findTestObject('Bulk Upload/New Entry/TF - File Description'), RandFileDesc1, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Bulk Upload/New Entry/BTN - Continue'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

