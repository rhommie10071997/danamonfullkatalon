import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Bulk Upload/Bucket Confirm/Label - Bucket Confirm'), 0, FailureHandling.CONTINUE_ON_FAILURE)

LabelBucketDetail = WebUI.getText(findTestObject('Bulk Upload/Bucket Confirm/Label - Bucket Confirm'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(LabelBucketDetail, 'Confirm', true, FailureHandling.CONTINUE_ON_FAILURE)

FileType = WebUI.getText(findTestObject('Bulk Upload/BucketDetail/Label - File Type'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(FileType, GlobalVariable.FileType123, true, FailureHandling.CONTINUE_ON_FAILURE)

FileTemplate = WebUI.getText(findTestObject('Bulk Upload/BucketDetail/Label - File Template'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(FileTemplate, GlobalVariable.FileTemplate123, true, FailureHandling.CONTINUE_ON_FAILURE)

FileUpload = WebUI.getText(findTestObject('Bulk Upload/BucketDetail/Label - File Upload'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(FileUpload, FileUpload, true, FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.FileUploadName123 = FileUpload

FileDescription = WebUI.getText(findTestObject('Bulk Upload/BucketDetail/Label - File Description'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(FileDescription, GlobalVariable.FileDescription123, true, FailureHandling.CONTINUE_ON_FAILURE)

TransactionType = WebUI.getText(findTestObject('Bulk Upload/BucketDetail/Label - Transaction Type'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TransactionType, 'Detail', true, FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.TransactionType123 = TransactionType

TotalRecordInFile = WebUI.getText(findTestObject('Bulk Upload/BucketDetail/Label - Total Record In File'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TotalRecordInFile, GlobalVariable.TotalRecordInFile123, true, FailureHandling.CONTINUE_ON_FAILURE)

TotalTransactionRecord = WebUI.getText(findTestObject('Bulk Upload/BucketDetail/Label - Total Transaction Record'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TotalTransactionRecord, GlobalVariable.TotalTransactionRecord123, true, FailureHandling.CONTINUE_ON_FAILURE)

TotalUploadAmountInIDR = WebUI.getText(findTestObject('Bulk Upload/BucketDetail/Label - Total Upload Amount in USD'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TotalUploadAmountInIDR, GlobalVariable.TotalUploadAmountinIDR123, true, FailureHandling.CONTINUE_ON_FAILURE)

DebitAccount = WebUI.getText(findTestObject('Bulk Upload/Bucket Confirm/Label - Debit Account'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.FromAccount123 = DebitAccount

WebUI.verifyMatch(DebitAccount, GlobalVariable.FromAccount123, false, FailureHandling.CONTINUE_ON_FAILURE)

Product = WebUI.getText(findTestObject('Bulk Upload/Bucket Confirm/Label - Product'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(Product, 'International Foreign Currency Transfer', false, FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.product123 = Product

TotalTransactionRecord2 = WebUI.getText(findTestObject('Bulk Upload/Bucket Confirm/Label - Total Transaction Record - 2'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TotalTransactionRecord2, GlobalVariable.TotalTransactionRecord123, true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Bulk Upload/Bucket Confirm/BTN - Total Transaction Record - See Detail Record'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

PopUpListRecord = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - List Record'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(PopUpListRecord, 'List Record', true, FailureHandling.CONTINUE_ON_FAILURE)

PopUpProduct = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Product'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(PopUpProduct, GlobalVariable.product123, false, FailureHandling.CONTINUE_ON_FAILURE)

PopUpDebitAccount = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Debit Account'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(PopUpDebitAccount, GlobalVariable.FromAccount123, false, FailureHandling.CONTINUE_ON_FAILURE)

PopUpTotalTransactionRecord = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Total Transaction Record'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(PopUpTotalTransactionRecord, GlobalVariable.TotalTransactionRecord123, true, FailureHandling.CONTINUE_ON_FAILURE)

TotalTransactionAmountinIDR = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Total Transaction AmountinUSD'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.TotalTransactionAmountinIDR123 = TotalTransactionAmountinIDR

WebUI.verifyMatch(TotalTransactionAmountinIDR, GlobalVariable.TotalTransactionAmountinIDR123, false, FailureHandling.CONTINUE_ON_FAILURE)

TotalTransactionAmountinUSDequivalent = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Total Transaction AmountUSD - Equivalent to Debit Account'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.TotalTransactionAmountEquivalent123 = TotalTransactionAmountinUSDequivalent

WebUI.verifyMatch(TotalTransactionAmountinUSDequivalent, GlobalVariable.TotalTransactionAmountEquivalent123, false, FailureHandling.CONTINUE_ON_FAILURE)

not_run: ExchangeRate = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Exchange Rate'), FailureHandling.CONTINUE_ON_FAILURE)

not_run: GlobalVariable.ExchangeRate123 = ExchangeRate

not_run: WebUI.verifyMatch(ExchangeRate, GlobalVariable.ExchangeRate123, false, FailureHandling.CONTINUE_ON_FAILURE)

not_run: ExchangeRateBuy = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Exchange Rate - Buy Rate'), FailureHandling.CONTINUE_ON_FAILURE)

not_run: GlobalVariable.ExchangeRateBuy123 = ExchangeRateBuy

not_run: WebUI.verifyMatch(ExchangeRateBuy, GlobalVariable.ExchangeRateBuy123, false, FailureHandling.CONTINUE_ON_FAILURE)

not_run: ExchangeRateSell = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Exchange Rate - Sell Rate'), FailureHandling.CONTINUE_ON_FAILURE)

not_run: GlobalVariable.ExchangeRateSell123 = ExchangeRateSell

not_run: WebUI.verifyMatch(ExchangeRateSell, GlobalVariable.ExchangeRateSell123, false, FailureHandling.CONTINUE_ON_FAILURE)

MinimumTransactionFee = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Minimum Transaction Fee'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.MinimumTransactionFee123 = MinimumTransactionFee

WebUI.verifyMatch(MinimumTransactionFee, GlobalVariable.MinimumTransactionFee123, false, FailureHandling.CONTINUE_ON_FAILURE)

Provision = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Provision'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.Provision123 = Provision

WebUI.verifyMatch(Provision, GlobalVariable.Provision123, false, FailureHandling.CONTINUE_ON_FAILURE)

FullAmountCharge = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Full Amount Charge'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.FullAmountCharge123 = FullAmountCharge

WebUI.verifyMatch(FullAmountCharge, GlobalVariable.FullAmountCharge123, false, FailureHandling.CONTINUE_ON_FAILURE)

FullAmountChargeEquivalent = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Full Amount Charge - Equivalent'), 
    FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.FullAmountChargeEquivalent123 = FullAmountChargeEquivalent

WebUI.verifyMatch(FullAmountChargeEquivalent, GlobalVariable.FullAmountChargeEquivalent123, false, FailureHandling.CONTINUE_ON_FAILURE)

CorrespondentBankFee = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Correspondent Bank Fee'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.CorrespondentBankFee123 = CorrespondentBankFee

WebUI.verifyMatch(CorrespondentBankFee, GlobalVariable.CorrespondentBankFee123, false, FailureHandling.CONTINUE_ON_FAILURE)

CableFee = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Cable Fee'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.CableFee123 = CableFee

WebUI.verifyMatch(CableFee, GlobalVariable.CableFee123, false, FailureHandling.CONTINUE_ON_FAILURE)

TotalCharges = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Total Charges'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.TotalCharge123 = TotalCharges

WebUI.verifyMatch(TotalCharges, GlobalVariable.TotalCharge123, false, FailureHandling.CONTINUE_ON_FAILURE)

ChargeToRemitter = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Charge to Remitter'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.ChargetoRemitter123 = ChargeToRemitter

WebUI.verifyMatch(ChargeToRemitter, GlobalVariable.ChargetoRemitter123, false, FailureHandling.CONTINUE_ON_FAILURE)

TotalDebitAmount = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Total Debit Amount'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TotalDebitAmount, TotalDebitAmount, false, FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.TotalDebetAmount123 = TotalDebitAmount

GlobalVariable.TotalTransactionAmountinIDR123 = TotalTransactionAmountinIDR

PopUpDeatilTableInfo = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Detail Table Info'), FailureHandling.CONTINUE_ON_FAILURE)

String[] PopUpDeatilTableInfo1 = PopUpDeatilTableInfo.split(' ')

PopUpDeatilTableInfo2 = (PopUpDeatilTableInfo1[5])

WebUI.verifyMatch(PopUpDeatilTableInfo2, GlobalVariable.TotalTransactionRecord123, true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.getAttribute(findTestObject('Bulk Upload/List Record/BTN - Next Page - Kosong'), 'class', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.callTestCase(findTestCase('Bulk Upload/Detail Loop/Get Text - Table'), [('btnnext') : findTestObject('Bulk Upload/List Record/BTN - Next Page')
        , ('btnnextattribute') : findTestObject('Bulk Upload/List Record/BTN - Next Page - Kosong'), ('ColumnCreditAccountNumber') : 2
        , ('ColumnAmount') : 5], FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Bulk Upload/List Record/BTN - Close'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Bulk Upload/Bucket Confirm/BTN - Total Transaction Record - See Detail Record'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

TotalTransactionAmountinIDR = WebUI.getText(findTestObject('Bulk Upload/Bucket Confirm/Label - Total Transaction AmountinUSD'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TotalTransactionAmountinIDR, GlobalVariable.TotalTransactionAmountinIDR123, false, FailureHandling.CONTINUE_ON_FAILURE)

TotalTransactionAmountUSDEquivalent = WebUI.getText(findTestObject('Bulk Upload/Bucket Confirm/Label - Total Transaction AmountUSD - Equivalent to Debit Account'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TotalTransactionAmountUSDEquivalent, GlobalVariable.TotalTransactionAmountEquivalent123, false, FailureHandling.CONTINUE_ON_FAILURE)

not_run: ExchangeRate = WebUI.getText(findTestObject('Bulk Upload/Bucket Confirm/Label - Exchange Rate'), FailureHandling.CONTINUE_ON_FAILURE)

not_run: WebUI.verifyMatch(ExchangeRate, GlobalVariable.ExchangeRate123, false, FailureHandling.CONTINUE_ON_FAILURE)

not_run: ExchangeRateBuy = WebUI.getText(findTestObject('Bulk Upload/Bucket Confirm/Label - Exchange Rate - Buy Rate'), 
    FailureHandling.CONTINUE_ON_FAILURE)

not_run: WebUI.verifyMatch(ExchangeRateBuy, GlobalVariable.ExchangeRateBuy123, false, FailureHandling.CONTINUE_ON_FAILURE)

not_run: ExchangeRateSell = WebUI.getText(findTestObject('Bulk Upload/Bucket Confirm/Label - Exchange Rate - Sell Rate'), 
    FailureHandling.CONTINUE_ON_FAILURE)

not_run: WebUI.verifyMatch(ExchangeRateSell, GlobalVariable.ExchangeRateSell123, false, FailureHandling.CONTINUE_ON_FAILURE)

MinimumTransactionFee = WebUI.getText(findTestObject('Bulk Upload/Bucket Confirm/Label - Minimum Transaction Fee'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(MinimumTransactionFee, GlobalVariable.MinimumTransactionFee123, false, FailureHandling.CONTINUE_ON_FAILURE)

FullAmountCharge = WebUI.getText(findTestObject('Bulk Upload/Bucket Confirm/Label - Full Amount Charge'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(FullAmountCharge, GlobalVariable.FullAmountCharge123, false, FailureHandling.CONTINUE_ON_FAILURE)

FullAmountChargeEquivalent = WebUI.getText(findTestObject('Bulk Upload/Bucket Confirm/Label - Full Amount Charge - Equivalent'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(FullAmountChargeEquivalent, GlobalVariable.FullAmountChargeEquivalent123, false, FailureHandling.CONTINUE_ON_FAILURE)

CorrespondentBankFee = WebUI.getText(findTestObject('Bulk Upload/Bucket Confirm/Label - Correspondent Bank Fee'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(CorrespondentBankFee, GlobalVariable.CorrespondentBankFee123, false, FailureHandling.CONTINUE_ON_FAILURE)

Provision = WebUI.getText(findTestObject('Bulk Upload/Bucket Confirm/Label - Provision'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(Provision, GlobalVariable.Provision123, false, FailureHandling.CONTINUE_ON_FAILURE)

CableFee = WebUI.getText(findTestObject('Bulk Upload/Bucket Confirm/Label - Cable Fee'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(CableFee, GlobalVariable.CableFee123, false, FailureHandling.CONTINUE_ON_FAILURE)

TotalCharge = WebUI.getText(findTestObject('Bulk Upload/Bucket Confirm/Label - Total Charges'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TotalCharge, GlobalVariable.TotalCharge123, false, FailureHandling.CONTINUE_ON_FAILURE)

ChargetoRemitter = WebUI.getText(findTestObject('Bulk Upload/Bucket Confirm/Label - Charge to Remitter'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(ChargetoRemitter, GlobalVariable.ChargetoRemitter123, false, FailureHandling.CONTINUE_ON_FAILURE)

TotalDebitAmount = WebUI.getText(findTestObject('Bulk Upload/Bucket Confirm/Label - Total Debit Amount'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TotalDebitAmount, GlobalVariable.TotalDebetAmount123, false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(3)

CustomKeywords.'get.Screencapture.getEntirePage'('')

WebUI.delay(3)

WebUI.scrollToElement(findTestObject('Bulk Upload/Bucket Confirm/Label - Product'), 0)

WebUI.delay(2)

CustomKeywords.'get.Screencapture.getEntirePage'('')

WebUI.delay(2)

WebUI.setText(findTestObject('Pending Task/Balance Transfer/Inhouse/Detail/TextField - Response Code'), '123456', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.scrollToElement(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Confirm Screen/BTN - Sumbit'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Confirm Screen/BTN - Sumbit'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(3, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Confirm Screen/BTN - Pop Up - OK'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Balance Transfer/Balance Transfer - inhouse/Balance Transfer - Confirm Screen/BTN - Pop Up - OK'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(4, FailureHandling.CONTINUE_ON_FAILURE)

