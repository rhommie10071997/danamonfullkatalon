import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Bulk Upload/BucketDetail/Label - BucketDetail'), 0, FailureHandling.CONTINUE_ON_FAILURE)

LabelBucketDetail = WebUI.getText(findTestObject('Bulk Upload/BucketDetail/Label - BucketDetail'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(LabelBucketDetail, 'BucketDetail', true, FailureHandling.CONTINUE_ON_FAILURE)

FileUploadStatus = WebUI.getText(findTestObject('Bulk Upload/BucketDetail/Label - File Upload Status - Comp'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(FileUploadStatus, 'Complete', true, FailureHandling.CONTINUE_ON_FAILURE)

FileType = WebUI.getText(findTestObject('Bulk Upload/BucketDetail/Label - File Type'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(FileType, GlobalVariable.FileType123, true, FailureHandling.CONTINUE_ON_FAILURE)

FileTemplate = WebUI.getText(findTestObject('Bulk Upload/BucketDetail/Label - File Template'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(FileTemplate, GlobalVariable.FileTemplate123, true, FailureHandling.CONTINUE_ON_FAILURE)

FileUpload = WebUI.getText(findTestObject('Bulk Upload/BucketDetail/Label - File Upload'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(FileUpload, FileUpload, true, FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.FileUploadName123 = FileUpload

FileDescription = WebUI.getText(findTestObject('Bulk Upload/BucketDetail/Label - File Description'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(FileDescription, GlobalVariable.FileDescription123, true, FailureHandling.CONTINUE_ON_FAILURE)

TransactionType = WebUI.getText(findTestObject('Bulk Upload/BucketDetail/Label - Transaction Type'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TransactionType, 'Detail', true, FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.TransactionType123 = TransactionType

TotalRecordInFile = WebUI.getText(findTestObject('Bulk Upload/BucketDetail/Label - Total Record In File'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.TotalRecordInFile123 = TotalRecordInFile

WebUI.verifyMatch(TotalRecordInFile, GlobalVariable.TotalRecordInFile123, true, FailureHandling.CONTINUE_ON_FAILURE)

TotalTransactionRecord = WebUI.getText(findTestObject('Bulk Upload/BucketDetail/Label - Total Transaction Record'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.TotalTransactionRecord123 = TotalTransactionRecord

WebUI.verifyMatch(TotalTransactionRecord, GlobalVariable.TotalTransactionRecord123, true, FailureHandling.CONTINUE_ON_FAILURE)

TotalSucces = WebUI.getText(findTestObject('Bulk Upload/BucketDetail/Label - Total Succes'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TotalSucces, GlobalVariable.TotalRecordInFile123, true, FailureHandling.CONTINUE_ON_FAILURE)

TotalFailed = WebUI.getText(findTestObject('Bulk Upload/BucketDetail/Label - Total Failed'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TotalFailed, '0', true, FailureHandling.CONTINUE_ON_FAILURE)

TotalUploadAmountInIDR = WebUI.getText(findTestObject('Bulk Upload/BucketDetail/Label - Total Upload Amount in USD'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.TotalUploadAmountinIDR123 = TotalUploadAmountInIDR

WebUI.delay(4)

CustomKeywords.'get.Screencapture.getEntirePage'('')

WebUI.delay(4)

WebUI.click(findTestObject('Bulk Upload/BucketDetail/BTN - Confirm'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

