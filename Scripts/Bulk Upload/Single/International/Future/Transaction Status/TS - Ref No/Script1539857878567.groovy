import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Transaction Status/Bulk Upload/Inhouse/TS - Ref No/Label - Reference Number Detail'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

ReferenceNumberDetail = WebUI.getText(findTestObject('Transaction Status/Bulk Upload/Inhouse/TS - Ref No/Label - Reference Number Detail'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(ReferenceNumberDetail, 'Reference Number Detail', false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(8, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Transaction Status/Bulk Upload/Inhouse/TS - Ref No/Label - Transaction Reference No'), 
    0, FailureHandling.CONTINUE_ON_FAILURE)

TransactionReferenceNo = WebUI.getText(findTestObject('Transaction Status/Bulk Upload/Inhouse/TS - Ref No/Label - Transaction Reference No'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TransactionReferenceNo, GlobalVariable.RefNo, true, FailureHandling.CONTINUE_ON_FAILURE)

Menu = WebUI.getText(findTestObject('Transaction Status/Bulk Upload/Inhouse/TS - Ref No/Label - Menu'), FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.menu123 = 'Bulk Upload'

WebUI.verifyMatch(Menu, GlobalVariable.menu123, true, FailureHandling.CONTINUE_ON_FAILURE)

Product = WebUI.getText(findTestObject('Transaction Status/Bulk Upload/Inhouse/TS - Ref No/Label - Product'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(Product, GlobalVariable.product123, false, FailureHandling.CONTINUE_ON_FAILURE)

FileUpload = WebUI.getText(findTestObject('Transaction Status/Bulk Upload/Inhouse/TS - Ref No/Label - File Upload'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(FileUpload, GlobalVariable.FileUploadName123, true, FailureHandling.CONTINUE_ON_FAILURE)

FileDescription = WebUI.getText(findTestObject('Transaction Status/Bulk Upload/Inhouse/TS - Ref No/Label - File Description'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(FileDescription, GlobalVariable.FileDescription123, true, FailureHandling.CONTINUE_ON_FAILURE)

DebitAccount = WebUI.getText(findTestObject('Transaction Status/Bulk Upload/Inhouse/TS - Ref No/Label - Debit Account'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(DebitAccount, GlobalVariable.FromAccount123, false, FailureHandling.CONTINUE_ON_FAILURE)

InstructionDate = WebUI.getText(findTestObject('Transaction Status/Bulk Upload/Inhouse/TS - Ref No/Label - Instruction Date'))

WebUI.verifyMatch(InstructionDate, GlobalVariable.InstructionDate123, false, FailureHandling.CONTINUE_ON_FAILURE)

TotalTransactionRecord = WebUI.getText(findTestObject('Transaction Status/Bulk Upload/Inhouse/TS - Ref No/Label - Total Transaction Record'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TotalTransactionRecord, GlobalVariable.TotalTransactionRecord123, true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Transaction Status/Bulk Upload/Inhouse/TS - Ref No/BTN - Total Transaction Record - See Detail Record'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2)

PopUpListRecord = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - List Record'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(PopUpListRecord, 'List Record', true, FailureHandling.CONTINUE_ON_FAILURE)

PopUpProduct = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Product'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(PopUpProduct, GlobalVariable.product123, false, FailureHandling.CONTINUE_ON_FAILURE)

PopUpDebitAccount = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Debit Account'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(PopUpDebitAccount, GlobalVariable.FromAccount123, false, FailureHandling.CONTINUE_ON_FAILURE)

InstructionDate = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Instruction Date'))

WebUI.verifyMatch(InstructionDate, GlobalVariable.InstructionDate123, false, FailureHandling.CONTINUE_ON_FAILURE)

PopUpTotalTransactionRecord = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Total Transaction Record'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(PopUpTotalTransactionRecord, GlobalVariable.TotalTransactionRecord123, true, FailureHandling.CONTINUE_ON_FAILURE)

TotalTransactionAmountinIDR = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Total Transaction AmountinUSD'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TotalTransactionAmountinIDR, GlobalVariable.TotalTransactionAmountinIDR123, false, FailureHandling.CONTINUE_ON_FAILURE)

TotalTransactionAmountinUSDequivalent = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Total Transaction AmountUSD - Equivalent to Debit Account'))

WebUI.verifyMatch(TotalTransactionAmountinUSDequivalent, GlobalVariable.TotalTransactionAmountEquivalent123, false, FailureHandling.CONTINUE_ON_FAILURE)

not_run: ExchangeRate = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Exchange Rate'))

not_run: WebUI.verifyMatch(ExchangeRate, GlobalVariable.ExchangeRate123, false, FailureHandling.CONTINUE_ON_FAILURE)

not_run: ExchangeRateBuy = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Exchange Rate - Buy Rate'))

not_run: WebUI.verifyMatch(ExchangeRateBuy, GlobalVariable.ExchangeRateBuy123, false, FailureHandling.CONTINUE_ON_FAILURE)

not_run: ExchangeRateSell = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Exchange Rate - Sell Rate'))

not_run: WebUI.verifyMatch(ExchangeRateSell, GlobalVariable.ExchangeRateSell123, false, FailureHandling.CONTINUE_ON_FAILURE)

MinimumTransactionFee = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Minimum Transaction Fee'))

WebUI.verifyMatch(MinimumTransactionFee, GlobalVariable.MinimumTransactionFee123, false, FailureHandling.CONTINUE_ON_FAILURE)

Provision = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Provision'))

WebUI.verifyMatch(Provision, GlobalVariable.Provision123, false, FailureHandling.CONTINUE_ON_FAILURE)

FullAmountCharge = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Full Amount Charge'))

WebUI.verifyMatch(FullAmountCharge, GlobalVariable.FullAmountCharge123, false, FailureHandling.CONTINUE_ON_FAILURE)

FullAmountChargeEquivalent = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Full Amount Charge - Equivalent'))

WebUI.verifyMatch(FullAmountChargeEquivalent, GlobalVariable.FullAmountChargeEquivalent123, false, FailureHandling.CONTINUE_ON_FAILURE)

CorrespondentBankFee = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Correspondent Bank Fee'))

WebUI.verifyMatch(CorrespondentBankFee, GlobalVariable.CorrespondentBankFee123, false, FailureHandling.CONTINUE_ON_FAILURE)

CableFee = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Cable Fee'))

WebUI.verifyMatch(CableFee, GlobalVariable.CableFee123, false, FailureHandling.CONTINUE_ON_FAILURE)

TotalCharges = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Total Charges'))

WebUI.verifyMatch(TotalCharges, GlobalVariable.TotalCharge123, false, FailureHandling.CONTINUE_ON_FAILURE)

ChargeToRemitter = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Charge to Remitter'))

WebUI.verifyMatch(ChargeToRemitter, GlobalVariable.ChargetoRemitter123, false, FailureHandling.CONTINUE_ON_FAILURE)

TotalDebitAmount = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Total Debit Amount'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TotalDebitAmount, TotalDebitAmount, false, FailureHandling.CONTINUE_ON_FAILURE)

PopUpDeatilTableInfo = WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Detail Table Info'), FailureHandling.CONTINUE_ON_FAILURE)

String[] PopUpDeatilTableInfo1 = PopUpDeatilTableInfo.split(' ')

PopUpDeatilTableInfo2 = (PopUpDeatilTableInfo1[5])

WebUI.verifyMatch(PopUpDeatilTableInfo2, GlobalVariable.TotalTransactionRecord123, true, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.callTestCase(findTestCase('Bulk Upload/Detail Loop/Verify Transaction Status'), [('TableXpath') : '//table[@id="detailTable"]/tbody'
        , ('ColumnCreditAccountNumber') : 4, ('NextButton') : findTestObject('Bulk Upload/List Record/BTN - Next Page'), ('StopCondition') : [
            ('value') : 'next btn grid-nextpage disabled', ('attribute') : 'class'], ('btnnextattribute') : findTestObject(
            'Bulk Upload/List Record/BTN - Next Page - Kosong'), ('ColumnAmount') : 3], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Bulk Upload/List Record/BTN - Close'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(10, FailureHandling.CONTINUE_ON_FAILURE)

TotalTransactionAmountinIDR = WebUI.getText(findTestObject('Transaction Status/Bulk Upload/Inhouse/TS - Ref No/Label - Total Transaction AmountinUSD'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TotalTransactionAmountinIDR, GlobalVariable.TotalTransactionAmountinIDR123, false, FailureHandling.CONTINUE_ON_FAILURE)

TotalTransactionAmountUSDEquivalent = WebUI.getText(findTestObject('Transaction Status/Bulk Upload/Inhouse/TS - Ref No/Label - Total Transaction AmountUSD - Equivalent to Debit Account'))

WebUI.verifyMatch(TotalTransactionAmountUSDEquivalent, GlobalVariable.TotalTransactionAmountEquivalent123, false, FailureHandling.CONTINUE_ON_FAILURE)

not_run: ExchangeRate = WebUI.getText(findTestObject('Transaction Status/Bulk Upload/Inhouse/TS - Ref No/Label - Exchange Rate'))

not_run: WebUI.verifyMatch(ExchangeRate, GlobalVariable.ExchangeRate123, false, FailureHandling.CONTINUE_ON_FAILURE)

not_run: ExchangeRateBuy = WebUI.getText(findTestObject('Transaction Status/Bulk Upload/Inhouse/TS - Ref No/Label - Exchange Rate - Buy Rate'))

not_run: WebUI.verifyMatch(ExchangeRateBuy, GlobalVariable.ExchangeRateBuy123, false, FailureHandling.CONTINUE_ON_FAILURE)

not_run: ExchangeRateSell = WebUI.getText(findTestObject('Transaction Status/Bulk Upload/Inhouse/TS - Ref No/Label - Exchange Rate - Sell Rate'))

not_run: WebUI.verifyMatch(ExchangeRateSell, GlobalVariable.ExchangeRateSell123, false, FailureHandling.CONTINUE_ON_FAILURE)

MinimumTransactionFee = WebUI.getText(findTestObject('Transaction Status/Bulk Upload/Inhouse/TS - Ref No/Label - Minimum Transaction Fee'))

WebUI.verifyMatch(MinimumTransactionFee, GlobalVariable.MinimumTransactionFee123, false, FailureHandling.CONTINUE_ON_FAILURE)

FullAmountCharge = WebUI.getText(findTestObject('Transaction Status/Bulk Upload/Inhouse/TS - Ref No/Label - Full Amount Charge'))

WebUI.verifyMatch(FullAmountCharge, GlobalVariable.FullAmountCharge123, false, FailureHandling.CONTINUE_ON_FAILURE)

FullAmountChargeEquivalent = WebUI.getText(findTestObject('Transaction Status/Bulk Upload/Inhouse/TS - Ref No/Label - Full Amount Charge - Equivalent'))

WebUI.verifyMatch(FullAmountChargeEquivalent, GlobalVariable.FullAmountChargeEquivalent123, false, FailureHandling.CONTINUE_ON_FAILURE)

CorrespondentBankFee = WebUI.getText(findTestObject('Transaction Status/Bulk Upload/Inhouse/TS - Ref No/Label - Correspondent Bank Fee'))

WebUI.verifyMatch(CorrespondentBankFee, GlobalVariable.CorrespondentBankFee123, false, FailureHandling.CONTINUE_ON_FAILURE)

Provision = WebUI.getText(findTestObject('Transaction Status/Bulk Upload/Inhouse/TS - Ref No/Label - Provision'))

WebUI.verifyMatch(Provision, GlobalVariable.Provision123, false, FailureHandling.CONTINUE_ON_FAILURE)

CableFee = WebUI.getText(findTestObject('Transaction Status/Bulk Upload/Inhouse/TS - Ref No/Label - Cable Fee'))

WebUI.verifyMatch(CableFee, GlobalVariable.CableFee123, false, FailureHandling.CONTINUE_ON_FAILURE)

TotalCharge = WebUI.getText(findTestObject('Transaction Status/Bulk Upload/Inhouse/TS - Ref No/Label - Total Charges'))

WebUI.verifyMatch(TotalCharge, GlobalVariable.TotalCharge123, false, FailureHandling.CONTINUE_ON_FAILURE)

ChargetoRemitter = WebUI.getText(findTestObject('Transaction Status/Bulk Upload/Inhouse/TS - Ref No/Label - Charge to Remitter'))

WebUI.verifyMatch(ChargetoRemitter, GlobalVariable.ChargetoRemitter123, false, FailureHandling.CONTINUE_ON_FAILURE)

TotalDebitAmount = WebUI.getText(findTestObject('Transaction Status/Bulk Upload/Inhouse/TS - Ref No/Label - Total Debit Amount'), 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(TotalDebitAmount, GlobalVariable.TotalDebetAmount123, false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(3)

CustomKeywords.'get.Screencapture.getEntirePage'('')

WebUI.delay(3)

WebUI.scrollToElement(findTestObject('Transaction Status/Bulk Upload/Inhouse/TS - Ref No/Label - Product'), 0)

WebUI.delay(2)

CustomKeywords.'get.Screencapture.getEntirePage'('')

WebUI.delay(2)

WebUI.click(findTestObject('Transaction Status/Bulk Upload/Inhouse/TS - Ref No/BTN - Document Number'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

