import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import java.text.CollationElementIterator

import javax.media.bean.playerbean.MediaPlayer.visualComponentAdapter

import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint

totalpage=WebUI.getText(findTestObject('Bulk Upload/List Record/Label - Total Page'))
int totalpage123 = Integer.parseInt(totalpage)

TFromAccountDescription111 = new ArrayList()
TTransactionAmount111 = new ArrayList()
TTAmountandAccountNumber111 = new ArrayList()


WebDriver driver = DriverFactory.getWebDriver()
driver.switchTo().frame("login");
driver.switchTo().frame("detailRecordFrame");

	
	for (int i = 0; i<totalpage123; i++ ) {
		WebElement Table = driver.findElement(By.xpath('//table[@id="detailTable"]/tbody'))
		
		List<WebElement> Rows = Table.findElements(By.tagName('tr'))
			
		table: for(int j = 0; j<Rows.size(); j++){
			List<WebElement> Cols = Rows.get(j).findElements(By.tagName('td'))

				TFromAccountDescription=''+Cols.get(ColumnCreditAccountNumber).getText()
				TTransactionAmount=''+Cols.get(ColumnAmount).getText()
				
				TTAmountandAccountNumber111.add(TTransactionAmount+" "+TFromAccountDescription)
		
		}
		
		WebUI.switchToDefaultContent()
		while (WebUI.getAttribute(btnnextattribute, 'class') =='next btn grid-nextpage') {
			WebUI.click(btnnext)
	}
}

	driver.switchTo().frame("login");
	driver.switchTo().frame("detailRecordFrame");
	
	WebElement Table = driver.findElement(By.xpath('//table[@id="detailTable"]/tbody'))
	List<WebElement> Rows = Table.findElements(By.tagName('tr'))
	
	int TTRows = Integer.parseInt(GlobalVariable.TotalTransactionRecord123)
	
	Collections.sort(TTAmountandAccountNumber111)
	Collections.sort(GlobalVariable.TTAmountandAccountNumber111)

		for(TRows=0; TRows<TTRows; TRows++ ){

			WebUI.verifyMatch(TTAmountandAccountNumber111[TRows], GlobalVariable.TTAmountandAccountNumber111[TRows], false)
			
			}
		WebUI.switchToDefaultContent()
	
	
	