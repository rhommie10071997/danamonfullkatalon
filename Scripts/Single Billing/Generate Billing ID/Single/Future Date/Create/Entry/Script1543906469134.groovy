import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import java.awt.Desktop.Action as Action
import java.awt.event.ActionEvent as ActionEvent
import java.text.DecimalFormat as DecimalFormat
import java.text.NumberFormat as NumberFormat
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser(GlobalVariable.url, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForAngularLoad(0)

WebUI.waitForElementVisible(findTestObject('1 - Frame/1 - iframe login'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Log in/corpid'), 'pcmkillua', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Log in/userid'), 'killua', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Log in/password'), 'Password123', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Log in/login'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(3, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Menu/BTN - Menu Object'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Menu/TextField - Search'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Menu/TextField - Search'), ' ', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Menu/TextField - Search'), 'Single Billing', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Menu/Label - Single Billing (By Search)'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Menu/Label - Single Billing (By Search)'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForAngularLoad(0)

WebUI.waitForElementVisible(findTestObject('Single Billing/Generate Billing ID/New Entry/Label - Title'), 0)

Title = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/New Entry/Label - Title'))

WebUI.verifyMatch(Title, 'Single Billing', false)

SubTitle = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/New Entry/Label - New Entry'))

WebUI.verifyMatch(SubTitle, 'New Entry', false)

SubTitle2 = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/New Entry/Label - Sub Title - Generate Billing ID'))

WebUI.verifyMatch(SubTitle2, 'Generate Billing ID', false)

WebUI.click(findTestObject('Single Billing/Generate Billing ID/New Entry/DropList - Transfer From'))

WebUI.delay(2)

WebUI.waitForElementVisible(findTestObject('Single Billing/Generate Billing ID/New Entry/TextField - Drop List'), 0)

WebUI.setText(findTestObject('Single Billing/Generate Billing ID/New Entry/TextField - Drop List'), '000002814846')

WebUI.delay(1)

WebUI.waitForElementVisible(findTestObject('Single Billing/Generate Billing ID/New Entry/BTN - High Lighted'), 0)

WebUI.click(findTestObject('Single Billing/Generate Billing ID/New Entry/BTN - High Lighted'))

TransferFrom = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/New Entry/DropList - Transfer From'))

GlobalVariable.FromAccount123 = TransferFrom

Random random = new Random()

int RandAccDesc

RandAccDesc = random.nextInt(99)

RandAccDesc1 = ('FromAccDescArdo' + RandAccDesc)

GlobalVariable.FromAccountDescription123 = RandAccDesc1

WebUI.setText(findTestObject('Single Billing/Generate Billing ID/New Entry/TextField - From Account Description'), RandAccDesc1)

int FlagTaxInformation = random.nextInt(2) + 1

if (FlagTaxInformation == 1) {
    GlobalVariable.TaxInformation252 = 'Tax List'

    WebUI.click(findTestObject('Single Billing/Generate Billing ID/New Entry/DropList - Tax List'))

    WebUI.delay(2)

    WebUI.waitForElementVisible(findTestObject('Single Billing/Generate Billing ID/New Entry/TextField - Drop List'), 0)

    WebUI.setText(findTestObject('Single Billing/Generate Billing ID/New Entry/TextField - Drop List'), 'Tax List 001')

    WebUI.delay(1)

    WebUI.waitForElementVisible(findTestObject('Single Billing/Generate Billing ID/New Entry/BTN - High Lighted'), 0)

    WebUI.click(findTestObject('Single Billing/Generate Billing ID/New Entry/BTN - High Lighted'))

    TaxList = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/New Entry/DropList - Tax List'))

    GlobalVariable.TaxList252 = TaxList

    WebUI.delay(2)

    NPWP = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/New Entry/Label - NPWP'))

    GlobalVariable.NPWP252 = NPWP
} else {
    GlobalVariable.TaxInformation252 = 'New Entry'

    WebUI.click(findTestObject('Single Billing/Generate Billing ID/New Entry/BTN - New Entry'))

    WebUI.waitForElementVisible(findTestObject('Single Billing/Generate Billing ID/New Entry/Checkbox - Save to Tax List'), 
        0)

    int SavetoTaxList = random.nextInt(2) + 1

    if (SavetoTaxList == 2) {
        WebUI.click(findTestObject('Single Billing/Generate Billing ID/New Entry/Checkbox - Save to Tax List'))

        GlobalVariable.SavetoTaxList252 = 'Yes'
    } else {
        GlobalVariable.SavetoTaxList252 = 'No'
    }
    
    RandTaxAlias = ('TaxArd' + RandAccDesc)

    GlobalVariable.TaxAlias252 = RandTaxAlias

    WebUI.setText(findTestObject('Single Billing/Generate Billing ID/New Entry/TextField - Tax Alias'), RandTaxAlias)

    WebUI.click(findTestObject('Single Billing/Generate Billing ID/New Entry/BTN - NPWP'))

    GlobalVariable.NPWPType252 = 'NPWP'

    SetNpwp1 = '013084702'

    SetNpwp2 = '091'

    SetNpwp3 = '030'

    WebUI.setText(findTestObject('Single Billing/Generate Billing ID/New Entry/TextField - NPWP 1'), SetNpwp1)

    WebUI.setText(findTestObject('Single Billing/Generate Billing ID/New Entry/TextField - NPWP 2'), SetNpwp2)

    WebUI.setText(findTestObject('Single Billing/Generate Billing ID/New Entry/TextField - NPWP 3'), SetNpwp3)

    GlobalVariable.NPWP252 = ((SetNpwp1 + SetNpwp2) + SetNpwp3)
}

WebUI.click(findTestObject('Single Billing/Generate Billing ID/New Entry/DropList - Tax Akun Code'))

WebUI.delay(2)

WebUI.waitForElementVisible(findTestObject('Single Billing/Generate Billing ID/New Entry/TextField - Drop List'), 0)

WebUI.setText(findTestObject('Single Billing/Generate Billing ID/New Entry/TextField - Drop List'), '411121')

WebUI.delay(1)

WebUI.waitForElementVisible(findTestObject('Single Billing/Generate Billing ID/New Entry/BTN - High Lighted'), 0)

WebUI.click(findTestObject('Single Billing/Generate Billing ID/New Entry/BTN - High Lighted'))

TaxAkunCode = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/New Entry/DropList - Tax Akun Code'))

GlobalVariable.TaxAkunCode252 = TaxAkunCode

WebUI.click(findTestObject('Single Billing/Generate Billing ID/New Entry/DropList - Deposit Type'))

WebUI.delay(2)

WebUI.waitForElementVisible(findTestObject('Single Billing/Generate Billing ID/New Entry/TextField - Drop List'), 0)

WebUI.setText(findTestObject('Single Billing/Generate Billing ID/New Entry/TextField - Drop List'), '100')

WebUI.delay(2)

WebUI.waitForElementVisible(findTestObject('Single Billing/Generate Billing ID/New Entry/BTN - High Lighted'), 0)

WebUI.click(findTestObject('Single Billing/Generate Billing ID/New Entry/BTN - High Lighted'))

DepositType = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/New Entry/DropList - Deposit Type'))

GlobalVariable.DepositType252 = DepositType

TaxObjectNumber = '000'

GlobalVariable.TaxObjectNumber252 = TaxObjectNumber

FormatNoUrut = '000'

JenisSKP = '000'

TahunPajak = '00'

KodeKPP = '000'

TahunTerbit = '00'

WebUI.setText(findTestObject('Single Billing/Generate Billing ID/New Entry/TextField - Tax Object Number(NOP)'), TaxObjectNumber)

WebUI.setText(findTestObject('Single Billing/Generate Billing ID/New Entry/TextField - Format No Urut'), FormatNoUrut)

WebUI.setText(findTestObject('Single Billing/Generate Billing ID/New Entry/TextField - Jenis SKP'), JenisSKP)

WebUI.setText(findTestObject('Single Billing/Generate Billing ID/New Entry/TextField - Tahun Pajak'), TahunPajak)

WebUI.setText(findTestObject('Single Billing/Generate Billing ID/New Entry/TextField - Kode KPP'), KodeKPP)

WebUI.setText(findTestObject('Single Billing/Generate Billing ID/New Entry/TextField - Tahun Terbit'), TahunTerbit)

GlobalVariable.AssessmentNumber252 = ((((FormatNoUrut + JenisSKP) + TahunPajak) + KodeKPP) + TahunTerbit)

GlobalVariable.PaymentDescription252 = 'PaymentDescArdo'

WebUI.setText(findTestObject('Single Billing/Generate Billing ID/New Entry/TextField - Payment Description'), GlobalVariable.PaymentDescription252)

WebUI.click(findTestObject('Single Billing/Generate Billing ID/New Entry/DropList - Amount'))

WebUI.delay(2)

WebUI.waitForElementVisible(findTestObject('Single Billing/Generate Billing ID/New Entry/TextField - Drop List'), 0)

WebUI.setText(findTestObject('Single Billing/Generate Billing ID/New Entry/TextField - Drop List'), 'IDR')

WebUI.delay(1)

WebUI.waitForElementVisible(findTestObject('Single Billing/Generate Billing ID/New Entry/BTN - High Lighted'), 0)

WebUI.click(findTestObject('Single Billing/Generate Billing ID/New Entry/BTN - High Lighted'))

CurrencyAmount = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/New Entry/DropList - Amount'))

GlobalVariable.Currency = CurrencyAmount

int Randamount

Randamount = random.nextInt(999)

String AmountRandom = '250' + Randamount

WebUI.setText(findTestObject('Single Billing/Generate Billing ID/New Entry/TextField - Amount'), AmountRandom)

Locale local1 = new Locale('en', 'UK')

DecimalFormat amountformat = ((NumberFormat.getNumberInstance(local1)) as DecimalFormat)

amountformat.applyPattern('###,###,##0.00')

String amountformat1 = amountformat.format(new BigDecimal(AmountRandom))

GlobalVariable.Amount252 = (CurrencyAmount + amountformat1)

WebUI.waitForElementVisible(findTestObject('Single Billing/Generate Billing ID/New Entry/DropList - Tax Period Month From'), 
    0)

WebUI.click(findTestObject('Single Billing/Generate Billing ID/New Entry/DropList - Tax Period Month From'))

WebUI.delay(2)

WebUI.waitForElementVisible(findTestObject('Single Billing/Generate Billing ID/New Entry/TextField - Drop List'), 0)

WebUI.setText(findTestObject('Single Billing/Generate Billing ID/New Entry/TextField - Drop List'), 'JUNE')

WebUI.delay(1)

WebUI.waitForElementVisible(findTestObject('Single Billing/Generate Billing ID/New Entry/BTN - High Lighted'), 0)

WebUI.click(findTestObject('Single Billing/Generate Billing ID/New Entry/BTN - High Lighted'))

TaxPeriodMonth = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/New Entry/DropList - Tax Period Month From'))

GlobalVariable.TaxPeriodMonth252 = TaxPeriodMonth

WebUI.click(findTestObject('Single Billing/Generate Billing ID/New Entry/BTN - Specific Date'))

InstructionMode = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/New Entry/BTN - Specific Date'))

GlobalVariable.InstructionMode123 = InstructionMode

WebUI.delay(1)

WebUI.waitForElementVisible(findTestObject('Cash Pooling/New Entry/Drop List - Future Date'), 0)

WebUI.click(findTestObject('Cash Pooling/New Entry/Drop List - Future Date'))

WebUI.waitForElementVisible(findTestObject('Cash Pooling/New Entry/Drop List - Future Date - Date'), 0)

WebUI.click(findTestObject('Cash Pooling/New Entry/Drop List - Future Date - Date'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(1)

WebUI.click(findTestObject('Single Billing/Generate Billing ID/New Entry/DropList - Future - At'))

WebUI.waitForElementVisible(findTestObject('Cash Pooling/New Entry/Text Field - Set Text'), 0)

WebUI.setText(findTestObject('Cash Pooling/New Entry/Text Field - Set Text'), '07')

WebUI.waitForElementVisible(findTestObject('Cash Pooling/New Entry/Drop List - Highlight or 1'), 0)

WebUI.click(findTestObject('Cash Pooling/New Entry/Drop List - Highlight or 1'))

FutureAt = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/New Entry/DropList - Future - At'))

GlobalVariable.SpecificDateAt123 = FutureAt

WebUI.click(findTestObject('Cash Pooling/New Entry/Checkbox - Terms and Condition'))

WebUI.click(findTestObject('Cash Pooling/New Entry/BTN - Confirm'))

WebUI.delay(5)

