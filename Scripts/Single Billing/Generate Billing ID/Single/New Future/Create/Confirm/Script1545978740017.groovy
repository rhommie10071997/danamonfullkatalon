import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.waitForElementVisible(findTestObject('Single Billing/Generate Billing ID/Confirm/Label - Sub Label2'), 0, FailureHandling.CONTINUE_ON_FAILURE)

SubLabel2 = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/Confirm/Label - Sub Label2'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(SubLabel2, 'Confirmation', false, FailureHandling.CONTINUE_ON_FAILURE)

String FromAcc = GlobalVariable.FromAccount123

FromAcc = FromAcc.replace('(', ' (')

GlobalVariable.FromAccount123 = FromAcc

TransferFrom = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/Confirm/Label - Transfer From'), FailureHandling.CONTINUE_ON_FAILURE)

TransferFrom = TransferFrom.replace(':  ', '')

WebUI.verifyMatch(TransferFrom, GlobalVariable.FromAccount123, false, FailureHandling.CONTINUE_ON_FAILURE)

FromAccountDescription = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/Confirm/Label - From Account Description'), 
    FailureHandling.CONTINUE_ON_FAILURE)

FromAccountDescription = FromAccountDescription.replace(':  ', '')

WebUI.verifyMatch(FromAccountDescription, GlobalVariable.FromAccountDescription123, false, FailureHandling.CONTINUE_ON_FAILURE)

TaxInformation = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/Confirm/Label - Tax Information'), FailureHandling.CONTINUE_ON_FAILURE)

TaxInformation = TaxInformation.replace(':  ', '')

WebUI.verifyMatch(TaxInformation, GlobalVariable.TaxInformation252, false, FailureHandling.CONTINUE_ON_FAILURE)

if (GlobalVariable.TaxInformation252 == 'Tax List') {
    TaxList = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/Confirm/Label - Tax List'))

    TaxList = TaxList.replace(':  ', '')

    WebUI.verifyMatch(TaxList, GlobalVariable.TaxList252, false)
} else {
    SavetoTaxList = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/Confirm/Label - Save to Tax List'))

    SavetoTaxList = SavetoTaxList.replace(':  ', '')

    WebUI.verifyMatch(SavetoTaxList, GlobalVariable.SavetoTaxList252, false)

    TaxAliasName = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/Confirm/Label - Tax Alias Name'))

    TaxAliasName = TaxAliasName.replace(':  ', '')

    WebUI.verifyMatch(TaxAliasName, GlobalVariable.TaxAlias252, false)
}

NPWP = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/Confirm/Label - NPWP'), FailureHandling.CONTINUE_ON_FAILURE)

NPWP = NPWP.replace(':  ', '')

WebUI.verifyMatch(NPWP, GlobalVariable.NPWP252, false, FailureHandling.CONTINUE_ON_FAILURE)

TaxPayerName = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/Confirm/Label - Tax Payer Name'), FailureHandling.CONTINUE_ON_FAILURE)

TaxPayerName = TaxPayerName.replace(':  ', '')

GlobalVariable.TaxPayerName252 = TaxPayerName

TaxPayerAddress = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/Confirm/Label - Tax Payer Address'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TaxPayerAddress = TaxPayerAddress.replace(':  ', '')

GlobalVariable.TaxPayerAddress1252 = TaxPayerAddress

TaxPayerAddress2 = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/Confirm/Label - Tax Payer Address 2'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TaxPayerAddress2 = TaxPayerAddress2.replace(':  ', '')

GlobalVariable.TaxPayerAddress2252 = TaxPayerAddress2

TaxPayerAddress3 = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/Confirm/Label - Tax Payer Address 3'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TaxPayerAddress3 = TaxPayerAddress3.replace(':  ', '')

GlobalVariable.TaxPayerAddress3252 = TaxPayerAddress3

TaxPayerCity = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/Confirm/Label - Tax Payer City'), FailureHandling.CONTINUE_ON_FAILURE)

TaxPayerCity = TaxPayerCity.replace(':  ', '')

GlobalVariable.TaxPayerCity252 = TaxPayerCity

TaxAkunCode = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/Confirm/Label - Tax Akun Code'), FailureHandling.CONTINUE_ON_FAILURE)

TaxAkunCode = TaxAkunCode.replace(':  ', '')

String TaxAkunCode0 = GlobalVariable.TaxAkunCode252

String[] TaxAkunCode01 = TaxAkunCode0.split(' - ')

TaxAkunCode011 = (TaxAkunCode01[1])

WebUI.verifyMatch(TaxAkunCode, TaxAkunCode011, false, FailureHandling.CONTINUE_ON_FAILURE)

DepositType = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/Confirm/Label - Deposit Type'), FailureHandling.CONTINUE_ON_FAILURE)

DepositType = DepositType.replace(':  ', '')

WebUI.verifyMatch(DepositType, GlobalVariable.DepositType252, false, FailureHandling.CONTINUE_ON_FAILURE)

TaxObjectNumber = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/Confirm/Label - Tax Object Number'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TaxObjectNumber = TaxObjectNumber.replace(':  ', '')

WebUI.verifyMatch(TaxObjectNumber, GlobalVariable.TaxObjectNumber252, false, FailureHandling.CONTINUE_ON_FAILURE)

AssessmentNumber = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/Confirm/Label - Assessment Number'), 
    FailureHandling.CONTINUE_ON_FAILURE)

AssessmentNumber = AssessmentNumber.replace(':  ', '')

WebUI.verifyMatch(AssessmentNumber, GlobalVariable.AssessmentNumber252, false, FailureHandling.CONTINUE_ON_FAILURE)

PaymentDescription = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/Confirm/Label - Payment Description'), 
    FailureHandling.CONTINUE_ON_FAILURE)

PaymentDescription = PaymentDescription.replace(':  ', '')

WebUI.verifyMatch(PaymentDescription, GlobalVariable.PaymentDescription252, false, FailureHandling.CONTINUE_ON_FAILURE)

Amount = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/Confirm/Label - Amount'), FailureHandling.CONTINUE_ON_FAILURE)

Amount = Amount.replace(':  ', '')

WebUI.verifyMatch(Amount, GlobalVariable.Amount252, false, FailureHandling.CONTINUE_ON_FAILURE)

TotalCharge = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/Confirm/Label - Total Charge'), FailureHandling.CONTINUE_ON_FAILURE)

TotalCharge = TotalCharge.replace(':  ', '')

GlobalVariable.TotalCharge123 = TotalCharge

TotalDebitAmount = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/Confirm/Label - Total Debit Amount'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TotalDebitAmount = TotalDebitAmount.replace(':  ', '')

GlobalVariable.TotalDebetAmount123 = TotalDebitAmount

if (GlobalVariable.TaxPeriodType252 == 'Monthly') {
    TaxPeriodMonth = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/Confirm/Label - Tax Period Month'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    TaxPeriodMonth = TaxPeriodMonth.replace(':  ', '')

    WebUI.verifyMatch(TaxPeriodMonth, GlobalVariable.TaxPeriodMonth252, false, FailureHandling.CONTINUE_ON_FAILURE)

    GlobalVariable.TaxPeriodMonth252 = TaxPeriodMonth
} else if (GlobalVariable.TaxPeriodType252 == 'Annual') {
    TaxPeriodYear = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/Confirm/Label - Tax Period Year'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    TaxPeriodYear = TaxPeriodYear.replace(':  ', '')

    WebUI.verifyMatch(TaxPeriodYear, GlobalVariable.TaxPeriodYear252, false, FailureHandling.CONTINUE_ON_FAILURE)

    GlobalVariable.TaxPeriodYear252 = TaxPeriodYear
} else {
    TaxPeriodMonth = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/Confirm/Label - Tax Period Month'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    TaxPeriodMonth = TaxPeriodMonth.replace(':  ', '')

    WebUI.verifyMatch(TaxPeriodMonth, (GlobalVariable.TaxPeriodMonth252 + ' to ') + GlobalVariable.TaxPeriodMonthTo252, 
        false, FailureHandling.CONTINUE_ON_FAILURE)

    TaxPeriodYear = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/Confirm/Label - Tax Period Year'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    TaxPeriodYear = TaxPeriodYear.replace(':  ', '')

    WebUI.verifyMatch(TaxPeriodYear, GlobalVariable.TaxPeriodYear252, false, FailureHandling.CONTINUE_ON_FAILURE)

    GlobalVariable.TaxPeriodYear252 = TaxPeriodYear
}

InstructionMode = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/Confirm/Label - Instruction Mode'), FailureHandling.CONTINUE_ON_FAILURE)

InstructionMode = InstructionMode.replace(': ', '')

WebUI.verifyMatch(InstructionMode, GlobalVariable.InstructionMode123, false, FailureHandling.CONTINUE_ON_FAILURE)

SpecificDateOn = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/Confirm/Label - SpecificDateOn'))

SpecificDateOn = SpecificDateOn.replace(':  ', '')

GlobalVariable.FutureDate123 = SpecificDateOn

ExpiredAt = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/Confirm/Label - Expired On'), FailureHandling.CONTINUE_ON_FAILURE)

ExpiredAt = ExpiredAt.replace(':  ', '')

GlobalVariable.ExpiredOn123 = ExpiredAt

String abc = '123456'

WebUI.waitForElementVisible(findTestObject('Pending Task/Balance Transfer/Inhouse/Detail/TextField - Response Code'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Pending Task/Balance Transfer/Inhouse/Detail/TextField - Response Code'), abc, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Single Billing/Generate Billing ID/Confirm/BTN - Submit'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Single Billing/Generate Billing ID/Confirm/BTN - Pop Up OK'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Single Billing/Generate Billing ID/Confirm/BTN - Pop Up OK'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

if (GlobalVariable.SimilliarTransaction252 == 1) {
    WebUI.delay(2)

    STNpwp = WebUI.getText(findTestObject('Single Billing/Similliar Transaction/Label - Similliar Transaction - Npwp'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.verifyMatch(STNpwp, GlobalVariable.NPWP252, false, FailureHandling.CONTINUE_ON_FAILURE)

    STAkunCode = WebUI.getText(findTestObject('Single Billing/Similliar Transaction/Label - Similliar Transaction - Akun Code'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.verifyMatch(STAkunCode, TaxAkunCode01[0], false, FailureHandling.CONTINUE_ON_FAILURE)

    STTaxPeriod = WebUI.getText(findTestObject('Single Billing/Similliar Transaction/Label - Similliar Transaction - Tax Period'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.verifyMatch(STTaxPeriod, GlobalVariable.TaxPeriodType252, false, FailureHandling.CONTINUE_ON_FAILURE)

    STAmount = WebUI.getText(findTestObject('Single Billing/Similliar Transaction/Label - Similliar Transaction - Amount'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.verifyMatch(STAmount, GlobalVariable.STAmount252, false, FailureHandling.CONTINUE_ON_FAILURE)

    WebUI.click(findTestObject('Single Billing/Generate Billing ID/Confirm/BTN - Similliar Transaction - Continue'))
}

GlobalVariable.TaxAkunCode252 = TaxAkunCode011

