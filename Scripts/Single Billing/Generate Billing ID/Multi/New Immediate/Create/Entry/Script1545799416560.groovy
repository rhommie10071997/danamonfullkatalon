import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import java.awt.Desktop.Action as Action
import java.awt.event.ActionEvent as ActionEvent
import java.text.DecimalFormat as DecimalFormat
import java.text.NumberFormat as NumberFormat
import org.junit.After as After
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

Random random = new Random()

//Transfer From nya account apa
'----------------------------------------------------------------------------------------------------------------------------'
GlobalVariable.FromAccount123 = 'abd karim'

//From Account Descriptnya , nanti di belakang text akan di tambah angka random
GlobalVariable.FromAccDescArdo252 = 'FromAccDescArdoLetsgow'

//Untuk Tax Informationya mau Tax List, atau New Entry. Tax List = 0 dan untuk New Entry = 1
//int FlagTaxInformation = 1

//Untuk Flag Tax Information nya di random ato tidak, kalau tidak mau random di tambah //  , di depan int flag . . .. .
int FlagTaxInformation = random.nextInt(2) + 1

//Untuk Tax List
GlobalVariable.TaxList252 = 'Tax List 001'

//Flag Save to Tax List, 1 = tidak di save dan 2 = di save
//int SavetoTaxList = 1

//Untuk Flag Tax List mau di random atau tidak

int SavetoTaxList = random.nextInt(2) + 1

//Untuk Alias Name
RandTaxAli = 'TaxArdo'

//Npwp yang perlu di isi
SetNpwp1 = '013084702'

SetNpwp2 = '091'

SetNpwp3 = '030'

//Tax Akun Code nya
GlobalVariable.TaxAkunCode252 = '1148'

//Deposit Type nya
GlobalVariable.DepositType252 = '010'

// Tax Object Number (NOP)
TaxObjectNumber = '00000'

// Assessment Number
FormatNoUrut = '000'

JenisSKP = '000'

TahunPajak = '00'

KodeKPP = '000'

TahunTerbit = '00'

// Payment Description
GlobalVariable.PaymentDescription252 = 'PaymentDescArdo'

// Amount angka depan ,  Randamount
AmountAngkaDepan = '25'

int Randamount

Randamount = random.nextInt(999)

'---------'
//String AmountRandom = AmountAngkaDepan + Randamount

// Kalau mau Amount yang specific
String AmountRandom = '25252'

// Tax Period Type ada 3 yaitu , Monthly , Annual , dan Periodic  !!!!!!!!!! tolong di check dulu -Deposit Type- di backoffice
'---------'
GlobalVariable.TaxPeriodType252 = 'Periodic'

//Tax Period Type Monthly, Tax Period Month nya bulan apa
GlobalVariable.TaxPeriodMonth252 = 'June'

//
GlobalVariable.TaxPeriodMonthTo252 = 'August'

AnnualTaxPeriodYear = '2018'

//Similliar Transaction , kalau 1 berarti muncul
GlobalVariable.SimilliarTransaction252 = 1
'----------------------------------------------------------------------------------------------------------------------------'
WebUI.openBrowser(GlobalVariable.url, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForAngularLoad(0)

WebUI.waitForElementVisible(findTestObject('1 - Frame/1 - iframe login'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Log in/corpid'), 'pcm000100', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Log in/userid'), 'hermione', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Log in/password'), 'Password123', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Log in/login'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(3, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Menu/BTN - Menu Object'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Menu/TextField - Search'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Menu/TextField - Search'), ' ', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Menu/TextField - Search'), 'Single Billing', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Menu/Label - Single Billing (By Search)'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Menu/Label - Single Billing (By Search)'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForAngularLoad(0)

WebUI.waitForElementVisible(findTestObject('Single Billing/Generate Billing ID/New Entry/Label - Title'), 0)

Title = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/New Entry/Label - Title'))

WebUI.verifyMatch(Title, 'Single Billing', false)

SubTitle = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/New Entry/Label - New Entry'))

WebUI.verifyMatch(SubTitle, 'New Entry', false)

SubTitle2 = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/New Entry/Label - Sub Title - Generate Billing ID'))

WebUI.verifyMatch(SubTitle2, 'Generate Billing ID', false)

WebUI.click(findTestObject('Single Billing/Generate Billing ID/New Entry/DropList - Transfer From'))

WebUI.delay(2)

WebUI.waitForElementVisible(findTestObject('Single Billing/Generate Billing ID/New Entry/TextField - Drop List'), 0)

WebUI.setText(findTestObject('Single Billing/Generate Billing ID/New Entry/TextField - Drop List'), GlobalVariable.FromAccount123)

WebUI.delay(1)

WebUI.waitForElementVisible(findTestObject('Single Billing/Generate Billing ID/New Entry/BTN - High Lighted'), 0)

WebUI.click(findTestObject('Single Billing/Generate Billing ID/New Entry/BTN - High Lighted'))

TransferFrom = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/New Entry/DropList - Transfer From'))

GlobalVariable.FromAccount123 = TransferFrom

int RandAccDesc

RandAccDesc = random.nextInt(99)

RandAccDesc1 = (GlobalVariable.FromAccDescArdo252 + RandAccDesc)

GlobalVariable.FromAccountDescription123 = RandAccDesc1

WebUI.setText(findTestObject('Single Billing/Generate Billing ID/New Entry/TextField - From Account Description'), RandAccDesc1)

if (FlagTaxInformation == 1) {
    GlobalVariable.TaxInformation252 = 'Tax List'

    WebUI.click(findTestObject('Single Billing/Generate Billing ID/New Entry/DropList - Tax List'))

    WebUI.delay(2)

    WebUI.waitForElementVisible(findTestObject('Single Billing/Generate Billing ID/New Entry/TextField - Drop List'), 0)

    WebUI.setText(findTestObject('Single Billing/Generate Billing ID/New Entry/TextField - Drop List'), GlobalVariable.TaxList252)

    WebUI.delay(1)

    WebUI.waitForElementVisible(findTestObject('Single Billing/Generate Billing ID/New Entry/BTN - High Lighted'), 0)

    WebUI.click(findTestObject('Single Billing/Generate Billing ID/New Entry/BTN - High Lighted'))

    TaxList = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/New Entry/DropList - Tax List'))

    GlobalVariable.TaxList252 = TaxList

    WebUI.delay(2)

    NPWP = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/New Entry/Label - NPWP'))

    GlobalVariable.NPWP252 = NPWP
} else {
    GlobalVariable.TaxInformation252 = 'New Entry'

    WebUI.click(findTestObject('Single Billing/Generate Billing ID/New Entry/BTN - New Entry'))

    WebUI.waitForElementVisible(findTestObject('Single Billing/Generate Billing ID/New Entry/Checkbox - Save to Tax List'), 
        0)

    if (SavetoTaxList == 2) {
        WebUI.click(findTestObject('Single Billing/Generate Billing ID/New Entry/Checkbox - Save to Tax List'))

        GlobalVariable.SavetoTaxList252 = 'Yes'
    } else {
        GlobalVariable.SavetoTaxList252 = 'No'
    }
    
    RandTaxAlias = (RandTaxAli + RandAccDesc)

    GlobalVariable.TaxAlias252 = RandTaxAlias

    WebUI.setText(findTestObject('Single Billing/Generate Billing ID/New Entry/TextField - Tax Alias'), RandTaxAlias)

    WebUI.click(findTestObject('Single Billing/Generate Billing ID/New Entry/BTN - NPWP'))

    GlobalVariable.NPWPType252 = 'NPWP'

    WebUI.setText(findTestObject('Single Billing/Generate Billing ID/New Entry/TextField - NPWP 1'), SetNpwp1)

    WebUI.setText(findTestObject('Single Billing/Generate Billing ID/New Entry/TextField - NPWP 2'), SetNpwp2)

    WebUI.setText(findTestObject('Single Billing/Generate Billing ID/New Entry/TextField - NPWP 3'), SetNpwp3)

    GlobalVariable.NPWP252 = ((SetNpwp1 + SetNpwp2) + SetNpwp3)
}

WebUI.click(findTestObject('Single Billing/Generate Billing ID/New Entry/DropList - Tax Akun Code'))

WebUI.delay(2)

WebUI.waitForElementVisible(findTestObject('Single Billing/Generate Billing ID/New Entry/TextField - Drop List'), 0)

WebUI.setText(findTestObject('Single Billing/Generate Billing ID/New Entry/TextField - Drop List'), GlobalVariable.TaxAkunCode252)

WebUI.delay(1)

WebUI.waitForElementVisible(findTestObject('Single Billing/Generate Billing ID/New Entry/BTN - High Lighted'), 0)

WebUI.click(findTestObject('Single Billing/Generate Billing ID/New Entry/BTN - High Lighted'))

TaxAkunCode = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/New Entry/DropList - Tax Akun Code'))

GlobalVariable.TaxAkunCode252 = TaxAkunCode

WebUI.click(findTestObject('Single Billing/Generate Billing ID/New Entry/DropList - Deposit Type'))

WebUI.delay(2)

WebUI.waitForElementVisible(findTestObject('Single Billing/Generate Billing ID/New Entry/TextField - Drop List'), 0)

WebUI.setText(findTestObject('Single Billing/Generate Billing ID/New Entry/TextField - Drop List'), GlobalVariable.DepositType252)

WebUI.delay(2)

WebUI.waitForElementVisible(findTestObject('Single Billing/Generate Billing ID/New Entry/BTN - High Lighted'), 0)

WebUI.click(findTestObject('Single Billing/Generate Billing ID/New Entry/BTN - High Lighted'))

DepositType = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/New Entry/DropList - Deposit Type'))

GlobalVariable.DepositType252 = DepositType

GlobalVariable.TaxObjectNumber252 = TaxObjectNumber

WebUI.setText(findTestObject('Single Billing/Generate Billing ID/New Entry/TextField - Tax Object Number(NOP)'), TaxObjectNumber)

WebUI.setText(findTestObject('Single Billing/Generate Billing ID/New Entry/TextField - Format No Urut'), FormatNoUrut)

WebUI.setText(findTestObject('Single Billing/Generate Billing ID/New Entry/TextField - Jenis SKP'), JenisSKP)

WebUI.setText(findTestObject('Single Billing/Generate Billing ID/New Entry/TextField - Tahun Pajak'), TahunPajak)

WebUI.setText(findTestObject('Single Billing/Generate Billing ID/New Entry/TextField - Kode KPP'), KodeKPP)

WebUI.setText(findTestObject('Single Billing/Generate Billing ID/New Entry/TextField - Tahun Terbit'), TahunTerbit)

GlobalVariable.AssessmentNumber252 = ((((FormatNoUrut + JenisSKP) + TahunPajak) + KodeKPP) + TahunTerbit)

WebUI.setText(findTestObject('Single Billing/Generate Billing ID/New Entry/TextField - Payment Description'), GlobalVariable.PaymentDescription252)

WebUI.click(findTestObject('Single Billing/Generate Billing ID/New Entry/DropList - Amount'))

WebUI.delay(2)

WebUI.waitForElementVisible(findTestObject('Single Billing/Generate Billing ID/New Entry/TextField - Drop List'), 0)

WebUI.setText(findTestObject('Single Billing/Generate Billing ID/New Entry/TextField - Drop List'), 'IDR')

WebUI.delay(1)

WebUI.waitForElementVisible(findTestObject('Single Billing/Generate Billing ID/New Entry/BTN - High Lighted'), 0)

WebUI.click(findTestObject('Single Billing/Generate Billing ID/New Entry/BTN - High Lighted'))

CurrencyAmount = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/New Entry/DropList - Amount'))

GlobalVariable.Currency = CurrencyAmount

GlobalVariable.STAmount252 = AmountRandom

WebUI.setText(findTestObject('Single Billing/Generate Billing ID/New Entry/TextField - Amount'), AmountRandom)

Locale local1 = new Locale('en', 'UK')

DecimalFormat amountformat = ((NumberFormat.getNumberInstance(local1)) as DecimalFormat)

amountformat.applyPattern('###,###,##0.00')

String amountformat1 = amountformat.format(new BigDecimal(AmountRandom))

GlobalVariable.Amount252 = (CurrencyAmount +" "+ amountformat1)

if (GlobalVariable.TaxPeriodType252 == 'Monthly') {
    WebUI.click(findTestObject('Single Billing/Generate Billing ID/New Entry/DropList - Tax Period Month From'))

    WebUI.delay(2)

    WebUI.waitForElementVisible(findTestObject('Single Billing/Generate Billing ID/New Entry/TextField - Drop List'), 0)

    WebUI.setText(findTestObject('Single Billing/Generate Billing ID/New Entry/TextField - Drop List'), GlobalVariable.TaxPeriodMonth252)

    WebUI.delay(1)

    WebUI.waitForElementVisible(findTestObject('Single Billing/Generate Billing ID/New Entry/BTN - High Lighted'), 0)

    WebUI.click(findTestObject('Single Billing/Generate Billing ID/New Entry/BTN - High Lighted'))

    TaxPeriodMonth = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/New Entry/DropList - Tax Period Month From'))

    GlobalVariable.TaxPeriodMonth252 = TaxPeriodMonth
} else if (GlobalVariable.TaxPeriodType252 == 'Annual') {
	WebUI.delay(2)
	WebUI.waitForElementVisible(findTestObject('Single Billing/Generate Billing ID/New Entry/TextField - Annual - Tax Period Year'), 0)
	
    WebUI.setText(findTestObject('Single Billing/Generate Billing ID/New Entry/TextField - Annual - Tax Period Year'), AnnualTaxPeriodYear)
	GlobalVariable.TaxPeriodYear252 = AnnualTaxPeriodYear
} else {
	WebUI.delay(2)
	WebUI.waitForElementVisible(findTestObject('Single Billing/Generate Billing ID/New Entry/Droplist - Tax Period Month To'), 0)
	WebUI.waitForElementVisible(findTestObject('Single Billing/Generate Billing ID/New Entry/DropList - Tax Period Month From'), 0)
	WebUI.click(findTestObject('Single Billing/Generate Billing ID/New Entry/DropList - Tax Period Month From'))

	WebUI.delay(2)

	WebUI.waitForElementVisible(findTestObject('Single Billing/Generate Billing ID/New Entry/TextField - Drop List'), 0)

	WebUI.setText(findTestObject('Single Billing/Generate Billing ID/New Entry/TextField - Drop List'), GlobalVariable.TaxPeriodMonth252)

	WebUI.delay(1)

	WebUI.waitForElementVisible(findTestObject('Single Billing/Generate Billing ID/New Entry/BTN - High Lighted'), 0)

	WebUI.click(findTestObject('Single Billing/Generate Billing ID/New Entry/BTN - High Lighted'))

	TaxPeriodMonth = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/New Entry/DropList - Tax Period Month From'))

	GlobalVariable.TaxPeriodMonth252 = TaxPeriodMonth

	WebUI.click(findTestObject('Single Billing/Generate Billing ID/New Entry/DropList - Tax Period Month To'))
	
	WebUI.delay(2)
	
		WebUI.waitForElementVisible(findTestObject('Single Billing/Generate Billing ID/New Entry/TextField - Drop List'), 0)
	
		WebUI.setText(findTestObject('Single Billing/Generate Billing ID/New Entry/TextField - Drop List'), GlobalVariable.TaxPeriodMonthTo252)
	
		WebUI.delay(1)
	
		WebUI.waitForElementVisible(findTestObject('Single Billing/Generate Billing ID/New Entry/BTN - High Lighted'), 0)
	
		WebUI.click(findTestObject('Single Billing/Generate Billing ID/New Entry/BTN - High Lighted'))
	
		TaxPeriodMonthTo = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/New Entry/DropList - Tax Period Month To'))
		
			GlobalVariable.TaxPeriodMonthTo252 = TaxPeriodMonthTo
			
			WebUI.delay(2)
			WebUI.waitForElementVisible(findTestObject('Single Billing/Generate Billing ID/New Entry/TextField - Annual - Tax Period Year'), 0)
			
			WebUI.setText(findTestObject('Single Billing/Generate Billing ID/New Entry/TextField - Annual - Tax Period Year'), AnnualTaxPeriodYear)
			GlobalVariable.TaxPeriodYear252 = AnnualTaxPeriodYear
		
}

WebUI.click(findTestObject('Single Billing/Generate Billing ID/New Entry/BTN - Immediate'))

WebUI.click(findTestObject('Cash Pooling/New Entry/Checkbox - Terms and Condition'))

WebUI.click(findTestObject('Cash Pooling/New Entry/BTN - Confirm'))

WebUI.delay(5)

