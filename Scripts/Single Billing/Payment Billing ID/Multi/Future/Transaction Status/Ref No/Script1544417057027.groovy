import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

Subtitle = WebUI.getText(findTestObject('Transaction Status/Single Billing/Ref No/Label - Sub Title'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(Subtitle, 'Reference Number Detail', false, FailureHandling.CONTINUE_ON_FAILURE)

MenuName = WebUI.getText(findTestObject('Transaction Status/Single Billing/Ref No/Label - Menu Name'), FailureHandling.CONTINUE_ON_FAILURE)

MenuName = MenuName.replace(':  ', '')

WebUI.verifyMatch(MenuName, GlobalVariable.menu123, false, FailureHandling.CONTINUE_ON_FAILURE)

ProductandServiceName = WebUI.getText(findTestObject('Transaction Status/Single Billing/Ref No/Label - Product and Service Name'), 
    FailureHandling.CONTINUE_ON_FAILURE)

ProductandServiceName = ProductandServiceName.replace(':  ', '')

WebUI.verifyMatch(ProductandServiceName, GlobalVariable.product123, false, FailureHandling.CONTINUE_ON_FAILURE)

SystemReferenceNumber = WebUI.getText(findTestObject('Transaction Status/Single Billing/Ref No/Label - System Reference Number'), 
    FailureHandling.CONTINUE_ON_FAILURE)

SystemReferenceNumber = SystemReferenceNumber.replace(':  ', '')

WebUI.verifyMatch(SystemReferenceNumber, GlobalVariable.RefNo, false, FailureHandling.CONTINUE_ON_FAILURE)

TransferFrom = WebUI.getText(findTestObject('Transaction Status/Single Billing/Ref No/Label - Transfer From'), FailureHandling.CONTINUE_ON_FAILURE)

TransferFrom = TransferFrom.replace(':  ', '')

WebUI.verifyMatch(TransferFrom, GlobalVariable.FromAccount123, false, FailureHandling.CONTINUE_ON_FAILURE)

FromAccountDescription = WebUI.getText(findTestObject('Transaction Status/Single Billing/Ref No/Label - From Account Description'), 
    FailureHandling.CONTINUE_ON_FAILURE)

FromAccountDescription = FromAccountDescription.replace(':  ', '')

WebUI.verifyMatch(FromAccountDescription, GlobalVariable.FromAccountDescription123, false, FailureHandling.CONTINUE_ON_FAILURE)

TypePayment = WebUI.getText(findTestObject('Transaction Status/Single Billing/Ref No/Label - Type Payment'), FailureHandling.CONTINUE_ON_FAILURE)

TypePayment = TypePayment.replace(':  ', '')

WebUI.verifyMatch(TypePayment, GlobalVariable.TypePayment252, false, FailureHandling.CONTINUE_ON_FAILURE)

BillingID = WebUI.getText(findTestObject('Transaction Status/Single Billing/Ref No/Label - Billing ID'), FailureHandling.CONTINUE_ON_FAILURE)

BillingID = BillingID.replace(':  ', '')

WebUI.verifyMatch(BillingID, GlobalVariable.BillingID252, false, FailureHandling.CONTINUE_ON_FAILURE)

TaxPayerName = WebUI.getText(findTestObject('Transaction Status/Single Billing/Ref No/Label - Tax Payer Name'), FailureHandling.CONTINUE_ON_FAILURE)

TaxPayerName = TaxPayerName.replace(':  ', '')

WebUI.verifyMatch(TaxPayerName, GlobalVariable.TaxPayerName252, false, FailureHandling.CONTINUE_ON_FAILURE)

NPWP = WebUI.getText(findTestObject('Transaction Status/Single Billing/Ref No/Label - NPWP'), FailureHandling.CONTINUE_ON_FAILURE)

NPWP = NPWP.replace(':  ', '')

WebUI.verifyMatch(NPWP, GlobalVariable.NPWP252, false, FailureHandling.CONTINUE_ON_FAILURE)

TaxPayerAddress = WebUI.getText(findTestObject('Transaction Status/Single Billing/Ref No/Label - Tax Payer Address'), FailureHandling.CONTINUE_ON_FAILURE)

TaxPayerAddress = TaxPayerAddress.replace(':  ', '')

WebUI.verifyMatch(TaxPayerAddress, GlobalVariable.TaxPayerAddress1252, false, FailureHandling.CONTINUE_ON_FAILURE)

TaxPayerCity = WebUI.getText(findTestObject('Transaction Status/Single Billing/Ref No/Label - Tax Payer City'), FailureHandling.CONTINUE_ON_FAILURE)

TaxPayerCity = TaxPayerCity.replace(':  ', '')

WebUI.verifyMatch(TaxPayerCity, GlobalVariable.TaxPayerCity252, false, FailureHandling.CONTINUE_ON_FAILURE)

TaxAkunCode = WebUI.getText(findTestObject('Transaction Status/Single Billing/Ref No/Label - Tax Akun Code'), FailureHandling.CONTINUE_ON_FAILURE)

TaxAkunCode = TaxAkunCode.replace(':  ', '')

WebUI.verifyMatch(TaxAkunCode, GlobalVariable.TaxAkunCode252, false, FailureHandling.CONTINUE_ON_FAILURE)

DepositType = WebUI.getText(findTestObject('Transaction Status/Single Billing/Ref No/Label - Deposit Type'), FailureHandling.CONTINUE_ON_FAILURE)

DepositType = DepositType.replace(':  ', '')

WebUI.verifyMatch(DepositType, GlobalVariable.DepositType252, false, FailureHandling.CONTINUE_ON_FAILURE)

TaxObjectNumber = WebUI.getText(findTestObject('Transaction Status/Single Billing/Ref No/Label - Tax Object Number'), FailureHandling.CONTINUE_ON_FAILURE)

TaxObjectNumber = TaxObjectNumber.replace(':  ', '')

WebUI.verifyMatch(TaxObjectNumber, GlobalVariable.TaxObjectNumber252, false, FailureHandling.CONTINUE_ON_FAILURE)

AssessmentNumber = WebUI.getText(findTestObject('Transaction Status/Single Billing/Ref No/Label - Assessment Number'), FailureHandling.CONTINUE_ON_FAILURE)

AssessmentNumber = AssessmentNumber.replace(':  ', '')

WebUI.verifyMatch(AssessmentNumber, GlobalVariable.AssessmentNumber252, false, FailureHandling.CONTINUE_ON_FAILURE)

TaxPeriodMonth = WebUI.getText(findTestObject('Transaction Status/Single Billing/Ref No/Label - Tax Period Month'), FailureHandling.CONTINUE_ON_FAILURE)

TaxPeriodMonth = TaxPeriodMonth.replace(':  ', '')

WebUI.verifyMatch(TaxPeriodMonth, GlobalVariable.TaxPeriodMonth252, false, FailureHandling.CONTINUE_ON_FAILURE)

Amount = WebUI.getText(findTestObject('Transaction Status/Single Billing/Ref No/Label - Amount'), FailureHandling.CONTINUE_ON_FAILURE)

Amount = Amount.replace(':  ', '')

WebUI.verifyMatch(Amount, GlobalVariable.Amount252, false, FailureHandling.CONTINUE_ON_FAILURE)

TotalCharge = WebUI.getText(findTestObject('Transaction Status/Single Billing/Ref No/Label - Total Charge'), FailureHandling.CONTINUE_ON_FAILURE)

TotalCharge = TotalCharge.replace(':  ', '')

WebUI.verifyMatch(TotalCharge, GlobalVariable.TotalCharge123, false, FailureHandling.CONTINUE_ON_FAILURE)

TotalDebitAmount = WebUI.getText(findTestObject('Transaction Status/Single Billing/Ref No/Label - Total Debit Amount'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TotalDebitAmount = TotalDebitAmount.replace(':  ', '')

WebUI.verifyMatch(TotalDebitAmount, GlobalVariable.TotalDebetAmount123, false, FailureHandling.CONTINUE_ON_FAILURE)

InstructionMode = WebUI.getText(findTestObject('Transaction Status/Single Billing/Ref No/Label - Instruction Mode'), FailureHandling.CONTINUE_ON_FAILURE)

InstructionMode = InstructionMode.replace(':  ', '')

WebUI.verifyMatch(InstructionMode, GlobalVariable.InstructionMode123, false, FailureHandling.CONTINUE_ON_FAILURE)

SpecificDateOn = WebUI.getText(findTestObject('Transaction Status/Single Billing/Ref No/Label - SpecificDateOn'))

SpecificDateOn = SpecificDateOn.replace(':  ', '')

WebUI.verifyMatch(SpecificDateOn, GlobalVariable.FutureDate123, false, FailureHandling.CONTINUE_ON_FAILURE)

ExpiredAt = WebUI.getText(findTestObject('Transaction Status/Single Billing/Ref No/Label - Expired On'), FailureHandling.CONTINUE_ON_FAILURE)

ExpiredAt = ExpiredAt.replace(':  ', '')

WebUI.verifyMatch(ExpiredAt, GlobalVariable.ExpiredOn123, false, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Transaction Status/Single Billing/Ref No/BTN - Click Doc No'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Transaction Status/Single Billing/Ref No/BTN - Click Doc No'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

