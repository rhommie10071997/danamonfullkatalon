import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(5)

WebUI.waitForElementVisible(findTestObject('Single Billing/Generate Billing ID/Confirm/BTN - Submit'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Single Billing/Generate Billing ID/Confirm/Label - Sub Label2'), 0, FailureHandling.CONTINUE_ON_FAILURE)

SubLabel2 = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/Confirm/Label - Sub Label2'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.verifyMatch(SubLabel2, 'Confirmation', false, FailureHandling.CONTINUE_ON_FAILURE)

String FromAcc = GlobalVariable.FromAccount123

FromAcc = FromAcc.replace(' - ', '-')

GlobalVariable.FromAccount123 = FromAcc

TransferFrom = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/Confirm/Label - Transfer From'), FailureHandling.CONTINUE_ON_FAILURE)

TransferFrom = TransferFrom.replace(':  ', '')

WebUI.verifyMatch(TransferFrom, GlobalVariable.FromAccount123, false, FailureHandling.CONTINUE_ON_FAILURE)

FromAccountDescription = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/Confirm/Label - From Account Description'), 
    FailureHandling.CONTINUE_ON_FAILURE)

FromAccountDescription = FromAccountDescription.replace(':  ', '')

WebUI.verifyMatch(FromAccountDescription, GlobalVariable.FromAccountDescription123, false, FailureHandling.CONTINUE_ON_FAILURE)

PaymentDescription = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/Confirm/Label - Payment Description'), 
    FailureHandling.CONTINUE_ON_FAILURE)

PaymentDescription = PaymentDescription.replace(':  ', '')

WebUI.verifyMatch(PaymentDescription, GlobalVariable.PaymentDescription252, false, FailureHandling.CONTINUE_ON_FAILURE)

TypePayment = WebUI.getText(findTestObject('Single Billing/Inquiry Billing ID/Confirmation/Label - Type Payment'), FailureHandling.CONTINUE_ON_FAILURE)

TypePayment = TypePayment.replace(':  ', '')

if (GlobalVariable.FlagTypeBillingID252 == 0) {
    WebUI.verifyMatch(TypePayment, 'DJP', false, FailureHandling.CONTINUE_ON_FAILURE)

    GlobalVariable.TypePayment252 = TypePayment

    BillingID = WebUI.getText(findTestObject('Single Billing/Inquiry Billing ID/Confirmation/Label - Billing ID'), FailureHandling.CONTINUE_ON_FAILURE)

    BillingID = BillingID.replace(':  ', '')

    WebUI.verifyMatch(BillingID, GlobalVariable.BillingID252, false, FailureHandling.CONTINUE_ON_FAILURE)

    TaxPayerName = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/Confirm/Label - Tax Payer Name'), FailureHandling.CONTINUE_ON_FAILURE)

    TaxPayerName = TaxPayerName.replace(':  ', '')

    GlobalVariable.TaxPayerName252 = TaxPayerName

    NPWP = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/Confirm/Label - NPWP'), FailureHandling.CONTINUE_ON_FAILURE)

    NPWP = NPWP.replace(':  ', '')

    GlobalVariable.NPWP252 = NPWP

    TaxPayerAddress = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/Confirm/Label - Tax Payer Address'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    TaxPayerAddress = TaxPayerAddress.replace(':  ', '')

    GlobalVariable.TaxPayerAddress1252 = TaxPayerAddress

    TaxPayerCity = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/Confirm/Label - Tax Payer City'), FailureHandling.CONTINUE_ON_FAILURE)

    TaxPayerCity = TaxPayerCity.replace(':  ', '')

    GlobalVariable.TaxPayerCity252 = TaxPayerCity

    TaxAkunCode = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/Confirm/Label - Tax Akun Code'), FailureHandling.CONTINUE_ON_FAILURE)

    TaxAkunCode = TaxAkunCode.replace(':  ', '')

    GlobalVariable.TaxAkunCode252 = TaxAkunCode

    DepositType = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/Confirm/Label - Deposit Type'), FailureHandling.CONTINUE_ON_FAILURE)

    DepositType = DepositType.replace(':  ', '')

    GlobalVariable.DepositType252 = DepositType

    TaxObjectNumber = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/Confirm/Label - Tax Object Number'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    TaxObjectNumber = TaxObjectNumber.replace(':  ', '')

    GlobalVariable.TaxObjectNumber252 = TaxObjectNumber

    AssessmentNumber = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/Confirm/Label - Assessment Number'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    AssessmentNumber = AssessmentNumber.replace(':  ', '')

    GlobalVariable.AssessmentNumber252 = AssessmentNumber

    TaxPeriodMonth = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/Confirm/Label - Tax Period Month'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    TaxPeriodMonth = TaxPeriodMonth.replace(':  ', '')

    GlobalVariable.TaxPeriodMonth252 = TaxPeriodMonth

    GlobalVariable.TaxPeriodType252 = TaxPeriodMonth
} else if (GlobalVariable.FlagTypeBillingID252 == 1) {
    WebUI.verifyMatch(TypePayment, 'DJBC', false, FailureHandling.CONTINUE_ON_FAILURE)

    GlobalVariable.TypePayment252 = TypePayment

    BillingID = WebUI.getText(findTestObject('Single Billing/Inquiry Billing ID/Confirmation/Label - Billing ID'), FailureHandling.CONTINUE_ON_FAILURE)

    BillingID = BillingID.replace(':  ', '')

    WebUI.verifyMatch(BillingID, GlobalVariable.BillingID252, false, FailureHandling.CONTINUE_ON_FAILURE)

    TaxPayerName = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/Confirm/Label - Tax Payer Name'), FailureHandling.CONTINUE_ON_FAILURE)

    TaxPayerName = TaxPayerName.replace(':  ', '')

    GlobalVariable.TaxPayerName252 = TaxPayerName

    PayerID = WebUI.getText(findTestObject('Single Billing/Inquiry Billing ID/Confirmation/Label - DJBC - Payer ID'), FailureHandling.CONTINUE_ON_FAILURE)

    PayerID = PayerID.replace(':  ', '')

    GlobalVariable.PayerID252 = PayerID

    DocumentType = WebUI.getText(findTestObject('Single Billing/Inquiry Billing ID/Confirmation/Label - DJBC - Document Type'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    DocumentType = DocumentType.replace(':  ', '')

    GlobalVariable.DocumentType252 = DocumentType

    NoDocument = WebUI.getText(findTestObject('Single Billing/Inquiry Billing ID/Confirmation/Label - DJBC - No Document'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    NoDocument = NoDocument.replace(':  ', '')

    GlobalVariable.NoDocument252 = NoDocument

    DocumentDate = WebUI.getText(findTestObject('Single Billing/Inquiry Billing ID/Confirmation/Label - DJBC - Document Date'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    DocumentDate = DocumentDate.replace(':  ', '')

    GlobalVariable.DocumentDate252 = DocumentDate

    KPPBCCode = WebUI.getText(findTestObject('Single Billing/Inquiry Billing ID/Confirmation/Label - DJBC - KPPBC Code'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    KPPBCCode = KPPBCCode.replace(':  ', '')

    GlobalVariable.KPPBCCode252 = KPPBCCode
} else {
    WebUI.verifyMatch(TypePayment, 'DJA', false, FailureHandling.CONTINUE_ON_FAILURE)

    GlobalVariable.TypePayment252 = TypePayment

    BillingID = WebUI.getText(findTestObject('Single Billing/Inquiry Billing ID/Confirmation/Label - Billing ID'), FailureHandling.CONTINUE_ON_FAILURE)

    BillingID = BillingID.replace(':  ', '')

    WebUI.verifyMatch(BillingID, GlobalVariable.BillingID252, false, FailureHandling.CONTINUE_ON_FAILURE)

    TaxPayerName = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/Confirm/Label - Tax Payer Name'), FailureHandling.CONTINUE_ON_FAILURE)

    TaxPayerName = TaxPayerName.replace(':  ', '')

    GlobalVariable.TaxPayerName252 = TaxPayerName

    DocumentType = WebUI.getText(findTestObject('Single Billing/Inquiry Billing ID/Confirmation/Label - DJBC - Document Type'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    DocumentType = DocumentType.replace(':  ', '')

    GlobalVariable.DocumentType252 = DocumentType

    KL = WebUI.getText(findTestObject('Single Billing/Inquiry Billing ID/Confirmation/Label - DJA - K L'), FailureHandling.CONTINUE_ON_FAILURE)

    KL = KL.replace(':  ', '')

    GlobalVariable.KL252 = KL

    UnitEselon1 = WebUI.getText(findTestObject('Single Billing/Inquiry Billing ID/Confirmation/Label - DJA - Unit Eselon 1'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    UnitEselon1 = UnitEselon1.replace(':  ', '')

    GlobalVariable.UnitEselon1252 = UnitEselon1

    SatkerCode = WebUI.getText(findTestObject('Single Billing/Inquiry Billing ID/Confirmation/Label - DJA - Satker Code'), 
        FailureHandling.CONTINUE_ON_FAILURE)

    SatkerCode = SatkerCode.replace(':  ', '')

    GlobalVariable.SatkerCode252 = SatkerCode
}

Amount = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/Confirm/Label - Amount'), FailureHandling.CONTINUE_ON_FAILURE)

Amount = Amount.replace(':  ', '')

GlobalVariable.Amount252 = Amount

TotalCharge = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/Confirm/Label - Total Charge'), FailureHandling.CONTINUE_ON_FAILURE)

TotalCharge = TotalCharge.replace(':  ', '')

GlobalVariable.TotalCharge123 = TotalCharge

TotalDebitAmount = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/Confirm/Label - Total Debit Amount'), 
    FailureHandling.CONTINUE_ON_FAILURE)

TotalDebitAmount = TotalDebitAmount.replace(':  ', '')

GlobalVariable.TotalDebetAmount123 = TotalDebitAmount

InstructionMode = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/Confirm/Label - Instruction Mode'), FailureHandling.CONTINUE_ON_FAILURE)

InstructionMode = InstructionMode.replace(':  ', '')

WebUI.verifyMatch(InstructionMode, 'Specific Date', false, FailureHandling.CONTINUE_ON_FAILURE)

GlobalVariable.InstructionMode123 = InstructionMode

SpecificDateOn = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/Confirm/Label - SpecificDateOn'))

SpecificDateOn = SpecificDateOn.replace(':  ', '')

GlobalVariable.FutureDate123 = SpecificDateOn

ExpiredAt = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/Confirm/Label - Expired On'), FailureHandling.CONTINUE_ON_FAILURE)

ExpiredAt = ExpiredAt.replace(':  ', '')

GlobalVariable.ExpiredOn123 = ExpiredAt

WebUI.click(findTestObject('Single Billing/Generate Billing ID/Confirm/BTN - Submit'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Single Billing/Generate Billing ID/Confirm/BTN - Pop Up OK'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Single Billing/Generate Billing ID/Confirm/BTN - Pop Up OK'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

if (GlobalVariable.SimilliarTransaction252 == 1) {
    String[] Amount1 = Amount.split(' ')

    String Amount2 = Amount1[1]

    String[] Amount3 = Amount2.split('\\.')

    String Amount4 = Amount3[0]

    String Amount5 = Amount4.replace(',', '')

    /*
	String Amount = Amount.replace('IDR ','')
	String [] Amount112 = Amount.split('\\.')
	 String Amount123=Amount112[0]
	 Amount123 = Amount123.replace(',','')
	*/
    GlobalVariable.STAmount252 = Amount5

    if (GlobalVariable.FlagTypeBillingID252 == 0) {
        WebUI.delay(2)

        STNpwp = WebUI.getText(findTestObject('Single Billing/Similliar Transaction/Label - Similliar Transaction - Npwp'), 
            FailureHandling.CONTINUE_ON_FAILURE)

        WebUI.verifyMatch(STNpwp, GlobalVariable.NPWP252, false, FailureHandling.CONTINUE_ON_FAILURE)

        STAkunCode = WebUI.getText(findTestObject('Single Billing/Similliar Transaction/Label - Similliar Transaction - Akun Code'), 
            FailureHandling.CONTINUE_ON_FAILURE)

        String TaxAkunCode = GlobalVariable.TaxAkunCode252

        String[] TaxAkunCode1 = TaxAkunCode.split(' - ')

        TaxAkunCode02 = (TaxAkunCode1[0])

        WebUI.verifyMatch(STAkunCode, TaxAkunCode02, false, FailureHandling.CONTINUE_ON_FAILURE)

        STTaxPeriod = WebUI.getText(findTestObject('Single Billing/Similliar Transaction/Label - Similliar Transaction - Tax Period'), 
            FailureHandling.CONTINUE_ON_FAILURE)

        WebUI.verifyMatch(STTaxPeriod, GlobalVariable.TaxPeriodType252, false, FailureHandling.CONTINUE_ON_FAILURE)

        STAmount = WebUI.getText(findTestObject('Single Billing/Similliar Transaction/Label - Similliar Transaction - Amount'), 
            FailureHandling.CONTINUE_ON_FAILURE)

        WebUI.verifyMatch(STAmount, GlobalVariable.STAmount252, false, FailureHandling.CONTINUE_ON_FAILURE)

        WebUI.click(findTestObject('Single Billing/Generate Billing ID/Confirm/BTN - Similliar Transaction - Continue'))
    } else if (GlobalVariable.FlagTypeBillingID252 == 1) {
        BillingID = WebUI.getText(findTestObject('Single Billing/Similliar Transaction/Label - Similliar Transaction - Billing ID'))

        WebUI.verifyMatch(BillingID, GlobalVariable.BillingID252, false, FailureHandling.CONTINUE_ON_FAILURE)

        STAmount = WebUI.getText(findTestObject('Single Billing/Similliar Transaction/Label - Similliar Transaction - Amount'), 
            FailureHandling.CONTINUE_ON_FAILURE)

        WebUI.verifyMatch(STAmount, GlobalVariable.STAmount252, false, FailureHandling.CONTINUE_ON_FAILURE)

        WebUI.click(findTestObject('Single Billing/Generate Billing ID/Confirm/BTN - Similliar Transaction - Continue'))
    } else {
        BillingID = WebUI.getText(findTestObject('Single Billing/Similliar Transaction/Label - Similliar Transaction - Billing ID'))

        WebUI.verifyMatch(BillingID, GlobalVariable.BillingID252, false, FailureHandling.CONTINUE_ON_FAILURE)

        STAmount = WebUI.getText(findTestObject('Single Billing/Similliar Transaction/Label - Similliar Transaction - Amount'), 
            FailureHandling.CONTINUE_ON_FAILURE)

        WebUI.verifyMatch(STAmount, GlobalVariable.STAmount252, false, FailureHandling.CONTINUE_ON_FAILURE)

        WebUI.click(findTestObject('Single Billing/Generate Billing ID/Confirm/BTN - Similliar Transaction - Continue'))
    }
}

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

