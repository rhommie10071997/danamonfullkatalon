import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
'----------------------------------------------------------------------------------------------------------'
'Tolong di baca dulu'

//Billing Id nya
GlobalVariable.BillingID252 = '699999999007394'


//Transfer From nya, bisa account number atau account name nya
TransferFrom = 'abd karim'

//From Account Description Ardo , nanti di belakangnya ada angka random
FromAccDescArdo = 'FromAccDescArdo'

//Payment Description nya
GlobalVariable.PaymentDescription252 = 'PaymentDecriptionArdoletsgo'



//Untuk Currency masih bisa IDR karna dari applikasi bisanya IDR

//Similliar Transaction
GlobalVariable.SimilliarTransaction252 = 1

'----------------------------------------------------------------------------------------------------------'
String TypeBillingID = GlobalVariable.BillingID252

char BillingIDfirst = TypeBillingID.charAt(0);

if (BillingIDfirst == "1" || BillingIDfirst == "2" || BillingIDfirst == "3") {
	GlobalVariable.FlagTypeBillingID252 = 0
} else if (BillingIDfirst == "4" || BillingIDfirst == "5" || BillingIDfirst == "6") {
	GlobalVariable.FlagTypeBillingID252 = 1
} else {
GlobalVariable.FlagTypeBillingID252 = 2
}


WebUI.openBrowser(GlobalVariable.url, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForAngularLoad(0)

WebUI.waitForElementVisible(findTestObject('1 - Frame/1 - iframe login'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Log in/corpid'), 'pcm000100', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Log in/userid'), 'hermione', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Log in/password'), 'Password123', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Log in/login'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(3, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Menu/BTN - Menu Object'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Menu/TextField - Search'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Menu/TextField - Search'), ' ', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Menu/TextField - Search'), 'Single Billing', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForElementVisible(findTestObject('Menu/Label - Single Billing (By Search)'), 0, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.click(findTestObject('Menu/Label - Single Billing (By Search)'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(2, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(5, FailureHandling.CONTINUE_ON_FAILURE)

WebUI.waitForAngularLoad(0)

WebUI.waitForElementVisible(findTestObject('Single Billing/Generate Billing ID/New Entry/Label - Title'), 0)

Title = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/New Entry/Label - Title'))

WebUI.verifyMatch(Title, 'Single Billing', false)

WebUI.click(findTestObject('Single Billing/Inquiry Billing ID/New Entry/BTN - Payment Billing ID'))

WebUI.delay(5)

WebUI.waitForElementVisible(findTestObject('Single Billing/Inquiry Billing ID/New Entry/Label - Sub Title'), 0)

SubTitle = WebUI.getText(findTestObject('Single Billing/Inquiry Billing ID/New Entry/Label - Sub Title'), FailureHandling.STOP_ON_FAILURE)

WebUI.verifyMatch(SubTitle, 'Payment Billing ID', false)

WebUI.click(findTestObject('Single Billing/Generate Billing ID/New Entry/DropList - Transfer From'))

WebUI.delay(2)

WebUI.waitForElementVisible(findTestObject('Single Billing/Generate Billing ID/New Entry/TextField - Drop List'), 0)

WebUI.setText(findTestObject('Single Billing/Generate Billing ID/New Entry/TextField - Drop List'), TransferFrom)

WebUI.delay(1)

WebUI.waitForElementVisible(findTestObject('Single Billing/Generate Billing ID/New Entry/BTN - High Lighted'), 0)

WebUI.click(findTestObject('Single Billing/Generate Billing ID/New Entry/BTN - High Lighted'))

TransferFrom = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/New Entry/DropList - Transfer From'))

GlobalVariable.FromAccount123 = TransferFrom

Random random = new Random()

int RandAccDesc

RandAccDesc = random.nextInt(99)

RandAccDesc1 = (FromAccDescArdo + RandAccDesc)

GlobalVariable.FromAccountDescription123 = RandAccDesc1

WebUI.setText(findTestObject('Single Billing/Generate Billing ID/New Entry/TextField - From Account Description'), RandAccDesc1)

WebUI.setText(findTestObject('Single Billing/Inquiry Billing ID/New Entry/TextField - Billing ID'), GlobalVariable.BillingID252)

WebUI.click(findTestObject('Single Billing/Generate Billing ID/New Entry/DropList - Amount'))

WebUI.delay(2)

WebUI.waitForElementVisible(findTestObject('Single Billing/Generate Billing ID/New Entry/TextField - Drop List'), 0)

WebUI.setText(findTestObject('Single Billing/Generate Billing ID/New Entry/TextField - Drop List'), 'IDR')

WebUI.delay(1)

WebUI.waitForElementVisible(findTestObject('Single Billing/Generate Billing ID/New Entry/BTN - High Lighted'), 0)

WebUI.click(findTestObject('Single Billing/Generate Billing ID/New Entry/BTN - High Lighted'))

CurrencyAmount = WebUI.getText(findTestObject('Single Billing/Generate Billing ID/New Entry/DropList - Amount'))

GlobalVariable.Currency = CurrencyAmount


WebUI.setText(findTestObject('Single Billing/Generate Billing ID/New Entry/TextField - Payment Description'), GlobalVariable.PaymentDescription252)

WebUI.click(findTestObject('Single Billing/Generate Billing ID/New Entry/BTN - Immediate'))

WebUI.click(findTestObject('Cash Pooling/New Entry/Checkbox - Terms and Condition'))

WebUI.click(findTestObject('Cash Pooling/New Entry/BTN - Confirm'))

WebUI.delay(5)

