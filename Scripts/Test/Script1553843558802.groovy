import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser(GlobalVariable.url)

WebUI.delay(5)

WebUI.setText(findTestObject('Log in/corpid'), 'pcm000100', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Log in/userid'), 'hermione', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.setText(findTestObject('Log in/password'), 'Password123', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(4)

WebUI.click(findTestObject('Log in/login'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(4, FailureHandling.CONTINUE_ON_FAILURE)

not_run: WebUI.switchToFrame(findTestObject('1 - Frame/1 - iframe login'), 0)

CustomKeywords.'get.Screencapture.getElement'(findTestObject('1 - Frame/1 - iframe login'), '')

not_run: WebUI.switchToDefaultContent()

WebUI.click(findTestObject('5 - Log Out btn'), FailureHandling.CONTINUE_ON_FAILURE)

WebUI.delay(4, FailureHandling.CONTINUE_ON_FAILURE)

