<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>24</name>
   <tag></tag>
   <elementGuidId>40f05b82-6cf7-4a03-82d8-2ee5e2ebcb5f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//label[contains(.,&quot;Purpose Of Transaction&quot;)]/following-sibling::div/label</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/MainFrame</value>
   </webElementProperties>
</WebElementEntity>
