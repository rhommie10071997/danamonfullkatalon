<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>20</name>
   <tag></tag>
   <elementGuidId>40ad1300-8b73-4fe4-9597-1554f43e4d7d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>
//label[contains(.,&quot;Beneficiary Resident Status&quot;)]/following-sibling::div/label</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/MainFrame</value>
   </webElementProperties>
</WebElementEntity>
