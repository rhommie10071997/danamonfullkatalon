<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>V-reffNumber</name>
   <tag></tag>
   <elementGuidId>bc93e832-1725-426b-9103-089105dfa544</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//label[contains(.,&quot;Transaction Reference No&quot;)]/following-sibling::div/label</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/JobFrame</value>
   </webElementProperties>
</WebElementEntity>
