<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>V-beneficiaryCategory</name>
   <tag></tag>
   <elementGuidId>fc4b5214-e7bf-472f-bd14-4c37f67cf3b9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//label[contains(.,&quot;Beneficiary Category&quot;)]/following-sibling::div/label</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/MainFrame</value>
   </webElementProperties>
</WebElementEntity>
