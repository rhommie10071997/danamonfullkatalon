<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Label - File Format - Copy</name>
   <tag></tag>
   <elementGuidId>906e8efa-11d0-4a7d-b1bb-311f66406c7d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@ref_element = 'Object Repository/1 - Frame/2 - Iframe mainframe']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//label[contains(.,&quot;File Format&quot;)]/following-sibling::label)[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/1 - Frame/2 - Iframe mainframe</value>
   </webElementProperties>
</WebElementEntity>
