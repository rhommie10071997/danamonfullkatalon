<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Details-Frame</name>
   <tag></tag>
   <elementGuidId>915aa2b6-994c-4106-836f-5a789c647ca6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//iframe[@id=&quot;detailRecordFrame&quot;]</value>
      </entry>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'detailRecordFrame' and @ref_element = 'Object Repository/MainFrame']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>detailRecordFrame</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/LoginFrame</value>
   </webElementProperties>
</WebElementEntity>
