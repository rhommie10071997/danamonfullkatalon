<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Label - Repeat - Instruction On Holiday</name>
   <tag></tag>
   <elementGuidId>ff95deb6-54db-4543-985e-165b492aaddf</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[label[contains(.,&quot;Instruction on Holiday&quot;)]]/div/label</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/1 - Frame/2 - Iframe mainframe</value>
   </webElementProperties>
</WebElementEntity>
