<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Label - Phone</name>
   <tag></tag>
   <elementGuidId>e3c020b8-546d-49e1-834d-b5e914a7e676</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//label[contains(.,&quot;Beneficiary Notification&quot;)]/../following-sibling::div[1]/div/label</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/1 - Frame/2 - Iframe mainframe</value>
   </webElementProperties>
</WebElementEntity>
