<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>V-beneficiaryReffNumber</name>
   <tag></tag>
   <elementGuidId>cd374067-c186-4ca3-81cf-a703fb91a24e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//label[text()=&quot;Beneficiary Reference No&quot;]/following-sibling::div/label</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/JobFrame</value>
   </webElementProperties>
</WebElementEntity>
