<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>V-beneficiaryReferenceNumber</name>
   <tag></tag>
   <elementGuidId>638d2ab7-e6fd-466f-9a32-0c9a642c81d9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>/html[1]/body[1]/div[2]/form[1]/div[19]/div[1]/label[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/JobFrame</value>
   </webElementProperties>
</WebElementEntity>
