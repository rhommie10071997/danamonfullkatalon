<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>V-on</name>
   <tag></tag>
   <elementGuidId>bb2ad056-6851-42f0-824c-b2350f0abfb7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//label[contains(.,&quot;on&quot;)]/following-sibling::div/label)[9]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/JobFrame</value>
   </webElementProperties>
</WebElementEntity>
