<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Label - Cable Fee Equivalent</name>
   <tag></tag>
   <elementGuidId>97ce1219-3056-4b4d-9208-77d2297fb097</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//label[contains(.,&quot;Cable Fee&quot;)]/following-sibling::label[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/1 - Frame/3 - iJobFrame</value>
   </webElementProperties>
</WebElementEntity>
