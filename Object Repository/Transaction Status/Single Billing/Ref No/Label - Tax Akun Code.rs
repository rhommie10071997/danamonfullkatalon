<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Label - Tax Akun Code</name>
   <tag></tag>
   <elementGuidId>6a89da76-b1c7-4c49-a62e-c5a2b0d17cac</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//label[contains(.,&quot;Tax Akun Code&quot;)]/following-sibling::label</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/1 - Frame/3 - iJobFrame</value>
   </webElementProperties>
</WebElementEntity>
