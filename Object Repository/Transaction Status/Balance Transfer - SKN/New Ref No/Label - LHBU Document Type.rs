<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Label - LHBU Document Type</name>
   <tag></tag>
   <elementGuidId>f776dedb-b897-415b-9192-76fd947f993c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>( //label[contains(.,&quot;LHBU Document Type&quot;)]/following-sibling::div/label)[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/1 - Frame/3 - iJobFrame</value>
   </webElementProperties>
</WebElementEntity>
