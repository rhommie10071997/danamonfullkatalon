<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>BTN - Document Number</name>
   <tag></tag>
   <elementGuidId>521d4b42-2324-4d45-930c-62fe7f3b417f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//td[contains(.,&quot;:&quot;)]/following-sibling::td/a)[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/1 - Frame/2 - Iframe mainframe</value>
   </webElementProperties>
</WebElementEntity>
