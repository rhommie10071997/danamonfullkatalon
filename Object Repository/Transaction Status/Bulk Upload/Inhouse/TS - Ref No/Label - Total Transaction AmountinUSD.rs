<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Label - Total Transaction AmountinUSD</name>
   <tag></tag>
   <elementGuidId>00b6b57c-086a-4010-9163-caf21cbbd668</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//label[contains(.,&quot;Total Transaction AmountinUSD&quot;)]/following-sibling::label)[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/1 - Frame/3 - iJobFrame</value>
   </webElementProperties>
</WebElementEntity>
