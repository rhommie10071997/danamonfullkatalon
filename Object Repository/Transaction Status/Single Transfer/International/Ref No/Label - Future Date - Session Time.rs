<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Label - Future Date - Session Time</name>
   <tag></tag>
   <elementGuidId>b2b95845-cf59-4452-ab0c-91a595f06009</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[label[contains(.,&quot;Session Time&quot;)]]/div/label</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/1 - Frame/3 - iJobFrame</value>
   </webElementProperties>
</WebElementEntity>
