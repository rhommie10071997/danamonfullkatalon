<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Label - Provision</name>
   <tag></tag>
   <elementGuidId>b0f6ef9f-510f-4b64-95fa-ba5bc1ac2654</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//label[contains(.,&quot;Provision&quot;)]/following-sibling::div/label[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/1 - Frame/3 - iJobFrame</value>
   </webElementProperties>
</WebElementEntity>
