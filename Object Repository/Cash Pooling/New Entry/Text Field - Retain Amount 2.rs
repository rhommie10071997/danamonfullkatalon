<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Text Field - Retain Amount 2</name>
   <tag></tag>
   <elementGuidId>35a284ea-2c73-4dbe-aeed-73f109c4a1b7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//tbody[1]/tr[2]/td[3]/input</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/1 - Frame/2 - Iframe mainframe</value>
   </webElementProperties>
</WebElementEntity>
