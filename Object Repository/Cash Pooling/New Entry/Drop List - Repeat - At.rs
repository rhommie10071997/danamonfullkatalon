<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Drop List - Repeat - At</name>
   <tag></tag>
   <elementGuidId>80564f90-b17d-481c-a3b7-650f5526a0be</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@ref_element = 'Object Repository/1 - Frame/2 - Iframe mainframe']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id=&quot;s2id_sessionTimeRepeatLevelTemp&quot;]/a/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/1 - Frame/2 - Iframe mainframe</value>
   </webElementProperties>
</WebElementEntity>
