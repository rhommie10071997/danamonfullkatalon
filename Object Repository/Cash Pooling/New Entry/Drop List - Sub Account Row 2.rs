<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Drop List - Sub Account Row 2</name>
   <tag></tag>
   <elementGuidId>8ce2d510-1ace-4da4-a4e0-d3d2a3a06067</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//tbody[1]/tr[2]/td[2]/div[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/1 - Frame/2 - Iframe mainframe</value>
   </webElementProperties>
</WebElementEntity>
