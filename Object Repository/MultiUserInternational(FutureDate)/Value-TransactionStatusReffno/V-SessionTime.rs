<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>V-SessionTime</name>
   <tag></tag>
   <elementGuidId>c710c271-591d-4d32-bff3-d1382f334355</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>//label[contains(.,&quot;Session Time&quot;)]/following-sibling::div/label[contains(.,&quot;Session&quot;)]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//label[contains(.,&quot;Session Time&quot;)]/following-sibling::div/label[contains(.,&quot;Session&quot;)]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/JobFrame</value>
   </webElementProperties>
</WebElementEntity>
