<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>V-SessionTime</name>
   <tag></tag>
   <elementGuidId>bce8f4b2-aeae-4776-b177-7d001ce75e10</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>//label[contains(.,&quot;Session Time&quot;)]/following-sibling::div/label[contains(.,&quot;Session&quot;)]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//label[contains(.,&quot;Session Time&quot;)]/following-sibling::div/label[contains(.,&quot;Session&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/MainFrame</value>
   </webElementProperties>
</WebElementEntity>
