<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>V-address3Details</name>
   <tag></tag>
   <elementGuidId>2303ccef-1cd3-4611-9fb8-9ceb8a95c92e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//label[@for=&quot;benefAddress3&quot;]/following-sibling::div/label</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Details-Frame</value>
   </webElementProperties>
</WebElementEntity>
