<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Label - mainAccountChecker</name>
   <tag></tag>
   <elementGuidId>09e0c94c-c9e4-48e9-b2f7-bda11bd3a780</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//label[contains(.,&quot;Main Account&quot;)]/following-sibling::div</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/menu/Iframe - iframeMenu</value>
   </webElementProperties>
</WebElementEntity>
