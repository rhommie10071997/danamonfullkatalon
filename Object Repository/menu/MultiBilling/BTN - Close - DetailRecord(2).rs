<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>BTN - Close - DetailRecord(2)</name>
   <tag></tag>
   <elementGuidId>19f8a2cf-c155-4f4f-93e1-fd84e6fd44d9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//button[contains(.,&quot;Close&quot;)]/i)[2]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/1 - Frame/1 - iframe login</value>
   </webElementProperties>
</WebElementEntity>
