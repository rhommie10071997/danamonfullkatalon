<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>BTN - SubmitEnding</name>
   <tag></tag>
   <elementGuidId>17a60b51-5104-4db2-bfa5-c6ef0eb08094</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'btnSubmit' and @ref_element = 'Object Repository/1 - Frame/2 - Iframe mainframe']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[contains(.,&quot;Submit&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/1 - Frame/1 - iframe login</value>
   </webElementProperties>
</WebElementEntity>
