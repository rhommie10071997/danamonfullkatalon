<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Label - on - Copy</name>
   <tag></tag>
   <elementGuidId>0de4473a-812f-4010-9935-5f3a538b1059</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//label[.='on']/following-sibling::label</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/menu/Iframe - iframeMenu</value>
   </webElementProperties>
</WebElementEntity>
