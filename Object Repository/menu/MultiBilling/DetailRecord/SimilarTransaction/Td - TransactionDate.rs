<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Td - TransactionDate</name>
   <tag></tag>
   <elementGuidId>981c7268-6853-4b07-9e53-b58055129914</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@ref_element = 'Object Repository/1 - Frame/6 - iFrame Detail Record Frame']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//table[@id=&quot;globalTable&quot;]/tbody/tr/td[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/1 - Frame/6 - iFrame Detail Record Frame 2</value>
   </webElementProperties>
</WebElementEntity>
