<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Td - Npwp(DetailRecord)</name>
   <tag></tag>
   <elementGuidId>a0832e36-15ae-4a6b-bf66-9d736da4d7f3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//table[@id=&quot;globalTable&quot;]/tbody/tr/td[2]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ref_element = 'Object Repository/1 - Frame/6 - iFrame Detail Record Frame']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/1 - Frame/6 - iFrame Detail Record Frame</value>
   </webElementProperties>
</WebElementEntity>
