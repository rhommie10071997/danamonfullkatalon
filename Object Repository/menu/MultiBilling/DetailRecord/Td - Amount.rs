<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Td - Amount</name>
   <tag></tag>
   <elementGuidId>76431078-72ee-45cc-9c64-dbad4e527f38</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@ref_element = 'Object Repository/1 - Frame/6 - iFrame Detail Record Frame']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//table[@id=&quot;globalTable&quot;]/tbody/tr/td[9]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/1 - Frame/6 - iFrame Detail Record Frame</value>
   </webElementProperties>
</WebElementEntity>
