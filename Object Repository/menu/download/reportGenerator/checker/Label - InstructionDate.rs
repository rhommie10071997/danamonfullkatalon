<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Label - InstructionDate</name>
   <tag></tag>
   <elementGuidId>c1fd505f-1b46-4196-9d06-865c29fd98d5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'onetimeDate' and @ref_element = 'Object Repository/menu/iframeMenu']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//label[contains(.,&quot;Instruction Date&quot;)]/following-sibling::div/label</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>onetimeDate</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/menu/Iframe - iframeMenu</value>
   </webElementProperties>
</WebElementEntity>
