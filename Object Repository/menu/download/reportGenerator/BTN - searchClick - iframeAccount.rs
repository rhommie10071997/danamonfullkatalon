<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>BTN - searchClick - iframeAccount</name>
   <tag></tag>
   <elementGuidId>43c190d2-5fef-4e3c-a139-94eb109cf5d4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@ref_element = 'Object Repository/1 - Frame/iframeAccount']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//form[@id=&quot;accountStatementForm&quot;]//button[contains(.,&quot;Search&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/1 - Frame/iframeAccount</value>
   </webElementProperties>
</WebElementEntity>
