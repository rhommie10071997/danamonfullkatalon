<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Input - AccountNumberTextfield(2)</name>
   <tag></tag>
   <elementGuidId>054d9a3d-dd23-4403-bdf1-b7d9eac871bc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@ref_element = 'Object Repository/1 - Frame/4 - iAccountFrame']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//label[contains(.,&quot;Account Number&quot;)]/following-sibling::div/input[@id=&quot;accountNo&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/1 - Frame/4 - iAccountFrame</value>
   </webElementProperties>
</WebElementEntity>
