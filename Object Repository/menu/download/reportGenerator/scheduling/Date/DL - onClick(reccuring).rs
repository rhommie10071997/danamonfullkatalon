<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>DL - onClick(reccuring)</name>
   <tag></tag>
   <elementGuidId>2af00dd2-4c94-443f-8252-600ef53b9598</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id=&quot;s2id_recurringOn&quot;]/a/span</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'onetimeDate' and @ref_element = 'Object Repository/menu/iframeMenu']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>onetimeDate</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/menu/Iframe - iframeMenu</value>
   </webElementProperties>
</WebElementEntity>
