<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CheckBox - checkboxClick</name>
   <tag></tag>
   <elementGuidId>40fd9c0d-f0f2-4c2f-be7e-05eb328aba65</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//table[@id=&quot;globalTable&quot;]/tbody/tr[1]/td/input[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'accountNo' and @ref_element = 'Object Repository/menu/download/reportGenerator/search/iframeAccount']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>accountNo</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/1 - Frame/iframeAccount</value>
   </webElementProperties>
</WebElementEntity>
