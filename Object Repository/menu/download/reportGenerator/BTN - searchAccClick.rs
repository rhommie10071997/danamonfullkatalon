<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>BTN - searchAccClick</name>
   <tag></tag>
   <elementGuidId>2241bd64-5b2c-4126-b5c7-5f722cf2c739</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//form[@id=&quot;accountStatementForm&quot;]/div[3]/div/button</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'accountNo' and @ref_element = 'Object Repository/menu/download/reportGenerator/search/iframeAccount']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>accountNo</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/menu/download/reportGenerator/search/iframeAccount</value>
   </webElementProperties>
</WebElementEntity>
