<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Td - getTable</name>
   <tag></tag>
   <elementGuidId>2c04e539-b43c-4a1d-bcf4-cd4137f1cbcd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@ref_element = 'Object Repository/1 - Frame/2 - Iframe mainframe']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//table[@id=&quot;globalTable&quot;]//td</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/1 - Frame/2 - Iframe mainframe</value>
   </webElementProperties>
</WebElementEntity>
