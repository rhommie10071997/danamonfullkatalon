<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Label - totalAmount</name>
   <tag></tag>
   <elementGuidId>838b7933-e77f-4ff7-903b-2f62282104e1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//label[contains(.,&quot;Total Amount&quot;)]/following-sibling::label</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'onetimeDate' and @ref_element = 'Object Repository/menu/iframeMenu']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>onetimeDate</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/1 - Frame/6 - iFrame Detail Record Frame</value>
   </webElementProperties>
</WebElementEntity>
