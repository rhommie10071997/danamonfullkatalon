<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>detailClose</name>
   <tag></tag>
   <elementGuidId>2c9a48ea-69e2-455c-85ee-a8b134f941a7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>/html[1]/body[1]/div[10]/div[11]/div[1]/button[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ref_element = 'Object Repository/menu/trfManagement/bulkTransfer/recordList/detailRecord/iframeDetailRecord']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/homelogin/homeiframe</value>
   </webElementProperties>
</WebElementEntity>
