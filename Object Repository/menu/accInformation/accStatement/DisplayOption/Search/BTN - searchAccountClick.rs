<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>BTN - searchAccountClick</name>
   <tag></tag>
   <elementGuidId>5d5f11d1-9803-4cff-b25c-d3e4dd3ba47c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[normalize-space(text())='Search']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/menu/accInformation/accStatement/DisplayOption/Search/iframe - iframeAccount</value>
   </webElementProperties>
</WebElementEntity>
