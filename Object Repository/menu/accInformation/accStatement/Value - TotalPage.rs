<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Value - TotalPage</name>
   <tag></tag>
   <elementGuidId>309d8e78-edb7-4a8a-8276-43084c689d59</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//form[@id=&quot;accountStatementForm&quot;]/div[@id=&quot;hide&quot;]/div[2]/div[5]/div[@id=&quot;paggingArea&quot;]/span[@id=&quot;totalPage&quot;]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/1 - Frame/2 - Iframe mainframe</value>
   </webElementProperties>
</WebElementEntity>
