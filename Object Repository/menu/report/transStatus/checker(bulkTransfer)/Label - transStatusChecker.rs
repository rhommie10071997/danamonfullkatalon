<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Label - transStatusChecker</name>
   <tag></tag>
   <elementGuidId>dee0329f-f79d-4b00-8ddf-44447fbfe622</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//label[contains(.,&quot;Transaction Status&quot;)]/following-sibling::label</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/menu/report/transStatus/jobFrame/jobFrame</value>
   </webElementProperties>
</WebElementEntity>
