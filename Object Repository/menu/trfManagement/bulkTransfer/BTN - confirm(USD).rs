<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>BTN - confirm(USD)</name>
   <tag></tag>
   <elementGuidId>c3f4ba48-2668-4e8e-a17c-a71916ea3897</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[contains(.,&quot;Ok&quot;)]/i</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/homelogin/homeiframe</value>
   </webElementProperties>
</WebElementEntity>
