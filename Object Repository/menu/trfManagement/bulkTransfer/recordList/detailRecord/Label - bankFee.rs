<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Label - bankFee</name>
   <tag></tag>
   <elementGuidId>912d970b-373f-4532-ba1f-d2e49b19c811</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//label[contains(.,&quot;Correspondent Bank Fee&quot;)]/following-sibling::label[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ref_element = 'Object Repository/menu/trfManagement/bulkTransfer/recordList/recordFrame']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/menu/trfManagement/bulkTransfer/recordList/detailRecord/Iframe - iframeDetailRecord</value>
   </webElementProperties>
</WebElementEntity>
