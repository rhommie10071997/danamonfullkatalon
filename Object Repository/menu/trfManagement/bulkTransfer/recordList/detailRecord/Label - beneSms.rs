<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Label - beneSms</name>
   <tag></tag>
   <elementGuidId>e93b28d2-c5dc-4850-92ab-0084e516c096</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//label[contains(.,&quot;SMS&quot;)]/following-sibling::label</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/menu/trfManagement/bulkTransfer/recordList/detailRecord/Iframe - iframeDetailRecord</value>
   </webElementProperties>
</WebElementEntity>
