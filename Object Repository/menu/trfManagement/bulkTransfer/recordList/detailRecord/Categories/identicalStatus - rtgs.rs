<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>identicalStatus - rtgs</name>
   <tag></tag>
   <elementGuidId>f08e05a7-73fa-41ac-b1cc-029285a3814c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>/html[1]/body[1]/div[2]/div[1]/form[1]/div[28]/label[2]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ref_element = 'Object Repository/menu/trfManagement/bulkTransfer/recordList/detailRecord/iframeDetailRecord']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/menu/trfManagement/bulkTransfer/recordList/detailRecord/Iframe - iframeDetailRecord</value>
   </webElementProperties>
</WebElementEntity>
