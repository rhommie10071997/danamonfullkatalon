<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Label - minimumTransactionFee</name>
   <tag></tag>
   <elementGuidId>1da6ce0d-bcbc-4c5b-bfb4-ca69d7b6dc18</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//label[contains(.,&quot;Minimum Transaction Fee&quot;)]/following-sibling::label[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ref_element = 'Object Repository/menu/trfManagement/bulkTransfer/recordList/recordFrame']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/menu/trfManagement/bulkTransfer/recordList/detailRecord/Iframe - iframeDetailRecord</value>
   </webElementProperties>
</WebElementEntity>
