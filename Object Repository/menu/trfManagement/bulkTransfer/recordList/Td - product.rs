<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Td - product</name>
   <tag></tag>
   <elementGuidId>98137e5b-1792-4942-9493-b3ffa2cf4ff9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//table[@id=&quot;globalTableTotalRcd&quot;]/tbody/tr/td[2]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ref_element = 'Object Repository/menu/trfManagement/bulkTransfer/recordList/recordFrame']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/menu/trfManagement/bulkTransfer/recordList/Iframe - recordFrame</value>
   </webElementProperties>
</WebElementEntity>
