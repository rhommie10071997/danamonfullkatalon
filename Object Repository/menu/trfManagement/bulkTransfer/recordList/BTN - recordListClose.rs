<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>BTN - recordListClose</name>
   <tag></tag>
   <elementGuidId>0f8e9b1a-cb98-4597-9014-b90cd68c94ef</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@ref_element = 'Object Repository/menu/trfManagement/bulkTransfer/recordList/recordFrame']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[contains(.,&quot;Close&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/homelogin/homeiframe</value>
   </webElementProperties>
</WebElementEntity>
