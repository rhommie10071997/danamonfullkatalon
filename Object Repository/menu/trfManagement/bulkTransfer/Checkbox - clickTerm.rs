<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Checkbox - clickTerm</name>
   <tag></tag>
   <elementGuidId>129e8b59-c8a0-425c-857b-32a8ad8efbe4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id=&quot;checkAgree&quot;]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'checkAgree' and @ref_element = 'Object Repository/menu/trfManagement/bulkTransfer/iframeTermAndConditionFrame']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>checkAgree</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>checkAgree</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/menu/trfManagement/bulkTransfer/iframeTermAndConditionFrame</value>
   </webElementProperties>
</WebElementEntity>
