<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>iframeTermAndConditionFrame</name>
   <tag></tag>
   <elementGuidId>5f6983bb-87c1-4ead-b643-dec251a44b72</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>/html[1]/body[1]/div[2]/div[1]/div[1]/form[1]/div[13]/iframe[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'termAndConditionFrame' and @ref_element = 'Object Repository/homelogin/homeiframe']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>termAndConditionFrame</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/menu/Iframe - iframeMenu</value>
   </webElementProperties>
</WebElementEntity>
