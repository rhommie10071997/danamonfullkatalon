<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>BTN - Pop Up - OK</name>
   <tag></tag>
   <elementGuidId>d3744b80-0750-446c-b1e5-dd74a50d3633</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@ref_element = 'Object Repository/login']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@class=&quot;btn btn-primary&quot;][contains(.,&quot; Ok&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/1 - Frame/1 - iframe login</value>
   </webElementProperties>
</WebElementEntity>
