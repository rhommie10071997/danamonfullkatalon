<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Label - Your Transaction Status is waiting for approval</name>
   <tag></tag>
   <elementGuidId>666cc7b9-73de-4566-bc5e-b51ff7f762ef</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//form[1]/div[2]/div[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/1 - Frame/2 - Iframe mainframe</value>
   </webElementProperties>
</WebElementEntity>
