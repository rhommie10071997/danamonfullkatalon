<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>V-date</name>
   <tag></tag>
   <elementGuidId>3cdb7821-45dd-43cd-b82a-6765fff87516</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//label[text()=&quot;Submit Date Time&quot;]/following-sibling::div/label</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/MainFrame</value>
   </webElementProperties>
</WebElementEntity>
