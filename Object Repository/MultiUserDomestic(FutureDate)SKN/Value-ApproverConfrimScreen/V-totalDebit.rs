<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>V-totalDebit</name>
   <tag></tag>
   <elementGuidId>3738decf-20e1-49bb-943f-643bbad18616</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//label[text()=&quot;Total Charge&quot;]/following-sibling::div/label</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/MainFrame</value>
   </webElementProperties>
</WebElementEntity>
