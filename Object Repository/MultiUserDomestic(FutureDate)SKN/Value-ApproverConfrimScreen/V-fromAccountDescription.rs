<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>V-fromAccountDescription</name>
   <tag></tag>
   <elementGuidId>0c6819db-c797-42d3-b689-4f10abcd9612</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//label[text()=&quot;From Account Description&quot;]/following-sibling::div/label</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/MainFrame</value>
   </webElementProperties>
</WebElementEntity>
