<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>V-amount</name>
   <tag></tag>
   <elementGuidId>b91c77a2-1c5c-4e8d-80f6-2caee246bde2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//label[text()=&quot;Amount&quot;]/following-sibling::div/label</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/MainFrame</value>
   </webElementProperties>
</WebElementEntity>
