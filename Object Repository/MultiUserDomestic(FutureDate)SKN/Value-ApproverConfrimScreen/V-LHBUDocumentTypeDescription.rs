<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>V-LHBUDocumentTypeDescription</name>
   <tag></tag>
   <elementGuidId>2a985fac-a531-46db-8aa4-9c315f4e41f9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//label[text()=&quot;LHBU Document Type Description&quot;]/following-sibling::div/label</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/MainFrame</value>
   </webElementProperties>
</WebElementEntity>
