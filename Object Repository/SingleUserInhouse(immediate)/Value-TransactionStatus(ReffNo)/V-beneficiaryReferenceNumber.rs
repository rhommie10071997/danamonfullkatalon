<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>V-beneficiaryReferenceNumber</name>
   <tag></tag>
   <elementGuidId>ea6a80bd-24d4-4b85-9dd2-7307fddb1501</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>/html[1]/body[1]/div[2]/form[1]/div[16]/div[1]/label[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/JobFrame</value>
   </webElementProperties>
</WebElementEntity>
