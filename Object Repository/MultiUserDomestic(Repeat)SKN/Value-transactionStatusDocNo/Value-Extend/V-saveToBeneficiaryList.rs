<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>V-saveToBeneficiaryList</name>
   <tag></tag>
   <elementGuidId>8816eae5-642e-4b0f-87ef-d8850f70d7e9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//label[text()=&quot;Save to Beneficiary List&quot;]/following-sibling::div/label</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/JobFrame</value>
   </webElementProperties>
</WebElementEntity>
