<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>V-SortingTable</name>
   <tag></tag>
   <elementGuidId>783ffe15-35a6-41ec-b2a1-f46f18b22085</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//tr[@role=&quot;row&quot;]/th[1])[3]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/MainFrame</value>
   </webElementProperties>
</WebElementEntity>
