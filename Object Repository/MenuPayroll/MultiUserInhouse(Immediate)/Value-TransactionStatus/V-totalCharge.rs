<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>V-totalCharge</name>
   <tag></tag>
   <elementGuidId>385ea75f-466b-4f61-8d56-539adb2d0bb5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@ref_element = 'Object Repository/Details-Frame']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//label[contains(.,&quot;Total Charges&quot;)]/following-sibling::label</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Details-Frame</value>
   </webElementProperties>
</WebElementEntity>
