<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>V-address2</name>
   <tag></tag>
   <elementGuidId>a2461348-e46e-454e-967c-eaa09557aaca</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div/label[@for=&quot;beneficiaryAddress2&quot;]/following-sibling::div/label</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/JobFrame</value>
   </webElementProperties>
</WebElementEntity>
