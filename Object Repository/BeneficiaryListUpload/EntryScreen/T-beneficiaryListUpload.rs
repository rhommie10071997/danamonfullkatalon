<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>T-beneficiaryListUpload</name>
   <tag></tag>
   <elementGuidId>21871b41-a718-4587-b626-f03ceb9fc27c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//span[contains(.,&quot;Beneficiary List Upload&quot;)])[2]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/LoginFrame</value>
   </webElementProperties>
</WebElementEntity>
