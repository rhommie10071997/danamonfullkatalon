<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Status</name>
   <tag></tag>
   <elementGuidId>708901b0-02d1-4fa1-8fb3-cb84966d600d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id=&quot;successMessage&quot;]/div</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@ref_element = 'Object Repository/MainFrame']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/MainFrame</value>
   </webElementProperties>
</WebElementEntity>
