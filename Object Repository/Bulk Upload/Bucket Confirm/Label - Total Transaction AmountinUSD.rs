<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Label - Total Transaction AmountinUSD</name>
   <tag></tag>
   <elementGuidId>d83fb169-67d0-4a21-93fe-9d89532217c1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//label[contains(.,&quot;Total Transaction AmountinUSD&quot;)]/following-sibling::label)[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/MainFrame</value>
   </webElementProperties>
</WebElementEntity>
