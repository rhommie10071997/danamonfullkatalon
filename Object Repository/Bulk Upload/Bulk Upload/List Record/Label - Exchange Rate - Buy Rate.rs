<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Label - Exchange Rate - Buy Rate</name>
   <tag></tag>
   <elementGuidId>04b6ddbe-db7a-4f91-a38a-f8cb8fe83719</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@ref_element = 'Object Repository/1 - Frame/6 - iFrame Detail Record Frame']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//label[contains(.,&quot;Exchange Rate&quot;)]/following-sibling::label[2])[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/1 - Frame/6 - iFrame Detail Record Frame</value>
   </webElementProperties>
</WebElementEntity>
