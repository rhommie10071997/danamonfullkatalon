<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Label - RTGS Fee</name>
   <tag></tag>
   <elementGuidId>3920ffe6-3d9a-43a9-9e8b-95ad2f71a97a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//label[contains(.,&quot;RTGS Fee&quot;)]/following-sibling::label)[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/1 - Frame/2 - Iframe mainframe</value>
   </webElementProperties>
</WebElementEntity>
