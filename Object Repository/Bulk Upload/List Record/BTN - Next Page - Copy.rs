<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>BTN - Next Page - Copy</name>
   <tag></tag>
   <elementGuidId>a36afa4a-a104-4470-9d9a-d94d90c76aab</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//span[@id='totalPage']/following-sibling::button[@class=&quot;next btn grid-nextpage&quot;]/i</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
