<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Label - Total Transaction AmountUSD - Equivalent to Debit Account</name>
   <tag></tag>
   <elementGuidId>f78dd298-9e9f-41d9-9d54-51739f8627ce</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//label[contains(.,&quot;Equivalent to Debit Account&quot;)]/following-sibling::label)[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/1 - Frame/6 - iFrame Detail Record Frame</value>
   </webElementProperties>
</WebElementEntity>
