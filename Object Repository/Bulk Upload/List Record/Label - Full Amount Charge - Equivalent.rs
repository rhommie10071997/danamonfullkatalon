<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Label - Full Amount Charge - Equivalent</name>
   <tag></tag>
   <elementGuidId>55244201-791f-49ae-aaa3-0892e55d2e6e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//label[contains(.,&quot;Full Amount Charge&quot;)]/following-sibling::label[3])[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/1 - Frame/6 - iFrame Detail Record Frame</value>
   </webElementProperties>
</WebElementEntity>
