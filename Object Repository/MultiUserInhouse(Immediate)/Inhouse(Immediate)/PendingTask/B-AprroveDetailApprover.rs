<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>B-AprroveDetailApprover</name>
   <tag></tag>
   <elementGuidId>e7f2c4be-341a-46cc-9c5d-09f36952512d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'btnApprove' and @ref_element = 'Object Repository/MainFrame']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>/html[1]/body[1]/div[2]/div[1]/form[1]/div[44]/div[1]/button[3]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>btnApprove</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/MainFrame</value>
   </webElementProperties>
</WebElementEntity>
