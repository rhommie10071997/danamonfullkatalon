<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>B-ShowDiAccList</name>
   <tag></tag>
   <elementGuidId>952cca7c-8216-4ae6-813a-0cd4d24988fb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@ref_element = 'Object Repository/LoginFrame']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[contains(.,&quot;Show&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/LoginFrame</value>
   </webElementProperties>
</WebElementEntity>
