<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Tf-AccountNumberDiAccountList</name>
   <tag></tag>
   <elementGuidId>134e3cbd-2e5f-4e12-8fe6-7900de0ab138</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'accountNo' and @ref_element = 'Object Repository/MultiUserInhouse(Immediate)/ExternalObject(immeduate)TransactionInquiry/iframe-accountFrame']</value>
      </entry>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>XPATH</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>accountNo</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/iframe-accountFrame</value>
   </webElementProperties>
</WebElementEntity>
