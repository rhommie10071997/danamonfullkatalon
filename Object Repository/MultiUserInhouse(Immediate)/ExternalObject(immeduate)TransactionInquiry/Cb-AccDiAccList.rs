<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Cb-AccDiAccList</name>
   <tag></tag>
   <elementGuidId>7e042890-acf2-45c1-810c-cb4c1ec46a0c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@ref_element = 'Object Repository/iframe-accountFrame']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>/html[1]/body[1]/form[1]/div[4]/div[1]/div[1]/table[1]/tbody[1]/tr[1]/td[1]/input[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/iframe-accountFrame</value>
   </webElementProperties>
</WebElementEntity>
