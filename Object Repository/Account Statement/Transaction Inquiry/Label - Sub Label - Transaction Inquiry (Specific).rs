<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Label - Sub Label - Transaction Inquiry (Specific)</name>
   <tag></tag>
   <elementGuidId>03492464-ef62-4480-b2a3-d07876480f8a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//ol[@class=&quot;breadcrumb custom-breadcrumb&quot;]/li[3][contains(.,&quot;Transaction Inquiry&quot;)]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/1 - Frame/2 - Iframe mainframe</value>
   </webElementProperties>
</WebElementEntity>
